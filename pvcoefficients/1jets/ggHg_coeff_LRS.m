(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{SeriesData[\[Beta], 0, {2 + Eps^(-1) - 2*Log[MT] + Log[Mu^2], I*Pi, -2}, 0, 
  3, 1], 2 + Eps^(-1) + DiscB[T, MT, MT] + Log[Mu^2/MT^2], 
 2 + Eps^(-1) + DiscB[U, MT, MT] + Log[Mu^2/MT^2], 
 Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2/(2*T), 
 SeriesData[\[Beta], 0, 
  {(Pi^2 + Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
    (2*(MH^2 - 4*MT^2)), ((2*I)*Pi)/(MH^2 - 4*MT^2), 
   (2*(-MH^2 + MT^2*(4 + Pi^2) + 
      MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2}, 0, 3, 1], 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(2*(MH^2 - U)), 
 SeriesData[\[Beta], 0, 
  {(Pi^2 + Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
    (2*(MH^2 - 4*MT^2)), ((2*I)*Pi)/(MH^2 - 4*MT^2), 
   (2*(-MH^2 + MT^2*(4 + Pi^2) + 
      MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2}, 0, 3, 1], SeriesData[\[Beta], 0, 
  {(Pi^2 + Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
    (2*(MH^2 - 4*MT^2)), ((2*I)*Pi)/(MH^2 - 4*MT^2), 
   (2*(-MH^2 + MT^2*(4 + Pi^2) + 
      MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2}, 0, 3, 1], 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(2*(MH^2 - T)), 
 (2 + DiscB[T, MT, MT])/T, SeriesData[\[Beta], 0, 
  {DiscB[MH^2, MT, MT]/(MH^2 - 4*MT^2), ((-I)*Pi)/(MH^2 - 4*MT^2), 
   (2*(MH^2 - 4*MT^2 + 2*MT^2*DiscB[MH^2, MT, MT]))/(MH^2 - 4*MT^2)^2}, 0, 3, 
  1], (DiscB[MH^2, MT, MT] - DiscB[U, MT, MT])/(MH^2 - U), 
 SeriesData[\[Beta], 0, 
  {-(4*MH^2 - 16*MT^2 + MH^2*Pi^2 + 4*MH^2*DiscB[MH^2, MT, MT] + 
      MH^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
    (2*(MH^2 - 4*MT^2)^2), ((-I)*Pi)/(MH^2 - 4*MT^2), 
   (-4*MT^2*(4*MH^2 - 16*MT^2 + MH^2*Pi^2 + 4*MH^2*DiscB[MH^2, MT, MT] + 
      MH^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^3}, 0, 3, 1], (-DiscB[MH^2, MT, MT] + DiscB[U, MT, MT])/
  (2*(MH^2 - U)), 
 -(8 + 4*DiscB[T, MT, MT] + Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)]^2)/(2*T), SeriesData[\[Beta], 0, 
  {((MH^2 + 4*MT^2)*DiscB[MH^2, MT, MT] + 
     2*(MH^2 + MT^2*(-4 + Pi^2) + 
       MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2, 0, (-2*MT^2*(8*MH^2 - 32*MT^2 + MH^2*Pi^2 + 
      4*MT^2*Pi^2 + (6*MH^2 + 8*MT^2)*DiscB[MH^2, MT, MT] + 
      (MH^2 + 4*MT^2)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
        2))/(-MH^2 + 4*MT^2)^3}, 0, 3, 1], SeriesData[\[Beta], 0, 
  {((MH^2 + 4*MT^2)*DiscB[MH^2, MT, MT] + 
     2*(MH^2 + MT^2*(-4 + Pi^2) + 
       MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2, 0, (-2*MT^2*(8*MH^2 - 32*MT^2 + MH^2*Pi^2 + 
      4*MT^2*Pi^2 + (6*MH^2 + 8*MT^2)*DiscB[MH^2, MT, MT] + 
      (MH^2 + 4*MT^2)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
        2))/(-MH^2 + 4*MT^2)^3}, 0, 3, 1], SeriesData[\[Beta], 0, 
  {DiscB[MH^2, MT, MT]/(MH^2 - 4*MT^2), ((-I)*Pi)/(MH^2 - 4*MT^2), 
   (2*(MH^2 - 4*MT^2 + 2*MT^2*DiscB[MH^2, MT, MT]))/(MH^2 - 4*MT^2)^2}, 0, 3, 
  1], (DiscB[MH^2, MT, MT] - DiscB[T, MT, MT])/(MH^2 - T), 
 (-MH^2 + U - MH^2*DiscB[MH^2, MT, MT] + MH^2*DiscB[U, MT, MT] - 
   MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
  (2*(MH^2 - U)^2), SeriesData[\[Beta], 0, 
  {-(MH^2 - 4*MT^2 + MT^2*Pi^2 + MH^2*DiscB[MH^2, MT, MT] + 
      MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
    (2*(MH^2 - 4*MT^2)^2), ((I/2)*Pi)/(MH^2 - 4*MT^2), 
   -((MH^4 - 4*MH^2*MT^2 + 4*MT^4*Pi^2 + 4*MH^2*MT^2*DiscB[MH^2, MT, MT] + 
      4*MT^4*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
     (MH^2 - 4*MT^2)^3)}, 0, 3, 1], 
 (-MH^2 + T - MH^2*DiscB[MH^2, MT, MT] + MH^2*DiscB[T, MT, MT] - 
   MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
  (2*(MH^2 - T)^2), SeriesData[\[Beta], 0, 
  {-DiscB[MH^2, MT, MT]/(2*(MH^2 - 4*MT^2)), ((I/2)*Pi)/(MH^2 - 4*MT^2), 
   -((MH^2 - 4*MT^2 + 2*MT^2*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^2)}, 0, 3, 
  1], (-DiscB[MH^2, MT, MT] + DiscB[T, MT, MT])/(2*(MH^2 - T)), 
 (3*MH^2 + MH^2/Eps - 3*U - U/Eps + MH^2*DiscB[MH^2, MT, MT] - 
   U*DiscB[U, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
       (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - U*Log[Mu^2/MT^2] - 
   MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(4*(MH^2 - U)), 
 SeriesData[\[Beta], 0, {(3 + Eps^(-1) + (MT^2*Pi^2)/(MH^2 - 4*MT^2) + 
     (MH^2*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) - 
     (MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2) + Log[Mu^2/MT^2])/4, 0, 
   (MT^2*(MH^2 - 4*MT^2 + MT^2*Pi^2 + MH^2*DiscB[MH^2, MT, MT] + 
      MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2))/
    (MH^2 - 4*MT^2)^2}, 0, 3, 1], 
 (3*MH^2 + MH^2/Eps - 3*T - T/Eps + MH^2*DiscB[MH^2, MT, MT] - 
   T*DiscB[T, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
       (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - T*Log[Mu^2/MT^2] - 
   MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(4*(MH^2 - T)), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT], 
 ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-1/(2*(MH^2 - 4*MT^2 - T)), 0, 
     (-2*MT^2)/(-MH^2 + 4*MT^2 + T)^2}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {T/(2*MH^2 - 8*MT^2 - 2*T), 0, 
     (2*MT^2*T)/(-MH^2 + 4*MT^2 + T)^2}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MH^2 - 4*MT^2)/(8*MT^2*(-MH^2 + 4*MT^2 + T)), 0, 
     -(MH^4 + 16*MT^4 - MH^2*(8*MT^2 + T))/(8*MT^2*(-MH^2 + 4*MT^2 + T)^2)}, 
    0, 4, 1] + SeriesData[\[Beta], 0, 
   {((MH^2 - 8*MT^2 - T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2 + 8*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
          (2*MT^2)]^2)/(16*MT^2*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)), 0, 
    ((MH^4 + 32*MT^4 + 8*MT^2*T + T^2 - 2*MH^2*(4*MT^2 + T))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      32*MT^4*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (16*MT^2*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 SeriesData[\[Beta], 0, {1/(8*MT^2*U*(-MH^2 + 4*MT^2 + U)), 0, 
    (MH^2 - 8*MT^2 - U)/(8*MT^2*U*(-MH^2 + 4*MT^2 + U)^2)}, 0, 3, 1]*
  (ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*(MH^2 - U), 0, -4*MT^2*(MH^2 - U), 0, 
      -4*MT^2*(MH^2 - U)}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2*(MH^2 - U)*U, 0, 4*MT^2*(MH^2 - U)*U, 0, 
      4*MT^2*(MH^2 - U)*U}, 0, 5, 1] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-MH^4 + 4*MT^2*U + MH^2*(4*MT^2 + U), 0, 
      4*MT^2*(MH^2 + U), 0, 4*MT^2*(MH^2 + U)}, 0, 5, 1] + 
   SeriesData[\[Beta], 0, 
    {((MH^2 - U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       2 - 4*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2, 0, 
     -4*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2, 0, 
     -4*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2}, 0, 5, 
    1]), (T*(-MH^2 + U)*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - 2*T - U)*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*MH^2*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
   T*U^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)), 
 SeriesData[\[Beta], 0, {-1/(2*(MH^2 - 4*MT^2 - T)*T), 0, 
    (-2*MT^2)/(T*(-MH^2 + 4*MT^2 + T)^2)}, 0, 3, 1]*
  (T*ScalarC0[0, 0, T, MT, MT, MT] + (-MH^2 + T)*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + MH^2*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] - 
   2*T*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2, 0, -4*MT^2, 0, -4*MT^2}, 0, 5, 1] + 
   ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*T, 0, -4*MT^2*T, 0, -4*MT^2*T}, 0, 5, 
     1]), SeriesData[\[Beta], 0, {-1/(2*(MH^2 - 4*MT^2 - U)*U), 0, 
    (-2*MT^2)/(U*(-MH^2 + 4*MT^2 + U)^2)}, 0, 3, 1]*
  (U*ScalarC0[0, 0, U, MT, MT, MT] + (-MH^2 + U)*ScalarC0[0, MH^2, U, MT, MT, 
     MT] + MH^2*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] - 
   2*U*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2, 0, -4*MT^2, 0, -4*MT^2}, 0, 5, 1] + 
   ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*U, 0, -4*MT^2*U, 0, -4*MT^2*U}, 0, 5, 
     1]), (T*(MH^2 - T - 2*U)*ScalarC0[0, 0, T, MT, MT, MT] + 
   (-MH^2 + T)*U*ScalarC0[0, 0, U, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   2*MH^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T^2*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   T*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
   T^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-2*MT^2)/(-MH^2 + 4*MT^2 + T)^2, 0, 
     (2*MT^2*(MH^2 + 4*MT^2 - T))/(-MH^2 + 4*MT^2 + T)^3, 0, 
     (-2*MT^2*(MH^2 - T)*(MH^2 + 8*MT^2 - T))/(-MH^2 + 4*MT^2 + T)^4}, 0, 5, 
    1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^2 - 4*MT^2)/(2*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (2*MT^2*(MH^2 - 4*MT^2 + T))/(MH^2 - 4*MT^2 - T)^3, 0, 
     (2*MT^2*(MH^4 - 4*MH^2*MT^2 + 8*MT^2*T - T^2))/(-MH^2 + 4*MT^2 + T)^4}, 
    0, 5, 1] + ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
    MT, MT]*SeriesData[\[Beta], 0, {(MT^2*(MH^2 - 4*MT^2 + T))/
      (-MH^2 + 4*MT^2 + T)^2, 0, (-2*MT^2*(8*MT^4 - 2*MT^2*T + T^2 - 
        MH^2*(2*MT^2 + T)))/(MH^2 - 4*MT^2 - T)^3, 0, 
     (-2*MT^2*(-(MH^4*(2*MT^2 + T)) - T*(8*MT^4 - 6*MT^2*T + T^2) + 
        2*MH^2*(4*MT^4 - 2*MT^2*T + T^2)))/(-MH^2 + 4*MT^2 + T)^4}, 0, 5, 
    1] + SeriesData[\[Beta], 0, 
   {((4*(2*MH^2 - 4*MT^2 - T)*DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)*
        (MH^2 - 4*MT^2 - T)) + (-4*(MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT] + 
        (MH^2 - T)*(Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
           2 - 2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/
       (-MH^2 + 4*MT^2 + T)^2)/(4*(MH^2 - T)), 
    ((-I)*Pi)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)), 
    2/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)) + 
     (4*MT^2*(3*MH^4 + (4*MT^2 + T)^2 - MH^2*(16*MT^2 + 3*T))*
       DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - T)*
       (-MH^2 + 4*MT^2 + T)^2) - (4*MT^2*DiscB[T, MT, MT])/
      ((MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2) - 
     (2*MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + T)^3 + 
     (4*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + T)^3, ((4*I)*MT^2*Pi*(-2*MH^2 + 8*MT^2 + T))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2), 
    (2*(MH^4 + MH^2*(16*MT^2 - T) - 8*MT^2*(10*MT^2 + T)))/
      (3*(MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2) + 
     (4*MH^2*MT^2*(3*MH^4 + 48*MT^4 + 12*MT^2*T + T^2 - 3*MH^2*(8*MT^2 + T))*
       DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - T)^3) + 
     (4*MT^2*DiscB[T, MT, MT])/(-MH^2 + 4*MT^2 + T)^3 + 
     (2*MT^2*(MH^2 + 2*MT^2 - T)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + T)^4 - (4*MT^2*(MH^2 + 2*MT^2 - T)*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + T)^4}, 0, 5, 1], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^2*(MH^2 - U))/(U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (-2*MT^2*(MH^2 - U)*(MH^2 + 4*MT^2 - U))/(U*(-MH^2 + 4*MT^2 + U)^3)}, 0, 
    4, 1] + ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
    MT, MT]*SeriesData[\[Beta], 0, 
    {-((MT^2*(MH^2 - U)*(MH^2 - 4*MT^2 + U))/(U*(-MH^2 + 4*MT^2 + U)^2)), 0, 
     (2*MT^2*(MH^4*(2*MT^2 + U) - 2*MH^2*(4*MT^4 + U^2) + 
        U*(8*MT^4 - 2*MT^2*U + U^2)))/(U*(-MH^2 + 4*MT^2 + U)^3)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^6 + 16*MT^4*U - MH^4*(8*MT^2 + 3*U) + 
       2*MH^2*(8*MT^4 + 4*MT^2*U + U^2))/(2*(MH^2 - 4*MT^2)*U*
       (-MH^2 + 4*MT^2 + U)^2), 0, 
     (-2*MT^2*(-MH^8 + 16*MT^4*(4*MT^2 - U)*U + 6*MH^6*(2*MT^2 + U) - 
        MH^4*(48*MT^4 + 44*MT^2*U + 7*U^2) + 2*MH^2*(32*MT^6 + 32*MT^4*U + 
          16*MT^2*U^2 + U^3)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3*U)}, 
    0, 4, 1] + SeriesData[\[Beta], 0, 
   {(8/(MH^2 - 4*MT^2) + (4*MH^2*(MH^2 - 4*MT^2 - 2*U)*DiscB[MH^2, MT, MT])/
       ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)) + (4*DiscB[U, MT, MT])/
       (MH^2 - 4*MT^2 - U) - 
      ((MH^2 - U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + U)^2 + 
      (2*(MH^2 - U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + U)^2)/(4*U), 
    ((-I)*Pi*(MH^4 - 4*MT^2*U - MH^2*(4*MT^2 + U)))/
     ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)*U), 
    (2*((MH^4 - MH^2*U - 8*MT^2*(2*MT^2 + U))/((MH^2 - 4*MT^2)^2*
         (MH^2 - 4*MT^2 - U)) + (2*MH^2*MT^2*(2*MH^4 - MH^2*(16*MT^2 + 7*U) + 
          4*(8*MT^4 + 7*MT^2*U + U^2))*DiscB[MH^2, MT, MT])/
        ((MH^2 - 4*MT^2)^3*(-MH^2 + 4*MT^2 + U)^2) + 
       (2*MT^2*DiscB[U, MT, MT])/(-MH^2 + 4*MT^2 + U)^2 + 
       (MT^2*(MH^2 - U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/(-MH^2 + 4*MT^2 + U)^3 - 
       (2*MT^2*(MH^2 - U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
          2)/(-MH^2 + 4*MT^2 + U)^3))/U, 
    ((-4*I)*MT^2*Pi*(2*MH^6 + 4*MT^2*U*(8*MT^2 + U) - MH^4*(16*MT^2 + 5*U) + 
       MH^2*(32*MT^4 + 12*MT^2*U + 3*U^2)))/((MH^2 - 4*MT^2)^3*U*
      (-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 -(4*(-MH^2 + T + U)^2 - 2*MH^2*(MH^2 - T - U)*DiscB[MH^2, MT, MT] + 
    2*(MH^2 - U)*(MH^2 - T - U)*DiscB[T, MT, MT] + 
    2*(MH^2 - T)*(MH^2 - T - U)*DiscB[U, MT, MT] + 
    T^2*U*ScalarC0[0, 0, T, MT, MT, MT] + 
    T*U^2*ScalarC0[0, 0, U, MT, MT, MT] - (MH^2 - T)*T*U*
     ScalarC0[0, MH^2, T, MT, MT, MT] - T*(MH^2 - U)*U*
     ScalarC0[0, MH^2, U, MT, MT, MT] + (2*MH^6*MT^2 - 4*MH^4*MT^2*(T + U) + 
      2*MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + 2*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*T*(3*MH^2 - 12*MT^2 + T))/
      (2*(MH^2 - 4*MT^2 - T)^3), 0, 
     (-2*MT^2*T*(24*MT^4 - 2*MT^2*T + T^2 - MH^2*(6*MT^2 + T)))/
      (-MH^2 + 4*MT^2 + T)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2 + 3*T))/
      (2*(MH^2 - 4*MT^2 - T)^3), 0, 
     (2*MT^2*(8*MT^4 - 6*MT^2*T + T^2 - MH^2*(2*MT^2 + T)))/
      (-MH^2 + 4*MT^2 + T)^4}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 + 3*T))/
      (8*(MH^2 - 4*MT^2 - T)^3), 0, 
     -(MH^6 - 2*MH^4*(8*MT^2 + T) + MH^2*(80*MT^4 - 16*MT^2*T + T^2) - 
        16*MT^2*(8*MT^4 - 6*MT^2*T + T^2))/(8*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 
    1] + SeriesData[\[Beta], 0, 
   {-(-8*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2 - 
       (8*(MH^2 - 4*MT^2 - T)*(MH^6 - 4*MH^2*T*(2*MT^2 + T) + 
          T*(4*MT^2 + T)^2 + MH^4*(-4*MT^2 + 2*T))*DiscB[MH^2, MT, MT])/
        (MH^2 - 4*MT^2) + 8*(3*MH^2 - 4*MT^2 - 3*T)*(MH^2 - 4*MT^2 - T)*T*
        DiscB[T, MT, MT] + (MH^6 - 128*MT^6 - 96*MT^4*T - 20*MT^2*T^2 - 
         5*T^3 - MH^4*(20*MT^2 + 7*T) + MH^2*(96*MT^4 + 40*MT^2*T + 11*T^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
       8*(16*MT^6 + 12*MT^4*T + 2*MT^2*T^2 + T^3 + MH^4*(2*MT^2 + T) - 
         2*MH^2*(6*MT^4 + 2*MT^2*T + T^2))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (16*(MH^2 - T)^2*(MH^2 - 4*MT^2 - T)^3), ((-I/2)*Pi*(MH^2 - 4*MT^2 + T))/
     ((MH^2 - 4*MT^2)*(-MH^2 + 4*MT^2 + T)^2), 
    ((16*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2*(MH^4 - 2*MH^2*MT^2 - 8*MT^4 + 
         4*MT^2*T - T^2))/(MH^2 - 4*MT^2) + 
      (32*MT^2*(-MH^2 + 4*MT^2 + T)*(-2*MH^8 + 4*MH^6*(4*MT^2 - T) + 
         T*(4*MT^2 + T)^3 - MH^2*T*(48*MT^4 + 48*MT^2*T + 5*T^2) + 
         MH^4*(-32*MT^4 + 24*MT^2*T + 10*T^2))*DiscB[MH^2, MT, MT])/
       (MH^2 - 4*MT^2)^2 - 32*MT^2*T*(-MH^2 + 4*MT^2 + T)*
       (-5*MH^2 + 4*MT^2 + 5*T)*DiscB[T, MT, MT] + 
      (MH^8 + 512*MT^8 + 512*MT^6*T + 144*MT^4*T^2 + 64*MT^2*T^3 + T^4 - 
        4*MH^6*(4*MT^2 + T) + 6*MH^4*(24*MT^4 + 16*MT^2*T + T^2) - 
        4*MH^2*(128*MT^6 + 72*MT^4*T + 36*MT^2*T^2 + T^3))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      32*MT^2*(16*MT^6 + 16*MT^4*T + 3*MT^2*T^2 + 3*T^3 + 3*MH^4*(MT^2 + T) - 
        2*MH^2*(8*MT^4 + 3*MT^2*T + 3*T^2))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (16*(MH^2 - T)^2*(-MH^2 + 4*MT^2 + T)^4), 
    ((-2*I)*MT^2*Pi*(2*MH^4 + 32*MT^4 - 12*MT^2*T - T^2 + 
       MH^2*(-16*MT^2 + 3*T)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3)}, 0, 
   4, 1], ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, 
    MT]*SeriesData[\[Beta], 0, {(MT^2*(MH^2 - U)^2*(3*MH^2 - 12*MT^2 + U))/
      (2*(MH^2 - 4*MT^2 - U)^3*U), 0, 
     (-2*MT^2*(MH^2 - U)^2*(24*MT^4 - 2*MT^2*U + U^2 - MH^2*(6*MT^2 + U)))/
      (U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - U)^2*(MH^2 - 4*MT^2 + 3*U))/
      (2*(MH^2 - 4*MT^2 - U)^3*U^2), 0, 
     (2*MT^2*(MH^2 - U)^2*(8*MT^4 - 6*MT^2*U + U^2 - MH^2*(2*MT^2 + U)))/
      (U^2*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-MH^12 + MH^10*(16*MT^2 - U) + 
       64*MT^6*(4*MT^2 - 3*U)*U^2 + MH^8*(-96*MT^4 + 4*MT^2*U + 13*U^2) + 
       MH^6*(256*MT^6 + 48*MT^4*U - 40*MT^2*U^2 - 19*U^3) + 
       16*MH^2*MT^2*U*(32*MT^6 + 56*MT^4*U + 21*MT^2*U^2 + U^3) - 
       4*MH^4*(64*MT^8 + 80*MT^6*U + 72*MT^4*U^2 - MT^2*U^3 - 2*U^4))/
      (8*(MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3*U^2), 0, 
     (MH^16 - 4*MH^14*(7*MT^2 + U) - 1024*MT^8*U^2*(8*MT^4 - 6*MT^2*U + 
         U^2) + MH^12*(320*MT^4 + 64*MT^2*U + 6*U^2) - 
       4*MH^10*(480*MT^6 + 112*MT^4*U - 22*MT^2*U^2 + U^3) - 
       4*MH^6*MT^2*(2816*MT^8 + 1792*MT^6*U + 16*MT^4*U^2 - 488*MT^2*U^3 - 
         65*U^4) + MH^8*(6400*MT^8 + 2048*MT^6*U - 1008*MT^4*U^2 - 
         320*MT^2*U^3 + U^4) - 64*MH^2*MT^4*U*(256*MT^8 + 528*MT^6*U + 
         248*MT^4*U^2 + 19*MT^2*U^3 + 2*U^4) + 16*MH^4*MT^2*
        (512*MT^10 + 1024*MT^8*U + 1136*MT^6*U^2 + 72*MT^4*U^3 - 
         43*MT^2*U^4 - 4*U^5))/(8*(MH^2 - 4*MT^2)^3*U^2*
       (-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {-((32*U*(-MH^2 + 4*MT^2 + U)^2*(2*MH^4 + 2*MT^2*(6*MT^2 + U) - 
          MH^2*(11*MT^2 + 2*U)))/(MH^2 - 4*MT^2)^2 + 
       (8*MH^2*(MH^2 - 4*MT^2 - U)*U*(3*MH^6 + 32*MT^4*U - 
          3*MH^4*(8*MT^2 + 3*U) + MH^2*(48*MT^4 + 28*MT^2*U + 6*U^2))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + 8*(3*MH^2 - 4*MT^2 - 3*U)*
        (MH^2 - 4*MT^2 - U)*U*DiscB[U, MT, MT] - (MH^2 - U)^2*
        (MH^2 - 4*MT^2 + 3*U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + 8*(16*MT^6 + 12*MT^4*U + 2*MT^2*U^2 + U^3 + 
         MH^4*(2*MT^2 + U) - 2*MH^2*(6*MT^4 + 2*MT^2*U + U^2))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (16*(MH^2 - 4*MT^2 - U)^3*U^2), 
    ((I/2)*Pi*(3*MH^8 + 16*MT^4*(4*MT^2 - U)*U + 16*MH^2*MT^2*U*
        (3*MT^2 + U) - 6*MH^6*(4*MT^2 + U) + 
       MH^4*(48*MT^4 + 8*MT^2*U + 3*U^2)))/((MH^2 - 4*MT^2)^3*U*
      (-MH^2 + 4*MT^2 + U)^2), 
    -((16*U*(-MH^2 + 4*MT^2 + U)^2*(3*MH^8 + 2*MH^6*(MT^2 - 3*U) - 
          32*MT^4*(12*MT^4 + 2*MT^2*U + U^2) + MH^4*(-184*MT^4 - 46*MT^2*U + 
            3*U^2) + 4*MH^2*(152*MT^6 + 74*MT^4*U + 11*MT^2*U^2)))/
        (MH^2 - 4*MT^2)^3 + (32*MH^2*MT^2*(MH^2 - 4*MT^2 - U)*U*
         (9*MH^8 - 32*MT^4*U*(12*MT^2 + U) - MH^6*(108*MT^2 + 41*U) + 
          MH^4*(432*MT^4 + 304*MT^2*U + 50*U^2) - 
          2*MH^2*(288*MT^6 + 232*MT^4*U + 96*MT^2*U^2 + 9*U^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 + 
       32*MT^2*U*(-MH^2 + 4*MT^2 + U)*(-5*MH^2 + 4*MT^2 + 5*U)*
        DiscB[U, MT, MT] + (MH^2 - U)^2*(MH^4 + 48*MT^4 - 32*MT^2*U + U^2 - 
         2*MH^2*(8*MT^2 + U))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + 32*MT^2*(16*MT^6 + 16*MT^4*U + 3*MT^2*U^2 + 3*U^3 + 
         3*MH^4*(MT^2 + U) - 2*MH^2*(8*MT^4 + 3*MT^2*U + 3*U^2))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (16*U^2*(-MH^2 + 4*MT^2 + U)^4), 
    ((-2*I)*MT^2*Pi*(-9*MH^10 + MH^8*(108*MT^2 + 31*U) + 
       16*MT^4*U*(32*MT^4 - 12*MT^2*U - U^2) + 8*MH^2*MT^2*U*
        (80*MT^4 + 46*MT^2*U + 3*U^2) - MH^6*(432*MT^4 + 192*MT^2*U + 
         35*U^2) + MH^4*(576*MT^6 + 80*MT^4*U + 60*MT^2*U^2 + 13*U^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - U)^3*U)}, 0, 4, 1], 
 -(T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 3*T + 2*U) + 
    (MH^2*T*(MH^2 - T - U)*U*(MH^4 + 2*T*U - MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/(MH^2 - T) + 
    (T*U*(-MH^2 + T + U)*(MH^6 + T*(T - U)*U + MH^2*U*(2*T + U) - 
       MH^4*(T + 2*U))*DiscB[T, MT, MT])/(MH^2 - T) + 
    T*U*(-MH^2 + T + U)*(MH^4 - MH^2*(2*T + U) + T*(T + 3*U))*
     DiscB[U, MT, MT] + T*(MH^2 - U)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     ScalarC0[0, 0, T, MT, MT, MT] + 
    U*(-(MH^8*MT^2) + MH^6*MT^2*(2*T + 3*U) + 
      MH^4*MT^2*(T^2 - 3*T*U - 3*U^2) - 
      MH^2*(T^2*U^2 + MT^2*(4*T^3 + 6*T^2*U - U^3)) + 
      T*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)))*
     ScalarC0[0, 0, U, MT, MT, MT] + (MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 
      3*MH^6*MT^2*(T + U)^2 - MH^2*T^2*U^2*(-MT^2 + T + U) + 
      T^2*U^2*(T*U - MT^2*(T + U)) - MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*
     ScalarC0[0, MH^2, T, MT, MT, MT] - (MH^2 - U)^2*
     (MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + MH^2*MT^2*(T^2 + T*U + U^2) + 
      T*U*(-(T*U) + MT^2*(T + U)))*ScalarC0[0, MH^2, U, MT, MT, MT] - 
    T*(MH^2 - U)*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-8*MT^4)/(T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (16*MT^4*(MH^2 - T))/(T*(-MH^2 + 4*MT^2 + T)^3)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(4*MT^4*(MH^2 - 4*MT^2 + T))/
      (T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (-4*MT^4*(MH^2 - T)*(MH^2 - 4*MT^2 + 3*T))/(T*(-MH^2 + 4*MT^2 + T)^3)}, 
    0, 4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(-2*MT^2*(MH^4 - 4*MH^2*(2*MT^2 + T) + 2*(8*MT^4 + 8*MT^2*T + T^2)))/
      ((MH^2 - 4*MT^2)*T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (2*MT^2*(-MH^8 + 128*MT^6*T + MH^6*(12*MT^2 + 5*T) - 
        2*MH^4*(24*MT^4 + 16*MT^2*T + 3*T^2) + 
        2*MH^2*(32*MT^6 + 8*MT^4*T + 12*MT^2*T^2 + T^3)))/
      ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3*T)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {-((2/(MH^2 - 4*MT^2) + ((MH^2*(4*MT^2 - T) - 4*MT^2*(4*MT^2 + T))*
         DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)) + 
       DiscB[T, MT, MT]/(MH^2 - 4*MT^2 - T) - 
       (MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
        (-MH^2 + 4*MT^2 + T)^2 + 
       (2*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
        (-MH^2 + 4*MT^2 + T)^2)/T), ((4*I)*MT^2*Pi*(MH^2 - 4*MT^2 - 2*T))/
     ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)*T), 
    (MT^2*((8*(-2*MH^2 + 8*MT^2 + 3*T))/((MH^2 - 4*MT^2)^2*
         (MH^2 - 4*MT^2 - T)) - (4*(MH^6 + 4*MT^2*(4*MT^2 + T)^2 - 
          MH^4*(4*MT^2 + 5*T) + MH^2*(-16*MT^4 + 12*MT^2*T + 3*T^2))*
         DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(-MH^2 + 4*MT^2 + T)^2) - 
       (4*DiscB[T, MT, MT])/(-MH^2 + 4*MT^2 + T)^2 + 
       ((MH^2 + 4*MT^2 - T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/(MH^2 - 4*MT^2 - T)^3 - 
       (2*(MH^2 + 4*MT^2 - T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
            (2*MT^2)]^2)/(MH^2 - 4*MT^2 - T)^3))/T, 
    ((4*I)*MT^2*Pi*(MH^6 - MH^4*(4*MT^2 + 3*T) - 
       2*MH^2*(8*MT^4 + 2*MT^2*T - T^2) + 8*MT^2*(8*MT^4 + 8*MT^2*T + T^2)))/
     ((MH^2 - 4*MT^2)^3*T*(-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-8*MT^4)/(U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (16*MT^4*(MH^2 - U))/(U*(-MH^2 + 4*MT^2 + U)^3)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(4*MT^4*(MH^2 - 4*MT^2 + U))/
      (U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (-4*MT^4*(MH^2 - U)*(MH^2 - 4*MT^2 + 3*U))/(U*(-MH^2 + 4*MT^2 + U)^3)}, 
    0, 4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(-2*MT^2*(MH^4 - 4*MH^2*(2*MT^2 + U) + 2*(8*MT^4 + 8*MT^2*U + U^2)))/
      ((MH^2 - 4*MT^2)*U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (2*MT^2*(-MH^8 + 128*MT^6*U + MH^6*(12*MT^2 + 5*U) - 
        2*MH^4*(24*MT^4 + 16*MT^2*U + 3*U^2) + 
        2*MH^2*(32*MT^6 + 8*MT^4*U + 12*MT^2*U^2 + U^3)))/
      ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3*U)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {-((2/(MH^2 - 4*MT^2) + ((MH^2*(4*MT^2 - U) - 4*MT^2*(4*MT^2 + U))*
         DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)) + 
       DiscB[U, MT, MT]/(MH^2 - 4*MT^2 - U) - 
       (MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
        (-MH^2 + 4*MT^2 + U)^2 + 
       (2*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
        (-MH^2 + 4*MT^2 + U)^2)/U), ((4*I)*MT^2*Pi*(MH^2 - 4*MT^2 - 2*U))/
     ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)*U), 
    (MT^2*((8*(-2*MH^2 + 8*MT^2 + 3*U))/((MH^2 - 4*MT^2)^2*
         (MH^2 - 4*MT^2 - U)) - (4*(MH^6 + 4*MT^2*(4*MT^2 + U)^2 - 
          MH^4*(4*MT^2 + 5*U) + MH^2*(-16*MT^4 + 12*MT^2*U + 3*U^2))*
         DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(-MH^2 + 4*MT^2 + U)^2) - 
       (4*DiscB[U, MT, MT])/(-MH^2 + 4*MT^2 + U)^2 + 
       ((MH^2 + 4*MT^2 - U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/(MH^2 - 4*MT^2 - U)^3 - 
       (2*(MH^2 + 4*MT^2 - U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
            (2*MT^2)]^2)/(MH^2 - 4*MT^2 - U)^3))/U, 
    ((4*I)*MT^2*Pi*(MH^6 - MH^4*(4*MT^2 + 3*U) - 
       2*MH^2*(8*MT^4 + 2*MT^2*U - U^2) + 8*MT^2*(8*MT^4 + 8*MT^2*U + U^2)))/
     ((MH^2 - 4*MT^2)^3*U*(-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 (4*(MH^2 - 2*U)*(MH^2 - U)*(-MH^2 + T + U)^2 - 2*MH^2*(MH^2 - T - U)*
    (MH^4 - T*U - MH^2*(T + U))*DiscB[MH^2, MT, MT] + 
   2*(MH^2 - T - 2*U)*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] + 
   2*(MH^2 - T - U)*(MH^6 - T*U^2 - MH^4*(T + U))*DiscB[U, MT, MT] + 
   T*(MH^2 - U)^2*(MH^4 + T^2 + 4*T*U + 2*U^2 - 2*MH^2*(T + 2*U))*
    ScalarC0[0, 0, T, MT, MT, MT] - (MH^2 - T)^2*(MH^2 - U)^2*U*
    ScalarC0[0, 0, U, MT, MT, MT] + (MH^2 - T)^3*(MH^2 - U)^2*
    ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - U)*(MH^8 - T^2*U^2 - 2*MH^6*(T + U) + 2*MH^2*T*U*(T + U) + 
     MH^4*(T^2 + U^2))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   (MH^2 - T)^2*(MH^2 - U)^2*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*(MH^2 - U)^2*U*(-MH^2 + T + U)^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(3*MH^2 - 12*MT^2 + T))/
      (MH^2 - 4*MT^2 - T)^3, 0, (2*MT^4*(3*MH^4 - 48*MT^4 + 2*MH^2*T + 
        16*MT^2*T - 5*T^2))/(-MH^2 + 4*MT^2 + T)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - 4*MT^2 + 3*T))/
      (T*(-MH^2 + 4*MT^2 + T)^3), 0, 
     (-2*MT^4*(MH^4 - 16*MT^4 + 6*MH^2*T + 16*MT^2*T - 7*T^2))/
      (T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(-MH^6 + 3*MH^4*(4*MT^2 + 3*T) - 6*MH^2*(8*MT^4 + 12*MT^2*T + 
          T^2) + 2*(32*MT^6 + 72*MT^4*T + 12*MT^2*T^2 + T^3)))/
      (2*(MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)^3*T), 0, 
     (-2*MT^2*(MH^8*(MT^2 - T) + MH^6*(-16*MT^4 + T^2) + 
        MH^4*(96*MT^6 + 96*MT^4*T - 3*MT^2*T^2) - 8*MH^2*MT^2*
         (32*MT^6 + 64*MT^4*T + 3*MT^2*T^2 + T^3) + 
        2*MT^2*(128*MT^8 + 384*MT^6*T + 40*MT^4*T^2 + 16*MT^2*T^3 + T^4)))/
      ((MH^2 - 4*MT^2)^2*T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, {((2*(-MH^2 + 4*MT^2 + T)^2)/(MH^2 - 4*MT^2) + 
      (2*(MH^2 - 4*MT^2 - T)*(MH^6 + MH^4*(8*MT^2 - T) + 
         4*MT^2*(4*MT^2 + T)^2 - 8*MH^2*MT^2*(8*MT^2 + T))*
        DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - T)) - 
      (2*(MH^2 - 4*MT^2 - T)*(MH^2 + 4*MT^2 - T)*DiscB[T, MT, MT])/
       (MH^2 - T) - (MT^2*(-MH^2 + 4*MT^2 - 3*T)*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/T + 
      (2*MT^2*(-MH^2 + 4*MT^2 - 3*T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
           (2*MT^2)]^2)/T)/(4*(MH^2 - 4*MT^2 - T)^3), 
    ((2*I)*MT^2*Pi*(-3*MH^2 + 12*MT^2 + T))/((MH^2 - 4*MT^2)^2*
      (-MH^2 + 4*MT^2 + T)^2), 
    (MT^2*((2*(8*MH^2 - 32*MT^2 - 3*T)*(-MH^2 + 4*MT^2 + T)^2)/
        (MH^2 - 4*MT^2)^2 + (2*(MH^2 - 4*MT^2 - T)*
         (6*MH^8 - 4*MT^2*(4*MT^2 + T)^3 - 2*MH^6*(16*MT^2 + 5*T) + 
          MH^4*(-48*MT^4 + 40*MT^2*T + 5*T^2) + MH^2*(384*MT^6 + 48*MT^4*T - 
            T^3))*DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(MH^2 - T)) - 
       (2*(3*MH^2 + 4*MT^2 - 3*T)*(MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT])/
        (MH^2 - T) + ((-8*MT^4 + 6*MT^2*T - T^2 + MH^2*(2*MT^2 + T))*
         Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/T + 
       (2*(8*MT^4 - 6*MT^2*T + T^2 - MH^2*(2*MT^2 + T))*
         Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T))/
     (-MH^2 + 4*MT^2 + T)^4, ((-2*I)*MT^2*Pi*(3*MH^6 - 4*MH^4*T + 
       MH^2*(-144*MT^4 + 4*MT^2*T + T^2) + 4*MT^2*(96*MT^4 + 12*MT^2*T + 
         T^2)))/((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - U)*(3*MH^2 - 12*MT^2 + U))/
      (U*(-MH^2 + 4*MT^2 + U)^3), 0, 
     (2*MT^4*(-3*MH^6 + MH^4*U + U*(-48*MT^4 + 16*MT^2*U - 5*U^2) + 
        MH^2*(48*MT^4 - 16*MT^2*U + 7*U^2)))/(U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 
    4, 1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-2*MT^4*(MH^2 - U)*(MH^2 - 4*MT^2 + 3*U))/
      (U^2*(-MH^2 + 4*MT^2 + U)^3), 0, 
     (2*MT^4*(MH^6 + 5*MH^4*U + MH^2*(-16*MT^4 + 16*MT^2*U - 13*U^2) + 
        U*(16*MT^4 - 16*MT^2*U + 7*U^2)))/(U^2*(-MH^2 + 4*MT^2 + U)^4)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(MH^10 + 2*MH^8*(-8*MT^2 + U) + MH^6*(96*MT^4 - 20*MT^2*U - 
          21*U^2) - 8*MT^2*U*(32*MT^6 + 72*MT^4*U + 12*MT^2*U^2 + U^3) + 
        4*MH^4*(-64*MT^6 + 12*MT^4*U + 33*MT^2*U^2 + 7*U^3) + 
        2*MH^2*(128*MT^8 + 32*MT^6*U - 24*MT^4*U^2 - 44*MT^2*U^3 - 5*U^4)))/
      (2*(MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3*U^2), 0, 
     (-2*MT^2*(-(MH^12*(MT^2 + U)) + MH^10*(20*MT^4 + 13*MT^2*U + 6*U^2) - 
        MH^8*(160*MT^6 + 52*MT^4*U + 21*MT^2*U^2 + 11*U^3) - 
        8*MT^4*U*(128*MT^8 + 384*MT^6*U + 40*MT^4*U^2 + 16*MT^2*U^3 + U^4) + 
        MH^6*(640*MT^8 + 32*MT^6*U - 276*MT^4*U^2 + MT^2*U^3 + 8*U^4) + 
        2*MH^2*MT^2*(512*MT^10 + 128*MT^8*U - 480*MT^6*U^2 - 616*MT^4*U^3 - 
          92*MT^2*U^4 - 7*U^5) - 2*MH^4*(640*MT^10 - 64*MT^8*U - 
          744*MT^6*U^2 - 250*MT^4*U^3 - 11*MT^2*U^4 + U^5)))/
      ((MH^2 - 4*MT^2)^3*U^2*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {((-2*U*(-MH^2 + 4*MT^2 + U)^2*(-3*MH^4 + 4*MH^2*MT^2 + 32*MT^4 + 
         3*MH^2*U + 12*MT^2*U))/(MH^2 - 4*MT^2)^2 + 
      (2*MH^2*(MH^2 - 4*MT^2 - U)*U*(MH^4*(12*MT^2 - U) + 
         MH^2*(-96*MT^4 - 36*MT^2*U + U^2) + 4*(48*MT^6 + 40*MT^4*U + 
           5*MT^2*U^2))*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + 
      2*(MH^2 - 4*MT^2 - U)*(MH^2 + 4*MT^2 - U)*U*DiscB[U, MT, MT] - 
      MT^2*(MH^2 - U)*(MH^2 - 4*MT^2 + 3*U)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      2*MT^2*(MH^2 - U)*(MH^2 - 4*MT^2 + 3*U)*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (4*(MH^2 - 4*MT^2 - U)^3*U^2), 
    ((-2*I)*MT^2*Pi*(3*MH^6 - 8*MH^4*(3*MT^2 + U) + 4*MT^2*U*(12*MT^2 + U) + 
       MH^2*(48*MT^4 + 20*MT^2*U + 5*U^2)))/((MH^2 - 4*MT^2)^3*U*
      (-MH^2 + 4*MT^2 + U)^2), 
    (MT^2*((2*U*(-MH^2 + 4*MT^2 + U)^2*(14*MH^6 - MH^4*(104*MT^2 + 33*U) + 
          MH^2*(160*MT^4 + 84*MT^2*U + 19*U^2) + 4*(32*MT^6 + 48*MT^4*U + 
            5*MT^2*U^2)))/(MH^2 - 4*MT^2)^3 + 
       (2*MH^2*(MH^2 - 4*MT^2 - U)*U*(3*MH^8 - MH^6*(12*MT^2 + 17*U) + 
          MH^4*(-144*MT^4 + 16*MT^2*U + 22*U^2) + 
          8*MH^2*(120*MT^6 + 86*MT^4*U + 4*MT^2*U^2 - U^3) - 
          8*(192*MT^8 + 240*MT^6*U + 60*MT^4*U^2 + 5*MT^2*U^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 + 2*(3*MH^2 + 4*MT^2 - 3*U)*
        (MH^2 - 4*MT^2 - U)*U*DiscB[U, MT, MT] - 
       (MH^4*(2*MT^2 + U) + U*(8*MT^4 - 6*MT^2*U + U^2) - 
         2*MH^2*(4*MT^4 - 2*MT^2*U + U^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
       2*(MH^4*(2*MT^2 + U) + U*(8*MT^4 - 6*MT^2*U + U^2) - 
         2*MH^2*(4*MT^4 - 2*MT^2*U + U^2))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
     (U^2*(-MH^2 + 4*MT^2 + U)^4), 
    ((2*I)*MT^2*Pi*(-3*MH^10 + MH^8*(12*MT^2 + 11*U) + 
       MH^6*(144*MT^4 + 32*MT^2*U - 13*U^2) + 
       16*MT^4*U*(96*MT^4 + 12*MT^2*U + U^2) - 
       MH^4*(960*MT^6 + 688*MT^4*U + 92*MT^2*U^2 - 5*U^3) + 
       48*MH^2*MT^2*(32*MT^6 + 24*MT^4*U + 11*MT^2*U^2 + U^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - U)^3*U)}, 0, 4, 1], 
 -(T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 2*T + 3*U) + 
    (MH^2*T*(MH^2 - T - U)*U*(MH^4 + 2*T*U - MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/(MH^2 - U) - T*(MH^2 - T - U)*U*
     (MH^4 + U*(3*T + U) - MH^2*(T + 2*U))*DiscB[T, MT, MT] + 
    (T*U*(-MH^2 + T + U)*(MH^6 + T*U*(-T + U) - MH^4*(2*T + U) + 
       MH^2*T*(T + 2*U))*DiscB[U, MT, MT])/(MH^2 - U) + 
    T*(-(MH^8*MT^2) + MH^6*MT^2*(3*T + 2*U) + 
      MH^4*MT^2*(-3*T^2 - 3*T*U + U^2) + 
      MH^2*(-(T^2*U^2) + MT^2*(T^3 - 6*T*U^2 - 4*U^3)) + 
      U*(T^3*U + MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3)))*
     ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)*U*
     (MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + MH^2*MT^2*(T^2 + T*U + U^2) + 
      T*U*(-(T*U) + MT^2*(T + U)))*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)^2*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 3*MH^6*MT^2*(T + U)^2 - 
      MH^2*T^2*U^2*(-MT^2 + T + U) + T^2*U^2*(T*U - MT^2*(T + U)) - 
      MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*ScalarC0[0, MH^2, U, MT, MT, MT] - 
    (MH^2 - T)*T*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(8*MT^6*(3*MH^2 - 12*MT^2 + T))/
      ((MH^2 - 4*MT^2 - T)^3*T), 0, (48*MT^6*(MH^2 - T)*(MH^2 - 4*MT^2 + T))/
      (T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-8*MT^6*(MH^2 - 4*MT^2 + 3*T))/
      ((MH^2 - 4*MT^2 - T)^3*T^2), 0, 
     (-16*MT^6*(MH^2 - T)*(MH^2 - 4*MT^2 + 5*T))/
      (T^2*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(-2*MT^4*(MH^8 + MH^6*(-16*MT^2 + 3*T) + 6*MH^4*(16*MT^4 - 6*MT^2*T - 
          5*T^2) + MH^2*(-256*MT^6 + 144*MT^4*T + 240*MT^2*T^2 + 34*T^3) + 
        4*(64*MT^8 - 48*MT^6*T - 120*MT^4*T^2 - 34*MT^2*T^3 - 3*T^4)))/
      ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3*T^2), 0, 
     (2*MT^4*(-MH^12 + MH^10*(20*MT^2 - 6*T) + 
        MH^8*(-160*MT^4 + 88*MT^2*T + 61*T^2) + 
        4*MH^6*(160*MT^6 - 112*MT^4*T - 157*MT^2*T^2 - 28*T^3) - 
        16*MT^2*T*(128*MT^8 + 416*MT^6*T + 96*MT^4*T^2 + 16*MT^2*T^3 + T^4) + 
        MH^4*(-1280*MT^8 + 768*MT^6*T + 1680*MT^4*T^2 + 800*MT^2*T^3 + 
          78*T^4) + 4*MH^2*(256*MT^10 + 128*MT^8*T + 272*MT^6*T^2 - 
          256*MT^4*T^3 - 62*MT^2*T^4 - 5*T^5)))/((MH^2 - 4*MT^2)^3*T^2*
       (-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((2*T*(-MH^2 + 4*MT^2 + T)^2*(MH^4 - MH^2*(18*MT^2 + T) + 
         8*MT^2*(7*MT^2 + 2*T)))/(MH^2 - 4*MT^2)^2 - 
      ((MH^2 - 4*MT^2 - T)*T*(MH^6*T + 48*MT^4*(4*MT^2 + T)^2 + 
         MH^4*(48*MT^4 - 28*MT^2*T - T^2) + 16*MH^2*MT^2*(-24*MT^4 + T^2))*
        DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + (MH^2 - 12*MT^2 - T)*
       (MH^2 - 4*MT^2 - T)*T*DiscB[T, MT, MT] + 2*MT^4*(MH^2 - 4*MT^2 + 3*T)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      4*MT^4*(MH^2 - 4*MT^2 + 3*T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
          (2*MT^2)]^2)/(2*(MH^2 - 4*MT^2 - T)^3*T^2), 
    ((8*I)*MT^4*Pi*(3*MH^4 + 48*MT^4 + 44*MT^2*T + 6*T^2 - 
       MH^2*(24*MT^2 + 11*T)))/((MH^2 - 4*MT^2)^3*T*(-MH^2 + 4*MT^2 + T)^2), 
    (MT^2*((-2*T*(-MH^2 + 4*MT^2 + T)^2*(3*MH^6 + 7*MH^4*(4*MT^2 - T) - 
          4*MH^2*(92*MT^4 + 31*MT^2*T - T^2) + 16*MT^2*(52*MT^4 + 38*MT^2*T + 
            5*T^2)))/(MH^2 - 4*MT^2)^3 - (2*(MH^2 - 4*MT^2 - T)*T*
         (MH^8*(24*MT^2 - 2*T) - 48*MT^4*(4*MT^2 + T)^3 + 
          MH^6*(-240*MT^4 - 128*MT^2*T + 3*T^2) + 
          MH^4*(576*MT^6 + 976*MT^4*T + 148*MT^2*T^2 - T^3) + 
          8*MH^2*(96*MT^8 - 144*MT^6*T - 62*MT^4*T^2 - 7*MT^2*T^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 - 2*(MH^2 - 4*MT^2 - T)*
        (MH^2 + 12*MT^2 - T)*T*DiscB[T, MT, MT] + 
       MT^2*(MH^4 - 16*MT^4 + 6*MH^2*T + 16*MT^2*T - 7*T^2)*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
       2*MT^2*(MH^4 - 16*MT^4 + 6*MH^2*T + 16*MT^2*T - 7*T^2)*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/
     (T^2*(-MH^2 + 4*MT^2 + T)^4), 
    ((-16*I)*MT^4*Pi*(-3*MH^8 + 2*MH^6*(15*MT^2 + 7*T) - 
       MH^4*(72*MT^4 + 74*MT^2*T + 17*T^2) + 
       MH^2*(-96*MT^6 - 80*MT^4*T + 32*MT^2*T^2 + 6*T^3) + 
       4*(96*MT^8 + 152*MT^6*T + 36*MT^4*T^2 + 3*MT^2*T^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - T)^3*T)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(8*MT^6*(3*MH^2 - 12*MT^2 + U))/
      ((MH^2 - 4*MT^2 - U)^3*U), 0, (48*MT^6*(MH^2 - U)*(MH^2 - 4*MT^2 + U))/
      (U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-8*MT^6*(MH^2 - 4*MT^2 + 3*U))/
      ((MH^2 - 4*MT^2 - U)^3*U^2), 0, 
     (-16*MT^6*(MH^2 - U)*(MH^2 - 4*MT^2 + 5*U))/
      (U^2*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(-2*MT^4*(MH^8 + MH^6*(-16*MT^2 + 3*U) + 6*MH^4*(16*MT^4 - 6*MT^2*U - 
          5*U^2) + MH^2*(-256*MT^6 + 144*MT^4*U + 240*MT^2*U^2 + 34*U^3) + 
        4*(64*MT^8 - 48*MT^6*U - 120*MT^4*U^2 - 34*MT^2*U^3 - 3*U^4)))/
      ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3*U^2), 0, 
     (2*MT^4*(-MH^12 + MH^10*(20*MT^2 - 6*U) + 
        MH^8*(-160*MT^4 + 88*MT^2*U + 61*U^2) + 
        4*MH^6*(160*MT^6 - 112*MT^4*U - 157*MT^2*U^2 - 28*U^3) - 
        16*MT^2*U*(128*MT^8 + 416*MT^6*U + 96*MT^4*U^2 + 16*MT^2*U^3 + U^4) + 
        MH^4*(-1280*MT^8 + 768*MT^6*U + 1680*MT^4*U^2 + 800*MT^2*U^3 + 
          78*U^4) + 4*MH^2*(256*MT^10 + 128*MT^8*U + 272*MT^6*U^2 - 
          256*MT^4*U^3 - 62*MT^2*U^4 - 5*U^5)))/((MH^2 - 4*MT^2)^3*U^2*
       (-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((2*U*(-MH^2 + 4*MT^2 + U)^2*(MH^4 - MH^2*(18*MT^2 + U) + 
         8*MT^2*(7*MT^2 + 2*U)))/(MH^2 - 4*MT^2)^2 - 
      ((MH^2 - 4*MT^2 - U)*U*(MH^6*U + 48*MT^4*(4*MT^2 + U)^2 + 
         MH^4*(48*MT^4 - 28*MT^2*U - U^2) + 16*MH^2*MT^2*(-24*MT^4 + U^2))*
        DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + (MH^2 - 12*MT^2 - U)*
       (MH^2 - 4*MT^2 - U)*U*DiscB[U, MT, MT] + 2*MT^4*(MH^2 - 4*MT^2 + 3*U)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      4*MT^4*(MH^2 - 4*MT^2 + 3*U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
          (2*MT^2)]^2)/(2*(MH^2 - 4*MT^2 - U)^3*U^2), 
    ((8*I)*MT^4*Pi*(3*MH^4 + 48*MT^4 + 44*MT^2*U + 6*U^2 - 
       MH^2*(24*MT^2 + 11*U)))/((MH^2 - 4*MT^2)^3*U*(-MH^2 + 4*MT^2 + U)^2), 
    (MT^2*((-2*U*(-MH^2 + 4*MT^2 + U)^2*(3*MH^6 + 7*MH^4*(4*MT^2 - U) - 
          4*MH^2*(92*MT^4 + 31*MT^2*U - U^2) + 16*MT^2*(52*MT^4 + 38*MT^2*U + 
            5*U^2)))/(MH^2 - 4*MT^2)^3 - (2*(MH^2 - 4*MT^2 - U)*U*
         (MH^8*(24*MT^2 - 2*U) - 48*MT^4*(4*MT^2 + U)^3 + 
          MH^6*(-240*MT^4 - 128*MT^2*U + 3*U^2) + 
          MH^4*(576*MT^6 + 976*MT^4*U + 148*MT^2*U^2 - U^3) + 
          8*MH^2*(96*MT^8 - 144*MT^6*U - 62*MT^4*U^2 - 7*MT^2*U^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 - 2*(MH^2 - 4*MT^2 - U)*
        (MH^2 + 12*MT^2 - U)*U*DiscB[U, MT, MT] + 
       MT^2*(MH^4 - 16*MT^4 + 6*MH^2*U + 16*MT^2*U - 7*U^2)*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
       2*MT^2*(MH^4 - 16*MT^4 + 6*MH^2*U + 16*MT^2*U - 7*U^2)*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
     (U^2*(-MH^2 + 4*MT^2 + U)^4), 
    ((-16*I)*MT^4*Pi*(-3*MH^8 + 2*MH^6*(15*MT^2 + 7*U) - 
       MH^4*(72*MT^4 + 74*MT^2*U + 17*U^2) + 
       MH^2*(-96*MT^6 - 80*MT^4*U + 32*MT^2*U^2 + 6*U^3) + 
       4*(96*MT^8 + 152*MT^6*U + 36*MT^4*U^2 + 3*MT^2*U^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - U)^3*U)}, 0, 4, 1], 
 ((T*U*(-MH^2 + T + U)^2*(-6*MH^6 + U^2*(13*T + 14*U) + MH^4*(6*T + 31*U) - 
      MH^2*U*(25*T + 39*U)))/(MH^2 - U)^2 + 
   (MH^2*T*(MH^2 - T - U)*U*(3*MH^8 - 2*T^2*U^2 - 6*MH^6*(T + U) + 
      5*MH^2*T*U*(T + U) + MH^4*(3*T^2 + T*U + 3*U^2))*DiscB[MH^2, MT, MT])/
    (MH^2 - U)^3 + T*U*(-MH^2 + T + U)*(3*MH^4 + 3*T^2 + 11*T*U + 6*U^2 - 
     MH^2*(6*T + 11*U))*DiscB[T, MT, MT] + 
   (T*U*(-MH^2 + T + U)*(3*MH^10 - 6*MH^4*T*U^2 - 6*MH^8*(T + U) + 
      3*MH^6*(T + U)^2 - T*U^3*(3*T + U) + MH^2*T*U^2*(6*T + 7*U))*
     DiscB[U, MT, MT])/(MH^2 - U)^3 - 
   T*(MH^8*MT^2 - T*U*(T^3 + 6*T^2*U + 6*T*U^2 + 2*U^3) + 
     MT^2*(T^4 + T^3*U - 6*T^2*U^2 - 10*T*U^3 - 4*U^4) + 
     MH^6*(T*U - MT^2*(4*T + U)) + 3*MH^4*(-(T*U*(T + 2*U)) + 
       MT^2*(2*T^2 + T*U - 2*U^2)) + MH^2*(3*T*U*(T^2 + 4*T*U + 2*U^2) + 
       MT^2*(-4*T^3 - 3*T^2*U + 12*T*U^2 + 10*U^3)))*
    ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)^3*U*
    (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^4*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + ((MH^14*MT^2 + MH^12*(T*U - 4*MT^2*(T + U)) + 
      T^3*U^3*(-(T*U) + MT^2*(T + U)) + 3*MH^10*(-(T*U*(T + U)) + 
        MT^2*(2*T^2 + 5*T*U + 2*U^2)) + MH^2*T^2*U^2*
       (3*T*U*(T + U) - MT^2*(3*T^2 + 7*T*U + 3*U^2)) - 
      3*MH^4*T*U*(T*U*(T^2 + 3*T*U + U^2) + MT^2*(T^3 + T^2*U + T*U^2 + 
          U^3)) + MH^8*(3*T*U*(T^2 + T*U + U^2) - 
        MT^2*(4*T^3 + 21*T^2*U + 21*T*U^2 + 4*U^3)) + 
      MH^6*(-(T*U*(T^3 - 3*T^2*U - 3*T*U^2 + U^3)) + 
        MT^2*(T^4 + 13*T^3*U + 21*T^2*U^2 + 13*T*U^3 + U^4)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 - 
   (MH^2 - T)^3*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 SeriesData[\[Beta], 0, {1/(8*MT^2*T*(-MH^2 + 4*MT^2 + T)), 0, 
    (MH^2 - 8*MT^2 - T)/(8*MT^2*T*(-MH^2 + 4*MT^2 + T)^2)}, 0, 3, 1]*
  (ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*(MH^2 - T), 0, -4*MT^2*(MH^2 - T), 0, 
      -4*MT^2*(MH^2 - T)}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2*(MH^2 - T)*T, 0, 4*MT^2*(MH^2 - T)*T, 0, 
      4*MT^2*(MH^2 - T)*T}, 0, 5, 1] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {-MH^4 + 4*MT^2*T + MH^2*(4*MT^2 + T), 0, 
      4*MT^2*(MH^2 + T), 0, 4*MT^2*(MH^2 + T)}, 0, 5, 1] + 
   SeriesData[\[Beta], 0, 
    {((MH^2 - T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       2 - 4*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2, 0, 
     -4*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2, 0, 
     -4*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2}, 0, 5, 
    1]), ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-1/(2*(MH^2 - 4*MT^2 - U)), 0, 
     (-2*MT^2)/(-MH^2 + 4*MT^2 + U)^2}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {U/(2*MH^2 - 8*MT^2 - 2*U), 0, 
     (2*MT^2*U)/(-MH^2 + 4*MT^2 + U)^2}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MH^2 - 4*MT^2)/(8*MT^2*(-MH^2 + 4*MT^2 + U)), 0, 
     -(MH^4 + 16*MT^4 - MH^2*(8*MT^2 + U))/(8*MT^2*(-MH^2 + 4*MT^2 + U)^2)}, 
    0, 4, 1] + SeriesData[\[Beta], 0, 
   {((MH^2 - 8*MT^2 - U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2 + 8*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
          (2*MT^2)]^2)/(16*MT^2*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)), 0, 
    ((MH^4 + 32*MT^4 + 8*MT^2*U + U^2 - 2*MH^2*(4*MT^2 + U))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      32*MT^4*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (16*MT^2*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 (T*ScalarC0[0, 0, T, MT, MT, MT] + U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*T*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T*(-MH^2 + T + U)), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^2)/(-MH^2 + 4*MT^2 + T)^2, 0, 
     (2*MT^2*(MH^2 + 4*MT^2 - T))/(MH^2 - 4*MT^2 - T)^3}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MH^2 - 4*MT^2)/(2*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (-2*MT^2*(MH^2 - 4*MT^2 + T))/(MH^2 - 4*MT^2 - T)^3}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^4 + 32*MT^4 + 4*MT^2*T + T^2 - 
       2*MH^2*(6*MT^2 + T))/(4*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (-MH^6 + 128*MT^6 + 32*MT^4*T + 20*MT^2*T^2 + T^3 + 
       3*MH^4*(4*MT^2 + T) - MH^2*(64*MT^4 + 32*MT^2*T + 3*T^2))/
      (4*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((MH^2*(-MH^2 + 4*MT^2 + T)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) + 
      (MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT] - 
      MT^2*(Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
        2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/
     (4*MT^2*(-MH^2 + 4*MT^2 + T)^2), 
    (I*Pi)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)), 
    (-8/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)) + 
      ((MH^6 + 8*MH^2*MT^2*(6*MT^2 + T) - MH^4*(16*MT^2 + T))*
        DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^
         2) + ((-MH^2 + 8*MT^2 + T)*DiscB[T, MT, MT])/
       (MT^2*(-MH^2 + 4*MT^2 + T)^2) + 
      (8*MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + T)^3 - 
      (16*MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + T)^3)/4, ((-4*I)*MT^2*Pi*(-2*MH^2 + 8*MT^2 + T))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^2)/(-MH^2 + 4*MT^2 + U)^2, 0, 
     (2*MT^2*(MH^2 + 4*MT^2 - U))/(MH^2 - 4*MT^2 - U)^3}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MH^2 - 4*MT^2)/(2*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (-2*MT^2*(MH^2 - 4*MT^2 + U))/(MH^2 - 4*MT^2 - U)^3}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^4 + 32*MT^4 + 4*MT^2*U + U^2 - 
       2*MH^2*(6*MT^2 + U))/(4*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (-MH^6 + 128*MT^6 + 32*MT^4*U + 20*MT^2*U^2 + U^3 + 
       3*MH^4*(4*MT^2 + U) - MH^2*(64*MT^4 + 32*MT^2*U + 3*U^2))/
      (4*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((MH^2*(-MH^2 + 4*MT^2 + U)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) + 
      (MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT] - 
      MT^2*(Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
        2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
     (4*MT^2*(-MH^2 + 4*MT^2 + U)^2), 
    (I*Pi)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)), 
    (-8/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)) + 
      ((MH^6 + 8*MH^2*MT^2*(6*MT^2 + U) - MH^4*(16*MT^2 + U))*
        DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^
         2) + ((-MH^2 + 8*MT^2 + U)*DiscB[U, MT, MT])/
       (MT^2*(-MH^2 + 4*MT^2 + U)^2) + 
      (8*MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + U)^3 - 
      (16*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + U)^3)/4, ((-4*I)*MT^2*Pi*(-2*MH^2 + 8*MT^2 + U))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 (-2*MH^2*(MH^2 - T - U)*DiscB[MH^2, MT, MT] - 2*T*(-MH^2 + T + U)*
    DiscB[T, MT, MT] + 2*(MH^2 - T)*(MH^2 - T - U)*DiscB[U, MT, MT] + 
   (MH^2 - T)*T^2*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - T)*T*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - T)*T*(MH^2 - U)*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   (MH^2 - T)*(2*MH^4*MT^2 - T^2*U - 2*MH^2*MT^2*(3*T + 2*U) + 
     2*MT^2*(2*T^2 + 3*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
     MT])/(2*(MH^2 - T)*T*(-MH^2 + T + U)^2), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MH^4 + 32*MT^4 - 4*MT^2*T + T^2 - 2*MH^2*(6*MT^2 + T))/
      (8*(MH^2 - 4*MT^2 - T)^3), 0, (MH^6 - 256*MT^6 + 16*MT^4*T - 
       32*MT^2*T^2 - T^3 - MH^4*(16*MT^2 + 3*T) + 
       MH^2*(112*MT^4 + 48*MT^2*T + 3*T^2))/(8*(-MH^2 + 4*MT^2 + T)^4)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-((MH^2 - 4*MT^2)*(MH^4 + 32*MT^4 - 4*MT^2*T + T^2 - 
         2*MH^2*(6*MT^2 + T)))/(32*MT^2*(-MH^2 + 4*MT^2 + T)^3), 0, 
     -(MH^8 - 3*MH^6*(6*MT^2 + T) + MH^4*(128*MT^4 + 38*MT^2*T + 3*T^2) - 
        MH^2*(416*MT^6 + 96*MT^4*T + 22*MT^2*T^2 + T^3) + 
        2*MT^2*(256*MT^6 - 16*MT^4*T + 32*MT^2*T^2 + T^3))/
      (16*MT^2*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(T*(MH^4 + 64*MT^4 + 4*MT^2*T + T^2 - 2*MH^2*(10*MT^2 + T)))/
      (8*(MH^2 - 4*MT^2 - T)^3), 0, 
     (T*(-MH^6 + 512*MT^6 + 48*MT^4*T + 32*MT^2*T^2 + T^3 + 
        MH^4*(16*MT^2 + 3*T) - MH^2*(176*MT^4 + 48*MT^2*T + 3*T^2)))/
      (8*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((-8*(-MH^2 + 4*MT^2 + T)^2)/MT^2 - (8*MH^2*(MH^2 - 4*MT^2 - T)*
        (-16*MT^4 - T^2 + MH^2*(4*MT^2 + T))*DiscB[MH^2, MT, MT])/
       (MT^2*(MH^2 - 4*MT^2)*(MH^2 - T)) - 
      (8*(MH^2 + 4*MT^2 - T)*T*(-MH^2 + 4*MT^2 + T)*DiscB[T, MT, MT])/
       (MT^2*(MH^2 - T)) + ((-MH^6 + 128*MT^6 + 64*MT^4*T + 28*MT^2*T^2 + 
         T^3 + 3*MH^4*(4*MT^2 + T) - MH^2*(64*MT^4 + 40*MT^2*T + 3*T^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (MT^2*(MH^2 - T)) + (32*(-4*MT^4 - MT^2*T - T^2 + MH^2*(MT^2 + T))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(MH^2 - T))/
     (64*(MH^2 - 4*MT^2 - T)^3), ((I/2)*Pi*(MH^2 - 4*MT^2 + T))/
     ((MH^2 - 4*MT^2)*(-MH^2 + 4*MT^2 + T)^2), 
    ((4*(-MH^2 + 4*MT^2 + T)^2*(MH^4 + 64*MT^4 - 4*MT^2*T - 
         MH^2*(20*MT^2 + T)))/(MH^2 - 4*MT^2) + 
      (4*MH^2*(MH^2 - 4*MT^2 - T)*(MH^6*T - 2*MH^4*(16*MT^4 + 10*MT^2*T + 
           T^2) - 8*MT^2*(64*MT^6 + 8*MT^2*T^2 + T^3) + 
         MH^2*(256*MT^6 + 64*MT^4*T + 28*MT^2*T^2 + T^3))*
        DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - T)) - 
      (4*(MH^2 - 4*MT^2 - T)*T*(MH^4 - 32*MT^4 + 12*MT^2*T + T^2 - 
         2*MH^2*(6*MT^2 + T))*DiscB[T, MT, MT])/(MH^2 - T) + 
      ((MH^8 + 512*MT^8 + 320*MT^6*T + 192*MT^4*T^2 + 16*MT^2*T^3 + T^4 - 
         4*MH^6*(4*MT^2 + T) + 6*MH^4*(4*MT^2 + T)^2 - 
         4*MH^2*(80*MT^6 + 72*MT^4*T + 12*MT^2*T^2 + T^3))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (MH^2 - T) - (64*MT^4*(8*MT^4 + 2*MT^2*T + 3*T^2 - 
         MH^2*(2*MT^2 + 3*T))*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
           (2*MT^2)]^2)/(MH^2 - T))/(32*MT^2*(-MH^2 + 4*MT^2 + T)^4), 
    ((2*I)*MT^2*Pi*(2*MH^4 + 32*MT^4 - 12*MT^2*T - T^2 + 
       MH^2*(-16*MT^2 + 3*T)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3)}, 0, 
   4, 1], ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - U)*(MH^4 + 32*MT^4 - 4*MT^2*U + U^2 - 
        2*MH^2*(6*MT^2 + U)))/(8*(MH^2 - 4*MT^2 - U)^3*U), 0, 
     -((MH^2 - U)*(MH^6 - 256*MT^6 + 16*MT^4*U - 32*MT^2*U^2 - U^3 - 
         MH^4*(16*MT^2 + 3*U) + MH^2*(112*MT^4 + 48*MT^2*U + 3*U^2)))/
      (8*U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-((MH^2 - U)*(MH^4 + 64*MT^4 + 4*MT^2*U + U^2 - 2*MH^2*(10*MT^2 + U)))/
      (8*(MH^2 - 4*MT^2 - U)^3), 0, 
     ((MH^2 - U)*(MH^6 - 512*MT^6 - 48*MT^4*U - 32*MT^2*U^2 - U^3 - 
        MH^4*(16*MT^2 + 3*U) + MH^2*(176*MT^4 + 48*MT^2*U + 3*U^2)))/
      (8*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MH^10 - MH^8*(20*MT^2 + 3*U) + 16*MT^4*U*(32*MT^4 - 4*MT^2*U + U^2) + 
        MH^6*(144*MT^4 + 64*MT^2*U + 3*U^2) + 8*MH^2*MT^2*
         (64*MT^6 + 48*MT^4*U + 22*MT^2*U^2 + U^3) - 
        MH^4*(448*MT^6 + 336*MT^4*U + 52*MT^2*U^2 + U^3))/
      (32*MT^2*(MH^2 - 4*MT^2)*U*(-MH^2 + 4*MT^2 + U)^3), 0, 
     -(MH^14 - 4*MH^8*(3*MT^2 + U)*(12*MT^2 + U)^2 - MH^12*(26*MT^2 + 4*U) + 
        2*MH^10*(144*MT^4 + 44*MT^2*U + 3*U^2) + 
        32*MT^6*U*(256*MT^6 - 16*MT^4*U + 32*MT^2*U^2 + U^3) + 
        32*MH^2*MT^4*(256*MT^8 + 192*MT^6*U + 120*MT^4*U^2 + 4*MT^2*U^3 + 
          U^4) + MH^6*(5888*MT^8 + 4352*MT^6*U + 800*MT^4*U^2 + 56*MT^2*U^3 + 
          U^4) - 2*MH^4*MT^2*(5376*MT^8 + 5120*MT^6*U + 1392*MT^4*U^2 + 
          128*MT^2*U^3 + 5*U^4))/(16*(MH^2*MT - 4*MT^3)^2*U*
       (-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((32*(-MH^2 + 4*MT^2 + U)^2)/(MH^2 - 4*MT^2) + 
      (8*MH^2*(MH^4 - 32*MT^4 + MH^2*(4*MT^2 - U))*(MH^2 - 4*MT^2 - U)*
        DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^2) + 
      (8*(MH^2 + 4*MT^2 - U)*(-MH^2 + 4*MT^2 + U)*DiscB[U, MT, MT])/MT^2 - 
      ((MH^2 - U)*(MH^4 + 32*MT^4 - 4*MT^2*U + U^2 - 2*MH^2*(6*MT^2 + U))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/(MT^2*U) + 
      (32*(4*MT^4 + MT^2*U + U^2 - MH^2*(MT^2 + U))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U)/
     (64*(MH^2 - 4*MT^2 - U)^3), 
    ((-I)*Pi*(MH^4 - 8*MT^4 + 2*MT^2*U - MH^2*(2*MT^2 + U)))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2), 
    ((64*(-MH^2 + 4*MT^2 + U)^2*(MH^4 - 16*MT^4 - MH^2*U + MT^2*U))/
       (MH^2 - 4*MT^2)^2 - (4*MH^2*(MH^2 - 4*MT^2 - U)*
        (MH^8 - 2*MH^6*(12*MT^2 + U) - 128*MT^6*(12*MT^2 + U) + 
         MH^4*(48*MT^4 + 36*MT^2*U + U^2) + 4*MH^2*(128*MT^6 - 20*MT^4*U - 
           3*MT^2*U^2))*DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^3) + 
      (4*(MH^2 - 4*MT^2 - U)*(MH^4 - 32*MT^4 + 12*MT^2*U + U^2 - 
         2*MH^2*(6*MT^2 + U))*DiscB[U, MT, MT])/MT^2 + 
      ((MH^2 - U)*(MH^6 - 192*MT^6 - 16*MT^2*U^2 - U^3 - 
         MH^4*(16*MT^2 + 3*U) + MH^2*(96*MT^4 + 32*MT^2*U + 3*U^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/(MT^2*U) + 
      (64*MT^2*(8*MT^4 + 2*MT^2*U + 3*U^2 - MH^2*(2*MT^2 + 3*U))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U)/
     (32*(-MH^2 + 4*MT^2 + U)^4), 
    ((-2*I)*MT^2*Pi*(7*MH^6 + 128*MT^6 - 48*MT^4*U - 4*MT^2*U^2 - 
       2*MH^4*(24*MT^2 + 5*U) + MH^2*(48*MT^4 + 52*MT^2*U + 3*U^2)))/
     ((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1], 
 ((-MH^2 + T + U)^2/(MH^2 - T) + (MH^2*(MH^4 - 2*T^2 + MH^2*(T - U))*
     (MH^2 - T - U)*DiscB[MH^2, MT, MT])/((MH^2 - T)^2*T) - 
   ((MH^2 - T - U)*(2*MH^4 + T*(-T + U) - MH^2*(T + 2*U))*DiscB[T, MT, MT])/
    (MH^2 - T)^2 + ((MH^2 + T - U)*(-MH^2 + T + U)*DiscB[U, MT, MT])/T + 
   ((MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, 0, T, MT, MT, MT])/(T*U) + 
   ((-(MH^6*MT^2) + T^2*U^2 + 3*MH^4*MT^2*(T + U) + 
      MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) - 
      MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*ScalarC0[0, 0, U, MT, MT, 
      MT])/T^2 + ((MH^10*MT^2 - MH^8*MT^2*(5*T + 3*U) + 
      T^2*U*(-(T^2*U) + MT^2*(2*T^2 + 3*T*U + U^2)) + 
      MH^6*(T^2*U + 3*MT^2*(3*T^2 + 4*T*U + U^2)) - 
      MH^4*(T^2*U*(2*T + U) + MT^2*(7*T^3 + 13*T^2*U + 9*T*U^2 + U^3)) + 
      MH^2*(T^3*U*(T + 2*U) + MT^2*T*(2*T^3 + 2*T^2*U + 3*T*U^2 + 2*U^3)))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/((MH^2 - T)*T^2*U) - 
   ((MH^2 - U)^2*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, MH^2, U, MT, MT, MT])/(T^2*U) - 
   ((MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(5*T + 2*U) + 
      MT^2*(4*T^2 + 5*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
      MT])/T)/(2*(MH^2 - T - U)^3), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^2*(MH^2 - T))/(T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (-2*MT^2*(MH^2 - T)*(MH^2 + 4*MT^2 - T))/(T*(-MH^2 + 4*MT^2 + T)^3)}, 0, 
    4, 1] + ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
    MT, MT]*SeriesData[\[Beta], 0, 
    {-((MT^2*(MH^2 - T)*(MH^2 - 4*MT^2 + T))/(T*(-MH^2 + 4*MT^2 + T)^2)), 0, 
     (2*MT^2*(MH^4*(2*MT^2 + T) - 2*MH^2*(4*MT^4 + T^2) + 
        T*(8*MT^4 - 2*MT^2*T + T^2)))/(T*(-MH^2 + 4*MT^2 + T)^3)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^6 + 16*MT^4*T - MH^4*(8*MT^2 + 3*T) + 
       2*MH^2*(8*MT^4 + 4*MT^2*T + T^2))/(2*(MH^2 - 4*MT^2)*T*
       (-MH^2 + 4*MT^2 + T)^2), 0, 
     (-2*MT^2*(-MH^8 + 16*MT^4*(4*MT^2 - T)*T + 6*MH^6*(2*MT^2 + T) - 
        MH^4*(48*MT^4 + 44*MT^2*T + 7*T^2) + 2*MH^2*(32*MT^6 + 32*MT^4*T + 
          16*MT^2*T^2 + T^3)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3*T)}, 
    0, 4, 1] + SeriesData[\[Beta], 0, 
   {(8/(MH^2 - 4*MT^2) + (4*MH^2*(MH^2 - 4*MT^2 - 2*T)*DiscB[MH^2, MT, MT])/
       ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)) + (4*DiscB[T, MT, MT])/
       (MH^2 - 4*MT^2 - T) - 
      ((MH^2 - T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + T)^2 + 
      (2*(MH^2 - T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
       (-MH^2 + 4*MT^2 + T)^2)/(4*T), 
    ((-I)*Pi*(MH^4 - 4*MT^2*T - MH^2*(4*MT^2 + T)))/
     ((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)*T), 
    (2*((MH^4 - MH^2*T - 8*MT^2*(2*MT^2 + T))/((MH^2 - 4*MT^2)^2*
         (MH^2 - 4*MT^2 - T)) + (2*MH^2*MT^2*(2*MH^4 - MH^2*(16*MT^2 + 7*T) + 
          4*(8*MT^4 + 7*MT^2*T + T^2))*DiscB[MH^2, MT, MT])/
        ((MH^2 - 4*MT^2)^3*(-MH^2 + 4*MT^2 + T)^2) + 
       (2*MT^2*DiscB[T, MT, MT])/(-MH^2 + 4*MT^2 + T)^2 + 
       (MT^2*(MH^2 - T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/(-MH^2 + 4*MT^2 + T)^3 - 
       (2*MT^2*(MH^2 - T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
          2)/(-MH^2 + 4*MT^2 + T)^3))/T, 
    ((-4*I)*MT^2*Pi*(2*MH^6 + 4*MT^2*T*(8*MT^2 + T) - MH^4*(16*MT^2 + 5*T) + 
       MH^2*(32*MT^4 + 12*MT^2*T + 3*T^2)))/((MH^2 - 4*MT^2)^3*T*
      (-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-2*MT^2)/(-MH^2 + 4*MT^2 + U)^2, 0, 
     (2*MT^2*(MH^2 + 4*MT^2 - U))/(-MH^2 + 4*MT^2 + U)^3, 0, 
     (-2*MT^2*(MH^2 - U)*(MH^2 + 8*MT^2 - U))/(-MH^2 + 4*MT^2 + U)^4}, 0, 5, 
    1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^2 - 4*MT^2)/(2*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (2*MT^2*(MH^2 - 4*MT^2 + U))/(MH^2 - 4*MT^2 - U)^3, 0, 
     (2*MT^2*(MH^4 - 4*MH^2*MT^2 + 8*MT^2*U - U^2))/(-MH^2 + 4*MT^2 + U)^4}, 
    0, 5, 1] + ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
    MT, MT]*SeriesData[\[Beta], 0, {(MT^2*(MH^2 - 4*MT^2 + U))/
      (-MH^2 + 4*MT^2 + U)^2, 0, (-2*MT^2*(8*MT^4 - 2*MT^2*U + U^2 - 
        MH^2*(2*MT^2 + U)))/(MH^2 - 4*MT^2 - U)^3, 0, 
     (-2*MT^2*(-(MH^4*(2*MT^2 + U)) - U*(8*MT^4 - 6*MT^2*U + U^2) + 
        2*MH^2*(4*MT^4 - 2*MT^2*U + U^2)))/(-MH^2 + 4*MT^2 + U)^4}, 0, 5, 
    1] + SeriesData[\[Beta], 0, 
   {((4*(2*MH^2 - 4*MT^2 - U)*DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)*
        (MH^2 - 4*MT^2 - U)) + (-4*(MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT] + 
        (MH^2 - U)*(Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
           2 - 2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
       (-MH^2 + 4*MT^2 + U)^2)/(4*(MH^2 - U)), 
    ((-I)*Pi)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)), 
    2/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)) + 
     (4*MT^2*(3*MH^4 + (4*MT^2 + U)^2 - MH^2*(16*MT^2 + 3*U))*
       DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - U)*
       (-MH^2 + 4*MT^2 + U)^2) - (4*MT^2*DiscB[U, MT, MT])/
      ((MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2) - 
     (2*MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + U)^3 + 
     (4*MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + U)^3, ((4*I)*MT^2*Pi*(-2*MH^2 + 8*MT^2 + U))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2), 
    (2*(MH^4 + MH^2*(16*MT^2 - U) - 8*MT^2*(10*MT^2 + U)))/
      (3*(MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2) + 
     (4*MH^2*MT^2*(3*MH^4 + 48*MT^4 + 12*MT^2*U + U^2 - 3*MH^2*(8*MT^2 + U))*
       DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - U)^3) + 
     (4*MT^2*DiscB[U, MT, MT])/(-MH^2 + 4*MT^2 + U)^3 + 
     (2*MT^2*(MH^2 + 2*MT^2 - U)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + U)^4 - (4*MT^2*(MH^2 + 2*MT^2 - U)*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
      (-MH^2 + 4*MT^2 + U)^4}, 0, 5, 1], 
 (4*(MH^2 - U)*(-MH^2 + T + U)^2 + 2*MH^2*(MH^2 - 2*T - U)*(MH^2 - T - U)*
    DiscB[MH^2, MT, MT] + 2*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] - 
   2*(MH^2 - T - U)*(MH^4 - T*U - MH^2*(T + U))*DiscB[U, MT, MT] + 
   (MH^2 - T)*T*(MH^2 - U)^2*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - T)*(MH^2 - U)^2*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*(MH^2 - U)^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   (MH^2 - U)*(MH^6 + T*U^2 - MH^4*(3*T + 2*U) + MH^2*(2*T^2 + 2*T*U + U^2))*
    ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - T)*(MH^2 - U)^2*
    (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
     MT, MT, MT])/(2*T*(MH^2 - U)^2*(-MH^2 + T + U)^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(-MH^4 - 4*MH^2*MT^2 + 32*MT^4 - 4*MT^2*T + 
        T^2))/(2*(MH^2 - 4*MT^2 - T)^3), 0, 
     (MT^2*(64*MT^6 - 32*MT^4*T + 8*MT^2*T^2 - T^3 - MH^4*(8*MT^2 + T) + 
        2*MH^2*(8*MT^4 + T^2)))/(-MH^2 + 4*MT^2 + T)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(8*MT^4 - 2*MT^2*T + T^2 - MH^2*(2*MT^2 + T)))/
      (T*(-MH^2 + 4*MT^2 + T)^3), 0, 
     (MT^2*(-32*MT^6 + 16*MT^4*T + 2*MH^2*(6*MT^2 - T)*T - 14*MT^2*T^2 + 
        T^3 + MH^4*(2*MT^2 + T)))/(T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MH^6*(2*MT^2 - T) + MH^4*(-24*MT^4 - 6*MT^2*T + T^2) + 
       4*MH^2*MT^2*(24*MT^4 + 24*MT^2*T + T^2) - 
       4*MT^2*(32*MT^6 + 56*MT^4*T + 8*MT^2*T^2 + T^3))/
      (4*(MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)^3*T), 0, 
     (MT^2*(MH^8*(2*MT^2 - 3*T) + 2*MH^6*(-16*MT^4 + 8*MT^2*T + T^2) + 
        MH^4*(192*MT^6 + 96*MT^4*T - 6*MT^2*T^2 + T^3) - 
        8*MH^2*(64*MT^8 + 96*MT^6*T + 6*MT^4*T^2 + 3*MT^2*T^3) + 
        4*MT^2*(128*MT^8 + 320*MT^6*T + 40*MT^4*T^2 + 20*MT^2*T^3 + T^4)))/
      ((MH^2 - 4*MT^2)^2*T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, {((-4*(-MH^2 + 4*MT^2 + T)^2)/(MH^2 - 4*MT^2) - 
      (4*MH^2*(3*MH^2 - 12*MT^2 - T)*(MH^2 - 4*MT^2 - T)*DiscB[MH^2, MT, MT])/
       (MH^2 - 4*MT^2)^2 + 8*(MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT] + 
      ((8*MT^4 - 2*MT^2*T + T^2 - MH^2*(2*MT^2 + T))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/T + 
      (2*(-8*MT^4 + 2*MT^2*T - T^2 + MH^2*(2*MT^2 + T))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T)/
     (8*(MH^2 - 4*MT^2 - T)^3), 
    ((I/2)*Pi*(MH^4 - 32*MT^4 + MH^2*(4*MT^2 - T)))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2), 
    (-MH^4 + MH^2*(-8*MT^2 + T) + 2*MT^2*(24*MT^2 + T))/
      ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2) - 
     (2*MH^2*MT^2*(9*MH^4 - MH^2*(72*MT^2 + 7*T) + 
        2*(72*MT^4 + 14*MT^2*T + T^2))*DiscB[MH^2, MT, MT])/
      ((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - T)^3) - (8*MT^2*DiscB[T, MT, MT])/
      (-MH^2 + 4*MT^2 + T)^3 + (2*MT^2*(4*MT^4 - MT^2*T + T^2 - 
        MH^2*(MT^2 + T))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2)/(T*(-MH^2 + 4*MT^2 + T)^4) - 
     (4*MT^2*(4*MT^4 - MT^2*T + T^2 - MH^2*(MT^2 + T))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
      (T*(-MH^2 + 4*MT^2 + T)^4), 
    ((2*I)*MT^2*Pi*(5*MH^6 + 256*MT^6 - MH^4*(24*MT^2 + 7*T) + 
       MH^2*(-48*MT^4 + 28*MT^2*T + 2*T^2)))/((MH^2 - 4*MT^2)^3*
      (MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(-MH^4 - 4*MH^2*MT^2 + 32*MT^4 - 4*MT^2*U + 
        U^2))/(2*(MH^2 - 4*MT^2 - U)^3), 0, 
     (MT^2*(64*MT^6 - 32*MT^4*U + 8*MT^2*U^2 - U^3 - MH^4*(8*MT^2 + U) + 
        2*MH^2*(8*MT^4 + U^2)))/(-MH^2 + 4*MT^2 + U)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(8*MT^4 - 2*MT^2*U + U^2 - MH^2*(2*MT^2 + U)))/
      (U*(-MH^2 + 4*MT^2 + U)^3), 0, 
     (MT^2*(-32*MT^6 + 16*MT^4*U + 2*MH^2*(6*MT^2 - U)*U - 14*MT^2*U^2 + 
        U^3 + MH^4*(2*MT^2 + U)))/(U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MH^6*(2*MT^2 - U) + MH^4*(-24*MT^4 - 6*MT^2*U + U^2) + 
       4*MH^2*MT^2*(24*MT^4 + 24*MT^2*U + U^2) - 
       4*MT^2*(32*MT^6 + 56*MT^4*U + 8*MT^2*U^2 + U^3))/
      (4*(MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)^3*U), 0, 
     (MT^2*(MH^8*(2*MT^2 - 3*U) + 2*MH^6*(-16*MT^4 + 8*MT^2*U + U^2) + 
        MH^4*(192*MT^6 + 96*MT^4*U - 6*MT^2*U^2 + U^3) - 
        8*MH^2*(64*MT^8 + 96*MT^6*U + 6*MT^4*U^2 + 3*MT^2*U^3) + 
        4*MT^2*(128*MT^8 + 320*MT^6*U + 40*MT^4*U^2 + 20*MT^2*U^3 + U^4)))/
      ((MH^2 - 4*MT^2)^2*U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, {((-4*(-MH^2 + 4*MT^2 + U)^2)/(MH^2 - 4*MT^2) - 
      (4*MH^2*(3*MH^2 - 12*MT^2 - U)*(MH^2 - 4*MT^2 - U)*DiscB[MH^2, MT, MT])/
       (MH^2 - 4*MT^2)^2 + 8*(MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT] + 
      ((8*MT^4 - 2*MT^2*U + U^2 - MH^2*(2*MT^2 + U))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/U + 
      (2*(-8*MT^4 + 2*MT^2*U - U^2 + MH^2*(2*MT^2 + U))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U)/
     (8*(MH^2 - 4*MT^2 - U)^3), 
    ((I/2)*Pi*(MH^4 - 32*MT^4 + MH^2*(4*MT^2 - U)))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2), 
    (-MH^4 + MH^2*(-8*MT^2 + U) + 2*MT^2*(24*MT^2 + U))/
      ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2) - 
     (2*MH^2*MT^2*(9*MH^4 - MH^2*(72*MT^2 + 7*U) + 
        2*(72*MT^4 + 14*MT^2*U + U^2))*DiscB[MH^2, MT, MT])/
      ((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - U)^3) - (8*MT^2*DiscB[U, MT, MT])/
      (-MH^2 + 4*MT^2 + U)^3 + (2*MT^2*(4*MT^4 - MT^2*U + U^2 - 
        MH^2*(MT^2 + U))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2)/(U*(-MH^2 + 4*MT^2 + U)^4) - 
     (4*MT^2*(4*MT^4 - MT^2*U + U^2 - MH^2*(MT^2 + U))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
      (U*(-MH^2 + 4*MT^2 + U)^4), 
    ((2*I)*MT^2*Pi*(5*MH^6 + 256*MT^6 - MH^4*(24*MT^2 + 7*U) + 
       MH^2*(-48*MT^4 + 28*MT^2*U + 2*U^2)))/((MH^2 - 4*MT^2)^3*
      (MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1], 
 ((2*(-MH^2 + T + U)^2)/T + (2*MH^2*(MH^2 - T - U)*(MH^2 + T - U)*
     DiscB[MH^2, MT, MT])/(T*(MH^2 - U)) - 4*(MH^2 - T - U)*
    DiscB[T, MT, MT] - (2*(-MH^2 + T + U)*(-MH^4 - 2*T*U + MH^2*(T + U))*
     DiscB[U, MT, MT])/(T*(MH^2 - U)) + 
   ((-2*MH^6*MT^2 + T^2*(T - U)*U + 2*MH^4*MT^2*(3*T + 4*U) + 
      2*MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3) - 
      MH^2*(T^2*U + 2*MT^2*(3*T^2 + 9*T*U + 5*U^2)))*
     ScalarC0[0, 0, T, MT, MT, MT])/(T*U) + 
   ((2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
      T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
      MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
     ScalarC0[0, 0, U, MT, MT, MT])/T^2 - 
   ((MH^2 - T)*(2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
      T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
      MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/(T^2*U) + 
   ((2*MH^8*MT^2 - 6*MH^6*MT^2*(T + U) + T^2*U*((T - U)*U - 2*MT^2*(T + U)) + 
      MH^4*(T^2*U + 6*MT^2*(T + U)^2) - 
      MH^2*(T^3*U + 2*MT^2*(T^3 + 2*T^2*U + 3*T*U^2 + U^3)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(T^2*U) + 
   ((-2*MH^6*MT^2 + 4*MH^4*MT^2*(2*T + U) + 
      T*(T*U*(-T + U) + 2*MT^2*(2*T^2 + T*U - U^2)) + 
      MH^2*(T^2*U - 2*MT^2*(5*T^2 + 3*T*U + U^2)))*ScalarD0[0, 0, 0, MH^2, T, 
      U, MT, MT, MT, MT])/T)/(4*(MH^2 - T - U)^3), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - T)*(3*MH^2 - 12*MT^2 + T))/
      (T*(-MH^2 + 4*MT^2 + T)^3), 0, 
     (2*MT^4*(-3*MH^6 + MH^4*T + T*(-48*MT^4 + 16*MT^2*T - 5*T^2) + 
        MH^2*(48*MT^4 - 16*MT^2*T + 7*T^2)))/(T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 
    4, 1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-2*MT^4*(MH^2 - T)*(MH^2 - 4*MT^2 + 3*T))/
      (T^2*(-MH^2 + 4*MT^2 + T)^3), 0, 
     (2*MT^4*(MH^6 + 5*MH^4*T + MH^2*(-16*MT^4 + 16*MT^2*T - 13*T^2) + 
        T*(16*MT^4 - 16*MT^2*T + 7*T^2)))/(T^2*(-MH^2 + 4*MT^2 + T)^4)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(MH^10 + 2*MH^8*(-8*MT^2 + T) + MH^6*(96*MT^4 - 20*MT^2*T - 
          21*T^2) - 8*MT^2*T*(32*MT^6 + 72*MT^4*T + 12*MT^2*T^2 + T^3) + 
        4*MH^4*(-64*MT^6 + 12*MT^4*T + 33*MT^2*T^2 + 7*T^3) + 
        2*MH^2*(128*MT^8 + 32*MT^6*T - 24*MT^4*T^2 - 44*MT^2*T^3 - 5*T^4)))/
      (2*(MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3*T^2), 0, 
     (-2*MT^2*(-(MH^12*(MT^2 + T)) + MH^10*(20*MT^4 + 13*MT^2*T + 6*T^2) - 
        MH^8*(160*MT^6 + 52*MT^4*T + 21*MT^2*T^2 + 11*T^3) - 
        8*MT^4*T*(128*MT^8 + 384*MT^6*T + 40*MT^4*T^2 + 16*MT^2*T^3 + T^4) + 
        MH^6*(640*MT^8 + 32*MT^6*T - 276*MT^4*T^2 + MT^2*T^3 + 8*T^4) + 
        2*MH^2*MT^2*(512*MT^10 + 128*MT^8*T - 480*MT^6*T^2 - 616*MT^4*T^3 - 
          92*MT^2*T^4 - 7*T^5) - 2*MH^4*(640*MT^10 - 64*MT^8*T - 
          744*MT^6*T^2 - 250*MT^4*T^3 - 11*MT^2*T^4 + T^5)))/
      ((MH^2 - 4*MT^2)^3*T^2*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {((-2*T*(-MH^2 + 4*MT^2 + T)^2*(-3*MH^4 + 4*MH^2*MT^2 + 32*MT^4 + 
         3*MH^2*T + 12*MT^2*T))/(MH^2 - 4*MT^2)^2 + 
      (2*MH^2*(MH^2 - 4*MT^2 - T)*T*(MH^4*(12*MT^2 - T) + 
         MH^2*(-96*MT^4 - 36*MT^2*T + T^2) + 4*(48*MT^6 + 40*MT^4*T + 
           5*MT^2*T^2))*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + 
      2*(MH^2 - 4*MT^2 - T)*(MH^2 + 4*MT^2 - T)*T*DiscB[T, MT, MT] - 
      MT^2*(MH^2 - T)*(MH^2 - 4*MT^2 + 3*T)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      2*MT^2*(MH^2 - T)*(MH^2 - 4*MT^2 + 3*T)*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (4*(MH^2 - 4*MT^2 - T)^3*T^2), 
    ((-2*I)*MT^2*Pi*(3*MH^6 - 8*MH^4*(3*MT^2 + T) + 4*MT^2*T*(12*MT^2 + T) + 
       MH^2*(48*MT^4 + 20*MT^2*T + 5*T^2)))/((MH^2 - 4*MT^2)^3*T*
      (-MH^2 + 4*MT^2 + T)^2), 
    (MT^2*((2*T*(-MH^2 + 4*MT^2 + T)^2*(14*MH^6 - MH^4*(104*MT^2 + 33*T) + 
          MH^2*(160*MT^4 + 84*MT^2*T + 19*T^2) + 4*(32*MT^6 + 48*MT^4*T + 
            5*MT^2*T^2)))/(MH^2 - 4*MT^2)^3 + 
       (2*MH^2*(MH^2 - 4*MT^2 - T)*T*(3*MH^8 - MH^6*(12*MT^2 + 17*T) + 
          MH^4*(-144*MT^4 + 16*MT^2*T + 22*T^2) + 
          8*MH^2*(120*MT^6 + 86*MT^4*T + 4*MT^2*T^2 - T^3) - 
          8*(192*MT^8 + 240*MT^6*T + 60*MT^4*T^2 + 5*MT^2*T^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 + 2*(3*MH^2 + 4*MT^2 - 3*T)*
        (MH^2 - 4*MT^2 - T)*T*DiscB[T, MT, MT] - 
       (MH^4*(2*MT^2 + T) + T*(8*MT^4 - 6*MT^2*T + T^2) - 
         2*MH^2*(4*MT^4 - 2*MT^2*T + T^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
       2*(MH^4*(2*MT^2 + T) + T*(8*MT^4 - 6*MT^2*T + T^2) - 
         2*MH^2*(4*MT^4 - 2*MT^2*T + T^2))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/
     (T^2*(-MH^2 + 4*MT^2 + T)^4), 
    ((2*I)*MT^2*Pi*(-3*MH^10 + MH^8*(12*MT^2 + 11*T) + 
       MH^6*(144*MT^4 + 32*MT^2*T - 13*T^2) + 
       16*MT^4*T*(96*MT^4 + 12*MT^2*T + T^2) - 
       MH^4*(960*MT^6 + 688*MT^4*T + 92*MT^2*T^2 - 5*T^3) + 
       48*MH^2*MT^2*(32*MT^6 + 24*MT^4*T + 11*MT^2*T^2 + T^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - T)^3*T)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(3*MH^2 - 12*MT^2 + U))/
      (MH^2 - 4*MT^2 - U)^3, 0, (2*MT^4*(3*MH^4 - 48*MT^4 + 2*MH^2*U + 
        16*MT^2*U - 5*U^2))/(-MH^2 + 4*MT^2 + U)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - 4*MT^2 + 3*U))/
      (U*(-MH^2 + 4*MT^2 + U)^3), 0, 
     (-2*MT^4*(MH^4 - 16*MT^4 + 6*MH^2*U + 16*MT^2*U - 7*U^2))/
      (U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(MT^2*(-MH^6 + 3*MH^4*(4*MT^2 + 3*U) - 6*MH^2*(8*MT^4 + 12*MT^2*U + 
          U^2) + 2*(32*MT^6 + 72*MT^4*U + 12*MT^2*U^2 + U^3)))/
      (2*(MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)^3*U), 0, 
     (-2*MT^2*(MH^8*(MT^2 - U) + MH^6*(-16*MT^4 + U^2) + 
        MH^4*(96*MT^6 + 96*MT^4*U - 3*MT^2*U^2) - 8*MH^2*MT^2*
         (32*MT^6 + 64*MT^4*U + 3*MT^2*U^2 + U^3) + 
        2*MT^2*(128*MT^8 + 384*MT^6*U + 40*MT^4*U^2 + 16*MT^2*U^3 + U^4)))/
      ((MH^2 - 4*MT^2)^2*U*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, {((2*(-MH^2 + 4*MT^2 + U)^2)/(MH^2 - 4*MT^2) + 
      (2*(MH^2 - 4*MT^2 - U)*(MH^6 + MH^4*(8*MT^2 - U) + 
         4*MT^2*(4*MT^2 + U)^2 - 8*MH^2*MT^2*(8*MT^2 + U))*
        DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - U)) - 
      (2*(MH^2 - 4*MT^2 - U)*(MH^2 + 4*MT^2 - U)*DiscB[U, MT, MT])/
       (MH^2 - U) - (MT^2*(-MH^2 + 4*MT^2 - 3*U)*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/U + 
      (2*MT^2*(-MH^2 + 4*MT^2 - 3*U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
           (2*MT^2)]^2)/U)/(4*(MH^2 - 4*MT^2 - U)^3), 
    ((2*I)*MT^2*Pi*(-3*MH^2 + 12*MT^2 + U))/((MH^2 - 4*MT^2)^2*
      (-MH^2 + 4*MT^2 + U)^2), 
    (MT^2*((2*(8*MH^2 - 32*MT^2 - 3*U)*(-MH^2 + 4*MT^2 + U)^2)/
        (MH^2 - 4*MT^2)^2 + (2*(MH^2 - 4*MT^2 - U)*
         (6*MH^8 - 4*MT^2*(4*MT^2 + U)^3 - 2*MH^6*(16*MT^2 + 5*U) + 
          MH^4*(-48*MT^4 + 40*MT^2*U + 5*U^2) + MH^2*(384*MT^6 + 48*MT^4*U - 
            U^3))*DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^3*(MH^2 - U)) - 
       (2*(3*MH^2 + 4*MT^2 - 3*U)*(MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT])/
        (MH^2 - U) + ((-8*MT^4 + 6*MT^2*U - U^2 + MH^2*(2*MT^2 + U))*
         Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/U + 
       (2*(8*MT^4 - 6*MT^2*U + U^2 - MH^2*(2*MT^2 + U))*
         Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U))/
     (-MH^2 + 4*MT^2 + U)^4, ((-2*I)*MT^2*Pi*(3*MH^6 - 4*MH^4*U + 
       MH^2*(-144*MT^4 + 4*MT^2*U + U^2) + 4*MT^2*(96*MT^4 + 12*MT^2*U + 
         U^2)))/((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1], 
 -(-((T*(-MH^2 + T + U)^2*(-8*MH^4 - U*(2*T + 3*U) + MH^2*(8*T + 11*U)))/
      (MH^2 - U)^2) + (MH^2*T*(MH^2 - T - U)*(3*MH^6 + 2*T*U^2 - 
       3*MH^4*(3*T + 2*U) + MH^2*(6*T^2 + 7*T*U + 3*U^2))*
      DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T + U)*
     (-3*MH^2 + 3*T + U)*DiscB[T, MT, MT] + 
    (T*(-MH^2 + T + U)*(3*MH^8 + T*U^2*(-T + U) - 6*MH^6*(T + U) + 
       MH^2*T*U*(4*T + 3*U) + MH^4*(3*T^2 + 2*T*U + 3*U^2))*DiscB[U, MT, MT])/
     (MH^2 - U)^3 + (T*(-(MH^6*MT^2) + T^3*U + 
       MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3) + 
       MH^4*(T*U + MT^2*(3*T + 5*U)) - 
       MH^2*(2*T^2*U + MT^2*(3*T^2 + 10*T*U + 6*U^2)))*
      ScalarC0[0, 0, T, MT, MT, MT])/U + (MH^2 - T)^2*
     (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
    ((MH^2 - T)^3*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, 
       MT, MT])/U + ((MH^12*MT^2 + T^2*U^3*(T*U - MT^2*(T + U)) + 
       MH^10*(T*U - MT^2*(3*T + 4*U)) + MH^8*(-(T*U*(4*T + 3*U)) + 
         MT^2*(3*T^2 + 11*T*U + 6*U^2)) + 
       MH^4*U*(-(T*(2*T^3 + 3*T^2*U + U^3)) + MT^2*(11*T^3 + 18*T^2*U + 
           9*T*U^2 + U^3)) - MH^2*T*U*(T*U^2*(3*T + 2*U) + 
         MT^2*(4*T^3 + 9*T^2*U + 6*T*U^2 + 2*U^3)) - 
       MH^6*(-(T*U*(5*T^2 + 6*T*U + 3*U^2)) + MT^2*(T^3 + 14*T^2*U + 
           15*T*U^2 + 4*U^3)))*ScalarC0[0, MH^2, U, MT, MT, MT])/
     ((MH^2 - U)^2*U) - (MH^2 - T)^2*T*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 -(4*(MH^2 - U)*(-MH^2 + T + U)^2 - 2*(MH^2 - T - U)*
     (MH^2*(T - U) + U*(T + U))*DiscB[MH^2, MT, MT] + 
    2*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] - 
    2*(MH^2 - 2*T - U)*(MH^2 - T - U)*U*DiscB[U, MT, MT] + 
    T*(MH^2 - U)^2*U*ScalarC0[0, 0, T, MT, MT, MT] + 
    (MH^2 - U)^2*U^2*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)*(MH^2 - U)^2*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^2 - U)*U*(MH^4 + 2*T^2 + 4*T*U + U^2 - 2*MH^2*(2*T + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - U)^2*U*
     (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
      MT, MT, MT])/(2*T*(MH^2 - U)^2*(-MH^2 + T + U)^2), 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - T)*(MH^4 + 32*MT^4 - 4*MT^2*T + T^2 - 
        2*MH^2*(6*MT^2 + T)))/(8*(MH^2 - 4*MT^2 - T)^3*T), 0, 
     -((MH^2 - T)*(MH^6 - 256*MT^6 + 16*MT^4*T - 32*MT^2*T^2 - T^3 - 
         MH^4*(16*MT^2 + 3*T) + MH^2*(112*MT^4 + 48*MT^2*T + 3*T^2)))/
      (8*T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-((MH^2 - T)*(MH^4 + 64*MT^4 + 4*MT^2*T + T^2 - 2*MH^2*(10*MT^2 + T)))/
      (8*(MH^2 - 4*MT^2 - T)^3), 0, 
     ((MH^2 - T)*(MH^6 - 512*MT^6 - 48*MT^4*T - 32*MT^2*T^2 - T^3 - 
        MH^4*(16*MT^2 + 3*T) + MH^2*(176*MT^4 + 48*MT^2*T + 3*T^2)))/
      (8*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MH^10 - MH^8*(20*MT^2 + 3*T) + 16*MT^4*T*(32*MT^4 - 4*MT^2*T + T^2) + 
        MH^6*(144*MT^4 + 64*MT^2*T + 3*T^2) + 8*MH^2*MT^2*
         (64*MT^6 + 48*MT^4*T + 22*MT^2*T^2 + T^3) - 
        MH^4*(448*MT^6 + 336*MT^4*T + 52*MT^2*T^2 + T^3))/
      (32*MT^2*(MH^2 - 4*MT^2)*T*(-MH^2 + 4*MT^2 + T)^3), 0, 
     -(MH^14 - 4*MH^8*(3*MT^2 + T)*(12*MT^2 + T)^2 - MH^12*(26*MT^2 + 4*T) + 
        2*MH^10*(144*MT^4 + 44*MT^2*T + 3*T^2) + 
        32*MT^6*T*(256*MT^6 - 16*MT^4*T + 32*MT^2*T^2 + T^3) + 
        32*MH^2*MT^4*(256*MT^8 + 192*MT^6*T + 120*MT^4*T^2 + 4*MT^2*T^3 + 
          T^4) + MH^6*(5888*MT^8 + 4352*MT^6*T + 800*MT^4*T^2 + 56*MT^2*T^3 + 
          T^4) - 2*MH^4*MT^2*(5376*MT^8 + 5120*MT^6*T + 1392*MT^4*T^2 + 
          128*MT^2*T^3 + 5*T^4))/(16*(MH^2*MT - 4*MT^3)^2*T*
       (-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((32*(-MH^2 + 4*MT^2 + T)^2)/(MH^2 - 4*MT^2) + 
      (8*MH^2*(MH^4 - 32*MT^4 + MH^2*(4*MT^2 - T))*(MH^2 - 4*MT^2 - T)*
        DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^2) + 
      (8*(MH^2 + 4*MT^2 - T)*(-MH^2 + 4*MT^2 + T)*DiscB[T, MT, MT])/MT^2 - 
      ((MH^2 - T)*(MH^4 + 32*MT^4 - 4*MT^2*T + T^2 - 2*MH^2*(6*MT^2 + T))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/(MT^2*T) + 
      (32*(4*MT^4 + MT^2*T + T^2 - MH^2*(MT^2 + T))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T)/
     (64*(MH^2 - 4*MT^2 - T)^3), 
    ((-I)*Pi*(MH^4 - 8*MT^4 + 2*MT^2*T - MH^2*(2*MT^2 + T)))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2), 
    ((64*(-MH^2 + 4*MT^2 + T)^2*(MH^4 - 16*MT^4 - MH^2*T + MT^2*T))/
       (MH^2 - 4*MT^2)^2 - (4*MH^2*(MH^2 - 4*MT^2 - T)*
        (MH^8 - 2*MH^6*(12*MT^2 + T) - 128*MT^6*(12*MT^2 + T) + 
         MH^4*(48*MT^4 + 36*MT^2*T + T^2) + 4*MH^2*(128*MT^6 - 20*MT^4*T - 
           3*MT^2*T^2))*DiscB[MH^2, MT, MT])/(MT^2*(MH^2 - 4*MT^2)^3) + 
      (4*(MH^2 - 4*MT^2 - T)*(MH^4 - 32*MT^4 + 12*MT^2*T + T^2 - 
         2*MH^2*(6*MT^2 + T))*DiscB[T, MT, MT])/MT^2 + 
      ((MH^2 - T)*(MH^6 - 192*MT^6 - 16*MT^2*T^2 - T^3 - 
         MH^4*(16*MT^2 + 3*T) + MH^2*(96*MT^4 + 32*MT^2*T + 3*T^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/(MT^2*T) + 
      (64*MT^2*(8*MT^4 + 2*MT^2*T + 3*T^2 - MH^2*(2*MT^2 + 3*T))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T)/
     (32*(-MH^2 + 4*MT^2 + T)^4), 
    ((-2*I)*MT^2*Pi*(7*MH^6 + 128*MT^6 - 48*MT^4*T - 4*MT^2*T^2 - 
       2*MH^4*(24*MT^2 + 5*T) + MH^2*(48*MT^4 + 52*MT^2*T + 3*T^2)))/
     ((MH^2 - 4*MT^2)^3*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1], 
 ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MH^4 + 32*MT^4 - 4*MT^2*U + U^2 - 2*MH^2*(6*MT^2 + U))/
      (8*(MH^2 - 4*MT^2 - U)^3), 0, (MH^6 - 256*MT^6 + 16*MT^4*U - 
       32*MT^2*U^2 - U^3 - MH^4*(16*MT^2 + 3*U) + 
       MH^2*(112*MT^4 + 48*MT^2*U + 3*U^2))/(8*(-MH^2 + 4*MT^2 + U)^4)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-((MH^2 - 4*MT^2)*(MH^4 + 32*MT^4 - 4*MT^2*U + U^2 - 
         2*MH^2*(6*MT^2 + U)))/(32*MT^2*(-MH^2 + 4*MT^2 + U)^3), 0, 
     -(MH^8 - 3*MH^6*(6*MT^2 + U) + MH^4*(128*MT^4 + 38*MT^2*U + 3*U^2) - 
        MH^2*(416*MT^6 + 96*MT^4*U + 22*MT^2*U^2 + U^3) + 
        2*MT^2*(256*MT^6 - 16*MT^4*U + 32*MT^2*U^2 + U^3))/
      (16*MT^2*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + 
  ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {(U*(MH^4 + 64*MT^4 + 4*MT^2*U + U^2 - 2*MH^2*(10*MT^2 + U)))/
      (8*(MH^2 - 4*MT^2 - U)^3), 0, 
     (U*(-MH^6 + 512*MT^6 + 48*MT^4*U + 32*MT^2*U^2 + U^3 + 
        MH^4*(16*MT^2 + 3*U) - MH^2*(176*MT^4 + 48*MT^2*U + 3*U^2)))/
      (8*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((-8*(-MH^2 + 4*MT^2 + U)^2)/MT^2 - (8*MH^2*(MH^2 - 4*MT^2 - U)*
        (-16*MT^4 - U^2 + MH^2*(4*MT^2 + U))*DiscB[MH^2, MT, MT])/
       (MT^2*(MH^2 - 4*MT^2)*(MH^2 - U)) - 
      (8*(MH^2 + 4*MT^2 - U)*U*(-MH^2 + 4*MT^2 + U)*DiscB[U, MT, MT])/
       (MT^2*(MH^2 - U)) + ((-MH^6 + 128*MT^6 + 64*MT^4*U + 28*MT^2*U^2 + 
         U^3 + 3*MH^4*(4*MT^2 + U) - MH^2*(64*MT^4 + 40*MT^2*U + 3*U^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (MT^2*(MH^2 - U)) + (32*(-4*MT^4 - MT^2*U - U^2 + MH^2*(MT^2 + U))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(MH^2 - U))/
     (64*(MH^2 - 4*MT^2 - U)^3), ((I/2)*Pi*(MH^2 - 4*MT^2 + U))/
     ((MH^2 - 4*MT^2)*(-MH^2 + 4*MT^2 + U)^2), 
    ((4*(-MH^2 + 4*MT^2 + U)^2*(MH^4 + 64*MT^4 - 4*MT^2*U - 
         MH^2*(20*MT^2 + U)))/(MH^2 - 4*MT^2) + 
      (4*MH^2*(MH^2 - 4*MT^2 - U)*(MH^6*U - 2*MH^4*(16*MT^4 + 10*MT^2*U + 
           U^2) - 8*MT^2*(64*MT^6 + 8*MT^2*U^2 + U^3) + 
         MH^2*(256*MT^6 + 64*MT^4*U + 28*MT^2*U^2 + U^3))*
        DiscB[MH^2, MT, MT])/((MH^2 - 4*MT^2)^2*(MH^2 - U)) - 
      (4*(MH^2 - 4*MT^2 - U)*U*(MH^4 - 32*MT^4 + 12*MT^2*U + U^2 - 
         2*MH^2*(6*MT^2 + U))*DiscB[U, MT, MT])/(MH^2 - U) + 
      ((MH^8 + 512*MT^8 + 320*MT^6*U + 192*MT^4*U^2 + 16*MT^2*U^3 + U^4 - 
         4*MH^6*(4*MT^2 + U) + 6*MH^4*(4*MT^2 + U)^2 - 
         4*MH^2*(80*MT^6 + 72*MT^4*U + 12*MT^2*U^2 + U^3))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
       (MH^2 - U) - (64*MT^4*(8*MT^4 + 2*MT^2*U + 3*U^2 - 
         MH^2*(2*MT^2 + 3*U))*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
           (2*MT^2)]^2)/(MH^2 - U))/(32*MT^2*(-MH^2 + 4*MT^2 + U)^4), 
    ((2*I)*MT^2*Pi*(2*MH^4 + 32*MT^4 - 12*MT^2*U - U^2 + 
       MH^2*(-16*MT^2 + 3*U)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3)}, 0, 
   4, 1], 
 -((-MH^2 + T + U)^2/T + (MH^2*(MH^2 - T - U)*(-T^2 - U^2 + MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/((MH^2 - T)*T*(MH^2 - U)) - 
    ((MH^2 - T - U)*(MH^2 - T + U)*DiscB[T, MT, MT])/(MH^2 - T) + 
    ((MH^2 + T - U)*U*(-MH^2 + T + U)*DiscB[U, MT, MT])/(T*(MH^2 - U)) + 
    ((MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + MT^2*(2*T^2 + 3*T*U + U^2))*
      ScalarC0[0, 0, T, MT, MT, MT])/T + 
    (U*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, 0, U, MT, MT, MT])/T^2 - 
    ((MH^2 - T)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, MH^2, T, MT, MT, MT])/T^2 + 
    ((MH^6*MT^2 - T^2*U^2 - 3*MH^4*MT^2*(T + U) - 
       MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) + 
       MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*ScalarC0[0, MH^2, U, MT, 
       MT, MT])/T^2 - (U*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(5*T + 2*U) + 
       MT^2*(4*T^2 + 5*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
       MT])/T)/(2*(MH^2 - T - U)^3), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(MH^2 - T)^2*(3*MH^2 - 12*MT^2 + T))/
      (2*(MH^2 - 4*MT^2 - T)^3*T), 0, 
     (-2*MT^2*(MH^2 - T)^2*(24*MT^4 - 2*MT^2*T + T^2 - MH^2*(6*MT^2 + T)))/
      (T*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - T)^2*(MH^2 - 4*MT^2 + 3*T))/
      (2*(MH^2 - 4*MT^2 - T)^3*T^2), 0, 
     (2*MT^2*(MH^2 - T)^2*(8*MT^4 - 6*MT^2*T + T^2 - MH^2*(2*MT^2 + T)))/
      (T^2*(-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(-MH^12 + MH^10*(16*MT^2 - T) + 
       64*MT^6*(4*MT^2 - 3*T)*T^2 + MH^8*(-96*MT^4 + 4*MT^2*T + 13*T^2) + 
       MH^6*(256*MT^6 + 48*MT^4*T - 40*MT^2*T^2 - 19*T^3) + 
       16*MH^2*MT^2*T*(32*MT^6 + 56*MT^4*T + 21*MT^2*T^2 + T^3) - 
       4*MH^4*(64*MT^8 + 80*MT^6*T + 72*MT^4*T^2 - MT^2*T^3 - 2*T^4))/
      (8*(MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - T)^3*T^2), 0, 
     (MH^16 - 4*MH^14*(7*MT^2 + T) - 1024*MT^8*T^2*(8*MT^4 - 6*MT^2*T + 
         T^2) + MH^12*(320*MT^4 + 64*MT^2*T + 6*T^2) - 
       4*MH^10*(480*MT^6 + 112*MT^4*T - 22*MT^2*T^2 + T^3) - 
       4*MH^6*MT^2*(2816*MT^8 + 1792*MT^6*T + 16*MT^4*T^2 - 488*MT^2*T^3 - 
         65*T^4) + MH^8*(6400*MT^8 + 2048*MT^6*T - 1008*MT^4*T^2 - 
         320*MT^2*T^3 + T^4) - 64*MH^2*MT^4*T*(256*MT^8 + 528*MT^6*T + 
         248*MT^4*T^2 + 19*MT^2*T^3 + 2*T^4) + 16*MH^4*MT^2*
        (512*MT^10 + 1024*MT^8*T + 1136*MT^6*T^2 + 72*MT^4*T^3 - 
         43*MT^2*T^4 - 4*T^5))/(8*(MH^2 - 4*MT^2)^3*T^2*
       (-MH^2 + 4*MT^2 + T)^4)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {-((32*T*(-MH^2 + 4*MT^2 + T)^2*(2*MH^4 + 2*MT^2*(6*MT^2 + T) - 
          MH^2*(11*MT^2 + 2*T)))/(MH^2 - 4*MT^2)^2 + 
       (8*MH^2*(MH^2 - 4*MT^2 - T)*T*(3*MH^6 + 32*MT^4*T - 
          3*MH^4*(8*MT^2 + 3*T) + MH^2*(48*MT^4 + 28*MT^2*T + 6*T^2))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^3 + 8*(3*MH^2 - 4*MT^2 - 3*T)*
        (MH^2 - 4*MT^2 - T)*T*DiscB[T, MT, MT] - (MH^2 - T)^2*
        (MH^2 - 4*MT^2 + 3*T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + 8*(16*MT^6 + 12*MT^4*T + 2*MT^2*T^2 + T^3 + 
         MH^4*(2*MT^2 + T) - 2*MH^2*(6*MT^4 + 2*MT^2*T + T^2))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (16*(MH^2 - 4*MT^2 - T)^3*T^2), 
    ((I/2)*Pi*(3*MH^8 + 16*MT^4*(4*MT^2 - T)*T + 16*MH^2*MT^2*T*
        (3*MT^2 + T) - 6*MH^6*(4*MT^2 + T) + 
       MH^4*(48*MT^4 + 8*MT^2*T + 3*T^2)))/((MH^2 - 4*MT^2)^3*T*
      (-MH^2 + 4*MT^2 + T)^2), 
    -((16*T*(-MH^2 + 4*MT^2 + T)^2*(3*MH^8 + 2*MH^6*(MT^2 - 3*T) - 
          32*MT^4*(12*MT^4 + 2*MT^2*T + T^2) + MH^4*(-184*MT^4 - 46*MT^2*T + 
            3*T^2) + 4*MH^2*(152*MT^6 + 74*MT^4*T + 11*MT^2*T^2)))/
        (MH^2 - 4*MT^2)^3 + (32*MH^2*MT^2*(MH^2 - 4*MT^2 - T)*T*
         (9*MH^8 - 32*MT^4*T*(12*MT^2 + T) - MH^6*(108*MT^2 + 41*T) + 
          MH^4*(432*MT^4 + 304*MT^2*T + 50*T^2) - 
          2*MH^2*(288*MT^6 + 232*MT^4*T + 96*MT^2*T^2 + 9*T^3))*
         DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2)^4 + 
       32*MT^2*T*(-MH^2 + 4*MT^2 + T)*(-5*MH^2 + 4*MT^2 + 5*T)*
        DiscB[T, MT, MT] + (MH^2 - T)^2*(MH^4 + 48*MT^4 - 32*MT^2*T + T^2 - 
         2*MH^2*(8*MT^2 + T))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + 32*MT^2*(16*MT^6 + 16*MT^4*T + 3*MT^2*T^2 + 3*T^3 + 
         3*MH^4*(MT^2 + T) - 2*MH^2*(8*MT^4 + 3*MT^2*T + 3*T^2))*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (16*T^2*(-MH^2 + 4*MT^2 + T)^4), 
    ((-2*I)*MT^2*Pi*(-9*MH^10 + MH^8*(108*MT^2 + 31*T) + 
       16*MT^4*T*(32*MT^4 - 12*MT^2*T - T^2) + 8*MH^2*MT^2*T*
        (80*MT^4 + 46*MT^2*T + 3*T^2) - MH^6*(432*MT^4 + 192*MT^2*T + 
         35*T^2) + MH^4*(576*MT^6 + 80*MT^4*T + 60*MT^2*T^2 + 13*T^3)))/
     ((MH^2 - 4*MT^2)^4*(MH^2 - 4*MT^2 - T)^3*T)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*U*(3*MH^2 - 12*MT^2 + U))/
      (2*(MH^2 - 4*MT^2 - U)^3), 0, 
     (-2*MT^2*U*(24*MT^4 - 2*MT^2*U + U^2 - MH^2*(6*MT^2 + U)))/
      (-MH^2 + 4*MT^2 + U)^4}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2 + 3*U))/
      (2*(MH^2 - 4*MT^2 - U)^3), 0, 
     (2*MT^2*(8*MT^4 - 6*MT^2*U + U^2 - MH^2*(2*MT^2 + U)))/
      (-MH^2 + 4*MT^2 + U)^4}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 + 3*U))/
      (8*(MH^2 - 4*MT^2 - U)^3), 0, 
     -(MH^6 - 2*MH^4*(8*MT^2 + U) + MH^2*(80*MT^4 - 16*MT^2*U + U^2) - 
        16*MT^2*(8*MT^4 - 6*MT^2*U + U^2))/(8*(-MH^2 + 4*MT^2 + U)^4)}, 0, 4, 
    1] + SeriesData[\[Beta], 0, 
   {-(-8*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2 - 
       (8*(MH^2 - 4*MT^2 - U)*(MH^6 - 4*MH^2*U*(2*MT^2 + U) + 
          U*(4*MT^2 + U)^2 + MH^4*(-4*MT^2 + 2*U))*DiscB[MH^2, MT, MT])/
        (MH^2 - 4*MT^2) + 8*(3*MH^2 - 4*MT^2 - 3*U)*(MH^2 - 4*MT^2 - U)*U*
        DiscB[U, MT, MT] + (MH^6 - 128*MT^6 - 96*MT^4*U - 20*MT^2*U^2 - 
         5*U^3 - MH^4*(20*MT^2 + 7*U) + MH^2*(96*MT^4 + 40*MT^2*U + 11*U^2))*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
       8*(16*MT^6 + 12*MT^4*U + 2*MT^2*U^2 + U^3 + MH^4*(2*MT^2 + U) - 
         2*MH^2*(6*MT^4 + 2*MT^2*U + U^2))*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (16*(MH^2 - U)^2*(MH^2 - 4*MT^2 - U)^3), ((-I/2)*Pi*(MH^2 - 4*MT^2 + U))/
     ((MH^2 - 4*MT^2)*(-MH^2 + 4*MT^2 + U)^2), 
    ((16*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2*(MH^4 - 2*MH^2*MT^2 - 8*MT^4 + 
         4*MT^2*U - U^2))/(MH^2 - 4*MT^2) + 
      (32*MT^2*(-MH^2 + 4*MT^2 + U)*(-2*MH^8 + 4*MH^6*(4*MT^2 - U) + 
         U*(4*MT^2 + U)^3 - MH^2*U*(48*MT^4 + 48*MT^2*U + 5*U^2) + 
         MH^4*(-32*MT^4 + 24*MT^2*U + 10*U^2))*DiscB[MH^2, MT, MT])/
       (MH^2 - 4*MT^2)^2 - 32*MT^2*U*(-MH^2 + 4*MT^2 + U)*
       (-5*MH^2 + 4*MT^2 + 5*U)*DiscB[U, MT, MT] + 
      (MH^8 + 512*MT^8 + 512*MT^6*U + 144*MT^4*U^2 + 64*MT^2*U^3 + U^4 - 
        4*MH^6*(4*MT^2 + U) + 6*MH^4*(24*MT^4 + 16*MT^2*U + U^2) - 
        4*MH^2*(128*MT^6 + 72*MT^4*U + 36*MT^2*U^2 + U^3))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
      32*MT^2*(16*MT^6 + 16*MT^4*U + 3*MT^2*U^2 + 3*U^3 + 3*MH^4*(MT^2 + U) - 
        2*MH^2*(8*MT^4 + 3*MT^2*U + 3*U^2))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (16*(MH^2 - U)^2*(-MH^2 + 4*MT^2 + U)^4), 
    ((-2*I)*MT^2*Pi*(2*MH^4 + 32*MT^4 - 12*MT^2*U - U^2 + 
       MH^2*(-16*MT^2 + 3*U)))/((MH^2 - 4*MT^2)^2*(MH^2 - 4*MT^2 - U)^3)}, 0, 
   4, 1], (-((T*(-MH^2 + T + U)^2*(-3*MH^4 + MH^2*(3*T + U) + U*(3*T + 2*U)))/
     (MH^2 - U)^2) - (MH^2*T*(MH^2 - T - U)*(MH^4*(T - 3*U) - 
      U*(5*T^2 + 10*T*U + 3*U^2) + MH^2*(-T^2 + 9*T*U + 6*U^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T - U)*(-MH^2 + T + U)*
    DiscB[T, MT, MT] + (T*U*(-MH^2 + T + U)*(3*MH^6 + T*U*(T + 3*U) - 
      2*MH^4*(4*T + 3*U) + MH^2*(5*T^2 + 5*T*U + 3*U^2))*DiscB[U, MT, MT])/
    (MH^2 - U)^3 + (MH^2 - T)*T*(MH^2*MT^2 + T*U - MT^2*(T + U))*
    ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)*U*
    (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + ((MH^10*MT^2 + MH^8*(T*U - 2*MT^2*(T + 2*U)) - 
      MH^6*(T*U*(5*T + 3*U) + MT^2*(T^2 - 7*T*U - 6*U^2)) + 
      MH^4*(3*T*U*(2*T^2 + 3*T*U + U^2) + MT^2*(4*T^3 - 3*T^2*U - 9*T*U^2 - 
          4*U^3)) - T*U*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)) + 
      MH^2*(-(T*U*(2*T^3 + 6*T^2*U + 3*T*U^2 + U^3)) + 
        MT^2*(-2*T^4 + 2*T^3*U + 9*T^2*U^2 + 5*T*U^3 + U^4)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 - 
   (MH^2 - T)*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 (-((T*(-MH^2 + T + U)^2*(-2*MH^4 - U*(8*T + 7*U) + MH^2*(2*T + 9*U)))/
     (MH^2 - U)^2) - (T*(-MH^2 + T + U)*(-(MH^6*T) - 3*U^2*(T + U)^2 + 
      MH^4*(T^2 + 7*T*U - 3*U^2) + MH^2*(-4*T^2*U + 6*U^3))*
     DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T + U)*(-MH^2 + T + 3*U)*
    DiscB[T, MT, MT] + (T*U^2*(-MH^2 + T + U)*(3*MH^4 + 6*T^2 + 11*T*U + 
      3*U^2 - MH^2*(11*T + 6*U))*DiscB[U, MT, MT])/(-MH^2 + U)^3 - 
   T*U*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, T, MT, MT, MT] + 
   U^2*(-(MH^2*MT^2) - T*U + MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - T)*U*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] - (U*(MH^8*MT^2 - T*U*(2*T^3 + 6*T^2*U + 6*T*U^2 + U^3) + 
      MT^2*(-4*T^4 - 10*T^3*U - 6*T^2*U^2 + T*U^3 + U^4) + 
      MH^6*(T*U - MT^2*(T + 4*U)) - 3*MH^4*(T*U*(2*T + U) + 
        MT^2*(2*T^2 - T*U - 2*U^2)) + MH^2*(3*T*U*(2*T^2 + 4*T*U + U^2) + 
        MT^2*(10*T^3 + 12*T^2*U - 3*T*U^2 - 4*U^3)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 + 
   T*U^2*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, 
     MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 SeriesData[\[Beta], 0, {-1/(4*(MH^2 - 4*MT^2 - T)), 0, 
    -(MT^2/(-MH^2 + 4*MT^2 + T)^2)}, 0, 3, 1]*
  (T*ScalarC0[0, 0, T, MT, MT, MT] + (-MH^2 + T)*ScalarC0[0, MH^2, T, MT, MT, 
     MT] - MH^2*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] - 
   4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
     MT, MT] + 4*MT^2*T*ScalarD0[0, 0, 0, MH^2, T, 
     (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT] + 
   ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {16*MT^4, 0, 16*MT^4, 0, 16*MT^4}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*T, 0, -4*MT^2*T, 0, -4*MT^2*T}, 0, 5, 
     1]), SeriesData[\[Beta], 0, {-1/(4*(MH^2 - 4*MT^2 - U)), 0, 
    -(MT^2/(-MH^2 + 4*MT^2 + U)^2)}, 0, 3, 1]*
  (U*ScalarC0[0, 0, U, MT, MT, MT] + (-MH^2 + U)*ScalarC0[0, MH^2, U, MT, MT, 
     MT] - MH^2*ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT] - 
   4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, 
     MT, MT] + 4*MT^2*U*ScalarD0[0, 0, 0, MH^2, U, 
     (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT] + 
   ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
    SeriesData[\[Beta], 0, {4*MT^2, 0, 4*MT^2, 0, 4*MT^2}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {16*MT^4, 0, 16*MT^4, 0, 16*MT^4}, 0, 5, 1] + 
   ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
    SeriesData[\[Beta], 0, {-4*MT^2*U, 0, -4*MT^2*U, 0, -4*MT^2*U}, 0, 5, 
     1]), -((T*ScalarC0[0, 0, T, MT, MT, MT] + 
    U*ScalarC0[0, 0, U, MT, MT, MT] - MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
    MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
    U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
    4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    4*MT^2*T*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    4*MT^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
    T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(4*MH^2 - 4*T - 4*U)), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(MH^2 - 4*MT^2)*T)/
      (2*(-MH^2 + 4*MT^2 + T)^2), 0, 
     -(MT^2*T*(16*MT^4 + T^2 - MH^2*(4*MT^2 + T)))/
      (2*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2 + T))/
      (4*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (MT^2*(8*MT^4 - 2*MT^2*T + T^2 - MH^2*(2*MT^2 + T)))/
      (2*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 + T))/
      (16*(-MH^2 + 4*MT^2 + T)^2), 0, 
     -(MH^6 - 2*MH^4*(6*MT^2 + T) - 8*MT^2*(8*MT^4 - 2*MT^2*T + T^2) + 
        MH^2*(48*MT^4 + 4*MT^2*T + T^2))/(16*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 
    1] + SeriesData[\[Beta], 0, 
   {(8*MH^2*(MH^2 - 4*MT^2 - T)*DiscB[MH^2, MT, MT] - 
      8*(MH^2 - 4*MT^2 - T)*T*DiscB[T, MT, MT] - 
      (MH^4 + 32*MT^4 + 12*MT^2*T + 3*T^2 - 4*MH^2*(3*MT^2 + T))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      4*(8*MT^4 + 2*MT^2*T + T^2 - MH^2*(2*MT^2 + T))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (32*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2), ((-I/4)*Pi)/(MH^2 - 4*MT^2 - T), 
    (16*(MH^2 - T)*(-MH^2 + 4*MT^2 + T)^2 + 32*MH^2*MT^2*(MH^2 - 4*MT^2 - T)*
       DiscB[MH^2, MT, MT] + 32*MT^2*T*(-MH^2 + 4*MT^2 + T)*
       DiscB[T, MT, MT] + (MH^6 - 128*MT^6 - 64*MT^4*T - 28*MT^2*T^2 - T^3 - 
        3*MH^4*(4*MT^2 + T) + MH^2*(64*MT^4 + 40*MT^2*T + 3*T^2))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      32*MT^2*(4*MT^4 + MT^2*T + T^2 - MH^2*(MT^2 + T))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
     (32*(MH^2 - T)*(MH^2 - 4*MT^2 - T)^3), ((-I)*MT^2*Pi)/
     (-MH^2 + 4*MT^2 + T)^2}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2)*(MH^2 - U))/
      (2*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (MT^2*(-(MH^4*(4*MT^2 + U)) - U*(16*MT^4 + U^2) + 
        2*MH^2*(8*MT^4 + 2*MT^2*U + U^2)))/(2*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 
    1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(MH^2 - U)*(MH^2 - 4*MT^2 + U))/
      (4*U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     -(MT^2*(MH^4*(2*MT^2 + U) - 2*MH^2*(4*MT^4 + U^2) + 
         U*(8*MT^4 - 2*MT^2*U + U^2)))/(2*U*(-MH^2 + 4*MT^2 + U)^3)}, 0, 4, 
    1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^6 + 4*MT^2*(4*MT^2 - U)*U - 
       4*MH^4*(2*MT^2 + U) + MH^2*(16*MT^4 + 12*MT^2*U + 3*U^2))/
      (16*U*(-MH^2 + 4*MT^2 + U)^2), 0, (-MH^8 + 3*MH^6*(4*MT^2 + U) + 
       8*MT^2*U*(8*MT^4 - 2*MT^2*U + U^2) - MH^4*(48*MT^4 + 32*MT^2*U + 
         3*U^2) + MH^2*(64*MT^6 + 64*MT^4*U + 12*MT^2*U^2 + U^3))/
      (16*(MH^2 - 4*MT^2 - U)^3*U)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {-((8*MH^2*(MH^2 - 4*MT^2 - U)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) - 
       8*(MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT] + 
       ((MH^2 - U)*(MH^2 - 4*MT^2 + U)*
         Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/U + 
       (4*(8*MT^4 + 2*MT^2*U + U^2 - MH^2*(2*MT^2 + U))*
         Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U)/
     (32*(-MH^2 + 4*MT^2 + U)^2), (I*MT^2*Pi)/((MH^2 - 4*MT^2)*
      (MH^2 - 4*MT^2 - U)), (-2*MT^2)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)) - 
     (MH^2*MT^2*(2*MH^2 - 8*MT^2 - U)*DiscB[MH^2, MT, MT])/
      ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2) + (MT^2*DiscB[U, MT, MT])/
      (-MH^2 + 4*MT^2 + U)^2 - ((MH^2 - U)*(MH^4 + 32*MT^4 - 4*MT^2*U + U^2 - 
        2*MH^2*(6*MT^2 + U))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2)/(32*U*(-MH^2 + 4*MT^2 + U)^3) + 
     (MT^2*(4*MT^4 + MT^2*U + U^2 - MH^2*(MT^2 + U))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
      (U*(-MH^2 + 4*MT^2 + U)^3), (I*MT^2*Pi*(MH^4 - 16*MT^4 - MH^2*U))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 -(2*MH^2*T*(MH^2 - T - U)*U*DiscB[MH^2, MT, MT] + 
    2*T^2*U*(-MH^2 + T + U)*DiscB[T, MT, MT] - 2*(MH^2 - T)*T*(MH^2 - T - U)*
     U*DiscB[U, MT, MT] - (MH^2 - T)*T*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 
      2*MT^2*(T + U))*ScalarC0[0, 0, T, MT, MT, MT] + 
    (MH^2 - T)*U*(2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
      MH^2*(T*U + MT^2*(6*T + 4*U)))*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)*(2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
      MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
        2*MT^2*(T^2 + T*U + U^2)))*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^2 - T)*(MH^2 - U)^2*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT] + (MH^2 - T)*T*(MH^2 - U)*U*
     (4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
      MT, MT, MT])/(8*(MH^2 - T)*T*U*(-MH^2 + T + U)^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - 4*MT^2))/(-MH^2 + 4*MT^2 + T)^2, 
     0, (2*MT^4*(MH^2 - T)*(MH^2 - 4*MT^2 + T))/(MH^2 - 4*MT^2 - T)^3}, 0, 4, 
    1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-((MT^4*(MH^2 - 4*MT^2 + T))/
       (T*(-MH^2 + 4*MT^2 + T)^2)), 0, 
     (MT^4*(MH^2 - T)*(MH^2 - 4*MT^2 + 3*T))/(T*(-MH^2 + 4*MT^2 + T)^3)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MT^2*(MH^4 - MH^2*(8*MT^2 + 5*T) + 2*(8*MT^4 + 10*MT^2*T + T^2)))/
      (4*T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     -(MT^2*(-MH^4 + 8*MT^4 - 6*MT^2*T + MH^2*(2*MT^2 + T)))/
      (2*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((2*MH^2*(MH^2 - 4*MT^2 - T)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) - 
      2*(MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT] + 
      (MT^2*(MH^2 - 4*MT^2 + T)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2)/T - (2*MT^2*(MH^2 - 4*MT^2 + T)*
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T)/
     (8*(-MH^2 + 4*MT^2 + T)^2), ((-I)*MT^2*Pi)/((MH^2 - 4*MT^2)*
      (MH^2 - 4*MT^2 - T)), (MT^2*(8/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)) + 
       (4*MH^2*(2*MH^2 - 8*MT^2 - T)*DiscB[MH^2, MT, MT])/
        ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2) - (4*DiscB[T, MT, MT])/
        (-MH^2 + 4*MT^2 + T)^2 + ((-8*MT^4 + 2*MT^2*T - T^2 + 
          MH^2*(2*MT^2 + T))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/((MH^2 - 4*MT^2 - T)^3*T) - 
       (2*(-8*MT^4 + 2*MT^2*T - T^2 + MH^2*(2*MT^2 + T))*
         Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
        ((MH^2 - 4*MT^2 - T)^3*T)))/4, (I*MT^2*Pi*(-MH^4 + 16*MT^4 + MH^2*T))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(2*MT^4*(MH^2 - 4*MT^2))/(-MH^2 + 4*MT^2 + U)^2, 
     0, (2*MT^4*(MH^2 - U)*(MH^2 - 4*MT^2 + U))/(MH^2 - 4*MT^2 - U)^3}, 0, 4, 
    1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-((MT^4*(MH^2 - 4*MT^2 + U))/
       (U*(-MH^2 + 4*MT^2 + U)^2)), 0, 
     (MT^4*(MH^2 - U)*(MH^2 - 4*MT^2 + 3*U))/(U*(-MH^2 + 4*MT^2 + U)^3)}, 0, 
    4, 1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, 
    {-(MT^2*(MH^4 - MH^2*(8*MT^2 + 5*U) + 2*(8*MT^4 + 10*MT^2*U + U^2)))/
      (4*U*(-MH^2 + 4*MT^2 + U)^2), 0, 
     -(MT^2*(-MH^4 + 8*MT^4 - 6*MT^2*U + MH^2*(2*MT^2 + U)))/
      (2*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1] + SeriesData[\[Beta], 0, 
   {((2*MH^2*(MH^2 - 4*MT^2 - U)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) - 
      2*(MH^2 - 4*MT^2 - U)*DiscB[U, MT, MT] + 
      (MT^2*(MH^2 - 4*MT^2 + U)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2)/U - (2*MT^2*(MH^2 - 4*MT^2 + U)*
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U)/
     (8*(-MH^2 + 4*MT^2 + U)^2), ((-I)*MT^2*Pi)/((MH^2 - 4*MT^2)*
      (MH^2 - 4*MT^2 - U)), (MT^2*(8/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - U)) + 
       (4*MH^2*(2*MH^2 - 8*MT^2 - U)*DiscB[MH^2, MT, MT])/
        ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2) - (4*DiscB[U, MT, MT])/
        (-MH^2 + 4*MT^2 + U)^2 + ((-8*MT^4 + 2*MT^2*U - U^2 + 
          MH^2*(2*MT^2 + U))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
            (2*MT^2)]^2)/((MH^2 - 4*MT^2 - U)^3*U) - 
       (2*(-8*MT^4 + 2*MT^2*U - U^2 + MH^2*(2*MT^2 + U))*
         Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
        ((MH^2 - 4*MT^2 - U)^3*U)))/4, (I*MT^2*Pi*(-MH^4 + 16*MT^4 + MH^2*U))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + U)^2)}, 0, 4, 1], 
 -(2*MH^2*T*(MH^2 - T - U)*U*DiscB[MH^2, MT, MT] + 
    2*T*(MH^2 - U)*U*(-MH^2 + T + U)*DiscB[T, MT, MT] + 
    2*T*U^2*(-MH^2 + T + U)*DiscB[U, MT, MT] + 
    T*(MH^2 - U)*(2*MH^4*MT^2 + T^2*U + 2*MT^2*(T^2 + 3*T*U + 2*U^2) - 
      MH^2*(T*U + MT^2*(4*T + 6*U)))*ScalarC0[0, 0, T, MT, MT, MT] - 
    (MH^2 - T)*(MH^2 - U)*U*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarC0[0, 0, U, MT, MT, MT] + (MH^2 - T)^2*(MH^2 - U)*
     (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, MT] - 
    (MH^2 - U)*(2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
      MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
        2*MT^2*(T^2 + T*U + U^2)))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
    (MH^2 - T)*T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (8*T*(MH^2 - U)*U*(-MH^2 + T + U)^2), 
 ScalarD0[0, 0, 0, MH^2, T, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2)*(MH^2 - T))/
      (2*(-MH^2 + 4*MT^2 + T)^2), 0, 
     (MT^2*(-(MH^4*(4*MT^2 + T)) - T*(16*MT^4 + T^2) + 
        2*MH^2*(8*MT^4 + 2*MT^2*T + T^2)))/(2*(MH^2 - 4*MT^2 - T)^3)}, 0, 4, 
    1] + ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(MH^2 - T)*(MH^2 - 4*MT^2 + T))/
      (4*T*(-MH^2 + 4*MT^2 + T)^2), 0, 
     -(MT^2*(MH^4*(2*MT^2 + T) - 2*MH^2*(4*MT^4 + T^2) + 
         T*(8*MT^4 - 2*MT^2*T + T^2)))/(2*T*(-MH^2 + 4*MT^2 + T)^3)}, 0, 4, 
    1] + ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MH^6 + 4*MT^2*(4*MT^2 - T)*T - 
       4*MH^4*(2*MT^2 + T) + MH^2*(16*MT^4 + 12*MT^2*T + 3*T^2))/
      (16*T*(-MH^2 + 4*MT^2 + T)^2), 0, (-MH^8 + 3*MH^6*(4*MT^2 + T) + 
       8*MT^2*T*(8*MT^4 - 2*MT^2*T + T^2) - MH^4*(48*MT^4 + 32*MT^2*T + 
         3*T^2) + MH^2*(64*MT^6 + 64*MT^4*T + 12*MT^2*T^2 + T^3))/
      (16*(MH^2 - 4*MT^2 - T)^3*T)}, 0, 4, 1] + 
  SeriesData[\[Beta], 0, 
   {-((8*MH^2*(MH^2 - 4*MT^2 - T)*DiscB[MH^2, MT, MT])/(MH^2 - 4*MT^2) - 
       8*(MH^2 - 4*MT^2 - T)*DiscB[T, MT, MT] + 
       ((MH^2 - T)*(MH^2 - 4*MT^2 + T)*
         Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/T + 
       (4*(8*MT^4 + 2*MT^2*T + T^2 - MH^2*(2*MT^2 + T))*
         Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T)/
     (32*(-MH^2 + 4*MT^2 + T)^2), (I*MT^2*Pi)/((MH^2 - 4*MT^2)*
      (MH^2 - 4*MT^2 - T)), (-2*MT^2)/((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 - T)) - 
     (MH^2*MT^2*(2*MH^2 - 8*MT^2 - T)*DiscB[MH^2, MT, MT])/
      ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2) + (MT^2*DiscB[T, MT, MT])/
      (-MH^2 + 4*MT^2 + T)^2 - ((MH^2 - T)*(MH^4 + 32*MT^4 - 4*MT^2*T + T^2 - 
        2*MH^2*(6*MT^2 + T))*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
          (2*MT^2)]^2)/(32*T*(-MH^2 + 4*MT^2 + T)^3) + 
     (MT^2*(4*MT^4 + MT^2*T + T^2 - MH^2*(MT^2 + T))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
      (T*(-MH^2 + 4*MT^2 + T)^3), (I*MT^2*Pi*(MH^4 - 16*MT^4 - MH^2*T))/
     ((MH^2 - 4*MT^2)^2*(-MH^2 + 4*MT^2 + T)^2)}, 0, 4, 1], 
 ScalarD0[0, 0, 0, MH^2, U, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT, MT]*
   SeriesData[\[Beta], 0, {(MT^2*(MH^2 - 4*MT^2)*U)/
      (2*(-MH^2 + 4*MT^2 + U)^2), 0, 
     -(MT^2*U*(16*MT^4 + U^2 - MH^2*(4*MT^2 + U)))/
      (2*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1] + 
  ScalarC0[0, 0, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {-(MT^2*(MH^2 - 4*MT^2 + U))/
      (4*(-MH^2 + 4*MT^2 + U)^2), 0, 
     (MT^2*(8*MT^4 - 2*MT^2*U + U^2 - MH^2*(2*MT^2 + U)))/
      (2*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 1] + 
  ScalarC0[0, MH^2, (-4*MT^2)/(-1 + \[Beta]^2), MT, MT, MT]*
   SeriesData[\[Beta], 0, {((MH^2 - 4*MT^2)*(MH^2 - 4*MT^2 + U))/
      (16*(-MH^2 + 4*MT^2 + U)^2), 0, 
     -(MH^6 - 2*MH^4*(6*MT^2 + U) - 8*MT^2*(8*MT^4 - 2*MT^2*U + U^2) + 
        MH^2*(48*MT^4 + 4*MT^2*U + U^2))/(16*(MH^2 - 4*MT^2 - U)^3)}, 0, 4, 
    1] + SeriesData[\[Beta], 0, 
   {(8*MH^2*(MH^2 - 4*MT^2 - U)*DiscB[MH^2, MT, MT] - 
      8*(MH^2 - 4*MT^2 - U)*U*DiscB[U, MT, MT] - 
      (MH^4 + 32*MT^4 + 12*MT^2*U + 3*U^2 - 4*MH^2*(3*MT^2 + U))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      4*(8*MT^4 + 2*MT^2*U + U^2 - MH^2*(2*MT^2 + U))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (32*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2), ((-I/4)*Pi)/(MH^2 - 4*MT^2 - U), 
    (16*(MH^2 - U)*(-MH^2 + 4*MT^2 + U)^2 + 32*MH^2*MT^2*(MH^2 - 4*MT^2 - U)*
       DiscB[MH^2, MT, MT] + 32*MT^2*U*(-MH^2 + 4*MT^2 + U)*
       DiscB[U, MT, MT] + (MH^6 - 128*MT^6 - 64*MT^4*U - 28*MT^2*U^2 - U^3 - 
        3*MH^4*(4*MT^2 + U) + MH^2*(64*MT^4 + 40*MT^2*U + 3*U^2))*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
      32*MT^2*(4*MT^4 + MT^2*U + U^2 - MH^2*(MT^2 + U))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
     (32*(MH^2 - U)*(MH^2 - 4*MT^2 - U)^3), ((-I)*MT^2*Pi)/
     (-MH^2 + 4*MT^2 + U)^2}, 0, 4, 1], 
 (2*MH^2*T*(MH^2 - T - U)*DiscB[MH^2, MT, MT] + 
   2*T*(MH^2 - U)*(-MH^2 + T + U)*DiscB[T, MT, MT] + 
   2*T*U*(-MH^2 + T + U)*DiscB[U, MT, MT] - 
   T*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarC0[0, 0, T, MT, MT, MT] - (MH^2 - U)*U*(2*MH^2*MT^2 + T*U - 
     2*MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - U)*(2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
     MH^2*(T*U + MT^2*(6*T + 4*U)))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (8*T*(MH^2 - U)*(-MH^2 + T + U)^2)}
