(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(2*(MH^2 - U)), 
 PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Sqrt[MH^4 - 4*MH^2*MT^2]*U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
       (2*MT^2)] - MH^2*Sqrt[U*(-4*MT^2 + U)]*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(MH^2*(MH^2 - U)*U), 
 PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-(Sqrt[MH^4 - 4*MH^2*MT^2]*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]) + 
    MH^2*Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
       (2*MT^2)])/(2*MH^2*(MH^2 - U)*U), 
 PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (U*(-MH^2 + U) - Sqrt[MH^4 - 4*MH^2*MT^2]*U*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MT^2*U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    MH^2*Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
       (2*MT^2)] + MT^2*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
      2)/(2*(MH^2 - U)^2*U), PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (3*MH^2 + MH^2/Eps - 3*U - U/Eps + 
    Sqrt[MH^4 - 4*MH^2*MT^2]*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
       (2*MT^2)] + MT^2*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
        (2*MT^2)]^2 + (MH^2 - U)*Log[Mu^2/MT^2] - 
    Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
       (2*MT^2)] - MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
   (4*(MH^2 - U)), PVC[0, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
   (2*(MH^2 - S)), PVC[0, 1, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Sqrt[MH^4 - 4*MH^2*MT^2]*S*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MH^2*Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
       (2*MT^2)])/(MH^2*(MH^2 - S)*S), 
 PVC[0, 1, 1, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (S*(-MH^2 + S) - Sqrt[MH^4 - 4*MH^2*MT^2]*S*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MT^2*S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    MH^2*Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
       (2*MT^2)] + MT^2*S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
      2)/(2*(MH^2 - S)^2*S), PVC[0, 2, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (-(Sqrt[MH^4 - 4*MH^2*MT^2]*S*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]) + 
    MH^2*Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
       (2*MT^2)])/(2*MH^2*(MH^2 - S)*S), 
 PVC[1, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (3*MH^2 + MH^2/Eps - 3*S - S/Eps + Sqrt[MH^4 - 4*MH^2*MT^2]*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    MT^2*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    (MH^2 - S)*Log[Mu^2/MT^2] - Sqrt[S*(-4*MT^2 + S)]*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
    MT^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
   (4*(MH^2 - S)), PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
   (2*(MH^2 - T)), PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Sqrt[MH^4 - 4*MH^2*MT^2]*T*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MH^2*Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)])/(MH^2*(MH^2 - T)*T), 
 PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (T*(-MH^2 + T) - Sqrt[MH^4 - 4*MH^2*MT^2]*T*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MT^2*T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    MH^2*Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)] + MT^2*T*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
      2)/(2*(MH^2 - T)^2*T), PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (-(Sqrt[MH^4 - 4*MH^2*MT^2]*T*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]) + 
    MH^2*Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)])/(2*MH^2*(MH^2 - T)*T), 
 PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (3*MH^2 + MH^2/Eps - 3*T - T/Eps + Sqrt[MH^4 - 4*MH^2*MT^2]*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    MT^2*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    (MH^2 - T)*Log[Mu^2/MT^2] - Sqrt[T*(-4*MT^2 + T)]*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(4*(MH^2 - T))}
