(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVD[0, 0, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]], PVD[0, 0, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> ScalarD0[0, 0, 0, MH^2, S, U, 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]], 
 PVD[0, 0, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]], PVD[0, 0, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-(S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2) + 
    MH^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    S*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    S*T*(-MH^2 + T)*ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]])/(2*S*(MH^2 - T)*(-MH^2 + S + T)), 
 PVD[0, 0, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
     MH^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     S*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - S)*(MH^2 - U)*U*ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(2*(MH^2 - S)*(MH^2 - S - U)*U), 
 PVD[0, 0, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
     MH^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     T*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - T)*(MH^2 - U)*U*ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(2*(MH^2 - T)*(MH^2 - T - U)*U), 
 PVD[0, 0, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    MH^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    S*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (MH^2 - S)*S*T*ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]])/(2*(MH^2 - S)*(MH^2 - S - T)*T), 
 PVD[0, 0, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    MH^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    S*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (MH^2 - S)*S*U*ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]])/(2*(MH^2 - S)*(MH^2 - S - U)*U), 
 PVD[0, 0, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    MH^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    T*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (MH^2 - T)*T*(MH^2 - U)*ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(2*T*(MH^2 - U)*(-MH^2 + T + U)), 
 PVD[0, 0, 1, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Sqrt[MH^4 - 4*MH^2*MT^2]*(2*MH^2 - S - T)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
    (MH^2*(MH^2 - S)*(MH^2 - T)*(MH^2 - S - T)) - 
   (Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
    (S*(-MH^2 + S)*(-MH^2 + S + T)) - 
   Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2/
    (4*(-MH^2 + S + T)^2) + 
   (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
    (4*(-MH^2 + S + T)^2) - (Sqrt[T*(-4*MT^2 + T)]*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/
    (T*(-MH^2 + T)*(-MH^2 + S + T)) - 
   Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2/
    (4*(-MH^2 + S + T)^2) + 
   (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
    (4*(-MH^2 + S + T)^2) + ((2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(2*(-MH^2 + S + T)^2), 
 PVD[0, 0, 1, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (8*(MH^2 - S)*S*U*(-MH^2 + S + U)^2 + 
    4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - 2*U)*(MH^2 - S - U)*U*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    4*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*(-MH^4 + S*U + MH^2*(S + U))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
    (MH^2 - S)^2*S*(MH^2 - U)*U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)]^2 + S*U*(MH^6 + S^2*U - MH^4*(2*S + 3*U) + 
      MH^2*(S^2 + 2*S*U + 2*U^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
    4*(MH^2 - S)^2*S*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
    (MH^2 - S)^2*S*(MH^2 - U)*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)]^2 - (MH^2 - S)^2*S*(MH^2 - U)*U*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*(MH^2 - U)*U*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S)^2*S*U^2*(-MH^2 + S + U)^2), 
 PVD[0, 0, 1, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-8*T*U*(-MH^2 + T + U)^2 + 4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*
     (MH^2 - T - U)*U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
       (2*MT^2)] + 4*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - U)*U*(-MH^2 + T + U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    T^2*U^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    T^2*U^2*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    4*(MH^2 - T)*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    T^2*U^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    T^2*U^2*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*T*U*(2*MH^6*MT^2 - 4*MH^4*MT^2*(T + U) + 
      2*MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + 2*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*U^2*(-MH^2 + T + U)^2), 
 PVD[0, 0, 1, 2, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((2*(-MH^2 + S + T)^2)/(MH^2 - T) + 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*(MH^6 - MH^4*(S - 2*T) + 
       T*(S + T)^2 - 2*MH^2*T*(S + 2*T))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     ((MH^2 - S)*(MH^3 - MH*T)^2) + (2*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - T)*
      (-MH^2 + S + T)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
     (S*(-MH^2 + S)) + ((-(MH^2*MT^2) - S*T + MT^2*(S + T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/S + 
    ((MH^2*MT^2 + S*T - MT^2*(S + T))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/S - 
    (2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*(-3*MH^2 + S + 3*T)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/(MH^2 - T)^2 + 
    ((-(MH^2*MT^2) - S*T + MT^2*(S + T))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/S + 
    ((-(MH^6*MT^2) + S*T^3 + MT^2*(2*S^3 + 6*S^2*T + 5*S*T^2 + T^3) + 
       MH^4*(S*T + MT^2*(5*S + 3*T)) - 
       MH^2*(2*S*T^2 + MT^2*(6*S^2 + 10*S*T + 3*T^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/
     (S*(MH^2 - T)^2) + 2*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S - T)^3), 
 PVD[0, 0, 1, 2, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -((2*U*(-MH^2 + S + U)^2*(8*MH^4 + S*(3*S + 2*U) - MH^2*(11*S + 8*U)))/
      (MH^2 - S)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*
       (3*MH^6 + 2*S^2*U - 3*MH^4*(2*S + 3*U) + MH^2*(3*S^2 + 7*S*U + 6*U^2))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      (MH^2 - S)^3 - (2*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
       (3*MH^8 + S^2*(S - U)*U - 6*MH^6*(S + U) + MH^2*S*U*(3*S + 4*U) + 
        MH^4*(3*S^2 + 2*S*U + 3*U^2))*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
      (S*(-MH^2 + S)^3) + ((MH^2 - U)^2*(MH^2*MT^2 + S*U - MT^2*(S + U))*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/S + 
     ((MH^12*MT^2 + S^3*U^2*(S*U - MT^2*(S + U)) + 
        MH^10*(S*U - MT^2*(4*S + 3*U)) + MH^8*(-(S*U*(3*S + 4*U)) + 
          MT^2*(6*S^2 + 11*S*U + 3*U^2)) - 
        MH^6*(-(S*U*(3*S^2 + 6*S*U + 5*U^2)) + MT^2*(4*S^3 + 15*S^2*U + 
            14*S*U^2 + U^3)) - MH^2*S*U*(S^2*U*(2*S + 3*U) + 
          MT^2*(2*S^3 + 6*S^2*U + 9*S*U^2 + 4*U^3)) + 
        MH^4*S*(-(U*(S^3 + 3*S*U^2 + 2*U^3)) + MT^2*(S^3 + 9*S^2*U + 
            18*S*U^2 + 11*U^3)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
      ((MH^2 - S)^3*S) + 2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
      (-3*MH^2 + S + 3*U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)] + ((-(MH^6*MT^2) + S*U^3 + MT^2*(2*S^3 + 6*S^2*U + 
          5*S*U^2 + U^3) + MH^4*(S*U + MT^2*(5*S + 3*U)) - 
        MH^2*(2*S*U^2 + MT^2*(6*S^2 + 10*S*U + 3*U^2)))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/S - 
     ((MH^2 - U)^2*(MH^2*MT^2 + S*U - MT^2*(S + U))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/S - 
     2*(MH^2 - U)^2*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
      ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(4*(MH^2 - S - U)^3*U^2), 
 PVD[0, 0, 1, 2, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-2*T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 3*T + 2*U) - 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*
      (MH^4 + 2*T*U - MH^2*(T + U))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/(MH^2 - T) + 
    (2*Sqrt[T*(-4*MT^2 + T)]*U*(-MH^2 + T + U)*(MH^6 + T*(T - U)*U + 
       MH^2*U*(2*T + U) - MH^4*(T + 2*U))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/(-MH^2 + T) - 
    (MH^2 - U)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    ((MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 3*MH^6*MT^2*(T + U)^2 - 
       MH^2*T^2*U^2*(-MT^2 + T + U) + T^2*U^2*(T*U - MT^2*(T + U)) - 
       MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/(MH^2 - T) - 
    2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(MH^4 - MH^2*(2*T + U) + 
      T*(T + 3*U))*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
    (MH^8*MT^2 - MH^6*MT^2*(2*T + 3*U) + MH^4*MT^2*(-T^2 + 3*T*U + 3*U^2) + 
      MH^2*(T^2*U^2 + MT^2*(4*T^3 + 6*T^2*U - U^3)) - 
      T*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (MH^2 - U)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*T*(MH^2 - U)*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3*U^2), 
 PVD[0, 0, 2, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(8*MH^2*(MH^2 - S)*T*(-MH^2 + S + T)^2 + 
     4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*(MH^2*(S - T) - S*(S + T))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
     4*MH^2*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*(-MH^2 + S + 2*T)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
     MH^2*(MH^2 - S)^2*S*T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
       2 + MH^2*S*T*(MH^4 + S^2 + 4*S*T + 2*T^2 - 2*MH^2*(S + 2*T))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
     4*(MH^3 - MH*S)^2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
     MH^2*(MH^2 - S)^2*S*T*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
       2 - S*(MH^3 - MH*S)^2*T*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
     2*S*(MH^3 - MH*S)^2*T*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
      ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(4*MH^2*(MH^2 - S)^2*T^2*(-MH^2 + S + T)^2), 
 PVD[0, 0, 2, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(8*MH^2*(MH^2 - S)*U*(-MH^2 + S + U)^2 + 
     4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*(MH^2*(S - U) - S*(S + U))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
     4*MH^2*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*(-MH^2 + S + 2*U)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
     MH^2*(MH^2 - S)^2*S*U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
       2 + MH^2*S*U*(MH^4 + S^2 + 4*S*U + 2*U^2 - 2*MH^2*(S + 2*U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
     4*(MH^3 - MH*S)^2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
     MH^2*(MH^2 - S)^2*S*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
       2 - S*(MH^3 - MH*S)^2*U*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
     2*S*(MH^3 - MH*S)^2*U*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
      ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(4*MH^2*(MH^2 - S)^2*U^2*(-MH^2 + S + U)^2), 
 PVD[0, 0, 2, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (8*T*(MH^2 - 2*U)*(MH^2 - U)*U*(-MH^2 + T + U)^2 - 
    4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*(MH^4 - T*U - MH^2*(T + U))*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - U)^2*U*(-MH^2 + T + U)*(-MH^2 + T + 2*U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    T*(MH^2 - U)^2*U*(MH^4 + T^2 + 4*T*U + 2*U^2 - 2*MH^2*(T + 2*U))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (MH^2 - T)^2*T*(MH^2 - U)^2*U*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    4*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(-MH^6 + T*U^2 + MH^4*(T + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - T)^2*T*(MH^2 - U)^2*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)]^2 - T*U*(MH^8 - T^2*U^2 - 2*MH^6*(T + U) + 
      2*MH^2*T*U*(T + U) + MH^4*(T^2 + U^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*(MH^2 - T)^2*T*(MH^2 - U)^2*U*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - U)^2*U^2*(-MH^2 + T + U)^2), 
 PVD[0, 0, 3, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((2*T*(-MH^2 + S + T)^2*(2*MH^4 - MH^2*(9*S + 2*T) + S*(7*S + 8*T)))/
     (MH^2 - S)^2 - (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*
      (MH^6*T + 3*S^2*(S + T)^2 + MH^4*(3*S^2 - 7*S*T - T^2) + 
       MH^2*(-6*S^3 + 4*S*T^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2*(MH^2 - S)^3) - (2*S*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
      (3*MH^4 + 3*S^2 + 11*S*T + 6*T^2 - MH^2*(6*S + 11*T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/(MH^2 - S)^3 + 
    S*(-(MH^2*MT^2) - S*T + MT^2*(S + T))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    (S*(MH^8*MT^2 - S*T*(S^3 + 6*S^2*T + 6*S*T^2 + 2*T^3) + 
       MT^2*(S^4 + S^3*T - 6*S^2*T^2 - 10*S*T^3 - 4*T^4) + 
       MH^6*(S*T - MT^2*(4*S + T)) + 3*MH^4*(-(S*T*(S + 2*T)) + 
         MT^2*(2*S^2 + S*T - 2*T^2)) + MH^2*(3*S*T*(S^2 + 4*S*T + 2*T^2) + 
         MT^2*(-4*S^3 - 3*S^2*T + 12*S*T^2 + 10*T^3)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/(MH^2 - S)^3 + 
    2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*(-MH^2 + 3*S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    S*(-(MH^2*MT^2) - S*T + MT^2*(S + T))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    S*(MH^2*MT^2 + S*T - MT^2*(S + T))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    2*S^2*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, 
      T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*(MH^2 - S - T)^3*T^2), PVD[0, 0, 2, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*(-MH^2 + S + T)^2)/(MH^2 - S) + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*
      (MH^2 - S - T)*(MH^6 + MH^4*(2*S - T) + S*(S + T)^2 - 
       2*MH^2*S*(2*S + T))*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
        (2*MT^2)])/((MH^3 - MH*S)^2*(MH^2 - T)) - 
    (2*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S + T)*(-3*MH^2 + 3*S + T)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/(MH^2 - S)^2 + 
    ((-(MH^2*MT^2) - S*T + MT^2*(S + T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/T - 
    ((MH^6*MT^2 - S^3*T - MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 2*T^3) - 
       MH^4*(S*T + MT^2*(3*S + 5*T)) + 
       MH^2*(2*S^2*T + MT^2*(3*S^2 + 10*S*T + 6*T^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
     ((MH^2 - S)^2*T) + (2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 - S + T)*
      (-MH^2 + S + T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/
     (T*(-MH^2 + T)) + ((-(MH^2*MT^2) - S*T + MT^2*(S + T))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T + 
    ((MH^2*MT^2 + S*T - MT^2*(S + T))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/T + 
    2*S*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*(MH^2 - S - T)^3), 
 PVD[0, 0, 2, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((-2*U*(-MH^2 + S + U)^2*(-3*MH^4 + MH^2*(S + 3*U) + S*(2*S + 3*U)))/
     (MH^2 - S)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*
      (MH^4*(3*S - U) + MH^2*(-6*S^2 - 9*S*U + U^2) + 
       S*(3*S^2 + 10*S*U + 5*U^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - S)^3 + (2*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
      (3*MH^6 + S*U*(3*S + U) - 2*MH^4*(3*S + 4*U) + 
       MH^2*(3*S^2 + 5*S*U + 5*U^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)])/(MH^2 - S)^3 + (MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    ((MH^10*MT^2 + MH^8*(S*U - 2*MT^2*(2*S + U)) + 
       MH^6*(-(S*U*(3*S + 5*U)) + MT^2*(6*S^2 + 7*S*U - U^2)) + 
       MH^4*(3*S*U*(S^2 + 3*S*U + 2*U^2) - MT^2*(4*S^3 + 9*S^2*U + 3*S*U^2 - 
           4*U^3)) - S*U*(S^3*U + MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 2*U^3)) + 
       MH^2*(-(S*U*(S^3 + 3*S^2*U + 6*S*U^2 + 2*U^3)) + 
         MT^2*(S^4 + 5*S^3*U + 9*S^2*U^2 + 2*S*U^3 - 2*U^4)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/(MH^2 - S)^3 + 
    2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - S + U)*(-MH^2 + S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
    (MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*S*(MH^2 - U)*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S - U)^3*U^2), 
 PVD[0, 0, 2, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-2*T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 2*T + 3*U) - 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*
      (MH^4 + 2*T*U - MH^2*(T + U))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/(MH^2 - U) + 
    2*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - T - U)*U*(MH^4 + U*(3*T + U) - 
      MH^2*(T + 2*U))*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^8*MT^2 - MH^6*MT^2*(3*T + 2*U) + MH^4*MT^2*(3*T^2 + 3*T*U - U^2) - 
      U*(T^3*U + MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3)) + 
      MH^2*(T^2*U^2 + MT^2*(-T^3 + 6*T*U^2 + 4*U^3)))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (MH^2 - T)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    (2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(MH^6 + T*U*(-T + U) - 
       MH^4*(2*T + U) + MH^2*T*(T + 2*U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(-MH^2 + U) - 
    (MH^2 - T)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    ((MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 3*MH^6*MT^2*(T + U)^2 - 
       MH^2*T^2*U^2*(-MT^2 + T + U) + T^2*U^2*(T*U - MT^2*(T + U)) - 
       MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U) + 
    2*(MH^2 - T)*T*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3*U^2), 
 PVD[0, 0, 3, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((2*U*(-MH^2 + S + U)^2*(2*MH^4 - MH^2*(9*S + 2*U) + S*(7*S + 8*U)))/
     (MH^2 - S)^2 - (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*
      (MH^6*U + 3*S^2*(S + U)^2 + MH^4*(3*S^2 - 7*S*U - U^2) + 
       MH^2*(-6*S^3 + 4*S*U^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2*(MH^2 - S)^3) - (2*S*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
      (3*MH^4 + 3*S^2 + 11*S*U + 6*U^2 - MH^2*(6*S + 11*U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/(MH^2 - S)^3 + 
    S*(-(MH^2*MT^2) - S*U + MT^2*(S + U))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    (S*(MH^8*MT^2 - S*U*(S^3 + 6*S^2*U + 6*S*U^2 + 2*U^3) + 
       MT^2*(S^4 + S^3*U - 6*S^2*U^2 - 10*S*U^3 - 4*U^4) + 
       MH^6*(S*U - MT^2*(4*S + U)) + 3*MH^4*(-(S*U*(S + 2*U)) + 
         MT^2*(2*S^2 + S*U - 2*U^2)) + MH^2*(3*S*U*(S^2 + 4*S*U + 2*U^2) + 
         MT^2*(-4*S^3 - 3*S^2*U + 12*S*U^2 + 10*U^3)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/(MH^2 - S)^3 + 
    2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*(-MH^2 + 3*S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
    S*(-(MH^2*MT^2) - S*U + MT^2*(S + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    S*(MH^2*MT^2 + S*U - MT^2*(S + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*S^2*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, 
      U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*(MH^2 - S - U)^3*U^2), PVD[0, 0, 3, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*T*U*(-MH^2 + T + U)^2*(-6*MH^6 + U^2*(13*T + 14*U) + 
       MH^4*(6*T + 31*U) - MH^2*U*(25*T + 39*U)))/(MH^2 - U)^2 + 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*(3*MH^8 - 2*T^2*U^2 - 
       6*MH^6*(T + U) + 5*MH^2*T*U*(T + U) + MH^4*(3*T^2 + T*U + 3*U^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - U)^3 + 2*Sqrt[T*(-4*MT^2 + T)]*U*(-MH^2 + T + U)*
     (3*MH^4 + 3*T^2 + 11*T*U + 6*U^2 - MH^2*(6*T + 11*U))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    (MH^8*MT^2 - T*U*(T^3 + 6*T^2*U + 6*T*U^2 + 2*U^3) + 
      MT^2*(T^4 + T^3*U - 6*T^2*U^2 - 10*T*U^3 - 4*U^4) + 
      MH^6*(T*U - MT^2*(4*T + U)) + 3*MH^4*(-(T*U*(T + 2*U)) + 
        MT^2*(2*T^2 + T*U - 2*U^2)) + MH^2*(3*T*U*(T^2 + 4*T*U + 2*U^2) + 
        MT^2*(-4*T^3 - 3*T^2*U + 12*T*U^2 + 10*U^3)))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    (MH^2 - T)^3*(MH^2*MT^2 + T*U - MT^2*(T + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    (2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(-3*MH^10 + 6*MH^4*T*U^2 + 
       6*MH^8*(T + U) - 3*MH^6*(T + U)^2 + T*U^3*(3*T + U) - 
       MH^2*T*U^2*(6*T + 7*U))*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)])/(MH^2 - U)^3 + (MH^2 - T)^3*(MH^2*MT^2 + T*U - 
      MT^2*(T + U))*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    ((MH^14*MT^2 + MH^12*(T*U - 4*MT^2*(T + U)) + 
       T^3*U^3*(-(T*U) + MT^2*(T + U)) + 3*MH^10*(-(T*U*(T + U)) + 
         MT^2*(2*T^2 + 5*T*U + 2*U^2)) + MH^2*T^2*U^2*(3*T*U*(T + U) - 
         MT^2*(3*T^2 + 7*T*U + 3*U^2)) - 3*MH^4*T*U*
        (T*U*(T^2 + 3*T*U + U^2) + MT^2*(T^3 + T^2*U + T*U^2 + U^3)) + 
       MH^8*(3*T*U*(T^2 + T*U + U^2) - MT^2*(4*T^3 + 21*T^2*U + 21*T*U^2 + 
           4*U^3)) + MH^6*(-(T*U*(T^3 - 3*T^2*U - 3*T*U^2 + U^3)) + 
         MT^2*(T^4 + 13*T^3*U + 21*T^2*U^2 + 13*T*U^3 + U^4)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U)^3 - 
    2*(MH^2 - T)^3*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3*U^2), 
 PVD[0, 1, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
     MH^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     S*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     (MH^2 - S)*(MH^2 - T)*T*ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(2*(MH^2 - S)*(MH^2 - S - T)*T), 
 PVD[0, 1, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (-(S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2) + 
    MH^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    S*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    S*U*(-MH^2 + U)*ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]])/(2*S*(MH^2 - U)*(-MH^2 + S + U)), 
 PVD[0, 1, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (-(T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2) + 
    MH^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    T*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    T*U*(-MH^2 + U)*ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]])/(2*T*(MH^2 - U)*(-MH^2 + T + U)), 
 PVD[0, 1, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
     (MH^2 - S)*S*T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
     (MH^2 - S)*S*T*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
        2) + 4*(MH^2 - S)*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
     (MH^2 - S)*S*T*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     (MH^2 - S)*S*T*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
        2) - 2*(MH^2 - S)*T*(2*MH^4*MT^2 - S^2*T - 2*MH^2*MT^2*(3*S + 2*T) + 
       2*MT^2*(2*S^2 + 3*S*T + T^2))*ScalarD0[0, 0, 0, MH^2, S, T, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*(MH^2 - S)*S*T*(-MH^2 + S + T)^2), 
 PVD[0, 1, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
     (MH^2 - S)*S*U*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
     (MH^2 - S)*S*U*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
        2) + 4*(MH^2 - S)*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
     (MH^2 - S)*S*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - S)*S*U*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
        2) - 2*(MH^2 - S)*U*(2*MH^4*MT^2 - S^2*U - 2*MH^2*MT^2*(3*S + 2*U) + 
       2*MT^2*(2*S^2 + 3*S*U + U^2))*ScalarD0[0, 0, 0, MH^2, S, U, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*(MH^2 - S)*S*U*(-MH^2 + S + U)^2), 
 PVD[0, 1, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - T - U)*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*Sqrt[T*(-4*MT^2 + T)]*U*(-MH^2 + T + U)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
     (MH^2 - T)*T*U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     (MH^2 - T)*T*U*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
        2) + 4*(MH^2 - T)*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
     (MH^2 - T)*T*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - T)*T*U*(Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
          (2*MT^2)]^2 - Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
        2) - 2*(MH^2 - T)*U*(2*MH^4*MT^2 - T^2*U - 2*MH^2*MT^2*(3*T + 2*U) + 
       2*MT^2*(2*T^2 + 3*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*(MH^2 - T)*T*U*(-MH^2 + T + U)^2), 
 PVD[0, 1, 0, 2, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(2*S*(-MH^2 + S + T)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - T)*
       (-S^2 - T^2 + MH^2*(S + T))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      ((MH^2 - S)*(MH^2 - T)) + (2*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - T)*
       (-MH^2 + S + T)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
      (-MH^2 + S) + (MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
       MT^2*(2*S^2 + 3*S*T + T^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
         (2*MT^2)]^2 - (MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
       MT^2*(2*S^2 + 3*S*T + T^2))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
     (2*S*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 - S + T)*(-MH^2 + S + T)*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/(-MH^2 + T) + 
     (MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + MT^2*(2*S^2 + 3*S*T + T^2))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     ((MH^6*MT^2 - S^2*T^2 - 3*MH^4*MT^2*(S + T) - 
        MT^2*(2*S^3 + 4*S^2*T + 3*S*T^2 + T^3) + 
        MH^2*(S^2*T + MT^2*(4*S^2 + 6*S*T + 3*T^2)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/(MH^2 - T) - 
     2*S*T*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(5*S + 2*T) + 
       MT^2*(4*S^2 + 5*S*T + T^2))*ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*S^2*(MH^2 - S - T)^3), 
 PVD[0, 1, 0, 2, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*(MH^2 - S)*S^2*U*(-MH^2 + S + U)^2 + 
    2*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^4 - 2*S^2 + MH^2*(S - U))*(MH^2 - S - U)*
     U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    2*S*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*(-2*MH^4 + S*(S - U) + 
      MH^2*(S + 2*U))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
    (MH^2 - S)^2*(MH^2 - U)*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
      MT^2*(2*S^2 + 3*S*U + U^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)]^2 + (MH^10*MT^2 - MH^8*MT^2*(5*S + 3*U) + 
      S^2*U*(-(S^2*U) + MT^2*(2*S^2 + 3*S*U + U^2)) + 
      MH^6*(S^2*U + 3*MT^2*(3*S^2 + 4*S*U + U^2)) - 
      MH^4*(S^2*U*(2*S + U) + MT^2*(7*S^3 + 13*S^2*U + 9*S*U^2 + U^3)) + 
      MH^2*(S^3*U*(S + 2*U) + MT^2*S*(2*S^3 + 2*S^2*U + 3*S*U^2 + 2*U^3)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - S + U)*(-MH^2 + S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - S)^2*(MH^6*MT^2 - S^2*U^2 - 3*MH^4*MT^2*(S + U) - 
      MT^2*(2*S^3 + 4*S^2*U + 3*S*U^2 + U^3) + 
      MH^2*(S^2*U + MT^2*(4*S^2 + 6*S*U + 3*U^2)))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (MH^2 - S)^2*(MH^2 - U)*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
      MT^2*(2*S^2 + 3*S*U + U^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*(MH^2 - U)*U*(MH^4*MT^2 - S^2*U - 
      MH^2*MT^2*(5*S + 2*U) + MT^2*(4*S^2 + 5*S*U + U^2))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S)^2*S^2*(MH^2 - S - U)^3*U), 
 PVD[0, 1, 0, 2, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*(MH^2 - T)*T^2*U*(-MH^2 + T + U)^2 + 
    2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^4 - 2*T^2 + MH^2*(T - U))*(MH^2 - T - U)*
     U*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    2*T*Sqrt[T*(-4*MT^2 + T)]*U*(-MH^2 + T + U)*(-2*MH^4 + T*(T - U) + 
      MH^2*(T + 2*U))*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^2 - T)^2*(MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)]^2 + (MH^10*MT^2 - MH^8*MT^2*(5*T + 3*U) + 
      T^2*U*(-(T^2*U) + MT^2*(2*T^2 + 3*T*U + U^2)) + 
      MH^6*(T^2*U + 3*MT^2*(3*T^2 + 4*T*U + U^2)) - 
      MH^4*(T^2*U*(2*T + U) + MT^2*(7*T^3 + 13*T^2*U + 9*T*U^2 + U^3)) + 
      MH^2*(T^3*U*(T + 2*U) + MT^2*T*(2*T^3 + 2*T^2*U + 3*T*U^2 + 2*U^3)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    2*(MH^2 - T)^2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - T + U)*(-MH^2 + T + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - T)^2*(MH^6*MT^2 - T^2*U^2 - 3*MH^4*MT^2*(T + U) - 
      MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) + 
      MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (MH^2 - T)^2*(MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*(MH^2 - T)^2*T*(MH^2 - U)*U*(MH^4*MT^2 - T^2*U - 
      MH^2*MT^2*(5*T + 2*U) + MT^2*(4*T^2 + 5*T*U + U^2))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - T)^2*T^2*(MH^2 - T - U)^3*U), 
 PVD[0, 1, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (8*(MH^2 - S)*S*T*(-MH^2 + S + T)^2 + 
    4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - 2*T)*(MH^2 - S - T)*T*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    4*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*(-MH^4 + S*T + MH^2*(S + T))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
    (MH^2 - S)^2*S*(MH^2 - T)*T*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)]^2 + S*T*(MH^6 + S^2*T - MH^4*(2*S + 3*T) + 
      MH^2*(S^2 + 2*S*T + 2*T^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
    4*(MH^2 - S)^2*S*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^2 - S)^2*S*(MH^2 - T)*T*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)]^2 - (MH^2 - S)^2*S*(MH^2 - T)*T*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*(MH^2 - T)*T*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S)^2*S*T^2*(-MH^2 + S + T)^2), 
 PVD[0, 1, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Sqrt[MH^4 - 4*MH^2*MT^2]*(2*MH^2 - S - U)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
    (MH^2*(MH^2 - S)*(MH^2 - U)*(MH^2 - S - U)) - 
   (Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
    (S*(-MH^2 + S)*(-MH^2 + S + U)) - 
   Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2/
    (4*(-MH^2 + S + U)^2) + 
   (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
    (4*(-MH^2 + S + U)^2) - (Sqrt[U*(-4*MT^2 + U)]*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/
    (U*(-MH^2 + U)*(-MH^2 + S + U)) - 
   Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2/
    (4*(-MH^2 + S + U)^2) + 
   (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
    (4*(-MH^2 + S + U)^2) + ((2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(2*(-MH^2 + S + U)^2), 
 PVD[0, 1, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (8*T*(MH^2 - U)*U*(-MH^2 + T + U)^2 + 
    4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - 2*T - U)*(MH^2 - T - U)*U*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    4*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - U)^2*U*(-MH^2 + T + U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^2 - T)*T*(MH^2 - U)^2*U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)]^2 - (MH^2 - T)*T*(MH^2 - U)^2*U*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    4*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(-MH^4 + T*U + MH^2*(T + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
    (MH^2 - T)*T*(MH^2 - U)^2*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)]^2 + T*U*(MH^6 + T*U^2 - MH^4*(3*T + 2*U) + 
      MH^2*(2*T^2 + 2*T*U + U^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) - 
    2*(MH^2 - T)*T*(MH^2 - U)^2*U*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - U)^2*U*(-MH^2 + T + U)^2), 
 PVD[0, 1, 1, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((-4*(-MH^2 + S + U)^2)/(MH^2 - S) - 
    (4*Sqrt[MH^4 - 4*MH^2*MT^2]*(3*MH^2 - 3*S - U)*(MH^2 - S - U)*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - S)^2 + (4*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S + U)*
      (-MH^4 + 2*S^2 + MH^2*(-S + U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/((MH^2 - S)^2*S) + 
    (((S - U)*U + MH^2*(2*MT^2 + U) - 2*MT^2*(S + U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/U + 
    ((MH^6*(2*MT^2 - U) + S^2*U*(-S + U) - 2*MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 
         2*U^3) + MH^4*(U*(S + U) - 2*MT^2*(3*S + 5*U)) + 
       MH^2*(S*(S - 2*U)*U + 2*MT^2*(3*S^2 + 10*S*U + 6*U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
     ((MH^2 - S)^2*U) - (8*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/U + 
    (((S - U)*U + MH^2*(2*MT^2 + U) - 2*MT^2*(S + U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U + 
    ((-(MH^2*(2*MT^2 + U)) + U*(-S + U) + 2*MT^2*(S + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/U - 
    2*(2*MH^4*MT^2 + S*(S - U)*U + MH^2*(2*MT^2*(S - 2*U) + S*U) - 
      2*MT^2*(2*S^2 + S*U - U^2))*ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(8*(MH^2 - S - U)^3), 
 PVD[0, 1, 1, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*T*(-MH^2 + T + U)^2 + 
    (4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*(MH^2 + T - U)*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/(MH^2 - U) + 
    8*T*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T + U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    ((-2*MH^6*MT^2 + T^2*(T - U)*U + 2*MH^4*MT^2*(3*T + 4*U) + 
       2*MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3) - 
       MH^2*(T^2*U + 2*MT^2*(3*T^2 + 9*T*U + 5*U^2)))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/U - 
    ((2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
       T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
       MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/U - 
    (4*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(MH^4 + 2*T*U - MH^2*(T + U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(U*(-MH^2 + U)) + 
    ((2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
       T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
       MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U + 
    ((2*MH^8*MT^2 - 6*MH^6*MT^2*(T + U) + 
       T^2*U*((T - U)*U - 2*MT^2*(T + U)) + MH^4*(T^2*U + 6*MT^2*(T + U)^2) - 
       MH^2*(T^3*U + 2*MT^2*(T^3 + 2*T^2*U + 3*T*U^2 + U^3)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
     ((MH^2 - U)*U) + 2*T*(-2*MH^6*MT^2 + 4*MH^4*MT^2*(2*T + U) + 
      T*(T*U*(-T + U) + 2*MT^2*(2*T^2 + T*U - U^2)) + 
      MH^2*(T^2*U - 2*MT^2*(5*T^2 + 3*T*U + U^2)))*ScalarD0[0, 0, 0, MH^2, T, 
      U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (8*T^2*(MH^2 - T - U)^3), PVD[0, 1, 1, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((-4*(-MH^2 + S + T)^2)/(MH^2 - S) - 
    (4*Sqrt[MH^4 - 4*MH^2*MT^2]*(3*MH^2 - 3*S - T)*(MH^2 - S - T)*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - S)^2 + (4*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S + T)*
      (-MH^4 + 2*S^2 + MH^2*(-S + T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/((MH^2 - S)^2*S) + 
    (((S - T)*T + MH^2*(2*MT^2 + T) - 2*MT^2*(S + T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/T + 
    ((MH^6*(2*MT^2 - T) + S^2*T*(-S + T) - 2*MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 
         2*T^3) + MH^4*(T*(S + T) - 2*MT^2*(3*S + 5*T)) + 
       MH^2*(S*(S - 2*T)*T + 2*MT^2*(3*S^2 + 10*S*T + 6*T^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
     ((MH^2 - S)^2*T) - (8*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/T + 
    (((S - T)*T + MH^2*(2*MT^2 + T) - 2*MT^2*(S + T))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/T + 
    ((-(MH^2*(2*MT^2 + T)) + T*(-S + T) + 2*MT^2*(S + T))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/T - 
    2*(2*MH^4*MT^2 + S*(S - T)*T + MH^2*(2*MT^2*(S - 2*T) + S*T) - 
      2*MT^2*(2*S^2 + S*T - T^2))*ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(8*(MH^2 - S - T)^3), 
 PVD[0, 1, 2, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((-2*T*(-MH^2 + S + T)^2*(-3*MH^4 + MH^2*(S + 3*T) + S*(2*S + 3*T)))/
     (MH^2 - S)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*
      (MH^4*(3*S - T) + MH^2*(-6*S^2 - 9*S*T + T^2) + 
       S*(3*S^2 + 10*S*T + 5*T^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - S)^3 + (2*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
      (3*MH^6 + S*T*(3*S + T) - 2*MH^4*(3*S + 4*T) + 
       MH^2*(3*S^2 + 5*S*T + 5*T^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)])/(MH^2 - S)^3 + (MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    ((MH^10*MT^2 + MH^8*(S*T - 2*MT^2*(2*S + T)) + 
       MH^6*(-(S*T*(3*S + 5*T)) + MT^2*(6*S^2 + 7*S*T - T^2)) + 
       MH^4*(3*S*T*(S^2 + 3*S*T + 2*T^2) - MT^2*(4*S^3 + 9*S^2*T + 3*S*T^2 - 
           4*T^3)) - S*T*(S^3*T + MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 2*T^3)) + 
       MH^2*(-(S*T*(S^3 + 3*S^2*T + 6*S*T^2 + 2*T^3)) + 
         MT^2*(S^4 + 5*S^3*T + 9*S^2*T^2 + 2*S*T^3 - 2*T^4)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/(MH^2 - S)^3 + 
    2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 - S + T)*(-MH^2 + S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    (MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    2*S*(MH^2 - T)*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S - T)^3*T^2), 
 PVD[0, 1, 2, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((2*(-MH^2 + S + U)^2)/(MH^2 - S) + 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*(MH^6 + MH^4*(2*S - U) + 
       S*(S + U)^2 - 2*MH^2*S*(2*S + U))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     ((MH^3 - MH*S)^2*(MH^2 - U)) - (2*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S + U)*
      (-3*MH^2 + 3*S + U)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
     (MH^2 - S)^2 + ((-(MH^2*MT^2) - S*U + MT^2*(S + U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/U - 
    ((MH^6*MT^2 - S^3*U - MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 2*U^3) - 
       MH^4*(S*U + MT^2*(3*S + 5*U)) + 
       MH^2*(2*S^2*U + MT^2*(3*S^2 + 10*S*U + 6*U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
     ((MH^2 - S)^2*U) + (2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - S + U)*
      (-MH^2 + S + U)*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/
     (U*(-MH^2 + U)) + ((-(MH^2*MT^2) - S*U + MT^2*(S + U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U + 
    ((MH^2*MT^2 + S*U - MT^2*(S + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/U + 
    2*S*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*(MH^2 - S - U)^3), 
 PVD[0, 1, 2, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -((-2*T*(-MH^2 + T + U)^2*(-8*MH^4 - U*(2*T + 3*U) + MH^2*(8*T + 11*U)))/
      (MH^2 - U)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*
       (3*MH^6 + 2*T*U^2 - 3*MH^4*(3*T + 2*U) + MH^2*(6*T^2 + 7*T*U + 3*U^2))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      (MH^2 - U)^3 + 2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T + U)*
      (-3*MH^2 + 3*T + U)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)] + ((-(MH^6*MT^2) + T^3*U + MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 
          2*U^3) + MH^4*(T*U + MT^2*(3*T + 5*U)) - 
        MH^2*(2*T^2*U + MT^2*(3*T^2 + 10*T*U + 6*U^2)))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/U - 
     ((MH^2 - T)^2*(MH^2*MT^2 + T*U - MT^2*(T + U))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/U - 
     (2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(3*MH^8 + T*U^2*(-T + U) - 
        6*MH^6*(T + U) + MH^2*T*U*(4*T + 3*U) + MH^4*(3*T^2 + 2*T*U + 3*U^2))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/
      (U*(-MH^2 + U)^3) + ((MH^2 - T)^2*(MH^2*MT^2 + T*U - MT^2*(T + U))*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/U + 
     ((MH^12*MT^2 + T^2*U^3*(T*U - MT^2*(T + U)) + 
        MH^10*(T*U - MT^2*(3*T + 4*U)) + MH^8*(-(T*U*(4*T + 3*U)) + 
          MT^2*(3*T^2 + 11*T*U + 6*U^2)) + 
        MH^4*U*(-(T*(2*T^3 + 3*T^2*U + U^3)) + MT^2*(11*T^3 + 18*T^2*U + 
            9*T*U^2 + U^3)) - MH^2*T*U*(T*U^2*(3*T + 2*U) + 
          MT^2*(4*T^3 + 9*T^2*U + 6*T*U^2 + 2*U^3)) - 
        MH^6*(-(T*U*(5*T^2 + 6*T*U + 3*U^2)) + MT^2*(T^3 + 14*T^2*U + 
            15*T*U^2 + 4*U^3)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
      ((MH^2 - U)^3*U) - 2*(MH^2 - T)^2*T*(3*MH^2*MT^2 + T*U - 
       3*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3), 
 PVD[0, 2, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-8*MH^2*T*(MH^2 - U)*(-MH^2 + T + U)^2 + 
    4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*(MH^2*(T - U) + U*(T + U))*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T + U)*(MH^3 - MH*U)^2*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    MH^2*T*(MH^2 - U)^2*U*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^
      2 + T*U*(MH^3 - MH*U)^2*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    4*MH^2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(-MH^2 + 2*T + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    MH^2*T*(MH^2 - U)^2*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^
      2 - MH^2*T*U*(MH^4 + 2*T^2 + 4*T*U + U^2 - 2*MH^2*(2*T + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*T*U*(MH^3 - MH*U)^2*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*MH^2*T^2*(MH^2 - U)^2*(-MH^2 + T + U)^2), 
 PVD[0, 2, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*(MH^2 - S)*S^2*T*(-MH^2 + S + T)^2 + 
    2*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^4 - 2*S^2 + MH^2*(S - T))*(MH^2 - S - T)*
     T*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    2*S*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*(-2*MH^4 + S*(S - T) + 
      MH^2*(S + 2*T))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
    (MH^2 - S)^2*(MH^2 - T)*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
      MT^2*(2*S^2 + 3*S*T + T^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
        (2*MT^2)]^2 + (MH^10*MT^2 - MH^8*MT^2*(5*S + 3*T) + 
      S^2*T*(-(S^2*T) + MT^2*(2*S^2 + 3*S*T + T^2)) + 
      MH^6*(S^2*T + 3*MT^2*(3*S^2 + 4*S*T + T^2)) - 
      MH^4*(S^2*T*(2*S + T) + MT^2*(7*S^3 + 13*S^2*T + 9*S*T^2 + T^3)) + 
      MH^2*(S^3*T*(S + 2*T) + MT^2*S*(2*S^3 + 2*S^2*T + 3*S*T^2 + 2*T^3)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 - S + T)*(-MH^2 + S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    (MH^2 - S)^2*(MH^6*MT^2 - S^2*T^2 - 3*MH^4*MT^2*(S + T) - 
      MT^2*(2*S^3 + 4*S^2*T + 3*S*T^2 + T^3) + 
      MH^2*(S^2*T + MT^2*(4*S^2 + 6*S*T + 3*T^2)))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    (MH^2 - S)^2*(MH^2 - T)*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
      MT^2*(2*S^2 + 3*S*T + T^2))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    2*(MH^2 - S)^2*S*(MH^2 - T)*T*(MH^4*MT^2 - S^2*T - 
      MH^2*MT^2*(5*S + 2*T) + MT^2*(4*S^2 + 5*S*T + T^2))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S)^2*S^2*(MH^2 - S - T)^3*T), 
 PVD[0, 2, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(2*S*(-MH^2 + S + U)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - U)*
       (-S^2 - U^2 + MH^2*(S + U))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      ((MH^2 - S)*(MH^2 - U)) + (2*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - U)*
       (-MH^2 + S + U)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
      (-MH^2 + S) + (MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
       MT^2*(2*S^2 + 3*S*U + U^2))*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
         (2*MT^2)]^2 - (MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
       MT^2*(2*S^2 + 3*S*U + U^2))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
     (2*S*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - S + U)*(-MH^2 + S + U)*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(-MH^2 + U) + 
     (MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + MT^2*(2*S^2 + 3*S*U + U^2))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     ((MH^6*MT^2 - S^2*U^2 - 3*MH^4*MT^2*(S + U) - 
        MT^2*(2*S^3 + 4*S^2*U + 3*S*U^2 + U^3) + 
        MH^2*(S^2*U + MT^2*(4*S^2 + 6*S*U + 3*U^2)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U) - 
     2*S*U*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(5*S + 2*U) + 
       MT^2*(4*S^2 + 5*S*U + U^2))*ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*S^2*(MH^2 - S - U)^3), 
 PVD[0, 2, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(2*T*(-MH^2 + T + U)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*
       (-T^2 - U^2 + MH^2*(T + U))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      ((MH^2 - T)*(MH^2 - U)) + (2*T*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T - U)*
       (-MH^2 + T + U)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/
      (-MH^2 + T) + (MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
         (2*MT^2)]^2 - (MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
     (2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 - T + U)*(-MH^2 + T + U)*
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(-MH^2 + U) + 
     (MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + MT^2*(2*T^2 + 3*T*U + U^2))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     ((MH^6*MT^2 - T^2*U^2 - 3*MH^4*MT^2*(T + U) - 
        MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) + 
        MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U) - 
     2*T*U*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(5*T + 2*U) + 
       MT^2*(4*T^2 + 5*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3), 
 PVD[0, 2, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((-2*T*(-MH^2 + T + U)^2*(-3*MH^4 + MH^2*(3*T + U) + U*(3*T + 2*U)))/
     (MH^2 - U)^2 - (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*
      (MH^4*(T - 3*U) - U*(5*T^2 + 10*T*U + 3*U^2) + 
       MH^2*(-T^2 + 9*T*U + 6*U^2))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2 - U)^3 + 2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T - U)*(-MH^2 + T + U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    (MH^2 - T)*(MH^2*MT^2 + T*U - MT^2*(T + U))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    (MH^2 - T)*(MH^2*MT^2 + T*U - MT^2*(T + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    (2*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(3*MH^6 + T*U*(T + 3*U) - 
       2*MH^4*(4*T + 3*U) + MH^2*(5*T^2 + 5*T*U + 3*U^2))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(MH^2 - U)^3 + 
    (MH^2 - T)*(MH^2*MT^2 + T*U - MT^2*(T + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    ((MH^10*MT^2 + MH^8*(T*U - 2*MT^2*(T + 2*U)) - 
       MH^6*(T*U*(5*T + 3*U) + MT^2*(T^2 - 7*T*U - 6*U^2)) + 
       MH^4*(3*T*U*(2*T^2 + 3*T*U + U^2) + MT^2*(4*T^3 - 3*T^2*U - 9*T*U^2 - 
           4*U^3)) - T*U*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)) + 
       MH^2*(-(T*U*(2*T^3 + 6*T^2*U + 3*T*U^2 + U^3)) + 
         MT^2*(-2*T^4 + 2*T^3*U + 9*T^2*U^2 + 5*T*U^3 + U^4)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U)^3 - 
    2*(MH^2 - T)*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*T^2*(MH^2 - T - U)^3), 
 PVD[0, 2, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -((2*T*(-MH^2 + S + T)^2*(8*MH^4 + S*(3*S + 2*T) - MH^2*(11*S + 8*T)))/
      (MH^2 - S)^2 + (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*
       (3*MH^6 + 2*S^2*T - 3*MH^4*(2*S + 3*T) + MH^2*(3*S^2 + 7*S*T + 6*T^2))*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      (MH^2 - S)^3 - (2*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
       (3*MH^8 + S^2*(S - T)*T - 6*MH^6*(S + T) + MH^2*S*T*(3*S + 4*T) + 
        MH^4*(3*S^2 + 2*S*T + 3*T^2))*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
      (S*(-MH^2 + S)^3) + ((MH^2 - T)^2*(MH^2*MT^2 + S*T - MT^2*(S + T))*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/S + 
     ((MH^12*MT^2 + S^3*T^2*(S*T - MT^2*(S + T)) + 
        MH^10*(S*T - MT^2*(4*S + 3*T)) + MH^8*(-(S*T*(3*S + 4*T)) + 
          MT^2*(6*S^2 + 11*S*T + 3*T^2)) - 
        MH^6*(-(S*T*(3*S^2 + 6*S*T + 5*T^2)) + MT^2*(4*S^3 + 15*S^2*T + 
            14*S*T^2 + T^3)) - MH^2*S*T*(S^2*T*(2*S + 3*T) + 
          MT^2*(2*S^3 + 6*S^2*T + 9*S*T^2 + 4*T^3)) + 
        MH^4*S*(-(T*(S^3 + 3*S*T^2 + 2*T^3)) + MT^2*(S^3 + 9*S^2*T + 
            18*S*T^2 + 11*T^3)))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
      ((MH^2 - S)^3*S) + 2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
      (-3*MH^2 + S + 3*T)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)] + ((-(MH^6*MT^2) + S*T^3 + MT^2*(2*S^3 + 6*S^2*T + 
          5*S*T^2 + T^3) + MH^4*(S*T + MT^2*(5*S + 3*T)) - 
        MH^2*(2*S*T^2 + MT^2*(6*S^2 + 10*S*T + 3*T^2)))*
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/S - 
     ((MH^2 - T)^2*(MH^2*MT^2 + S*T - MT^2*(S + T))*
       (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
        Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2))/S - 
     2*(MH^2 - T)^2*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
      ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(4*(MH^2 - S - T)^3*T^2), 
 PVD[0, 2, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((2*(-MH^2 + S + U)^2)/(MH^2 - U) + 
    (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*(MH^6 - MH^4*(S - 2*U) + 
       U*(S + U)^2 - 2*MH^2*U*(S + 2*U))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     ((MH^2 - S)*(MH^3 - MH*U)^2) + (2*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - U)*
      (-MH^2 + S + U)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
     (S*(-MH^2 + S)) + ((-(MH^2*MT^2) - S*U + MT^2*(S + U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/S + 
    ((MH^2*MT^2 + S*U - MT^2*(S + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/S - 
    (2*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*(-3*MH^2 + S + 3*U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/(MH^2 - U)^2 + 
    ((-(MH^2*MT^2) - S*U + MT^2*(S + U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/S + 
    ((-(MH^6*MT^2) + S*U^3 + MT^2*(2*S^3 + 6*S^2*U + 5*S*U^2 + U^3) + 
       MH^4*(S*U + MT^2*(5*S + 3*U)) - 
       MH^2*(2*S*U^2 + MT^2*(6*S^2 + 10*S*U + 3*U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/
     (S*(MH^2 - U)^2) + 2*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(4*(MH^2 - S - U)^3), 
 PVD[0, 3, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  ((-2*T*(-MH^2 + T + U)^2*(-2*MH^4 - U*(8*T + 7*U) + MH^2*(2*T + 9*U)))/
     (MH^2 - U)^2 - (2*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*
      (MH^6*T + 3*U^2*(T + U)^2 - MH^4*(T^2 + 7*T*U - 3*U^2) + 
       MH^2*(4*T^2*U - 6*U^3))*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
     (MH^2*(MH^2 - U)^3) + 2*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + T + U)*
     (-MH^2 + T + 3*U)*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
    U*(-(MH^2*MT^2) - T*U + MT^2*(T + U))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    U*(MH^2*MT^2 + T*U - MT^2*(T + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) - 
    (2*T*U*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*(3*MH^4 + 6*T^2 + 11*T*U + 
       3*U^2 - MH^2*(11*T + 6*U))*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
        (2*MT^2)])/(MH^2 - U)^3 + U*(-(MH^2*MT^2) - T*U + MT^2*(T + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (U*(MH^8*MT^2 - T*U*(2*T^3 + 6*T^2*U + 6*T*U^2 + U^3) + 
       MT^2*(-4*T^4 - 10*T^3*U - 6*T^2*U^2 + T*U^3 + U^4) + 
       MH^6*(T*U - MT^2*(T + 4*U)) - 3*MH^4*(T*U*(2*T + U) + 
         MT^2*(2*T^2 - T*U - 2*U^2)) + MH^2*(3*T*U*(2*T^2 + 4*T*U + U^2) + 
         MT^2*(10*T^3 + 12*T^2*U - 3*T*U^2 - 4*U^3)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2))/(MH^2 - U)^3 + 
    2*T*U^2*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, 
      U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
   (4*T^2*(MH^2 - T - U)^3), PVD[1, 0, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*(MH^2 - S - T)), 
 PVD[1, 0, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*(MH^2 - S - U)), 
 PVD[1, 0, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(4*(MH^2 - T - U)), 
 PVD[1, 0, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - T)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[S*(-4*MT^2 + S)]*(MH^2 - T)*(-MH^2 + S + T)*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
    (MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    (MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
    4*S*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    (MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
    (2*MH^4*MT^2 + S*T^2 + 2*MT^2*(2*S^2 + 3*S*T + T^2) - 
      MH^2*(S*T + MT^2*(6*S + 4*T)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    2*S*(MH^2 - T)*T*(4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(16*S*(MH^2 - T)*(-MH^2 + S + T)^2), 
 PVD[1, 0, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - U)*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*S*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
     (MH^2 - S)*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
     (2*MH^6*MT^2 + S*U*(-(S*U) + 2*MT^2*(S + U)) - 
       MH^4*(S*U + 4*MT^2*(S + U)) + MH^2*(S*U*(S + U) + 
         2*MT^2*(S^2 + S*U + U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
     4*(MH^2 - S)*S*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
     (MH^2 - S)*(2*MH^4*MT^2 + S*U^2 + 2*MT^2*(2*S^2 + 3*S*U + U^2) - 
       MH^2*(S*U + MT^2*(6*S + 4*U)))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - S)*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
     2*(MH^2 - S)*S*(MH^2 - U)*U*(4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*
      ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(16*(MH^2 - S)*S*U*(-MH^2 + S + U)^2), 
 PVD[1, 0, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*T*Sqrt[T*(-4*MT^2 + T)]*U*(-MH^2 + T + U)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
     (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 - 
     (2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
       MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
         2*MT^2*(T^2 + T*U + U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
     4*(MH^2 - T)*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
     (MH^2 - T)*(2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
       MH^2*(T*U + MT^2*(6*T + 4*U)))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
     (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
     2*(MH^2 - T)*T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
      ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(16*(MH^2 - T)*T*U*(-MH^2 + T + U)^2), 
 PVD[1, 0, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - T)*T*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
    (MH^2 - S)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    (2*MH^4*MT^2 + S^2*T + 2*MT^2*(S^2 + 3*S*T + 2*T^2) - 
      MH^2*(S*T + MT^2*(4*S + 6*T)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
    4*(MH^2 - S)*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    (MH^2 - S)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (MH^2 - S)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    2*(MH^2 - S)*S*T*(4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*
     ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(16*(MH^2 - S)*T*(-MH^2 + S + T)^2), 
 PVD[1, 0, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - S - U)*U*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[S*(-4*MT^2 + S)]*U*(-MH^2 + S + U)*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
    (MH^2 - S)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
    (2*MH^4*MT^2 + S^2*U + 2*MT^2*(S^2 + 3*S*U + 2*U^2) - 
      MH^2*(S*U + MT^2*(4*S + 6*U)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
    4*(MH^2 - S)*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - S)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 + 
    (MH^2 - S)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*(MH^2 - S)*S*U*(4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(16*(MH^2 - S)*U*(-MH^2 + S + U)^2), 
 PVD[1, 0, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*U*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - U)*U*(-MH^2 + T + U)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
     (MH^2 - U)*(2*MH^4*MT^2 + T^2*U + 2*MT^2*(T^2 + 3*T*U + 2*U^2) - 
       MH^2*(T*U + MT^2*(4*T + 6*U)))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
     4*T*U*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
     (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
     (2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
       MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
         2*MT^2*(T^2 + T*U + U^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
     2*(MH^2 - T)*T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
      ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(16*T*(MH^2 - U)*U*(-MH^2 + T + U)^2), 
 PVD[1, 1, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - T)*T*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     4*S*Sqrt[S*(-4*MT^2 + S)]*T*(-MH^2 + S + T)*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
     (MH^2 - S)*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 - 
     (2*MH^6*MT^2 + S*T*(-(S*T) + 2*MT^2*(S + T)) - 
       MH^4*(S*T + 4*MT^2*(S + T)) + MH^2*(S*T*(S + T) + 
         2*MT^2*(S^2 + S*T + T^2)))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
     4*(MH^2 - S)*S*Sqrt[T*(-4*MT^2 + T)]*(-MH^2 + S + T)*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + 
     (MH^2 - S)*(2*MH^4*MT^2 + S*T^2 + 2*MT^2*(2*S^2 + 3*S*T + T^2) - 
       MH^2*(S*T + MT^2*(6*S + 4*T)))*
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
     (MH^2 - S)*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
      (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
       Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
     2*(MH^2 - S)*S*(MH^2 - T)*T*(4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*
      ScalarD0[0, 0, 0, MH^2, S, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(16*(MH^2 - S)*S*T*(-MH^2 + S + T)^2), 
 PVD[1, 1, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*(MH^2 - S - U)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[S*(-4*MT^2 + S)]*(MH^2 - U)*(-MH^2 + S + U)*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
    (MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2 + 
    (MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2) + 
    4*S*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + S + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (2*MH^4*MT^2 + S*U^2 + 2*MT^2*(2*S^2 + 3*S*U + U^2) - 
      MH^2*(S*U + MT^2*(6*S + 4*U)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*S*(MH^2 - U)*U*(4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*
     ScalarD0[0, 0, 0, MH^2, S, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(16*S*(MH^2 - U)*(-MH^2 + S + U)^2), 
 PVD[1, 1, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (4*Sqrt[MH^4 - 4*MH^2*MT^2]*T*(MH^2 - T - U)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    4*Sqrt[T*(-4*MT^2 + T)]*(MH^2 - U)*(-MH^2 + T + U)*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
    (MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2 + 
    (MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2) + 
    4*T*Sqrt[U*(-4*MT^2 + U)]*(-MH^2 + T + U)*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
    (MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2 - 
    (2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
      MH^2*(T*U + MT^2*(6*T + 4*U)))*
     (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
      Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2) + 
    2*T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/(16*T*(MH^2 - U)*(-MH^2 + T + U)^2)}
