(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVC[0, 0, 1, 0, 0, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*T + Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)])/T^2, PVC[0, 0, 1, 0, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Sqrt[MH^4 - 4*MH^2*MT^2]*S*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MH^2*Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
       (2*MT^2)])/(MH^2*(MH^2 - S)*S), 
 PVC[0, 0, 1, MH^2, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (4*S*(-MH^2 + S) - 4*Sqrt[MH^4 - 4*MH^2*MT^2]*S*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] - 
    MH^2*S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    2*(MH^2 + S)*Sqrt[S*(-4*MT^2 + S)]*
     Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
    MH^2*S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
   (2*(MH^2 - S)^2*S), PVC[0, 1, 0, 0, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(8*T + 4*Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
        (2*MT^2)] + T*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
   (2*T^2), PVC[0, 1, 0, 0, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 + S)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    MH^2*S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    MH^2*(4*MH^2 - 4*S - 4*Sqrt[S*(-4*MT^2 + S)]*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
      S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
   (2*(MH^3 - MH*S)^2), PVC[0, 1, 0, MH^2, 0, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 + S)*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
    MH^2*S*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 + 
    MH^2*(4*MH^2 - 4*S - 4*Sqrt[S*(-4*MT^2 + S)]*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
      S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2))/
   (2*(MH^3 - MH*S)^2), PVC[0, 0, 0, 0, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2/(2*T), 
 PVC[0, 0, 0, 0, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)), 
 PVC[0, 0, 0, MH^2, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S))}
