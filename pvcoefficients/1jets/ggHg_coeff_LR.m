(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{2 + Eps^(-1) + DiscB[S, MT, MT] + Log[Mu^2/MT^2], 
 2 + Eps^(-1) + DiscB[T, MT, MT] + Log[Mu^2/MT^2], 
 2 + Eps^(-1) + DiscB[U, MT, MT] + Log[Mu^2/MT^2], 
 Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2/(2*T), 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)), 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(2*(MH^2 - U)), 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)), 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)), 
 (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(2*(MH^2 - T)), 
 (2 + DiscB[T, MT, MT])/T, (DiscB[MH^2, MT, MT] - DiscB[S, MT, MT])/
  (MH^2 - S), (DiscB[MH^2, MT, MT] - DiscB[U, MT, MT])/(MH^2 - U), 
 (-4*MH^2 + 4*S - 4*MH^2*DiscB[MH^2, MT, MT] + 
   2*(MH^2 + S)*DiscB[S, MT, MT] - 
   MH^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MH^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
  (2*(MH^2 - S)^2), (-DiscB[MH^2, MT, MT] + DiscB[U, MT, MT])/(2*(MH^2 - U)), 
 -(8 + 4*DiscB[T, MT, MT] + Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/
       (2*MT^2)]^2)/(2*T), (4*MH^2 - 4*S + 2*(MH^2 + S)*DiscB[MH^2, MT, MT] - 
   4*S*DiscB[S, MT, MT] + 
   S*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)^2), 
 (4*MH^2 - 4*S + 2*(MH^2 + S)*DiscB[MH^2, MT, MT] - 4*S*DiscB[S, MT, MT] + 
   S*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
   S*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(2*(MH^2 - S)^2), 
 (DiscB[MH^2, MT, MT] - DiscB[S, MT, MT])/(MH^2 - S), 
 (DiscB[MH^2, MT, MT] - DiscB[T, MT, MT])/(MH^2 - T), 
 (-MH^2 + U - MH^2*DiscB[MH^2, MT, MT] + MH^2*DiscB[U, MT, MT] - 
   MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
  (2*(MH^2 - U)^2), (-MH^2 + S - MH^2*DiscB[MH^2, MT, MT] + 
   MH^2*DiscB[S, MT, MT] - 
   MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MT^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
  (2*(MH^2 - S)^2), (-MH^2 + T - MH^2*DiscB[MH^2, MT, MT] + 
   MH^2*DiscB[T, MT, MT] - 
   MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
  (2*(MH^2 - T)^2), (-DiscB[MH^2, MT, MT] + DiscB[S, MT, MT])/(2*(MH^2 - S)), 
 (-DiscB[MH^2, MT, MT] + DiscB[T, MT, MT])/(2*(MH^2 - T)), 
 (3*MH^2 + MH^2/Eps - 3*U - U/Eps + MH^2*DiscB[MH^2, MT, MT] - 
   U*DiscB[U, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
       (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - U*Log[Mu^2/MT^2] - 
   MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/(4*(MH^2 - U)), 
 (3*MH^2 + MH^2/Eps - 3*S - S/Eps + MH^2*DiscB[MH^2, MT, MT] - 
   S*DiscB[S, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
       (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - S*Log[Mu^2/MT^2] - 
   MT^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/(4*(MH^2 - S)), 
 (3*MH^2 + MH^2/Eps - 3*T - T/Eps + MH^2*DiscB[MH^2, MT, MT] - 
   T*DiscB[T, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
       (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - T*Log[Mu^2/MT^2] - 
   MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/(4*(MH^2 - T)), 
 ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT], 
 ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT], 
 ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT], 
 (S*ScalarC0[0, 0, S, MT, MT, MT] + T*ScalarC0[0, 0, T, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   2*S*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   S*T*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(2*S*(-MH^2 + S + T)), 
 (S*(-MH^2 + U)*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - 2*S - U)*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*S*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*U*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*U*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*MH^2*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*S*U*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT] - 
   S*U^2*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/
  (2*S*U*(-MH^2 + S + U)), (T*(-MH^2 + U)*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - 2*T - U)*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*MH^2*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
   T*U^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)), (S*ScalarC0[0, 0, S, MT, MT, MT] + 
   T*ScalarC0[0, 0, T, MT, MT, MT] + MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   2*T*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   S*T*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(2*T*(-MH^2 + S + T)), 
 (S*ScalarC0[0, 0, S, MT, MT, MT] + U*ScalarC0[0, 0, U, MT, MT, MT] + 
   MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   2*U*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   S*U*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(2*U*(-MH^2 + S + U)), 
 (T*(MH^2 - T - 2*U)*ScalarC0[0, 0, T, MT, MT, MT] + 
   (-MH^2 + T)*U*ScalarC0[0, 0, U, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   2*MH^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T^2*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   T*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   MH^2*T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
   T^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)), 
 ((2*(2*MH^2 - S - T)*DiscB[MH^2, MT, MT])/((MH^2 - S)*(MH^2 - T)*
     (MH^2 - S - T)) - (2*DiscB[S, MT, MT])/((MH^2 - S)*(MH^2 - S - T)) - 
   (2*DiscB[T, MT, MT])/((MH^2 - T)*(MH^2 - S - T)) - 
   (S*ScalarC0[0, 0, S, MT, MT, MT])/(-MH^2 + S + T)^2 - 
   (T*ScalarC0[0, 0, T, MT, MT, MT])/(-MH^2 + S + T)^2 + 
   ((MH^2 - S)*ScalarC0[0, MH^2, S, MT, MT, MT])/(-MH^2 + S + T)^2 + 
   ((MH^2 - T)*ScalarC0[0, MH^2, T, MT, MT, MT])/(-MH^2 + S + T)^2 + 
   ((2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
      MT, MT, MT])/(-MH^2 + S + T)^2)/2, 
 (4*(MH^2 - S)*(-MH^2 + S + U)^2 + 2*MH^2*(MH^2 - S - 2*U)*(MH^2 - S - U)*
    DiscB[MH^2, MT, MT] - 2*(MH^2 - S - U)*(MH^4 - S*U - MH^2*(S + U))*
    DiscB[S, MT, MT] + 2*(MH^2 - S)^2*(MH^2 - S - U)*DiscB[U, MT, MT] + 
   (MH^2 - S)^2*S*(MH^2 - U)*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - S)^2*(MH^2 - U)*U*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - S)*(MH^6 + S^2*U - MH^4*(2*S + 3*U) + MH^2*(S^2 + 2*S*U + 2*U^2))*
    ScalarC0[0, MH^2, S, MT, MT, MT] - (MH^2 - S)^2*(MH^2 - U)^2*
    ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - S)^2*(MH^2 - U)*
    (2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
     MT, MT, MT])/(2*(MH^2 - S)^2*U*(-MH^2 + S + U)^2), 
 -(4*MH^4 - 8*MH^2*T + 4*T^2 - 8*MH^2*U + 8*T*U + 4*U^2 + 
    2*MH^2*(-MH^2 + T + U)*DiscB[MH^2, MT, MT] + 2*(MH^2 - U)*(MH^2 - T - U)*
     DiscB[T, MT, MT] + 2*MH^4*DiscB[U, MT, MT] - 4*MH^2*T*DiscB[U, MT, MT] + 
    2*T^2*DiscB[U, MT, MT] - 2*MH^2*U*DiscB[U, MT, MT] + 
    2*T*U*DiscB[U, MT, MT] + T^2*U*ScalarC0[0, 0, T, MT, MT, MT] + 
    T*U^2*ScalarC0[0, 0, U, MT, MT, MT] - 
    MH^2*T*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    T^2*U*ScalarC0[0, MH^2, T, MT, MT, MT] - 
    MH^2*T*U*ScalarC0[0, MH^2, U, MT, MT, MT] + 
    T*U^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
    2*MH^6*MT^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
    4*MH^4*MT^2*T*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    2*MH^2*MT^2*T^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
    4*MH^4*MT^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    2*MH^2*MT^2*T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    2*MT^2*T^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    2*MH^2*MT^2*U^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
    2*MT^2*T*U^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
    T^2*U^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*U*(-MH^2 + T + U)^2), ((-MH^2 + S + T)^2/(MH^2 - T) + 
   ((MH^2 - S - T)*(MH^6 - MH^4*(S - 2*T) + T*(S + T)^2 - 2*MH^2*T*(S + 2*T))*
     DiscB[MH^2, MT, MT])/((MH^2 - S)*(MH^2 - T)^2) - 
   ((MH^2 - S - T)*(MH^2 - S + T)*DiscB[S, MT, MT])/(MH^2 - S) - 
   ((3*MH^2 - S - 3*T)*(MH^2 - S - T)*T*DiscB[T, MT, MT])/(MH^2 - T)^2 - 
   (MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, MT] + 
   (T*(-(MH^2*MT^2) - S*T + MT^2*(S + T))*ScalarC0[0, 0, T, MT, MT, MT])/S + 
   ((MH^2 - S)*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, MH^2, S, MT, MT, 
      MT])/S + ((-(MH^6*MT^2) + S*T^3 + MT^2*(2*S^3 + 6*S^2*T + 5*S*T^2 + 
        T^3) + MH^4*(S*T + MT^2*(5*S + 3*T)) - 
      MH^2*(2*S*T^2 + MT^2*(6*S^2 + 10*S*T + 3*T^2)))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/(S*(MH^2 - T)) + 
   T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
     MT, MT, MT])/(2*(MH^2 - S - T)^3), 
 -((U*(-MH^2 + S + U)^2*(8*MH^4 + S*(3*S + 2*U) - MH^2*(11*S + 8*U)))/
     (MH^2 - S)^2 + (MH^2*(MH^2 - S - U)*U*(3*MH^6 + 2*S^2*U - 
       3*MH^4*(2*S + 3*U) + MH^2*(3*S^2 + 7*S*U + 6*U^2))*
      DiscB[MH^2, MT, MT])/(MH^2 - S)^3 - 
    ((MH^2 - S - U)*U*(3*MH^8 + S^2*(S - U)*U - 6*MH^6*(S + U) + 
       MH^2*S*U*(3*S + 4*U) + MH^4*(3*S^2 + 2*S*U + 3*U^2))*DiscB[S, MT, MT])/
     (MH^2 - S)^3 + (3*MH^2 - S - 3*U)*(MH^2 - S - U)*U*DiscB[U, MT, MT] + 
    (MH^2 - U)^2*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, 
      MT] + (U*(-(MH^6*MT^2) + S*U^3 + MT^2*(2*S^3 + 6*S^2*U + 5*S*U^2 + 
         U^3) + MH^4*(S*U + MT^2*(5*S + 3*U)) - 
       MH^2*(2*S*U^2 + MT^2*(6*S^2 + 10*S*U + 3*U^2)))*
      ScalarC0[0, 0, U, MT, MT, MT])/S + 
    ((MH^12*MT^2 + S^3*U^2*(S*U - MT^2*(S + U)) + 
       MH^10*(S*U - MT^2*(4*S + 3*U)) + MH^8*(-(S*U*(3*S + 4*U)) + 
         MT^2*(6*S^2 + 11*S*U + 3*U^2)) - 
       MH^6*(-(S*U*(3*S^2 + 6*S*U + 5*U^2)) + MT^2*(4*S^3 + 15*S^2*U + 
           14*S*U^2 + U^3)) - MH^2*S*U*(S^2*U*(2*S + 3*U) + 
         MT^2*(2*S^3 + 6*S^2*U + 9*S*U^2 + 4*U^3)) + 
       MH^4*S*(-(U*(S^3 + 3*S*U^2 + 2*U^3)) + MT^2*(S^3 + 9*S^2*U + 
           18*S*U^2 + 11*U^3)))*ScalarC0[0, MH^2, S, MT, MT, MT])/
     ((MH^2 - S)^2*S) - ((MH^2 - U)^3*(MH^2*MT^2 + S*U - MT^2*(S + U))*
      ScalarC0[0, MH^2, U, MT, MT, MT])/S - (MH^2 - U)^2*U*
     (3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
      MT, MT, MT])/(2*(MH^2 - S - U)^3*U^2), 
 -(T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 3*T + 2*U) + 
    (MH^2*T*(MH^2 - T - U)*U*(MH^4 + 2*T*U - MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/(MH^2 - T) + 
    (T*U*(-MH^2 + T + U)*(MH^6 + T*(T - U)*U + MH^2*U*(2*T + U) - 
       MH^4*(T + 2*U))*DiscB[T, MT, MT])/(MH^2 - T) + 
    T*U*(-MH^2 + T + U)*(MH^4 - MH^2*(2*T + U) + T*(T + 3*U))*
     DiscB[U, MT, MT] + T*(MH^2 - U)*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     ScalarC0[0, 0, T, MT, MT, MT] + 
    U*(-(MH^8*MT^2) + MH^6*MT^2*(2*T + 3*U) + 
      MH^4*MT^2*(T^2 - 3*T*U - 3*U^2) - 
      MH^2*(T^2*U^2 + MT^2*(4*T^3 + 6*T^2*U - U^3)) + 
      T*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)))*
     ScalarC0[0, 0, U, MT, MT, MT] + (MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 
      3*MH^6*MT^2*(T + U)^2 - MH^2*T^2*U^2*(-MT^2 + T + U) + 
      T^2*U^2*(T*U - MT^2*(T + U)) - MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*
     ScalarC0[0, MH^2, T, MT, MT, MT] - (MH^2 - U)^2*
     (MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + MH^2*MT^2*(T^2 + T*U + U^2) + 
      T*U*(-(T*U) + MT^2*(T + U)))*ScalarC0[0, MH^2, U, MT, MT, MT] - 
    T*(MH^2 - U)*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 -(4*(MH^2 - S)*(-MH^2 + S + T)^2 + 2*(MH^2 - S - T)*
     (MH^2*(S - T) - S*(S + T))*DiscB[MH^2, MT, MT] - 
    2*S*(-MH^2 + S + T)*(-MH^2 + S + 2*T)*DiscB[S, MT, MT] + 
    2*(MH^2 - S)^2*(MH^2 - S - T)*DiscB[T, MT, MT] + 
    (MH^2 - S)^2*S^2*ScalarC0[0, 0, S, MT, MT, MT] + 
    (MH^2 - S)^2*S*T*ScalarC0[0, 0, T, MT, MT, MT] + 
    (MH^2 - S)*S*(MH^4 + S^2 + 4*S*T + 2*T^2 - 2*MH^2*(S + 2*T))*
     ScalarC0[0, MH^2, S, MT, MT, MT] - (MH^2 - S)^2*S*(MH^2 - T)*
     ScalarC0[0, MH^2, T, MT, MT, MT] - (MH^2 - S)^2*S*
     (2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
      MT, MT, MT])/(2*(MH^2 - S)^2*T*(-MH^2 + S + T)^2), 
 -(4*(MH^2 - S)*(-MH^2 + S + U)^2 + 2*(MH^2 - S - U)*
     (MH^2*(S - U) - S*(S + U))*DiscB[MH^2, MT, MT] - 
    2*S*(-MH^2 + S + U)*(-MH^2 + S + 2*U)*DiscB[S, MT, MT] + 
    2*(MH^2 - S)^2*(MH^2 - S - U)*DiscB[U, MT, MT] + 
    (MH^2 - S)^2*S^2*ScalarC0[0, 0, S, MT, MT, MT] + 
    (MH^2 - S)^2*S*U*ScalarC0[0, 0, U, MT, MT, MT] + 
    (MH^2 - S)*S*(MH^4 + S^2 + 4*S*U + 2*U^2 - 2*MH^2*(S + 2*U))*
     ScalarC0[0, MH^2, S, MT, MT, MT] - (MH^2 - S)^2*S*(MH^2 - U)*
     ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - S)^2*S*
     (2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
      MT, MT, MT])/(2*(MH^2 - S)^2*U*(-MH^2 + S + U)^2), 
 (4*(MH^2 - 2*U)*(MH^2 - U)*(-MH^2 + T + U)^2 - 2*MH^2*(MH^2 - T - U)*
    (MH^4 - T*U - MH^2*(T + U))*DiscB[MH^2, MT, MT] + 
   2*(MH^2 - T - 2*U)*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] + 
   2*(MH^2 - T - U)*(MH^6 - T*U^2 - MH^4*(T + U))*DiscB[U, MT, MT] + 
   T*(MH^2 - U)^2*(MH^4 + T^2 + 4*T*U + 2*U^2 - 2*MH^2*(T + 2*U))*
    ScalarC0[0, 0, T, MT, MT, MT] - (MH^2 - T)^2*(MH^2 - U)^2*U*
    ScalarC0[0, 0, U, MT, MT, MT] + (MH^2 - T)^3*(MH^2 - U)^2*
    ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - U)*(MH^8 - T^2*U^2 - 2*MH^6*(T + U) + 2*MH^2*T*U*(T + U) + 
     MH^4*(T^2 + U^2))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   (MH^2 - T)^2*(MH^2 - U)^2*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T*(MH^2 - U)^2*U*(-MH^2 + T + U)^2), 
 ((-MH^2 + S + T)^2/(MH^2 - S) + 
   ((MH^2 - S - T)*(MH^6 + MH^4*(2*S - T) + S*(S + T)^2 - 2*MH^2*S*(2*S + T))*
     DiscB[MH^2, MT, MT])/((MH^2 - S)^2*(MH^2 - T)) - 
   (S*(-MH^2 + S + T)*(-3*MH^2 + 3*S + T)*DiscB[S, MT, MT])/(MH^2 - S)^2 - 
   ((MH^2 - S - T)*(MH^2 + S - T)*DiscB[T, MT, MT])/(MH^2 - T) + 
   (S*(-(MH^2*MT^2) - S*T + MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, MT])/T - 
   (MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, 0, T, MT, MT, MT] - 
   ((MH^6*MT^2 - S^3*T - MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 2*T^3) - 
      MH^4*(S*T + MT^2*(3*S + 5*T)) + 
      MH^2*(2*S^2*T + MT^2*(3*S^2 + 10*S*T + 6*T^2)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*T) + 
   ((MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, MH^2, T, MT, MT, 
      MT])/T + S*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
    ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(2*(MH^2 - S - T)^3), 
 ((U*(-MH^2 + S + U)^2*(3*MH^4 - MH^2*(S + 3*U) - S*(2*S + 3*U)))/
    (MH^2 - S)^2 + (MH^2*(MH^2 - S - U)*U*(MH^4*(3*S - U) + 
      MH^2*(-6*S^2 - 9*S*U + U^2) + S*(3*S^2 + 10*S*U + 5*U^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - S)^3 + 
   (S*U*(-MH^2 + S + U)*(3*MH^6 + S*U*(3*S + U) - 2*MH^4*(3*S + 4*U) + 
      MH^2*(3*S^2 + 5*S*U + 5*U^2))*DiscB[S, MT, MT])/(MH^2 - S)^3 + 
   (MH^2 - S - U)*(MH^2 + S - U)*U*DiscB[U, MT, MT] + 
   S*(MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, 
     MT] + (MH^2 - U)*U*(MH^2*MT^2 + S*U - MT^2*(S + U))*
    ScalarC0[0, 0, U, MT, MT, MT] + 
   ((MH^10*MT^2 + MH^8*(S*U - 2*MT^2*(2*S + U)) + 
      MH^6*(-(S*U*(3*S + 5*U)) + MT^2*(6*S^2 + 7*S*U - U^2)) + 
      MH^4*(3*S*U*(S^2 + 3*S*U + 2*U^2) - MT^2*(4*S^3 + 9*S^2*U + 3*S*U^2 - 
          4*U^3)) - S*U*(S^3*U + MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 2*U^3)) + 
      MH^2*(-(S*U*(S^3 + 3*S^2*U + 6*S*U^2 + 2*U^3)) + 
        MT^2*(S^4 + 5*S^3*U + 9*S^2*U^2 + 2*S*U^3 - 2*U^4)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/(MH^2 - S)^2 - 
   (MH^2 - U)^2*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, MH^2, U, MT, MT, 
     MT] - S*(MH^2 - U)*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
    ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(2*(MH^2 - S - U)^3*U^2), 
 -(T*U*(-MH^2 + T + U)^2*(-2*MH^2 + 2*T + 3*U) + 
    (MH^2*T*(MH^2 - T - U)*U*(MH^4 + 2*T*U - MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/(MH^2 - U) - T*(MH^2 - T - U)*U*
     (MH^4 + U*(3*T + U) - MH^2*(T + 2*U))*DiscB[T, MT, MT] + 
    (T*U*(-MH^2 + T + U)*(MH^6 + T*U*(-T + U) - MH^4*(2*T + U) + 
       MH^2*T*(T + 2*U))*DiscB[U, MT, MT])/(MH^2 - U) + 
    T*(-(MH^8*MT^2) + MH^6*MT^2*(3*T + 2*U) + 
      MH^4*MT^2*(-3*T^2 - 3*T*U + U^2) + 
      MH^2*(-(T^2*U^2) + MT^2*(T^3 - 6*T*U^2 - 4*U^3)) + 
      U*(T^3*U + MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3)))*
     ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)*U*
     (MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + MH^2*MT^2*(T^2 + T*U + U^2) + 
      T*U*(-(T*U) + MT^2*(T + U)))*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)^2*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 + T*U + U^2) + T*U*(-(T*U) + MT^2*(T + U)))*
     ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^10*MT^2 - 3*MH^8*MT^2*(T + U) + 3*MH^6*MT^2*(T + U)^2 - 
      MH^2*T^2*U^2*(-MT^2 + T + U) + T^2*U^2*(T*U - MT^2*(T + U)) - 
      MH^4*(-(T^2*U^2) + MT^2*(T + U)^3))*ScalarC0[0, MH^2, U, MT, MT, MT] - 
    (MH^2 - T)*T*U*(MH^6*MT^2 - 2*MH^4*MT^2*(T + U) + 
      MH^2*MT^2*(T^2 - T*U + U^2) + T*U*(-(T*U) + 3*MT^2*(T + U)))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 ((T*(-MH^2 + S + T)^2*(2*MH^4 - MH^2*(9*S + 2*T) + S*(7*S + 8*T)))/
    (MH^2 - S)^2 - ((MH^2 - S - T)*T*(MH^6*T + 3*S^2*(S + T)^2 + 
      MH^4*(3*S^2 - 7*S*T - T^2) + MH^2*(-6*S^3 + 4*S*T^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - S)^3 + 
   (S^2*T*(-MH^2 + S + T)*(3*MH^4 + 3*S^2 + 11*S*T + 6*T^2 - 
      MH^2*(6*S + 11*T))*DiscB[S, MT, MT])/(-MH^2 + S)^3 + 
   (MH^2 - 3*S - T)*(MH^2 - S - T)*T*DiscB[T, MT, MT] + 
   S^2*(-(MH^2*MT^2) - S*T + MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, MT] - 
   S*T*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, 0, T, MT, MT, MT] - 
   (S*(MH^8*MT^2 - S*T*(S^3 + 6*S^2*T + 6*S*T^2 + 2*T^3) + 
      MT^2*(S^4 + S^3*T - 6*S^2*T^2 - 10*S*T^3 - 4*T^4) + 
      MH^6*(S*T - MT^2*(4*S + T)) + 3*MH^4*(-(S*T*(S + 2*T)) + 
        MT^2*(2*S^2 + S*T - 2*T^2)) + MH^2*(3*S*T*(S^2 + 4*S*T + 2*T^2) + 
        MT^2*(-4*S^3 - 3*S^2*T + 12*S*T^2 + 10*T^3)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/(MH^2 - S)^2 + 
   S*(MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + S^2*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
    ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(2*(MH^2 - S - T)^3*T^2), 
 ((U*(-MH^2 + S + U)^2*(2*MH^4 - MH^2*(9*S + 2*U) + S*(7*S + 8*U)))/
    (MH^2 - S)^2 - ((MH^2 - S - U)*U*(MH^6*U + 3*S^2*(S + U)^2 + 
      MH^4*(3*S^2 - 7*S*U - U^2) + MH^2*(-6*S^3 + 4*S*U^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - S)^3 + 
   (S^2*U*(-MH^2 + S + U)*(3*MH^4 + 3*S^2 + 11*S*U + 6*U^2 - 
      MH^2*(6*S + 11*U))*DiscB[S, MT, MT])/(-MH^2 + S)^3 + 
   (MH^2 - 3*S - U)*(MH^2 - S - U)*U*DiscB[U, MT, MT] + 
   S^2*(-(MH^2*MT^2) - S*U + MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, MT] - 
   S*U*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (S*(MH^8*MT^2 - S*U*(S^3 + 6*S^2*U + 6*S*U^2 + 2*U^3) + 
      MT^2*(S^4 + S^3*U - 6*S^2*U^2 - 10*S*U^3 - 4*U^4) + 
      MH^6*(S*U - MT^2*(4*S + U)) + 3*MH^4*(-(S*U*(S + 2*U)) + 
        MT^2*(2*S^2 + S*U - 2*U^2)) + MH^2*(3*S*U*(S^2 + 4*S*U + 2*U^2) + 
        MT^2*(-4*S^3 - 3*S^2*U + 12*S*U^2 + 10*U^3)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/(MH^2 - S)^2 + 
   S*(MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, MH^2, U, MT, MT, 
     MT] + S^2*U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
    ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(2*(MH^2 - S - U)^3*U^2), 
 ((T*U*(-MH^2 + T + U)^2*(-6*MH^6 + U^2*(13*T + 14*U) + MH^4*(6*T + 31*U) - 
      MH^2*U*(25*T + 39*U)))/(MH^2 - U)^2 + 
   (MH^2*T*(MH^2 - T - U)*U*(3*MH^8 - 2*T^2*U^2 - 6*MH^6*(T + U) + 
      5*MH^2*T*U*(T + U) + MH^4*(3*T^2 + T*U + 3*U^2))*DiscB[MH^2, MT, MT])/
    (MH^2 - U)^3 + T*U*(-MH^2 + T + U)*(3*MH^4 + 3*T^2 + 11*T*U + 6*U^2 - 
     MH^2*(6*T + 11*U))*DiscB[T, MT, MT] + 
   (T*U*(-MH^2 + T + U)*(3*MH^10 - 6*MH^4*T*U^2 - 6*MH^8*(T + U) + 
      3*MH^6*(T + U)^2 - T*U^3*(3*T + U) + MH^2*T*U^2*(6*T + 7*U))*
     DiscB[U, MT, MT])/(MH^2 - U)^3 - 
   T*(MH^8*MT^2 - T*U*(T^3 + 6*T^2*U + 6*T*U^2 + 2*U^3) + 
     MT^2*(T^4 + T^3*U - 6*T^2*U^2 - 10*T*U^3 - 4*U^4) + 
     MH^6*(T*U - MT^2*(4*T + U)) + 3*MH^4*(-(T*U*(T + 2*U)) + 
       MT^2*(2*T^2 + T*U - 2*U^2)) + MH^2*(3*T*U*(T^2 + 4*T*U + 2*U^2) + 
       MT^2*(-4*T^3 - 3*T^2*U + 12*T*U^2 + 10*U^3)))*
    ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)^3*U*
    (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^4*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + ((MH^14*MT^2 + MH^12*(T*U - 4*MT^2*(T + U)) + 
      T^3*U^3*(-(T*U) + MT^2*(T + U)) + 3*MH^10*(-(T*U*(T + U)) + 
        MT^2*(2*T^2 + 5*T*U + 2*U^2)) + MH^2*T^2*U^2*
       (3*T*U*(T + U) - MT^2*(3*T^2 + 7*T*U + 3*U^2)) - 
      3*MH^4*T*U*(T*U*(T^2 + 3*T*U + U^2) + MT^2*(T^3 + T^2*U + T*U^2 + 
          U^3)) + MH^8*(3*T*U*(T^2 + T*U + U^2) - 
        MT^2*(4*T^3 + 21*T^2*U + 21*T*U^2 + 4*U^3)) + 
      MH^6*(-(T*U*(T^3 - 3*T^2*U - 3*T*U^2 + U^3)) + 
        MT^2*(T^4 + 13*T^3*U + 21*T^2*U^2 + 13*T*U^3 + U^4)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 - 
   (MH^2 - T)^3*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (2*T^2*(MH^2 - T - U)^3*U^2), 
 (S*(-MH^2 + T)*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - 2*S - T)*T*ScalarC0[0, 0, T, MT, MT, MT] - 
   MH^4*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*S*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*T*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*T*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^4*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   2*MH^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*S*T*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT] - 
   S*T^2*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/
  (2*S*T*(-MH^2 + S + T)), (S*ScalarC0[0, 0, S, MT, MT, MT] + 
   U*ScalarC0[0, 0, U, MT, MT, MT] - MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*S*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   S*U*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(2*S*(-MH^2 + S + U)), 
 (T*ScalarC0[0, 0, T, MT, MT, MT] + U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   2*T*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T*(-MH^2 + T + U)), 
 (-2*MH^2*(MH^2 - S - T)*DiscB[MH^2, MT, MT] - 2*S*(-MH^2 + S + T)*
    DiscB[S, MT, MT] + 2*(MH^2 - S)*(MH^2 - S - T)*DiscB[T, MT, MT] + 
   (MH^2 - S)*S^2*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - S)*S*T*ScalarC0[0, 0, T, MT, MT, MT] - 
   (MH^2 - S)^2*S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   (MH^2 - S)*S*(MH^2 - T)*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   (MH^2 - S)*(2*MH^4*MT^2 - S^2*T - 2*MH^2*MT^2*(3*S + 2*T) + 
     2*MT^2*(2*S^2 + 3*S*T + T^2))*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, 
     MT])/(2*(MH^2 - S)*S*(-MH^2 + S + T)^2), 
 (-2*MH^2*(MH^2 - S - U)*DiscB[MH^2, MT, MT] - 2*S*(-MH^2 + S + U)*
    DiscB[S, MT, MT] + 2*(MH^2 - S)*(MH^2 - S - U)*DiscB[U, MT, MT] + 
   (MH^2 - S)*S^2*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - S)*S*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - S)^2*S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   (MH^2 - S)*S*(MH^2 - U)*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   (MH^2 - S)*(2*MH^4*MT^2 - S^2*U - 2*MH^2*MT^2*(3*S + 2*U) + 
     2*MT^2*(2*S^2 + 3*S*U + U^2))*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, 
     MT])/(2*(MH^2 - S)*S*(-MH^2 + S + U)^2), 
 (-2*MH^2*(MH^2 - T - U)*DiscB[MH^2, MT, MT] - 2*T*(-MH^2 + T + U)*
    DiscB[T, MT, MT] + 2*(MH^2 - T)*(MH^2 - T - U)*DiscB[U, MT, MT] + 
   (MH^2 - T)*T^2*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - T)*T*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - T)*T*(MH^2 - U)*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   (MH^2 - T)*(2*MH^4*MT^2 - T^2*U - 2*MH^2*MT^2*(3*T + 2*U) + 
     2*MT^2*(2*T^2 + 3*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
     MT])/(2*(MH^2 - T)*T*(-MH^2 + T + U)^2), 
 -((-MH^2 + S + T)^2/S + (MH^2*(MH^2 - S - T)*(-S^2 - T^2 + MH^2*(S + T))*
      DiscB[MH^2, MT, MT])/((MH^2 - S)*S*(MH^2 - T)) - 
    ((MH^2 - S - T)*(MH^2 - S + T)*DiscB[S, MT, MT])/(MH^2 - S) + 
    ((MH^2 + S - T)*T*(-MH^2 + S + T)*DiscB[T, MT, MT])/(S*(MH^2 - T)) + 
    ((MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + MT^2*(2*S^2 + 3*S*T + T^2))*
      ScalarC0[0, 0, S, MT, MT, MT])/S + 
    (T*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
       MT^2*(2*S^2 + 3*S*T + T^2))*ScalarC0[0, 0, T, MT, MT, MT])/S^2 - 
    ((MH^2 - S)*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
       MT^2*(2*S^2 + 3*S*T + T^2))*ScalarC0[0, MH^2, S, MT, MT, MT])/S^2 + 
    ((MH^6*MT^2 - S^2*T^2 - 3*MH^4*MT^2*(S + T) - 
       MT^2*(2*S^3 + 4*S^2*T + 3*S*T^2 + T^3) + 
       MH^2*(S^2*T + MT^2*(4*S^2 + 6*S*T + 3*T^2)))*ScalarC0[0, MH^2, T, MT, 
       MT, MT])/S^2 - (T*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(5*S + 2*T) + 
       MT^2*(4*S^2 + 5*S*T + T^2))*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, 
       MT])/S)/(2*(MH^2 - S - T)^3), 
 ((-MH^2 + S + U)^2/(MH^2 - S) + (MH^2*(MH^4 - 2*S^2 + MH^2*(S - U))*
     (MH^2 - S - U)*DiscB[MH^2, MT, MT])/((MH^2 - S)^2*S) - 
   ((MH^2 - S - U)*(2*MH^4 + S*(-S + U) - MH^2*(S + 2*U))*DiscB[S, MT, MT])/
    (MH^2 - S)^2 + ((MH^2 + S - U)*(-MH^2 + S + U)*DiscB[U, MT, MT])/S + 
   ((MH^2 - U)*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
      MT^2*(2*S^2 + 3*S*U + U^2))*ScalarC0[0, 0, S, MT, MT, MT])/(S*U) + 
   ((-(MH^6*MT^2) + S^2*U^2 + 3*MH^4*MT^2*(S + U) + 
      MT^2*(2*S^3 + 4*S^2*U + 3*S*U^2 + U^3) - 
      MH^2*(S^2*U + MT^2*(4*S^2 + 6*S*U + 3*U^2)))*ScalarC0[0, 0, U, MT, MT, 
      MT])/S^2 + ((MH^10*MT^2 - MH^8*MT^2*(5*S + 3*U) + 
      S^2*U*(-(S^2*U) + MT^2*(2*S^2 + 3*S*U + U^2)) + 
      MH^6*(S^2*U + 3*MT^2*(3*S^2 + 4*S*U + U^2)) - 
      MH^4*(S^2*U*(2*S + U) + MT^2*(7*S^3 + 13*S^2*U + 9*S*U^2 + U^3)) + 
      MH^2*(S^3*U*(S + 2*U) + MT^2*S*(2*S^3 + 2*S^2*U + 3*S*U^2 + 2*U^3)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*S^2*U) - 
   ((MH^2 - U)^2*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
      MT^2*(2*S^2 + 3*S*U + U^2))*ScalarC0[0, MH^2, U, MT, MT, MT])/(S^2*U) - 
   ((MH^2 - U)*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(5*S + 2*U) + 
      MT^2*(4*S^2 + 5*S*U + U^2))*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, 
      MT])/S)/(2*(MH^2 - S - U)^3), 
 ((-MH^2 + T + U)^2/(MH^2 - T) + (MH^2*(MH^4 - 2*T^2 + MH^2*(T - U))*
     (MH^2 - T - U)*DiscB[MH^2, MT, MT])/((MH^2 - T)^2*T) - 
   ((MH^2 - T - U)*(2*MH^4 + T*(-T + U) - MH^2*(T + 2*U))*DiscB[T, MT, MT])/
    (MH^2 - T)^2 + ((MH^2 + T - U)*(-MH^2 + T + U)*DiscB[U, MT, MT])/T + 
   ((MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, 0, T, MT, MT, MT])/(T*U) + 
   ((-(MH^6*MT^2) + T^2*U^2 + 3*MH^4*MT^2*(T + U) + 
      MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) - 
      MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*ScalarC0[0, 0, U, MT, MT, 
      MT])/T^2 + ((MH^10*MT^2 - MH^8*MT^2*(5*T + 3*U) + 
      T^2*U*(-(T^2*U) + MT^2*(2*T^2 + 3*T*U + U^2)) + 
      MH^6*(T^2*U + 3*MT^2*(3*T^2 + 4*T*U + U^2)) - 
      MH^4*(T^2*U*(2*T + U) + MT^2*(7*T^3 + 13*T^2*U + 9*T*U^2 + U^3)) + 
      MH^2*(T^3*U*(T + 2*U) + MT^2*T*(2*T^3 + 2*T^2*U + 3*T*U^2 + 2*U^3)))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/((MH^2 - T)*T^2*U) - 
   ((MH^2 - U)^2*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
      MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, MH^2, U, MT, MT, MT])/(T^2*U) - 
   ((MH^2 - U)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(5*T + 2*U) + 
      MT^2*(4*T^2 + 5*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
      MT])/T)/(2*(MH^2 - T - U)^3), 
 (4*(MH^2 - S)*(-MH^2 + S + T)^2 + 2*MH^2*(MH^2 - S - 2*T)*(MH^2 - S - T)*
    DiscB[MH^2, MT, MT] - 2*(MH^2 - S - T)*(MH^4 - S*T - MH^2*(S + T))*
    DiscB[S, MT, MT] + 2*(MH^2 - S)^2*(MH^2 - S - T)*DiscB[T, MT, MT] + 
   (MH^2 - S)^2*S*(MH^2 - T)*ScalarC0[0, 0, S, MT, MT, MT] + 
   (MH^2 - S)^2*(MH^2 - T)*T*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - S)*(MH^6 + S^2*T - MH^4*(2*S + 3*T) + MH^2*(S^2 + 2*S*T + 2*T^2))*
    ScalarC0[0, MH^2, S, MT, MT, MT] - (MH^2 - S)^2*(MH^2 - T)^2*
    ScalarC0[0, MH^2, T, MT, MT, MT] - (MH^2 - S)^2*(MH^2 - T)*
    (2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
     MT, MT, MT])/(2*(MH^2 - S)^2*T*(-MH^2 + S + T)^2), 
 ((2*(2*MH^2 - S - U)*DiscB[MH^2, MT, MT])/((MH^2 - S)*(MH^2 - U)*
     (MH^2 - S - U)) - (2*DiscB[S, MT, MT])/((MH^2 - S)*(MH^2 - S - U)) - 
   (2*DiscB[U, MT, MT])/((MH^2 - U)*(MH^2 - S - U)) - 
   (S*ScalarC0[0, 0, S, MT, MT, MT])/(-MH^2 + S + U)^2 - 
   (U*ScalarC0[0, 0, U, MT, MT, MT])/(-MH^2 + S + U)^2 + 
   ((MH^2 - S)*ScalarC0[0, MH^2, S, MT, MT, MT])/(-MH^2 + S + U)^2 + 
   ((MH^2 - U)*ScalarC0[0, MH^2, U, MT, MT, MT])/(-MH^2 + S + U)^2 + 
   ((2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
      MT, MT, MT])/(-MH^2 + S + U)^2)/2, 
 (4*(MH^2 - U)*(-MH^2 + T + U)^2 + 2*MH^2*(MH^2 - 2*T - U)*(MH^2 - T - U)*
    DiscB[MH^2, MT, MT] + 2*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] - 
   2*(MH^2 - T - U)*(MH^4 - T*U - MH^2*(T + U))*DiscB[U, MT, MT] + 
   (MH^2 - T)*T*(MH^2 - U)^2*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - T)*(MH^2 - U)^2*U*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*(MH^2 - U)^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   (MH^2 - U)*(MH^6 + T*U^2 - MH^4*(3*T + 2*U) + MH^2*(2*T^2 + 2*T*U + U^2))*
    ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - T)*(MH^2 - U)^2*
    (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
     MT, MT, MT])/(2*T*(MH^2 - U)^2*(-MH^2 + T + U)^2), 
 ((-2*(-MH^2 + S + T)^2)/(MH^2 - S) - 
   (2*MH^2*(3*MH^2 - 3*S - T)*(MH^2 - S - T)*DiscB[MH^2, MT, MT])/
    (MH^2 - S)^2 + (2*(MH^4 - 2*S^2 + MH^2*(S - T))*(MH^2 - S - T)*
     DiscB[S, MT, MT])/(MH^2 - S)^2 + 4*(MH^2 - S - T)*DiscB[T, MT, MT] + 
   (S*((S - T)*T + MH^2*(2*MT^2 + T) - 2*MT^2*(S + T))*
     ScalarC0[0, 0, S, MT, MT, MT])/T + 
   ((S - T)*T + MH^2*(2*MT^2 + T) - 2*MT^2*(S + T))*
    ScalarC0[0, 0, T, MT, MT, MT] + 
   ((MH^6*(2*MT^2 - T) + S^2*T*(-S + T) - 2*MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 
        2*T^3) + MH^4*(T*(S + T) - 2*MT^2*(3*S + 5*T)) + 
      MH^2*(S*(S - 2*T)*T + 2*MT^2*(3*S^2 + 10*S*T + 6*T^2)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*T) - 
   ((MH^2 - T)*((S - T)*T + MH^2*(2*MT^2 + T) - 2*MT^2*(S + T))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/T - 
   (2*MH^4*MT^2 + S*(S - T)*T + MH^2*(2*MT^2*(S - 2*T) + S*T) - 
     2*MT^2*(2*S^2 + S*T - T^2))*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, 
     MT])/(4*(MH^2 - S - T)^3), ((-2*(-MH^2 + S + U)^2)/(MH^2 - S) - 
   (2*MH^2*(3*MH^2 - 3*S - U)*(MH^2 - S - U)*DiscB[MH^2, MT, MT])/
    (MH^2 - S)^2 + (2*(MH^4 - 2*S^2 + MH^2*(S - U))*(MH^2 - S - U)*
     DiscB[S, MT, MT])/(MH^2 - S)^2 + 4*(MH^2 - S - U)*DiscB[U, MT, MT] + 
   (S*((S - U)*U + MH^2*(2*MT^2 + U) - 2*MT^2*(S + U))*
     ScalarC0[0, 0, S, MT, MT, MT])/U + 
   ((S - U)*U + MH^2*(2*MT^2 + U) - 2*MT^2*(S + U))*
    ScalarC0[0, 0, U, MT, MT, MT] + 
   ((MH^6*(2*MT^2 - U) + S^2*U*(-S + U) - 2*MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 
        2*U^3) + MH^4*(U*(S + U) - 2*MT^2*(3*S + 5*U)) + 
      MH^2*(S*(S - 2*U)*U + 2*MT^2*(3*S^2 + 10*S*U + 6*U^2)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*U) - 
   ((MH^2 - U)*((S - U)*U + MH^2*(2*MT^2 + U) - 2*MT^2*(S + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/U - 
   (2*MH^4*MT^2 + S*(S - U)*U + MH^2*(2*MT^2*(S - 2*U) + S*U) - 
     2*MT^2*(2*S^2 + S*U - U^2))*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, 
     MT])/(4*(MH^2 - S - U)^3), 
 ((2*(-MH^2 + T + U)^2)/T + (2*MH^2*(MH^2 - T - U)*(MH^2 + T - U)*
     DiscB[MH^2, MT, MT])/(T*(MH^2 - U)) - 4*(MH^2 - T - U)*
    DiscB[T, MT, MT] - (2*(-MH^2 + T + U)*(-MH^4 - 2*T*U + MH^2*(T + U))*
     DiscB[U, MT, MT])/(T*(MH^2 - U)) + 
   ((-2*MH^6*MT^2 + T^2*(T - U)*U + 2*MH^4*MT^2*(3*T + 4*U) + 
      2*MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3) - 
      MH^2*(T^2*U + 2*MT^2*(3*T^2 + 9*T*U + 5*U^2)))*
     ScalarC0[0, 0, T, MT, MT, MT])/(T*U) + 
   ((2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
      T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
      MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
     ScalarC0[0, 0, U, MT, MT, MT])/T^2 - 
   ((MH^2 - T)*(2*MH^6*MT^2 - 2*MH^4*MT^2*(3*T + 2*U) - 
      T^2*(U*(-T + U) + 2*MT^2*(T + U)) + 
      MH^2*(-(T^2*U) + 2*MT^2*(3*T^2 + 3*T*U + U^2)))*
     ScalarC0[0, MH^2, T, MT, MT, MT])/(T^2*U) + 
   ((2*MH^8*MT^2 - 6*MH^6*MT^2*(T + U) + T^2*U*((T - U)*U - 2*MT^2*(T + U)) + 
      MH^4*(T^2*U + 6*MT^2*(T + U)^2) - 
      MH^2*(T^3*U + 2*MT^2*(T^3 + 2*T^2*U + 3*T*U^2 + U^3)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(T^2*U) + 
   ((-2*MH^6*MT^2 + 4*MH^4*MT^2*(2*T + U) + 
      T*(T*U*(-T + U) + 2*MT^2*(2*T^2 + T*U - U^2)) + 
      MH^2*(T^2*U - 2*MT^2*(5*T^2 + 3*T*U + U^2)))*ScalarD0[0, 0, 0, MH^2, T, 
      U, MT, MT, MT, MT])/T)/(4*(MH^2 - T - U)^3), 
 ((T*(-MH^2 + S + T)^2*(3*MH^4 - MH^2*(S + 3*T) - S*(2*S + 3*T)))/
    (MH^2 - S)^2 + (MH^2*(MH^2 - S - T)*T*(MH^4*(3*S - T) + 
      MH^2*(-6*S^2 - 9*S*T + T^2) + S*(3*S^2 + 10*S*T + 5*T^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - S)^3 + 
   (S*T*(-MH^2 + S + T)*(3*MH^6 + S*T*(3*S + T) - 2*MH^4*(3*S + 4*T) + 
      MH^2*(3*S^2 + 5*S*T + 5*T^2))*DiscB[S, MT, MT])/(MH^2 - S)^3 + 
   (MH^2 - S - T)*(MH^2 + S - T)*T*DiscB[T, MT, MT] + 
   S*(MH^2 - T)*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, 
     MT] + (MH^2 - T)*T*(MH^2*MT^2 + S*T - MT^2*(S + T))*
    ScalarC0[0, 0, T, MT, MT, MT] + 
   ((MH^10*MT^2 + MH^8*(S*T - 2*MT^2*(2*S + T)) + 
      MH^6*(-(S*T*(3*S + 5*T)) + MT^2*(6*S^2 + 7*S*T - T^2)) + 
      MH^4*(3*S*T*(S^2 + 3*S*T + 2*T^2) - MT^2*(4*S^3 + 9*S^2*T + 3*S*T^2 - 
          4*T^3)) - S*T*(S^3*T + MT^2*(S^3 + 5*S^2*T + 6*S*T^2 + 2*T^3)) + 
      MH^2*(-(S*T*(S^3 + 3*S^2*T + 6*S*T^2 + 2*T^3)) + 
        MT^2*(S^4 + 5*S^3*T + 9*S^2*T^2 + 2*S*T^3 - 2*T^4)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/(MH^2 - S)^2 - 
   (MH^2 - T)^2*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] - S*(MH^2 - T)*T*(3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*
    ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(2*(MH^2 - S - T)^3*T^2), 
 ((-MH^2 + S + U)^2/(MH^2 - S) + 
   ((MH^2 - S - U)*(MH^6 + MH^4*(2*S - U) + S*(S + U)^2 - 2*MH^2*S*(2*S + U))*
     DiscB[MH^2, MT, MT])/((MH^2 - S)^2*(MH^2 - U)) - 
   (S*(-MH^2 + S + U)*(-3*MH^2 + 3*S + U)*DiscB[S, MT, MT])/(MH^2 - S)^2 - 
   ((MH^2 - S - U)*(MH^2 + S - U)*DiscB[U, MT, MT])/(MH^2 - U) + 
   (S*(-(MH^2*MT^2) - S*U + MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, MT])/U - 
   (MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   ((MH^6*MT^2 - S^3*U - MT^2*(S^3 + 5*S^2*U + 6*S*U^2 + 2*U^3) - 
      MH^4*(S*U + MT^2*(3*S + 5*U)) + 
      MH^2*(2*S^2*U + MT^2*(3*S^2 + 10*S*U + 6*U^2)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*U) + 
   ((MH^2 - U)*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, MH^2, U, MT, MT, 
      MT])/U + S*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*
    ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(2*(MH^2 - S - U)^3), 
 -(-((T*(-MH^2 + T + U)^2*(-8*MH^4 - U*(2*T + 3*U) + MH^2*(8*T + 11*U)))/
      (MH^2 - U)^2) + (MH^2*T*(MH^2 - T - U)*(3*MH^6 + 2*T*U^2 - 
       3*MH^4*(3*T + 2*U) + MH^2*(6*T^2 + 7*T*U + 3*U^2))*
      DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T + U)*
     (-3*MH^2 + 3*T + U)*DiscB[T, MT, MT] + 
    (T*(-MH^2 + T + U)*(3*MH^8 + T*U^2*(-T + U) - 6*MH^6*(T + U) + 
       MH^2*T*U*(4*T + 3*U) + MH^4*(3*T^2 + 2*T*U + 3*U^2))*DiscB[U, MT, MT])/
     (MH^2 - U)^3 + (T*(-(MH^6*MT^2) + T^3*U + 
       MT^2*(T^3 + 5*T^2*U + 6*T*U^2 + 2*U^3) + 
       MH^4*(T*U + MT^2*(3*T + 5*U)) - 
       MH^2*(2*T^2*U + MT^2*(3*T^2 + 10*T*U + 6*U^2)))*
      ScalarC0[0, 0, T, MT, MT, MT])/U + (MH^2 - T)^2*
     (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
    ((MH^2 - T)^3*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, 
       MT, MT])/U + ((MH^12*MT^2 + T^2*U^3*(T*U - MT^2*(T + U)) + 
       MH^10*(T*U - MT^2*(3*T + 4*U)) + MH^8*(-(T*U*(4*T + 3*U)) + 
         MT^2*(3*T^2 + 11*T*U + 6*U^2)) + 
       MH^4*U*(-(T*(2*T^3 + 3*T^2*U + U^3)) + MT^2*(11*T^3 + 18*T^2*U + 
           9*T*U^2 + U^3)) - MH^2*T*U*(T*U^2*(3*T + 2*U) + 
         MT^2*(4*T^3 + 9*T^2*U + 6*T*U^2 + 2*U^3)) - 
       MH^6*(-(T*U*(5*T^2 + 6*T*U + 3*U^2)) + MT^2*(T^3 + 14*T^2*U + 
           15*T*U^2 + 4*U^3)))*ScalarC0[0, MH^2, U, MT, MT, MT])/
     ((MH^2 - U)^2*U) - (MH^2 - T)^2*T*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 -(4*(MH^2 - U)*(-MH^2 + T + U)^2 - 2*(MH^2 - T - U)*
     (MH^2*(T - U) + U*(T + U))*DiscB[MH^2, MT, MT] + 
    2*(MH^2 - U)^2*(MH^2 - T - U)*DiscB[T, MT, MT] - 
    2*(MH^2 - 2*T - U)*(MH^2 - T - U)*U*DiscB[U, MT, MT] + 
    T*(MH^2 - U)^2*U*ScalarC0[0, 0, T, MT, MT, MT] + 
    (MH^2 - U)^2*U^2*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)*(MH^2 - U)^2*U*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^2 - U)*U*(MH^4 + 2*T^2 + 4*T*U + U^2 - 2*MH^2*(2*T + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT] - (MH^2 - U)^2*U*
     (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
      MT, MT, MT])/(2*T*(MH^2 - U)^2*(-MH^2 + T + U)^2), 
 ((-MH^2 + S + T)^2/(MH^2 - S) + (MH^2*(MH^4 - 2*S^2 + MH^2*(S - T))*
     (MH^2 - S - T)*DiscB[MH^2, MT, MT])/((MH^2 - S)^2*S) - 
   ((MH^2 - S - T)*(2*MH^4 + S*(-S + T) - MH^2*(S + 2*T))*DiscB[S, MT, MT])/
    (MH^2 - S)^2 + ((MH^2 + S - T)*(-MH^2 + S + T)*DiscB[T, MT, MT])/S + 
   ((MH^2 - T)*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
      MT^2*(2*S^2 + 3*S*T + T^2))*ScalarC0[0, 0, S, MT, MT, MT])/(S*T) + 
   ((-(MH^6*MT^2) + S^2*T^2 + 3*MH^4*MT^2*(S + T) + 
      MT^2*(2*S^3 + 4*S^2*T + 3*S*T^2 + T^3) - 
      MH^2*(S^2*T + MT^2*(4*S^2 + 6*S*T + 3*T^2)))*ScalarC0[0, 0, T, MT, MT, 
      MT])/S^2 + ((MH^10*MT^2 - MH^8*MT^2*(5*S + 3*T) + 
      S^2*T*(-(S^2*T) + MT^2*(2*S^2 + 3*S*T + T^2)) + 
      MH^6*(S^2*T + 3*MT^2*(3*S^2 + 4*S*T + T^2)) - 
      MH^4*(S^2*T*(2*S + T) + MT^2*(7*S^3 + 13*S^2*T + 9*S*T^2 + T^3)) + 
      MH^2*(S^3*T*(S + 2*T) + MT^2*S*(2*S^3 + 2*S^2*T + 3*S*T^2 + 2*T^3)))*
     ScalarC0[0, MH^2, S, MT, MT, MT])/((MH^2 - S)*S^2*T) - 
   ((MH^2 - T)^2*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(3*S + 2*T) + 
      MT^2*(2*S^2 + 3*S*T + T^2))*ScalarC0[0, MH^2, T, MT, MT, MT])/(S^2*T) - 
   ((MH^2 - T)*(MH^4*MT^2 - S^2*T - MH^2*MT^2*(5*S + 2*T) + 
      MT^2*(4*S^2 + 5*S*T + T^2))*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, 
      MT])/S)/(2*(MH^2 - S - T)^3), 
 -((-MH^2 + S + U)^2/S + (MH^2*(MH^2 - S - U)*(-S^2 - U^2 + MH^2*(S + U))*
      DiscB[MH^2, MT, MT])/((MH^2 - S)*S*(MH^2 - U)) - 
    ((MH^2 - S - U)*(MH^2 - S + U)*DiscB[S, MT, MT])/(MH^2 - S) + 
    ((MH^2 + S - U)*U*(-MH^2 + S + U)*DiscB[U, MT, MT])/(S*(MH^2 - U)) + 
    ((MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + MT^2*(2*S^2 + 3*S*U + U^2))*
      ScalarC0[0, 0, S, MT, MT, MT])/S + 
    (U*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
       MT^2*(2*S^2 + 3*S*U + U^2))*ScalarC0[0, 0, U, MT, MT, MT])/S^2 - 
    ((MH^2 - S)*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(3*S + 2*U) + 
       MT^2*(2*S^2 + 3*S*U + U^2))*ScalarC0[0, MH^2, S, MT, MT, MT])/S^2 + 
    ((MH^6*MT^2 - S^2*U^2 - 3*MH^4*MT^2*(S + U) - 
       MT^2*(2*S^3 + 4*S^2*U + 3*S*U^2 + U^3) + 
       MH^2*(S^2*U + MT^2*(4*S^2 + 6*S*U + 3*U^2)))*ScalarC0[0, MH^2, U, MT, 
       MT, MT])/S^2 - (U*(MH^4*MT^2 - S^2*U - MH^2*MT^2*(5*S + 2*U) + 
       MT^2*(4*S^2 + 5*S*U + U^2))*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, 
       MT])/S)/(2*(MH^2 - S - U)^3), 
 -((-MH^2 + T + U)^2/T + (MH^2*(MH^2 - T - U)*(-T^2 - U^2 + MH^2*(T + U))*
      DiscB[MH^2, MT, MT])/((MH^2 - T)*T*(MH^2 - U)) - 
    ((MH^2 - T - U)*(MH^2 - T + U)*DiscB[T, MT, MT])/(MH^2 - T) + 
    ((MH^2 + T - U)*U*(-MH^2 + T + U)*DiscB[U, MT, MT])/(T*(MH^2 - U)) + 
    ((MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + MT^2*(2*T^2 + 3*T*U + U^2))*
      ScalarC0[0, 0, T, MT, MT, MT])/T + 
    (U*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, 0, U, MT, MT, MT])/T^2 - 
    ((MH^2 - T)*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(3*T + 2*U) + 
       MT^2*(2*T^2 + 3*T*U + U^2))*ScalarC0[0, MH^2, T, MT, MT, MT])/T^2 + 
    ((MH^6*MT^2 - T^2*U^2 - 3*MH^4*MT^2*(T + U) - 
       MT^2*(2*T^3 + 4*T^2*U + 3*T*U^2 + U^3) + 
       MH^2*(T^2*U + MT^2*(4*T^2 + 6*T*U + 3*U^2)))*ScalarC0[0, MH^2, U, MT, 
       MT, MT])/T^2 - (U*(MH^4*MT^2 - T^2*U - MH^2*MT^2*(5*T + 2*U) + 
       MT^2*(4*T^2 + 5*T*U + U^2))*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, 
       MT])/T)/(2*(MH^2 - T - U)^3), 
 -((T*(-MH^2 + S + T)^2*(8*MH^4 + S*(3*S + 2*T) - MH^2*(11*S + 8*T)))/
     (MH^2 - S)^2 + (MH^2*(MH^2 - S - T)*T*(3*MH^6 + 2*S^2*T - 
       3*MH^4*(2*S + 3*T) + MH^2*(3*S^2 + 7*S*T + 6*T^2))*
      DiscB[MH^2, MT, MT])/(MH^2 - S)^3 - 
    ((MH^2 - S - T)*T*(3*MH^8 + S^2*(S - T)*T - 6*MH^6*(S + T) + 
       MH^2*S*T*(3*S + 4*T) + MH^4*(3*S^2 + 2*S*T + 3*T^2))*DiscB[S, MT, MT])/
     (MH^2 - S)^3 + (3*MH^2 - S - 3*T)*(MH^2 - S - T)*T*DiscB[T, MT, MT] + 
    (MH^2 - T)^2*(MH^2*MT^2 + S*T - MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, 
      MT] + (T*(-(MH^6*MT^2) + S*T^3 + MT^2*(2*S^3 + 6*S^2*T + 5*S*T^2 + 
         T^3) + MH^4*(S*T + MT^2*(5*S + 3*T)) - 
       MH^2*(2*S*T^2 + MT^2*(6*S^2 + 10*S*T + 3*T^2)))*
      ScalarC0[0, 0, T, MT, MT, MT])/S + 
    ((MH^12*MT^2 + S^3*T^2*(S*T - MT^2*(S + T)) + 
       MH^10*(S*T - MT^2*(4*S + 3*T)) + MH^8*(-(S*T*(3*S + 4*T)) + 
         MT^2*(6*S^2 + 11*S*T + 3*T^2)) - 
       MH^6*(-(S*T*(3*S^2 + 6*S*T + 5*T^2)) + MT^2*(4*S^3 + 15*S^2*T + 
           14*S*T^2 + T^3)) - MH^2*S*T*(S^2*T*(2*S + 3*T) + 
         MT^2*(2*S^3 + 6*S^2*T + 9*S*T^2 + 4*T^3)) + 
       MH^4*S*(-(T*(S^3 + 3*S*T^2 + 2*T^3)) + MT^2*(S^3 + 9*S^2*T + 
           18*S*T^2 + 11*T^3)))*ScalarC0[0, MH^2, S, MT, MT, MT])/
     ((MH^2 - S)^2*S) - ((MH^2 - T)^3*(MH^2*MT^2 + S*T - MT^2*(S + T))*
      ScalarC0[0, MH^2, T, MT, MT, MT])/S - (MH^2 - T)^2*T*
     (3*MH^2*MT^2 + S*T - 3*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
      MT, MT, MT])/(2*(MH^2 - S - T)^3*T^2), 
 ((-MH^2 + S + U)^2/(MH^2 - U) + 
   ((MH^2 - S - U)*(MH^6 - MH^4*(S - 2*U) + U*(S + U)^2 - 2*MH^2*U*(S + 2*U))*
     DiscB[MH^2, MT, MT])/((MH^2 - S)*(MH^2 - U)^2) - 
   ((MH^2 - S - U)*(MH^2 - S + U)*DiscB[S, MT, MT])/(MH^2 - S) - 
   ((3*MH^2 - S - 3*U)*(MH^2 - S - U)*U*DiscB[U, MT, MT])/(MH^2 - U)^2 - 
   (MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, MT] + 
   (U*(-(MH^2*MT^2) - S*U + MT^2*(S + U))*ScalarC0[0, 0, U, MT, MT, MT])/S + 
   ((MH^2 - S)*(MH^2*MT^2 + S*U - MT^2*(S + U))*ScalarC0[0, MH^2, S, MT, MT, 
      MT])/S + ((-(MH^6*MT^2) + S*U^3 + MT^2*(2*S^3 + 6*S^2*U + 5*S*U^2 + 
        U^3) + MH^4*(S*U + MT^2*(5*S + 3*U)) - 
      MH^2*(2*S*U^2 + MT^2*(6*S^2 + 10*S*U + 3*U^2)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(S*(MH^2 - U)) + 
   U*(3*MH^2*MT^2 + S*U - 3*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
     MT, MT, MT])/(2*(MH^2 - S - U)^3), 
 (-((T*(-MH^2 + T + U)^2*(-3*MH^4 + MH^2*(3*T + U) + U*(3*T + 2*U)))/
     (MH^2 - U)^2) - (MH^2*T*(MH^2 - T - U)*(MH^4*(T - 3*U) - 
      U*(5*T^2 + 10*T*U + 3*U^2) + MH^2*(-T^2 + 9*T*U + 6*U^2))*
     DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T - U)*(-MH^2 + T + U)*
    DiscB[T, MT, MT] + (T*U*(-MH^2 + T + U)*(3*MH^6 + T*U*(T + 3*U) - 
      2*MH^4*(4*T + 3*U) + MH^2*(5*T^2 + 5*T*U + 3*U^2))*DiscB[U, MT, MT])/
    (MH^2 - U)^3 + (MH^2 - T)*T*(MH^2*MT^2 + T*U - MT^2*(T + U))*
    ScalarC0[0, 0, T, MT, MT, MT] + (MH^2 - T)*U*
    (MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - T)^2*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] + ((MH^10*MT^2 + MH^8*(T*U - 2*MT^2*(T + 2*U)) - 
      MH^6*(T*U*(5*T + 3*U) + MT^2*(T^2 - 7*T*U - 6*U^2)) + 
      MH^4*(3*T*U*(2*T^2 + 3*T*U + U^2) + MT^2*(4*T^3 - 3*T^2*U - 9*T*U^2 - 
          4*U^3)) - T*U*(T*U^3 + MT^2*(2*T^3 + 6*T^2*U + 5*T*U^2 + U^3)) + 
      MH^2*(-(T*U*(2*T^3 + 6*T^2*U + 3*T*U^2 + U^3)) + 
        MT^2*(-2*T^4 + 2*T^3*U + 9*T^2*U^2 + 5*T*U^3 + U^4)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 - 
   (MH^2 - T)*T*U*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 (-((T*(-MH^2 + T + U)^2*(-2*MH^4 - U*(8*T + 7*U) + MH^2*(2*T + 9*U)))/
     (MH^2 - U)^2) - (T*(-MH^2 + T + U)*(-(MH^6*T) - 3*U^2*(T + U)^2 + 
      MH^4*(T^2 + 7*T*U - 3*U^2) + MH^2*(-4*T^2*U + 6*U^3))*
     DiscB[MH^2, MT, MT])/(MH^2 - U)^3 + T*(-MH^2 + T + U)*(-MH^2 + T + 3*U)*
    DiscB[T, MT, MT] + (T*U^2*(-MH^2 + T + U)*(3*MH^4 + 6*T^2 + 11*T*U + 
      3*U^2 - MH^2*(11*T + 6*U))*DiscB[U, MT, MT])/(-MH^2 + U)^3 - 
   T*U*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, 0, T, MT, MT, MT] + 
   U^2*(-(MH^2*MT^2) - T*U + MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - T)*U*(MH^2*MT^2 + T*U - MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, 
     MT] - (U*(MH^8*MT^2 - T*U*(2*T^3 + 6*T^2*U + 6*T*U^2 + U^3) + 
      MT^2*(-4*T^4 - 10*T^3*U - 6*T^2*U^2 + T*U^3 + U^4) + 
      MH^6*(T*U - MT^2*(T + 4*U)) - 3*MH^4*(T*U*(2*T + U) + 
        MT^2*(2*T^2 - T*U - 2*U^2)) + MH^2*(3*T*U*(2*T^2 + 4*T*U + U^2) + 
        MT^2*(10*T^3 + 12*T^2*U - 3*T*U^2 - 4*U^3)))*
     ScalarC0[0, MH^2, U, MT, MT, MT])/(MH^2 - U)^2 + 
   T*U^2*(3*MH^2*MT^2 + T*U - 3*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, 
     MT, MT, MT, MT])/(2*T^2*(MH^2 - T - U)^3), 
 (S*ScalarC0[0, 0, S, MT, MT, MT] + T*ScalarC0[0, 0, T, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT] + 
   4*MT^2*S*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT] + 
   4*MT^2*T*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT] - 
   S*T*ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/(4*(-MH^2 + S + T)), 
 (S*ScalarC0[0, 0, S, MT, MT, MT] + U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   S*ScalarC0[0, MH^2, S, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT] + 
   4*MT^2*S*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT] + 
   4*MT^2*U*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT] - 
   S*U*ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/(4*(-MH^2 + S + U)), 
 (T*ScalarC0[0, 0, T, MT, MT, MT] + U*ScalarC0[0, 0, U, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   T*ScalarC0[0, MH^2, T, MT, MT, MT] - 
   MH^2*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   U*ScalarC0[0, MH^2, U, MT, MT, MT] - 
   4*MH^2*MT^2*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
   4*MT^2*T*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] + 
   4*MT^2*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT] - 
   T*U*ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/(4*(-MH^2 + T + U)), 
 (2*MH^2*S*(MH^2 - S - T)*DiscB[MH^2, MT, MT] + 
   2*S*(MH^2 - T)*(-MH^2 + S + T)*DiscB[S, MT, MT] + 
   2*S*T*(-MH^2 + S + T)*DiscB[T, MT, MT] - 
   S*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
    ScalarC0[0, 0, S, MT, MT, MT] - (MH^2 - T)*T*(2*MH^2*MT^2 + S*T - 
     2*MT^2*(S + T))*ScalarC0[0, 0, T, MT, MT, MT] + 
   (MH^2 - S)*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
    ScalarC0[0, MH^2, S, MT, MT, MT] - 
   (MH^2 - T)*(2*MH^4*MT^2 + S*T^2 + 2*MT^2*(2*S^2 + 3*S*T + T^2) - 
     MH^2*(S*T + MT^2*(6*S + 4*T)))*ScalarC0[0, MH^2, T, MT, MT, MT] + 
   S*(MH^2 - T)*T*(4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*
    ScalarD0[0, 0, 0, MH^2, S, T, MT, MT, MT, MT])/
  (8*S*(MH^2 - T)*(-MH^2 + S + T)^2), 
 -(2*MH^2*S*(MH^2 - S - U)*U*DiscB[MH^2, MT, MT] + 
    2*S^2*U*(-MH^2 + S + U)*DiscB[S, MT, MT] - 2*(MH^2 - S)*S*(MH^2 - S - U)*
     U*DiscB[U, MT, MT] - (MH^2 - S)*S*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 
      2*MT^2*(S + U))*ScalarC0[0, 0, S, MT, MT, MT] + 
    (MH^2 - S)*U*(2*MH^4*MT^2 + S*U^2 + 2*MT^2*(2*S^2 + 3*S*U + U^2) - 
      MH^2*(S*U + MT^2*(6*S + 4*U)))*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - S)*(2*MH^6*MT^2 + S*U*(-(S*U) + 2*MT^2*(S + U)) - 
      MH^4*(S*U + 4*MT^2*(S + U)) + MH^2*(S*U*(S + U) + 
        2*MT^2*(S^2 + S*U + U^2)))*ScalarC0[0, MH^2, S, MT, MT, MT] + 
    (MH^2 - S)*(MH^2 - U)^2*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT] + (MH^2 - S)*S*(MH^2 - U)*U*
     (4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
      MT, MT, MT])/(8*(MH^2 - S)*S*U*(-MH^2 + S + U)^2), 
 -(2*MH^2*T*(MH^2 - T - U)*U*DiscB[MH^2, MT, MT] + 
    2*T^2*U*(-MH^2 + T + U)*DiscB[T, MT, MT] - 2*(MH^2 - T)*T*(MH^2 - T - U)*
     U*DiscB[U, MT, MT] - (MH^2 - T)*T*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 
      2*MT^2*(T + U))*ScalarC0[0, 0, T, MT, MT, MT] + 
    (MH^2 - T)*U*(2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
      MH^2*(T*U + MT^2*(6*T + 4*U)))*ScalarC0[0, 0, U, MT, MT, MT] - 
    (MH^2 - T)*(2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
      MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
        2*MT^2*(T^2 + T*U + U^2)))*ScalarC0[0, MH^2, T, MT, MT, MT] + 
    (MH^2 - T)*(MH^2 - U)^2*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarC0[0, MH^2, U, MT, MT, MT] + (MH^2 - T)*T*(MH^2 - U)*U*
     (4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*ScalarD0[0, 0, 0, MH^2, T, U, MT, 
      MT, MT, MT])/(8*(MH^2 - T)*T*U*(-MH^2 + T + U)^2), 
 (2*MH^2*(MH^2 - S - T)*T*DiscB[MH^2, MT, MT] + 2*S*T*(-MH^2 + S + T)*
    DiscB[S, MT, MT] - 2*(MH^2 - S)*(MH^2 - S - T)*T*DiscB[T, MT, MT] - 
   (MH^2 - S)*S*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
    ScalarC0[0, 0, S, MT, MT, MT] - (MH^2 - S)*T*(2*MH^2*MT^2 + S*T - 
     2*MT^2*(S + T))*ScalarC0[0, 0, T, MT, MT, MT] - 
   (MH^2 - S)*(2*MH^4*MT^2 + S^2*T + 2*MT^2*(S^2 + 3*S*T + 2*T^2) - 
     MH^2*(S*T + MT^2*(4*S + 6*T)))*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   (MH^2 - S)*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
    ScalarC0[0, MH^2, T, MT, MT, MT] + (MH^2 - S)*S*T*
    (4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
     MT, MT, MT])/(8*(MH^2 - S)*T*(-MH^2 + S + T)^2), 
 (2*MH^2*(MH^2 - S - U)*U*DiscB[MH^2, MT, MT] + 2*S*U*(-MH^2 + S + U)*
    DiscB[S, MT, MT] - 2*(MH^2 - S)*(MH^2 - S - U)*U*DiscB[U, MT, MT] - 
   (MH^2 - S)*S*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
    ScalarC0[0, 0, S, MT, MT, MT] - (MH^2 - S)*U*(2*MH^2*MT^2 + S*U - 
     2*MT^2*(S + U))*ScalarC0[0, 0, U, MT, MT, MT] - 
   (MH^2 - S)*(2*MH^4*MT^2 + S^2*U + 2*MT^2*(S^2 + 3*S*U + 2*U^2) - 
     MH^2*(S*U + MT^2*(4*S + 6*U)))*ScalarC0[0, MH^2, S, MT, MT, MT] + 
   (MH^2 - S)*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
    ScalarC0[0, MH^2, U, MT, MT, MT] + (MH^2 - S)*S*U*
    (4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*ScalarD0[0, 0, 0, MH^2, S, U, MT, 
     MT, MT, MT])/(8*(MH^2 - S)*U*(-MH^2 + S + U)^2), 
 -(2*MH^2*T*(MH^2 - T - U)*U*DiscB[MH^2, MT, MT] + 
    2*T*(MH^2 - U)*U*(-MH^2 + T + U)*DiscB[T, MT, MT] + 
    2*T*U^2*(-MH^2 + T + U)*DiscB[U, MT, MT] + 
    T*(MH^2 - U)*(2*MH^4*MT^2 + T^2*U + 2*MT^2*(T^2 + 3*T*U + 2*U^2) - 
      MH^2*(T*U + MT^2*(4*T + 6*U)))*ScalarC0[0, 0, T, MT, MT, MT] - 
    (MH^2 - T)*(MH^2 - U)*U*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
     ScalarC0[0, 0, U, MT, MT, MT] + (MH^2 - T)^2*(MH^2 - U)*
     (2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*ScalarC0[0, MH^2, T, MT, MT, MT] - 
    (MH^2 - U)*(2*MH^6*MT^2 + T*U*(-(T*U) + 2*MT^2*(T + U)) - 
      MH^4*(T*U + 4*MT^2*(T + U)) + MH^2*(T*U*(T + U) + 
        2*MT^2*(T^2 + T*U + U^2)))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
    (MH^2 - T)*T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
     ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (8*T*(MH^2 - U)*U*(-MH^2 + T + U)^2), 
 -(2*MH^2*S*(MH^2 - S - T)*T*DiscB[MH^2, MT, MT] + 
    2*S^2*T*(-MH^2 + S + T)*DiscB[S, MT, MT] - 2*(MH^2 - S)*S*(MH^2 - S - T)*
     T*DiscB[T, MT, MT] - (MH^2 - S)*S*(MH^2 - T)*(2*MH^2*MT^2 + S*T - 
      2*MT^2*(S + T))*ScalarC0[0, 0, S, MT, MT, MT] + 
    (MH^2 - S)*T*(2*MH^4*MT^2 + S*T^2 + 2*MT^2*(2*S^2 + 3*S*T + T^2) - 
      MH^2*(S*T + MT^2*(6*S + 4*T)))*ScalarC0[0, 0, T, MT, MT, MT] - 
    (MH^2 - S)*(2*MH^6*MT^2 + S*T*(-(S*T) + 2*MT^2*(S + T)) - 
      MH^4*(S*T + 4*MT^2*(S + T)) + MH^2*(S*T*(S + T) + 
        2*MT^2*(S^2 + S*T + T^2)))*ScalarC0[0, MH^2, S, MT, MT, MT] + 
    (MH^2 - S)*(MH^2 - T)^2*(2*MH^2*MT^2 + S*T - 2*MT^2*(S + T))*
     ScalarC0[0, MH^2, T, MT, MT, MT] + (MH^2 - S)*S*(MH^2 - T)*T*
     (4*MH^2*MT^2 + S*T - 4*MT^2*(S + T))*ScalarD0[0, 0, 0, MH^2, S, T, MT, 
      MT, MT, MT])/(8*(MH^2 - S)*S*T*(-MH^2 + S + T)^2), 
 (2*MH^2*S*(MH^2 - S - U)*DiscB[MH^2, MT, MT] + 
   2*S*(MH^2 - U)*(-MH^2 + S + U)*DiscB[S, MT, MT] + 
   2*S*U*(-MH^2 + S + U)*DiscB[U, MT, MT] - 
   S*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
    ScalarC0[0, 0, S, MT, MT, MT] - (MH^2 - U)*U*(2*MH^2*MT^2 + S*U - 
     2*MT^2*(S + U))*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - S)*(MH^2 - U)*(2*MH^2*MT^2 + S*U - 2*MT^2*(S + U))*
    ScalarC0[0, MH^2, S, MT, MT, MT] - 
   (MH^2 - U)*(2*MH^4*MT^2 + S*U^2 + 2*MT^2*(2*S^2 + 3*S*U + U^2) - 
     MH^2*(S*U + MT^2*(6*S + 4*U)))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   S*(MH^2 - U)*U*(4*MH^2*MT^2 + S*U - 4*MT^2*(S + U))*
    ScalarD0[0, 0, 0, MH^2, S, U, MT, MT, MT, MT])/
  (8*S*(MH^2 - U)*(-MH^2 + S + U)^2), 
 (2*MH^2*T*(MH^2 - T - U)*DiscB[MH^2, MT, MT] + 
   2*T*(MH^2 - U)*(-MH^2 + T + U)*DiscB[T, MT, MT] + 
   2*T*U*(-MH^2 + T + U)*DiscB[U, MT, MT] - 
   T*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarC0[0, 0, T, MT, MT, MT] - (MH^2 - U)*U*(2*MH^2*MT^2 + T*U - 
     2*MT^2*(T + U))*ScalarC0[0, 0, U, MT, MT, MT] + 
   (MH^2 - T)*(MH^2 - U)*(2*MH^2*MT^2 + T*U - 2*MT^2*(T + U))*
    ScalarC0[0, MH^2, T, MT, MT, MT] - 
   (MH^2 - U)*(2*MH^4*MT^2 + T*U^2 + 2*MT^2*(2*T^2 + 3*T*U + U^2) - 
     MH^2*(T*U + MT^2*(6*T + 4*U)))*ScalarC0[0, MH^2, U, MT, MT, MT] + 
   T*(MH^2 - U)*U*(4*MH^2*MT^2 + T*U - 4*MT^2*(T + U))*
    ScalarD0[0, 0, 0, MH^2, T, U, MT, MT, MT, MT])/
  (8*T*(MH^2 - U)*(-MH^2 + T + U)^2)}
