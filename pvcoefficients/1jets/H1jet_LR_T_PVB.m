(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] -> 2 + Eps^(-1) + Log[Mu^2/MT^2] + 
   (Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/
    U, PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + Log[Mu^2/MT^2] + 
   (Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
    S, PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + Log[Mu^2/MT^2] + 
   (Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/
    T}
