(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]] -> 2 + Eps^(-1) + Log[Mu^2/MT^2] + 
   (Sqrt[S*(-4*MT^2 + S)]*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
    S}
