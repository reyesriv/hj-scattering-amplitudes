(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVC[0, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   (DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
     DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
     DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
     DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
     DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (-(MH^2*MZ^2) + MZ^4 - MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
     DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (-(MH^2*MZ^2) + MZ^4 - MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
     DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
           Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
     DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
           Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
     DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
           MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
         S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
           S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
          Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
     DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
       (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
           MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
         S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
           S]]))/(-(MH^2*S) - MZ^2*S + S^2 + 
        Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
      MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]])/
    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]], Kallen\[Lambda][MH^2, MZ^2, S] > 
    0], PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   ((MH^2*(MH^2 - MZ^2 - S)*
       (-DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] + DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] + DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
      Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] - 2*Sqrt[MH^4 - 4*MH^2*MT^2]*
      Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
     (Sqrt[-4*MT^2*MZ^2 + MZ^4]*(MH^2 + MZ^2 - S)*
       Log[(2*MT^2 - MZ^2 + Sqrt[-4*MT^2*MZ^2 + MZ^4])/(2*MT^2)])/MZ^2 + 
     (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S)/
    Kallen\[Lambda][MH^2, MZ^2, S], Kallen\[Lambda][MH^2, MZ^2, S] > 0], 
 PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   ((MZ^2*(-MH^2 + MZ^2 - S)*
       (-DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] + DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] + DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
      Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + 
     (Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 + MZ^2 - S)*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/MH^2 - 
     2*Sqrt[-4*MT^2*MZ^2 + MZ^4]*
      Log[(2*MT^2 - MZ^2 + Sqrt[-4*MT^2*MZ^2 + MZ^4])/(2*MT^2)] + 
     (Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + MZ^2 + S)*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S)/
    Kallen\[Lambda][MH^2, MZ^2, S], Kallen\[Lambda][MH^2, MZ^2, S] > 0], 
 PVC[0, 1, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   -((2*(MH^6*(MT^2 + MZ^2) + MT^2*(MZ^2 - S)^3 + MH^2*(MZ^2 - S)*
          (MZ^4 + 2*MZ^2*S - MT^2*(MZ^2 + 3*S)) - 
         MH^4*(2*MZ^4 - MZ^2*S + MT^2*(MZ^2 + 3*S)))*
        (DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
            Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
            Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
            Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + 
            S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + (MH^2 + MZ^2 - S)*
       Kallen\[Lambda][MH^2, MZ^2, S] + Sqrt[MH^4 - 4*MH^2*MT^2]*
       (MH^4 - 5*MZ^4 + MH^2*(4*MZ^2 - 2*S) + 4*MZ^2*S + S^2)*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)] + 
      Sqrt[-4*MT^2*MZ^2 + MZ^4]*(-5*MH^4 + (MZ^2 - S)^2 + 4*MH^2*(MZ^2 + S))*
       Log[(2*MT^2 - MZ^2 + Sqrt[-4*MT^2*MZ^2 + MZ^4])/(2*MT^2)] - 
      (Sqrt[S*(-4*MT^2 + S)]*(MH^6 - MH^4*(MZ^2 + 2*S) + (MZ^3 - MZ*S)^2 + 
         MH^2*(-MZ^4 + 8*MZ^2*S + S^2))*
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S)/
    (2*Kallen\[Lambda][MH^2, MZ^2, S]^2), Kallen\[Lambda][MH^2, MZ^2, S] > 
    0], PVC[0, 2, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   -((-2*MZ^2*(MH^4*(2*MT^2 + MZ^2) + (2*MT^2 + MZ^2)*(MZ^2 - S)^2 - 
         2*MH^2*(MZ^4 - 2*MZ^2*S + 2*MT^2*(MZ^2 + S)))*
        (DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
            Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
            Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
            Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + 
            S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] - 
      2*MZ^2*Kallen\[Lambda][MH^2, MZ^2, S] + 
      (Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^6 + (MZ^2 - S)^2*(3*MZ^2 - S) - 
         MH^4*(5*MZ^2 + 3*S) + MH^2*(MZ^4 + 3*S^2))*
        Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/MH^2 + 
      6*MZ^2*Sqrt[-4*MT^2*MZ^2 + MZ^4]*(MH^2 - MZ^2 + S)*
       Log[(2*MT^2 - MZ^2 + Sqrt[-4*MT^2*MZ^2 + MZ^4])/(2*MT^2)] + 
      (Sqrt[S*(-4*MT^2 + S)]*(-MH^6 + 3*MZ^6 + MZ^4*S - 5*MZ^2*S^2 + S^3 + 
         MH^4*(5*MZ^2 + 3*S) - MH^2*(7*MZ^4 + 3*S^2))*
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S)/
    (2*Kallen\[Lambda][MH^2, MZ^2, S]^2), Kallen\[Lambda][MH^2, MZ^2, S] > 
    0], PVC[1, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ConditionalExpression[
   (3 + Eps^(-1) + (2*(MH^4*MT^2 + MT^2*(MZ^2 - S)^2 + 
        MH^2*(MZ^2*S - 2*MT^2*(MZ^2 + S)))*
       (DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) - Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MH^2*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MH^2*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^4 - MH^2*(MZ^2 + S) + Sqrt[MH^2*(MH^2 - 4*MT^2)*
             Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[(MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
              S]]))/(-(MH^2*MZ^2) + MZ^4 - MZ^2*S + 
           Sqrt[MZ^2*(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
        DiLog[-((MZ^2*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
               S]]))/(MH^2*MZ^2 - MZ^4 + MZ^2*S + Sqrt[MZ^2*(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
        DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
          (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
              MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
              MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + S^2 + 
           Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
         MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
      Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 
     (Sqrt[MH^4 - 4*MH^2*MT^2]*(MH^2 - MZ^2 - S)*
       Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/
      Kallen\[Lambda][MH^2, MZ^2, S] + Log[Mu^2/MT^2] + 
     (Sqrt[-4*MT^2*MZ^2 + MZ^4]*(-MH^2 + MZ^2 - S)*
       Log[(2*MT^2 - MZ^2 + Sqrt[-4*MT^2*MZ^2 + MZ^4])/(2*MT^2)])/
      Kallen\[Lambda][MH^2, MZ^2, S] + 
     (Sqrt[S*(-4*MT^2 + S)]*(-MH^2 - MZ^2 + S)*
       Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/
      Kallen\[Lambda][MH^2, MZ^2, S])/4, Kallen\[Lambda][MH^2, MZ^2, S] > 0]}
