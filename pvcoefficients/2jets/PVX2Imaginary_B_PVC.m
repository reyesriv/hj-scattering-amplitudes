(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVC[0, 0, 0, T24, MH^2, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 0, U, 0, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 0, U, T15, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 1, T24, MH^2, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 1, U, 0, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 1, U, T15, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 1, 0, T24, MH^2, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 1, 0, U, 0, T15, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 1, 0, U, T15, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVC[0, 0, 0, 0, S45, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
    Log[(-(S45*(S45 - T)) + Sqrt[S45*(-4*MT^2 + S45)]*(S45 - T))/
      (-(S45*(S45 - T)) + Sqrt[S45*(-4*MT^2 + S45)]*(-S45 + T))])/(S45 - T), 
 PVC[0, 0, 0, MH^2, S45, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
     Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
         Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
        Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
    Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
   ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
     Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
         Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
        Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
    Sqrt[Kallen\[Lambda][MH^2, S, S45]], 
 PVC[0, 0, 0, S45, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
     Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
         Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
        Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
    Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
   ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
     Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
         Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
        Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
    Sqrt[Kallen\[Lambda][MH^2, S, S45]], 
 PVC[0, 0, 1, 0, S45, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*Sqrt[S45*(-4*MT^2 + S45)]*HeavisideTheta[-4*MT^2 + S45])/
   (S45*(S45 - T)), PVC[0, 0, 1, MH^2, S45, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((Sqrt[S*(-4*MT^2 + S)]*(MH^2 + S - S45))/
      (S*Kallen\[Lambda][MH^2, S, S45]) - 
     (MH^2*(MH^2 - S - S45)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
          Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
    ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^2 - S + S45))/
      (S45*Kallen\[Lambda][MH^2, S, S45]) - 
     (MH^2*(MH^2 - S - S45)*Log[((MH^2 + S - S45)*S45 + 
          Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
         ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)), 
 PVC[0, 0, 1, S45, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S + S45))/
      (S*Kallen\[Lambda][MH^2, S, S45]) + 
     ((MH^2 + S - S45)*S45*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
          Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
    ((-2*Sqrt[S45*(-4*MT^2 + S45)])/Kallen\[Lambda][MH^2, S, S45] + 
     ((MH^2 + S - S45)*S45*Log[((MH^2 + S - S45)*S45 + 
          Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
         ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)), 
 PVC[0, 1, 0, 0, S45, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
   ((Sqrt[S45*(-4*MT^2 + S45)]*(S45 + T))/(S45*(S45 - T)^2) + 
    (T*Log[(-(S45*(S45 - T)) + Sqrt[S45*(-4*MT^2 + S45)]*(S45 - T))/
        (-(S45*(S45 - T)) + Sqrt[S45*(-4*MT^2 + S45)]*(-S45 + T))])/
     (-S45 + T)^2), PVC[0, 1, 0, MH^2, S45, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
     (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
          Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
    ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
      (S45*Kallen\[Lambda][MH^2, S, S45]) + 
     (S*(MH^2 - S + S45)*Log[((MH^2 + S - S45)*S45 + 
          Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
         ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)), 
 PVC[0, 1, 0, S45, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
     (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
          Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
    ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
      (S45*Kallen\[Lambda][MH^2, S, S45]) + 
     (S*(MH^2 - S + S45)*Log[((MH^2 + S - S45)*S45 + 
          Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
         ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
           Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
      Kallen\[Lambda][MH^2, S, S45]^(3/2)), 
 PVC[0, 0, 0, S34, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
     Log[(-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(S - S34))/
       (-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S34))])/(S - S34) - 
   ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
     Log[((S - S34)*S34 + Sqrt[S34*(-4*MT^2 + S34)]*(-S + S34))/
       ((S - S34)*S34 + (S - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/(S - S34), 
 PVC[0, 0, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
     Log[(-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(S - S34))/
       (-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S34))])/(S - S34) - 
   ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
     Log[((S - S34)*S34 + Sqrt[S34*(-4*MT^2 + S34)]*(-S + S34))/
       ((S - S34)*S34 + (S - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/(S - S34), 
 PVC[0, 0, 0, S35, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
     Log[(-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(S - S35))/
       (-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S35))])/(S - S35) - 
   ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
     Log[((S - S35)*S35 + Sqrt[S35*(-4*MT^2 + S35)]*(-S + S35))/
       ((S - S35)*S35 + (S - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/(S - S35), 
 PVC[0, 0, 1, S34, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*((Sqrt[S*(-4*MT^2 + S)]*(S + S34))/
      (S*(S - S34)^2) + 
     (S34*Log[(-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(S - S34))/
         (-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S34))])/
      (-S + S34)^2) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
    ((-2*Sqrt[S34*(-4*MT^2 + S34)])/(S - S34)^2 - 
     (S34*Log[((S - S34)*S34 + Sqrt[S34*(-4*MT^2 + S34)]*(-S + S34))/
         ((S - S34)*S34 + (S - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
      (-S + S34)^2), PVC[0, 0, 1, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((Sqrt[S*(-4*MT^2 + S)]*(S + S34))/(S*(S - S34)^2) + 
     (S34*Log[(-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(S - S34))/
         (-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S34))])/
      (-S + S34)^2) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
    ((-2*Sqrt[S34*(-4*MT^2 + S34)])/(S - S34)^2 - 
     (S34*Log[((S - S34)*S34 + Sqrt[S34*(-4*MT^2 + S34)]*(-S + S34))/
         ((S - S34)*S34 + (S - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
      (-S + S34)^2), PVC[0, 0, 1, S35, 0, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((Sqrt[S*(-4*MT^2 + S)]*(S + S35))/(S*(S - S35)^2) + 
     (S35*Log[(-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(S - S35))/
         (-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S35))])/
      (-S + S35)^2) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
    ((-2*Sqrt[S35*(-4*MT^2 + S35)])/(S - S35)^2 - 
     (S35*Log[((S - S35)*S35 + Sqrt[S35*(-4*MT^2 + S35)]*(-S + S35))/
         ((S - S35)*S35 + (S - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/
      (-S + S35)^2), PVC[0, 1, 0, S34, 0, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((-2*Sqrt[S*(-4*MT^2 + S)])/(S - S34)^2 - 
     (S*Log[(-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(S - S34))/
         (-(S*(S - S34)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S34))])/(S - S34)^2) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
    ((Sqrt[S34*(-4*MT^2 + S34)]*(S + S34))/((S - S34)^2*S34) + 
     (S*Log[((S - S34)*S34 + Sqrt[S34*(-4*MT^2 + S34)]*(-S + S34))/
         ((S - S34)*S34 + (S - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
      (S - S34)^2), PVC[0, 1, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((2*I)*Pi*Sqrt[S*(-4*MT^2 + S)]*
     HeavisideTheta[-4*MT^2 + S])/(S*(S - S34)) - 
   ((2*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
    ((S - S34)*S34), PVC[0, 1, 0, S35, 0, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
    ((-2*Sqrt[S*(-4*MT^2 + S)])/(S - S35)^2 - 
     (S*Log[(-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(S - S35))/
         (-(S*(S - S35)) + Sqrt[S*(-4*MT^2 + S)]*(-S + S35))])/(S - S35)^2) + 
   (2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
    ((Sqrt[S35*(-4*MT^2 + S35)]*(S + S35))/((S - S35)^2*S35) + 
     (S*Log[((S - S35)*S35 + Sqrt[S35*(-4*MT^2 + S35)]*(-S + S35))/
         ((S - S35)*S35 + (S - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/
      (S - S35)^2), PVC[0, 0, 0, 0, T24, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, MH^2, T24, T15, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, T24, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, MH^2, T24, T15, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, T24, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, 0, T24, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, MH^2, T24, T15, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, 0, T24, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, T24, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, T25, MH^2, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, U, 0, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, U, T14, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, T25, MH^2, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, U, 0, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, U, T14, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, T25, MH^2, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, U, 0, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, U, T14, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, 0, T25, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, MH^2, T25, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 0, T25, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, MH^2, T25, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, T25, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, 0, T25, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, MH^2, T25, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 0, 1, 0, T25, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0, PVC[0, 1, 0, T25, 0, T, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 0}
