(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVB[0, 0, T24, Sqrt[MT^2], Sqrt[MT^2]] -> 0, 
 PVB[0, 0, S45, Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*Sqrt[S45*(-4*MT^2 + S45)]*HeavisideTheta[-4*MT^2 + S45])/S45, 
 PVB[0, 0, S35, Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/S35, 
 PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((2*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/S34, 
 PVB[0, 0, T25, Sqrt[MT^2], Sqrt[MT^2]] -> 0}
