(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[2*MH^2 - S34 - T - U, MT, MT] + Log[Mu^2/MT^2], 
 PVB[0, 0, MH^2 - S - T24 - U, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[MH^2 - S - T24 - U, MT, MT] + Log[Mu^2/MT^2], 
 PVB[0, 0, MH^2 - S34 - T14 - T24, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[MH^2 - S34 - T14 - T24, MT, MT] + Log[Mu^2/MT^2], 
 PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[S34, MT, MT] + Log[Mu^2/MT^2], 
 PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] -> 2 + Eps^(-1) + DiscB[T, MT, MT] + 
   Log[Mu^2/MT^2], PVB[0, 0, T24, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[T24, MT, MT] + Log[Mu^2/MT^2], 
 PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] -> 2 + Eps^(-1) + DiscB[U, MT, MT] + 
   Log[Mu^2/MT^2], PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
   (2*(MH^2 - U)), PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> 
  -(-Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
     Log[(-2*MH^2 + 2*MT^2 + S34 + T + U + Sqrt[(-2*MH^2 + S34 + T + U)*
           (-2*MH^2 + 4*MT^2 + S34 + T + U)])/(2*MT^2)]^2)/
   (2*(-MH^2 + S34 + T + U)), PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> ScalarC0[MH^2, T14, 
   MH^2 - S - T24 - U, MT, MT, MT], 
 PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ScalarC0[MH^2, S, MH^2 - S34 - T14 - T24, MT, MT, MT], 
 PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
    Log[(2*MT^2 - S34 + Sqrt[S34*(-4*MT^2 + S34)])/(2*MT^2)]^2)/
   (2*(MH^2 - S34)), PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
     2 - Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
   (2*(MH^2 - T)), PVC[0, 0, 0, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, 
   MT], PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (DiscB[MH^2, MT, MT] - DiscB[U, MT, MT])/(MH^2 - U), 
 PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-2*MH^2*DiscB[MH^2, MT, MT] + (S + T14 + T24 + U)*
     DiscB[T14, MT, MT] + (2*MH^2 - S - T14 - T24 - U)*
     DiscB[MH^2 - S - T24 - U, MT, MT] - MH^2*(S - T14 + T24 + U)*
     ScalarC0[MH^2, T14, MH^2 - S - T24 - U, MT, MT, MT])/
   Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U], 
 PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-2*MH^2*DiscB[MH^2, MT, MT] + (S + S34 + T14 + T24)*
     DiscB[S, MT, MT] + (2*MH^2 - S - S34 - T14 - T24)*
     DiscB[MH^2 - S34 - T14 - T24, MT, MT] + MH^2*(S - S34 - T14 - T24)*
     ScalarC0[MH^2, S, MH^2 - S34 - T14 - T24, MT, MT, MT])/
   Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24], 
 PVC[0, 0, 1, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-2*MH^2*DiscB[MH^2, MT, MT] + (2*MH^2 - S - T - T14 - T24)*
     DiscB[MH^2 - S - T - T14, MT, MT] + S*DiscB[T24, MT, MT] + 
    T*DiscB[T24, MT, MT] + T14*DiscB[T24, MT, MT] + T24*DiscB[T24, MT, MT] - 
    MH^2*S*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT] - 
    MH^2*T*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT] - 
    MH^2*T14*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT] + 
    MH^2*T24*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT])/
   Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24], 
 PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-DiscB[MH^2, MT, MT] + DiscB[U, MT, MT])/(2*(MH^2 - U)), 
 PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -((-DiscB[MH^2, MT, MT] + DiscB[2*MH^2 - S34 - T - U, MT, 
      MT])/(-MH^2 + S34 + T + U)), 
 PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -((-((S + T14 + T24 + U)*DiscB[MH^2, MT, MT]) + 
     2*T14*DiscB[T14, MT, MT] + (S - T14 + T24 + U)*DiscB[MH^2 - S - T24 - U, 
       MT, MT] + T14*(-2*MH^2 + S + T14 + T24 + U)*ScalarC0[MH^2, T14, 
       MH^2 - S - T24 - U, MT, MT, MT])/Kallen\[Lambda][MH^2, T14, 
     MH^2 - S - T24 - U]), PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, S, 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  ((S + S34 + T14 + T24)*DiscB[MH^2, MT, MT] - 2*S*DiscB[S, MT, MT] + 
    (S - S34 - T14 - T24)*DiscB[MH^2 - S34 - T14 - T24, MT, MT] - 
    S*(-2*MH^2 + S + S34 + T14 + T24)*ScalarC0[MH^2, S, 
      MH^2 - S34 - T14 - T24, MT, MT, MT])/Kallen\[Lambda][MH^2, S, 
    MH^2 - S34 - T14 - T24], PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> (DiscB[MH^2, MT, MT] - DiscB[S34, MT, MT])/
   (MH^2 - S34), PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (DiscB[MH^2, MT, MT] - DiscB[T, MT, MT])/(MH^2 - T), 
 PVC[0, 1, 0, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((2*MH^2 - S - T - T14 - T24)*DiscB[MH^2, MT, MT] - 
    2*(MH^2 - S - T - T14)*DiscB[MH^2 - S - T - T14, MT, MT] - 
    (S + T + T14 - T24)*DiscB[T24, MT, MT] + (MH^2 - S - T - T14)*
     (S + T + T14 + T24)*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT])/
   Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24], 
 PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-MH^2 + U - MH^2*DiscB[MH^2, MT, MT] + MH^2*DiscB[U, MT, MT] - 
    MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
    MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
   (2*(MH^2 - U)^2), PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> 
  -(-MH^2 + S34 + T + U + MH^2*DiscB[MH^2, MT, MT] - 
     MH^2*DiscB[2*MH^2 - S34 - T - U, MT, MT] + 
     MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
     MT^2*Log[(-2*MH^2 + 2*MT^2 + S34 + T + U + Sqrt[(-2*MH^2 + S34 + T + U)*
            (-2*MH^2 + 4*MT^2 + S34 + T + U)])/(2*MT^2)]^2)/
   (2*(-MH^2 + S34 + T + U)^2), PVC[0, 1, 1, MH^2, MH^2 - S - T24 - U, T14, 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  -(MH^2*(S^2 + 8*MH^2*T14 - 5*T14^2 - 4*T14*T24 + T24^2 - 4*T14*U + 
       2*T24*U + U^2 + 2*S*(-2*T14 + T24 + U))*DiscB[MH^2, MT, MT] + 
     T14*((S + T14 + T24 + U)^2 - 2*MH^2*(3*S - T14 + 3*(T24 + U)))*
      DiscB[T14, MT, MT] - (8*MH^4*T14 + T14*(S + T14 + T24 + U)^2 + 
       MH^2*(S^2 - 3*T14^2 - 10*T14*(T24 + U) + (T24 + U)^2 + 
         2*S*(-5*T14 + T24 + U)))*DiscB[MH^2 - S - T24 - U, MT, MT] + 
     (S + T14 + T24 + U)*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U] + 
     2*(MT^2*(S + T14 + T24 + U)*(S^2 - 4*MH^2*T14 + 2*S*(T14 + T24 + U) + 
         (T14 + T24 + U)^2) - MH^2*T14*(2*S^2 - T14^2 + T14*T24 + 2*T24^2 + 
         T14*U + 4*T24*U + 2*U^2 + MH^2*(-3*S + T14 - 3*(T24 + U)) + 
         S*(T14 + 4*(T24 + U))))*ScalarC0[MH^2, T14, MH^2 - S - T24 - U, MT, 
       MT, MT])/(2*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U]^2), 
 PVC[0, 1, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  -(MH^2*(8*MH^2*S - 5*S^2 - 4*S*(S34 + T14 + T24) + (S34 + T14 + T24)^2)*
      DiscB[MH^2, MT, MT] + S*((S + S34 + T14 + T24)^2 + 
       2*MH^2*(S - 3*(S34 + T14 + T24)))*DiscB[S, MT, MT] - 
     (8*MH^4*S + S*(S + S34 + T14 + T24)^2 + 
       MH^2*(-3*S^2 - 10*S*(S34 + T14 + T24) + (S34 + T14 + T24)^2))*
      DiscB[MH^2 - S34 - T14 - T24, MT, MT] + (S + S34 + T14 + T24)*
      Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24] - 
     2*(-(MT^2*(S + S34 + T14 + T24)^3) + MH^4*S*(S - 3*(S34 + T14 + T24)) + 
       MH^2*S*(S + S34 + T14 + T24)*(4*MT^2 - S + 2*(S34 + T14 + T24)))*
      ScalarC0[MH^2, S, MH^2 - S34 - T14 - T24, MT, MT, MT])/
   (2*Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24]^2), 
 PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-MH^2 + S34 - MH^2*DiscB[MH^2, MT, MT] + MH^2*DiscB[S34, MT, MT] - 
    MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
    MT^2*Log[(2*MT^2 - S34 + Sqrt[S34*(-4*MT^2 + S34)])/(2*MT^2)]^2)/
   (2*(MH^2 - S34)^2), PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-MH^2 + T - MH^2*DiscB[MH^2, MT, MT] + 
    MH^2*DiscB[T, MT, MT] - 
    MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
    MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
   (2*(MH^2 - T)^2), PVC[0, 1, 1, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-(MH^2*(-5*S^2 - 5*T^2 - 10*T*T14 - 5*T14^2 - 4*T*T24 - 4*T14*T24 + 
       T24^2 + 2*MH^2*(3*S + 3*T + 3*T14 + T24) - 2*S*(5*T + 5*T14 + 2*T24))*
      DiscB[MH^2, MT, MT]) + (MH^2 - S - T - T14)*
     (MH^2*(6*S + 6*T + 6*T14 - 2*T24) - (S + T + T14 + T24)^2)*
     DiscB[MH^2 - S - T - T14, MT, MT] + 
    (2*MH^2*(S + T + T14 - T24)^2 + 4*MH^4*T24 - 
      (S + T + T14)*(S + T + T14 + T24)^2)*DiscB[T24, MT, MT] - 
    (2*MH^2 - S - T - T14 - T24)*Kallen\[Lambda][MH^2, MH^2 - S - T - T14, 
      T24] - 2*(2*MH^6*T24 - MT^2*(S + T + T14 + T24)^3 + 
      MH^4*(S^2 + T^2 + 2*T*T14 + T14^2 + S*(2*T + 2*T14 - 3*T24) - 
        8*MT^2*T24 - 3*T*T24 - 3*T14*T24 - 2*T24^2) - 
      MH^2*(S + T + T14 + T24)*((S + T + T14)*(S + T + T14 - 2*T24) - 
        2*MT^2*(S + T + T14 + 3*T24)))*ScalarC0[MH^2, MH^2 - S - T - T14, 
      T24, MT, MT, MT])/(2*Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24]^2), 
 PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> -(DiscB[MH^2, MT, MT] - DiscB[2*MH^2 - S34 - T - U, MT, 
      MT])/(2*(-MH^2 + S34 + T + U)), 
 PVC[0, 2, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> 
  (-((S^3 + (T14 + T24 + U)^2*(3*T14 + T24 + U) + S^2*(5*T14 + 3*(T24 + U)) - 
       2*MH^2*T14*(3*T14 + 5*(T24 + U)) + S*(-10*MH^2*T14 + 7*T14^2 + 
         10*T14*(T24 + U) + 3*(T24 + U)^2))*DiscB[MH^2, MT, MT]) + 
    6*T14^2*(-2*MH^2 + S + T14 + T24 + U)*DiscB[T14, MT, MT] + 
    (S^3 - 3*T14^3 + T14^2*T24 + 5*T14*T24^2 + T24^3 + T14^2*U + 
      10*T14*T24*U + 3*T24^2*U + 5*T14*U^2 + 3*T24*U^2 + U^3 + 
      2*MH^2*T14*(3*T14 - 5*(T24 + U)) + S^2*(5*T14 + 3*(T24 + U)) + 
      S*(-10*MH^2*T14 + T14^2 + 10*T14*(T24 + U) + 3*(T24 + U)^2))*
     DiscB[MH^2 - S - T24 - U, MT, MT] + 
    2*T14*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U] + 
    2*T14*(2*MT^2*(S^2 - 4*MH^2*T14 + 2*S*(T14 + T24 + U) + 
        (T14 + T24 + U)^2) + T14*(6*MH^4 + (S + T14 + T24 + U)^2 - 
        2*MH^2*(3*S + 2*T14 + 3*(T24 + U))))*ScalarC0[MH^2, T14, 
      MH^2 - S - T24 - U, MT, MT, MT])/
   (2*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U]^2), 
 PVC[0, 2, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> ((-((S + S34 + T14 + T24)^2*(3*S + S34 + T14 + T24)) + 
      2*MH^2*S*(3*S + 5*(S34 + T14 + T24)))*DiscB[MH^2, MT, MT] + 
    6*S^2*(-2*MH^2 + S + S34 + T14 + T24)*DiscB[S, MT, MT] + 
    (-3*S^3 + S^2*(S34 + T14 + T24) + 5*S*(S34 + T14 + T24)^2 + 
      (S34 + T14 + T24)^3 + 2*MH^2*S*(3*S - 5*(S34 + T14 + T24)))*
     DiscB[MH^2 - S34 - T14 - T24, MT, MT] + 
    2*S*Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24] + 
    2*S*(6*MH^4*S + (2*MT^2 + S)*(S + S34 + T14 + T24)^2 - 
      2*MH^2*S*(4*MT^2 + 2*S + 3*(S34 + T14 + T24)))*
     ScalarC0[MH^2, S, MH^2 - S34 - T14 - T24, MT, MT, MT])/
   (2*Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24]^2), 
 PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-DiscB[MH^2, MT, MT] + DiscB[S34, MT, MT])/(2*(MH^2 - S34)), 
 PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (-DiscB[MH^2, MT, MT] + DiscB[T, MT, MT])/(2*(MH^2 - T)), 
 PVC[0, 2, 0, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (((S + T + T14 + T24)^2*(3*S + 3*T + 3*T14 + T24) + 
      2*MH^4*(3*S + 3*T + 3*T14 + 5*T24) - 2*MH^2*(5*S^2 + 5*T^2 + 10*T*T14 + 
        5*T14^2 + 7*T*T24 + 7*T14*T24 + 4*T24^2 + S*(10*T + 10*T14 + 7*T24)))*
     DiscB[MH^2, MT, MT] - 6*(-MH^2 + S + T + T14)^2*(S + T + T14 + T24)*
     DiscB[MH^2 - S - T - T14, MT, MT] - 
    (-3*S^3 - 3*T^3 - 9*T^2*T14 - 9*T*T14^2 - 3*T14^3 + 4*MH^4*T24 + 
      T^2*T24 + 2*T*T14*T24 + T14^2*T24 + 5*T*T24^2 + 5*T14*T24^2 + T24^3 + 
      S^2*(-9*T - 9*T14 + T24) + 2*MH^2*(S^2 + T^2 + 2*T*T14 + T14^2 + 
        S*(2*T + 2*T14 - T24) - T*T24 - T14*T24 - 4*T24^2) + 
      S*(-9*T^2 - 9*T14^2 + 2*T14*T24 + 5*T24^2 + 2*T*(-9*T14 + T24)))*
     DiscB[T24, MT, MT] + 2*(MH^2 - S - T - T14)*Kallen\[Lambda][MH^2, 
      MH^2 - S - T - T14, T24] + 2*(MH^2 - S - T - T14)*
     (2*MH^4*T24 + (2*MT^2 - S - T - T14)*(S + T + T14 + T24)^2 + 
      MH^2*(S^2 + T^2 + 2*T*T14 + T14^2 + 2*S*(T + T14) - 8*MT^2*T24 + 
        T24^2))*ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT])/
   (2*Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24]^2), 
 PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (3*MH^2 + MH^2/Eps - 3*U - U/Eps + MH^2*DiscB[MH^2, MT, MT] - 
    U*DiscB[U, MT, MT] + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
        (2*MT^2)]^2 + MH^2*Log[Mu^2/MT^2] - U*Log[Mu^2/MT^2] - 
    MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
   (4*(MH^2 - U)), PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> 
  -(3*MH^2 + MH^2/Eps - 3*S34 - S34/Eps - 3*T - T/Eps - 3*U - U/Eps - 
     MH^2*DiscB[MH^2, MT, MT] + (2*MH^2 - S34 - T - U)*
      DiscB[2*MH^2 - S34 - T - U, MT, MT] - 
     MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
     MH^2*Log[Mu^2/MT^2] - S34*Log[Mu^2/MT^2] - T*Log[Mu^2/MT^2] - 
     U*Log[Mu^2/MT^2] + 
     MT^2*Log[(-2*MH^2 + 2*MT^2 + S34 + T + U + Sqrt[(-2*MH^2 + S34 + T + U)*
            (-2*MH^2 + 4*MT^2 + S34 + T + U)])/(2*MT^2)]^2)/
   (4*(-MH^2 + S34 + T + U)), PVC[1, 0, 0, MH^2, MH^2 - S - T24 - U, T14, 
   Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (MH^2*(S - T14 + T24 + U)*DiscB[MH^2, MT, MT] + 
    T14*(-2*MH^2 + S + T14 + T24 + U)*DiscB[T14, MT, MT] - 
    (MH^2 - S - T24 - U)*(S + T14 + T24 + U)*DiscB[MH^2 - S - T24 - U, MT, 
      MT] + 3*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U] + 
    Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U]/Eps + 
    Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U]*Log[Mu^2/MT^2] + 
    2*(MH^2*T14*(MH^2 - S - T24 - U) + MT^2*(S^2 - 4*MH^2*T14 + 
        2*S*(T14 + T24 + U) + (T14 + T24 + U)^2))*ScalarC0[MH^2, T14, 
      MH^2 - S - T24 - U, MT, MT, MT])/
   (4*Kallen\[Lambda][MH^2, T14, MH^2 - S - T24 - U]), 
 PVC[1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (-(MH^2*(S - S34 - T14 - T24)*DiscB[MH^2, MT, MT]) + 
    S*(-2*MH^2 + S + S34 + T14 + T24)*DiscB[S, MT, MT] - 
    (MH^2 - S34 - T14 - T24)*(S + S34 + T14 + T24)*
     DiscB[MH^2 - S34 - T14 - T24, MT, MT] + 
    3*Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24] + 
    Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24]/Eps + 
    Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24]*Log[Mu^2/MT^2] + 
    2*(MH^4*S - MH^2*S*(4*MT^2 + S34 + T14 + T24) + 
      MT^2*(S + S34 + T14 + T24)^2)*ScalarC0[MH^2, S, MH^2 - S34 - T14 - T24, 
      MT, MT, MT])/(4*Kallen\[Lambda][MH^2, S, MH^2 - S34 - T14 - T24]), 
 PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (3*MH^2 + MH^2/Eps - 3*S34 - S34/Eps + MH^2*DiscB[MH^2, MT, MT] - 
    S34*DiscB[S34, MT, MT] + 
    MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
    MH^2*Log[Mu^2/MT^2] - S34*Log[Mu^2/MT^2] - 
    MT^2*Log[(2*MT^2 - S34 + Sqrt[S34*(-4*MT^2 + S34)])/(2*MT^2)]^2)/
   (4*(MH^2 - S34)), PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], 
   Sqrt[MT^2]] -> (3*MH^2 + MH^2/Eps - 3*T - T/Eps + 
    MH^2*DiscB[MH^2, MT, MT] - T*DiscB[T, MT, MT] + 
    MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
    MH^2*Log[Mu^2/MT^2] - T*Log[Mu^2/MT^2] - 
    MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
   (4*(MH^2 - T)), PVC[1, 0, 0, MH^2, T24, MH^2 - S - T - T14, Sqrt[MT^2], 
   Sqrt[MT^2], Sqrt[MT^2]] -> (MH^2*(S + T + T14 - T24)*DiscB[MH^2, MT, MT] - 
    (MH^2 - S - T - T14)*(S + T + T14 + T24)*DiscB[MH^2 - S - T - T14, MT, 
      MT] - (2*MH^2 - S - T - T14 - T24)*T24*DiscB[T24, MT, MT] + 
    3*Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24] + 
    Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24]/Eps + 
    Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24]*Log[Mu^2/MT^2] + 
    2*(MH^2*(MH^2 - S - T - T14)*T24 + MT^2*(S^2 + T^2 + T14^2 - 4*MH^2*T24 + 
        2*T14*T24 + T24^2 + 2*T*(T14 + T24) + 2*S*(T + T14 + T24)))*
     ScalarC0[MH^2, MH^2 - S - T - T14, T24, MT, MT, MT])/
   (4*Kallen\[Lambda][MH^2, MH^2 - S - T - T14, T24])}
