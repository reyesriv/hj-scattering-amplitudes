(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVC[0, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2/(2*MH^2), 
 PVC[0, 1, 1, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (MH^2 + MT^2*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)]^2)/
   (2*MH^4), PVC[1, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (Sqrt[MH^4 - 4*MH^2*MT^2]*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
       (2*MT^2)] + MT^2*Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/
        (2*MT^2)]^2 + MH^2*(3 + Eps^(-1) + Log[Mu^2/MT^2]))/(4*MH^2)}
