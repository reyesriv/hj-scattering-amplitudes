(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{2 + Eps^(-1) + DiscB[MH^2, MT, MT] + Log[Mu^2/MT^2], 
 Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2/(2*MH^2), 
 (MH^2 + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
  (2*MH^4), (3 + Eps^(-1) + DiscB[MH^2, MT, MT] - 2*Log[MT] + 
   (MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/MH^2 + 
   Log[Mu^2])/4}
