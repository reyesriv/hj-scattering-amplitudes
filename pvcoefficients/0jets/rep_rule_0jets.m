(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, MH^2, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + DiscB[MH^2, MT, MT] + Log[Mu^2/MT^2], 
 PVC[0, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2/(2*MH^2), 
 PVC[0, 1, 1, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (MH^2 + MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/
   (2*MH^4), PVC[1, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] -> 
  (3 + Eps^(-1) + DiscB[MH^2, MT, MT] - 2*Log[MT] + 
    (MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)/MH^2 + 
    Log[Mu^2])/4}
