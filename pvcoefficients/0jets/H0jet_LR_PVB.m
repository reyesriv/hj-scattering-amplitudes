(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{PVB[0, 0, MH^2, Sqrt[MT^2], Sqrt[MT^2]] -> 
  2 + Eps^(-1) + (Sqrt[MH^4 - 4*MH^2*MT^2]*
     Log[(-MH^2 + 2*MT^2 + Sqrt[MH^4 - 4*MH^2*MT^2])/(2*MT^2)])/MH^2 + 
   Log[Mu^2/MT^2]}
