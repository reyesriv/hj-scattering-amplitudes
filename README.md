To be able to run all scripts you need:

FeynArts
FormCalc
X
FA2X
colliderfourvec
helicityvec

These are included inside hj-scattering-amplitudes/packages/

To install them move all these files to your Mathematica $UserBaseDirectory/Applications.

Then run ./FeynInstall there.
