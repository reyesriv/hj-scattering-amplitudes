(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{rS^2, (rS*(-2*k3^2 - MH^2 + Sqrt[k3^2 + MH^2]*rS + 
    k3*(2*Sqrt[k3^2 + MH^2] - rS)*Cos[\[Theta]3]*Cos[\[Theta]4] + 
    k3*(2*Sqrt[k3^2 + MH^2] - rS)*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*
     Sin[\[Theta]4] + 2*k3*Sqrt[k3^2 + MH^2]*Sin[\[Theta]3]*Sin[\[Theta]4]*
     Sin[\[Phi]3]*Sin[\[Phi]4] - k3*rS*Sin[\[Theta]3]*Sin[\[Theta]4]*
     Sin[\[Phi]3]*Sin[\[Phi]4]))/(-Sqrt[k3^2 + MH^2] + rS + 
   k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
    Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
    Sin[\[Phi]3]*Sin[\[Phi]4]), MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS), 
 rS*(rS - (MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))/(-Sqrt[k3^2 + MH^2] + rS + 
     k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
      Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
      Sin[\[Phi]3]*Sin[\[Phi]4])), MH^2 - Sqrt[k3^2 + MH^2]*rS + 
  k3*rS*Cos[\[Theta]3], (rS*(MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
   (-1 + Cos[\[Theta]4]))/(2*(-Sqrt[k3^2 + MH^2] + rS + 
    k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
     Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
     Sin[\[Phi]3]*Sin[\[Phi]4])), 
 (Sqrt[k3^2 + MH^2] - rS/2 + (MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))/
     (2*(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*Cos[\[Theta]4] + 
       k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*Sin[\[Theta]4] + 
       k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*Sin[\[Phi]4])))^2 - 
  (rS + 2*k3*Cos[\[Theta]3] + ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
       Cos[\[Theta]4])/(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*
        Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*
        Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*
        Sin[\[Phi]4]))^2/4 - (k3*Cos[\[Phi]3]*Sin[\[Theta]3] + 
    ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*Cos[\[Phi]4]*Sin[\[Theta]4])/
     (2*(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*Cos[\[Theta]4] + 
       k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*Sin[\[Theta]4] + 
       k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*Sin[\[Phi]4])))^2 - 
  (k3*Sin[\[Theta]3]*Sin[\[Phi]3] + ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
      Sin[\[Theta]4]*Sin[\[Phi]4])/(2*(-Sqrt[k3^2 + MH^2] + rS + 
       k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
        Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
        Sin[\[Phi]3]*Sin[\[Phi]4])))^2, MH^2 - Sqrt[k3^2 + MH^2]*rS - 
  k3*rS*Cos[\[Theta]3], -(rS*(MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
    (1 + Cos[\[Theta]4]))/(2*(-Sqrt[k3^2 + MH^2] + rS + 
    k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
     Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
     Sin[\[Phi]3]*Sin[\[Phi]4])), 
 (Sqrt[k3^2 + MH^2] - rS/2 + (MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))/
     (2*(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*Cos[\[Theta]4] + 
       k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*Sin[\[Theta]4] + 
       k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*Sin[\[Phi]4])))^2 - 
  (-rS/2 + k3*Cos[\[Theta]3] + ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
      Cos[\[Theta]4])/(2*(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*
        Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*
        Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*
        Sin[\[Phi]4])))^2 - (k3*Cos[\[Phi]3]*Sin[\[Theta]3] + 
    ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*Cos[\[Phi]4]*Sin[\[Theta]4])/
     (2*(-Sqrt[k3^2 + MH^2] + rS + k3*Cos[\[Theta]3]*Cos[\[Theta]4] + 
       k3*Cos[\[Phi]3]*Cos[\[Phi]4]*Sin[\[Theta]3]*Sin[\[Theta]4] + 
       k3*Sin[\[Theta]3]*Sin[\[Theta]4]*Sin[\[Phi]3]*Sin[\[Phi]4])))^2 - 
  (k3*Sin[\[Theta]3]*Sin[\[Phi]3] + ((MH^2 + rS*(-2*Sqrt[k3^2 + MH^2] + rS))*
      Sin[\[Theta]4]*Sin[\[Phi]4])/(2*(-Sqrt[k3^2 + MH^2] + rS + 
       k3*Cos[\[Theta]3]*Cos[\[Theta]4] + k3*Cos[\[Phi]3]*Cos[\[Phi]4]*
        Sin[\[Theta]3]*Sin[\[Theta]4] + k3*Sin[\[Theta]3]*Sin[\[Theta]4]*
        Sin[\[Phi]3]*Sin[\[Phi]4])))^2}
