(* ::Package:: *)

(*Functions to convert from FORMCALC to PackageX
Kirtimaan Mohan
email= kamohan@msu.edu
version 1.0
10/20/2017
Note: THis code has not been optimized.*)
(*
Implementation of five point function E0i \[Rule] PVX
done by J.G. Reyes. 
10/03/2019
*)


Print["FA2X by Kirtimaan Mohan \n email: kamohan@msu.edu \n version 1.1 \n 10/03/2019 \n Not optimized! Possibly lots of bugs!!!"]


IndE[rr_]:=Module[{aa,ret},aa=ToExpression[StringSplit[StringTrim[ToString[rr],"ee"],""]];If[Total[aa]>=0,ret=Table[Count[aa,i],{i,0,4}];If[Mod[ret[[1]],2]==0 && ret[[1]]>1,ret[[1]]=ret[[1]]/2,ret[[1]]=0],Throw[$Failed,ret];Abort[]];ret];
IndD[rr_]:=Module[{aa,ret},aa=ToExpression[StringSplit[StringTrim[ToString[rr],"dd"],""]];If[Total[aa]>=0,ret=Table[Count[aa,i],{i,0,3}];If[Mod[ret[[1]],2]==0 && ret[[1]]>1,ret[[1]]=ret[[1]]/2,ret[[1]]=0],Throw[$Failed,ret];Abort[]];ret];
IndC[rr_]:=Module[{aa,ret},aa=ToExpression[StringSplit[StringTrim[ToString[rr],"cc"],""]];If[Total[aa]>=0,ret=Table[Count[aa,i],{i,0,2}];If[Mod[ret[[1]],2]==0 && ret[[1]]>1,ret[[1]]=ret[[1]]/2,ret[[1]]=0],Throw[$Failed,ret];Abort[]];ret];
IndB[rr_]:=Module[{aa,ret},aa=ToExpression[StringSplit[StringTrim[ToString[rr],"bb"],""]];If[Total[aa]>=0,ret=Table[Count[aa,i],{i,0,1}];If[Mod[ret[[1]],2]==0&& ret[[1]]>1,ret[[1]]=ret[[1]]/2,ret[[1]]=0],Throw[$Failed,ret];Abort[]];ret];
IndA[rr_]:=Module[{aa,ret},aa=ToExpression[StringSplit[StringTrim[ToString[rr],"aa"],""]];If[Total[aa]>=0,ret=Table[Count[aa,i],{i,0,0}];If[Mod[ret[[1]],2]==0 && ret[[1]]>1,ret[[1]]=ret[[1]]/2,ret[[1]]=0],Throw[$Failed,ret];Abort[]];ret];

RIntList={
E0i[ind_,s1_,s2_,s3_,s4_,s5_,s6_,s7_,s8_,s9_,s10_,m0_,m1_,m2_,m3_,m4_]:> PVX[IndE[ind][[1]],IndE[ind][[2]],IndE[ind][[3]],IndE[ind][[4]],IndE[ind][[5]],s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,Sqrt[m0],Sqrt[m1],Sqrt[m2],Sqrt[m3],Sqrt[m4]],
D0i[ind_,s1_,s2_,s3_,s4_,s12_,s23_,m1_,m2_,m3_,m4_]:>  PVD[IndD[ind][[1]],IndD[ind][[2]],IndD[ind][[3]],IndD[ind][[4]],s1,s2,s3,s4,s12,s23,Sqrt[m1],Sqrt[m2],Sqrt[m3],Sqrt[m4]],
C0i[ind_,s1_,s12_,s2_,m0_,m1_,m2_]:> PVC[IndC[ind][[1]],IndC[ind][[2]],IndC[ind][[3]],s1,s12,s2,Sqrt[m0],Sqrt[m1],Sqrt[m2]],
B0i[ind_,s_,m0_,m1_]:> PVB[IndB[ind][[1]],IndB[ind][[2]],s,Sqrt[m0],Sqrt[m1]],
A0i[ind_,m0_]:> PVA[IndA[ind][[1]],Sqrt[m0]]
};
