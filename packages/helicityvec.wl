(* ::Package:: *)

BeginPackage["helicityvec`"]

  
  Print["helicityvec by J.G. Reyes v1. August 2019"]  
  ek1Euc::usage = "ind. polarization vector in Euclidean coordinates" 
  ek2Euc::usage = "ind. polarization vector in Euclidean coordinates"
  ek3Euc::usage = "ind. polarization vector and longitudinal helicity  eigenvector in Euclidean coordinates"
  ek1::usage = "ind. polarization vector" 
  ek2::usage = "ind. polarization vector"
  ek3::usage = "ind. polarization vector and longitudinal helicity  eigenvector"
  ek1c::usage = "ek1 in collider coordinates"
  ek2c::usage = "ek2 in collider coordinates"
  ek3c::usage = "ek3 in collider coordinates"
  ekT::usage = "transverse helicity eigenvector"
  ekTstar::usage = "c.c. transverse helicity eigenvector"
  ekTEuc::usage = "transverse helicity eigenvector in Euclidean coordinates"
  ekTstarEuc::usage = "c.c. transverse helicity eigenvector in Euclidean coordinates"
  ekTc::usage = "ekT in collider coordinates"
  ekTcstar::usage = "ekTstar in collider coordinates"
  Epol::usage = "four vector using ekT/ekTstar/ek3"
  Epolstar::usage = "four vector c.c. using ekT/ekTstar/ek3"
  Epolc::usage = "Epol in collider coordinates"
  Epolcstar::usage = "Epolstar in collider coordinates"
  EpolEuc::usage = "Epol in Euclidean coordinates"
  EpolstarEuc::usage = "Epolstar in Euclidean coordinates"
  gmn::usage =  "metric tensor"
  Gmn::usage = "metric tensor elements"
  MyPair::usage = "contracts two four vectors"
  Cov4::usage = "covariant form for a four vector"
  vec4::usage = "four vector"
  vec4M::usage = "massive four vector"
  vec4c::usage = "four vector in collider coordinates"
  vec4Mc::usage = "massive four vector in collider coordinates"
  Begin["Private`"]
  (*pT = p*Sin[\[Theta]];
  EnT = En*Sin[\[Theta]];
  mT = Sqrt[m^2+px^2+py^2]
  y = (1/2)*((E+ppara)/(E-ppara))
  En = (1/2)*(e^y)*(m^2+p^2perp)
  Eta = -log[Tan[\[Theta]/2]];
  \[Theta]=2ArcTan[E^\[Eta]];*)
  

  ek1Euc[kx_,ky_,kz_]:= Module[{magk=Sqrt[kx^2+ky^2+kz^2],kT=Sqrt[kx^2+ky^2]},(1/(magk *(kT)))*{0,kx*kz,ky*kz,-kT^2}];
  ek2Euc[kx_,ky_,kz_]:= Module[{kT=Sqrt[kx^2+ky^2]},(1/kT)*{0,-ky,kx,0}];
  ek3Euc[m_,kx_,ky_,kz_]:= Module[{E=Sqrt[m^2+kx^2+ky^2+kz^2],magk=Sqrt[kx^2+ky^2]},(E/(m*magk))*{magk^2/E,kx,ky,kz}];
  
  ek1[k_,\[Theta]_,\[Phi]_]:= (1/(k *(k Sin[\[Theta]])))*{0,k^2 Sin[\[Theta]]Cos[\[Phi]]Cos[\[Theta]],k^2 Sin[\[Theta]]Sin[\[Phi]]Cos[\[Theta]], -k^2 Sin[\[Theta]]^2};
  ek2[k_,\[Theta]_,\[Phi]_]:= (1/(k Sin[\[Theta]]))*{0,-k Sin[\[Theta]]Sin[\[Phi]],k Sin[\[Theta]]Cos[\[Phi]],0};
  ek3[m_,k_,\[Theta]_,\[Phi]_]:= (Sqrt[k^2+m^2]/(m*k))*{k^2/Sqrt[k^2+m^2],k Sin[\[Theta]] Cos[\[Phi]],k Sin[\[Theta]] Sin[\[Phi]],k Cos[\[Theta]]};
  
  ek1c[pT_,\[Eta]_,\[Phi]_]:=(1/(pT Cosh[\[Eta]] *pT)){0,pT^2 Cos[\[Phi]]Sinh[\[Eta]],pT^2 Sin[\[Phi]]Sinh[\[Eta]], -pT^2};
  ek2c[pT_,\[Eta]_,\[Phi]_]:=(1/pT){0,-pT Sin[\[Phi]],pT Cos[\[Phi]],0};
  ek3c[m_,pT_,\[Eta]_,\[Phi]_]:=(Sqrt[pT^2+(pT*Sinh[\[Eta]])^2+m^2]/(m*(pT Cosh[\[Eta]])))*{(pT Cosh[\[Eta]])^2/Sqrt[pT^2+(pT*Sinh[\[Eta]])^2+m^2],pT Cos[\[Phi]],pT Sin[\[Phi]],pT Sinh[\[Eta]]};
  
  ekT[l_,k_,\[Theta]_,\[Phi]_]:=Sqrt[1/2]*(-l*ek1[k,\[Theta],\[Phi]]-I*ek2[k,\[Theta],\[Phi]]);
  ekTstar[l_,k_,\[Theta]_,\[Phi]_]:= Sqrt[1/2]*(-l*ek1[k,\[Theta],\[Phi]]+I*ek2[k,\[Theta],\[Phi]]);
  
  ekTc[l_,pT_,\[Eta]_,\[Phi]_]:=Sqrt[1/2]*(-l*ek1c[pT,\[Eta],\[Phi]]-I*ek2c[pT,\[Eta],\[Phi]]);
  ekTcstar[l_,pT_,\[Eta]_,\[Phi]_]:= Sqrt[1/2]*(-l*ek1c[pT,\[Eta],\[Phi]]+I*ek2c[pT,\[Eta],\[Phi]]);
  
  ekTEuc[l_,kx_,ky_,kz_]:=Sqrt[1/2]*(-l*ek1Euc[kx,ky,kz]-I*ek2Euc[kx,ky,kz]);
  ekTstarEuc[l_,kx_,ky_,kz_]:= Sqrt[1/2]*(-l*ek1Euc[kx,ky,kz]+I*ek2Euc[kx,ky,kz]);
  
  Epol[m_,k_,\[Theta]_,\[Phi]_,l_]:=Module[{m1=m,k1=k,t1=\[Theta],p1=\[Phi],l1=l},Switch[l1,0,ek3[m1,k1,t1,p1],1,ekT[1,k1,t1,p1],-1,ekT[-1,k1,t1,p1], _,Message["Polarization Does not exist"]]];
  Epolstar[m_,k_,\[Theta]_,\[Phi]_,l_]:=Module[{m1=m,k1=k,t1=\[Theta],p1=\[Phi],l1=l},Switch[l1,0,ek3[m1,k1,t1,p1],1,ekTstar[1,k1,t1,p1],-1,ekTstar[-1,k1,t1,p1], _,Message["Polarization Does not exist"]]];
  
  EpolEuc[m_,kx_,ky_,kz_,l_]:=Module[{M=m,kX=kx,kY=ky,kZ=kz,l1=l},Switch[l1,0,ek3Euc[M,kX,kY,kZ],1,ekTEuc[1,kX,kY,kZ],-1,ekTEuc[-1,kX,kY,kZ], _,Message["Polarization Does not exist"]]];
  EpolstarEuc[m_,kx_,ky_,kz_,l_]:=Module[{M=m,kX=kx,kY=ky,kZ=kz,l1=l},Switch[l1,0,ek3Euc[M,kX,kY,kZ],1,ekTstarEuc[1,kX,kY,kZ],-1,ekTstarEuc[-1,kX,kY,kZ], _,Message["Polarization Does not exist"]]];
  
  Epolc[m_,pT_,\[Eta]_,\[Phi]_,l_]:=Module[{m1=m,pT1=pT,e1=\[Eta],p1=\[Phi],l1=l},Switch[l1,0,ek3c[m1,pT1,e1,p1],1,ekTc[1,pT1,e1,p1],-1,ekTc[-1,pT1,e1,p1], _,Message["Polarization Does not exist"]]];
  Epolcstar[m_,pT_,\[Eta]_,\[Phi]_,l_]:=Module[{m1=m,pT1=pT,e1=\[Eta],p1=\[Phi],l1=l},Switch[l1,0,ek3c[m1,pT1,e1,p1],1,ekTcstar[1,pT1,e1,p1],-1,ekTcstar[-1,pT1,e1,p1], _,Message["Polarization Does not exist"]]];
  
  gmn={{1,0,0,0},{0,-1,0,0},{0,0,-1,0},{0,0,0,-1}};
  Gmn[m_,n_]:=Module[{m1=m,n1=n},gmn[[m1,n1]]];
  MyPair[k1_,k2_]:=Module[{p1=k1,p2=k2,x1,x2,x},x1=p1[[All]];x2=p2[[All]];x=Simplify[Sum[Gmn[i,i]x1[[i]] x2[[i]],{i,1,4}]];x];
  Cov4[p1_]:=Module[{k1=p1,x=p1},Do[x[[n]]=Gmn[n,n]k1[[n]],{n,1,4}];x];
  vec4[k_,\[Theta]_,\[Phi]_]:=Module[{},{k,k Sin[\[Theta]] Cos[\[Phi]],k Sin[\[Theta]] Sin[\[Phi]],k Cos[\[Theta]]}];
  vec4M[m_,k_,\[Theta]_,\[Phi]_]:=Module[{},{Sqrt[k^2 + m^2],k Sin[\[Theta]] Cos[\[Phi]],k Sin[\[Theta]] Sin[\[Phi]],k Cos[\[Theta]]}];
  vec4c[pT_,\[Eta]_,\[Phi]_]:=Module[{},{pT Cosh[\[Eta]],pT Cos[\[Phi]],pT Sin[\[Phi]],pT Sinh[\[Eta]]}];
  vec4Mc[m_,pT_,\[Eta]_,\[Phi]_]:=Module[{},{Sqrt[m^2+(pT Cosh[\[Eta]])^2],pT Cos[\[Phi]],pT Sin[\[Phi]],pT Sinh[\[Eta]]}];
  


End[]

EndPackage[]
