(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*EL*GS*MT^2*(4*Sqrt[MH^2 - 4*MT^2]*S*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
       MH*(-MH^2 + 4*MT^2 + S)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + MH*(4*(MH^2 - S) - 4*Sqrt[S*(-4*MT^2 + S)]*
          Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
         (MH^2 - 4*MT^2 - S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
             (2*MT^2)]^2))*Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], 
         Spinor[k[2], MU, 1]]*SUNT[Glu4, Col2, Col1]])/
     (8*MH*MW*Pi*(MH^2 - S)*S*SW)}}}}
