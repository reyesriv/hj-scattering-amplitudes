(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*EL*GS*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 1, k[3], 
      Spinor[k[2], MU, 1]]*SUNT[Glu4, Col2, Col1]]*Pair[ec[4], k[3]]*
   (PVC[0, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
    4*(PVC[0, 1, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 1, 1, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 2, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])))/
  (2*MW*Pi*S*SW) + 
 (Alfas*EL*GS*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], 
      Spinor[k[2], MU, 1]]*SUNT[Glu4, Col2, Col1]]*
   (-2*PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]] - 
    (MH^2 - S)*PVC[0, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
    2*(MH^2 - S)*PVC[0, 1, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]] + 8*PVC[1, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]]))/(4*MW*Pi*S*SW)
