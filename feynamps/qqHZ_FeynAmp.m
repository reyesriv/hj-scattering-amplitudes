(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfa2*MT2*Den[S, MZ2]*
  ((-4*(-27 + 108*SW2 - 144*SW2^2 + 128*SW2^3)*B0i[bb0, S, MT2, MT2] - 
     (-3 + 8*SW2)*(36*MT2 + MH2*(9 - 24*SW2 + 32*SW2^2) + 
       (MZ2 - S)*(9 - 24*SW2 + 32*SW2^2))*C0i[cc0, MH2, S, MZ2, MT2, MT2, 
       MT2] + 8*(-3 + 8*SW2)*(9 - 24*SW2 + 32*SW2^2)*
      C0i[cc00, MH2, S, MZ2, MT2, MT2, MT2] + 
     2*(8*MH2*SW2*(9 - 36*SW2 + 32*SW2^2) + (2*MH2 + MZ2 - S)*(3 - 8*SW2)*
        (9 - 24*SW2 + 32*SW2^2))*C0i[cc1, MH2, S, MZ2, MT2, MT2, MT2] - 
     (-3 + 8*SW2)*(9*MH2 - 9*S + MZ2*(45 - 96*SW2 + 128*SW2^2))*
      C0i[cc2, MH2, S, MZ2, MT2, MT2, MT2])*
    Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]] - (12*(9 - 12*SW2 + 16*SW2^2)*B0i[bb0, S, MT2, MT2] + 
     3*(36*MT2 + MH2*(9 - 24*SW2 + 32*SW2^2) + 
       (MZ2 - S)*(9 - 24*SW2 + 32*SW2^2))*C0i[cc0, MH2, S, MZ2, MT2, MT2, 
       MT2] - 24*(9 - 24*SW2 + 32*SW2^2)*C0i[cc00, MH2, S, MZ2, MT2, MT2, 
       MT2] + 2*(-24*MH2*SW2*(-3 + 4*SW2) + 3*(2*MH2 + MZ2 - S)*
        (9 - 24*SW2 + 32*SW2^2))*C0i[cc1, MH2, S, MZ2, MT2, MT2, MT2] + 
     3*(9*MH2 - 9*S + MZ2*(45 - 96*SW2 + 128*SW2^2))*
      C0i[cc2, MH2, S, MZ2, MT2, MT2, MT2])*
    Mat[DiracChain[Spinor[k[1], MU, -1], 5, ec[4], Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]] - 
   12*MU*((9 - 24*SW2 + 32*SW2^2)*C0i[cc0, MH2, S, MZ2, MT2, MT2, MT2] + 
     2*(9 - 24*SW2 + 32*SW2^2)*C0i[cc1, MH2, S, MZ2, MT2, MT2, MT2] + 
     4*(9 - 24*SW2 + 32*SW2^2)*C0i[cc12, MH2, S, MZ2, MT2, MT2, MT2] + 
     9*C0i[cc2, MH2, S, MZ2, MT2, MT2, MT2])*
    Mat[DiracChain[Spinor[k[1], MU, -1], 5, Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]]*Pair[ec[4], k[3]] - 
   2*((3 - 8*SW2)*(9 - 24*SW2 + 32*SW2^2)*C0i[cc0, MH2, S, MZ2, MT2, MT2, 
       MT2] + 4*(3 - 8*SW2)*(9 - 24*SW2 + 32*SW2^2)*
      (C0i[cc1, MH2, S, MZ2, MT2, MT2, MT2] + C0i[cc11, MH2, S, MZ2, MT2, 
        MT2, MT2] + C0i[cc12, MH2, S, MZ2, MT2, MT2, MT2]) - 
     9*(-3 + 8*SW2)*C0i[cc2, MH2, S, MZ2, MT2, MT2, MT2])*
    Mat[DiracChain[Spinor[k[1], MU, -1], 1, k[3], Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]]*Pair[ec[4], k[3]] + 
   6*((9 - 24*SW2 + 32*SW2^2)*C0i[cc0, MH2, S, MZ2, MT2, MT2, MT2] + 
     4*(9 - 24*SW2 + 32*SW2^2)*(C0i[cc1, MH2, S, MZ2, MT2, MT2, MT2] + 
       C0i[cc11, MH2, S, MZ2, MT2, MT2, MT2] + C0i[cc12, MH2, S, MZ2, MT2, 
        MT2, MT2]) + 9*C0i[cc2, MH2, S, MZ2, MT2, MT2, MT2])*
    Mat[DiracChain[Spinor[k[1], MU, -1], 5, k[3], Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]]*Pair[ec[4], k[3]]))/(144*CW*CW2*MW*SW2^2)
