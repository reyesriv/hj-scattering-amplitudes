(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas^2*col3*EL*MT^2*
   (((-2*(Pair[e[1], ec[5]]*((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH^2 - S34)*
                 Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
              Pair[e[2], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
                (MH^2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
              2*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + 
                T24*Pair[ec[4], k[3]]))) + 
          4*(Pair[e[1], k[5]]*(-((MH^2 - S34)*(Pair[e[2], k[1]] - 
                 Pair[e[2], k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*(
                (-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
            (MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
             Pair[ec[5], k[1]] + (MH^2 - S34)*Pair[e[2], ec[4]]*
             (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[
                e[2], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
                 Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(-(Pair[e[1], ec[5]]*(-((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*Pair[
                e[2], ec[4]]) + Pair[e[2], k[4]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + (5*MH^2 + 2*S - 3*S34 - 6*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + (MH^2 - 2*S + S34 - 2*T + 6*T14)*
                Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*(-2*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + 2*(Pair[e[1], k[5]]*(-2*(MH^2 - S34)*(
                Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
                (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])) - 
            2*(MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 2*(MH^2 - S34)*
             Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, 
          S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        8*(Pair[ec[4], k[3]]*(2*(-MH^2 + S34 + 2*T24)*Pair[e[1], k[5]]*
             Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*T24)*
               Pair[e[2], k[1]] + (-MH^2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
              (MH^2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
            2*(-MH^2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
            0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
           (2*(MH^2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
            Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
              (MH^2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
              (-3*MH^2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH^2 - S34 - 2*U)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 
            0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
            Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
            Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
                Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], 
                 k[1]] - Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(
                2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 
            2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
       (MH^2 - S - T - T14) + 
      (-2*(Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
             Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
          2*(2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
             (Pair[e[1], k[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
                2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH^2 - S34)*
                 Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
              Pair[e[1], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
                (MH^2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
            2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
             ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
             Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                  Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[4]]))))*
         PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + 2*(MH^2 - S34 - 2*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (5*MH^2 + 2*S - 3*S34 + 2*T24 - 6*U)*
                Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (MH^2 - 2*S + S34 + 6*T24 - 2*U)*
                Pair[ec[4], k[3]]))) + Pair[e[1], ec[4]]*
           ((MH^2 - S34)*(MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(MH^2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
          2*(-2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
              (MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
            2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - S34 - 2*T + 2*T14)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        8*(Pair[ec[4], k[3]]*(((-2*MH^2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
              (-MH^2 + S34 + 4*T24)*Pair[e[1], k[3]] + (MH^2 + 2*S - S34 + 
                2*T24 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            2*(MH^2 - S34 - 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(MH^2 - S34 - 2*T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
           (-(((-2*MH^2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - (MH^2 - 2*S + S34 + 
                 2*T24 - 2*U)*Pair[e[1], k[3]] + (-3*MH^2 + S34 + 4*U)*
                Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 2*(-MH^2 + S34 + 2*T)*
             Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 2*(-MH^2 + S34 + 2*T)*
             Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]) + 
        (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
       (MH^2 - S - T24 - U))/S34 - 
    ((-2*(-4*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH^2 - S34 - T - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
           (-((S - T14)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
            4*(MH^2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
           ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - 
                U)*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
             ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + T + 
                U)*Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, 
          2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH^2 + S + T14 + T24)*
              Pair[e[2], ec[4]]) + 4*(MH^2 - S34 - T - U)*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) + 2*(-2*(MH^2 - S34 - T - U)*
             (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
             Pair[ec[4], ec[5]] - 2*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - 
              Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
            8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                 Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                 Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*
             Pair[e[2], k[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
              (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]]) + 
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(2*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[1]] + (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], 
                k[3]])) - Pair[e[2], ec[4]]*(4*(S - T14)*Pair[e[1], k[3]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[4]]*(4*(MH^2 - S34 - T - U)*
               Pair[ec[5], k[2]] + (MH^2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[
                ec[5], k[3]]) + Pair[e[1], k[2]]*((MH^2 - 5*S34 + 3*T - 
                2*T24 + 3*U)*Pair[ec[5], k[3]] + 4*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[4]])))*PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        8*(-(((MH^2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
             2*(S - T14)*Pair[e[1], k[3]] + (MH^2 + S34 + T - 2*T24 - 3*U)*
              Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
          2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 
          2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        8*(((MH^2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
             Pair[e[1], k[3]] + (MH^2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
            Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
             (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
              Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
         (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]))/T24 + 
      (-2*(4*(-((MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH^2 - S34 - T - U)*
             Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
            2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH^2 + S34 + T + U)*Pair[
                ec[5], k[2]] + (-MH^2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
            Pair[e[1], k[4]]*(-((MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - 
                 Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*(
                (-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH^2 + S + T24 + 
                  U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           (-((S - T24)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
            2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
              Pair[e[2], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                (-2*MH^2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
              Pair[e[2], k[1]]*((-2*MH^2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                  k[3]] + 2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))))*
         PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] - 4*(2*(-2*(MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
            2*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
            8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[2]] + (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], 
                k[3]]) + Pair[e[1], k[4]]*(-2*(MH^2 - S34 - T - U)*(
                Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + 
                (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
          Pair[e[1], ec[4]]*(-((S - T24)*(-MH^2 + S + T14 + T24)*
              Pair[e[2], ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(4*(MH^2 - S - T14 - T24)*
               Pair[ec[5], k[1]] + (-7*MH^2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[1]]*((-7*MH^2 + 3*S + 8*S34 + 
                5*T14 + 3*T24)*Pair[ec[5], k[3]] + 4*(-MH^2 + S + T14 + T24)*
               Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        8*((-2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
             Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-3*MH^2 + S + 4*S34 + 
                3*T14 + T24)*Pair[e[2], k[1]] - 2*(S - T24)*Pair[e[2], 
                k[3]] + (-3*MH^2 + S + 4*T + 3*T14 + T24)*Pair[e[2], k[4]]) - 
            2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], e[2]]*
             Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                 Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
           Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (-2*(MH^2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
            Pair[e[1], ec[4]]*((MH^2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 
              2*(S - T24)*Pair[e[2], k[3]] + (MH^2 + S34 - 3*T + U)*Pair[
                e[2], k[4]]) - 2*(MH^2 - S34 - T + U)*Pair[e[1], e[2]]*
             Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                 Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
           Pair[ec[5], k[3]]*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
                Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
             (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
         (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]))/T14)/(2*MH^2 - S34 - T - U)))/(MW*SW) + 
 (Alfas^2*col4*EL*MT^2*
   (((-2*(Pair[e[1], ec[5]]*((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH^2 - S34)*
                 Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
              Pair[e[2], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
                (MH^2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
              2*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + 
                T24*Pair[ec[4], k[3]]))) + 
          4*(Pair[e[1], k[5]]*(-((MH^2 - S34)*(Pair[e[2], k[1]] - 
                 Pair[e[2], k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*(
                (-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
            (MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
             Pair[ec[5], k[1]] + (MH^2 - S34)*Pair[e[2], ec[4]]*
             (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[
                e[2], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
                 Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(-(Pair[e[1], ec[5]]*(-((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*Pair[
                e[2], ec[4]]) + Pair[e[2], k[4]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + (5*MH^2 + 2*S - 3*S34 - 6*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + (MH^2 - 2*S + S34 - 2*T + 6*T14)*
                Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*(-2*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + 2*(Pair[e[1], k[5]]*(-2*(MH^2 - S34)*(
                Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
                (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])) - 
            2*(MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 2*(MH^2 - S34)*
             Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, 
          S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        8*(Pair[ec[4], k[3]]*(2*(-MH^2 + S34 + 2*T24)*Pair[e[1], k[5]]*
             Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*T24)*
               Pair[e[2], k[1]] + (-MH^2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
              (MH^2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
            2*(-MH^2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
            0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
           (2*(MH^2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
            Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
              (MH^2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
              (-3*MH^2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH^2 - S34 - 2*U)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 
            0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
            Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
            Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
                Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], 
                 k[1]] - Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(
                2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 
            2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
       (MH^2 - S - T - T14) + 
      (-2*(Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
             Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
          2*(2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
             (Pair[e[1], k[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
                2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH^2 - S34)*
                 Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
              Pair[e[1], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
                (MH^2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
            2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
             ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
             Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                  Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[4]]))))*
         PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[1]] + 2*(MH^2 - S34 - 2*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (5*MH^2 + 2*S - 3*S34 + 2*T24 - 6*U)*
                Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-4*(MH^2 - S34)*
                Pair[ec[4], k[2]] + (MH^2 - 2*S + S34 + 6*T24 - 2*U)*
                Pair[ec[4], k[3]]))) + Pair[e[1], ec[4]]*
           ((MH^2 - S34)*(MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(MH^2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
          2*(-2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
              (MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
            2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - S34 - 2*T + 2*T14)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        8*(Pair[ec[4], k[3]]*(((-2*MH^2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
              (-MH^2 + S34 + 4*T24)*Pair[e[1], k[3]] + (MH^2 + 2*S - S34 + 
                2*T24 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            2*(MH^2 - S34 - 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(MH^2 - S34 - 2*T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
           (-(((-2*MH^2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - (MH^2 - 2*S + S34 + 
                 2*T24 - 2*U)*Pair[e[1], k[3]] + (-3*MH^2 + S34 + 4*U)*
                Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 2*(-MH^2 + S34 + 2*T)*
             Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 2*(-MH^2 + S34 + 2*T)*
             Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]) + 
        (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
       (MH^2 - S - T24 - U))/S34 - 
    ((-2*(-4*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH^2 - S34 - T - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
           (-((S - T14)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
            4*(MH^2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
           ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - 
                U)*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
             ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + T + 
                U)*Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, 
          2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH^2 + S + T14 + T24)*
              Pair[e[2], ec[4]]) + 4*(MH^2 - S34 - T - U)*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) + 2*(-2*(MH^2 - S34 - T - U)*
             (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
             Pair[ec[4], ec[5]] - 2*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - 
              Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
            8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                 Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                 Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*
             Pair[e[2], k[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
              (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]]) + 
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(2*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[1]] + (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], 
                k[3]])) - Pair[e[2], ec[4]]*(4*(S - T14)*Pair[e[1], k[3]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[4]]*(4*(MH^2 - S34 - T - U)*
               Pair[ec[5], k[2]] + (MH^2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[
                ec[5], k[3]]) + Pair[e[1], k[2]]*((MH^2 - 5*S34 + 3*T - 
                2*T24 + 3*U)*Pair[ec[5], k[3]] + 4*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[4]])))*PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
        8*(-(((MH^2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
             2*(S - T14)*Pair[e[1], k[3]] + (MH^2 + S34 + T - 2*T24 - 3*U)*
              Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
          2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 
          2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        8*(((MH^2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
             Pair[e[1], k[3]] + (MH^2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
            Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
             (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
              Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
         (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]))/T24 + 
      (-2*(4*(-((MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH^2 - S34 - T - U)*
             Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
            2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH^2 + S34 + T + U)*Pair[
                ec[5], k[2]] + (-MH^2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
            Pair[e[1], k[4]]*(-((MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - 
                 Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*(
                (-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH^2 + S + T24 + 
                  U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           (-((S - T24)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
            2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
              Pair[e[2], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                (-2*MH^2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
              Pair[e[2], k[1]]*((-2*MH^2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                  k[3]] + 2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))))*
         PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] - 4*(2*(-2*(MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
            2*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
            8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH^2 + S34 + T + U)*Pair[
                ec[5], k[2]] + (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], 
                k[3]]) + Pair[e[1], k[4]]*(-2*(MH^2 - S34 - T - U)*(
                Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + 
                (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
          Pair[e[1], ec[4]]*(-((S - T24)*(-MH^2 + S + T14 + T24)*
              Pair[e[2], ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(4*(MH^2 - S - T14 - T24)*
               Pair[ec[5], k[1]] + (-7*MH^2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[1]]*((-7*MH^2 + 3*S + 8*S34 + 
                5*T14 + 3*T24)*Pair[ec[5], k[3]] + 4*(-MH^2 + S + T14 + T24)*
               Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        8*((-2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
             Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-3*MH^2 + S + 4*S34 + 
                3*T14 + T24)*Pair[e[2], k[1]] - 2*(S - T24)*Pair[e[2], 
                k[3]] + (-3*MH^2 + S + 4*T + 3*T14 + T24)*Pair[e[2], k[4]]) - 
            2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], e[2]]*
             Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                 Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
           Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (-2*(MH^2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
            Pair[e[1], ec[4]]*((MH^2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 
              2*(S - T24)*Pair[e[2], k[3]] + (MH^2 + S34 - 3*T + U)*Pair[
                e[2], k[4]]) - 2*(MH^2 - S34 - T + U)*Pair[e[1], e[2]]*
             Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[
                ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                 Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
           Pair[ec[5], k[3]]*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
                Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
             (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
         (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]))/T14)/(2*MH^2 - S34 - T - U)))/(MW*SW) - 
 (Alfas^2*col1*EL*MT^2*
   ((-2*(Pair[e[1], ec[5]]*((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
           Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH^2 - S34)*Pair[
                ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 + S - S34 - T + T14)*
               Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]]))) + 
        4*(Pair[e[1], k[5]]*(-((MH^2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
          (MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + (MH^2 - S34)*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 
        0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(-(Pair[e[1], ec[5]]*(-((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]]) + Pair[e[2], k[4]]*
            (-4*(MH^2 - S34)*Pair[ec[4], k[1]] + (5*MH^2 + 2*S - 3*S34 - 6*
                T + 2*T14)*Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*
            (-4*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - 2*S + S34 - 2*T + 6*
                T14)*Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
            (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
              Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*(-2*(MH^2 - S34)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])) - 2*(MH^2 - S34)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
            (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
          2*(MH^2 - S34)*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 
          8*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] - Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      8*(Pair[ec[4], k[3]]*(2*(-MH^2 + S34 + 2*T24)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*T24)*
             Pair[e[2], k[1]] + (-MH^2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
            (MH^2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
          2*(-MH^2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (2*(MH^2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
            (MH^2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
            (-3*MH^2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH^2 - S34 - 2*U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
          Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
          Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
        2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     (S34*(MH^2 - S - T - T14)) - 
    (-2*(-4*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH^2 - S34 - T - U)*
         (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         (-((S - T14)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
          4*(MH^2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
         ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + 
            (S34 + T - T24 - U)*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
           ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + T + U)*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH^2 + S + T14 + T24)*
            Pair[e[2], ec[4]]) + 4*(MH^2 - S34 - T - U)*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) + 2*(-2*(MH^2 - S34 - T - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
           Pair[ec[4], ec[5]] - 2*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - 
            Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(2*(-MH^2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*
             Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
         (4*(S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(4*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + 
            (MH^2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*((MH^2 - 5*S34 + 3*T - 2*T24 + 3*U)*
             Pair[ec[5], k[3]] + 4*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - 8*(-(((MH^2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
           2*(S - T14)*Pair[e[1], k[3]] + (MH^2 + S34 + T - 2*T24 - 3*U)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
        2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 
        2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      8*(((MH^2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
           Pair[e[1], k[3]] + (MH^2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]] - 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 2, 0, MH^2, 
        2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
           (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(T24*(2*MH^2 - S34 - T - U))))/(MW*SW) - 
 (Alfas^2*col2*EL*MT^2*
   ((-2*(Pair[e[1], ec[5]]*((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
           Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH^2 - S34)*Pair[
                ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 + S - S34 - T + T14)*
               Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]]))) + 
        4*(Pair[e[1], k[5]]*(-((MH^2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
          (MH^2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + (MH^2 - S34)*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 
        0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(-(Pair[e[1], ec[5]]*(-((MH^2 - S34)*(MH^2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]]) + Pair[e[2], k[4]]*
            (-4*(MH^2 - S34)*Pair[ec[4], k[1]] + (5*MH^2 + 2*S - 3*S34 - 6*
                T + 2*T14)*Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*
            (-4*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - 2*S + S34 - 2*T + 6*
                T14)*Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
            (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
              Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*(-2*(MH^2 - S34)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (-2*(MH^2 - S34)*Pair[ec[4], k[2]] + (MH^2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])) - 2*(MH^2 - S34)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
            (MH^2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
          2*(MH^2 - S34)*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 
          8*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] - Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      8*(Pair[ec[4], k[3]]*(2*(-MH^2 + S34 + 2*T24)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*T24)*
             Pair[e[2], k[1]] + (-MH^2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
            (MH^2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
          2*(-MH^2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (2*(MH^2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*((-2*MH^2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
            (MH^2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
            (-3*MH^2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH^2 - S34 - 2*U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 
          0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
          Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
          Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
        2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     (S34*(MH^2 - S - T - T14)) - 
    (-2*(-4*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH^2 - S34 - T - U)*
         (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         (-((S - T14)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
          4*(MH^2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (MH^2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
         ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + 
            (S34 + T - T24 - U)*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
           ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + T + U)*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH^2 + S + T14 + T24)*
            Pair[e[2], ec[4]]) + 4*(MH^2 - S34 - T - U)*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) + 2*(-2*(MH^2 - S34 - T - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
           Pair[ec[4], ec[5]] - 2*(MH^2 - S34 - T - U)*(Pair[e[1], k[2]] - 
            Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*(2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(2*(-MH^2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (3*MH^2 - 3*S34 + T - 2*T24 - 3*U)*
             Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
         (4*(S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(4*(MH^2 - S34 - T - U)*Pair[ec[5], k[2]] + 
            (MH^2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*((MH^2 - 5*S34 + 3*T - 2*T24 + 3*U)*
             Pair[ec[5], k[3]] + 4*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - 8*(-(((MH^2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
           2*(S - T14)*Pair[e[1], k[3]] + (MH^2 + S34 + T - 2*T24 - 3*U)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
        2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        2*(MH^2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 1, 1, MH^2, 
        2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      8*(((MH^2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
           Pair[e[1], k[3]] + (MH^2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]] + 2*(-MH^2 + S34 - T + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]] - 
        8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*PVC[0, 2, 0, MH^2, 
        2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
           (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(T24*(2*MH^2 - S34 - T - U))))/(MW*SW) + 
 (Alfas^2*col5*EL*MT^2*
   (-((-2*(Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
            Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
         2*(2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
            Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
             T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
            (Pair[e[1], k[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 2*T14*
                Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH^2 - S34)*
                Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
             Pair[e[1], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
               (MH^2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
           2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
            ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
            Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
            (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH^2 - S34)*Pair[ec[4], 
                k[1]] + 2*(MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[4]]*(-4*(MH^2 - S34)*Pair[ec[4], k[2]] + 
              (5*MH^2 + 2*S - 3*S34 + 2*T24 - 6*U)*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[3]]*(-4*(MH^2 - S34)*Pair[ec[4], k[2]] + 
              (MH^2 - 2*S + S34 + 6*T24 - 2*U)*Pair[ec[4], k[3]]))) + 
         Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
            Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
         2*(-2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
            Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
             (MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
           2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
            (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - S34 - 2*T + 2*T14)*
              Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
            (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] + 
       8*(Pair[ec[4], k[3]]*(((-2*MH^2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
             (-MH^2 + S34 + 4*T24)*Pair[e[1], k[3]] + 
             (MH^2 + 2*S - S34 + 2*T24 - 2*U)*Pair[e[1], k[4]])*
            Pair[e[2], ec[5]] + 2*(MH^2 - S34 - 2*T14)*Pair[e[1], ec[5]]*
            Pair[e[2], k[5]] + 2*(MH^2 - S34 - 2*T14)*Pair[e[1], e[2]]*
            Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + 
             Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
           0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
          (-(((-2*MH^2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - (MH^2 - 2*S + S34 + 
                2*T24 - 2*U)*Pair[e[1], k[3]] + (-3*MH^2 + S34 + 4*U)*Pair[
                e[1], k[4]])*Pair[e[2], ec[5]]) + 2*(-MH^2 + S34 + 2*T)*
            Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 2*(-MH^2 + S34 + 2*T)*
            Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
           8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
          (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
          ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], 
           Sqrt[MT^2]] - 16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/(S34*(MH^2 - S - T24 - U))) + 
    (-2*(4*(-((MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH^2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH^2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH^2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-((MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
             ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH^2 + S + T24 + 
                U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
          2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
              (-2*MH^2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*((-2*MH^2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                k[3]] + 2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - 4*(2*(-2*(MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
          2*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]) - 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*
           Pair[ec[5], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           (2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-2*(MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             (2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (3*MH^2 - 3*S34 - 
                3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*(-((S - T24)*(-MH^2 + S + T14 + T24)*
            Pair[e[2], ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(4*(MH^2 - S - T14 - T24)*
             Pair[ec[5], k[1]] + (-7*MH^2 + 3*S + 8*T + 5*T14 + 3*T24)*
             Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           ((-7*MH^2 + 3*S + 8*S34 + 5*T14 + 3*T24)*Pair[ec[5], k[3]] + 
            4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 8*((-2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           ((-3*MH^2 + S + 4*S34 + 3*T14 + T24)*Pair[e[2], k[1]] - 
            2*(S - T24)*Pair[e[2], k[3]] + (-3*MH^2 + S + 4*T + 3*T14 + T24)*
             Pair[e[2], k[4]]) - 2*(MH^2 - S34 - T - 2*T14 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-2*(MH^2 - S34 - T + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*((MH^2 - 3*S34 + T + U)*
             Pair[e[2], k[1]] + 2*(S - T24)*Pair[e[2], k[3]] + 
            (MH^2 + S34 - 3*T + U)*Pair[e[2], k[4]]) - 2*(MH^2 - S34 - T + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) + 
      (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
           (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
           (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(T14*(2*MH^2 - S34 - T - U))))/(MW*SW) + 
 (Alfas^2*col6*EL*MT^2*
   (-((-2*(Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
            Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
         2*(2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
            Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
             T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
            (Pair[e[1], k[2]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 2*T14*
                Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH^2 - S34)*
                Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
             Pair[e[1], k[4]]*(-2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
               (MH^2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
           2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
            ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
            Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
            (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]]))))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH^2 - S34)*Pair[ec[4], 
                k[1]] + 2*(MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[4]]*(-4*(MH^2 - S34)*Pair[ec[4], k[2]] + 
              (5*MH^2 + 2*S - 3*S34 + 2*T24 - 6*U)*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[3]]*(-4*(MH^2 - S34)*Pair[ec[4], k[2]] + 
              (MH^2 - 2*S + S34 + 6*T24 - 2*U)*Pair[ec[4], k[3]]))) + 
         Pair[e[1], ec[4]]*((MH^2 - S34)*(MH^2 - 2*S - T - T14)*
            Pair[e[2], ec[5]] + 4*(MH^2 - S34)*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
         2*(-2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
            Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
             (MH^2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
           2*(MH^2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
            (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (MH^2 - S34 - 2*T + 2*T14)*
              Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
            (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]]))))*PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] + 
       8*(Pair[ec[4], k[3]]*(((-2*MH^2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
             (-MH^2 + S34 + 4*T24)*Pair[e[1], k[3]] + 
             (MH^2 + 2*S - S34 + 2*T24 - 2*U)*Pair[e[1], k[4]])*
            Pair[e[2], ec[5]] + 2*(MH^2 - S34 - 2*T14)*Pair[e[1], ec[5]]*
            Pair[e[2], k[5]] + 2*(MH^2 - S34 - 2*T14)*Pair[e[1], e[2]]*
            Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + 
             Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, S34, 
           0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
          (-(((-2*MH^2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - (MH^2 - 2*S + S34 + 
                2*T24 - 2*U)*Pair[e[1], k[3]] + (-3*MH^2 + S34 + 4*U)*Pair[
                e[1], k[4]])*Pair[e[2], ec[5]]) + 2*(-MH^2 + S34 + 2*T)*
            Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 2*(-MH^2 + S34 + 2*T)*
            Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
           8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
          (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
          ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[5], k[2]])))*(4*PVB[0, 0, S34, Sqrt[MT^2], 
           Sqrt[MT^2]] - 16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/(S34*(MH^2 - S - T24 - U))) + 
    (-2*(4*(-((MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH^2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH^2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH^2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-((MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
             ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH^2 + S + T24 + 
                U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH^2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
          2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
              (-2*MH^2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*((-2*MH^2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                k[3]] + 2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - 4*(2*(-2*(MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
          2*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]) - 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*
           Pair[ec[5], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           (2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (3*MH^2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-2*(MH^2 - S34 - T - U)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             (2*(-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (3*MH^2 - 3*S34 - 
                3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*(-((S - T24)*(-MH^2 + S + T14 + T24)*
            Pair[e[2], ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(4*(MH^2 - S - T14 - T24)*
             Pair[ec[5], k[1]] + (-7*MH^2 + 3*S + 8*T + 5*T14 + 3*T24)*
             Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           ((-7*MH^2 + 3*S + 8*S34 + 5*T14 + 3*T24)*Pair[ec[5], k[3]] + 
            4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 8*((-2*(MH^2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           ((-3*MH^2 + S + 4*S34 + 3*T14 + T24)*Pair[e[2], k[1]] - 
            2*(S - T24)*Pair[e[2], k[3]] + (-3*MH^2 + S + 4*T + 3*T14 + T24)*
             Pair[e[2], k[4]]) - 2*(MH^2 - S34 - T - 2*T14 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-2*(MH^2 - S34 - T + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*((MH^2 - 3*S34 + T + U)*
             Pair[e[2], k[1]] + 2*(S - T24)*Pair[e[2], k[3]] + 
            (MH^2 + S34 - 3*T + U)*Pair[e[2], k[4]]) - 2*(MH^2 - S34 - T + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]*
         PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) + 
      (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
           (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
           (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]])))*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(T14*(2*MH^2 - S34 - T - U))))/(MW*SW)
