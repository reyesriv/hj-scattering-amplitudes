(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*c12*EL*MT^2*(-4*MH^2 + (MH^2 - 4*MT^2)*
    Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2)*
  Pair[e[1], e[2]])/(4*MH^2*MW*Pi*SW)
