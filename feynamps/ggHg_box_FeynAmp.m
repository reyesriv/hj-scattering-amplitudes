(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas*EL*GS*MT2*Mat[SUNT[Glu1, Glu2, Glu4, 0, 0]]*
    ((C0i[cc1, 0, 0, T, MT2, MT2, MT2]*Pair[e[1], k[2]] - 
       C0i[cc1, 0, MH2, S, MT2, MT2, MT2]*Pair[e[1], k[4]])*
      Pair[e[2], ec[4]] - C0i[cc2, 0, 0, T, MT2, MT2, MT2]*
      (-(Pair[e[1], k[3]]*Pair[e[2], ec[4]]) + Pair[e[1], ec[4]]*
        Pair[e[2], k[4]]) + 
     (-4*(D0i[dd133, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
         D0i[dd133, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*
        Pair[e[1], k[2]] + 4*D0i[dd133, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
         MT2]*Pair[e[1], k[3]])*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
     4*(D0i[dd233, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]] + 
       (D0i[dd233, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
         D0i[dd233, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*
        Pair[e[1], k[4]])*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
     4*(D0i[dd002, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
       D0i[dd002, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*
      (Pair[e[1], k[4]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
        Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[4], k[1]]) + 
     4*(D0i[dd003, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
       D0i[dd003, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
       D0i[dd003, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*
      (Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]) - D0i[dd13, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, 
       MT2]*Pair[e[2], k[1]]*(-(S*Pair[e[1], ec[4]]) + 
       4*Pair[e[1], k[2]]*Pair[ec[4], k[1]]) - 
     D0i[dd12, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
      (-(S*Pair[e[1], ec[4]]) + 4*Pair[e[1], k[2]]*Pair[ec[4], k[1]]) - 
     D0i[dd23, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[1]]*
      (U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*Pair[ec[4], k[1]]) - 
     D0i[dd22, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
      (U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*Pair[ec[4], k[1]]) - 
     D0i[dd22, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
      (U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], k[4]]*
        (-2*U*Pair[e[2], ec[4]] + 8*Pair[e[2], k[3]]*Pair[ec[4], k[1]])) + 
     D0i[dd13, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
      ((-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
       Pair[e[1], k[3]]*(-(U*Pair[e[2], ec[4]]) + 
         4*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[1]])) - 
     D0i[dd23, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
      (U*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], k[4]]*
        (-(U*Pair[e[2], ec[4]]) + 4*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
          Pair[ec[4], k[1]])) - 
     4*((D0i[dd222, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
         D0i[dd222, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*Pair[e[1], k[4]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
       (D0i[dd223, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
         D0i[dd223, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*Pair[e[1], k[4]]*
        (2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
       D0i[dd223, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
        (-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]]))) + 
     4*D0i[dd222, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
      (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
      Pair[ec[4], k[2]] - 4*(D0i[dd001, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
        MT2] + D0i[dd001, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*
      (Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
        Pair[ec[4], k[2]]) + D0i[dd13, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
       MT2]*Pair[e[1], k[2]]*(-(S*Pair[e[2], ec[4]]) + 
       4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + 
     D0i[dd23, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
      (-(S*Pair[e[2], ec[4]]) + 4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) - 
     4*D0i[dd123, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
      (Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
        Pair[ec[4], k[1]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
        Pair[ec[4], k[2]]) + D0i[dd12, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
       MT2]*Pair[e[1], k[2]]*(T*Pair[e[2], ec[4]] + 4*Pair[e[2], k[4]]*
        Pair[ec[4], k[2]]) + D0i[dd22, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
       MT2]*(T*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
        Pair[ec[4], k[2]]) + 2*(D0i[dd00, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, 
         MT2]*(-(Pair[e[1], ec[4]]*Pair[e[2], k[1]]) + 2*Pair[e[1], e[2]]*
          Pair[ec[4], k[1]]) + D0i[dd00, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
         MT2]*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - 2*Pair[e[2], k[4]]) + 2*Pair[e[1], e[2]]*
          Pair[ec[4], k[1]]) + D0i[dd00, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
         MT2]*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]])) + 4*D0i[dd123, MH2, 0, 0, 0, U, T, MT2, MT2, 
       MT2, MT2]*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
        Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
        (-2*Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
          (4*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))) - 
     (D0i[dd0, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
       ((U*Pair[e[1], k[2]] + S*Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
        Pair[e[1], ec[4]]*(T*Pair[e[2], k[1]] + S*Pair[e[2], k[4]]) + 
        Pair[e[1], e[2]]*(T*Pair[ec[4], k[1]] - (2*S + U)*
           Pair[ec[4], k[2]]) + 2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 2*Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))))/4 + 
     (D0i[dd3, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
        ((-MH2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*Pair[e[2], k[1]])*
        Pair[ec[4], k[1]] + D0i[dd3, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
        Pair[e[2], k[1]]*((-MH2 + T)*Pair[e[1], ec[4]] + 
         2*Pair[e[1], k[3]]*Pair[ec[4], k[1]]) + 
       D0i[dd3, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
        (Pair[e[2], k[1]]*(-(U*Pair[e[1], ec[4]]) + 2*Pair[e[1], k[3]]*
            Pair[ec[4], k[1]]) + S*Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 
           Pair[ec[4], k[3]])))/2 - C0i[cc2, 0, MH2, S, MT2, MT2, MT2]*
      (Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
        Pair[ec[4], k[3]]) - C0i[cc2, MH2, 0, S, MT2, MT2, MT2]*
      (Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
        Pair[ec[4], k[3]]) - C0i[cc1, MH2, 0, S, MT2, MT2, MT2]*
      (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
        Pair[ec[4], k[3]]) - 4*D0i[dd112, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
       MT2]*Pair[e[2], k[3]]*(Pair[e[1], k[3]]*(Pair[ec[4], k[2]] - 
         3*Pair[ec[4], k[3]]) + Pair[e[1], k[2]]*Pair[ec[4], k[3]]) + 
     4*(Pair[e[1], k[2]]*(D0i[dd112, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
          Pair[e[2], k[3]] + D0i[dd112, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
           MT2]*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
       D0i[dd111, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[3]]) + 
     (D0i[dd2, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
       (((T + U)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]] + U*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
          3*Pair[e[2], k[3]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(-5*Pair[e[2], k[1]]*
             Pair[ec[4], k[1]] + Pair[e[2], k[4]]*(5*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]]))) - Pair[e[1], e[2]]*
         (2*U*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]])))/2 + 
     4*(D0i[dd002, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
        (-(Pair[e[1], k[3]]*Pair[e[2], ec[4]]) + Pair[e[1], ec[4]]*
          Pair[e[2], k[4]] - Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
       D0i[dd001, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
        (Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
          Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])) + 
     4*((D0i[dd113, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
         D0i[dd113, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[2]] + D0i[dd113, MH2, 0, 0, 0, U, T, 
         MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
        (Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[4], k[3]])) + D0i[dd11, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
       MT2]*((-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 
       Pair[e[1], k[3]]*((MH2 + U)*Pair[e[2], ec[4]] - 4*Pair[e[2], k[3]]*
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) + 
     (-(D0i[dd2, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
         (((-T + U)*Pair[e[1], k[2]] + T*Pair[e[1], k[3]])*
           Pair[e[2], ec[4]] + U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + 
          Pair[e[1], e[2]]*((-MH2 + T)*Pair[ec[4], k[2]] + 
            S*Pair[ec[4], k[3]]) - 2*(Pair[e[1], k[3]]*(Pair[e[2], k[3]] + 
              Pair[e[2], k[4]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]))) - 
       D0i[dd2, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
        (-(T*Pair[e[1], k[3]]*Pair[e[2], ec[4]]) - Pair[e[1], ec[4]]*
          (T*Pair[e[2], k[1]] - U*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*
          (T*Pair[ec[4], k[1]] - S*Pair[ec[4], k[2]]) + 
         2*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
           Pair[e[1], k[4]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]])))/2 + 
     4*(D0i[dd122, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
        (2*Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
        Pair[ec[4], k[2]] + D0i[dd123, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
         MT2]*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[2]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[1]]) + 
           2*Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + Pair[e[2], k[3]]*
        (D0i[dd122, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
          (-(Pair[e[1], k[3]]*Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
            (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
         D0i[dd122, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
          (Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))) - 
     D0i[dd12, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
      ((2*U*Pair[e[1], k[2]] - (MH2 + 3*U)*Pair[e[1], k[4]])*
        Pair[e[2], ec[4]] + Pair[e[2], k[3]]*((S + 2*U)*Pair[e[1], ec[4]] - 
         4*(2*Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))) + 
     (D0i[dd0, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
        (((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
          ((-MH2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]]) + 
         2*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
            Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
          ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] - 
           S*Pair[ec[4], k[3]])) + D0i[dd0, MH2, 0, 0, 0, U, T, MT2, MT2, 
         MT2, MT2]*(((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*((MH2 + U)*Pair[e[2], k[1]] - 
           (S + 2*U)*Pair[e[2], k[3]]) - Pair[e[1], e[2]]*
          ((MH2 + U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
           S*Pair[ec[4], k[3]]) + 2*(Pair[e[1], k[3]]*
            (2*Pair[e[2], k[3]]*Pair[ec[4], k[1]] - Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
            (-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
              Pair[ec[4], k[3]]))))/4 + 
     (-(C0i[cc0, 0, MH2, S, MT2, MT2, MT2]*
         ((2*Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
          Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])) + 
       C0i[cc0, 0, 0, T, MT2, MT2, MT2]*
        ((Pair[e[1], k[2]] - 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]] - Pair[e[1], e[2]]*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])) - 
       D0i[dd1, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[2]]*
        ((-MH2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
          Pair[ec[4], k[2]]) - D0i[dd1, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
         MT2]*(-(S*Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[2]]*(-(T*Pair[e[2], ec[4]]) + 2*Pair[e[2], k[3]]*
            Pair[ec[4], k[2]])) - C0i[cc0, MH2, 0, S, MT2, MT2, MT2]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
         Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])) - 
       D0i[dd1, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
        (-(((-MH2 + T)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
           Pair[e[2], ec[4]]) - Pair[e[1], ec[4]]*
          ((MH2 + U)*Pair[e[2], k[1]] - (2*S + 3*U)*Pair[e[2], k[3]]) + 
         Pair[e[1], e[2]]*((MH2 + U)*Pair[ec[4], k[1]] + 
           (-MH2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) - 
         2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[2]] + 
              5*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]))))/2))/(MW*Pi*SW)) + 
 (Alfas*EL*GS*MT2*Mat[SUNT[Glu1, Glu4, Glu2, 0, 0]]*
   ((C0i[cc1, 0, 0, T, MT2, MT2, MT2]*Pair[e[1], k[2]] - 
      C0i[cc1, 0, MH2, S, MT2, MT2, MT2]*Pair[e[1], k[4]])*
     Pair[e[2], ec[4]] - C0i[cc2, 0, 0, T, MT2, MT2, MT2]*
     (-(Pair[e[1], k[3]]*Pair[e[2], ec[4]]) + Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]) + 
    (-4*(D0i[dd133, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
        D0i[dd133, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*
       Pair[e[1], k[2]] + 4*D0i[dd133, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
        MT2]*Pair[e[1], k[3]])*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
    4*(D0i[dd233, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]] + 
      (D0i[dd233, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
        D0i[dd233, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*Pair[e[1], k[4]])*
     Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
    4*(D0i[dd002, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
      D0i[dd002, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*
     (Pair[e[1], k[4]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[4], k[1]]) + 
    4*(D0i[dd003, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
      D0i[dd003, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
      D0i[dd003, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*
     (Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
       Pair[ec[4], k[1]]) - D0i[dd13, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
     Pair[e[2], k[1]]*(-(S*Pair[e[1], ec[4]]) + 4*Pair[e[1], k[2]]*
       Pair[ec[4], k[1]]) - D0i[dd12, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(-(S*Pair[e[1], ec[4]]) + 4*Pair[e[1], k[2]]*
       Pair[ec[4], k[1]]) - D0i[dd23, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
     Pair[e[2], k[1]]*(U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*
       Pair[ec[4], k[1]]) - D0i[dd22, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*
       Pair[ec[4], k[1]]) - D0i[dd22, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
     (U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], k[4]]*
       (-2*U*Pair[e[2], ec[4]] + 8*Pair[e[2], k[3]]*Pair[ec[4], k[1]])) + 
    D0i[dd13, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
     ((-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
      Pair[e[1], k[3]]*(-(U*Pair[e[2], ec[4]]) + 
        4*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[1]])) - 
    D0i[dd23, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
     (U*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], k[4]]*
       (-(U*Pair[e[2], ec[4]]) + 4*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
         Pair[ec[4], k[1]])) - 
    4*((D0i[dd222, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
        D0i[dd222, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*Pair[e[1], k[4]]*
       Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
      (D0i[dd223, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2] + 
        D0i[dd223, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2])*Pair[e[1], k[4]]*
       (2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
      D0i[dd223, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
       (-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]]))) + 
    4*D0i[dd222, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
     Pair[ec[4], k[2]] - 4*(D0i[dd001, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
       MT2] + D0i[dd001, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*
     (Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]) + D0i[dd13, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
     Pair[e[1], k[2]]*(-(S*Pair[e[2], ec[4]]) + 4*Pair[e[2], k[1]]*
       Pair[ec[4], k[2]]) + D0i[dd23, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
     Pair[e[1], k[3]]*(-(S*Pair[e[2], ec[4]]) + 4*Pair[e[2], k[1]]*
       Pair[ec[4], k[2]]) - 4*D0i[dd123, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, 
      MT2]*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       Pair[ec[4], k[1]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[2]]) + D0i[dd12, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
     Pair[e[1], k[2]]*(T*Pair[e[2], ec[4]] + 4*Pair[e[2], k[4]]*
       Pair[ec[4], k[2]]) + D0i[dd22, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
     (T*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
       Pair[ec[4], k[2]]) + 
    2*(D0i[dd00, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
       (-(Pair[e[1], ec[4]]*Pair[e[2], k[1]]) + 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]) + D0i[dd00, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
        MT2]*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - 2*Pair[e[2], k[4]]) + 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]) + D0i[dd00, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
        MT2]*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])) + 4*D0i[dd123, MH2, 0, 0, 0, U, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
       (-2*Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         (4*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))) - 
    (D0i[dd0, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
      ((U*Pair[e[1], k[2]] + S*Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
       Pair[e[1], ec[4]]*(T*Pair[e[2], k[1]] + S*Pair[e[2], k[4]]) + 
       Pair[e[1], e[2]]*(T*Pair[ec[4], k[1]] - (2*S + U)*Pair[ec[4], k[2]]) + 
       2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
           2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]))))/4 + 
    (D0i[dd3, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
       ((-MH2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*Pair[e[2], k[1]])*
       Pair[ec[4], k[1]] + D0i[dd3, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
       Pair[e[2], k[1]]*((-MH2 + T)*Pair[e[1], ec[4]] + 
        2*Pair[e[1], k[3]]*Pair[ec[4], k[1]]) + 
      D0i[dd3, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
       (Pair[e[2], k[1]]*(-(U*Pair[e[1], ec[4]]) + 2*Pair[e[1], k[3]]*
           Pair[ec[4], k[1]]) + S*Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 
          Pair[ec[4], k[3]])))/2 - C0i[cc2, 0, MH2, S, MT2, MT2, MT2]*
     (Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]) - C0i[cc2, MH2, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]) - C0i[cc1, MH2, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]) - 4*D0i[dd112, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
      MT2]*Pair[e[2], k[3]]*(Pair[e[1], k[3]]*(Pair[ec[4], k[2]] - 
        3*Pair[ec[4], k[3]]) + Pair[e[1], k[2]]*Pair[ec[4], k[3]]) + 
    4*(Pair[e[1], k[2]]*(D0i[dd112, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
         Pair[e[2], k[3]] + D0i[dd112, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
      D0i[dd111, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
       Pair[e[2], k[3]]*Pair[ec[4], k[3]]) + 
    (D0i[dd2, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
      (((T + U)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
        Pair[e[2], ec[4]] + U*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         3*Pair[e[2], k[3]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[1]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(-5*Pair[e[2], k[1]]*
            Pair[ec[4], k[1]] + Pair[e[2], k[4]]*(5*Pair[ec[4], k[1]] + 
             Pair[ec[4], k[2]]))) - Pair[e[1], e[2]]*(2*U*Pair[ec[4], k[1]] + 
         (-MH2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]])))/2 + 
    4*(D0i[dd002, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
       (-(Pair[e[1], k[3]]*Pair[e[2], ec[4]]) + Pair[e[1], ec[4]]*
         Pair[e[2], k[4]] - Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
      D0i[dd001, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])) + 
    4*((D0i[dd113, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2] + 
        D0i[dd113, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*
       Pair[e[2], k[1]]*Pair[ec[4], k[2]] + 
      D0i[dd113, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*Pair[e[1], k[3]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[3]])) + D0i[dd11, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
      MT2]*((-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 
      Pair[e[1], k[3]]*((MH2 + U)*Pair[e[2], ec[4]] - 4*Pair[e[2], k[3]]*
         (Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) + 
    (-(D0i[dd2, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
        (((-T + U)*Pair[e[1], k[2]] + T*Pair[e[1], k[3]])*Pair[e[2], ec[4]] + 
         U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
          ((-MH2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) - 
         2*(Pair[e[1], k[3]]*(Pair[e[2], k[3]] + Pair[e[2], k[4]])*
            Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[4]]*
            Pair[ec[4], k[3]]))) - D0i[dd2, 0, 0, MH2, 0, T, S, MT2, MT2, 
        MT2, MT2]*(-(T*Pair[e[1], k[3]]*Pair[e[2], ec[4]]) - 
        Pair[e[1], ec[4]]*(T*Pair[e[2], k[1]] - U*Pair[e[2], k[4]]) + 
        Pair[e[1], e[2]]*(T*Pair[ec[4], k[1]] - S*Pair[ec[4], k[2]]) + 
        2*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]])))/2 + 
    4*(D0i[dd122, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
       (2*Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
       Pair[ec[4], k[2]] + D0i[dd123, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, MT2]*
       (-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[1]]) + 
          2*Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + Pair[e[2], k[3]]*
       (D0i[dd122, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
         (-(Pair[e[1], k[3]]*Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
           (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
        D0i[dd122, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))) - 
    D0i[dd12, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
     ((2*U*Pair[e[1], k[2]] - (MH2 + 3*U)*Pair[e[1], k[4]])*
       Pair[e[2], ec[4]] + Pair[e[2], k[3]]*((S + 2*U)*Pair[e[1], ec[4]] - 
        4*(2*Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))) + 
    (D0i[dd0, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*
       (((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]]) + 2*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
         ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] - 
          S*Pair[ec[4], k[3]])) + D0i[dd0, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, 
        MT2]*(((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*((MH2 + U)*Pair[e[2], k[1]] - 
          (S + 2*U)*Pair[e[2], k[3]]) - Pair[e[1], e[2]]*
         ((MH2 + U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]]) + 
        2*(Pair[e[1], k[3]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[1]] - 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
           (-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
             Pair[ec[4], k[3]]))))/4 + 
    (-(C0i[cc0, 0, MH2, S, MT2, MT2, MT2]*
        ((2*Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
         Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 
         Pair[e[1], e[2]]*Pair[ec[4], k[1]])) + 
      C0i[cc0, 0, 0, T, MT2, MT2, MT2]*
       ((Pair[e[1], k[2]] - 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]] - Pair[e[1], e[2]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])) - 
      D0i[dd1, 0, MH2, 0, 0, U, S, MT2, MT2, MT2, MT2]*Pair[e[1], k[2]]*
       ((-MH2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
         Pair[ec[4], k[2]]) - D0i[dd1, 0, 0, MH2, 0, T, S, MT2, MT2, MT2, 
        MT2]*(-(S*Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
        Pair[e[1], k[2]]*(-(T*Pair[e[2], ec[4]]) + 2*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]])) - C0i[cc0, MH2, 0, S, MT2, MT2, MT2]*
       ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])) - 
      D0i[dd1, MH2, 0, 0, 0, U, T, MT2, MT2, MT2, MT2]*
       (-(((-MH2 + T)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
          Pair[e[2], ec[4]]) - Pair[e[1], ec[4]]*
         ((MH2 + U)*Pair[e[2], k[1]] - (2*S + 3*U)*Pair[e[2], k[3]]) + 
        Pair[e[1], e[2]]*((MH2 + U)*Pair[ec[4], k[1]] + 
          (-MH2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) - 
        2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[2]] + 
             5*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[4], k[3]]))))/2))/(MW*Pi*SW)
