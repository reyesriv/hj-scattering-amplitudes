(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas^2*col1*EL*MT^2*
    (-(((Pair[e[1], ec[5]]*((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
            4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) - 
          4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
             Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*((Pair[e[1], k[3]] + 
                2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*(
                Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
             Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
          2*Pair[e[2], ec[4]]*(-2*Pair[e[1], k[4]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[2]]*Pair[ec[5], k[4]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, 
          T24, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[1], ec[5]]*((S34*T14 - S*U)*Pair[e[2], ec[4]] + 
            2*(((S34 + U)*Pair[e[2], k[1]] - (S + T14)*Pair[e[2], k[3]])*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], 
                  k[1]] - (S + T14)*Pair[ec[4], k[3]]))) + 
          2*(((-MH^2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
              (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
             Pair[ec[4], ec[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
              (S + T14)*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
             Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[1], ec[4]]*
             Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
              (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
             Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
              (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - 
            2*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[5]])*Pair[ec[5], k[1]] - 
              Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                 Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
                 Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                   Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                    k[3]]))) + Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(
                (-MH^2 + S34 + T + T14)*Pair[ec[5], k[1]] + 
                (-MH^2 + S + T + T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*(
                (-S + T14)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + 
                S*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*((-MH^2 + S + T + U)*
                 Pair[ec[5], k[1]] + (MH^2 - S - T - T14)*Pair[ec[5], 
                  k[4]]))))*PVD[0, 0, 0, 0, 0, MH^2, T24, 0, 
          2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 
        (-((S - T14)*(-MH^2 + S + T + T14)*Pair[e[1], ec[5]]*
            Pair[e[2], ec[4]]) - 4*(MH^2 - S - T - T14)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]) + 2*(2*T14*Pair[e[1], k[2]] + 
            2*S*Pair[e[1], k[4]] + (S - T14)*Pair[e[1], k[5]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          4*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          8*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]])*
         PVD[0, 0, 0, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-((MH^2*(S - T14) - S*(S34 + T - U) + 
             T14*(-S34 + T + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 
          4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
            (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
             Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
            (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
             Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          4*Pair[e[1], ec[5]]*(((-MH^2 + T)*Pair[e[2], k[1]] + 
              (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*((-MH^2 + T)*Pair[ec[4], k[1]] + 
              (S + T14)*Pair[ec[4], k[3]])) + 
          8*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
               Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
          4*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          4*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
          2*Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(2*(MH^2 - T - U)*Pair[ec[5], 
                k[2]] + (S + T14)*Pair[ec[5], k[3]] + 2*(MH^2 - S34 - T)*Pair[
                ec[5], k[4]]) + Pair[e[1], k[2]]*(2*(-MH^2 + T + U)*Pair[
                ec[5], k[2]] + (S + T14)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + 
                T)*Pair[ec[5], k[4]]) + Pair[e[1], k[3]]*
             (2*S*Pair[ec[5], k[2]] + (-S + T14)*Pair[ec[5], k[3]] + 
              2*T14*Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, T24, 0, 
          2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        2*((Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
              4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                 Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                 k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                  k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
             Pair[ec[5], k[1]])*PVC[0, 0, 1, MH^2, T24, MH^2 - S - T - T14, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
              4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                 Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                 k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                  k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
             Pair[ec[5], k[3]])*PVC[0, 1, 0, MH^2, T24, MH^2 - S - T - T14, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
           (2*(-MH^2 + S34 + T + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
            2*(-MH^2 + S34 + T + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
            4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
             ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (S - S34 - T14 + 
                U)*Pair[ec[5], k[3]] + (-MH^2 + S34 + T + U)*Pair[ec[5], 
                k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - 
             U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]]) - 
        4*(((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
             Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
             Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
             Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 1, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          ((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
             Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
             Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
             Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 2, 0, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
               Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
             Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
             Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
             Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]))*
           Pair[ec[5], k[1]]*PVD[0, 1, 0, 1, 0, MH^2, T24, 0, 
            2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + 
          (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
               Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
             Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
             Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
             Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
           Pair[ec[5], k[3]]*PVD[0, 1, 1, 0, 0, MH^2, T24, 0, 
            2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]) + 
        8*(Pair[e[1], k[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[1]]*PVD[0, 2, 0, 1, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
           ((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]]*PVD[0, 2, 1, 0, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (Pair[e[1], ec[5]]*((S - T14)*Pair[e[2], ec[4]] - 
              4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                 Pair[ec[4], k[2]])) - ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
               Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
                Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])*
           PVD[1, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]]) - 
        8*((-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
              Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 2, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[
                4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
            4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[4]]*Pair[
                ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
             (2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[4]]))*
           PVD[0, 0, 2, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
              Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 3, 0, 0, MH^2, 
            T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
           ((-S + T14)*Pair[e[2], ec[4]] + 4*(Pair[e[2], k[4]]*Pair[ec[4], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]))*
           Pair[ec[5], k[1]]*PVD[0, 1, 0, 2, 0, MH^2, T24, 0, 
            2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + 
          (-(Pair[e[2], ec[4]]*((S - S34 - T14 + U)*Pair[e[1], k[3]]*
                Pair[ec[5], k[1]] + Pair[e[1], k[5]]*((2*S - S34 - 2*T14 + U)*
                  Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]]))) + 
            4*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[5]]*
                 Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[5]])*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                 Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*(
                2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], 
                 k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - 
             U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + (-(((S - S34 - T14 + U)*Pair[e[1], k[3]] + 
               (2*S - S34 - 2*T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]]) + 
            4*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*(
                Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 2*Pair[e[2], k[5]]*
                 Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + 
                  2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
           PVD[0, 1, 2, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
              4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                 Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                 k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                  k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
             Pair[ec[5], k[1]])*PVD[1, 0, 0, 1, 0, MH^2, T24, 0, 
            2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + 
          (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
              4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                 Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
             ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
             ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
             (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
              Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]])))*
           PVD[1, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + S34 + T14 - U)*Pair[e[2], 
                ec[4]] + 4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[1], k[5]]*
             (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
                 Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
                Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, T24, 0, 
            2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]))/T24) + 
     ((-(Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], ec[4]] + 
            2*(2*Pair[e[2], k[5]]*(-Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
              Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
              2*Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])))) + 
         4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
              Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 
               Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(3*Pair[ec[4], k[2]] - 
               Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
            (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 0, T24, MH^2, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
           2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*(3*Pair[ec[4], k[2]] - 2*
                Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
               Pair[ec[4], k[5]]))) + 
         4*(Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
             Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
           (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
              (3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*
            Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))*
        PVC[0, 0, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-(S*S34) - S34*T24 - T*T24 + 
             MH^2*(S - T14 + T24) + T14*U)*Pair[e[2], ec[4]] + 
           2*(Pair[e[2], k[3]]*(-(T24*Pair[ec[4], k[1]]) + T14*Pair[ec[4], 
                 k[2]]) + Pair[e[2], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
               (MH^2 - S34 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
             Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*
                Pair[ec[4], k[3]]))) + 
         2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*
                Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
              ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) + 
           Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + (MH^2 - U)*
              Pair[e[2], k[4]])*Pair[ec[5], k[1]] - Pair[e[1], e[2]]*
            ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
            Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
            ((-MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
             T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - MH^2*Pair[e[1], k[4]]*
              Pair[ec[5], k[1]] + U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
             MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
              Pair[ec[5], k[2]] - T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] - 
             MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + U*Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]) - 2*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
               Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*
              Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
              Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
             Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, T24, MH^2, S34, 0, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + (Pair[e[1], ec[5]]*
          ((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + S34*U + T14*U)*
            Pair[e[2], ec[4]] - 2*(Pair[e[2], k[4]]*((MH^2 - U)*Pair[ec[4], 
                 k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], 
                 k[3]]) + Pair[e[2], k[3]]*(T24*Pair[ec[4], k[1]] - 
               (S34 + T14)*Pair[ec[4], k[2]] + 2*S*Pair[ec[4], k[3]]) + 
             Pair[e[2], k[1]]*((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + 
                 T24 + 2*U)*Pair[ec[4], k[3]]))) + 
         2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*
                Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
              ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*
                Pair[ec[4], k[3]])) + Pair[e[1], ec[4]]*
            (T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], k[4]])*
            Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
            ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*
              Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
            ((MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
             T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[4]]*
              Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
             MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
              Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + 
             MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - U*Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]) + 2*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - Pair[e[1], k[3]]*
              Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
             2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 2*Pair[e[1], k[5]]*
              Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
             Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
              Pair[ec[5], k[2]] - Pair[e[1], k[5]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[5]]*
              Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*
        PVD[0, 0, 0, 0, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       ((MH^4 - 2*T*T24 + 2*T14*U + S34*(-T24 + U) - 
           MH^2*(S34 + 2*T14 - T24 + U))*Pair[e[1], ec[5]]*
          Pair[e[2], ec[4]] + 4*Pair[e[1], k[5]]*
          ((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + (S34 - U)*Pair[e[2], k[4]])*
          Pair[ec[4], ec[5]] + 4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
          Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*Pair[e[1], ec[5]]*
          (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
           2*Pair[e[2], k[3]]*((-MH^2 + S34 + T24)*Pair[ec[4], k[1]] - 
             T14*Pair[ec[4], k[2]] + (S + T14)*Pair[ec[4], k[3]]) + 
           Pair[e[2], k[4]]*(2*(S34 - U)*Pair[ec[4], k[1]] + 
             2*(-MH^2 + S34 + T)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + 2*T14 + 
               T24 + U)*Pair[ec[4], k[3]])) + 4*Pair[e[1], ec[4]]*
          ((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + (S34 - U)*Pair[e[2], k[4]])*
          Pair[ec[5], k[1]] + 4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*
          Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
         8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[1]] - Pair[e[1], k[3]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*
            Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - (Pair[e[2], k[1]] - Pair[
                e[2], k[5]])*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*
              Pair[ec[5], k[3]])) - 4*Pair[e[2], ec[4]]*
          (T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + (MH^2 - U)*
            Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            (T24*Pair[ec[5], k[3]] + (MH^2 - U)*Pair[ec[5], k[4]])))*
        PVD[0, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], ec[5]]*((MH^4 + 4*S*S34 + S34*T24 - 2*T*T24 + 3*S34*U + 
             2*T14*U - MH^2*(3*S34 + 2*T14 - T24 + U))*Pair[e[2], ec[4]] - 
           2*(2*Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + 
                 U)*Pair[ec[4], k[3]]) - 2*Pair[e[2], k[3]]*
              ((MH^2 + S34 - T24)*Pair[ec[4], k[1]] + (2*S34 + T14)*
                Pair[ec[4], k[2]] + (-9*MH^2 + 4*S34 + 5*T + 4*T14 + 4*T24 + 
                 4*U)*Pair[ec[4], k[3]]) - Pair[e[2], k[4]]*(2*(S34 + U)*
                Pair[ec[4], k[1]] + 2*(MH^2 + S34 - T)*Pair[ec[4], k[2]] + 
               (-17*MH^2 + 8*S34 + 10*T + 8*T14 + 7*T24 + 7*U)*Pair[ec[4], 
                 k[3]]))) + 4*(Pair[e[1], k[5]]*
            ((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*
                Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
              (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], 
                 k[3]])) + Pair[e[1], ec[4]]*(-((MH^2 + S34 - T24)*Pair[e[2], 
                k[3]]) - (S34 + U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
           Pair[e[1], e[2]]*(2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*
              Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
           2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*(
                Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[1]]) + 
             Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 4*
                (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[3]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + (Pair[e[2], k[1]] - 
                 Pair[e[2], k[5]])*(-(Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
                 Pair[ec[4], k[3]]*(4*Pair[ec[5], k[2]] + Pair[ec[5], 
                    k[3]])))) - Pair[e[2], ec[4]]*(2*S34*Pair[e[1], k[2]]*
              Pair[ec[5], k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
             MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
              Pair[ec[5], k[1]] + 2*S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
             T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*
              Pair[ec[5], k[4]] - U*Pair[e[1], k[5]]*Pair[ec[5], k[4]])))*
        PVD[0, 0, 0, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       2*((-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + Pair[
                e[2], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
               Pair[e[1], e[2]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
            ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
             4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
                Pair[ec[5], k[3]])))*PVC[0, 1, 0, T24, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         Pair[e[2], k[4]]*(2*(-MH^2 + S34)*Pair[e[1], k[5]]*
            Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*
            (-2*(MH^2 - S34)*Pair[ec[4], k[1]] + (-MH^2 + S34)*
              Pair[ec[4], k[2]] + (2*T14 + T24)*Pair[ec[4], k[3]]) + 
           2*(-MH^2 + S34)*Pair[e[1], ec[4]]*Pair[ec[5], k[1]] - 
           4*Pair[ec[4], k[3]]*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*Pair[ec[5], k[4]]))*PVD[0, 0, 1, 0, 0, T24, 
           MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + 
               S34*U + T14*U)*Pair[e[2], ec[4]] - Pair[e[2], k[4]]*
              (2*(MH^2 - U)*Pair[ec[4], k[1]] - 2*(MH^2 - T)*Pair[ec[4], 
                 k[2]] + (MH^2 + 2*S - U)*Pair[ec[4], k[3]]) + 
             Pair[e[2], k[3]]*(2*(MH^2 + S34 - T24)*Pair[ec[4], k[1]] + 
               (MH^2 + 3*S34 + 2*T14)*Pair[ec[4], k[2]] + 2*(-3*MH^2 - 3*S + 
                 S34 + 2*T + T14 + U)*Pair[ec[4], k[3]]) - 2*Pair[e[2], k[1]]*
              ((MH^2 + S34)*Pair[ec[4], k[2]] + (-3*MH^2 + T24 + 3*U)*
                Pair[ec[4], k[3]])) + 2*(Pair[e[1], k[5]]*
              ((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + (MH^2 - U)*
                  Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
                ((MH^2 + S34)*Pair[ec[4], k[2]] + (-3*MH^2 + T24 + 3*U)*
                  Pair[ec[4], k[3]])) + Pair[e[1], ec[4]]*
              (-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + (MH^2 - U)*
                Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
              ((MH^2 + S34)*Pair[ec[4], k[2]] + (-3*MH^2 + T24 + 3*U)*
                Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
              ((MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + T24*
                Pair[e[1], k[3]]*Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[4]]*
                Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
               MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
                Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + 
               MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - U*Pair[e[1], k[5]]*
                Pair[ec[5], k[4]]) + 2*(3*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[2]]*
                Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
               Pair[e[1], k[4]]*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] + 
                 Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[3]]*
                (-(Pair[e[2], k[4]]*Pair[ec[4], k[2]]) + 2*Pair[e[2], k[3]]*
                  Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 3*Pair[e[1], k[5]]*
                Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
                Pair[ec[5], k[2]] - Pair[e[1], k[5]]*Pair[e[2], k[4]]*
                Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[5]]*
                Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
               Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[4]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, U, 
           0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) - 
       4*((-(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + Pair[e[2], k[5]])*
             (Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) + 
           2*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
            (Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
              Pair[ec[5], k[1]]))*PVC[0, 0, 1, T24, MH^2, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (2*Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
             Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) - 
           Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
             2*Pair[e[2], k[5]]*Pair[ec[4], k[5]] - Pair[e[2], k[4]]*
              (Pair[ec[4], k[1]] + Pair[ec[4], k[5]])) + 
           2*(Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
              (Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*
          PVC[0, 1, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
            (T24*Pair[e[2], k[1]] + 2*(-MH^2 + 2*S + T24 + U)*
              Pair[e[2], k[4]] + T24*Pair[e[2], k[5]]) - 
           2*(T24*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + T24*Pair[e[1], e[2]]*
              Pair[ec[5], k[1]] + 4*Pair[e[2], k[4]]*(Pair[e[1], k[3]]*
                Pair[ec[5], k[1]] + Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
               Pair[e[1], k[5]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))*
          PVD[0, 0, 1, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (2*Pair[e[1], k[5]]*((MH^2 + 3*S34)*Pair[e[2], k[3]] + 
             (MH^2 + S34)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
           2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[3]] - Pair[e[1], ec[5]]*(2*(2*MH^2 - T24 - 2*U)*
              Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[4]]*
              ((MH^2 + S34)*Pair[ec[4], k[1]] + (-4*S + 2*S34 + 4*T - 3*T24 - 
                 2*U)*Pair[ec[4], k[3]] + (MH^2 + S34)*Pair[ec[4], k[5]]) + 
             Pair[e[2], k[3]]*((MH^2 + 3*S34)*Pair[ec[4], k[1]] + 
               (4*MH^2 - 12*S + 2*S34 + 4*T - 7*T24 - 6*U)*Pair[ec[4], 
                 k[3]] + (MH^2 + 3*S34)*Pair[ec[4], k[5]])) + 
           2*Pair[e[1], ec[4]]*((MH^2 + 3*S34)*Pair[e[2], k[3]] + 
             (MH^2 + S34)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] - 
           2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[1]] - 8*Pair[ec[4], k[3]]*
            (2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(3*Pair[e[2], k[3]] + Pair[
                e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
               Pair[e[2], k[3]]*(4*Pair[ec[5], k[3]] + 3*Pair[ec[5], 
                   k[4]]))))*PVD[0, 0, 1, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (2*(MH^2 + S34)*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
            Pair[ec[4], ec[5]] + 2*(MH^2 - U)*Pair[e[1], k[5]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
            (-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], 
                k[3]]) - Pair[e[2], k[3]]*(2*(MH^2 + S34)*Pair[ec[4], k[1]] + 
               (MH^2 + S34)*Pair[ec[4], k[2]] + (-8*MH^2 - 2*S + 3*S34 + 
                 6*T + 2*T14 + 3*U)*Pair[ec[4], k[3]])) + 
           2*(MH^2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[3]]*
            Pair[ec[5], k[1]] + 2*(MH^2 - U)*Pair[e[1], e[2]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[e[2], k[3]]*
            Pair[ec[4], k[3]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))*PVD[0, 0, 2, 0, 0, U, 
           0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       (T24*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
         4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
          Pair[ec[4], k[2]] - 2*Pair[e[1], ec[5]]*
          (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[3]] + 
           (MH^2 - T24 - U)*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
         4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
          Pair[ec[5], k[1]] - 4*T24*Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            Pair[ec[5], k[2]]) + 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, T24, MH^2, S34, 0, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] - ((MH^2 - U)*(MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
          Pair[e[2], ec[4]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*
          Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 4*(MH^2 - U)*Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            Pair[ec[5], k[2]]) - 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, U, 0, S34, MH^2, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + 4*(Pair[ec[4], k[3]]*(2*(-MH^2 + T24 + U)*
            Pair[e[1], k[5]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
            ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
             (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
            Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, T24, 
           MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + (-4*S34*Pair[e[1], k[5]]*
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*(Pair[e[2], k[5]]*
              (-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 + 8*S + 3*(T24 + U))*
                Pair[ec[4], k[3]] - 2*S34*Pair[ec[4], k[5]]) + 
             Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 - 8*S - 
                 5*(T24 + U))*Pair[ec[4], k[3]] + 2*S34*Pair[ec[4], k[5]])) - 
           4*S34*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
            Pair[ec[5], k[1]] + 2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 16*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*Pair[ec[4], k[3]]*(Pair[e[1], k[2]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*
          PVD[0, 0, 0, 2, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[2]]*
          (2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] - 
           Pair[e[1], ec[5]]*((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[
                1]] + (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 
           2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
           8*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-2*(MH^2 - T24 - U)*Pair[e[1], k[5]]*
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[2]] - Pair[e[1], ec[5]]*
            (-(Pair[e[2], k[5]]*(4*S*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                  3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*
                 Pair[ec[4], k[5]])) + Pair[e[2], k[1]]*(2*(-MH^2 + 2*S + 
                 T24 + U)*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 3*(T24 + U))*
                Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*Pair[ec[4], k[5]])) - 
           2*(MH^2 - T24 - U)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 2*(-MH^2 + T24 + U)*
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
           8*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[1]] + 
             2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*T24*Pair[e[1], k[5]]*
            Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*(T24*Pair[e[2], k[1]] + 
             2*(-MH^2 + 2*S + T24 + U)*Pair[e[2], k[4]] + 
             T24*Pair[e[2], k[5]]) + 2*T24*Pair[e[1], e[2]]*
            Pair[ec[5], k[1]] + 8*Pair[e[2], k[4]]*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
            Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + U)*
            Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
           Pair[e[1], ec[5]]*(-((MH^2 - U)*(2*Pair[e[2], k[1]] - 
                Pair[e[2], k[4]])*Pair[ec[4], k[2]]) + Pair[e[2], k[3]]*
              (2*(-MH^2 + T24 + U)*Pair[ec[4], k[1]] + (-2*MH^2 + 4*S + 
                 3*T24 + 2*U)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T24 + U)*
                Pair[ec[4], k[3]])) + 2*(-MH^2 + T24 + U)*Pair[e[1], ec[4]]*
            Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 2*(-MH^2 + U)*
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
           8*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       8*((Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
              Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - 
               Pair[ec[4], k[5]])) - Pair[e[1], ec[5]]*
            (Pair[e[2], k[1]]*Pair[ec[4], k[1]] - Pair[e[2], k[5]]*
              Pair[ec[4], k[5]]) + (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[
                ec[4], k[5]]))*Pair[ec[5], k[1]])*PVC[0, 0, 1, U, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         Pair[e[2], k[4]]*Pair[ec[4], k[2]]*((-MH^2 + 2*S + T24 + U)*
            Pair[e[1], ec[5]] - 4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, T24, 
           MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
          ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
           4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-(Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*(
                Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 2*(Pair[e[1], k[5]]*
                 Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[5], k[1]]))) + 
           Pair[e[2], ec[4]]*((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
             4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
                Pair[ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
              Pair[e[2], ec[4]] + Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] + 
               Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - 2*
                Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
              ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
               Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
             (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
               Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*
              Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*
                Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
          PVD[1, 0, 0, 0, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       8*(Pair[ec[4], k[3]]*((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
            (Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
           4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*(PVD[0, 0, 0, 3, 0, T24, MH^2, S34, 0, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + PVD[0, 0, 0, 3, 0, U, 0, S34, MH^2, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]]) + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
            ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], k[3]] + 
             (-2*S + S34 + 2*T14 - U)*Pair[e[2], k[4]]) + 
           4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[5], k[4]] + Pair[e[2], k[4]]*(Pair[ec[5], k[3]] + 
                 2*Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 2, 0, T24, MH^2, S34, 
           0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[ec[4], k[3]]*
          (-(Pair[e[1], ec[5]]*((4*S - S34 - 2*T + 2*T24 + U)*Pair[e[2], 
                k[3]] + (MH^2 + 2*S - S34 - 2*T + T24)*Pair[e[2], k[4]])) + 
           4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
               Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 2*Pair[ec[5], 
                   k[4]]))))*PVD[0, 0, 1, 2, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
          ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
           4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
            ((-7*MH^2 + 4*S34 + 6*T + 2*T14 + T24 + 3*U)*Pair[e[2], k[3]] + 
             (-2*MH^2 + S34 + 2*T + U)*Pair[e[2], k[4]]) + 
           4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                Pair[ec[5], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 
                 Pair[ec[5], k[4]]))))*PVD[0, 0, 2, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
          ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
           4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[3]]))*PVD[0, 0, 3, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] + 
             Pair[ec[4], k[3]]) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
            (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*(PVD[0, 1, 0, 2, 0, T24, MH^2, S34, 0, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, U, 0, S34, MH^2, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*
              Pair[e[2], k[3]]*Pair[ec[4], k[2]] - Pair[e[2], k[4]]*
              ((2*S - S34 - 2*T14 + U)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + 
                 T24 + U)*Pair[ec[4], k[3]])) + 
           4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]])*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
              Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
             Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, T24, 
           MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (-(Pair[e[1], ec[5]]*(Pair[e[2], k[4]]*((2*MH^2 - S34 - 2*T - U)*
                 Pair[ec[4], k[2]] + (-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                  k[3]]) + Pair[e[2], k[3]]*((MH^2 + 2*S - S34 - 2*T + T24)*
                 Pair[ec[4], k[2]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                  k[3]]))) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[3]]*
              (2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[2]]*
              Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
             2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
              Pair[ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
              Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[5]]*
              Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
             Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, U, 0, 
           S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
          ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
           4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + Pair[e[2], k[3]]*(Pair[e[1], ec[5]]*
            ((-2*MH^2 + S34 + 2*T + U)*Pair[ec[4], k[2]] + 
             (MH^2 - 2*S - T24 - U)*Pair[ec[4], k[3]]) + 
           4*(Pair[e[1], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[2]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
               Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))*PVD[0, 1, 2, 0, 0, U, 
           0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[2]]*
          ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]]) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))*(PVD[0, 2, 0, 1, 0, T24, MH^2, S34, 0, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, U, 0, S34, MH^2, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]]) + (-(Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*Pair[
                e[2], ec[4]] + 2*(Pair[e[2], k[1]]*(2*Pair[ec[4], k[1]] + 
                  Pair[ec[4], k[2]]) + Pair[e[2], k[5]]*(Pair[ec[4], k[2]] - 
                  2*Pair[ec[4], k[5]])))) + 
           4*(Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
                Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
             (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
               Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
             Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
               Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
          (PVD[1, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
           PVD[1, 0, 0, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, 
            Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
         (-2*Pair[e[2], k[4]]*(Pair[e[1], ec[5]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[ec[4], ec[5]] + 
               Pair[e[1], ec[4]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
            ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
             4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
                Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-2*MH^2 + S34 + 2*T + U)*
              Pair[e[2], ec[4]] - 2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
                Pair[ec[4], k[3]] + Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
                 Pair[ec[4], k[5]]))) + 4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
             (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
                Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
              (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
                Pair[ec[5], k[3]])))*PVD[1, 0, 1, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
              (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
             2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
                Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
            ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]] + 
             4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
                Pair[ec[5], k[2]])))*(PVD[1, 1, 0, 0, 0, T24, MH^2, S34, 0, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, U, 0, S34, MH^2, 
            MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
            Sqrt[MT^2]])))/(MH^2 - S - T - T14)))/(MW*SW)) - 
 (Alfas^2*col2*EL*MT^2*
   (-(((Pair[e[1], ec[5]]*((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
           4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) - 
         4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
            ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[3]])) + 2*Pair[e[2], ec[4]]*
          (-2*Pair[e[1], k[4]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*
            Pair[ec[5], k[4]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T24, MH^2 - S - T - T14, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], ec[5]]*((S34*T14 - S*U)*Pair[e[2], ec[4]] + 
           2*(((S34 + U)*Pair[e[2], k[1]] - (S + T14)*Pair[e[2], k[3]])*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], 
                 k[1]] - (S + T14)*Pair[ec[4], k[3]]))) + 
         2*(((-MH^2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
             (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
             (S + T14)*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
            Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - 
           2*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])*Pair[ec[5], k[1]] - 
             Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                  Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                   k[3]]))) + Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*
              ((-MH^2 + S34 + T + T14)*Pair[ec[5], k[1]] + (-MH^2 + S + T + 
                 T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*((-S + T14)*
                Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + S*Pair[ec[5], 
                 k[4]]) + Pair[e[1], k[2]]*((-MH^2 + S + T + U)*Pair[ec[5], 
                 k[1]] + (MH^2 - S - T - T14)*Pair[ec[5], k[4]]))))*
        PVD[0, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] - (-((S - T14)*(-MH^2 + S + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], ec[4]]) - 4*(MH^2 - S - T - T14)*Pair[e[1], ec[5]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]]) + 2*(2*T14*Pair[e[1], k[2]] + 
           2*S*Pair[e[1], k[4]] + (S - T14)*Pair[e[1], k[5]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], ec[4]]*
          Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 8*Pair[e[1], k[3]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]])*Pair[ec[5], k[1]])*PVD[0, 0, 0, 1, 0, MH^2, 
         T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (-((MH^2*(S - T14) - S*(S34 + T - U) + T14*(-S34 + T + U))*
           Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
         4*Pair[e[1], ec[5]]*(((-MH^2 + T)*Pair[e[2], k[1]] + 
             (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*((-MH^2 + T)*Pair[ec[4], k[1]] + 
             (S + T14)*Pair[ec[4], k[3]])) + 
         8*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]]) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
            (Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         4*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
         4*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
         2*Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(2*(MH^2 - T - U)*
              Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[3]] + 
             2*(MH^2 - S34 - T)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (2*(-MH^2 + T + U)*Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[
                3]] + 2*(-MH^2 + S34 + T)*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[3]]*(2*S*Pair[ec[5], k[2]] + (-S + T14)*
              Pair[ec[5], k[3]] + 2*T14*Pair[ec[5], k[4]])))*
        PVD[0, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + 2*((Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVC[0, 0, 1, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[3]])*PVC[0, 1, 0, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          (2*(-MH^2 + S34 + T + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
           2*(-MH^2 + S34 + T + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
            ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (S - S34 - T14 + U)*
              Pair[ec[5], k[3]] + (-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))*
          PVD[0, 1, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       4*(((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         ((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 2, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]]*PVD[0, 1, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       8*(Pair[e[1], k[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]]*PVD[0, 2, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[3]]*PVD[0, 2, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((S - T14)*Pair[e[2], ec[4]] - 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
              Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
               Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])*
          PVD[1, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       8*((-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
           4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 3, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((-S + T14)*Pair[e[2], ec[4]] + 
           4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-(Pair[e[2], ec[4]]*((S - S34 - T14 + U)*Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*((2*S - S34 - 2*T14 + U)*
                 Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]]))) + 
           4*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
              (2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[4]])))*
          PVD[0, 1, 1, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-(((S - S34 - T14 + U)*Pair[e[1], k[3]] + 
              (2*S - S34 - 2*T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]]) + 
           4*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*
              (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 2*Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + 
                 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
          PVD[0, 1, 2, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVD[1, 0, 0, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
             Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
             Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]])))*
          PVD[1, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + S34 + T14 - U)*
              Pair[e[2], ec[4]] + 4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[1], k[5]]*
            (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
                Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
               Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/T24) + 
    ((-(Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], ec[4]] + 
           2*(2*Pair[e[2], k[5]]*(-Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
             2*Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])))) + 
        4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, T24, MH^2, MH^2 - S - T - T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
          2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*(3*Pair[ec[4], k[2]] - 
              2*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]))) + 
        4*(Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
          (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
             (3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-(S*S34) - S34*T24 - T*T24 + 
            MH^2*(S - T14 + T24) + T14*U)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]]*(-(T24*Pair[ec[4], k[1]]) + T14*Pair[ec[4], 
                k[2]]) + Pair[e[2], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
              (MH^2 - S34 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + 
              T24*Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) + 
          Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + (MH^2 - U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] - Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
           ((-MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
             Pair[ec[5], k[2]] - T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + U*Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]) - 2*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + 
            S34*U + T14*U)*Pair[e[2], ec[4]] - 
          2*(Pair[e[2], k[4]]*((MH^2 - U)*Pair[ec[4], k[1]] + 
              (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[3]]*(T24*Pair[ec[4], k[1]] - (S34 + T14)*Pair[ec[4], 
                k[2]] + 2*S*Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]])) + Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + 
            (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
            (-2*MH^2 + T24 + 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
            MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
            S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
          2*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] - Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + ((MH^4 - 2*T*T24 + 2*T14*U + S34*(-T24 + U) - 
          MH^2*(S34 + 2*T14 - T24 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*Pair[e[1], k[5]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[3]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
          2*Pair[e[2], k[3]]*((-MH^2 + S34 + T24)*Pair[ec[4], k[1]] - 
            T14*Pair[ec[4], k[2]] + (S + T14)*Pair[ec[4], k[3]]) + 
          Pair[e[2], k[4]]*(2*(S34 - U)*Pair[ec[4], k[1]] + 
            2*(-MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
            (-MH^2 + 2*S + 2*T14 + T24 + U)*Pair[ec[4], k[3]])) + 
        4*Pair[e[1], ec[4]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[1]] + 8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - Pair[e[1], k[3]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])*Pair[ec[5], k[3]])) - 4*Pair[e[2], ec[4]]*
         (T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + (MH^2 - U)*
           Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (T24*Pair[ec[5], k[3]] + (MH^2 - U)*Pair[ec[5], k[4]])))*
       PVD[0, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((MH^4 + 4*S*S34 + S34*T24 - 2*T*T24 + 3*S34*U + 
            2*T14*U - MH^2*(3*S34 + 2*T14 - T24 + U))*Pair[e[2], ec[4]] - 
          2*(2*Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + 
                U)*Pair[ec[4], k[3]]) - 2*Pair[e[2], k[3]]*
             ((MH^2 + S34 - T24)*Pair[ec[4], k[1]] + (2*S34 + T14)*Pair[
                ec[4], k[2]] + (-9*MH^2 + 4*S34 + 5*T + 4*T14 + 4*T24 + 4*U)*
               Pair[ec[4], k[3]]) - Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], 
                k[1]] + 2*(MH^2 + S34 - T)*Pair[ec[4], k[2]] + 
              (-17*MH^2 + 8*S34 + 10*T + 8*T14 + 7*T24 + 7*U)*Pair[ec[4], 
                k[3]]))) + 4*(Pair[e[1], k[5]]*
           ((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], 
                k[3]])) + Pair[e[1], ec[4]]*
           (-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + 2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[
                e[2], k[5]])*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*
              Pair[ec[5], k[1]]) + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + 
              (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(-(Pair[ec[4], k[2]]*
                  Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                 (4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))) - 
          Pair[e[2], ec[4]]*(2*S34*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
            2*S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 1, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 
      2*((-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + 
              Pair[e[2], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVC[0, 1, 0, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         (2*(-MH^2 + S34)*Pair[e[1], k[5]]*Pair[ec[4], ec[5]] - 
          Pair[e[1], ec[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
            (-MH^2 + S34)*Pair[ec[4], k[2]] + (2*T14 + T24)*
             Pair[ec[4], k[3]]) + 2*(-MH^2 + S34)*Pair[e[1], ec[4]]*
           Pair[ec[5], k[1]] - 4*Pair[ec[4], k[3]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*
           ((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + S34*U + T14*U)*
             Pair[e[2], ec[4]] - Pair[e[2], k[4]]*(2*(MH^2 - U)*Pair[ec[4], 
                k[1]] - 2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - U)*
               Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(2*(MH^2 + S34 - T24)*
               Pair[ec[4], k[1]] + (MH^2 + 3*S34 + 2*T14)*Pair[ec[4], k[2]] + 
              2*(-3*MH^2 - 3*S + S34 + 2*T + T14 + U)*Pair[ec[4], k[3]]) - 
            2*Pair[e[2], k[1]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
          2*(Pair[e[1], k[5]]*((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
                (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
                (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
            Pair[e[1], ec[4]]*(-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
              (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
            Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
            Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], 
                k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
              MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
               Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
              S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
               Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
              U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
            2*(3*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*(
                Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              Pair[e[1], k[3]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[2]]) + 
                2*Pair[e[2], k[3]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              3*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[2]] - Pair[e[1], k[5]]*Pair[e[2], k[4]]*
               Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[5]]*Pair[
                e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
              Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], 
                k[4]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
               Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      4*((-(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + Pair[e[2], k[5]])*
            (Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[1]]))*PVC[0, 0, 1, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) - 
          Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
            2*Pair[e[2], k[5]]*Pair[ec[4], k[5]] - Pair[e[2], k[4]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[5]])) + 
          2*(Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
             (Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*
         PVC[0, 1, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           (T24*Pair[e[2], k[1]] + 2*(-MH^2 + 2*S + T24 + U)*
             Pair[e[2], k[4]] + T24*Pair[e[2], k[5]]) - 
          2*(T24*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + T24*Pair[e[1], e[2]]*
             Pair[ec[5], k[1]] + 4*Pair[e[2], k[4]]*(Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*((MH^2 + 3*S34)*Pair[e[2], k[3]] + 
            (MH^2 + S34)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
          2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] - Pair[e[1], ec[5]]*(2*(2*MH^2 - T24 - 2*U)*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[4]]*
             ((MH^2 + S34)*Pair[ec[4], k[1]] + (-4*S + 2*S34 + 4*T - 3*T24 - 
                2*U)*Pair[ec[4], k[3]] + (MH^2 + S34)*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[3]]*((MH^2 + 3*S34)*Pair[ec[4], k[1]] + 
              (4*MH^2 - 12*S + 2*S34 + 4*T - 7*T24 - 6*U)*Pair[ec[4], k[3]] + 
              (MH^2 + 3*S34)*Pair[ec[4], k[5]])) + 2*Pair[e[1], ec[4]]*
           ((MH^2 + 3*S34)*Pair[e[2], k[3]] + (MH^2 + S34)*Pair[e[2], k[4]])*
           Pair[ec[5], k[1]] - 2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[ec[4], k[3]]*
           (2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(3*Pair[e[2], k[3]] + 
              Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*(MH^2 + S34)*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
           Pair[ec[4], ec[5]] + 2*(MH^2 - U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
              Pair[ec[4], k[3]]) - Pair[e[2], k[3]]*(2*(MH^2 + S34)*Pair[
                ec[4], k[1]] + (MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-8*MH^2 - 2*S + 3*S34 + 6*T + 2*T14 + 3*U)*Pair[ec[4], 
                k[3]])) + 2*(MH^2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[3]]*
           Pair[ec[5], k[1]] + 2*(MH^2 - U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))*PVD[0, 0, 2, 0, 0, U, 
          0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (T24*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[3]] + 
          (MH^2 - T24 - U)*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[1]] - 4*T24*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) + 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((MH^2 - U)*(MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
         Pair[e[2], ec[4]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 4*(MH^2 - U)*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) - 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, U, 0, S34, MH^2, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[ec[4], k[3]]*(2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-4*S34*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (Pair[e[2], k[5]]*(-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 + 8*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] - 2*S34*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 - 8*S - 
                5*(T24 + U))*Pair[ec[4], k[3]] + 2*S34*Pair[ec[4], k[5]])) - 
          4*S34*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[5], k[1]] + 2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          16*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[3]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*(-MH^2 + T24 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-2*(MH^2 - T24 - U)*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] - Pair[e[1], ec[5]]*
           (-(Pair[e[2], k[5]]*(4*S*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                 3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*
                Pair[ec[4], k[5]])) + Pair[e[2], k[1]]*
             (2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*Pair[
                ec[4], k[5]])) - 2*(MH^2 - T24 - U)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*T24*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*(T24*Pair[e[2], k[1]] + 
            2*(-MH^2 + 2*S + T24 + U)*Pair[e[2], k[4]] + 
            T24*Pair[e[2], k[5]]) + 2*T24*Pair[e[1], e[2]]*
           Pair[ec[5], k[1]] + 8*Pair[e[2], k[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          Pair[e[1], ec[5]]*(-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], 
                k[4]])*Pair[ec[4], k[2]]) + Pair[e[2], k[3]]*
             (2*(-MH^2 + T24 + U)*Pair[ec[4], k[1]] + (-2*MH^2 + 4*S + 
                3*T24 + 2*U)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T24 + U)*Pair[
                ec[4], k[3]])) + 2*(-MH^2 + T24 + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 2*(-MH^2 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          8*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      8*((Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]])) - Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]]*Pair[ec[4], k[1]] - Pair[e[2], k[5]]*
             Pair[ec[4], k[5]]) + (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*PVC[0, 0, 1, U, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[2]]*((-MH^2 + 2*S + T24 + U)*
           Pair[e[1], ec[5]] - 4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
          4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-(Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
              (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
             2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
                Pair[ec[5], k[1]]))) + Pair[e[2], ec[4]]*
           ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
             Pair[e[2], ec[4]] + Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
             ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*
             Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         PVD[1, 0, 0, 0, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*(Pair[ec[4], k[3]]*((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 0, 3, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 0, 0, 3, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], k[3]] + 
            (-2*S + S34 + 2*T14 - U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[
                ec[5], k[4]] + Pair[e[2], k[4]]*(Pair[ec[5], k[3]] + 
                2*Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 2, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (-(Pair[e[1], ec[5]]*((4*S - S34 - 2*T + 2*T24 + U)*Pair[e[2], k[
                3]] + (MH^2 + 2*S - S34 - 2*T + T24)*Pair[e[2], k[4]])) + 
          4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 2, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-7*MH^2 + 4*S34 + 6*T + 2*T14 + T24 + 3*U)*Pair[e[2], k[3]] + 
            (-2*MH^2 + S34 + 2*T + U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 
                Pair[ec[5], k[4]]))))*PVD[0, 0, 2, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[3]]))*PVD[0, 0, 3, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[3]]) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 1, 0, 2, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]] - Pair[e[2], k[4]]*
             ((2*S - S34 - 2*T14 + U)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + 
                T24 + U)*Pair[ec[4], k[3]])) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        (-(Pair[e[1], ec[5]]*(Pair[e[2], k[4]]*((2*MH^2 - S34 - 2*T - U)*
                Pair[ec[4], k[2]] + (-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]) + Pair[e[2], k[3]]*((MH^2 + 2*S - S34 - 2*T + T24)*
                Pair[ec[4], k[2]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]))) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[3]]*
             (2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[2]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[5]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
         ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*(Pair[e[1], ec[5]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[ec[4], k[2]] + 
            (MH^2 - 2*S - T24 - U)*Pair[ec[4], k[3]]) + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))*PVD[0, 1, 2, 0, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*((MH^2 - 2*S - T24 - U)*
           Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 2, 0, 1, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (-(Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
              Pair[e[2], ec[4]] + 2*(Pair[e[2], k[1]]*(2*Pair[ec[4], k[1]] + 
                 Pair[ec[4], k[2]]) + Pair[e[2], k[5]]*(Pair[ec[4], k[2]] - 
                 2*Pair[ec[4], k[5]])))) + 
          4*(Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
            Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         (PVD[1, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 0, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*Pair[e[2], k[4]]*(Pair[e[1], ec[5]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[ec[4], ec[5]] + 
              Pair[e[1], ec[4]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-2*MH^2 + S34 + 2*T + U)*
             Pair[e[2], ec[4]] - 2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
               Pair[ec[4], k[3]] + Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
                Pair[ec[4], k[5]]))) + 
          4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
             (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVD[1, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
            2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*(PVD[1, 1, 0, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]])))/(MH^2 - S - T - T14)))/(MW*SW) + 
 (col5*(-((Alfas^2*EL*MT^2*(4*Pair[e[2], k[5]]*
         ((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[ec[4], k[1]]) - 2*Pair[e[2], ec[5]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[4], k[1]] + (2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*
           (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
        4*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]])/(MW*SW)) + 
    (Alfas^2*EL*MT^2*(2*Pair[e[2], ec[5]]*(-2*Pair[e[1], k[5]]*
          Pair[ec[4], k[2]] + 2*Pair[e[1], k[2]]*Pair[ec[4], k[5]] + 
         Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        (-((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[2], ec[5]]) + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(Pair[ec[4], k[1]] - 
         Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
        (2*Pair[e[1], k[2]]*Pair[ec[4], k[2]] - 2*Pair[e[1], k[5]]*
          Pair[ec[4], k[5]] - Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
         Pair[ec[4], k[3]])*Pair[ec[5], k[2]])*PVC[0, 0, 1, 0, 
       MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[1]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*
      (2*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
        (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 2*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]])*PVC[0, 1, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[3]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], ec[4]]*
        ((S*S34 + S34*T14 - MH^2*(S + T14 - T24) - T*T24 + T14*U)*
          Pair[e[2], ec[5]] - 2*(-(((MH^2 - S34)*Pair[e[2], k[1]] + 
              T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
             Pair[ec[5], k[2]]) - Pair[e[2], k[5]]*
            ((MH^2 - S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) - 
       2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
          (Pair[e[1], k[3]]*(T24*Pair[ec[4], k[1]] - T14*Pair[ec[4], k[2]]) + 
           Pair[e[1], k[4]]*((MH^2 - S34 - U)*Pair[ec[4], k[1]] + 
             (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
             T14*Pair[ec[4], k[3]])) + (T14*Pair[e[1], k[3]] + 
           (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
         Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(-(Pair[e[1], k[4]]*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
                Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + Pair[e[1], k[3]]*
            Pair[ec[4], k[1]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*Pair[ec[5], k[4]]))))*PVD[0, 0, 0, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], ec[4]]*
        ((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + T*T24 - T14*U)*
          Pair[e[2], ec[5]] - 2*(((MH^2 + S34)*Pair[e[2], k[1]] + 
             T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            ((MH^2 + S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) + 
       2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
          (Pair[e[1], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[3]]*(-((S34 + T24)*Pair[ec[4], k[1]]) + 
             T14*Pair[ec[4], k[2]] + 2*S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]])) + 
         (T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
          ((MH^2 + S34)*Pair[ec[4], k[1]] + (-2*MH^2 + 2*T + T14)*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
               Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
            ((Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 2*Pair[e[2], k[1]]*
                Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
              (2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(T14*(-MH^2 + 2*S + T + T14)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] - 2*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]] + 
         2*S*Pair[e[1], k[3]] + (MH^2 - T - T14)*Pair[e[1], k[4]])*
        Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(-MH^2 + T + T14)*
        Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
       4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 4*T14*Pair[e[1], ec[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((MH^2 - T)*(MH^2 - 2*S - T - T14)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(MH^2 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]) - 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((-MH^4 + S34*(-T + T14) - 2*T*T24 + 
         MH^2*(S34 + T - T14 + 2*T24) + 2*T14*U)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
         (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[3]] + 2*Pair[e[2], ec[5]]*
        (2*(-MH^2 + T + T14)*Pair[e[1], k[2]]*Pair[ec[4], k[3]] + 
         2*Pair[e[1], k[3]]*(-(T24*Pair[ec[4], k[1]]) + (-MH^2 + S34 + T14)*
            Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], k[4]]*(2*(-MH^2 + S34 + U)*Pair[ec[4], k[1]] + 
           2*(S34 - T)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + T + T14 + 2*T24)*
            Pair[ec[4], k[3]])) - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
         (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
        Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]])) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
          (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])) + 4*Pair[e[1], ec[4]]*
        (T14*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + (MH^2 - T)*Pair[e[2], k[4]]*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(T14*Pair[ec[5], k[3]] + 
           (MH^2 - T)*Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(2*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*(4*S34*Pair[ec[4], k[1]] + 6*(-MH^2 + T + T14)*
            Pair[ec[4], k[3]]) - 2*Pair[e[1], k[3]]*
          ((2*S34 + T24)*Pair[ec[4], k[1]] + (MH^2 + S34 - T14)*
            Pair[ec[4], k[2]] + (-9*MH^2 + 4*S34 + 4*T + 4*T14 + 4*T24 + 5*U)*
            Pair[ec[4], k[3]]) - Pair[e[1], k[4]]*
          (2*(MH^2 + S34 - U)*Pair[ec[4], k[1]] + 2*(S34 + T)*
            Pair[ec[4], k[2]] + (-17*MH^2 + 8*S34 + 7*T + 7*T14 + 8*T24 + 
             10*U)*Pair[ec[4], k[3]])) - 
       4*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
            Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(2*S34*Pair[ec[4], k[1]] + 
           3*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) + 
         (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
         Pair[e[1], e[2]]*(2*S34*Pair[ec[4], k[1]] + 3*(-MH^2 + T + T14)*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            ((4*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
                (-Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(-(Pair[ec[4], k[1]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*(4*Pair[ec[5], k[1]] + Pair[ec[5], 
                  k[3]]))))) + Pair[e[1], ec[4]]*
        (-((MH^4 + 4*S*S34 + 3*S34*T + S34*T14 + 2*T*T24 - 
            MH^2*(3*S34 + T - T14 + 2*T24) - 2*T14*U)*Pair[e[2], ec[5]]) + 
         4*((2*S34*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(2*S34*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*
      (-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[2]] + 
          (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[5]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*
        (-(Pair[e[1], k[5]]*(4*S*Pair[ec[4], k[1]] + 
            (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 
            2*(-MH^2 + T + T14)*Pair[ec[4], k[5]])) + Pair[e[1], k[2]]*
          (2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 
           2*(-MH^2 + T + T14)*Pair[ec[4], k[5]])) - 2*(MH^2 - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*(2*Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*((MH^2 - 2*S - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 1, 2, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 1, 2, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[2]] + 
          (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*
        Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
       Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(2*S34*Pair[ec[4], k[1]] + 
           (5*MH^2 + 3*S - 7*S34 - 5*T24 - 5*U)*Pair[ec[4], k[3]] - 
           4*S34*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*
          (-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 - 5*S + 5*S34 + 3*T24 + 3*U)*
            Pair[ec[4], k[3]] + 4*S34*Pair[ec[4], k[5]])) - 
       4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
        Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
       16*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[5]])*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[3]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 2, 1, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 2, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*((MH^2 - 2*S - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 3, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 3, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*(2*(MH^2 - S34)*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
        ((-MH^2 + S34)*Pair[ec[4], k[1]] - 2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
         (T14 + 2*T24)*Pair[ec[4], k[3]]) + 2*(MH^2 - S34)*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
        (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[4]]*(-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - T)*
            Pair[ec[4], k[3]]) + 2*Pair[e[1], k[2]]*
          ((MH^2 + S34)*Pair[ec[4], k[1]] + (-3*MH^2 + 3*T + T14)*
            Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
          ((MH^2 + 3*S34 + 2*T24)*Pair[ec[4], k[1]] + 2*(MH^2 + S34 - T14)*
            Pair[ec[4], k[2]] + 2*(-3*MH^2 - 3*S + S34 + T + T24 + 2*U)*
            Pair[ec[4], k[3]])) + Pair[e[1], ec[4]]*
        (-((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + T*T24 - T14*U)*
           Pair[e[2], ec[5]]) + 2*(((MH^2 + S34)*Pair[e[2], k[1]] + 
             T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            ((MH^2 + S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) - 
       2*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*
            Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]]) + 
         (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
         Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
               Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
            (((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], k[3]] + 
               Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[4], k[1]]*
                Pair[ec[5], k[4]] + Pair[ec[4], k[3]]*(3*Pair[ec[5], k[1]] + 
                 2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))*
      PVD[0, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*
      (-((T14*Pair[e[1], k[2]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
          T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] + 2*(-MH^2 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*(2*(-MH^2 + T)*Pair[e[1], k[2]]*
          Pair[ec[4], k[1]] + (MH^2 - T)*Pair[e[1], k[4]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[3]]*((-2*MH^2 + 4*S + 2*T + 3*T14)*Pair[ec[4], k[1]] + 
           2*(-MH^2 + T + T14)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T + T14)*
            Pair[ec[4], k[3]])) + 2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 2*(-MH^2 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 8*Pair[e[1], k[3]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*Pair[ec[4], k[1]]*
      ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
       4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
      ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
       4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (-((T14*Pair[e[1], k[2]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
          T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
           Pair[ec[5], k[4]])))*PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*(2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
         (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*(-2*MH^2 + 2*T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[3]] + Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + 2*T + T14)*Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], k[4]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (7*MH^2 - 7*S34 - 2*T - T14 - 4*T24 - 8*U)*Pair[ec[4], k[3]] - 
           2*(MH^2 + S34)*Pair[ec[4], k[5]]) + Pair[e[1], k[3]]*
          ((MH^2 + 3*S34)*Pair[ec[4], k[1]] + (19*MH^2 - 17*S34 - 6*T - 
             5*T14 - 12*T24 - 16*U)*Pair[ec[4], k[3]] - 2*(MH^2 + 3*S34)*
            Pair[ec[4], k[5]])) + 2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
         (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       2*(-2*MH^2 + 2*T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
        Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
        (Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
         Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           3*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
      PVD[0, 1, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*((-MH^2 + S34 + T14 + 2*T24)*
          Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
          ((2*S - S34 + T - 2*T24)*Pair[ec[4], k[1]] + 
           (-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
       4*(Pair[e[1], k[4]]*(2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - 
           Pair[ec[4], k[5]])*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
           Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[1]]*
          (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[4]]*((-2*MH^2 + S34 + T + 2*U)*Pair[ec[4], k[1]] + 
           (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
          ((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[ec[4], k[1]] + 
           2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
          (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
              Pair[ec[5], k[3]]))))*PVD[0, 1, 1, 1, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]] + 
         (-2*S + S34 - T + 2*T24)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[2], k[4]]*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
             2*Pair[ec[5], k[4]]))))*PVD[0, 1, 2, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-4*S + S34 - T - 2*T14 + 2*U)*Pair[e[1], k[3]] - 
         (MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
         Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           2*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
      PVD[0, 1, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-(Pair[e[2], ec[5]]*
         (-((MH^2 - T)*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + 2*(MH^2 + S34)*
             Pair[ec[4], k[2]] + (-8*MH^2 - 2*S + 3*S34 + 3*T + 2*T24 + 6*U)*
             Pair[ec[4], k[3]]))) - 2*((MH^2 - T)*Pair[ec[4], k[3]]*
          (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*
          ((MH^2 + S34)*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
           (MH^2 + S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
           4*Pair[ec[4], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[3]])))))*PVD[0, 2, 0, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*
      Pair[ec[4], k[1]]*((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (Pair[e[2], ec[5]]*((-2*MH^2 + S34 + T + 2*U)*Pair[ec[4], k[1]] + 
         (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) + 
       4*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
            Pair[ec[5], k[3]])))*PVD[0, 2, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]*
      ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-7*MH^2 + 4*S34 + 3*T + T14 + 2*T24 + 6*U)*Pair[e[1], k[3]] + 
         (-2*MH^2 + S34 + T + 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
          (3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(3*Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]]))))*PVD[0, 2, 1, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[ec[4], k[3]]*((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVD[0, 3, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-(Pair[ec[4], k[1]]*
         ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
          2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]))) + Pair[e[1], ec[4]]*
        ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[5]]*(2*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
         Pair[e[1], k[2]]*(Pair[ec[4], k[3]] - 2*Pair[ec[4], k[5]])) + 
       2*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*PVD[1, 0, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[1]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], k[5]]*
        ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[ec[4], k[3]]) - 2*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[5]])) + 
       4*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
         Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
       Pair[e[1], ec[4]]*((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*(PVD[1, 0, 1, 0, 0, MH^2, 
        MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*Pair[e[1], k[4]]*
        (Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) - 
         2*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], ec[5]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[ec[4], k[3]] + 
         Pair[e[1], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
       4*Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
         Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + 4*Pair[e[1], k[3]]*
        (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVD[1, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/
  (MH^2 - S - T24 - U) + 
 (col6*(-((Alfas^2*EL*MT^2*(4*Pair[e[2], k[5]]*
         ((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[ec[4], k[1]]) - 2*Pair[e[2], ec[5]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[4], k[1]] + (2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*
           (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
        4*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]])/(MW*SW)) + 
    (Alfas^2*EL*MT^2*(2*Pair[e[2], ec[5]]*(-2*Pair[e[1], k[5]]*
          Pair[ec[4], k[2]] + 2*Pair[e[1], k[2]]*Pair[ec[4], k[5]] + 
         Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        (-((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[2], ec[5]]) + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(Pair[ec[4], k[1]] - 
         Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
        (2*Pair[e[1], k[2]]*Pair[ec[4], k[2]] - 2*Pair[e[1], k[5]]*
          Pair[ec[4], k[5]] - Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
         Pair[ec[4], k[3]])*Pair[ec[5], k[2]])*PVC[0, 0, 1, 0, 
       MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[1]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*
      (2*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
        (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 2*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]])*PVC[0, 1, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[3]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], ec[4]]*
        ((S*S34 + S34*T14 - MH^2*(S + T14 - T24) - T*T24 + T14*U)*
          Pair[e[2], ec[5]] - 2*(-(((MH^2 - S34)*Pair[e[2], k[1]] + 
              T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
             Pair[ec[5], k[2]]) - Pair[e[2], k[5]]*
            ((MH^2 - S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) - 
       2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
          (Pair[e[1], k[3]]*(T24*Pair[ec[4], k[1]] - T14*Pair[ec[4], k[2]]) + 
           Pair[e[1], k[4]]*((MH^2 - S34 - U)*Pair[ec[4], k[1]] + 
             (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
             T14*Pair[ec[4], k[3]])) + (T14*Pair[e[1], k[3]] + 
           (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
         Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(-(Pair[e[1], k[4]]*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
                Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + Pair[e[1], k[3]]*
            Pair[ec[4], k[1]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*Pair[ec[5], k[4]]))))*PVD[0, 0, 0, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], ec[4]]*
        ((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + T*T24 - T14*U)*
          Pair[e[2], ec[5]] - 2*(((MH^2 + S34)*Pair[e[2], k[1]] + 
             T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            ((MH^2 + S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) + 
       2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
          (Pair[e[1], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[3]]*(-((S34 + T24)*Pair[ec[4], k[1]]) + 
             T14*Pair[ec[4], k[2]] + 2*S*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]])) + 
         (T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
          ((MH^2 + S34)*Pair[ec[4], k[1]] + (-2*MH^2 + 2*T + T14)*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
               Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
            ((Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 2*Pair[e[2], k[1]]*
                Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
              (2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(T14*(-MH^2 + 2*S + T + T14)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] - 2*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]] + 
         2*S*Pair[e[1], k[3]] + (MH^2 - T - T14)*Pair[e[1], k[4]])*
        Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(-MH^2 + T + T14)*
        Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
       4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 4*T14*Pair[e[1], ec[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((MH^2 - T)*(MH^2 - 2*S - T - T14)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(MH^2 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]) - 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((-MH^4 + S34*(-T + T14) - 2*T*T24 + 
         MH^2*(S34 + T - T14 + 2*T24) + 2*T14*U)*Pair[e[1], ec[4]]*
        Pair[e[2], ec[5]] - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
         (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[3]] + 2*Pair[e[2], ec[5]]*
        (2*(-MH^2 + T + T14)*Pair[e[1], k[2]]*Pair[ec[4], k[3]] + 
         2*Pair[e[1], k[3]]*(-(T24*Pair[ec[4], k[1]]) + (-MH^2 + S34 + T14)*
            Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], k[4]]*(2*(-MH^2 + S34 + U)*Pair[ec[4], k[1]] + 
           2*(S34 - T)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + T + T14 + 2*T24)*
            Pair[ec[4], k[3]])) - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
         (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
        Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]])) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          (Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
          (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])) + 4*Pair[e[1], ec[4]]*
        (T14*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + (MH^2 - T)*Pair[e[2], k[4]]*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(T14*Pair[ec[5], k[3]] + 
           (MH^2 - T)*Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(2*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*(4*S34*Pair[ec[4], k[1]] + 6*(-MH^2 + T + T14)*
            Pair[ec[4], k[3]]) - 2*Pair[e[1], k[3]]*
          ((2*S34 + T24)*Pair[ec[4], k[1]] + (MH^2 + S34 - T14)*
            Pair[ec[4], k[2]] + (-9*MH^2 + 4*S34 + 4*T + 4*T14 + 4*T24 + 5*U)*
            Pair[ec[4], k[3]]) - Pair[e[1], k[4]]*
          (2*(MH^2 + S34 - U)*Pair[ec[4], k[1]] + 2*(S34 + T)*
            Pair[ec[4], k[2]] + (-17*MH^2 + 8*S34 + 7*T + 7*T14 + 8*T24 + 
             10*U)*Pair[ec[4], k[3]])) - 
       4*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
            Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(2*S34*Pair[ec[4], k[1]] + 
           3*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) + 
         (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
         Pair[e[1], e[2]]*(2*S34*Pair[ec[4], k[1]] + 3*(-MH^2 + T + T14)*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[e[2], k[5]]*
              Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
            ((4*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
                (-Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(-(Pair[ec[4], k[1]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*(4*Pair[ec[5], k[1]] + Pair[ec[5], 
                  k[3]]))))) + Pair[e[1], ec[4]]*
        (-((MH^4 + 4*S*S34 + 3*S34*T + S34*T14 + 2*T*T24 - 
            MH^2*(3*S34 + T - T14 + 2*T24) - 2*T14*U)*Pair[e[2], ec[5]]) + 
         4*((2*S34*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(2*S34*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*
      (-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[2]] + 
          (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[5]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*
        (-(Pair[e[1], k[5]]*(4*S*Pair[ec[4], k[1]] + 
            (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 
            2*(-MH^2 + T + T14)*Pair[ec[4], k[5]])) + Pair[e[1], k[2]]*
          (2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 
           2*(-MH^2 + T + T14)*Pair[ec[4], k[5]])) - 2*(MH^2 - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*(2*Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*((MH^2 - 2*S - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 1, 2, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 1, 2, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[2]] + 
          (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
       2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
       8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*
        Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
       Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(2*S34*Pair[ec[4], k[1]] + 
           (5*MH^2 + 3*S - 7*S34 - 5*T24 - 5*U)*Pair[ec[4], k[3]] - 
           4*S34*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*
          (-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 - 5*S + 5*S34 + 3*T24 + 3*U)*
            Pair[ec[4], k[3]] + 4*S34*Pair[ec[4], k[5]])) - 
       4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
        Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
       16*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[5]])*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[3]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 2, 1, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 2, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*((MH^2 - 2*S - T - T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*(PVD[0, 0, 3, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 3, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*(2*(MH^2 - S34)*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
        ((-MH^2 + S34)*Pair[ec[4], k[1]] - 2*(MH^2 - S34)*Pair[ec[4], k[2]] + 
         (T14 + 2*T24)*Pair[ec[4], k[3]]) + 2*(MH^2 - S34)*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
        (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[4]]*(-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - T)*
            Pair[ec[4], k[3]]) + 2*Pair[e[1], k[2]]*
          ((MH^2 + S34)*Pair[ec[4], k[1]] + (-3*MH^2 + 3*T + T14)*
            Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
          ((MH^2 + 3*S34 + 2*T24)*Pair[ec[4], k[1]] + 2*(MH^2 + S34 - T14)*
            Pair[ec[4], k[2]] + 2*(-3*MH^2 - 3*S + S34 + T + T24 + 2*U)*
            Pair[ec[4], k[3]])) + Pair[e[1], ec[4]]*
        (-((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + T*T24 - T14*U)*
           Pair[e[2], ec[5]]) + 2*(((MH^2 + S34)*Pair[e[2], k[1]] + 
             T14*Pair[e[2], k[3]] + (MH^2 - T)*Pair[e[2], k[4]])*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            ((MH^2 + S34)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[3]] + 
             (MH^2 - T)*Pair[ec[5], k[4]]))) - 
       2*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*
            Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]]) + 
         (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*
            Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
         Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
             Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
               Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
            (((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], k[3]] + 
               Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[4], k[1]]*
                Pair[ec[5], k[4]] + Pair[ec[4], k[3]]*(3*Pair[ec[5], k[1]] + 
                 2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))*
      PVD[0, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[1]]*
      (-((T14*Pair[e[1], k[2]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
          T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] + 2*(-MH^2 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*(2*(-MH^2 + T)*Pair[e[1], k[2]]*
          Pair[ec[4], k[1]] + (MH^2 - T)*Pair[e[1], k[4]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[3]]*((-2*MH^2 + 4*S + 2*T + 3*T14)*Pair[ec[4], k[1]] + 
           2*(-MH^2 + T + T14)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T + T14)*
            Pair[ec[4], k[3]])) + 2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 2*(-MH^2 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 8*Pair[e[1], k[3]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
        (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*Pair[ec[4], k[1]]*
      ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
       4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
      ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
       4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
          Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (-((T14*Pair[e[1], k[2]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
          T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
       2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
          Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
           Pair[ec[5], k[4]])))*PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*(2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
         (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*(-2*MH^2 + 2*T + T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[3]] + Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + 2*T + T14)*Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], k[4]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
           (7*MH^2 - 7*S34 - 2*T - T14 - 4*T24 - 8*U)*Pair[ec[4], k[3]] - 
           2*(MH^2 + S34)*Pair[ec[4], k[5]]) + Pair[e[1], k[3]]*
          ((MH^2 + 3*S34)*Pair[ec[4], k[1]] + (19*MH^2 - 17*S34 - 6*T - 
             5*T14 - 12*T24 - 16*U)*Pair[ec[4], k[3]] - 2*(MH^2 + 3*S34)*
            Pair[ec[4], k[5]])) + 2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
         (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       2*(-2*MH^2 + 2*T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
        Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
        (Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
         Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           3*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
      PVD[0, 1, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*((-MH^2 + S34 + T14 + 2*T24)*
          Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
          ((2*S - S34 + T - 2*T24)*Pair[ec[4], k[1]] + 
           (-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
       4*(Pair[e[1], k[4]]*(2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - 
           Pair[ec[4], k[5]])*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
           Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[1]]*
          (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[4]]*((-2*MH^2 + S34 + T + 2*U)*Pair[ec[4], k[1]] + 
           (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
          ((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[ec[4], k[1]] + 
           2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
          (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
              Pair[ec[5], k[3]]))))*PVD[0, 1, 1, 1, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]] + 
         (-2*S + S34 - T + 2*T24)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[2], k[4]]*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
             2*Pair[ec[5], k[4]]))))*PVD[0, 1, 2, 0, 0, MH^2, 
       MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-4*S + S34 - T - 2*T14 + 2*U)*Pair[e[1], k[3]] - 
         (MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
         Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           2*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            (3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
      PVD[0, 1, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-(Pair[e[2], ec[5]]*
         (-((MH^2 - T)*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + 2*(MH^2 + S34)*
             Pair[ec[4], k[2]] + (-8*MH^2 - 2*S + 3*S34 + 3*T + 2*T24 + 6*U)*
             Pair[ec[4], k[3]]))) - 2*((MH^2 - T)*Pair[ec[4], k[3]]*
          (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*
          ((MH^2 + S34)*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
           (MH^2 + S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
           4*Pair[ec[4], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[3]])))))*PVD[0, 2, 0, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*
      Pair[ec[4], k[1]]*((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (Pair[e[2], ec[5]]*((-2*MH^2 + S34 + T + 2*U)*Pair[ec[4], k[1]] + 
         (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) + 
       4*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
            Pair[ec[5], k[3]])))*PVD[0, 2, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]*
      ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 0, 
       S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (((-7*MH^2 + 4*S34 + 3*T + T14 + 2*T24 + 6*U)*Pair[e[1], k[3]] + 
         (-2*MH^2 + S34 + T + 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
       4*(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
          (3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
            Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(3*Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]]))))*PVD[0, 2, 1, 0, MH^2, 0, 
       MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[ec[4], k[3]]*((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
       4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVD[0, 3, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 0, 
       S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-(Pair[ec[4], k[1]]*
         ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
          2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]))) + Pair[e[1], ec[4]]*
        ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(Pair[e[2], ec[5]]*
        (Pair[e[1], k[5]]*(2*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
         Pair[e[1], k[2]]*(Pair[ec[4], k[3]] - 2*Pair[ec[4], k[5]])) + 
       2*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*PVD[1, 0, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*Pair[ec[4], k[1]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[e[2], ec[5]] - 
         2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], k[5]]*
        ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[ec[4], k[3]]) - 2*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[5]])) + 
       4*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
         Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
       Pair[e[1], ec[4]]*((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[5], k[2]])))*(PVD[1, 0, 1, 0, 0, MH^2, 
        MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*Pair[e[1], k[4]]*
        (Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) - 
         2*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
       0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], ec[5]]*
        ((Pair[e[1], k[2]] + Pair[e[1], k[5]])*Pair[ec[4], k[3]] + 
         Pair[e[1], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
       4*Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
         Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + 4*Pair[e[1], k[3]]*
        (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
         4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
            Pair[ec[5], k[3]])))*PVD[1, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
       0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/
  (MH^2 - S - T24 - U) + 
 (Alfas^2*col3*EL*MT^2*
   (-(((Pair[e[1], ec[5]]*((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
           4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) - 
         4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
            ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[3]])) + 2*Pair[e[2], ec[4]]*
          (-2*Pair[e[1], k[4]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*
            Pair[ec[5], k[4]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T24, MH^2 - S - T - T14, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], ec[5]]*((S34*T14 - S*U)*Pair[e[2], ec[4]] + 
           2*(((S34 + U)*Pair[e[2], k[1]] - (S + T14)*Pair[e[2], k[3]])*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], 
                 k[1]] - (S + T14)*Pair[ec[4], k[3]]))) + 
         2*(((-MH^2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
             (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
             (S + T14)*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
            Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - 
           2*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])*Pair[ec[5], k[1]] - 
             Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                  Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                   k[3]]))) + Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*
              ((-MH^2 + S34 + T + T14)*Pair[ec[5], k[1]] + (-MH^2 + S + T + 
                 T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*((-S + T14)*
                Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + S*Pair[ec[5], 
                 k[4]]) + Pair[e[1], k[2]]*((-MH^2 + S + T + U)*Pair[ec[5], 
                 k[1]] + (MH^2 - S - T - T14)*Pair[ec[5], k[4]]))))*
        PVD[0, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] - (-((S - T14)*(-MH^2 + S + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], ec[4]]) - 4*(MH^2 - S - T - T14)*Pair[e[1], ec[5]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]]) + 2*(2*T14*Pair[e[1], k[2]] + 
           2*S*Pair[e[1], k[4]] + (S - T14)*Pair[e[1], k[5]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], ec[4]]*
          Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 8*Pair[e[1], k[3]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]])*Pair[ec[5], k[1]])*PVD[0, 0, 0, 1, 0, MH^2, 
         T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (-((MH^2*(S - T14) - S*(S34 + T - U) + T14*(-S34 + T + U))*
           Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
         4*Pair[e[1], ec[5]]*(((-MH^2 + T)*Pair[e[2], k[1]] + 
             (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*((-MH^2 + T)*Pair[ec[4], k[1]] + 
             (S + T14)*Pair[ec[4], k[3]])) + 
         8*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]]) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
            (Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         4*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
         4*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
         2*Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(2*(MH^2 - T - U)*
              Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[3]] + 
             2*(MH^2 - S34 - T)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (2*(-MH^2 + T + U)*Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[
                3]] + 2*(-MH^2 + S34 + T)*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[3]]*(2*S*Pair[ec[5], k[2]] + (-S + T14)*
              Pair[ec[5], k[3]] + 2*T14*Pair[ec[5], k[4]])))*
        PVD[0, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + 2*((Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVC[0, 0, 1, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[3]])*PVC[0, 1, 0, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          (2*(-MH^2 + S34 + T + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
           2*(-MH^2 + S34 + T + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
            ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (S - S34 - T14 + U)*
              Pair[ec[5], k[3]] + (-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))*
          PVD[0, 1, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       4*(((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         ((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 2, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]]*PVD[0, 1, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       8*(Pair[e[1], k[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]]*PVD[0, 2, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[3]]*PVD[0, 2, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((S - T14)*Pair[e[2], ec[4]] - 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
              Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
               Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])*
          PVD[1, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       8*((-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
           4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 3, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((-S + T14)*Pair[e[2], ec[4]] + 
           4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-(Pair[e[2], ec[4]]*((S - S34 - T14 + U)*Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*((2*S - S34 - 2*T14 + U)*
                 Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]]))) + 
           4*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
              (2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[4]])))*
          PVD[0, 1, 1, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-(((S - S34 - T14 + U)*Pair[e[1], k[3]] + 
              (2*S - S34 - 2*T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]]) + 
           4*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*
              (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 2*Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + 
                 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
          PVD[0, 1, 2, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVD[1, 0, 0, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
             Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
             Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]])))*
          PVD[1, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + S34 + T14 - U)*
              Pair[e[2], ec[4]] + 4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[1], k[5]]*
            (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
                Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
               Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/T24) + 
    ((-(Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], ec[4]] + 
           2*(2*Pair[e[2], k[5]]*(-Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
             2*Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])))) + 
        4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, T24, MH^2, MH^2 - S - T - T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
          2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*(3*Pair[ec[4], k[2]] - 
              2*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]))) + 
        4*(Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
          (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
             (3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-(S*S34) - S34*T24 - T*T24 + 
            MH^2*(S - T14 + T24) + T14*U)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]]*(-(T24*Pair[ec[4], k[1]]) + T14*Pair[ec[4], 
                k[2]]) + Pair[e[2], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
              (MH^2 - S34 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + 
              T24*Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) + 
          Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + (MH^2 - U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] - Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
           ((-MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
             Pair[ec[5], k[2]] - T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + U*Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]) - 2*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + 
            S34*U + T14*U)*Pair[e[2], ec[4]] - 
          2*(Pair[e[2], k[4]]*((MH^2 - U)*Pair[ec[4], k[1]] + 
              (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[3]]*(T24*Pair[ec[4], k[1]] - (S34 + T14)*Pair[ec[4], 
                k[2]] + 2*S*Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]])) + Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + 
            (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
            (-2*MH^2 + T24 + 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
            MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
            S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
          2*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] - Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + ((MH^4 - 2*T*T24 + 2*T14*U + S34*(-T24 + U) - 
          MH^2*(S34 + 2*T14 - T24 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*Pair[e[1], k[5]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[3]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
          2*Pair[e[2], k[3]]*((-MH^2 + S34 + T24)*Pair[ec[4], k[1]] - 
            T14*Pair[ec[4], k[2]] + (S + T14)*Pair[ec[4], k[3]]) + 
          Pair[e[2], k[4]]*(2*(S34 - U)*Pair[ec[4], k[1]] + 
            2*(-MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
            (-MH^2 + 2*S + 2*T14 + T24 + U)*Pair[ec[4], k[3]])) + 
        4*Pair[e[1], ec[4]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[1]] + 8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - Pair[e[1], k[3]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])*Pair[ec[5], k[3]])) - 4*Pair[e[2], ec[4]]*
         (T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + (MH^2 - U)*
           Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (T24*Pair[ec[5], k[3]] + (MH^2 - U)*Pair[ec[5], k[4]])))*
       PVD[0, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((MH^4 + 4*S*S34 + S34*T24 - 2*T*T24 + 3*S34*U + 
            2*T14*U - MH^2*(3*S34 + 2*T14 - T24 + U))*Pair[e[2], ec[4]] - 
          2*(2*Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + 
                U)*Pair[ec[4], k[3]]) - 2*Pair[e[2], k[3]]*
             ((MH^2 + S34 - T24)*Pair[ec[4], k[1]] + (2*S34 + T14)*Pair[
                ec[4], k[2]] + (-9*MH^2 + 4*S34 + 5*T + 4*T14 + 4*T24 + 4*U)*
               Pair[ec[4], k[3]]) - Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], 
                k[1]] + 2*(MH^2 + S34 - T)*Pair[ec[4], k[2]] + 
              (-17*MH^2 + 8*S34 + 10*T + 8*T14 + 7*T24 + 7*U)*Pair[ec[4], 
                k[3]]))) + 4*(Pair[e[1], k[5]]*
           ((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], 
                k[3]])) + Pair[e[1], ec[4]]*
           (-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + 2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[
                e[2], k[5]])*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*
              Pair[ec[5], k[1]]) + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + 
              (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(-(Pair[ec[4], k[2]]*
                  Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                 (4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))) - 
          Pair[e[2], ec[4]]*(2*S34*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
            2*S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 1, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 
      2*((-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + 
              Pair[e[2], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVC[0, 1, 0, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         (2*(-MH^2 + S34)*Pair[e[1], k[5]]*Pair[ec[4], ec[5]] - 
          Pair[e[1], ec[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
            (-MH^2 + S34)*Pair[ec[4], k[2]] + (2*T14 + T24)*
             Pair[ec[4], k[3]]) + 2*(-MH^2 + S34)*Pair[e[1], ec[4]]*
           Pair[ec[5], k[1]] - 4*Pair[ec[4], k[3]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*
           ((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + S34*U + T14*U)*
             Pair[e[2], ec[4]] - Pair[e[2], k[4]]*(2*(MH^2 - U)*Pair[ec[4], 
                k[1]] - 2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - U)*
               Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(2*(MH^2 + S34 - T24)*
               Pair[ec[4], k[1]] + (MH^2 + 3*S34 + 2*T14)*Pair[ec[4], k[2]] + 
              2*(-3*MH^2 - 3*S + S34 + 2*T + T14 + U)*Pair[ec[4], k[3]]) - 
            2*Pair[e[2], k[1]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
          2*(Pair[e[1], k[5]]*((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
                (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
                (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
            Pair[e[1], ec[4]]*(-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
              (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
            Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
            Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], 
                k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
              MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
               Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
              S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
               Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
              U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
            2*(3*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*(
                Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              Pair[e[1], k[3]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[2]]) + 
                2*Pair[e[2], k[3]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              3*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[2]] - Pair[e[1], k[5]]*Pair[e[2], k[4]]*
               Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[5]]*Pair[
                e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
              Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], 
                k[4]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
               Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      4*((-(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + Pair[e[2], k[5]])*
            (Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[1]]))*PVC[0, 0, 1, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) - 
          Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
            2*Pair[e[2], k[5]]*Pair[ec[4], k[5]] - Pair[e[2], k[4]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[5]])) + 
          2*(Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
             (Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*
         PVC[0, 1, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           (T24*Pair[e[2], k[1]] + 2*(-MH^2 + 2*S + T24 + U)*
             Pair[e[2], k[4]] + T24*Pair[e[2], k[5]]) - 
          2*(T24*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + T24*Pair[e[1], e[2]]*
             Pair[ec[5], k[1]] + 4*Pair[e[2], k[4]]*(Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*((MH^2 + 3*S34)*Pair[e[2], k[3]] + 
            (MH^2 + S34)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
          2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] - Pair[e[1], ec[5]]*(2*(2*MH^2 - T24 - 2*U)*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[4]]*
             ((MH^2 + S34)*Pair[ec[4], k[1]] + (-4*S + 2*S34 + 4*T - 3*T24 - 
                2*U)*Pair[ec[4], k[3]] + (MH^2 + S34)*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[3]]*((MH^2 + 3*S34)*Pair[ec[4], k[1]] + 
              (4*MH^2 - 12*S + 2*S34 + 4*T - 7*T24 - 6*U)*Pair[ec[4], k[3]] + 
              (MH^2 + 3*S34)*Pair[ec[4], k[5]])) + 2*Pair[e[1], ec[4]]*
           ((MH^2 + 3*S34)*Pair[e[2], k[3]] + (MH^2 + S34)*Pair[e[2], k[4]])*
           Pair[ec[5], k[1]] - 2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[ec[4], k[3]]*
           (2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(3*Pair[e[2], k[3]] + 
              Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*(MH^2 + S34)*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
           Pair[ec[4], ec[5]] + 2*(MH^2 - U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
              Pair[ec[4], k[3]]) - Pair[e[2], k[3]]*(2*(MH^2 + S34)*Pair[
                ec[4], k[1]] + (MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-8*MH^2 - 2*S + 3*S34 + 6*T + 2*T14 + 3*U)*Pair[ec[4], 
                k[3]])) + 2*(MH^2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[3]]*
           Pair[ec[5], k[1]] + 2*(MH^2 - U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))*PVD[0, 0, 2, 0, 0, U, 
          0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (T24*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[3]] + 
          (MH^2 - T24 - U)*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[1]] - 4*T24*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) + 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((MH^2 - U)*(MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
         Pair[e[2], ec[4]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 4*(MH^2 - U)*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) - 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, U, 0, S34, MH^2, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[ec[4], k[3]]*(2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-4*S34*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (Pair[e[2], k[5]]*(-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 + 8*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] - 2*S34*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 - 8*S - 
                5*(T24 + U))*Pair[ec[4], k[3]] + 2*S34*Pair[ec[4], k[5]])) - 
          4*S34*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[5], k[1]] + 2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          16*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[3]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*(-MH^2 + T24 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-2*(MH^2 - T24 - U)*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] - Pair[e[1], ec[5]]*
           (-(Pair[e[2], k[5]]*(4*S*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                 3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*
                Pair[ec[4], k[5]])) + Pair[e[2], k[1]]*
             (2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*Pair[
                ec[4], k[5]])) - 2*(MH^2 - T24 - U)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*T24*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*(T24*Pair[e[2], k[1]] + 
            2*(-MH^2 + 2*S + T24 + U)*Pair[e[2], k[4]] + 
            T24*Pair[e[2], k[5]]) + 2*T24*Pair[e[1], e[2]]*
           Pair[ec[5], k[1]] + 8*Pair[e[2], k[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          Pair[e[1], ec[5]]*(-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], 
                k[4]])*Pair[ec[4], k[2]]) + Pair[e[2], k[3]]*
             (2*(-MH^2 + T24 + U)*Pair[ec[4], k[1]] + (-2*MH^2 + 4*S + 
                3*T24 + 2*U)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T24 + U)*Pair[
                ec[4], k[3]])) + 2*(-MH^2 + T24 + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 2*(-MH^2 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          8*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      8*((Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]])) - Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]]*Pair[ec[4], k[1]] - Pair[e[2], k[5]]*
             Pair[ec[4], k[5]]) + (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*PVC[0, 0, 1, U, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[2]]*((-MH^2 + 2*S + T24 + U)*
           Pair[e[1], ec[5]] - 4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
          4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-(Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
              (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
             2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
                Pair[ec[5], k[1]]))) + Pair[e[2], ec[4]]*
           ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
             Pair[e[2], ec[4]] + Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
             ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*
             Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         PVD[1, 0, 0, 0, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*(Pair[ec[4], k[3]]*((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 0, 3, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 0, 0, 3, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], k[3]] + 
            (-2*S + S34 + 2*T14 - U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[
                ec[5], k[4]] + Pair[e[2], k[4]]*(Pair[ec[5], k[3]] + 
                2*Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 2, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (-(Pair[e[1], ec[5]]*((4*S - S34 - 2*T + 2*T24 + U)*Pair[e[2], k[
                3]] + (MH^2 + 2*S - S34 - 2*T + T24)*Pair[e[2], k[4]])) + 
          4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 2, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-7*MH^2 + 4*S34 + 6*T + 2*T14 + T24 + 3*U)*Pair[e[2], k[3]] + 
            (-2*MH^2 + S34 + 2*T + U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 
                Pair[ec[5], k[4]]))))*PVD[0, 0, 2, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[3]]))*PVD[0, 0, 3, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[3]]) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 1, 0, 2, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]] - Pair[e[2], k[4]]*
             ((2*S - S34 - 2*T14 + U)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + 
                T24 + U)*Pair[ec[4], k[3]])) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        (-(Pair[e[1], ec[5]]*(Pair[e[2], k[4]]*((2*MH^2 - S34 - 2*T - U)*
                Pair[ec[4], k[2]] + (-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]) + Pair[e[2], k[3]]*((MH^2 + 2*S - S34 - 2*T + T24)*
                Pair[ec[4], k[2]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]))) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[3]]*
             (2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[2]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[5]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
         ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*(Pair[e[1], ec[5]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[ec[4], k[2]] + 
            (MH^2 - 2*S - T24 - U)*Pair[ec[4], k[3]]) + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))*PVD[0, 1, 2, 0, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*((MH^2 - 2*S - T24 - U)*
           Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 2, 0, 1, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (-(Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
              Pair[e[2], ec[4]] + 2*(Pair[e[2], k[1]]*(2*Pair[ec[4], k[1]] + 
                 Pair[ec[4], k[2]]) + Pair[e[2], k[5]]*(Pair[ec[4], k[2]] - 
                 2*Pair[ec[4], k[5]])))) + 
          4*(Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
            Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         (PVD[1, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 0, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*Pair[e[2], k[4]]*(Pair[e[1], ec[5]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[ec[4], ec[5]] + 
              Pair[e[1], ec[4]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-2*MH^2 + S34 + 2*T + U)*
             Pair[e[2], ec[4]] - 2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
               Pair[ec[4], k[3]] + Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
                Pair[ec[4], k[5]]))) + 
          4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
             (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVD[1, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
            2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*(PVD[1, 1, 0, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]])))/(MH^2 - S - T - T14) + 
    ((4*Pair[e[2], k[5]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[ec[4], k[1]]) - 
        2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[5]]*Pair[ec[4], k[1]] + (2*Pair[e[1], k[3]] + 
            Pair[e[1], k[4]])*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
        4*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - (2*Pair[e[2], ec[5]]*(-2*Pair[e[1], k[5]]*
           Pair[ec[4], k[2]] + 2*Pair[e[1], k[2]]*Pair[ec[4], k[5]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
        4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
          ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
           Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
         (-((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[2], ec[5]]) + 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))*
       PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 4*(2*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
         Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
         (2*Pair[e[1], k[2]]*Pair[ec[4], k[2]] - 2*Pair[e[1], k[5]]*
           Pair[ec[4], k[5]] - Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[5]])) + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[3]])*Pair[ec[5], k[2]])*PVC[0, 0, 1, 0, 
        MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      2*(-2*Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])))*PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 4*Pair[e[1], k[4]]*
       (2*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
         (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]])*PVC[0, 1, 0, 0, MH^2 - S - T24 - U, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      2*(-2*Pair[ec[4], k[3]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])))*PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      (Pair[e[1], ec[4]]*((S*S34 + S34*T14 - MH^2*(S + T14 - T24) - T*T24 + 
            T14*U)*Pair[e[2], ec[5]] - 
          2*(-(((MH^2 - S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
               (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]]) - 
            Pair[e[2], k[5]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) - 
        2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
            T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
           (Pair[e[1], k[3]]*(T24*Pair[ec[4], k[1]] - T14*Pair[ec[4], 
                k[2]]) + Pair[e[1], k[4]]*((MH^2 - S34 - U)*Pair[ec[4], 
                k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], 
                k[3]]) + Pair[e[1], k[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]])) + (T14*Pair[e[1], k[3]] + 
            (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] - Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
           Pair[ec[5], k[2]] + 2*(-(Pair[e[1], k[4]]*
              ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                  Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                  Pair[ec[5], k[3]]))) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]]))))*PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[4]]*((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + 
            T*T24 - T14*U)*Pair[e[2], ec[5]] - 
          2*(((MH^2 + S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
              (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) + 
        2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
            (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
              (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[3]]*(-((S34 + T24)*Pair[ec[4], k[1]]) + 
              T14*Pair[ec[4], k[2]] + 2*S*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
              (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]])) + 
          (T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + (-2*MH^2 + 2*T + T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
                Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             ((Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 2*Pair[e[2], k[1]]*
                 Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                 Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 0, MH^2, 0, 
        MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + (T14*(-MH^2 + 2*S + T + T14)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - 2*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]] + 
          2*S*Pair[e[1], k[3]] + (MH^2 - T - T14)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(-MH^2 + T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
        4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] - 4*T14*Pair[e[1], ec[4]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]) + 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((MH^2 - T)*(MH^2 - 2*S - T - T14)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]) - 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
        0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((-MH^4 + S34*(-T + T14) - 2*T*T24 + MH^2*(S34 + T - T14 + 2*T24) + 
          2*T14*U)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
        4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + (S34 - T)*Pair[e[1], k[4]])*
         Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
        2*Pair[e[2], ec[5]]*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]]*
           Pair[ec[4], k[3]] + 2*Pair[e[1], k[3]]*(-(T24*Pair[ec[4], k[1]]) + 
            (-MH^2 + S34 + T14)*Pair[ec[4], k[2]] + (S + T24)*
             Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*
           (2*(-MH^2 + S34 + U)*Pair[ec[4], k[1]] + 2*(S34 - T)*
             Pair[ec[4], k[2]] + (-MH^2 + 2*S + T + T14 + 2*T24)*
             Pair[ec[4], k[3]])) - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
          (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
        4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
            (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[5], k[2]])) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
           (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])) + 4*Pair[e[1], ec[4]]*
         (T14*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + (MH^2 - T)*
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
           (T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])))*
       PVD[0, 0, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      (2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(4*S34*Pair[ec[4], k[1]] + 
            6*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) - 2*Pair[e[1], k[3]]*
           ((2*S34 + T24)*Pair[ec[4], k[1]] + (MH^2 + S34 - T14)*
             Pair[ec[4], k[2]] + (-9*MH^2 + 4*S34 + 4*T + 4*T14 + 4*T24 + 
              5*U)*Pair[ec[4], k[3]]) - Pair[e[1], k[4]]*
           (2*(MH^2 + S34 - U)*Pair[ec[4], k[1]] + 2*(S34 + T)*
             Pair[ec[4], k[2]] + (-17*MH^2 + 8*S34 + 7*T + 7*T14 + 8*T24 + 
              10*U)*Pair[ec[4], k[3]])) - 
        4*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
             Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(2*S34*Pair[ec[4], k[1]] + 
            3*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) + 
          (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
             Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          Pair[e[1], e[2]]*(2*S34*Pair[ec[4], k[1]] + 3*(-MH^2 + T + T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          2*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             ((4*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
                 (-Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*Pair[ec[5], 
                k[2]] + Pair[e[2], k[5]]*(-(Pair[ec[4], k[1]]*Pair[ec[5], 
                   k[3]]) + Pair[ec[4], k[3]]*(4*Pair[ec[5], k[1]] + 
                  Pair[ec[5], k[3]]))))) + Pair[e[1], ec[4]]*
         (-((MH^4 + 4*S*S34 + 3*S34*T + S34*T14 + 2*T*T24 - 
             MH^2*(3*S34 + T - T14 + 2*T24) - 2*T14*U)*Pair[e[2], ec[5]]) + 
          4*((2*S34*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
              (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(2*S34*Pair[ec[5], k[1]] + T14*Pair[ec[5], 
                k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))))*
       PVD[0, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[ec[4], k[1]]*(-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[
                2]] + (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[5]]) + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*
           Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] - 
          Pair[e[2], ec[5]]*(-(Pair[e[1], k[5]]*(4*S*Pair[ec[4], k[1]] + 
               (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 2*
                (-MH^2 + T + T14)*Pair[ec[4], k[5]])) + Pair[e[1], k[2]]*
             (2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[1]] + (-3*MH^2 + 4*S + 
                3*(T + T14))*Pair[ec[4], k[3]] + 2*(-MH^2 + T + T14)*Pair[
                ec[4], k[5]])) - 2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - 
            Pair[e[1], k[5]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*Pair[ec[4], k[1]]*((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - 
          Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
        4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*(PVD[0, 0, 1, 2, 0, MH^2, MH^2 - S - T24 - U, 
         0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        PVD[0, 0, 1, 2, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(Pair[ec[4], k[3]]*(-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[
                2]] + (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[5]]) + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]] - Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 + 3*S - 
                7*S34 - 5*T24 - 5*U)*Pair[ec[4], k[3]] - 4*S34*Pair[ec[4], 
                k[5]]) + Pair[e[1], k[5]]*(-2*S34*Pair[ec[4], k[1]] + 
              (-3*MH^2 - 5*S + 5*S34 + 3*T24 + 3*U)*Pair[ec[4], k[3]] + 
              4*S34*Pair[ec[4], k[5]])) - 4*S34*(Pair[e[1], k[2]] - 
            Pair[e[1], k[5]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[2]] + 16*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[ec[4], k[3]]*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) + 
      8*(((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*(2*Pair[ec[4], k[1]] + 
            Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 2, 1, 0, MH^2, 
           MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 2, 1, MH^2, 0, 
           MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + Pair[ec[4], k[3]]*((MH^2 - 2*S - T - T14)*
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 3, 0, 0, MH^2, 
           MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 3, 0, MH^2, 0, 
           MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]])) - 
      2*(Pair[e[1], k[4]]*(2*(MH^2 - S34)*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
           ((-MH^2 + S34)*Pair[ec[4], k[1]] - 2*(MH^2 - S34)*
             Pair[ec[4], k[2]] + (T14 + 2*T24)*Pair[ec[4], k[3]]) + 
          2*(MH^2 - S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          4*Pair[ec[4], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[4]]*(-2*(MH^2 - U)*Pair[ec[4], 
                k[1]] + 2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - T)*
               Pair[ec[4], k[3]]) + 2*Pair[e[1], k[2]]*
             ((MH^2 + S34)*Pair[ec[4], k[1]] + (-3*MH^2 + 3*T + T14)*Pair[
                ec[4], k[3]]) - Pair[e[1], k[3]]*((MH^2 + 3*S34 + 2*T24)*Pair[
                ec[4], k[1]] + 2*(MH^2 + S34 - T14)*Pair[ec[4], k[2]] + 
              2*(-3*MH^2 - 3*S + S34 + T + T24 + 2*U)*Pair[ec[4], k[3]])) + 
          Pair[e[1], ec[4]]*(-((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + 
               T*T24 - T14*U)*Pair[e[2], ec[5]]) + 
            2*(((MH^2 + S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
                (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[5], k[1]] + 
                T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) - 
          2*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*Pair[e[1], 
                k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            Pair[e[1], ec[5]]*Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], 
                k[1]] + (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]]) + 
            (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
            Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
              (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
            2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
                  Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
                  Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                ((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], 
                    k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                     k[3]]))*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 (Pair[ec[4], k[1]]*Pair[ec[5], k[4]] + Pair[ec[4], k[3]]*
                   (3*Pair[ec[5], k[1]] + 2*Pair[ec[5], k[3]] + Pair[ec[5], 
                     k[4]]))))))*PVD[0, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(Pair[ec[4], k[1]]*(-((T14*Pair[e[1], k[2]] + 
             2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
             T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
          2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + T)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*
           (2*(-MH^2 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            (MH^2 - T)*Pair[e[1], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[3]]*((-2*MH^2 + 4*S + 2*T + 3*T14)*Pair[ec[4], 
                k[1]] + 2*(-MH^2 + T + T14)*Pair[ec[4], k[2]] + 
              (-MH^2 + 4*S + T + T14)*Pair[ec[4], k[3]])) + 
          2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*(-MH^2 + T)*Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 8*Pair[e[1], k[3]]*
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
      8*(Pair[e[1], k[4]]*Pair[ec[4], k[1]]*((-MH^2 + 2*S + T + T14)*
           Pair[e[2], ec[5]] - 4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*Pair[ec[4], k[3]]*(-((T14*Pair[e[1], k[2]] + 
           2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + T14*Pair[e[1], k[5]])*
          Pair[e[2], ec[5]]) + 2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + (MH^2 + S34)*Pair[e[1], k[4]])*
         Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*(-2*MH^2 + 2*T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[5]]*(2*(-2*MH^2 + 2*T + T14)*Pair[e[1], k[5]]*
           Pair[ec[4], k[3]] + Pair[e[1], k[4]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + (7*MH^2 - 7*S34 - 2*T - T14 - 
              4*T24 - 8*U)*Pair[ec[4], k[3]] - 2*(MH^2 + S34)*
             Pair[ec[4], k[5]]) + Pair[e[1], k[3]]*
           ((MH^2 + 3*S34)*Pair[ec[4], k[1]] + (19*MH^2 - 17*S34 - 6*T - 
              5*T14 - 12*T24 - 16*U)*Pair[ec[4], k[3]] - 2*(MH^2 + 3*S34)*
             Pair[ec[4], k[5]])) + 2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
          (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] - 2*(-2*MH^2 + 2*T + T14)*Pair[e[1], e[2]]*
         Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
         (Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
            3*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
       PVD[0, 1, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      8*((Pair[e[2], ec[5]]*((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] - Pair[e[1], k[4]]*((2*S - S34 + T - 2*T24)*
               Pair[ec[4], k[1]] + (-MH^2 + 2*S + T + T14)*Pair[ec[4], 
                k[3]])) + 4*(Pair[e[1], k[4]]*(2*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*(Pair[e[2], k[5]]*Pair[
                ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[1]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[4]]*((-2*MH^2 + S34 + T + 2*U)*Pair[
                ec[4], k[1]] + (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) - 
            Pair[e[1], k[3]]*((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[ec[4], 
                k[1]] + 2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
          4*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
             (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                 Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                 Pair[ec[5], k[3]]))))*PVD[0, 1, 1, 1, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]] + 
            (-2*S + S34 - T + 2*T24)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*
             (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[2], k[4]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
                2*Pair[ec[5], k[4]]))))*PVD[0, 1, 2, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-4*S + S34 - T - 2*T14 + 2*U)*Pair[e[1], k[3]] - 
            (MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[5]] + 4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
              2*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
         PVD[0, 1, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(-(Pair[e[2], ec[5]]*(-((MH^2 - T)*(2*Pair[e[1], k[2]] - 
              Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
            ((MH^2 + S34)*Pair[ec[4], k[1]] + 2*(MH^2 + S34)*Pair[ec[4], k[
                2]] + (-8*MH^2 - 2*S + 3*S34 + 3*T + 2*T24 + 6*U)*
              Pair[ec[4], k[3]]))) - 2*((MH^2 - T)*Pair[ec[4], k[3]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*
           ((MH^2 + S34)*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            (MH^2 + S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
            4*Pair[ec[4], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]])))))*PVD[0, 2, 0, 0, MH^2, 0, 
        MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 8*(Pair[e[1], k[4]]*Pair[ec[4], k[1]]*
         ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 2, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[3]]*(Pair[e[2], ec[5]]*((-2*MH^2 + S34 + T + 2*U)*
             Pair[ec[4], k[1]] + (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) + 
          4*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[
                ec[5], k[3]])))*PVD[0, 2, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[3]]*((-MH^2 + S34 + T14 + 2*T24)*
           Pair[e[2], ec[5]] + 4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-7*MH^2 + 4*S34 + 3*T + T14 + 2*T24 + 6*U)*Pair[e[1], k[3]] + 
            (-2*MH^2 + S34 + T + 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
             (3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(3*Pair[ec[5], k[3]] + 
                Pair[ec[5], k[4]]))))*PVD[0, 2, 1, 0, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[1], k[3]]*Pair[ec[4], k[3]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]]))*PVD[0, 3, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
      8*((-(Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
              Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
               Pair[e[1], e[2]]*Pair[ec[5], k[2]]))) + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*(2*Pair[ec[4], k[2]] + 
              Pair[ec[4], k[3]]) + Pair[e[1], k[2]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]])) + 
          2*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
             Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
             (Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]) + 
            ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
              Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))*
             Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*((-2*Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
             Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
           ((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(PVD[1, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
           0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (4*Pair[e[2], k[5]]*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[ec[4], k[3]]) - 
          2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 
              2*Pair[ec[4], k[2]]) + Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 
              2*Pair[ec[4], k[5]])) + 
          4*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          Pair[e[1], ec[4]]*((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(PVD[1, 0, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
           0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*Pair[e[1], k[4]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] + 
              Pair[ec[4], k[5]]) - 2*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
           ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-2*Pair[e[2], ec[5]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
             Pair[ec[4], k[3]] + Pair[e[1], k[3]]*(Pair[ec[4], k[2]] + 
              Pair[ec[4], k[5]])) + 4*Pair[ec[4], k[3]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + 4*Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
           ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[3]])))*PVD[1, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     (MH^2 - S - T24 - U)))/(MW*SW) + 
 (Alfas^2*col4*EL*MT^2*
   (-(((Pair[e[1], ec[5]]*((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
           4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))) - 
         4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
            ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[3]])) + 2*Pair[e[2], ec[4]]*
          (-2*Pair[e[1], k[4]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*
            Pair[ec[5], k[4]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T24, MH^2 - S - T - T14, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], ec[5]]*((S34*T14 - S*U)*Pair[e[2], ec[4]] + 
           2*(((S34 + U)*Pair[e[2], k[1]] - (S + T14)*Pair[e[2], k[3]])*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], 
                 k[1]] - (S + T14)*Pair[ec[4], k[3]]))) + 
         2*(((-MH^2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
             (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
             (S + T14)*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
            Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[4]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]*((-MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (-MH^2 + S + T + T14)*Pair[ec[5], k[3]]) - 
           2*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])*Pair[ec[5], k[1]] - 
             Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                  Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                   k[3]]))) + Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*
              ((-MH^2 + S34 + T + T14)*Pair[ec[5], k[1]] + (-MH^2 + S + T + 
                 T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*((-S + T14)*
                Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + S*Pair[ec[5], 
                 k[4]]) + Pair[e[1], k[2]]*((-MH^2 + S + T + U)*Pair[ec[5], 
                 k[1]] + (MH^2 - S - T - T14)*Pair[ec[5], k[4]]))))*
        PVD[0, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] - (-((S - T14)*(-MH^2 + S + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], ec[4]]) - 4*(MH^2 - S - T - T14)*Pair[e[1], ec[5]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]]) + 2*(2*T14*Pair[e[1], k[2]] + 
           2*S*Pair[e[1], k[4]] + (S - T14)*Pair[e[1], k[5]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], ec[4]]*
          Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S + T14)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 8*Pair[e[1], k[3]]*
          (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
            Pair[ec[4], k[2]])*Pair[ec[5], k[1]])*PVD[0, 0, 0, 1, 0, MH^2, 
         T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (-((MH^2*(S - T14) - S*(S34 + T - U) + T14*(-S34 + T + U))*
           Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
         4*((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], k[2]] + 
           (S + T14)*Pair[e[1], k[3]] + (2*MH^2 - S34 - 2*T - U)*
            Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
         4*Pair[e[1], ec[5]]*(((-MH^2 + T)*Pair[e[2], k[1]] + 
             (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*((-MH^2 + T)*Pair[ec[4], k[1]] + 
             (S + T14)*Pair[ec[4], k[3]])) + 
         8*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]]) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
            (Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         4*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
         4*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
         2*Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(2*(MH^2 - T - U)*
              Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[3]] + 
             2*(MH^2 - S34 - T)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (2*(-MH^2 + T + U)*Pair[ec[5], k[2]] + (S + T14)*Pair[ec[5], k[
                3]] + 2*(-MH^2 + S34 + T)*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[3]]*(2*S*Pair[ec[5], k[2]] + (-S + T14)*
              Pair[ec[5], k[3]] + 2*T14*Pair[ec[5], k[4]])))*
        PVD[0, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
         MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + 2*((Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVC[0, 0, 1, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[3]])*PVC[0, 1, 0, MH^2, T24, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          (2*(-MH^2 + S34 + T + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
           2*(-MH^2 + S34 + T + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
            ((-MH^2 + S34 + T + U)*Pair[ec[5], k[2]] + (S - S34 - T14 + U)*
              Pair[ec[5], k[3]] + (-MH^2 + S34 + T + U)*Pair[ec[5], k[4]]))*
          PVD[0, 1, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       4*(((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         ((-((S - 3*T14)*Pair[e[1], k[2]]) + (3*S - T14)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] - 2*(S + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(S + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 2, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (((-MH^2 + S + T + T14)*Pair[e[1], k[2]] + (-MH^2 + S + T + T14)*
              Pair[e[1], k[4]] + 2*(-S + T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] - 2*(-MH^2 + S + T + T14)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] + 8*Pair[e[1], k[5]]*
            (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*
              Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]]*PVD[0, 1, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       8*(Pair[e[1], k[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]]*PVD[0, 2, 0, 1, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
           4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[3]]*PVD[0, 2, 1, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((S - T14)*Pair[e[2], ec[4]] - 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
              Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
               Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])*
          PVD[1, 0, 0, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) - 
       8*((-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 0, 1, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
           4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 3, 0, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[5]]*
          ((-S + T14)*Pair[e[2], ec[4]] + 
           4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]*PVD[0, 1, 0, 2, 0, MH^2, 
           T24, 0, 2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         (-(Pair[e[2], ec[4]]*((S - S34 - T14 + U)*Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*((2*S - S34 - 2*T14 + U)*
                 Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]]))) + 
           4*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
              (2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[4]])))*
          PVD[0, 1, 1, 1, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (-(((S - S34 - T14 + U)*Pair[e[1], k[3]] + 
              (2*S - S34 - 2*T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]]) + 
           4*(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]]) + Pair[e[1], k[5]]*
              (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 2*Pair[e[2], k[5]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]] + 
                 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
          PVD[0, 1, 2, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], 
                 k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*
            Pair[ec[5], k[1]])*PVD[1, 0, 0, 1, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + 
         (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
            ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
             Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
             Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]])))*
          PVD[1, 0, 1, 0, 0, MH^2, T24, 0, 2*MH^2 - S34 - T - U, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-S + S34 + T14 - U)*
              Pair[e[2], ec[4]] + 4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[1], k[5]]*
            (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
                Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
               Pair[ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, T24, 0, 
           2*MH^2 - S34 - T - U, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/T24) + 
    ((-(Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], ec[4]] + 
           2*(2*Pair[e[2], k[5]]*(-Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
             Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
             2*Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])))) + 
        4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(3*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, T24, MH^2, MH^2 - S - T - T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
          2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*(3*Pair[ec[4], k[2]] - 
              2*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]))) + 
        4*(Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
          (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
             (3*Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-(S*S34) - S34*T24 - T*T24 + 
            MH^2*(S - T14 + T24) + T14*U)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]]*(-(T24*Pair[ec[4], k[1]]) + T14*Pair[ec[4], 
                k[2]]) + Pair[e[2], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
              (MH^2 - S34 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], k[2]] + 
              T24*Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
             ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) + 
          Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + (MH^2 - U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] - Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
           ((-MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + S34*Pair[e[1], k[5]]*
             Pair[ec[5], k[2]] - T24*Pair[e[1], k[5]]*Pair[ec[5], k[3]] - 
            MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + U*Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]) - 2*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + 
            S34*U + T14*U)*Pair[e[2], ec[4]] - 
          2*(Pair[e[2], k[4]]*((MH^2 - U)*Pair[ec[4], k[1]] + 
              (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[2], k[3]]*(T24*Pair[ec[4], k[1]] - (S34 + T14)*Pair[ec[4], 
                k[2]] + 2*S*Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*((T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             ((MH^2 + S34)*Pair[ec[4], k[2]] + (-2*MH^2 + T24 + 2*U)*Pair[
                ec[4], k[3]])) + Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + 
            (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
            (-2*MH^2 + T24 + 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
          Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
            MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
            S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
          2*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] - Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 0, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + ((MH^4 - 2*T*T24 + 2*T14*U + S34*(-T24 + U) - 
          MH^2*(S34 + 2*T14 - T24 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*Pair[e[1], k[5]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[3]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
          2*Pair[e[2], k[3]]*((-MH^2 + S34 + T24)*Pair[ec[4], k[1]] - 
            T14*Pair[ec[4], k[2]] + (S + T14)*Pair[ec[4], k[3]]) + 
          Pair[e[2], k[4]]*(2*(S34 - U)*Pair[ec[4], k[1]] + 
            2*(-MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
            (-MH^2 + 2*S + 2*T14 + T24 + U)*Pair[ec[4], k[3]])) + 
        4*Pair[e[1], ec[4]]*((-MH^2 + S34 + T24)*Pair[e[2], k[3]] + 
          (S34 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[1]] + 8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - Pair[e[1], k[3]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])*Pair[ec[5], k[3]])) - 4*Pair[e[2], ec[4]]*
         (T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + (MH^2 - U)*
           Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (T24*Pair[ec[5], k[3]] + (MH^2 - U)*Pair[ec[5], k[4]])))*
       PVD[0, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[5]]*((MH^4 + 4*S*S34 + S34*T24 - 2*T*T24 + 3*S34*U + 
            2*T14*U - MH^2*(3*S34 + 2*T14 - T24 + U))*Pair[e[2], ec[4]] - 
          2*(2*Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + 
                U)*Pair[ec[4], k[3]]) - 2*Pair[e[2], k[3]]*
             ((MH^2 + S34 - T24)*Pair[ec[4], k[1]] + (2*S34 + T14)*Pair[
                ec[4], k[2]] + (-9*MH^2 + 4*S34 + 5*T + 4*T14 + 4*T24 + 4*U)*
               Pair[ec[4], k[3]]) - Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], 
                k[1]] + 2*(MH^2 + S34 - T)*Pair[ec[4], k[2]] + 
              (-17*MH^2 + 8*S34 + 10*T + 8*T14 + 7*T24 + 7*U)*Pair[ec[4], 
                k[3]]))) + 4*(Pair[e[1], k[5]]*
           ((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*Pair[e[2], 
                k[4]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], 
                k[3]])) + Pair[e[1], ec[4]]*
           (-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) - (S34 + U)*
             Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           (2*S34*Pair[ec[4], k[2]] + 3*(-MH^2 + T24 + U)*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + 2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[
                e[2], k[5]])*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*
              Pair[ec[5], k[1]]) + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + 
              (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(-(Pair[ec[4], k[2]]*
                  Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                 (4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))) - 
          Pair[e[2], ec[4]]*(2*S34*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
            2*S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
             Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
            U*Pair[e[1], k[5]]*Pair[ec[5], k[4]])))*PVD[0, 0, 0, 1, 0, U, 0, 
        S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 
      2*((-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + 
              Pair[e[2], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVC[0, 1, 0, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         (2*(-MH^2 + S34)*Pair[e[1], k[5]]*Pair[ec[4], ec[5]] - 
          Pair[e[1], ec[5]]*(-2*(MH^2 - S34)*Pair[ec[4], k[1]] + 
            (-MH^2 + S34)*Pair[ec[4], k[2]] + (2*T14 + T24)*
             Pair[ec[4], k[3]]) + 2*(-MH^2 + S34)*Pair[e[1], ec[4]]*
           Pair[ec[5], k[1]] - 4*Pair[ec[4], k[3]]*
           (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*
           ((S*S34 - T*T24 + MH^2*(S - S34 - T14 + T24) + S34*U + T14*U)*
             Pair[e[2], ec[4]] - Pair[e[2], k[4]]*(2*(MH^2 - U)*Pair[ec[4], 
                k[1]] - 2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - U)*
               Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(2*(MH^2 + S34 - T24)*
               Pair[ec[4], k[1]] + (MH^2 + 3*S34 + 2*T14)*Pair[ec[4], k[2]] + 
              2*(-3*MH^2 - 3*S + S34 + 2*T + T14 + U)*Pair[ec[4], k[3]]) - 
            2*Pair[e[2], k[1]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
          2*(Pair[e[1], k[5]]*((-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
                (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
                (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])) + 
            Pair[e[1], ec[4]]*(-((MH^2 + S34 - T24)*Pair[e[2], k[3]]) + 
              (MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
            Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-3*MH^2 + T24 + 3*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] - 
            Pair[e[2], ec[4]]*((MH^2 + S34)*Pair[e[1], k[2]]*Pair[ec[5], 
                k[1]] + T24*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
              MH^2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - U*Pair[e[1], k[4]]*
               Pair[ec[5], k[1]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 
              S34*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*
               Pair[ec[5], k[3]] + MH^2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] - 
              U*Pair[e[1], k[5]]*Pair[ec[5], k[4]]) + 
            2*(3*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*(
                Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              Pair[e[1], k[3]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[2]]) + 
                2*Pair[e[2], k[3]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
              3*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*Pair[
                ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], 
                k[3]]*Pair[ec[5], k[2]] - Pair[e[1], k[5]]*Pair[e[2], k[4]]*
               Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[5]]*Pair[
                e[2], k[3]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
              Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], 
                k[4]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
               Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      4*((-(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] + Pair[e[2], k[5]])*
            (Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[1]]))*PVC[0, 0, 1, T24, MH^2, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])) - 
          Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]]*Pair[ec[4], k[1]] - 
            2*Pair[e[2], k[5]]*Pair[ec[4], k[5]] - Pair[e[2], k[4]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[5]])) + 
          2*(Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
             (Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*
         PVC[0, 1, 0, U, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           (T24*Pair[e[2], k[1]] + 2*(-MH^2 + 2*S + T24 + U)*
             Pair[e[2], k[4]] + T24*Pair[e[2], k[5]]) - 
          2*(T24*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + T24*Pair[e[1], e[2]]*
             Pair[ec[5], k[1]] + 4*Pair[e[2], k[4]]*(Pair[e[1], k[3]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*Pair[e[1], k[5]]*((MH^2 + 3*S34)*Pair[e[2], k[3]] + 
            (MH^2 + S34)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
          2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] - Pair[e[1], ec[5]]*(2*(2*MH^2 - T24 - 2*U)*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[4]]*
             ((MH^2 + S34)*Pair[ec[4], k[1]] + (-4*S + 2*S34 + 4*T - 3*T24 - 
                2*U)*Pair[ec[4], k[3]] + (MH^2 + S34)*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[3]]*((MH^2 + 3*S34)*Pair[ec[4], k[1]] + 
              (4*MH^2 - 12*S + 2*S34 + 4*T - 7*T24 - 6*U)*Pair[ec[4], k[3]] + 
              (MH^2 + 3*S34)*Pair[ec[4], k[5]])) + 2*Pair[e[1], ec[4]]*
           ((MH^2 + 3*S34)*Pair[e[2], k[3]] + (MH^2 + S34)*Pair[e[2], k[4]])*
           Pair[ec[5], k[1]] - 2*(-2*MH^2 + T24 + 2*U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[ec[4], k[3]]*
           (2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(3*Pair[e[2], k[3]] + 
              Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*(MH^2 + S34)*Pair[e[1], k[5]]*Pair[e[2], k[3]]*
           Pair[ec[4], ec[5]] + 2*(MH^2 - U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
              Pair[ec[4], k[3]]) - Pair[e[2], k[3]]*(2*(MH^2 + S34)*Pair[
                ec[4], k[1]] + (MH^2 + S34)*Pair[ec[4], k[2]] + 
              (-8*MH^2 - 2*S + 3*S34 + 6*T + 2*T14 + 3*U)*Pair[ec[4], 
                k[3]])) + 2*(MH^2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[3]]*
           Pair[ec[5], k[1]] + 2*(MH^2 - U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 8*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])))*PVD[0, 0, 2, 0, 0, U, 
          0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (T24*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] - 2*Pair[e[1], ec[5]]*
         (2*(-MH^2 + T24 + U)*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[3]] + 
          (MH^2 - T24 - U)*Pair[e[2], k[4]])*Pair[ec[4], k[2]] + 
        4*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[1]] - 4*T24*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) + 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, T24, MH^2, S34, 0, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((MH^2 - U)*(MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
         Pair[e[2], ec[4]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 4*(MH^2 - U)*Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]) - 8*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           Pair[ec[5], k[2]]))*PVD[0, 1, 0, 0, 0, U, 0, S34, MH^2, 
        MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[ec[4], k[3]]*(2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-4*S34*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[3]] + Pair[e[1], ec[5]]*
           (Pair[e[2], k[5]]*(-2*S34*Pair[ec[4], k[1]] + (-3*MH^2 + 8*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] - 2*S34*Pair[ec[4], k[5]]) + 
            Pair[e[2], k[1]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 - 8*S - 
                5*(T24 + U))*Pair[ec[4], k[3]] + 2*S34*Pair[ec[4], k[5]])) - 
          4*S34*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[5], k[1]] + 2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          16*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[3]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 0, 2, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*(-MH^2 + T24 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           ((-3*MH^2 + 4*S + 3*(T24 + U))*Pair[e[2], k[1]] + 
            (MH^2 - 4*S - T24 - U)*Pair[e[2], k[5]]) + 2*(-MH^2 + T24 + U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - 
            Pair[e[2], k[5]])*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (-2*(MH^2 - T24 - U)*Pair[e[1], k[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] - Pair[e[1], ec[5]]*
           (-(Pair[e[2], k[5]]*(4*S*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                 3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*
                Pair[ec[4], k[5]])) + Pair[e[2], k[1]]*
             (2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], k[2]] + (-3*MH^2 + 4*S + 
                3*(T24 + U))*Pair[ec[4], k[3]] + 2*(-MH^2 + T24 + U)*Pair[
                ec[4], k[5]])) - 2*(MH^2 - T24 - U)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          2*(-MH^2 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[1]] + 8*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*(2*T24*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*(T24*Pair[e[2], k[1]] + 
            2*(-MH^2 + 2*S + T24 + U)*Pair[e[2], k[4]] + 
            T24*Pair[e[2], k[5]]) + 2*T24*Pair[e[1], e[2]]*
           Pair[ec[5], k[1]] + 8*Pair[e[2], k[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (2*(-MH^2 + T24 + U)*Pair[e[1], k[5]]*
           Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + U)*
           Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          Pair[e[1], ec[5]]*(-((MH^2 - U)*(2*Pair[e[2], k[1]] - Pair[e[2], 
                k[4]])*Pair[ec[4], k[2]]) + Pair[e[2], k[3]]*
             (2*(-MH^2 + T24 + U)*Pair[ec[4], k[1]] + (-2*MH^2 + 4*S + 
                3*T24 + 2*U)*Pair[ec[4], k[2]] + (-MH^2 + 4*S + T24 + U)*Pair[
                ec[4], k[3]])) + 2*(-MH^2 + T24 + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 2*(-MH^2 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          8*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[3]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) - 
      8*((Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]])) - Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]]*Pair[ec[4], k[1]] - Pair[e[2], k[5]]*
             Pair[ec[4], k[5]]) + (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]])*PVC[0, 0, 1, U, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[2]]*((-MH^2 + 2*S + T24 + U)*
           Pair[e[1], ec[5]] - 4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
         ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
          4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*PVD[0, 2, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-(Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
              (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
             2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
                Pair[ec[5], k[1]]))) + Pair[e[2], ec[4]]*
           ((-MH^2 + 2*S + T24 + U)*Pair[e[1], ec[5]] - 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
             Pair[e[2], ec[4]] + Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
             ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]])) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 2*Pair[ec[4], k[3]]))*
             Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         PVD[1, 0, 0, 0, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*(Pair[ec[4], k[3]]*((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 0, 3, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 0, 0, 3, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[2], k[3]] + 
            (-2*S + S34 + 2*T14 - U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[
                ec[5], k[4]] + Pair[e[2], k[4]]*(Pair[ec[5], k[3]] + 
                2*Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 2, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (-(Pair[e[1], ec[5]]*((4*S - S34 - 2*T + 2*T24 + U)*Pair[e[2], k[
                3]] + (MH^2 + 2*S - S34 - 2*T + T24)*Pair[e[2], k[4]])) + 
          4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[4]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
              Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
         PVD[0, 0, 1, 2, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]*((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*(Pair[e[1], ec[5]]*
           ((-7*MH^2 + 4*S34 + 6*T + 2*T14 + T24 + 3*U)*Pair[e[2], k[3]] + 
            (-2*MH^2 + S34 + 2*T + U)*Pair[e[2], k[4]]) + 
          4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[4]])*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[5], k[3]] + 
                Pair[ec[5], k[4]]))))*PVD[0, 0, 2, 1, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         ((-2*MH^2 + S34 + 2*T + U)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[3]]))*PVD[0, 0, 3, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[3]]) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[ec[4], k[1]] + 2*Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 1, 0, 2, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*((-MH^2 + S34 + 2*T14 + T24)*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]] - Pair[e[2], k[4]]*
             ((2*S - S34 - 2*T14 + U)*Pair[ec[4], k[2]] + (-MH^2 + 2*S + 
                T24 + U)*Pair[ec[4], k[3]])) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 
              2*Pair[e[2], k[4]])*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]] + 2*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, T24, 
          MH^2, S34, 0, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        (-(Pair[e[1], ec[5]]*(Pair[e[2], k[4]]*((2*MH^2 - S34 - 2*T - U)*
                Pair[ec[4], k[2]] + (-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]) + Pair[e[2], k[3]]*((MH^2 + 2*S - S34 - 2*T + T24)*
                Pair[ec[4], k[2]] + 2*(-MH^2 + 2*S + T24 + U)*Pair[ec[4], 
                 k[3]]))) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[3]]*
             (2*Pair[e[2], k[3]] + Pair[e[2], k[4]])*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[2]]*
             Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            2*Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[5]]*Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[5]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]))*PVD[0, 1, 1, 1, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
         ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
          4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[2], k[3]]*(Pair[e[1], ec[5]]*
           ((-2*MH^2 + S34 + 2*T + U)*Pair[ec[4], k[2]] + 
            (MH^2 - 2*S - T24 - U)*Pair[ec[4], k[3]]) + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[2]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))*PVD[0, 1, 2, 0, 0, U, 0, 
          S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[2]]*((MH^2 - 2*S - T24 - U)*
           Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
          4*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))*(PVD[0, 2, 0, 1, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + (-(Pair[e[1], ec[5]]*((-MH^2 + 2*S + T24 + U)*
              Pair[e[2], ec[4]] + 2*(Pair[e[2], k[1]]*(2*Pair[ec[4], k[1]] + 
                 Pair[ec[4], k[2]]) + Pair[e[2], k[5]]*(Pair[ec[4], k[2]] - 
                 2*Pair[ec[4], k[5]])))) + 
          4*(Pair[e[1], k[5]]*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
            Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]])))*
         (PVD[1, 0, 0, 1, 0, T24, MH^2, S34, 0, MH^2 - S - T - T14, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 0, 1, 0, U, 0, S34, MH^2, MH^2 - S - T - T14, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*Pair[e[2], k[4]]*(Pair[e[1], ec[5]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[5]]) - 2*(Pair[e[1], k[5]]*Pair[ec[4], ec[5]] + 
              Pair[e[1], ec[4]]*Pair[ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((-MH^2 + S34 + 2*T14 + T24)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, T24, MH^2, S34, 0, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (Pair[e[1], ec[5]]*((-2*MH^2 + S34 + 2*T + U)*
             Pair[e[2], ec[4]] - 2*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
               Pair[ec[4], k[3]] + Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
                Pair[ec[4], k[5]]))) + 
          4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*Pair[ec[4], k[3]]) + 
            (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[3]])*Pair[ec[5], k[1]] + Pair[e[2], ec[4]]*
             (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[3]])))*PVD[1, 0, 1, 0, 0, U, 0, S34, MH^2, 
          MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + (-2*Pair[ec[4], k[2]]*(Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] + Pair[e[2], k[5]]) - 
            2*(Pair[e[1], k[5]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[1]])) + Pair[e[2], ec[4]]*
           ((MH^2 - 2*S - T24 - U)*Pair[e[1], ec[5]] + 
            4*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]])))*(PVD[1, 1, 0, 0, 0, T24, MH^2, S34, 0, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, U, 0, S34, MH^2, 
           MH^2 - S - T - T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]])))/(MH^2 - S - T - T14) + 
    ((4*Pair[e[2], k[5]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[ec[4], k[1]]) - 
        2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[5]]*Pair[ec[4], k[1]] + (2*Pair[e[1], k[3]] + 
            Pair[e[1], k[4]])*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) + 
        4*((2*Pair[e[1], k[3]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
       PVC[0, 0, 0, 0, MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] - (2*Pair[e[2], ec[5]]*(-2*Pair[e[1], k[5]]*
           Pair[ec[4], k[2]] + 2*Pair[e[1], k[2]]*Pair[ec[4], k[5]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
        4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
          ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
           Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
         (-((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[2], ec[5]]) + 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))*
       PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 4*(2*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
         Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
         (2*Pair[e[1], k[2]]*Pair[ec[4], k[2]] - 2*Pair[e[1], k[5]]*
           Pair[ec[4], k[5]] - Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[5]])) + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[3]])*Pair[ec[5], k[2]])*PVC[0, 0, 1, 0, 
        MH^2 - S - T24 - U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      2*(-2*Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])))*PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 4*Pair[e[1], k[4]]*
       (2*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[2], ec[5]]*
         (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]])*PVC[0, 1, 0, 0, MH^2 - S - T24 - U, T, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      2*(-2*Pair[ec[4], k[3]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])))*PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      (Pair[e[1], ec[4]]*((S*S34 + S34*T14 - MH^2*(S + T14 - T24) - T*T24 + 
            T14*U)*Pair[e[2], ec[5]] - 
          2*(-(((MH^2 - S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
               (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]]) - 
            Pair[e[2], k[5]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) - 
        2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
            T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
           (Pair[e[1], k[3]]*(T24*Pair[ec[4], k[1]] - T14*Pair[ec[4], 
                k[2]]) + Pair[e[1], k[4]]*((MH^2 - S34 - U)*Pair[ec[4], 
                k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], 
                k[3]]) + Pair[e[1], k[2]]*((-MH^2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]])) + (T14*Pair[e[1], k[3]] + 
            (MH^2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] - Pair[e[1], e[2]]*
           ((-MH^2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
           Pair[ec[5], k[2]] + 2*(-(Pair[e[1], k[4]]*
              ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                  Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                  Pair[ec[5], k[3]]))) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]]))))*PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[4]]*((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + 
            T*T24 - T14*U)*Pair[e[2], ec[5]] - 
          2*(((MH^2 + S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
              (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) + 
        2*((T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
            (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[4]]*((-MH^2 + U)*Pair[ec[4], k[1]] + 
              (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[3]]*(-((S34 + T24)*Pair[ec[4], k[1]]) + 
              T14*Pair[ec[4], k[2]] + 2*S*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
              (-2*MH^2 + 2*T + T14)*Pair[ec[4], k[3]])) + 
          (T14*Pair[e[1], k[3]] + (MH^2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + (-2*MH^2 + 2*T + T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
                Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             ((Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 2*Pair[e[2], k[1]]*
                 Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                 Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 0, MH^2, 0, 
        MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + (T14*(-MH^2 + 2*S + T + T14)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - 2*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]] + 
          2*S*Pair[e[1], k[3]] + (MH^2 - T - T14)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(-MH^2 + T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
        4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] - 4*T14*Pair[e[1], ec[4]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]) + 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((MH^2 - T)*(MH^2 - 2*S - T - T14)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + 2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[1]] + 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]) - 8*Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*PVD[0, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
        0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      ((-MH^4 + S34*(-T + T14) - 2*T*T24 + MH^2*(S34 + T - T14 + 2*T24) + 
          2*T14*U)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
        4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + (S34 - T)*Pair[e[1], k[4]])*
         Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
        2*Pair[e[2], ec[5]]*(2*(-MH^2 + T + T14)*Pair[e[1], k[2]]*
           Pair[ec[4], k[3]] + 2*Pair[e[1], k[3]]*(-(T24*Pair[ec[4], k[1]]) + 
            (-MH^2 + S34 + T14)*Pair[ec[4], k[2]] + (S + T24)*
             Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*
           (2*(-MH^2 + S34 + U)*Pair[ec[4], k[1]] + 2*(S34 - T)*
             Pair[ec[4], k[2]] + (-MH^2 + 2*S + T + T14 + 2*T24)*
             Pair[ec[4], k[3]])) - 4*((-MH^2 + S34 + T14)*Pair[e[1], k[3]] + 
          (S34 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
        4*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
            (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[5], k[2]])) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
           (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]])) + 4*Pair[e[1], ec[4]]*
         (T14*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + (MH^2 - T)*
           Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
           (T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])))*
       PVD[0, 0, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      (2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(4*S34*Pair[ec[4], k[1]] + 
            6*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) - 2*Pair[e[1], k[3]]*
           ((2*S34 + T24)*Pair[ec[4], k[1]] + (MH^2 + S34 - T14)*
             Pair[ec[4], k[2]] + (-9*MH^2 + 4*S34 + 4*T + 4*T14 + 4*T24 + 
              5*U)*Pair[ec[4], k[3]]) - Pair[e[1], k[4]]*
           (2*(MH^2 + S34 - U)*Pair[ec[4], k[1]] + 2*(S34 + T)*
             Pair[ec[4], k[2]] + (-17*MH^2 + 8*S34 + 7*T + 7*T14 + 8*T24 + 
              10*U)*Pair[ec[4], k[3]])) - 
        4*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
             Pair[e[1], k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[5]]*(2*S34*Pair[ec[4], k[1]] + 
            3*(-MH^2 + T + T14)*Pair[ec[4], k[3]]) + 
          (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) - (S34 + T)*
             Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          Pair[e[1], e[2]]*(2*S34*Pair[ec[4], k[1]] + 3*(-MH^2 + T + T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          2*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             ((4*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
                 (-Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*Pair[ec[5], 
                k[2]] + Pair[e[2], k[5]]*(-(Pair[ec[4], k[1]]*Pair[ec[5], 
                   k[3]]) + Pair[ec[4], k[3]]*(4*Pair[ec[5], k[1]] + 
                  Pair[ec[5], k[3]]))))) + Pair[e[1], ec[4]]*
         (-((MH^4 + 4*S*S34 + 3*S34*T + S34*T14 + 2*T*T24 - 
             MH^2*(3*S34 + T - T14 + 2*T24) - 2*T14*U)*Pair[e[2], ec[5]]) + 
          4*((2*S34*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
              (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*(2*S34*Pair[ec[5], k[1]] + T14*Pair[ec[5], 
                k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))))*
       PVD[0, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[ec[4], k[1]]*(-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[
                2]] + (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[5]]) + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*
           Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] - 
          Pair[e[2], ec[5]]*(-(Pair[e[1], k[5]]*(4*S*Pair[ec[4], k[1]] + 
               (-3*MH^2 + 4*S + 3*(T + T14))*Pair[ec[4], k[3]] + 2*
                (-MH^2 + T + T14)*Pair[ec[4], k[5]])) + Pair[e[1], k[2]]*
             (2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[1]] + (-3*MH^2 + 4*S + 
                3*(T + T14))*Pair[ec[4], k[3]] + 2*(-MH^2 + T + T14)*Pair[
                ec[4], k[5]])) - 2*(MH^2 - T - T14)*(Pair[e[1], k[2]] - 
            Pair[e[1], k[5]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 1, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*Pair[ec[4], k[1]]*((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - 
          Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
        4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
         (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[5], k[2]]))*(PVD[0, 0, 1, 2, 0, MH^2, MH^2 - S - T24 - U, 
         0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        PVD[0, 0, 1, 2, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(Pair[ec[4], k[3]]*(-(((-3*MH^2 + 4*S + 3*(T + T14))*Pair[e[1], k[
                2]] + (MH^2 - 4*S - T - T14)*Pair[e[1], k[5]])*
            Pair[e[2], ec[5]]) + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-4*S34*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + T + T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]] - Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*(2*S34*Pair[ec[4], k[1]] + (5*MH^2 + 3*S - 
                7*S34 - 5*T24 - 5*U)*Pair[ec[4], k[3]] - 4*S34*Pair[ec[4], 
                k[5]]) + Pair[e[1], k[5]]*(-2*S34*Pair[ec[4], k[1]] + 
              (-3*MH^2 - 5*S + 5*S34 + 3*T24 + 3*U)*Pair[ec[4], k[3]] + 
              4*S34*Pair[ec[4], k[5]])) - 4*S34*(Pair[e[1], k[2]] - 
            Pair[e[1], k[5]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(-MH^2 + T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[2]] + 16*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[ec[4], k[3]]*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]]))*PVD[0, 0, 2, 0, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]]) + 
      8*(((MH^2 - 2*S - T - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*(2*Pair[ec[4], k[1]] + 
            Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 2, 1, 0, MH^2, 
           MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 2, 1, MH^2, 0, 
           MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]]) + Pair[ec[4], k[3]]*((MH^2 - 2*S - T - T14)*
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*(PVD[0, 0, 3, 0, 0, MH^2, 
           MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 3, 0, MH^2, 0, 
           MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]])) - 
      2*(Pair[e[1], k[4]]*(2*(MH^2 - S34)*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
           ((-MH^2 + S34)*Pair[ec[4], k[1]] - 2*(MH^2 - S34)*
             Pair[ec[4], k[2]] + (T14 + 2*T24)*Pair[ec[4], k[3]]) + 
          2*(MH^2 - S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          4*Pair[ec[4], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[4]]*(-2*(MH^2 - U)*Pair[ec[4], 
                k[1]] + 2*(MH^2 - T)*Pair[ec[4], k[2]] + (MH^2 + 2*S - T)*
               Pair[ec[4], k[3]]) + 2*Pair[e[1], k[2]]*
             ((MH^2 + S34)*Pair[ec[4], k[1]] + (-3*MH^2 + 3*T + T14)*Pair[
                ec[4], k[3]]) - Pair[e[1], k[3]]*((MH^2 + 3*S34 + 2*T24)*Pair[
                ec[4], k[1]] + 2*(MH^2 + S34 - T14)*Pair[ec[4], k[2]] + 
              2*(-3*MH^2 - 3*S + S34 + T + T24 + 2*U)*Pair[ec[4], k[3]])) + 
          Pair[e[1], ec[4]]*(-((S*S34 + S34*T + MH^2*(S - S34 + T14 - T24) + 
               T*T24 - T14*U)*Pair[e[2], ec[5]]) + 
            2*(((MH^2 + S34)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
                (MH^2 - T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[5], k[1]] + 
                T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]]))) - 
          2*((-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*Pair[e[1], 
                k[4]])*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            Pair[e[1], ec[5]]*Pair[e[2], k[5]]*((MH^2 + S34)*Pair[ec[4], 
                k[1]] + (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]]) + 
            (-((MH^2 + S34 - T14)*Pair[e[1], k[3]]) + (MH^2 - T)*Pair[e[1], 
                k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
            Pair[e[1], e[2]]*((MH^2 + S34)*Pair[ec[4], k[1]] + 
              (-3*MH^2 + 3*T + T14)*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
            2*(Pair[e[1], k[4]]*((-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
                  Pair[e[2], k[1]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
                Pair[e[2], k[5]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
                  Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                ((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], 
                    k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                     k[3]]))*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 (Pair[ec[4], k[1]]*Pair[ec[5], k[4]] + Pair[ec[4], k[3]]*
                   (3*Pair[ec[5], k[1]] + 2*Pair[ec[5], k[3]] + Pair[ec[5], 
                     k[4]]))))))*PVD[0, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(Pair[ec[4], k[1]]*(-((T14*Pair[e[1], k[2]] + 
             2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + 
             T14*Pair[e[1], k[5]])*Pair[e[2], ec[5]]) + 
          2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + T)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*
           (2*(-MH^2 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            (MH^2 - T)*Pair[e[1], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[3]]*((-2*MH^2 + 4*S + 2*T + 3*T14)*Pair[ec[4], 
                k[1]] + 2*(-MH^2 + T + T14)*Pair[ec[4], k[2]] + 
              (-MH^2 + 4*S + T + T14)*Pair[ec[4], k[3]])) + 
          2*(-MH^2 + T + T14)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*(-MH^2 + T)*Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 8*Pair[e[1], k[3]]*
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
           (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
      8*(Pair[e[1], k[4]]*Pair[ec[4], k[1]]*((-MH^2 + 2*S + T + T14)*
           Pair[e[2], ec[5]] - 4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))*PVD[0, 1, 0, 2, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*Pair[ec[4], k[3]]*(-((T14*Pair[e[1], k[2]] + 
           2*(-MH^2 + 2*S + T + T14)*Pair[e[1], k[4]] + T14*Pair[e[1], k[5]])*
          Pair[e[2], ec[5]]) + 2*T14*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + 8*Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
        0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + (MH^2 + S34)*Pair[e[1], k[4]])*
         Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*(-2*MH^2 + 2*T + T14)*
         Pair[e[1], ec[5]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[5]]*(2*(-2*MH^2 + 2*T + T14)*Pair[e[1], k[5]]*
           Pair[ec[4], k[3]] + Pair[e[1], k[4]]*
           ((MH^2 + S34)*Pair[ec[4], k[1]] + (7*MH^2 - 7*S34 - 2*T - T14 - 
              4*T24 - 8*U)*Pair[ec[4], k[3]] - 2*(MH^2 + S34)*
             Pair[ec[4], k[5]]) + Pair[e[1], k[3]]*
           ((MH^2 + 3*S34)*Pair[ec[4], k[1]] + (19*MH^2 - 17*S34 - 6*T - 
              5*T14 - 12*T24 - 16*U)*Pair[ec[4], k[3]] - 2*(MH^2 + 3*S34)*
             Pair[ec[4], k[5]])) + 2*((MH^2 + 3*S34)*Pair[e[1], k[3]] + 
          (MH^2 + S34)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] - 2*(-2*MH^2 + 2*T + T14)*Pair[e[1], e[2]]*
         Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
         (Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
            3*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (4*Pair[ec[5], k[3]] + 3*Pair[ec[5], k[4]]))))*
       PVD[0, 1, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      8*((Pair[e[2], ec[5]]*((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] - Pair[e[1], k[4]]*((2*S - S34 + T - 2*T24)*
               Pair[ec[4], k[1]] + (-MH^2 + 2*S + T + T14)*Pair[ec[4], 
                k[3]])) + 4*(Pair[e[1], k[4]]*(2*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*(Pair[e[2], k[5]]*Pair[
                ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[1]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[4]]*((-2*MH^2 + S34 + T + 2*U)*Pair[
                ec[4], k[1]] + (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) - 
            Pair[e[1], k[3]]*((MH^2 + 2*S - S34 + T14 - 2*U)*Pair[ec[4], 
                k[1]] + 2*(-MH^2 + 2*S + T + T14)*Pair[ec[4], k[3]])) + 
          4*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*
             (Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             ((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                 Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
                 Pair[ec[5], k[3]]))))*PVD[0, 1, 1, 1, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-MH^2 + S34 + T14 + 2*T24)*Pair[e[1], k[3]] + 
            (-2*S + S34 - T + 2*T24)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*
             (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 2*Pair[e[2], k[4]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[3]] + 
                2*Pair[ec[5], k[4]]))))*PVD[0, 1, 2, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-4*S + S34 - T - 2*T14 + 2*U)*Pair[e[1], k[3]] - 
            (MH^2 + 2*S - S34 + T14 - 2*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[5]] + 4*(Pair[e[1], k[4]]*(2*Pair[e[2], k[3]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*(2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
              2*Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(
                3*Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))*
         PVD[0, 1, 2, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      4*(-(Pair[e[2], ec[5]]*(-((MH^2 - T)*(2*Pair[e[1], k[2]] - 
              Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - Pair[e[1], k[3]]*
            ((MH^2 + S34)*Pair[ec[4], k[1]] + 2*(MH^2 + S34)*Pair[ec[4], k[
                2]] + (-8*MH^2 - 2*S + 3*S34 + 3*T + 2*T24 + 6*U)*
              Pair[ec[4], k[3]]))) - 2*((MH^2 - T)*Pair[ec[4], k[3]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*
           ((MH^2 + S34)*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            (MH^2 + S34)*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
            4*Pair[ec[4], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]])))))*PVD[0, 2, 0, 0, MH^2, 0, 
        MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + 8*(Pair[e[1], k[4]]*Pair[ec[4], k[1]]*
         ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[4]]))*PVD[0, 2, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[3]]*(Pair[e[2], ec[5]]*((-2*MH^2 + S34 + T + 2*U)*
             Pair[ec[4], k[1]] + (MH^2 - 2*S - T - T14)*Pair[ec[4], k[3]]) + 
          4*((Pair[e[2], k[3]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             (Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[
                ec[5], k[3]])))*PVD[0, 2, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[3]]*((-MH^2 + S34 + T14 + 2*T24)*
           Pair[e[2], ec[5]] + 4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
            Pair[e[2], k[5]]*Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
          MH^2 - S - T24 - U, 0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + Pair[ec[4], k[3]]*
         (((-7*MH^2 + 4*S34 + 3*T + T14 + 2*T24 + 6*U)*Pair[e[1], k[3]] + 
            (-2*MH^2 + S34 + T + 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          4*(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
             (3*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[5], k[2]] + Pair[e[2], k[5]]*(3*Pair[ec[5], k[3]] + 
                Pair[ec[5], k[4]]))))*PVD[0, 2, 1, 0, MH^2, 0, 
          MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + Pair[e[1], k[3]]*Pair[ec[4], k[3]]*
         ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
             Pair[ec[5], k[3]]))*PVD[0, 3, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
      8*((-(Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
              Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
               Pair[e[1], e[2]]*Pair[ec[5], k[2]]))) + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*(2*Pair[ec[4], k[2]] + 
              Pair[ec[4], k[3]]) + Pair[e[1], k[2]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]])) + 
          2*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
             Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
             (Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]) + 
            ((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
              Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))*
             Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
           ((-MH^2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*PVD[1, 0, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      8*((-2*Pair[ec[4], k[1]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
             Pair[e[2], ec[5]] - 2*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
              Pair[e[1], e[2]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
           ((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(PVD[1, 0, 0, 1, 0, MH^2, MH^2 - S - T24 - U, 
           0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 0, 1, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (4*Pair[e[2], k[5]]*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[ec[4], k[3]]) - 
          2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 
              2*Pair[ec[4], k[2]]) + Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 
              2*Pair[ec[4], k[5]])) + 
          4*((Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
          Pair[e[1], ec[4]]*((MH^2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]])))*(PVD[1, 0, 1, 0, 0, MH^2, MH^2 - S - T24 - U, 
           0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
          PVD[1, 0, 1, 0, MH^2, 0, MH^2 - S - T24 - U, 0, S34, T, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
        (-2*Pair[e[1], k[4]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[2]] + 
              Pair[ec[4], k[5]]) - 2*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
           ((-MH^2 + S34 + T14 + 2*T24)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))*PVD[1, 1, 0, 0, 0, MH^2, MH^2 - S - T24 - U, 
          0, S34, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        (-2*Pair[e[2], ec[5]]*((Pair[e[1], k[2]] + Pair[e[1], k[5]])*
             Pair[ec[4], k[3]] + Pair[e[1], k[3]]*(Pair[ec[4], k[2]] + 
              Pair[ec[4], k[5]])) + 4*Pair[ec[4], k[3]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*
             Pair[ec[5], k[2]]) + 4*Pair[e[1], k[3]]*
           (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
           ((-2*MH^2 + S34 + T + 2*U)*Pair[e[2], ec[5]] + 
            4*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[3]])))*PVD[1, 1, 0, 0, MH^2, 0, MH^2 - S - T24 - U, 
          0, S34, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     (MH^2 - S - T24 - U)))/(MW*SW)
