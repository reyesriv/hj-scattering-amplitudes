(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas^2*col1*EL*MT^2*
    (-((-2*(-2*(2*Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
              S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*
             (T24*Pair[e[2], k[3]] + (MH^2 - U)*Pair[e[2], k[4]]) - 
            Pair[e[1], k[2]]*((MH^2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
              2*(MH^2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
          4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
           Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
           ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
           Pair[ec[4], k[5]] - 4*(MH^2 - U)*(Pair[e[1], k[2]] - 
            Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
          4*Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
           ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
            4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
           (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]])))*PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 
        4*(-((2*Pair[e[1], k[4]]*(2*(MH^2 - U)*Pair[e[2], k[1]] + 
               (-MH^2 - 2*S + 2*T + U)*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
              ((MH^2 - 2*S34 - 2*T14 + 6*T24 + U)*Pair[e[2], k[3]] + 4*
                (MH^2 - U)*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
              ((-5*MH^2 + 6*S34 - 2*T14 - 2*T24 + 3*U)*Pair[e[2], k[3]] + 4*
                (-MH^2 + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
          4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
           Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*
           (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
             Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 4*(MH^2 - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*
           (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
             Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
           ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
            4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 16*Pair[e[2], k[3]]*
           (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]])))*PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 
        8*(Pair[e[2], k[3]]*(-(((-3*MH^2 + 4*S34 + U)*Pair[e[1], k[2]] + 
               (MH^2 - 2*S34 - 2*T14 + 2*T24 + U)*Pair[e[1], k[3]] + 2*
                (-MH^2 + 2*T + U)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]]) + 
            2*(MH^2 - 2*T - U)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 
            2*(-MH^2 + 2*T + U)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
            8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
                Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*(
                -(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                 Pair[ec[5], k[4]])))*PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
           (-(((-MH^2 + 2*S34 - 2*T14 - 2*T24 + U)*Pair[e[1], k[2]] + 
               (-MH^2 + 4*T24 + U)*Pair[e[1], k[3]] + 2*(MH^2 - 2*S - U)*
                Pair[e[1], k[4]])*Pair[ec[4], ec[5]]) + 2*(-MH^2 + 2*S + U)*
             Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - U)*
             Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
            8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
                Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*(
                -(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                 Pair[ec[5], k[4]])))*PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], 
            Sqrt[MT^2], Sqrt[MT^2]]) + 
        ((Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]) + 
            Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 3*Pair[e[2], k[3]] + 
              4*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
           Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
            Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
            Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
           ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
            4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
               Pair[ec[5], k[4]])))*(4*PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] - 
          16*PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
       U) + 
     (-2*(2*(Pair[e[1], k[3]]*((-MH^2 + S + 2*T14 + 2*T24 + U)*
              Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
             2*S*Pair[e[2], k[4]]) + 2*(MH^2 - T)*(Pair[e[1], k[4]]*
              (Pair[e[2], k[1]] - Pair[e[2], k[3]]) - Pair[e[1], k[2]]*
              Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*(MH^2 - T)*
          Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 4*((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         4*(MH^2 - T)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - T)*(MH^2 - S - 2*T24 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - T)*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 8*Pair[e[1], k[3]]*
          (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*((-(Pair[e[1], k[3]]*(4*(MH^2 - S - 2*T24 - U)*Pair[e[2], k[3]] + 
              (-7*MH^2 - 2*S + 8*S34 + 3*T + 6*U)*Pair[e[2], k[4]] + 
              (-9*MH^2 + 2*S + 8*S34 + 5*T + 2*U)*Pair[e[2], k[5]])) - 
           4*(MH^2 - T)*(Pair[e[1], k[5]]*Pair[e[2], k[4]] - 
             Pair[e[1], k[4]]*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
         2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + (MH^2 + 2*S - T - 2*U)*
            Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
         4*(MH^2 - T)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + 
           (MH^2 + 2*S - T - 2*U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
         Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
            Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
         16*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]) - (Pair[e[2], k[4]] + Pair[e[2], k[5]])*
            (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       8*(Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
             (3*MH^2 + 2*S - 4*S34 - T - 2*U)*Pair[e[2], k[4]] + 
             (5*MH^2 - 2*S - 4*S34 - 3*T - 2*U)*Pair[e[2], k[5]])*
            Pair[ec[4], ec[5]] + 2*(-MH^2 + 2*S + T)*Pair[e[2], ec[5]]*
            Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - T)*Pair[e[2], ec[4]]*
            Pair[ec[5], k[4]] + 8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
             (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
          PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
         Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
             (5*MH^2 - 4*S34 - 3*T - 4*U)*Pair[e[2], k[4]] + 
             (3*MH^2 - 4*S34 - T)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           2*(MH^2 - T - 2*U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
           2*(-MH^2 + T + 2*U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
             (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
          PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       ((4*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[3]]*
            (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 4*Pair[e[1], k[4]]*
            Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
         2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
           4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]])))*(4*PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
      T))/(MW*SW*(MH^2 - S34 - T14 - T24))) - 
 (Alfas^2*col2*EL*MT^2*
   (-((-2*(-2*(2*Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*(T24*Pair[e[2], k[3]] + 
             (MH^2 - U)*Pair[e[2], k[4]]) - Pair[e[1], k[2]]*
            ((MH^2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
             2*(MH^2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 4*(MH^2 - U)*(Pair[e[1], k[2]] - 
           Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         4*Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(-((2*Pair[e[1], k[4]]*(2*(MH^2 - U)*Pair[e[2], k[1]] + 
              (-MH^2 - 2*S + 2*T + U)*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
             ((MH^2 - 2*S34 - 2*T14 + 6*T24 + U)*Pair[e[2], k[3]] + 
              4*(MH^2 - U)*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
             ((-5*MH^2 + 6*S34 - 2*T14 - 2*T24 + 3*U)*Pair[e[2], k[3]] + 
              4*(-MH^2 + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 4*(MH^2 - U)*
          (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 16*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       8*(Pair[e[2], k[3]]*(-(((-3*MH^2 + 4*S34 + U)*Pair[e[1], k[2]] + 
              (MH^2 - 2*S34 - 2*T14 + 2*T24 + U)*Pair[e[1], k[3]] + 
              2*(-MH^2 + 2*T + U)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]]) + 
           2*(MH^2 - 2*T - U)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 
           2*(-MH^2 + 2*T + U)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
          (-(((-MH^2 + 2*S34 - 2*T14 - 2*T24 + U)*Pair[e[1], k[2]] + 
              (-MH^2 + 4*T24 + U)*Pair[e[1], k[3]] + 2*(MH^2 - 2*S - U)*Pair[
                e[1], k[4]])*Pair[ec[4], ec[5]]) + 2*(-MH^2 + 2*S + U)*
            Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - U)*
            Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       ((Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]) + 
           Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 3*Pair[e[2], k[3]] + 
             4*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
           4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]])))*(4*PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
      U) + 
    (-2*(2*(Pair[e[1], k[3]]*((-MH^2 + S + 2*T14 + 2*T24 + U)*
             Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
            2*S*Pair[e[2], k[4]]) + 2*(MH^2 - T)*
           (Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]]) - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*(MH^2 - T)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        8*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*((-(Pair[e[1], k[3]]*(4*(MH^2 - S - 2*T24 - U)*Pair[e[2], k[3]] + 
             (-7*MH^2 - 2*S + 8*S34 + 3*T + 6*U)*Pair[e[2], k[4]] + 
             (-9*MH^2 + 2*S + 8*S34 + 5*T + 2*U)*Pair[e[2], k[5]])) - 
          4*(MH^2 - T)*(Pair[e[1], k[5]]*Pair[e[2], k[4]] - 
            Pair[e[1], k[4]]*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
        2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + (MH^2 + 2*S - T - 2*U)*
           Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
        4*(MH^2 - T)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], k[5]] - 2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + 
          (MH^2 + 2*S - T - 2*U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        16*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) - (Pair[e[2], k[4]] + Pair[e[2], k[5]])*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
             Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      8*(Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (3*MH^2 + 2*S - 4*S34 - T - 2*U)*Pair[e[2], k[4]] + 
            (5*MH^2 - 2*S - 4*S34 - 3*T - 2*U)*Pair[e[2], k[5]])*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + 2*S + T)*Pair[e[2], ec[5]]*
           Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - T)*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]] + 8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (5*MH^2 - 4*S34 - 3*T - 4*U)*Pair[e[2], k[4]] + 
            (3*MH^2 - 4*S34 - T)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(MH^2 - T - 2*U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
          2*(-MH^2 + T + 2*U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
          8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      ((4*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[3]]*
           (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 4*Pair[e[1], k[4]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
         ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
          4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*(4*PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/T))/
  (MW*SW*(MH^2 - S34 - T14 - T24)) + 
 (Alfas^2*col5*EL*MT^2*
   (-((-2*(-2*(2*Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*(T24*Pair[e[2], k[3]] + 
             (MH^2 - U)*Pair[e[2], k[4]]) - Pair[e[1], k[2]]*
            ((MH^2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
             2*(MH^2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 4*(MH^2 - U)*(Pair[e[1], k[2]] - 
           Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         4*Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(-((2*Pair[e[1], k[4]]*(2*(MH^2 - U)*Pair[e[2], k[1]] + 
              (-MH^2 - 2*S + 2*T + U)*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
             ((MH^2 - 2*S34 - 2*T14 + 6*T24 + U)*Pair[e[2], k[3]] + 
              4*(MH^2 - U)*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
             ((-5*MH^2 + 6*S34 - 2*T14 - 2*T24 + 3*U)*Pair[e[2], k[3]] + 
              4*(-MH^2 + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 4*(MH^2 - U)*
          (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 16*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       8*(Pair[e[2], k[3]]*(-(((-3*MH^2 + 4*S34 + U)*Pair[e[1], k[2]] + 
              (MH^2 - 2*S34 - 2*T14 + 2*T24 + U)*Pair[e[1], k[3]] + 
              2*(-MH^2 + 2*T + U)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]]) + 
           2*(MH^2 - 2*T - U)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 
           2*(-MH^2 + 2*T + U)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
          (-(((-MH^2 + 2*S34 - 2*T14 - 2*T24 + U)*Pair[e[1], k[2]] + 
              (-MH^2 + 4*T24 + U)*Pair[e[1], k[3]] + 2*(MH^2 - 2*S - U)*Pair[
                e[1], k[4]])*Pair[ec[4], ec[5]]) + 2*(-MH^2 + 2*S + U)*
            Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - U)*
            Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       ((Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]) + 
           Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 3*Pair[e[2], k[3]] + 
             4*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
           4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]])))*(4*PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
      U) + 
    (-2*(2*(Pair[e[1], k[3]]*((-MH^2 + S + 2*T14 + 2*T24 + U)*
             Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
            2*S*Pair[e[2], k[4]]) + 2*(MH^2 - T)*
           (Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]]) - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*(MH^2 - T)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        8*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*((-(Pair[e[1], k[3]]*(4*(MH^2 - S - 2*T24 - U)*Pair[e[2], k[3]] + 
             (-7*MH^2 - 2*S + 8*S34 + 3*T + 6*U)*Pair[e[2], k[4]] + 
             (-9*MH^2 + 2*S + 8*S34 + 5*T + 2*U)*Pair[e[2], k[5]])) - 
          4*(MH^2 - T)*(Pair[e[1], k[5]]*Pair[e[2], k[4]] - 
            Pair[e[1], k[4]]*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
        2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + (MH^2 + 2*S - T - 2*U)*
           Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
        4*(MH^2 - T)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], k[5]] - 2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + 
          (MH^2 + 2*S - T - 2*U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        16*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) - (Pair[e[2], k[4]] + Pair[e[2], k[5]])*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
             Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      8*(Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (3*MH^2 + 2*S - 4*S34 - T - 2*U)*Pair[e[2], k[4]] + 
            (5*MH^2 - 2*S - 4*S34 - 3*T - 2*U)*Pair[e[2], k[5]])*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + 2*S + T)*Pair[e[2], ec[5]]*
           Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - T)*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]] + 8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (5*MH^2 - 4*S34 - 3*T - 4*U)*Pair[e[2], k[4]] + 
            (3*MH^2 - 4*S34 - T)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(MH^2 - T - 2*U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
          2*(-MH^2 + T + 2*U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
          8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      ((4*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[3]]*
           (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 4*Pair[e[1], k[4]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
         ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
          4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*(4*PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/T))/
  (MW*SW*(MH^2 - S34 - T14 - T24)) + 
 (Alfas^2*col6*EL*MT^2*
   (-((-2*(-2*(2*Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*(T24*Pair[e[2], k[3]] + 
             (MH^2 - U)*Pair[e[2], k[4]]) - Pair[e[1], k[2]]*
            ((MH^2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
             2*(MH^2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 4*(MH^2 - U)*(Pair[e[1], k[2]] - 
           Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         4*Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(-((2*Pair[e[1], k[4]]*(2*(MH^2 - U)*Pair[e[2], k[1]] + 
              (-MH^2 - 2*S + 2*T + U)*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
             ((MH^2 - 2*S34 - 2*T14 + 6*T24 + U)*Pair[e[2], k[3]] + 
              4*(MH^2 - U)*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
             ((-5*MH^2 + 6*S34 - 2*T14 - 2*T24 + 3*U)*Pair[e[2], k[3]] + 
              4*(-MH^2 + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
         4*(MH^2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 4*(MH^2 - U)*
          (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*
          (-2*(MH^2 - U)*Pair[e[2], k[1]] + (MH^2 + 2*S - 2*T - U)*
            Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH^2 - S - T - 2*T14)*(MH^2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH^2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 16*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
             Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
              Pair[ec[5], k[4]])))*PVC[0, 0, 1, 0, U, MH^2, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       8*(Pair[e[2], k[3]]*(-(((-3*MH^2 + 4*S34 + U)*Pair[e[1], k[2]] + 
              (MH^2 - 2*S34 - 2*T14 + 2*T24 + U)*Pair[e[1], k[3]] + 
              2*(-MH^2 + 2*T + U)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]]) + 
           2*(MH^2 - 2*T - U)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 
           2*(-MH^2 + 2*T + U)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 0, 2, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
          (-(((-MH^2 + 2*S34 - 2*T14 - 2*T24 + U)*Pair[e[1], k[2]] + 
              (-MH^2 + 4*T24 + U)*Pair[e[1], k[3]] + 2*(MH^2 - 2*S - U)*Pair[
                e[1], k[4]])*Pair[ec[4], ec[5]]) + 2*(-MH^2 + 2*S + U)*
            Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - U)*
            Pair[e[1], ec[4]]*Pair[ec[5], k[4]] + 
           8*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
               Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
              (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))*PVC[0, 1, 1, 0, U, MH^2, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + 
       ((Pair[e[1], k[4]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]) + 
           Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 3*Pair[e[2], k[3]] + 
             4*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 2*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
         4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]] - 2*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
           4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]])))*(4*PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
      U) + 
    (-2*(2*(Pair[e[1], k[3]]*((-MH^2 + S + 2*T14 + 2*T24 + U)*
             Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
            2*S*Pair[e[2], k[4]]) + 2*(MH^2 - T)*
           (Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]]) - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*(MH^2 - T)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        4*((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        8*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*((-(Pair[e[1], k[3]]*(4*(MH^2 - S - 2*T24 - U)*Pair[e[2], k[3]] + 
             (-7*MH^2 - 2*S + 8*S34 + 3*T + 6*U)*Pair[e[2], k[4]] + 
             (-9*MH^2 + 2*S + 8*S34 + 5*T + 2*U)*Pair[e[2], k[5]])) - 
          4*(MH^2 - T)*(Pair[e[1], k[5]]*Pair[e[2], k[4]] - 
            Pair[e[1], k[4]]*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
        2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + (MH^2 + 2*S - T - 2*U)*
           Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
        4*(MH^2 - T)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], k[5]] - 2*(-2*(MH^2 - T)*Pair[e[1], k[2]] + 
          (MH^2 + 2*S - T - 2*U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - 4*(MH^2 - T)*Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
        Pair[e[1], e[2]]*((MH^2 - T)*(MH^2 - S - 2*T24 - U)*
           Pair[ec[4], ec[5]] + 4*(MH^2 - T)*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 
        16*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) - (Pair[e[2], k[4]] + Pair[e[2], k[5]])*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
             Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      8*(Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (3*MH^2 + 2*S - 4*S34 - T - 2*U)*Pair[e[2], k[4]] + 
            (5*MH^2 - 2*S - 4*S34 - 3*T - 2*U)*Pair[e[2], k[5]])*
           Pair[ec[4], ec[5]] + 2*(-MH^2 + 2*S + T)*Pair[e[2], ec[5]]*
           Pair[ec[4], k[5]] + 2*(MH^2 - 2*S - T)*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]] + 8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 1, 1, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
        Pair[e[1], k[3]]*((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[3]] + 
            (5*MH^2 - 4*S34 - 3*T - 4*U)*Pair[e[2], k[4]] + 
            (3*MH^2 - 4*S34 - T)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          2*(MH^2 - T - 2*U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
          2*(-MH^2 + T + 2*U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
          8*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
            (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))*
         PVC[0, 2, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      ((4*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[3]]*
           (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 4*Pair[e[1], k[4]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        2*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
         ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
          4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))*(4*PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/T))/
  (MW*SW*(MH^2 - S34 - T14 - T24))
