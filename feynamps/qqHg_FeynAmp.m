(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-(Alfas*EL*GS*MT2*Den[S, 0]*Mat[SUNT[Glu4, Col2, Col1]]*
   (2*B0i[bb0, S, MT2, MT2]*(WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], 
       Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, 
       ec[4], Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 
        0], 6, ec[4], Spinor[k[1], MU, 1, 1, 0]] + 
      WeylChain[Spinor[k[2], MU, -1, 2, 0], 7, ec[4], Spinor[k[1], MU, 1, 1, 
        0]]) + C0i[cc0, MH2, S, 0, MT2, MT2, MT2]*
     ((MH2 - S)*WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], 
        Spinor[k[1], MD, 1, 1, 0]] + (MH2 - S)*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], Spinor[k[1], MD, 1, 
          1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, ec[4], 
         Spinor[k[1], MU, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         7, ec[4], Spinor[k[1], MU, 1, 1, 0]]) - 2*Pair[ec[4], k[3]]*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, k[3], Spinor[k[1], MD, 1, 1, 
          0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, k[3], 
         Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         6, k[3], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[
         Spinor[k[2], MU, -1, 2, 0], 7, k[3], Spinor[k[1], MU, 1, 1, 0]])) - 
    2*(4*C0i[cc00, MH2, S, 0, MT2, MT2, MT2]*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], Spinor[k[1], MD, 1, 
          1, 0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], 
         Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         6, ec[4], Spinor[k[1], MU, 1, 1, 0]] + 
        WeylChain[Spinor[k[2], MU, -1, 2, 0], 7, ec[4], Spinor[k[1], MU, 1, 
          1, 0]]) + 4*(C0i[cc11, MH2, S, 0, MT2, MT2, MT2] + 
        C0i[cc12, MH2, S, 0, MT2, MT2, MT2])*Pair[ec[4], k[3]]*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, k[3], Spinor[k[1], MD, 1, 1, 
          0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, k[3], 
         Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         6, k[3], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[
         Spinor[k[2], MU, -1, 2, 0], 7, k[3], Spinor[k[1], MU, 1, 1, 0]]) + 
      C0i[cc1, MH2, S, 0, MT2, MT2, MT2]*
       ((-MH2 + S)*WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], 
          Spinor[k[1], MD, 1, 1, 0]] - (MH2 - S)*
         (WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], Spinor[k[1], MD, 1, 
            1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, ec[4], 
           Spinor[k[1], MU, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
           7, ec[4], Spinor[k[1], MU, 1, 1, 0]]) + 4*Pair[ec[4], k[3]]*
         (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, k[3], Spinor[k[1], MD, 1, 
            1, 0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, k[3], 
           Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
           6, k[3], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[
           Spinor[k[2], MU, -1, 2, 0], 7, k[3], Spinor[k[1], MU, 1, 1, 
            0]])))))/(4*MW*Pi*SW)
