(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*EL*GS*MT^2*(4*Sqrt[MH^2 - 4*MT^2]*S*
        Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
       MH*(-MH^2 + 4*MT^2 + S)*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
           (2*MT^2)]^2 + MH*(4*(MH^2 - S) - 4*Sqrt[S*(-4*MT^2 + S)]*
          Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
         (MH^2 - 4*MT^2 - S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
             (2*MT^2)]^2))*Mat[SUNT[Glu4, Col2, Col1]]*
      (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], Spinor[k[1], MD, 1, 1, 
         0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], 
        Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, 
        ec[4], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 
         0], 7, ec[4], Spinor[k[1], MU, 1, 1, 0]]))/
     (8*MH*MW*Pi*(MH^2 - S)*S*SW)}}}}
