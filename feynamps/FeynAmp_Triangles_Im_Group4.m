(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {
  {
   {-((Alfas^2*col1*EL*MT^2*((-8*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
              ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
               (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*
                       (-4*MT^2 + S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*
                     Sqrt[S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
              (2*(MH^2 + S - 2*S34 - T14 - T24)*Pair[e[1], k[2]]*
                Pair[e[2], ec[4]] + 2*(-MH^2 - S + 2*S34 + T14 + T24)*
                Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
                ((-3*MH^2 + 3*S + T14 + T24 + 4*U)*Pair[ec[4], k[1]] + 
                 (3*MH^2 - 3*S - 4*T - T14 - T24)*Pair[ec[4], k[2]] + 
                 2*(-T14 + T24)*Pair[ec[4], k[3]]) + 8*(Pair[e[1], k[5]]*
                  Pair[e[2], k[1]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
                  Pair[e[2], k[1]]*Pair[ec[4], k[5]] + Pair[e[1], k[2]]*
                  (-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*
                    Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
             (I*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*(
                2*(-MH^2 - S34 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
                2*(MH^2 + S34 - T - U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
                Pair[e[1], e[2]]*(-((MH^2 + S34 + T - 3*U)*Pair[ec[4], 
                     k[1]]) + (MH^2 + S34 - 3*T + U)*Pair[ec[4], k[2]] + 
                  2*(-T14 + T24)*Pair[ec[4], k[3]]) + 8*(Pair[e[1], k[5]]*
                   Pair[e[2], k[1]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
                   Pair[e[2], k[1]]*Pair[ec[4], k[5]] + Pair[e[1], k[2]]*
                   (-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*
                     Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/
              ((MH^2 - S35)*S35)) + (((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
               HeavisideTheta[-4*MT^2 + S35])/S35 - (32*I)*Pi*
              HeavisideTheta[-4*MT^2 + S35]*(-Sqrt[S35*(-4*MT^2 + S35)]/
                (4*(MH^2 - S35)) - (MT^2*Log[((MH^2 - S35)*S35 + 
                    (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/((MH^2 - S35)*
                     S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/
                (2*(MH^2 - S35))))*((-T14 + T24)*Pair[e[1], e[2]]*
              Pair[ec[4], ec[5]] + 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + 
               Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
             4*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]]) - 4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
              (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*
              (Pair[ec[4], k[1]]*(4*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[2]]*(-4*Pair[ec[5], k[1]] + Pair[ec[5], 
                  k[3]])) - 2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
              (Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*
              Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])) + 
           ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
             (-(Pair[e[1], e[2]]*(-((MH^2 - S - T14 - T24)*(T14 - T24)*
                   Pair[ec[4], ec[5]]) + 4*(T14 - T24)*Pair[ec[4], k[3]]*
                  Pair[ec[5], k[3]] + Pair[ec[4], k[2]]*(4*(MH^2 - S - T14 - 
                     T24)*Pair[ec[5], k[1]] + (-7*MH^2 + 5*S + 8*T + 3*T14 + 
                     3*T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
                  (4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
                   (7*MH^2 - 5*S - 3*T14 - 3*T24 - 8*U)*Pair[ec[5], 
                     k[3]]))) + Pair[e[1], k[2]]*(4*(MH^2 - S34 - T - U)*
                 Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
                2*Pair[e[2], ec[4]]*((-3*MH^2 + S + 4*S34 + 3*T14 + 3*T24)*
                   Pair[ec[5], k[3]] + 2*(-MH^2 + S + T14 + T24)*Pair[ec[5], 
                    k[4]])) + 2*(-2*(MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*
                   Pair[e[2], k[1]] - Pair[e[1], k[2]]*Pair[e[2], k[4]])*
                 Pair[ec[4], ec[5]] - 2*(MH^2 - S34 - T - U)*Pair[e[1], 
                  ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                   k[2]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], 
                    k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], 
                    k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], 
                       k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*
                 Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
                 ((-3*MH^2 + S + 4*S34 + 3*T14 + 3*T24)*Pair[ec[5], k[3]] + 
                  2*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]))))/
            ((MH^2 - S35)*S35) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
               ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
             (Pair[e[1], e[2]]*((MH^2 - S - T14 - T24)*(T14 - T24)*
                 Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
                   Pair[ec[5], k[3]] - Pair[ec[4], k[1]]*(2*(MH^2 - S34 - T - 
                      U)*Pair[ec[5], k[2]] + (-2*MH^2 + 2*S34 + 2*T + T14 + 
                      T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*
                   (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                    (-2*MH^2 + 2*S34 + T14 + T24 + 2*U)*Pair[ec[5], 
                      k[3]]))) - 4*((MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*
                   Pair[e[2], k[1]] - Pair[e[1], k[2]]*Pair[e[2], k[4]])*
                 Pair[ec[4], ec[5]] + (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*
                 Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
                2*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                  Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                  Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                    Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] - 
                Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH^2 + S34 + T14 + T24)*
                   Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                    k[4]]) + Pair[e[1], k[2]]*(-((MH^2 - S34 - T - U)*
                    Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                      k[2]])) + Pair[e[2], ec[4]]*((-MH^2 + S34 + T14 + T24)*
                     Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                      k[4]])))))/(MH^2 - S35))/S35 + 
         ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
              S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
              (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - (MT^2*
                 Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                        S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                      S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
            ((-T - T14 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
             4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
                Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
              Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
             2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*
                Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 4*Pair[e[1], ec[4]]*
              Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
             Pair[e[1], e[2]]*(-4*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
               Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 4*
                Pair[ec[4], k[1]]*Pair[ec[5], k[2]])) + 
           ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
             (-(Pair[e[1], e[2]]*(-((MH^2 - S34)*(T + T14 - T24 - U)*
                   Pair[ec[4], ec[5]]) + 4*(MH^2 - S34)*Pair[ec[4], k[2]]*
                  Pair[ec[5], k[1]] + 4*(-MH^2 + S34)*Pair[ec[4], k[1]]*
                  Pair[ec[5], k[2]] - Pair[ec[4], k[3]]*((7*MH^2 - 2*S - 
                     3*S34 - 8*U)*Pair[ec[5], k[1]] + (-7*MH^2 + 2*S + 
                     3*S34 + 8*T)*Pair[ec[5], k[2]] + 4*(-2*MH^2 + S + S34 + 
                     2*T24 + 2*U)*Pair[ec[5], k[3]]))) - 
              2*(2*(MH^2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
                  Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
                Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-3*MH^2 + 2*S + 3*S34 + 
                    4*T14 + 4*T24)*Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*
                   Pair[ec[4], k[5]]) + 2*(MH^2 - S34)*Pair[e[1], ec[4]]*
                 Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
                Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-3*MH^2 + 2*S + 3*S34 + 
                      4*T14 + 4*T24)*Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*
                     Pair[ec[4], k[5]]) + 2*(MH^2 - S34)*Pair[e[2], ec[4]]*
                   (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
                8*Pair[ec[4], k[3]]*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                  Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                  Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                      Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                      k[3]])))))/((MH^2 - S34)*S34) + 
           8*((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
               Pair[ec[4], k[3]]*(2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], 
                  k[2]]*Pair[e[2], ec[5]] - 2*(-3*MH^2 + S34 + 2*(T + U))*
                 Pair[e[1], ec[5]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
                 ((-3*MH^2 + S34 + 4*U)*Pair[ec[5], k[1]] + (3*MH^2 - S34 - 
                    4*T)*Pair[ec[5], k[2]] + 2*(T + T14 - T24 - U)*
                   Pair[ec[5], k[3]]) + 8*(-(Pair[e[1], k[3]]*Pair[e[2], 
                     k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                  Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                  Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                      Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                      k[3]]))))/((MH^2 - S34)*S34) + (2*I)*Pi*
              HeavisideTheta[-4*MT^2 + S34]*((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/
                (2*(MH^2 - S34)^2*S34) + (MT^2*Log[((MH^2 - S34)*S34 + 
                    (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/((MH^2 - S34)*
                     S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
                (MH^2 - S34)^2)*Pair[ec[4], k[3]]*(-2*(-MH^2 + S34 + 
                 2*(T14 + T24))*Pair[e[1], k[2]]*Pair[e[2], ec[5]] + 2*
                (-MH^2 + S34 + 2*(T14 + T24))*Pair[e[1], ec[5]]*Pair[e[2], 
                 k[1]] + 8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                   (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                 Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                 Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                     Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                     k[3]])) - Pair[e[1], e[2]]*((-MH^2 + S34 + 
                   2*(T14 + T24))*Pair[ec[5], k[1]] + (MH^2 - S34 - 
                   2*(T14 + T24))*Pair[ec[5], k[2]] + 2*(-T14 + T24)*
                  Pair[ec[5], k[3]] + 2*(T - U)*Pair[ec[5], k[4]]))) + 
           ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*Log[((MH^2 - S34)*S34 + 
                (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/((MH^2 - S34)*S34 + 
                (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
             (-4*((MH^2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
                  Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
                Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], 
                    k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                   Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[1], ec[4]]*
                 Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
                Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*
                     Pair[ec[4], k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + 
                    (T14 + T24)*Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[2], 
                    ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
                2*Pair[ec[4], k[3]]*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                  Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                  Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                      Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                      k[3]]))) + Pair[e[1], e[2]]*((MH^2 - S34)*(T + T14 - 
                  T24 - U)*Pair[ec[4], ec[5]] + 2*(2*(-MH^2 + S34)*
                   Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 2*(MH^2 - S34)*
                   Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]] - 
                    Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*(2*(T14 + T24)*
                     Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], k[3]] + 
                    (T - T14 - T24 - U)*Pair[ec[5], k[4]])))))/(MH^2 - S34))/
          S34))/(MW*S*SW)) - (Alfas^2*col2*EL*MT^2*
       ((-8*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (2*(MH^2 + S - 2*S34 - T14 - T24)*Pair[e[1], k[2]]*Pair[e[2], 
                ec[4]] + 2*(-MH^2 - S + 2*S34 + T14 + T24)*Pair[e[1], ec[4]]*
               Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + 3*S + T14 + 
                  T24 + 4*U)*Pair[ec[4], k[1]] + (3*MH^2 - 3*S - 4*T - T14 - 
                  T24)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*Pair[ec[4], 
                  k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], 
                     k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
             Pair[ec[5], k[3]] + (I*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
              HeavisideTheta[-4*MT^2 + S35]*(2*(-MH^2 - S34 + T + U)*
                Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 2*(MH^2 + S34 - T - U)*
                Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
                (-((MH^2 + S34 + T - 3*U)*Pair[ec[4], k[1]]) + 
                 (MH^2 + S34 - 3*T + U)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*
                  Pair[ec[4], k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*
                     Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], 
                     k[5]])))*Pair[ec[5], k[3]])/((MH^2 - S35)*S35)) + 
          (((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           ((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
            4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
            4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*(Pair[ec[4], k[1]]*(
                4*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*(
                -4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
            2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 
              2*Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])) + 
          ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S - T14 - T24)*(T14 - T24)*
                  Pair[ec[4], ec[5]]) + 4*(T14 - T24)*Pair[ec[4], k[3]]*
                 Pair[ec[5], k[3]] + Pair[ec[4], k[2]]*(4*(MH^2 - S - T14 - 
                    T24)*Pair[ec[5], k[1]] + (-7*MH^2 + 5*S + 8*T + 3*T14 + 
                    3*T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
                 (4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
                  (7*MH^2 - 5*S - 3*T14 - 3*T24 - 8*U)*Pair[ec[5], k[3]]))) + 
             Pair[e[1], k[2]]*(4*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*Pair[e[2], ec[4]]*
                ((-3*MH^2 + S + 4*S34 + 3*T14 + 3*T24)*Pair[ec[5], k[3]] + 
                 2*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]])) + 
             2*(-2*(MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*
                (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 8*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-3*MH^2 + S + 4*S34 + 
                   3*T14 + 3*T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S + T14 + 
                   T24)*Pair[ec[5], k[4]]))))/((MH^2 - S35)*S35) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[1], e[2]]*((MH^2 - S - T14 - T24)*(T14 - T24)*
                Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
                  Pair[ec[5], k[3]] - Pair[ec[4], k[1]]*(2*(MH^2 - S34 - T - 
                     U)*Pair[ec[5], k[2]] + (-2*MH^2 + 2*S34 + 2*T + T14 + 
                     T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*
                  (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                   (-2*MH^2 + 2*S34 + T14 + T24 + 2*U)*Pair[ec[5], k[3]]))) - 
             4*((MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
               (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] - 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH^2 + S34 + T14 + T24)*
                  Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                   k[4]]) + Pair[e[1], k[2]]*(-((MH^2 - S34 - T - U)*
                   Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                     k[2]])) + Pair[e[2], ec[4]]*((-MH^2 + S34 + T14 + T24)*
                    Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                     k[4]])))))/(MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           ((-T - T14 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
            4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[5]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
            2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 
              2*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 4*Pair[e[1], ec[4]]*
             Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
            Pair[e[1], e[2]]*(-4*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
              Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
              4*Pair[ec[4], k[1]]*Pair[ec[5], k[2]])) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S34)*(T + T14 - T24 - U)*
                  Pair[ec[4], ec[5]]) + 4*(MH^2 - S34)*Pair[ec[4], k[2]]*
                 Pair[ec[5], k[1]] + 4*(-MH^2 + S34)*Pair[ec[4], k[1]]*
                 Pair[ec[5], k[2]] - Pair[ec[4], k[3]]*((7*MH^2 - 2*S - 
                    3*S34 - 8*U)*Pair[ec[5], k[1]] + (-7*MH^2 + 2*S + 3*S34 + 
                    8*T)*Pair[ec[5], k[2]] + 4*(-2*MH^2 + S + S34 + 2*T24 + 
                    2*U)*Pair[ec[5], k[3]]))) - 2*(2*(MH^2 - S34)*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                  Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
                Pair[e[2], k[1]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                  Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 2*
                (MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
                (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - Pair[e[1], k[2]]*
                (Pair[e[2], ec[5]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                    Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 
                 2*(MH^2 - S34)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + 
                   Pair[ec[5], k[2]])) + 8*Pair[ec[4], k[3]]*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]])))))/
           ((MH^2 - S34)*S34) + 8*((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*
              HeavisideTheta[-4*MT^2 + S34]*Pair[ec[4], k[3]]*
              (2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], k[2]]*Pair[e[2], 
                 ec[5]] - 2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], ec[5]]*
                Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + S34 + 4*U)*
                  Pair[ec[5], k[1]] + (3*MH^2 - S34 - 4*T)*Pair[ec[5], 
                   k[2]] + 2*(T + T14 - T24 - U)*Pair[ec[5], k[3]]) + 8*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))))/((MH^2 - S34)*
              S34) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             Pair[ec[4], k[3]]*(-2*(-MH^2 + S34 + 2*(T14 + T24))*Pair[e[1], 
                k[2]]*Pair[e[2], ec[5]] + 2*(-MH^2 + S34 + 2*(T14 + T24))*
               Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 8*(-(Pair[e[1], k[3]]*
                  Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                    k[3]])) - Pair[e[1], e[2]]*((-MH^2 + S34 + 2*(T14 + T24))*
                 Pair[ec[5], k[1]] + (MH^2 - S34 - 2*(T14 + T24))*
                 Pair[ec[5], k[2]] + 2*(-T14 + T24)*Pair[ec[5], k[3]] + 
                2*(T - U)*Pair[ec[5], k[4]]))) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (-4*((MH^2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
               Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], 
                   k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                  Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[1], ec[4]]*
                Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
               Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], 
                     k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                    Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[2], ec[4]]*
                  (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 2*Pair[ec[4], 
                 k[3]]*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], 
                     k[1]] + Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*
                  Pair[e[2], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
                  (Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + Pair[e[1], e[2]]*
              ((MH^2 - S34)*(T + T14 - T24 - U)*Pair[ec[4], ec[5]] + 2*
                (2*(-MH^2 + S34)*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
                 2*(MH^2 - S34)*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
                   Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
                  (2*(T14 + T24)*Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], 
                     k[3]] + (T - T14 - T24 - U)*Pair[ec[5], k[4]])))))/
           (MH^2 - S34))/S34))/(MW*S*SW) + 
     (Alfas^2*col5*EL*MT^2*
       ((-8*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (2*(MH^2 + S - 2*S34 - T14 - T24)*Pair[e[1], k[2]]*Pair[e[2], 
                ec[4]] + 2*(-MH^2 - S + 2*S34 + T14 + T24)*Pair[e[1], ec[4]]*
               Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + 3*S + T14 + 
                  T24 + 4*U)*Pair[ec[4], k[1]] + (3*MH^2 - 3*S - 4*T - T14 - 
                  T24)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*Pair[ec[4], 
                  k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], 
                     k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
             Pair[ec[5], k[3]] + (I*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
              HeavisideTheta[-4*MT^2 + S35]*(2*(-MH^2 - S34 + T + U)*
                Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 2*(MH^2 + S34 - T - U)*
                Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
                (-((MH^2 + S34 + T - 3*U)*Pair[ec[4], k[1]]) + 
                 (MH^2 + S34 - 3*T + U)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*
                  Pair[ec[4], k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*
                     Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], 
                     k[5]])))*Pair[ec[5], k[3]])/((MH^2 - S35)*S35)) + 
          (((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           ((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
            4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
            4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*(Pair[ec[4], k[1]]*(
                4*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*(
                -4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
            2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 
              2*Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])) + 
          ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S - T14 - T24)*(T14 - T24)*
                  Pair[ec[4], ec[5]]) + 4*(T14 - T24)*Pair[ec[4], k[3]]*
                 Pair[ec[5], k[3]] + Pair[ec[4], k[2]]*(4*(MH^2 - S - T14 - 
                    T24)*Pair[ec[5], k[1]] + (-7*MH^2 + 5*S + 8*T + 3*T14 + 
                    3*T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
                 (4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
                  (7*MH^2 - 5*S - 3*T14 - 3*T24 - 8*U)*Pair[ec[5], k[3]]))) + 
             Pair[e[1], k[2]]*(4*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*Pair[e[2], ec[4]]*
                ((-3*MH^2 + S + 4*S34 + 3*T14 + 3*T24)*Pair[ec[5], k[3]] + 
                 2*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]])) + 
             2*(-2*(MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*
                (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 8*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-3*MH^2 + S + 4*S34 + 
                   3*T14 + 3*T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S + T14 + 
                   T24)*Pair[ec[5], k[4]]))))/((MH^2 - S35)*S35) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[1], e[2]]*((MH^2 - S - T14 - T24)*(T14 - T24)*
                Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
                  Pair[ec[5], k[3]] - Pair[ec[4], k[1]]*(2*(MH^2 - S34 - T - 
                     U)*Pair[ec[5], k[2]] + (-2*MH^2 + 2*S34 + 2*T + T14 + 
                     T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*
                  (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                   (-2*MH^2 + 2*S34 + T14 + T24 + 2*U)*Pair[ec[5], k[3]]))) - 
             4*((MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
               (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] - 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH^2 + S34 + T14 + T24)*
                  Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                   k[4]]) + Pair[e[1], k[2]]*(-((MH^2 - S34 - T - U)*
                   Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                     k[2]])) + Pair[e[2], ec[4]]*((-MH^2 + S34 + T14 + T24)*
                    Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                     k[4]])))))/(MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           ((-T - T14 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
            4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[5]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
            2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 
              2*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 4*Pair[e[1], ec[4]]*
             Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
            Pair[e[1], e[2]]*(-4*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
              Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
              4*Pair[ec[4], k[1]]*Pair[ec[5], k[2]])) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S34)*(T + T14 - T24 - U)*
                  Pair[ec[4], ec[5]]) + 4*(MH^2 - S34)*Pair[ec[4], k[2]]*
                 Pair[ec[5], k[1]] + 4*(-MH^2 + S34)*Pair[ec[4], k[1]]*
                 Pair[ec[5], k[2]] - Pair[ec[4], k[3]]*((7*MH^2 - 2*S - 
                    3*S34 - 8*U)*Pair[ec[5], k[1]] + (-7*MH^2 + 2*S + 3*S34 + 
                    8*T)*Pair[ec[5], k[2]] + 4*(-2*MH^2 + S + S34 + 2*T24 + 
                    2*U)*Pair[ec[5], k[3]]))) - 2*(2*(MH^2 - S34)*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                  Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
                Pair[e[2], k[1]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                  Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 2*
                (MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
                (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - Pair[e[1], k[2]]*
                (Pair[e[2], ec[5]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                    Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 
                 2*(MH^2 - S34)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + 
                   Pair[ec[5], k[2]])) + 8*Pair[ec[4], k[3]]*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]])))))/
           ((MH^2 - S34)*S34) + 8*((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*
              HeavisideTheta[-4*MT^2 + S34]*Pair[ec[4], k[3]]*
              (2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], k[2]]*Pair[e[2], 
                 ec[5]] - 2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], ec[5]]*
                Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + S34 + 4*U)*
                  Pair[ec[5], k[1]] + (3*MH^2 - S34 - 4*T)*Pair[ec[5], 
                   k[2]] + 2*(T + T14 - T24 - U)*Pair[ec[5], k[3]]) + 8*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))))/((MH^2 - S34)*
              S34) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             Pair[ec[4], k[3]]*(-2*(-MH^2 + S34 + 2*(T14 + T24))*Pair[e[1], 
                k[2]]*Pair[e[2], ec[5]] + 2*(-MH^2 + S34 + 2*(T14 + T24))*
               Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 8*(-(Pair[e[1], k[3]]*
                  Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                    k[3]])) - Pair[e[1], e[2]]*((-MH^2 + S34 + 2*(T14 + T24))*
                 Pair[ec[5], k[1]] + (MH^2 - S34 - 2*(T14 + T24))*
                 Pair[ec[5], k[2]] + 2*(-T14 + T24)*Pair[ec[5], k[3]] + 
                2*(T - U)*Pair[ec[5], k[4]]))) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (-4*((MH^2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
               Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], 
                   k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                  Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[1], ec[4]]*
                Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
               Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], 
                     k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                    Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[2], ec[4]]*
                  (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 2*Pair[ec[4], 
                 k[3]]*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], 
                     k[1]] + Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*
                  Pair[e[2], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
                  (Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + Pair[e[1], e[2]]*
              ((MH^2 - S34)*(T + T14 - T24 - U)*Pair[ec[4], ec[5]] + 2*
                (2*(-MH^2 + S34)*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
                 2*(MH^2 - S34)*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
                   Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
                  (2*(T14 + T24)*Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], 
                     k[3]] + (T - T14 - T24 - U)*Pair[ec[5], k[4]])))))/
           (MH^2 - S34))/S34))/(MW*S*SW) + 
     (Alfas^2*col6*EL*MT^2*
       ((-8*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (2*(MH^2 + S - 2*S34 - T14 - T24)*Pair[e[1], k[2]]*Pair[e[2], 
                ec[4]] + 2*(-MH^2 - S + 2*S34 + T14 + T24)*Pair[e[1], ec[4]]*
               Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + 3*S + T14 + 
                  T24 + 4*U)*Pair[ec[4], k[1]] + (3*MH^2 - 3*S - 4*T - T14 - 
                  T24)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*Pair[ec[4], 
                  k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], 
                  k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], 
                     k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
             Pair[ec[5], k[3]] + (I*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
              HeavisideTheta[-4*MT^2 + S35]*(2*(-MH^2 - S34 + T + U)*
                Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 2*(MH^2 + S34 - T - U)*
                Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
                (-((MH^2 + S34 + T - 3*U)*Pair[ec[4], k[1]]) + 
                 (MH^2 + S34 - 3*T + U)*Pair[ec[4], k[2]] + 2*(-T14 + T24)*
                  Pair[ec[4], k[3]]) + 8*(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[3]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
                  Pair[ec[4], k[5]] + Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*
                     Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*Pair[ec[4], 
                     k[5]])))*Pair[ec[5], k[3]])/((MH^2 - S35)*S35)) + 
          (((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           ((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
            4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
            4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*(Pair[ec[4], k[1]]*(
                4*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*(
                -4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
            2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 
              2*Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])) + 
          ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S - T14 - T24)*(T14 - T24)*
                  Pair[ec[4], ec[5]]) + 4*(T14 - T24)*Pair[ec[4], k[3]]*
                 Pair[ec[5], k[3]] + Pair[ec[4], k[2]]*(4*(MH^2 - S - T14 - 
                    T24)*Pair[ec[5], k[1]] + (-7*MH^2 + 5*S + 8*T + 3*T14 + 
                    3*T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
                 (4*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
                  (7*MH^2 - 5*S - 3*T14 - 3*T24 - 8*U)*Pair[ec[5], k[3]]))) + 
             Pair[e[1], k[2]]*(4*(MH^2 - S34 - T - U)*Pair[e[2], ec[5]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*Pair[e[2], ec[4]]*
                ((-3*MH^2 + S + 4*S34 + 3*T14 + 3*T24)*Pair[ec[5], k[3]] + 
                 2*(-MH^2 + S + T14 + T24)*Pair[ec[5], k[4]])) + 
             2*(-2*(MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*
                (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 8*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-3*MH^2 + S + 4*S34 + 
                   3*T14 + 3*T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S + T14 + 
                   T24)*Pair[ec[5], k[4]]))))/((MH^2 - S35)*S35) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[1], e[2]]*((MH^2 - S - T14 - T24)*(T14 - T24)*
                Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
                  Pair[ec[5], k[3]] - Pair[ec[4], k[1]]*(2*(MH^2 - S34 - T - 
                     U)*Pair[ec[5], k[2]] + (-2*MH^2 + 2*S34 + 2*T + T14 + 
                     T24)*Pair[ec[5], k[3]]) + Pair[ec[4], k[2]]*
                  (2*(MH^2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                   (-2*MH^2 + 2*S34 + T14 + T24 + 2*U)*Pair[ec[5], k[3]]))) - 
             4*((MH^2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
               (MH^2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 2*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
                 Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
                 Pair[e[1], k[2]]*(-(Pair[e[2], k[5]]*Pair[ec[4], k[3]]) + 
                   Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] - 
               Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH^2 + S34 + T14 + T24)*
                  Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                   k[4]]) + Pair[e[1], k[2]]*(-((MH^2 - S34 - T - U)*
                   Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], 
                     k[2]])) + Pair[e[2], ec[4]]*((-MH^2 + S34 + T14 + T24)*
                    Pair[ec[5], k[3]] + (MH^2 - S34 - T - U)*Pair[ec[5], 
                     k[4]])))))/(MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           ((-T - T14 + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
            4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*Pair[
                e[2], k[5]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
            2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 
              2*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 4*Pair[e[1], ec[4]]*
             Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
            Pair[e[1], e[2]]*(-4*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
              Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
              4*Pair[ec[4], k[1]]*Pair[ec[5], k[2]])) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (-(Pair[e[1], e[2]]*(-((MH^2 - S34)*(T + T14 - T24 - U)*
                  Pair[ec[4], ec[5]]) + 4*(MH^2 - S34)*Pair[ec[4], k[2]]*
                 Pair[ec[5], k[1]] + 4*(-MH^2 + S34)*Pair[ec[4], k[1]]*
                 Pair[ec[5], k[2]] - Pair[ec[4], k[3]]*((7*MH^2 - 2*S - 
                    3*S34 - 8*U)*Pair[ec[5], k[1]] + (-7*MH^2 + 2*S + 3*S34 + 
                    8*T)*Pair[ec[5], k[2]] + 4*(-2*MH^2 + S + S34 + 2*T24 + 
                    2*U)*Pair[ec[5], k[3]]))) - 2*(2*(MH^2 - S34)*
                (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                  Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
                Pair[e[2], k[1]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                  Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 2*
                (MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
                (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - Pair[e[1], k[2]]*
                (Pair[e[2], ec[5]]*((-3*MH^2 + 2*S + 3*S34 + 4*T14 + 4*T24)*
                    Pair[ec[4], k[3]] + 2*(-MH^2 + S34)*Pair[ec[4], k[5]]) + 
                 2*(MH^2 - S34)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + 
                   Pair[ec[5], k[2]])) + 8*Pair[ec[4], k[3]]*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]])))))/
           ((MH^2 - S34)*S34) + 8*((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*
              HeavisideTheta[-4*MT^2 + S34]*Pair[ec[4], k[3]]*
              (2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], k[2]]*Pair[e[2], 
                 ec[5]] - 2*(-3*MH^2 + S34 + 2*(T + U))*Pair[e[1], ec[5]]*
                Pair[e[2], k[1]] + Pair[e[1], e[2]]*((-3*MH^2 + S34 + 4*U)*
                  Pair[ec[5], k[1]] + (3*MH^2 - S34 - 4*T)*Pair[ec[5], 
                   k[2]] + 2*(T + T14 - T24 - U)*Pair[ec[5], k[3]]) + 8*
                (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
                  Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                    (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))))/((MH^2 - S34)*
              S34) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             Pair[ec[4], k[3]]*(-2*(-MH^2 + S34 + 2*(T14 + T24))*Pair[e[1], 
                k[2]]*Pair[e[2], ec[5]] + 2*(-MH^2 + S34 + 2*(T14 + T24))*
               Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 8*(-(Pair[e[1], k[3]]*
                  Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
                Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
                Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + 
                    Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*Pair[ec[5], 
                    k[3]])) - Pair[e[1], e[2]]*((-MH^2 + S34 + 2*(T14 + T24))*
                 Pair[ec[5], k[1]] + (MH^2 - S34 - 2*(T14 + T24))*
                 Pair[ec[5], k[2]] + 2*(-T14 + T24)*Pair[ec[5], k[3]] + 
                2*(T - U)*Pair[ec[5], k[4]]))) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (-4*((MH^2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
                 Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
               Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH^2 + S34)*Pair[ec[4], 
                   k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                  Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[1], ec[4]]*
                Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
               Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], 
                     k[1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
                    Pair[ec[4], k[3]]) + (MH^2 - S34)*Pair[e[2], ec[4]]*
                  (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 2*Pair[ec[4], 
                 k[3]]*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], 
                     k[1]] + Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*
                  Pair[e[2], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
                  (Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
                   Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + Pair[e[1], e[2]]*
              ((MH^2 - S34)*(T + T14 - T24 - U)*Pair[ec[4], ec[5]] + 2*
                (2*(-MH^2 + S34)*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
                 2*(MH^2 - S34)*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
                   Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
                  (2*(T14 + T24)*Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], 
                     k[3]] + (T - T14 - T24 - U)*Pair[ec[5], k[4]])))))/
           (MH^2 - S34))/S34))/(MW*S*SW)}}}}
