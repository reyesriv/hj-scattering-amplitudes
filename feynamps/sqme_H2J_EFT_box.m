(* Created with the Wolfram Language : www.wolfram.com *)
-8*Alfas*GH*Pi*
  ((-46*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T - T14, 0]*
       (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
              Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
           ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
          Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
         ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
            (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
      Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
           Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
           Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
        4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
             Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
        Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]])))) + 
      Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 - 
   (8*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T24 - U, 0]*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[e[4], k[5]])*
           Pair[ec[1], k[4]] + (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[5]] - 
        4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*
             Pair[ec[2], e[4]]) + (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[5]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + 
        Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
          4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
            (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
      Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
           Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
             Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*
             Pair[ec[2], k[5]])) - 
        4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[
                e[4], k[5]])*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
              Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + Pair[ec[1], k[4]]*
           ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[2], e[4]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[5]])) + 
          Pair[e[4], k[1]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]])))) + Den[S, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 - 
   (2*Alfas*GH*Pi*(4*(Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
             Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
          4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[
                ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
               Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
             (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
          Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) - 
      4*(Den[MH2 - S - T24 - U, 0]*
         (2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))))))/3)*
  (Den[T24, 0]*(Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*
        Pair[e[2], ec[4]] - 4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
          Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
           Pair[ec[4], k[5]]))) + 4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*
        Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
        ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
         Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
       Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
         Pair[ec[5], k[3]])) - 2*Pair[e[2], ec[4]]*
      (Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[2]]) + 
       Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]) + 
       Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[4]]))) + 
   Den[MH2 - S34 - T14 - T24, 0]*
    (-2*(Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]]) - 
       Pair[e[1], k[3]]*(Pair[e[2], k[1]] + 2*Pair[e[2], k[5]]) + 
       Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]))*
      Pair[ec[4], ec[5]] + 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*
      Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
      (2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
     4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
      Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
       Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
      ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
       4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))) + 
   Den[MH2 - S - T - T14, 0]*(Pair[e[1], ec[5]]*
      ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
       2*(Pair[e[2], k[5]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
         Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
         Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + 2*Pair[ec[4], k[5]]))) + 
     4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
          Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
           Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
           Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
        (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
          Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
           Pair[ec[5], k[4]])))) + 
   Den[S, 0]*(Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
       2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) - 
     4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
         2*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
        (-(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]])) + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))) + 
 8*Alfas*GH*Pi*
  ((8*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T - T14, 0]*
       (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
              Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
           ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
          Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
         ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
            (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
      Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
           Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
           Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
        4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
             Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
        Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]])))) + 
      Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 + 
   (46*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T24 - U, 0]*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[e[4], k[5]])*
           Pair[ec[1], k[4]] + (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[5]] - 
        4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*
             Pair[ec[2], e[4]]) + (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[5]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + 
        Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
          4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
            (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
      Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
           Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
             Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*
             Pair[ec[2], k[5]])) - 
        4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[
                e[4], k[5]])*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
              Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + Pair[ec[1], k[4]]*
           ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[2], e[4]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[5]])) + 
          Pair[e[4], k[1]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]])))) + Den[S, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 - 
   (2*Alfas*GH*Pi*(4*(Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
             Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
          4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[
                ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
               Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
             (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
          Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) - 
      4*(Den[MH2 - S - T24 - U, 0]*
         (2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))))))/3)*(Den[MH2 - S34 - T14 - T24, 0]*
    (-2*(Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]]) - 
       Pair[e[1], k[3]]*(Pair[e[2], k[1]] + 2*Pair[e[2], k[5]]) + 
       Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]))*
      Pair[ec[4], ec[5]] + 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*
      Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
      (2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
     4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
      Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
       Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
      ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
       4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))) + 
   Den[MH2 - S - T24 - U, 0]*(2*Pair[e[2], ec[5]]*
      (Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[2]]) + 
       Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 
       Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 2*Pair[ec[4], k[5]])) - 
     4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
       ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
         Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
        Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
      ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
       4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))) + 
   Den[T14, 0]*(-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
       Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - 
           Pair[ec[5], k[3]])) + Pair[e[1], k[4]]*
        ((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
     Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
       2*(Pair[e[2], k[4]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
         Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
         Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + 2*Pair[ec[5], k[4]])))) + 
   Den[S, 0]*(Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
       2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) - 
     4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
         2*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
        (-(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]])) + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))) + 
 2*Alfas*GH*Pi*
  ((8*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T - T14, 0]*
       (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
              Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
           ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
          Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
         ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
            (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
      Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
           Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
           Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
        4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
             Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
        Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
          4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]])))) + 
      Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 - 
   (8*Alfas*GH*Pi*(Den[MH2 - S34 - T14 - T24, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
            (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
         Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
          Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
         (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
        4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
          Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
         (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
         (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
          Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
          Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
      Den[MH2 - S - T24 - U, 0]*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[e[4], k[5]])*
           Pair[ec[1], k[4]] + (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*
           Pair[ec[1], k[5]])*Pair[ec[2], e[5]] - 
        4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
             Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*
             Pair[ec[2], e[4]]) + (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[5]] + Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + 
        Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
          4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
            (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
      Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
           Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
             Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*
             Pair[ec[2], k[5]])) - 
        4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] + Pair[
                e[4], k[5]])*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
              Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + Pair[ec[1], k[4]]*
           ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[2], e[4]] + 
            Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[5]])) + 
          Pair[e[4], k[1]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*
             Pair[ec[1], ec[2]] + Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]])))) + Den[S, 0]*
       (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
          2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
            Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
            Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
         Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*
           ((Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - 
            (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
          (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
           Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
           Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
           (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
              2*Pair[ec[2], k[5]]))))))/3 + 
   (23*Alfas*GH*Pi*(4*(Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[T24, 0]*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*
             Pair[ec[1], k[2]] + (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
          4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[
                ec[1], ec[2]] + (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*
               Pair[ec[2], e[5]]) + (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[4]] + Pair[e[4], e[5]]*
             (Pair[ec[1], k[3]] + 2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + 
          Pair[ec[1], e[5]]*((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) - 
      4*(Den[MH2 - S - T24 - U, 0]*
         (2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))))))/6)*
  (4*(Den[T24, 0]*(Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*
          Pair[e[2], ec[4]] - 4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*
            Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
             Pair[ec[4], k[5]]))) + 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
          ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]])) - 2*Pair[e[2], ec[4]]*
        (Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[2]]) + 
         Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]) + 
         Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[4]]))) + 
     Den[MH2 - S - T - T14, 0]*(Pair[e[1], ec[5]]*
        ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
         2*(Pair[e[2], k[5]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
           Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + 2*Pair[ec[4], k[5]]))) + 
       4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
            Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
            Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]]))))) - 
   4*(Den[MH2 - S - T24 - U, 0]*(2*Pair[e[2], ec[5]]*
        (Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[2]]) + 
         Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 2*Pair[ec[4], k[5]])) - 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], k[5]]*
          Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))) + 
     Den[T14, 0]*(-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
         Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - 
             Pair[ec[5], k[3]])) + Pair[e[1], k[4]]*
          ((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
       Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         2*(Pair[e[2], k[4]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
           Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + 2*Pair[ec[5], k[4]]))))))
