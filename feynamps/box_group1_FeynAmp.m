(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
Den[S, 0]*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
  ((-2*Alfas2*EL*MT2*C0i[cc1, S34, 0, S, MT2, MT2, MT2]*
     (((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (((T + T14 - T24 - U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, S, 0, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
         Pair[e[2], k[1]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         Pair[e[2], k[1]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, S34, S, 0, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
         (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
      2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
      2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd111, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
       MT2, MT2, MT2] + D0i[dd111, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     ((-T + U)*Pair[e[1], e[2]] + 4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]] + 
         (T - U)*Pair[ec[4], k[5]])) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
         Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-T + 2*T14 - 2*T24 + U)*Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*
         Pair[ec[4], k[5]]) + 4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          2*Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (((T14 - T24)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[3]] - 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(MH2 - S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      4*(T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      4*(T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*(2*(T + U)*Pair[ec[4], k[1]] + 
        (T - 3*U)*Pair[ec[4], k[3]] + (-3*T + U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T - U)*
         Pair[ec[5], k[2]] + (T + T14 - T24 - U)*Pair[ec[5], k[3]])))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd33, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
           Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
           Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]]))))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd11, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
      2*(-MH2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
      Pair[e[1], e[2]]*(-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - S34 - T - U)*
           Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd001, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*(D0i[dd001, MH2, S34, S, 2*MH2 - S34 - T - U, 
       0, 0, MT2, MT2, MT2, MT2] + D0i[dd001, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(-((T14 - T24)*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - 
         Pair[ec[5], k[4]])) - 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
      2*(-MH2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
       ((-MH2 + S34)*Pair[ec[4], k[1]] + (MH2 - S34)*Pair[ec[4], k[2]] + 
        (T14 - T24)*Pair[ec[4], k[3]]))*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T + U)*Pair[e[1], e[2]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*
     Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + T14 - T24 + U)*Pair[e[1], e[2]] - 
      4*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(2*Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-2*T - T14 + T24 + 2*U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          2*Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd23, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(-2*(T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
          4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
        Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
           Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]]))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*((-((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
        4*(MH2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-((MH2 - S34 - T14 - T24)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[4]] + 16*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[4]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      4*(MH2 - S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
      32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + 
        (MH2 - S34 - T14 - T24)*Pair[ec[4], k[2]] + 
        2*(T - U)*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*
       (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
        Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) - 
      2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
        Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*
       Pair[ec[5], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd23, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(T14 - T24)*
       Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 16*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd11, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-((MH2 - S34)*(T - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) - 4*(MH2 - S34)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 4*(T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 4*(T + U)*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
        Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]*(2*(T + U)*Pair[ec[5], k[1]] + 
        (T - 3*U)*Pair[ec[5], k[3]] + (-3*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - 4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*
       (Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) - 
      (T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[ec[4], k[3]]*(-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd113, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]]) - (T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
         (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*
     (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
           Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + (MH2 - S34)*
         (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       (-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
           Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*(MH2 - S34 + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(MH2 - S34 + T14 + T24)*
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
         (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
       Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH2 - S34 + T14 + T24)*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
         (2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (2*MH2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
          (2*MH2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (-2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - T14 + T24 - U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, S34, S, 0, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
            2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])) + Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd00, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (-(Pair[e[1], ec[5]]*Pair[ec[4], k[5]]) + 2*Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((-2*T - T14 + T24 + 2*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd123, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*((-2*T - T14 + T24 + 2*U)*
            Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd3, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, 
      S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(3*T + 2*T14 - 2*T24 - 
        3*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (-3*MH2 + 3*S34 + 2*(T14 + T24))*Pair[ec[5], k[4]]) + 
      4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 3*S34 + 2*(T14 + T24))*
         Pair[ec[5], k[4]]) - 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (-MH2 + S34 + T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (-2*(T - T24)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(T - U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T14 - T24)*(T - 2*T14 + 2*T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*Pair[ec[4], k[5]]) + 
      4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[4], k[3]] + (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*
         Pair[ec[4], k[5]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (MH2 - S34 - T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (2*(T - T24)*Pair[ec[5], k[1]] + 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(-T + U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
         ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
          (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
             (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, 
      MT2]*(-4*((-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         (Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          2*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[2]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, S34, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) - 
          Pair[ec[4], k[2]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      4*((-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
          2*Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])))))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd13, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((2*MH2 - 2*S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 2*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
      Pair[e[1], e[2]]*(-((T - U)*(S - S34 + T + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[5]]*((T + U)*Pair[ec[5], k[1]] - 
            (T + U)*Pair[ec[5], k[2]] + 2*(T - U)*(Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])) - 2*Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-T + U)*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd1, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-3*MH2*T + 3*S34*T + 2*T*T24 + 3*MH2*U - 3*S34*U - 2*T14*U)*
         Pair[ec[4], ec[5]] - 2*(-2*Pair[ec[4], k[2]]*
           ((MH2 - S34 + T)*Pair[ec[5], k[1]] + T*Pair[ec[5], k[2]] - 
            (T + T14)*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           ((3*T + 2*T24 - 5*U)*Pair[ec[5], k[1]] + (5*T - 2*T14 - 3*U)*
             Pair[ec[5], k[2]] + 5*(-T + U)*Pair[ec[5], k[3]]) + 
          2*Pair[ec[4], k[1]]*(U*Pair[ec[5], k[1]] + (MH2 - S34 + U)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[3]]))) + 
      4*(-(((3*MH2 - 3*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
           (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            ((-3*MH2 + 3*S34 + T14 + T24)*Pair[e[2], k[3]] - 
             (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
           Pair[ec[4], k[3]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
           Pair[ec[5], k[4]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
          2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[
                1]] + (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
              Pair[ec[4], k[3]])) + Pair[e[2], ec[4]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-(T*T14) + T24*U)*Pair[ec[4], ec[5]] + 
        2*(-(Pair[ec[4], k[5]]*((MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
             (-MH2 + S34 + T24 + 2*U)*Pair[ec[5], k[3]] + 
             T*Pair[ec[5], k[4]])) + Pair[ec[4], k[1]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*
             Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((T14 + T24)*Pair[e[2], k[3]] + (T + U)*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
          (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
              Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[
                5]])) + Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd13, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((T - T14 - T24 + U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]])) + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*(-((T - U)*(T - T14 - T24 + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[2]]*(2*(MH2 - S34 - T14 - T24)*Pair[ec[5], k[3]] - 
            (T + U)*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
           (2*(-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (T + U)*Pair[ec[5], k[4]]) + 2*(T - U)*
           (Pair[ec[4], k[5]]*(2*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*((-T + T14 - T24 + U)*
          Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*
          ((T14 - T24)*Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
           Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
            Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*
        ((2*MH2*T14 - S34*T14 - 2*MH2*T24 + S34*T24 + T*T24 - T14*U)*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
          (-2*(MH2 - S34)*Pair[ec[5], k[1]] + (-MH2 + S34 - 2*T14)*
            Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[ec[5], k[4]]) + 
         2*Pair[ec[4], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
           (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
           2*(-2*MH2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
           (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
           Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
             Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-2*MH2*T14 + S34*T14 + 2*MH2*T24 - 
          S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
        2*(-(Pair[ec[4], k[2]]*((MH2 - S34)*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH2 - T)*Pair[ec[5], k[4]])) + 
          Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*((-MH2 + S34)*Pair[ec[5], k[1]] + 
            (MH2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH2 - S34 - U)*
             Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH2 - S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
          Pair[e[2], ec[4]]*((MH2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
           (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
             Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
             Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd1, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((MH2*T - S34*T - T^2 + 2*T*T14 - MH2*U + S34*U - 
          2*T24*U + U^2)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(2*(MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*S34 - 3*T + 2*T24 + 5*U)*Pair[ec[5], k[3]] + 
            2*T*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[3]]*
           ((MH2 - S - T - U)*Pair[ec[5], k[1]] - U*Pair[ec[5], k[3]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[1]]*
           ((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*
             Pair[ec[5], k[4]]))) + 
      4*(((3*MH2 - S - 2*S34 - 3*T - 3*U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          (T + U)*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-3*MH2 + S + 2*S34 + 3*T + 3*U)*Pair[e[2], k[3]] + 
            (T + U)*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S + 2*(T + U))*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[3]]*
           Pair[e[2], k[1]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(-Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[5]]*
           Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd3, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((5*MH2*T14 - 3*S34*T14 - T*T14 - 5*MH2*T24 + 3*S34*T24 + 3*T*T24 - 
          3*T14*U + T24*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[3]]*(-2*(T14 + T24)*Pair[ec[5], k[1]] + 
            (T14 + T24)*Pair[ec[5], k[3]] + 2*T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(2*(-2*MH2 + T + U)*Pair[ec[5], k[1]] + 
            (2*MH2 - 2*T + T14 - 7*T24)*Pair[ec[5], k[3]] + 
            2*(-MH2 + S34 + U)*Pair[ec[5], k[4]]) - Pair[ec[4], k[2]]*
           ((4*MH2 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*T - 7*T14 + T24)*Pair[ec[5], k[3]] + 
            2*(-3*MH2 + S34 + 2*T + U)*Pair[ec[5], k[4]]))) - 
      4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-5*MH2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (5*MH2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-2*MH2 + T + U)*Pair[ec[4], k[1]] + (-2*MH2 + T + U)*
           Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(3*(T14 + T24)*Pair[ec[5], k[3]] + 
          2*(S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*((-2*MH2 + T + U)*Pair[ec[4], k[1]] + 
            (-2*MH2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
             Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
           (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
             Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
              4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((MH2*T - S34*T - T*T24 - MH2*U + S34*U + T14*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[2]]*((MH2 - S34 - T14)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[2]] + (T + T14)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(-(T24*Pair[ec[5], k[1]]) + 
            T14*Pair[ec[5], k[2]] + (-T + U)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(T24*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[4]]))) + 
      2*(((2*MH2 - 2*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-2*MH2 + 2*S34 + T14 + T24)*Pair[e[2], k[3]] - 
            (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) - 
          Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - 
            (MH2 - S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
             Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[S, 0]*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
  ((-2*Alfas2*EL*MT2*C0i[cc1, S34, 0, S, MT2, MT2, MT2]*
     (((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (((T + T14 - T24 - U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, S, 0, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
         Pair[e[2], k[1]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         Pair[e[2], k[1]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, S34, S, 0, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
         (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
      2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
      2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd111, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
       MT2, MT2, MT2] + D0i[dd111, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     ((-T + U)*Pair[e[1], e[2]] + 4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]] + 
         (T - U)*Pair[ec[4], k[5]])) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
         Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-T + 2*T14 - 2*T24 + U)*Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*
         Pair[ec[4], k[5]]) + 4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          2*Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (((T14 - T24)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[3]] - 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(MH2 - S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      4*(T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      4*(T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*(2*(T + U)*Pair[ec[4], k[1]] + 
        (T - 3*U)*Pair[ec[4], k[3]] + (-3*T + U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T - U)*
         Pair[ec[5], k[2]] + (T + T14 - T24 - U)*Pair[ec[5], k[3]])))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd33, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
           Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
           Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]]))))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd11, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
      2*(-MH2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
      Pair[e[1], e[2]]*(-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - S34 - T - U)*
           Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd001, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*(D0i[dd001, MH2, S34, S, 2*MH2 - S34 - T - U, 
       0, 0, MT2, MT2, MT2, MT2] + D0i[dd001, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(-((T14 - T24)*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - 
         Pair[ec[5], k[4]])) - 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
      2*(-MH2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
       ((-MH2 + S34)*Pair[ec[4], k[1]] + (MH2 - S34)*Pair[ec[4], k[2]] + 
        (T14 - T24)*Pair[ec[4], k[3]]))*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T + U)*Pair[e[1], e[2]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*
     Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + T14 - T24 + U)*Pair[e[1], e[2]] - 
      4*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(2*Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-2*T - T14 + T24 + 2*U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          2*Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd23, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(-2*(T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
          4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
        Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
           Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]]))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*((-((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
        4*(MH2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-((MH2 - S34 - T14 - T24)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[4]] + 16*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[4]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      4*(MH2 - S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
      32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + 
        (MH2 - S34 - T14 - T24)*Pair[ec[4], k[2]] + 
        2*(T - U)*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*
       (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
        Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) - 
      2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
        Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*
       Pair[ec[5], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd23, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(T14 - T24)*
       Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 16*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd11, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-((MH2 - S34)*(T - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) - 4*(MH2 - S34)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 4*(T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 4*(T + U)*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
        Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]*(2*(T + U)*Pair[ec[5], k[1]] + 
        (T - 3*U)*Pair[ec[5], k[3]] + (-3*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - 4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*
       (Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) - 
      (T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[ec[4], k[3]]*(-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd113, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]]) - (T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
         (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*
     (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
           Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + (MH2 - S34)*
         (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       (-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
           Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*(MH2 - S34 + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(MH2 - S34 + T14 + T24)*
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
         (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
       Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH2 - S34 + T14 + T24)*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
         (2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (2*MH2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
          (2*MH2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (-2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - T14 + T24 - U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, S34, S, 0, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
            2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])) + Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd00, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (-(Pair[e[1], ec[5]]*Pair[ec[4], k[5]]) + 2*Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((-2*T - T14 + T24 + 2*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd123, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*((-2*T - T14 + T24 + 2*U)*
            Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd3, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, 
      S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(3*T + 2*T14 - 2*T24 - 
        3*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (-3*MH2 + 3*S34 + 2*(T14 + T24))*Pair[ec[5], k[4]]) + 
      4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 3*S34 + 2*(T14 + T24))*
         Pair[ec[5], k[4]]) - 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (-MH2 + S34 + T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (-2*(T - T24)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(T - U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T14 - T24)*(T - 2*T14 + 2*T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*Pair[ec[4], k[5]]) + 
      4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[4], k[3]] + (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*
         Pair[ec[4], k[5]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (MH2 - S34 - T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (2*(T - T24)*Pair[ec[5], k[1]] + 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(-T + U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
         ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
          (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
             (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, 
      MT2]*(-4*((-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         (Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          2*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[2]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, S34, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) - 
          Pair[ec[4], k[2]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      4*((-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
          2*Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])))))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd13, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((2*MH2 - 2*S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 2*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
      Pair[e[1], e[2]]*(-((T - U)*(S - S34 + T + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[5]]*((T + U)*Pair[ec[5], k[1]] - 
            (T + U)*Pair[ec[5], k[2]] + 2*(T - U)*(Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])) - 2*Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-T + U)*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd1, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-3*MH2*T + 3*S34*T + 2*T*T24 + 3*MH2*U - 3*S34*U - 2*T14*U)*
         Pair[ec[4], ec[5]] - 2*(-2*Pair[ec[4], k[2]]*
           ((MH2 - S34 + T)*Pair[ec[5], k[1]] + T*Pair[ec[5], k[2]] - 
            (T + T14)*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           ((3*T + 2*T24 - 5*U)*Pair[ec[5], k[1]] + (5*T - 2*T14 - 3*U)*
             Pair[ec[5], k[2]] + 5*(-T + U)*Pair[ec[5], k[3]]) + 
          2*Pair[ec[4], k[1]]*(U*Pair[ec[5], k[1]] + (MH2 - S34 + U)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[3]]))) + 
      4*(-(((3*MH2 - 3*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
           (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            ((-3*MH2 + 3*S34 + T14 + T24)*Pair[e[2], k[3]] - 
             (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
           Pair[ec[4], k[3]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
           Pair[ec[5], k[4]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
          2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[
                1]] + (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
              Pair[ec[4], k[3]])) + Pair[e[2], ec[4]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-(T*T14) + T24*U)*Pair[ec[4], ec[5]] + 
        2*(-(Pair[ec[4], k[5]]*((MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
             (-MH2 + S34 + T24 + 2*U)*Pair[ec[5], k[3]] + 
             T*Pair[ec[5], k[4]])) + Pair[ec[4], k[1]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*
             Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((T14 + T24)*Pair[e[2], k[3]] + (T + U)*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
          (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
              Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[
                5]])) + Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd13, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((T - T14 - T24 + U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]])) + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*(-((T - U)*(T - T14 - T24 + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[2]]*(2*(MH2 - S34 - T14 - T24)*Pair[ec[5], k[3]] - 
            (T + U)*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
           (2*(-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (T + U)*Pair[ec[5], k[4]]) + 2*(T - U)*
           (Pair[ec[4], k[5]]*(2*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*((-T + T14 - T24 + U)*
          Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*
          ((T14 - T24)*Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
           Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
            Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*
        ((2*MH2*T14 - S34*T14 - 2*MH2*T24 + S34*T24 + T*T24 - T14*U)*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
          (-2*(MH2 - S34)*Pair[ec[5], k[1]] + (-MH2 + S34 - 2*T14)*
            Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[ec[5], k[4]]) + 
         2*Pair[ec[4], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
           (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
           2*(-2*MH2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
           (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
           Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
             Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-2*MH2*T14 + S34*T14 + 2*MH2*T24 - 
          S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
        2*(-(Pair[ec[4], k[2]]*((MH2 - S34)*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH2 - T)*Pair[ec[5], k[4]])) + 
          Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*((-MH2 + S34)*Pair[ec[5], k[1]] + 
            (MH2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH2 - S34 - U)*
             Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH2 - S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
          Pair[e[2], ec[4]]*((MH2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
           (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
             Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
             Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd1, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((MH2*T - S34*T - T^2 + 2*T*T14 - MH2*U + S34*U - 
          2*T24*U + U^2)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(2*(MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*S34 - 3*T + 2*T24 + 5*U)*Pair[ec[5], k[3]] + 
            2*T*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[3]]*
           ((MH2 - S - T - U)*Pair[ec[5], k[1]] - U*Pair[ec[5], k[3]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[1]]*
           ((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*
             Pair[ec[5], k[4]]))) + 
      4*(((3*MH2 - S - 2*S34 - 3*T - 3*U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          (T + U)*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-3*MH2 + S + 2*S34 + 3*T + 3*U)*Pair[e[2], k[3]] + 
            (T + U)*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S + 2*(T + U))*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[3]]*
           Pair[e[2], k[1]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(-Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[5]]*
           Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd3, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((5*MH2*T14 - 3*S34*T14 - T*T14 - 5*MH2*T24 + 3*S34*T24 + 3*T*T24 - 
          3*T14*U + T24*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[3]]*(-2*(T14 + T24)*Pair[ec[5], k[1]] + 
            (T14 + T24)*Pair[ec[5], k[3]] + 2*T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(2*(-2*MH2 + T + U)*Pair[ec[5], k[1]] + 
            (2*MH2 - 2*T + T14 - 7*T24)*Pair[ec[5], k[3]] + 
            2*(-MH2 + S34 + U)*Pair[ec[5], k[4]]) - Pair[ec[4], k[2]]*
           ((4*MH2 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*T - 7*T14 + T24)*Pair[ec[5], k[3]] + 
            2*(-3*MH2 + S34 + 2*T + U)*Pair[ec[5], k[4]]))) - 
      4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-5*MH2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (5*MH2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-2*MH2 + T + U)*Pair[ec[4], k[1]] + (-2*MH2 + T + U)*
           Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(3*(T14 + T24)*Pair[ec[5], k[3]] + 
          2*(S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*((-2*MH2 + T + U)*Pair[ec[4], k[1]] + 
            (-2*MH2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
             Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
           (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
             Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
              4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((MH2*T - S34*T - T*T24 - MH2*U + S34*U + T14*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[2]]*((MH2 - S34 - T14)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[2]] + (T + T14)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(-(T24*Pair[ec[5], k[1]]) + 
            T14*Pair[ec[5], k[2]] + (-T + U)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(T24*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[4]]))) + 
      2*(((2*MH2 - 2*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-2*MH2 + 2*S34 + T14 + T24)*Pair[e[2], k[3]] - 
            (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) - 
          Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - 
            (MH2 - S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
             Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[S, 0]*Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
  ((2*Alfas2*EL*MT2*C0i[cc1, S34, 0, S, MT2, MT2, MT2]*
     (((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (((T + T14 - T24 - U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, S, 0, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
         Pair[e[2], k[1]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         Pair[e[2], k[1]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, S34, S, 0, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
         (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
      2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
      2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd111, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
       MT2, MT2, MT2] + D0i[dd111, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     ((-T + U)*Pair[e[1], e[2]] + 4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]] + 
         (T - U)*Pair[ec[4], k[5]])) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
         Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-T + 2*T14 - 2*T24 + U)*Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*
         Pair[ec[4], k[5]]) + 4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          2*Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (((T14 - T24)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[3]] - 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(MH2 - S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      4*(T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      4*(T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*(2*(T + U)*Pair[ec[4], k[1]] + 
        (T - 3*U)*Pair[ec[4], k[3]] + (-3*T + U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T - U)*
         Pair[ec[5], k[2]] + (T + T14 - T24 - U)*Pair[ec[5], k[3]])))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd33, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
           Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
           Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd11, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
      2*(-MH2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
      Pair[e[1], e[2]]*(-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - S34 - T - U)*
           Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd001, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*(D0i[dd001, MH2, S34, S, 2*MH2 - S34 - T - U, 
       0, 0, MT2, MT2, MT2, MT2] + D0i[dd001, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(-((T14 - T24)*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - 
         Pair[ec[5], k[4]])) - 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
      2*(-MH2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
       ((-MH2 + S34)*Pair[ec[4], k[1]] + (MH2 - S34)*Pair[ec[4], k[2]] + 
        (T14 - T24)*Pair[ec[4], k[3]]))*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T + U)*Pair[e[1], e[2]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*
     Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + T14 - T24 + U)*Pair[e[1], e[2]] - 
      4*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(2*Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-2*T - T14 + T24 + 2*U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          2*Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd23, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(-2*(T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
          4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
        Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
           Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]]))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*((-((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
        4*(MH2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-((MH2 - S34 - T14 - T24)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[4]] + 16*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[4]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      4*(MH2 - S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
      32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + 
        (MH2 - S34 - T14 - T24)*Pair[ec[4], k[2]] + 
        2*(T - U)*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*
       (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
        Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) - 
      2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
        Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - U)*Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*
       Pair[ec[5], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd23, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(T14 - T24)*
       Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 16*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd11, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-((MH2 - S34)*(T - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) - 4*(MH2 - S34)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 4*(T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 4*(T + U)*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
        Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]*(2*(T + U)*Pair[ec[5], k[1]] + 
        (T - 3*U)*Pair[ec[5], k[3]] + (-3*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - 4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*
       (Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) - 
      (T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[ec[4], k[3]]*(-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd113, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]]) - (T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
         (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*
     (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
           Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + (MH2 - S34)*
         (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       (-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
           Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*(MH2 - S34 + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(MH2 - S34 + T14 + T24)*
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
         (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
       Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH2 - S34 + T14 + T24)*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
         (2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (2*MH2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
          (2*MH2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (-2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - T14 + T24 - U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, S34, S, 0, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
            2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])) + Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd00, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (-(Pair[e[1], ec[5]]*Pair[ec[4], k[5]]) + 2*Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((-2*T - T14 + T24 + 2*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd123, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*((-2*T - T14 + T24 + 2*U)*
            Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, 
      S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(3*T + 2*T14 - 2*T24 - 
        3*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (-3*MH2 + 3*S34 + 2*(T14 + T24))*Pair[ec[5], k[4]]) + 
      4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 3*S34 + 2*(T14 + T24))*
         Pair[ec[5], k[4]]) - 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (-MH2 + S34 + T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (-2*(T - T24)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(T - U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T14 - T24)*(T - 2*T14 + 2*T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*Pair[ec[4], k[5]]) + 
      4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[4], k[3]] + (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*
         Pair[ec[4], k[5]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (MH2 - S34 - T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (2*(T - T24)*Pair[ec[5], k[1]] + 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(-T + U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
         ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
          (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
             (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, 
      MT2]*(-4*((-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         (Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          2*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[2]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, S34, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) - 
          Pair[ec[4], k[2]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      4*((-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
          2*Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd13, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((2*MH2 - 2*S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 2*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
      Pair[e[1], e[2]]*(-((T - U)*(S - S34 + T + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[5]]*((T + U)*Pair[ec[5], k[1]] - 
            (T + U)*Pair[ec[5], k[2]] + 2*(T - U)*(Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])) - 2*Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-T + U)*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-3*MH2*T + 3*S34*T + 2*T*T24 + 3*MH2*U - 3*S34*U - 2*T14*U)*
         Pair[ec[4], ec[5]] - 2*(-2*Pair[ec[4], k[2]]*
           ((MH2 - S34 + T)*Pair[ec[5], k[1]] + T*Pair[ec[5], k[2]] - 
            (T + T14)*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           ((3*T + 2*T24 - 5*U)*Pair[ec[5], k[1]] + (5*T - 2*T14 - 3*U)*
             Pair[ec[5], k[2]] + 5*(-T + U)*Pair[ec[5], k[3]]) + 
          2*Pair[ec[4], k[1]]*(U*Pair[ec[5], k[1]] + (MH2 - S34 + U)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[3]]))) + 
      4*(-(((3*MH2 - 3*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
           (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            ((-3*MH2 + 3*S34 + T14 + T24)*Pair[e[2], k[3]] - 
             (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
           Pair[ec[4], k[3]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
           Pair[ec[5], k[4]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
          2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[
                1]] + (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
              Pair[ec[4], k[3]])) + Pair[e[2], ec[4]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-(T*T14) + T24*U)*Pair[ec[4], ec[5]] + 
        2*(-(Pair[ec[4], k[5]]*((MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
             (-MH2 + S34 + T24 + 2*U)*Pair[ec[5], k[3]] + 
             T*Pair[ec[5], k[4]])) + Pair[ec[4], k[1]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*
             Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((T14 + T24)*Pair[e[2], k[3]] + (T + U)*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
          (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
              Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[
                5]])) + Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd13, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((T - T14 - T24 + U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]])) + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*(-((T - U)*(T - T14 - T24 + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[2]]*(2*(MH2 - S34 - T14 - T24)*Pair[ec[5], k[3]] - 
            (T + U)*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
           (2*(-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (T + U)*Pair[ec[5], k[4]]) + 2*(T - U)*
           (Pair[ec[4], k[5]]*(2*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*((-T + T14 - T24 + U)*
          Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*
          ((T14 - T24)*Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
           Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
            Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*
        ((2*MH2*T14 - S34*T14 - 2*MH2*T24 + S34*T24 + T*T24 - T14*U)*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
          (-2*(MH2 - S34)*Pair[ec[5], k[1]] + (-MH2 + S34 - 2*T14)*
            Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[ec[5], k[4]]) + 
         2*Pair[ec[4], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
           (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
           2*(-2*MH2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
           (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
           Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
             Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-2*MH2*T14 + S34*T14 + 2*MH2*T24 - 
          S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
        2*(-(Pair[ec[4], k[2]]*((MH2 - S34)*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH2 - T)*Pair[ec[5], k[4]])) + 
          Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*((-MH2 + S34)*Pair[ec[5], k[1]] + 
            (MH2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH2 - S34 - U)*
             Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH2 - S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
          Pair[e[2], ec[4]]*((MH2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
           (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
             Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
             Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd1, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((MH2*T - S34*T - T^2 + 2*T*T14 - MH2*U + S34*U - 
          2*T24*U + U^2)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(2*(MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*S34 - 3*T + 2*T24 + 5*U)*Pair[ec[5], k[3]] + 
            2*T*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[3]]*
           ((MH2 - S - T - U)*Pair[ec[5], k[1]] - U*Pair[ec[5], k[3]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[1]]*
           ((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*
             Pair[ec[5], k[4]]))) + 
      4*(((3*MH2 - S - 2*S34 - 3*T - 3*U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          (T + U)*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-3*MH2 + S + 2*S34 + 3*T + 3*U)*Pair[e[2], k[3]] + 
            (T + U)*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S + 2*(T + U))*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[3]]*
           Pair[e[2], k[1]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(-Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[5]]*
           Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((5*MH2*T14 - 3*S34*T14 - T*T14 - 5*MH2*T24 + 3*S34*T24 + 3*T*T24 - 
          3*T14*U + T24*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[3]]*(-2*(T14 + T24)*Pair[ec[5], k[1]] + 
            (T14 + T24)*Pair[ec[5], k[3]] + 2*T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(2*(-2*MH2 + T + U)*Pair[ec[5], k[1]] + 
            (2*MH2 - 2*T + T14 - 7*T24)*Pair[ec[5], k[3]] + 
            2*(-MH2 + S34 + U)*Pair[ec[5], k[4]]) - Pair[ec[4], k[2]]*
           ((4*MH2 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*T - 7*T14 + T24)*Pair[ec[5], k[3]] + 
            2*(-3*MH2 + S34 + 2*T + U)*Pair[ec[5], k[4]]))) - 
      4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-5*MH2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (5*MH2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-2*MH2 + T + U)*Pair[ec[4], k[1]] + (-2*MH2 + T + U)*
           Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(3*(T14 + T24)*Pair[ec[5], k[3]] + 
          2*(S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*((-2*MH2 + T + U)*Pair[ec[4], k[1]] + 
            (-2*MH2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
             Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
           (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
             Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
              4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((MH2*T - S34*T - T*T24 - MH2*U + S34*U + T14*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[2]]*((MH2 - S34 - T14)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[2]] + (T + T14)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(-(T24*Pair[ec[5], k[1]]) + 
            T14*Pair[ec[5], k[2]] + (-T + U)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(T24*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[4]]))) + 
      2*(((2*MH2 - 2*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-2*MH2 + 2*S34 + T14 + T24)*Pair[e[2], k[3]] - 
            (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) - 
          Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - 
            (MH2 - S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
             Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[S, 0]*Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
  ((2*Alfas2*EL*MT2*C0i[cc1, S34, 0, S, MT2, MT2, MT2]*
     (((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (((T + T14 - T24 - U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
       (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[2]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, S, 0, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[2]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, S34, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
         Pair[e[2], k[1]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (2*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         Pair[e[2], k[1]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
      Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, S34, S, 0, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
         (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
      2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
      2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd111, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
       MT2, MT2, MT2] + D0i[dd111, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     ((-T + U)*Pair[e[1], e[2]] + 4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*
     Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]] + 
         (T - U)*Pair[ec[4], k[5]])) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
         Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-T + 2*T14 - 2*T24 + U)*Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*
         Pair[ec[4], k[5]]) + 4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
          2*Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
     Pair[ec[5], k[3]])/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, MT2]*
     (((T14 - T24)*Pair[e[1], e[2]] - 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[3]] - 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(MH2 - S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      4*(T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      4*(T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*(2*(T + U)*Pair[ec[4], k[1]] + 
        (T - 3*U)*Pair[ec[4], k[3]] + (-3*T + U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[3]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*Pair[ec[4], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T - U)*
         Pair[ec[5], k[2]] + (T + T14 - T24 - U)*Pair[ec[5], k[3]])))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd33, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
       (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
           Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
           Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd11, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
      2*(-MH2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
      Pair[e[1], e[2]]*(-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - S34 - T - U)*
           Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd001, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*(D0i[dd001, MH2, S34, S, 2*MH2 - S34 - T - U, 
       0, 0, MT2, MT2, MT2, MT2] + D0i[dd001, MH2, 2*MH2 - S34 - T - U, 0, 
       MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2])*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
       ((-T + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*(-((T14 - T24)*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - 
         Pair[ec[5], k[4]])) - 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
     Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
      2*(-MH2 + S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
       ((-MH2 + S34)*Pair[ec[4], k[1]] + (MH2 - S34)*Pair[ec[4], k[2]] + 
        (T14 - T24)*Pair[ec[4], k[3]]))*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T - T14 + T24 + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd333, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-T + U)*Pair[e[1], e[2]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*
     Pair[ec[5], k[4]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((-T + T14 - T24 + U)*Pair[e[1], e[2]] - 
      4*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(2*Pair[e[2], k[4]] + 
          Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((-2*T - T14 + T24 + 2*U)*Pair[e[1], e[2]] - 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          2*Pair[e[2], k[5]])))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]])/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd23, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 
      0, MT2, MT2, MT2, MT2]*(-2*(T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
      2*(T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
          4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
        Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
           Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
       (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
      2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]]))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
          Pair[ec[4], k[2]]))*Pair[ec[5], k[4]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*((-((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
        4*(MH2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
      2*((-T14 + T24)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-((MH2 - S34 - T14 - T24)*(T + T14 - T24 - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[5]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       Pair[ec[5], k[4]] + 16*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[4], k[2]] + 2*(2*T + T14 - T24 - 2*U)*Pair[ec[4], k[5]])*
       Pair[ec[5], k[4]]))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      4*(MH2 - S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
      32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - Pair[e[1], k[5]]*
         Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[4]] + 
          Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[1]] + 
        (MH2 - S34 - T14 - T24)*Pair[ec[4], k[2]] + 
        2*(T - U)*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*((-T14 + T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
      2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - Pair[e[1], e[2]]*
       (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
        Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) - 
      2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
        Pair[ec[5], k[4]]) + 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd33, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S - T - U)*(T - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      8*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 32*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - U)*Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd12, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*((MH2 - S34)*(T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[3]] + 16*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[3]] - Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*
       Pair[ec[5], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd23, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(T14 - T24)*
       Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T14 + T24)*
       Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*(-MH2 + S34 + T14 + T24)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       Pair[ec[4], k[5]] + 16*(-2*Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[4]] + Pair[e[2], k[5]]))*Pair[ec[4], k[5]]*
       Pair[ec[5], k[4]] + 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T14 - T24)*
         Pair[ec[5], k[2]] + 2*(T - T14 + T24 - U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd11, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-((MH2 - S34)*(T - U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) - 4*(MH2 - S34)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 4*(T + U)*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 4*(T + U)*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*Pair[ec[4], k[3]] - 
      16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
        Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[3]]*(2*(T + U)*Pair[ec[5], k[1]] + 
        (T - 3*U)*Pair[ec[5], k[3]] + (-3*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, S, 2*MH2 - S34 - T - U, 
      0, 0, MT2, MT2, MT2, MT2]*((T14 - T24)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] - 4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + 
        Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
        Pair[ec[4], k[2]]) - 4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
       (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd113, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*(-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
        Pair[e[1], k[2]]*Pair[e[2], k[3]])*
       (Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) - 
      (T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[ec[4], k[3]]*(-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd113, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
         Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]) + Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]]) - (T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
         (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*
     (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
           Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + (MH2 - S34)*
         (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       (-((MH2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
           Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[ec[4], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[ec[4], k[5]]) - 2*(MH2 - S34 + T14 + T24)*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(MH2 - S34 + T14 + T24)*
       Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
      8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
          Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
         (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
         (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
          Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
       Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH2 - S34 + T14 + T24)*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
         (2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (2*MH2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
         (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
          (2*MH2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
          (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*Pair[ec[4], k[3]]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
        (-T + U)*Pair[ec[5], k[4]]) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (-2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - T14 + T24 - U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd003, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd003, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((T - U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, S34, S, 0, MT2, MT2, MT2]*
     (-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
            2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])) + Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (4*Alfas2*EL*MT2*D0i[dd00, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[5]]*
           Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
            Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[2], k[1]]*
         (-(Pair[e[1], ec[5]]*Pair[ec[4], k[5]]) + 2*Pair[e[1], ec[4]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
           Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
       ((-2*T - T14 + T24 + 2*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          2*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd133, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*((T - U)*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd133, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((T - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
      4*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd123, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-(Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*((-2*T - T14 + T24 + 2*U)*
            Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - 
        Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, 
      S, MT2, MT2, MT2, MT2]*((MH2 - S34 - T14 - T24)*(3*T + 2*T14 - 2*T24 - 
        3*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[5]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], ec[5]]*
       Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-MH2 + S34 + T + U)*Pair[ec[4], k[5]]) - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (-3*MH2 + 3*S34 + 2*(T14 + T24))*Pair[ec[5], k[4]]) + 
      4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 3*S34 + 2*(T14 + T24))*
         Pair[ec[5], k[4]]) - 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (-MH2 + S34 + T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (MH2 - S34 - T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (-2*(T - T24)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(T - U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     ((MH2 - S34 - T14 - T24)*(T - 2*T14 + 2*T24 - U)*Pair[e[1], e[2]]*
       Pair[ec[4], ec[5]] + 4*(MH2 - S34 - T14 - T24)*
       (Pair[e[1], k[3]]*Pair[e[2], k[1]] - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[1]] - Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
          2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 4*Pair[e[1], k[2]]*
       Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
        (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*Pair[ec[4], k[5]]) + 
      4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
         Pair[ec[4], k[3]] + (-3*MH2 + S + 2*S34 + 3*T14 + 3*T24)*
         Pair[ec[4], k[5]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
       ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
        (MH2 - S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], e[2]]*
       (2*Pair[ec[4], k[1]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
          (MH2 - S34 - T14)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[2]]*
         ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[5]]*
         (2*(T - T24)*Pair[ec[5], k[1]] + 2*(T14 - U)*Pair[ec[5], k[2]] + 
          5*(-T + U)*Pair[ec[5], k[4]])) + 
      8*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
          (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
        Pair[e[2], k[1]]*(Pair[e[1], k[4]]*(Pair[ec[4], k[3]] - 
            2*Pair[ec[4], k[5]]) + 2*Pair[e[1], k[3]]*Pair[ec[4], k[5]])*
         Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[5]]*(Pair[ec[5], k[3]] - 
            2*Pair[ec[5], k[4]]) + (Pair[e[2], k[4]]*(Pair[ec[4], k[3]] - 
              2*Pair[ec[4], k[5]]) + 2*Pair[e[2], k[3]]*Pair[ec[4], k[5]])*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
         ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
        Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
          (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
             (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, 2*MH2 - S34 - T - U, 0, S, MT2, MT2, 
      MT2]*(-4*((-(Pair[e[1], k[5]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         (Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*((-T - T14 + T24 + U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          2*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[2]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, S34, 0, S, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[4], ec[5]] - 
        2*(-2*Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) - 
          Pair[ec[4], k[2]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      4*((-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
         Pair[e[2], k[1]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
          2*Pair[ec[5], k[4]]) - Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd13, MH2, S34, 0, MH2 - S34 - T14 - T24, 
      0, S, MT2, MT2, MT2, MT2]*
     (-4*((2*MH2 - 2*S34 - T14 - T24)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], k[2]]*Pair[e[2], ec[5]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (T + U)*Pair[ec[4], k[5]]) + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 2*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
      Pair[e[1], e[2]]*(-((T - U)*(S - S34 + T + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[5]]*((T + U)*Pair[ec[5], k[1]] - 
            (T + U)*Pair[ec[5], k[2]] + 2*(T - U)*(Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])) - 2*Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-T + U)*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((-3*MH2*T + 3*S34*T + 2*T*T24 + 3*MH2*U - 3*S34*U - 2*T14*U)*
         Pair[ec[4], ec[5]] - 2*(-2*Pair[ec[4], k[2]]*
           ((MH2 - S34 + T)*Pair[ec[5], k[1]] + T*Pair[ec[5], k[2]] - 
            (T + T14)*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           ((3*T + 2*T24 - 5*U)*Pair[ec[5], k[1]] + (5*T - 2*T14 - 3*U)*
             Pair[ec[5], k[2]] + 5*(-T + U)*Pair[ec[5], k[3]]) + 
          2*Pair[ec[4], k[1]]*(U*Pair[ec[5], k[1]] + (MH2 - S34 + U)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[3]]))) + 
      4*(-(((3*MH2 - 3*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
           (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            ((-3*MH2 + 3*S34 + T14 + T24)*Pair[e[2], k[3]] - 
             (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
           Pair[ec[4], k[3]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
           Pair[ec[5], k[4]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])) - 
          2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
           (-Pair[ec[5], k[3]] + Pair[ec[5], k[4]])) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[
                1]] + (-MH2 + S34)*Pair[ec[4], k[2]] + (-T + T14 + T24 - U)*
              Pair[ec[4], k[3]])) + Pair[e[2], ec[4]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((-(T*T14) + T24*U)*Pair[ec[4], ec[5]] + 
        2*(-(Pair[ec[4], k[5]]*((MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
             (-MH2 + S34 + T24 + 2*U)*Pair[ec[5], k[3]] + 
             T*Pair[ec[5], k[4]])) + Pair[ec[4], k[1]]*
           ((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*
             Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
           ((MH2 - S34 - T14 - T24)*Pair[ec[5], k[1]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((T14 + T24)*Pair[e[2], k[3]] + (T + U)*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-MH2 + S34 + T14 + T24)*Pair[ec[4], k[3]] + 
          (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(-(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
              Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[
                5]])) + Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[5]]*Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/
    (MW*SW) + (2*Alfas2*EL*MT2*D0i[dd13, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (-4*((T - T14 - T24 + U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
        4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]])) + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(2*(-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (T + U)*Pair[ec[5], k[4]])) + 
      Pair[e[1], e[2]]*(-((T - U)*(T - T14 - T24 + U)*Pair[ec[4], ec[5]]) - 
        2*(Pair[ec[4], k[2]]*(2*(MH2 - S34 - T14 - T24)*Pair[ec[5], k[3]] - 
            (T + U)*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
           (2*(-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (T + U)*Pair[ec[5], k[4]]) + 2*(T - U)*
           (Pair[ec[4], k[5]]*(2*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + 
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*((-T + T14 - T24 + U)*
          Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + Pair[ec[4], k[5]]*
          ((T14 - T24)*Pair[ec[5], k[3]] + (T - U)*Pair[ec[5], k[4]]))) + 
      4*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
           Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] + 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
            Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, 
      MT2, MT2, MT2]*(-(Pair[e[1], e[2]]*
        ((2*MH2*T14 - S34*T14 - 2*MH2*T24 + S34*T24 + T*T24 - T14*U)*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
          (-2*(MH2 - S34)*Pair[ec[5], k[1]] + (-MH2 + S34 - 2*T14)*
            Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[ec[5], k[4]]) + 
         2*Pair[ec[4], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
           (MH2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
           2*(-2*MH2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
           (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
           Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH2 - 2*S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
             Pair[ec[4], k[3]] + (-MH2 + S34)*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((2*MH2 - 2*S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-2*MH2*T14 + S34*T14 + 2*MH2*T24 - 
          S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
        2*(-(Pair[ec[4], k[2]]*((MH2 - S34)*Pair[ec[5], k[1]] + 
             T14*Pair[ec[5], k[3]] + (MH2 - T)*Pair[ec[5], k[4]])) + 
          Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*((-MH2 + S34)*Pair[ec[5], k[1]] + 
            (MH2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH2 - S34 - U)*
             Pair[ec[5], k[4]]))) + 
      2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-4*MH2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (4*MH2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH2 - S34 + T14 + T24)*
           Pair[ec[5], k[3]] + (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
          Pair[e[2], ec[4]]*((MH2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
            (3*MH2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
           (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
             Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
           (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
             Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd1, MH2, 2*MH2 - S34 - T - U, 0, 
      MH2 - S34 - T14 - T24, 0, S, MT2, MT2, MT2, MT2]*
     (Pair[e[1], e[2]]*((MH2*T - S34*T - T^2 + 2*T*T14 - MH2*U + S34*U - 
          2*T24*U + U^2)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*(2*(MH2 - S34 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*S34 - 3*T + 2*T24 + 5*U)*Pair[ec[5], k[3]] + 
            2*T*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[3]]*
           ((MH2 - S - T - U)*Pair[ec[5], k[1]] - U*Pair[ec[5], k[3]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[4]]) + 2*Pair[ec[4], k[1]]*
           ((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*
             Pair[ec[5], k[4]]))) + 
      4*(((3*MH2 - S - 2*S34 - 3*T - 3*U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          (T + U)*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-3*MH2 + S + 2*S34 + 3*T + 3*U)*Pair[e[2], k[3]] + 
            (T + U)*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) - 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S + 2*(T + U))*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34 + T14 + T24)*
             Pair[ec[4], k[3]] + (MH2 - S34 - 2*(T + U))*Pair[ec[4], k[5]]) + 
          Pair[e[2], ec[4]]*((-MH2 + S + 2*(T + U))*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(Pair[e[1], k[2]]*Pair[e[2], k[4]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[3]]*
           Pair[e[2], k[1]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 2*Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(-Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
          Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[4]] - Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[4]] - Pair[e[1], k[5]]*
           Pair[e[2], k[1]]*(-2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, MH2, S34, S, 2*MH2 - S34 - T - U, 0, 0, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((5*MH2*T14 - 3*S34*T14 - T*T14 - 5*MH2*T24 + 3*S34*T24 + 3*T*T24 - 
          3*T14*U + T24*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[3]]*(-2*(T14 + T24)*Pair[ec[5], k[1]] + 
            (T14 + T24)*Pair[ec[5], k[3]] + 2*T24*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(2*(-2*MH2 + T + U)*Pair[ec[5], k[1]] + 
            (2*MH2 - 2*T + T14 - 7*T24)*Pair[ec[5], k[3]] + 
            2*(-MH2 + S34 + U)*Pair[ec[5], k[4]]) - Pair[ec[4], k[2]]*
           ((4*MH2 - 2*(T + U))*Pair[ec[5], k[1]] + 
            (-2*MH2 + 2*T - 7*T14 + T24)*Pair[ec[5], k[3]] + 
            2*(-3*MH2 + S34 + 2*T + U)*Pair[ec[5], k[4]]))) - 
      4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
          (-5*MH2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
          Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
            (5*MH2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
         Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
         ((-2*MH2 + T + U)*Pair[ec[4], k[1]] + (-2*MH2 + T + U)*
           Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(3*(T14 + T24)*Pair[ec[5], k[3]] + 
          2*(S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], ec[5]]*((-2*MH2 + T + U)*Pair[ec[4], k[1]] + 
            (-2*MH2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
             Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
           (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
             Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
             Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
           (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
              4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, MH2, S34, 0, MH2 - S34 - T14 - T24, 0, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], e[2]]*
       ((MH2*T - S34*T - T*T24 - MH2*U + S34*U + T14*U)*Pair[ec[4], ec[5]] - 
        2*(Pair[ec[4], k[2]]*((MH2 - S34 - T14)*Pair[ec[5], k[1]] - 
            T14*Pair[ec[5], k[2]] + (T + T14)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(-(T24*Pair[ec[5], k[1]]) + 
            T14*Pair[ec[5], k[2]] + (-T + U)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[1]]*(T24*Pair[ec[5], k[1]] + (-MH2 + S34 + T24)*
             Pair[ec[5], k[2]] - (T24 + U)*Pair[ec[5], k[4]]))) + 
      2*(((2*MH2 - 2*S34 - T14 - T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
          (T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
           ((-2*MH2 + 2*S34 + T14 + T24)*Pair[e[2], k[3]] - 
            (T + U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
          (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((-MH2 + S34 + T14 + T24)*
           Pair[ec[5], k[3]] - (MH2 - S34 + T + U)*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) - 
          Pair[e[2], ec[4]]*((-MH2 + S34 + T14 + T24)*Pair[ec[5], k[3]] - 
            (MH2 - S34 + T + U)*Pair[ec[5], k[4]])) + 
        2*(-(Pair[e[2], k[1]]*(Pair[e[1], k[5]]*Pair[ec[4], k[3]] + 
             Pair[e[1], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[3]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((Pair[e[2], k[5]]*Pair[ec[4], k[3]] + 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW))
