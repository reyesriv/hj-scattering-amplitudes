(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*(c124 - c142)*EL*GS*MT^2*
  (-(((Pair[e[1], ec[4]]*((MH^2 - U)*Pair[e[2], k[1]] - S*Pair[e[2], k[3]]) + 
        ((-MH^2 + U)*Pair[e[1], e[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]])*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         ((-MH^2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]))*PVC[0, 0, 0, 0, U, MH^2, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] + 
      (Pair[e[1], ec[4]]*(2*(S + T)*Pair[e[2], k[1]] + 
          (-3*S + T)*Pair[e[2], k[3]]) + 2*((-MH^2 + U)*Pair[e[1], e[2]] + 
          4*Pair[e[1], k[3]]*Pair[e[2], k[3]])*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*(-2*(MH^2 - U)*Pair[e[2], ec[4]] + 
          8*Pair[e[2], k[3]]*Pair[ec[4], k[3]]))*PVC[0, 0, 1, 0, U, MH^2, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 2*Pair[e[2], k[3]]*
       ((-S + T)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[3]]))*PVC[0, 0, 2, 0, U, MH^2, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 2*Pair[e[2], k[3]]*
       ((-S + T)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*PVC[0, 1, 1, 0, U, MH^2, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      (2*Pair[e[1], k[4]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         (Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]])*(PVB[0, 0, U, Sqrt[MT^2], Sqrt[MT^2]] - 
        4*PVC[1, 0, 0, 0, U, MH^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/U) - 
   (((-MH^2 + S)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
       (MH^2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
       2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
        ((T + U)*Pair[ec[4], k[1]] - U*Pair[ec[4], k[3]]))*
      PVC[0, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (2*(-MH^2 + S)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] + 
       2*(MH^2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
       8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
        (2*(T + U)*Pair[ec[4], k[1]] + (T - 3*U)*Pair[ec[4], k[3]]))*
      PVC[0, 1, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     2*((-T + U)*Pair[e[1], e[2]] - 4*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
       4*Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], k[3]]*
      (PVC[0, 1, 1, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVC[0, 2, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
     (2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
         Pair[ec[4], k[2]]))*(PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]] - 
       4*PVC[1, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/S - 
   (-(((S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 
        (MH^2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + Pair[e[1], k[3]]*
         (-(S*Pair[e[2], ec[4]]) + 2*Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]))*PVC[0, 0, 0, MH^2, T, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
     (2*(S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 
       2*(MH^2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
         Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + Pair[e[1], k[3]]*
        ((-3*S + U)*Pair[e[2], ec[4]] + 
         8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
            Pair[ec[4], k[3]])))*PVC[0, 1, 0, MH^2, T, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] - 2*Pair[e[1], k[3]]*
      (((-S + U)*Pair[e[2], ec[4]] - 4*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
         4*Pair[e[2], k[1]]*Pair[ec[4], k[3]])*PVC[0, 1, 1, MH^2, T, 0, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       ((-S + U)*Pair[e[2], ec[4]] + 4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[4], k[3]]))*PVC[0, 2, 0, MH^2, T, 0, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
     (-((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
       2*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 2*Pair[e[1], e[2]]*
        Pair[ec[4], k[2]])*(PVB[0, 0, T, Sqrt[MT^2], Sqrt[MT^2]] - 
       4*PVC[1, 0, 0, MH^2, T, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/T))/
 (MW*Pi*SW)
