(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-(Alfa^2*MT^2*MU*Mat[DiracChain[Spinor[k[1], MU, -1], 5, Spinor[k[2], MU, 1]]*
      SUNT[Col1, Col2]]*Pair[ec[4], k[3]]*
    ((9 - 24*SW^2 + 32*SW^4)*PVC[0, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] + 9*PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] + 2*(9 - 24*SW^2 + 32*SW^4)*
      PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     4*(9 - 24*SW^2 + 32*SW^4)*PVC[0, 1, 1, MH^2, S, MZ^2, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]]))/(12*CW^3*MW*(-MZ^2 + S)*SW^4) + 
 (Alfa^2*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 5, k[3], 
      Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*Pair[ec[4], k[3]]*
   ((9 - 24*SW^2 + 32*SW^4)*PVC[0, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]] + 9*PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]] + 4*(9 - 24*SW^2 + 32*SW^4)*
     (PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 1, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 2, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])))/
  (24*CW^3*MW*(-MZ^2 + S)*SW^4) - 
 (Alfa^2*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 1, k[3], 
      Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*Pair[ec[4], k[3]]*
   ((3 - 8*SW^2)*(9 - 24*SW^2 + 32*SW^4)*PVC[0, 0, 0, MH^2, S, MZ^2, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 9*(-3 + 8*SW^2)*
     PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
    4*(3 - 8*SW^2)*(9 - 24*SW^2 + 32*SW^4)*
     (PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 1, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      PVC[0, 2, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])))/
  (72*CW^3*MW*(-MZ^2 + S)*SW^4) + 
 (Alfa^2*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 5, ec[4], 
      Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*
   (-12*(9 - 12*SW^2 + 16*SW^4)*PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]] - 
    3*(36*MT^2 + MH^2*(9 - 24*SW^2 + 32*SW^4) + 
      (MZ^2 - S)*(9 - 24*SW^2 + 32*SW^4))*PVC[0, 0, 0, MH^2, S, MZ^2, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
    3*(9*MH^2 - 9*S + MZ^2*(45 - 96*SW^2 + 128*SW^4))*
     PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
    2*(6*MH^2*(9 - 12*SW^2 + 16*SW^4) + 3*(MZ^2 - S)*(9 - 24*SW^2 + 32*SW^4))*
     PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
    24*(9 - 24*SW^2 + 32*SW^4)*PVC[1, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]]))/(144*CW^3*MW*(-MZ^2 + S)*SW^4) + 
 (Alfa^2*MT^2*Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], 
      Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*
   (-4*(-27 + 108*SW^2 - 144*SW^4 + 128*SW^6)*PVB[0, 0, S, Sqrt[MT^2], 
      Sqrt[MT^2]] - (-3 + 8*SW^2)*(36*MT^2 + MH^2*(9 - 24*SW^2 + 32*SW^4) + 
      (MZ^2 - S)*(9 - 24*SW^2 + 32*SW^4))*PVC[0, 0, 0, MH^2, S, MZ^2, 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - (-3 + 8*SW^2)*
     (9*MH^2 - 9*S + MZ^2*(45 - 96*SW^2 + 128*SW^4))*
     PVC[0, 0, 1, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
    2*(8*MH^2*SW^2*(9 - 36*SW^2 + 32*SW^4) + (2*MH^2 + MZ^2 - S)*(3 - 8*SW^2)*
       (9 - 24*SW^2 + 32*SW^4))*PVC[0, 1, 0, MH^2, S, MZ^2, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2]] + 8*(-3 + 8*SW^2)*(9 - 24*SW^2 + 32*SW^4)*
     PVC[1, 0, 0, MH^2, S, MZ^2, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
  (144*CW^3*MW*(-MZ^2 + S)*SW^4)
