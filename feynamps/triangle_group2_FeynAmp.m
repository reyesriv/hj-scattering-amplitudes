(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu4, Glu2, Glu5, 0, 0]]*
    (Den[2*MH2 - S34 - T - U, 0]*
      ((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
         16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
        (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
          Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
       (-16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
            Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]) + 
         16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
            Pair[e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
        Pair[ec[5], k[3]] + 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
         MT2]*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
           2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
         Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
           2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
         2*Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
           2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) + 
       2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
        (Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
           2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
          ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
            Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*
          ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]))) + Den[S34, 0]*
      ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
        (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
          Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
       4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
        (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
           2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
           2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
         2*Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
           2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) + 
       Pair[ec[4], k[3]]*(-16*C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
          (Pair[e[1], k[3]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
            Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
         16*C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
          (Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
            Pair[e[2], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
       2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
        (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
           2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
          ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
            Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*
          ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))))/(MW*SW)) - 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu5, Glu2, Glu4, 0, 0]]*
   (Den[2*MH2 - S34 - T - U, 0]*((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
      (-16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]) + 
        16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
       Pair[ec[5], k[3]] + 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) + 
      2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*
         ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))) + Den[S34, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) + 
      Pair[ec[4], k[3]]*(-16*C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
        16*C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
         ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*
         ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]])))))/(MW*SW) + 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
   (Den[2*MH2 - S34 - T - U, 0]*((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
      (16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]) - 
        16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
       Pair[ec[5], k[3]] - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) - 
      2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*
         ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))) + Den[S34, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) - 
      16*Pair[ec[4], k[3]]*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
        C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*(-2*Pair[e[1], k[4]]*
           Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
         ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*
         ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]])))))/(MW*SW) + 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
   (Den[2*MH2 - S34 - T - U, 0]*((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
      (16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]) - 
        16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
       Pair[ec[5], k[3]] - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) - 
      2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*
         ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))) + Den[S34, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) - 
      16*Pair[ec[4], k[3]]*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
           Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
        C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*(-2*Pair[e[1], k[4]]*
           Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
         ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*
         ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]])))))/(MW*SW) - 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
   (Den[2*MH2 - S34 - T - U, 0]*((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
      (-16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]) + 
        16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
       Pair[ec[5], k[3]] - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(2*Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) - 
      2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
         ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))) + Den[S34, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) + 
      Pair[ec[4], k[3]]*(-16*C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
        16*C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
         ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*
         ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]])))))/(MW*SW) - 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
   (Den[2*MH2 - S34 - T - U, 0]*((4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
      (-16*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]) + 
        16*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
       Pair[ec[5], k[3]] - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(2*Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])) - 
      2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (2*Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
         ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))) + Den[S34, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], ec[5]]*((-MH2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], e[2]]*((-MH2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))) + 
      Pair[ec[4], k[3]]*(-16*C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 
        16*C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[5], k[4]])) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
         ((-MH2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*
         ((-MH2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]])))))/(MW*SW)
