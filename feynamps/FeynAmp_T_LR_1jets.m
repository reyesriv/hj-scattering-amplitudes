(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*(c124 - c142)*EL*GS*MT^2*
      (-(((-2*(T + U)*(Sqrt[MH^2 - 4*MT^2]*S*Log[(-MH^2 + 2*MT^2 + 
                 MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] - MH*Sqrt[S*(-4*MT^2 + S)]*
              Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])*
            Pair[e[1], e[2]]*Pair[ec[4], k[1]])/(MH*(MH^2 - S)*S) - 
          ((T + U)*(Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
              2 - Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)*
            Pair[e[1], e[2]]*Pair[ec[4], k[1]])/(2*(MH^2 - S)) - 
          (2 + Eps^(-1) + Log[Mu^2/MT^2] + (Sqrt[S*(-4*MT^2 + S)]*
              Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S - 
            (3*MH^2 + MH^2/Eps - 3*S - S/Eps + MH*Sqrt[MH^2 - 4*MT^2]*Log[
                (-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
              MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
                2 + (MH^2 - S)*Log[Mu^2/MT^2] - Sqrt[S*(-4*MT^2 + S)]*Log[
                (2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] - 
              MT^2*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^2)/
             (MH^2 - S))*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
            Pair[ec[4], k[2]]))/S) - 
       (-((2 + Eps^(-1) + Log[Mu^2/MT^2] + (Sqrt[U*(-4*MT^2 + U)]*
              Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])/U - 
            (3*MH^2 + MH^2/Eps - 3*U - U/Eps + MH*Sqrt[MH^2 - 4*MT^2]*Log[
                (-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
              MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
                2 + (MH^2 - U)*Log[Mu^2/MT^2] - Sqrt[U*(-4*MT^2 + U)]*Log[
                (2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
              MT^2*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)/
             (MH^2 - U))*(2*Pair[e[1], k[4]]*Pair[e[2], ec[4]] - 
            Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 2*Pair[e[1], e[2]]*
             Pair[ec[4], k[1]])) + 
         ((-(Sqrt[MH^2 - 4*MT^2]*U*Log[(-MH^2 + 2*MT^2 + 
                 MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]) + 
            MH*Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/
               (2*MT^2)])*Pair[e[2], k[3]]*((-S + T)*Pair[e[1], ec[4]] + 
            4*Pair[e[1], k[3]]*Pair[ec[4], k[1]]))/(MH*(MH^2 - U)*U) + 
         ((Sqrt[MH^2 - 4*MT^2]*U*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 
                   4*MT^2])/(2*MT^2)] - MH*Sqrt[U*(-4*MT^2 + U)]*
             Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)])*
           (-2*(MH^2 - U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
            (-3*S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + 
            2*((-MH^2 + U)*Pair[e[1], e[2]] + 4*Pair[e[1], k[3]]*Pair[e[2], 
                k[3]])*Pair[ec[4], k[1]]))/(MH*(MH^2 - U)*U) + 
         ((U*(-MH^2 + U) - MH*Sqrt[MH^2 - 4*MT^2]*U*Log[(-MH^2 + 2*MT^2 + 
                MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] - 
            MT^2*U*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
              2 + MH^2*Sqrt[U*(-4*MT^2 + U)]*Log[(2*MT^2 - U + 
                Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] + 
            MT^2*U*Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)*
           Pair[e[2], k[3]]*((-S + T)*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))/((MH^2 - U)^2*U) + 
         ((Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
            Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)]^2)*
           (-(S*Pair[e[1], ec[4]]*Pair[e[2], k[3]]) + (-MH^2 + U)*
             Pair[e[1], e[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             ((-MH^2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*Pair[ec[4], 
                k[2]])))/(2*(MH^2 - U)))/U - 
       ((2 + Eps^(-1) + Log[Mu^2/MT^2] + (Sqrt[T*(-4*MT^2 + T)]*
             Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])/T - 
           (3*MH^2 + MH^2/Eps - 3*T - T/Eps + MH*Sqrt[MH^2 - 4*MT^2]*
              Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
             MT^2*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
             (MH^2 - T)*Log[Mu^2/MT^2] - Sqrt[T*(-4*MT^2 + T)]*
              Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] - 
             MT^2*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)/
            (MH^2 - T))*(-(Pair[e[1], k[4]]*Pair[e[2], ec[4]]) + 
           2*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 2*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]) - 
         ((Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 - 
            Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)*
           (Pair[e[1], k[3]]*(-(S*Pair[e[2], ec[4]]) + 2*Pair[e[2], k[4]]*
               Pair[ec[4], k[1]]) - (MH^2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], 
                k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]])))/
          (2*(MH^2 - T)) - 2*Pair[e[1], k[3]]*
          (((T*(-MH^2 + T) - MH*Sqrt[MH^2 - 4*MT^2]*T*Log[(-MH^2 + 2*MT^2 + 
                  MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] - 
              MT^2*T*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^
                2 + MH^2*Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + 
                  Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)] + MT^2*T*
               Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)]^2)*
             ((-S + U)*Pair[e[2], ec[4]] - 4*Pair[e[2], k[3]]*Pair[ec[4], 
                k[1]]))/(2*(MH^2 - T)^2*T) + 
           ((-(Sqrt[MH^2 - 4*MT^2]*T*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 
                      4*MT^2])/(2*MT^2)]) + MH*Sqrt[T*(-4*MT^2 + T)]*Log[
                (2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])*
             ((-S + U)*Pair[e[2], ec[4]] + 4*Pair[e[2], k[3]]*Pair[ec[4], 
                k[2]]))/(2*MH*(MH^2 - T)*T)) - 
         ((Sqrt[MH^2 - 4*MT^2]*T*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 
                   4*MT^2])/(2*MT^2)] - MH*Sqrt[T*(-4*MT^2 + T)]*
             Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*MT^2)])*
           (-2*(MH^2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
              Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + Pair[e[1], k[3]]*
             ((-3*S + U)*Pair[e[2], ec[4]] + 8*Pair[e[2], k[3]]*Pair[ec[4], 
                k[2]])))/(MH*(MH^2 - T)*T))/T))/(MW*Pi*SW)}}}}
