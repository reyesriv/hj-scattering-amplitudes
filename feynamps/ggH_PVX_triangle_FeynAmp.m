(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*c12*EL*MT^2*
  (((MH^2*Pair[e[1], e[2]] - 2*Pair[e[1], k[2]]*Pair[e[2], k[1]])*
     PVC[0, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/2 + 
   4*Pair[e[1], k[2]]*Pair[e[2], k[1]]*PVC[0, 1, 1, 0, MH^2, 0, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], e[2]]*
    (PVB[0, 0, MH^2, Sqrt[MT^2], Sqrt[MT^2]] - 
     4*PVC[1, 0, 0, 0, MH^2, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])))/
 (MW*Pi*SW)
