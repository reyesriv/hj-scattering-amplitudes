(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {
  {
   {(col5*((2*Alfas^2*EL*MT^2*(((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
             Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
            Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
           ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
             Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
                Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                   S45]])])/Sqrt[Kallen\[Lambda][MH^2, S, S45]])*
          ((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + S*(-T + U))*
            Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
           4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
             (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
             Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
               (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
            Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
            Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], 
                 k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]]) + 
             ((-MH^2 + U)*Pair[ec[4], k[1]] + (MH^2 - T)*Pair[ec[4], k[2]])*
              Pair[ec[5], k[4]])))/(MW*SW) - 
        (2*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
             (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 (S*(MH^2 - S + S45) - Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][MH^2, 
                S, S45]^(3/2)) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) + (S*(MH^2 - S + S45)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 2*MH^2*(T + 2*T14 - 2*
                T24 - U) + 2*S34*U - 4*T14*U + U^2)*Pair[e[1], e[2]]*
            Pair[ec[4], ec[5]] - 4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*
              Pair[e[2], k[1]] + (-2*MH^2 + T + U)*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + 2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
             T*Pair[e[1], k[5]]*Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + 2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 
             2*S34*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*
              Pair[e[2], k[3]] + 2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*
              Pair[e[2], k[4]] - U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*
              Pair[e[2], k[5]] + U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*
            Pair[ec[4], ec[5]] + 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
           32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
              Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(2*(MH^2 - U)*Pair[ec[5], 
                 k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + (T - U)*
                Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 2*
                (MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
              Pair[ec[5], k[4]])))/(MW*SW) + 
        (4*Alfas^2*EL*MT^2*S*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((Sqrt[S*(-4*MT^2 + S)]*(MH^2 + S - S45))/(S*Kallen\[Lambda][
                MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*Log[
                (S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^2 - S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*
               Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) + ((4*I)*Alfas^2*EL*MT^2*Pi*Sqrt[S45*(-4*MT^2 + S45)]*
          HeavisideTheta[-4*MT^2 + S45]*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*S45*SW) - (8*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[
             -4*MT^2 + S]*((Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/
              (4*Kallen\[Lambda][MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*
                 S + MT^2*S^2 - 2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + 
                MT^2*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 - S + S45))/(4*Kallen\[Lambda][
                MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*S + MT^2*S^2 - 
                2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + MT^2*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))))*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) - (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            (-(Sqrt[S*(-4*MT^2 + S)]*(-5*MH^4 + 4*MH^2*S + S^2 + 4*MH^2*S45 - 
                 2*S*S45 + S45^2))/(2*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^6 - MH^4*S - MH^2*S^2 + S^3 - 
                2*MH^4*S45 + 8*MH^2*S*S45 - 2*S^2*S45 + MH^2*S45^2 + 
                S*S45^2))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + 
                      S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][
                MH^2, S, S45]^(5/2)))*
          (4*((2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
               Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
             4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
                 Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
                Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
        (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((3*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/Kallen\[Lambda][
                MH^2, S, S45]^2 + (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 
                2*MH^2*S^2 + 2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 
                4*MH^2*S*S45 - 4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + 
                S*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            (-(Sqrt[S45*(-4*MT^2 + S45)]*(-MH^6 + 5*MH^4*S - 7*MH^2*S^2 + 
                 3*S^3 + 3*MH^4*S45 + S^2*S45 - 3*MH^2*S45^2 - 5*S*S45^2 + 
                 S45^3))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 2*MH^2*S^2 + 
                2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 4*MH^2*S*S45 - 
                4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + S*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)))*
          (4*(2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
           16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
             Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW)))/(S*S45) + 
     (col6*((2*Alfas^2*EL*MT^2*(((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
             Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
            Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
           ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
             Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
                Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                   S45]])])/Sqrt[Kallen\[Lambda][MH^2, S, S45]])*
          ((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + S*(-T + U))*
            Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
           4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
             (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
             Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
               (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
            Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
            Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], 
                 k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]]) + 
             ((-MH^2 + U)*Pair[ec[4], k[1]] + (MH^2 - T)*Pair[ec[4], k[2]])*
              Pair[ec[5], k[4]])))/(MW*SW) - 
        (2*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
             (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 (S*(MH^2 - S + S45) - Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][MH^2, 
                S, S45]^(3/2)) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) + (S*(MH^2 - S + S45)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 2*MH^2*(T + 2*T14 - 2*
                T24 - U) + 2*S34*U - 4*T14*U + U^2)*Pair[e[1], e[2]]*
            Pair[ec[4], ec[5]] - 4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*
              Pair[e[2], k[1]] + (-2*MH^2 + T + U)*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + 2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
             T*Pair[e[1], k[5]]*Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + 2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 
             2*S34*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*
              Pair[e[2], k[3]] + 2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*
              Pair[e[2], k[4]] - U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*
              Pair[e[2], k[5]] + U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*
            Pair[ec[4], ec[5]] + 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
           32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
              Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(2*(MH^2 - U)*Pair[ec[5], 
                 k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + (T - U)*
                Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 2*
                (MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
              Pair[ec[5], k[4]])))/(MW*SW) + 
        (4*Alfas^2*EL*MT^2*S*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((Sqrt[S*(-4*MT^2 + S)]*(MH^2 + S - S45))/(S*Kallen\[Lambda][
                MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*Log[
                (S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^2 - S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*
               Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) + ((4*I)*Alfas^2*EL*MT^2*Pi*Sqrt[S45*(-4*MT^2 + S45)]*
          HeavisideTheta[-4*MT^2 + S45]*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*S45*SW) - (8*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[
             -4*MT^2 + S]*((Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/
              (4*Kallen\[Lambda][MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*
                 S + MT^2*S^2 - 2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + 
                MT^2*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 - S + S45))/(4*Kallen\[Lambda][
                MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*S + MT^2*S^2 - 
                2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + MT^2*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))))*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) - (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            (-(Sqrt[S*(-4*MT^2 + S)]*(-5*MH^4 + 4*MH^2*S + S^2 + 4*MH^2*S45 - 
                 2*S*S45 + S45^2))/(2*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^6 - MH^4*S - MH^2*S^2 + S^3 - 
                2*MH^4*S45 + 8*MH^2*S*S45 - 2*S^2*S45 + MH^2*S45^2 + 
                S*S45^2))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + 
                      S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][
                MH^2, S, S45]^(5/2)))*
          (4*((2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
               Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
             4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
                 Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
                Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) - 
        (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((3*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/Kallen\[Lambda][
                MH^2, S, S45]^2 + (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 
                2*MH^2*S^2 + 2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 
                4*MH^2*S*S45 - 4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + 
                S*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            (-(Sqrt[S45*(-4*MT^2 + S45)]*(-MH^6 + 5*MH^4*S - 7*MH^2*S^2 + 
                 3*S^3 + 3*MH^4*S45 + S^2*S45 - 3*MH^2*S45^2 - 5*S*S45^2 + 
                 S45^3))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 2*MH^2*S^2 + 
                2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 4*MH^2*S*S45 - 
                4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + S*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)))*
          (4*(2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
           16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
             Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW)))/(S*S45) + 
     (col1*((-2*Alfas^2*EL*MT^2*(((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
             Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
            Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
           ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
             Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
                Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                   S45]])])/Sqrt[Kallen\[Lambda][MH^2, S, S45]])*
          ((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + S*(-T + U))*
            Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
           4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
             (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
             Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
               (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
            Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
            Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], 
                 k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]]) + 
             ((-MH^2 + U)*Pair[ec[4], k[1]] + (MH^2 - T)*Pair[ec[4], k[2]])*
              Pair[ec[5], k[4]])))/(MW*SW) + 
        (2*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
             (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 (S*(MH^2 - S + S45) - Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][MH^2, 
                S, S45]^(3/2)) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) + (S*(MH^2 - S + S45)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 2*MH^2*(T + 2*T14 - 2*
                T24 - U) + 2*S34*U - 4*T14*U + U^2)*Pair[e[1], e[2]]*
            Pair[ec[4], ec[5]] - 4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*
              Pair[e[2], k[1]] + (-2*MH^2 + T + U)*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + 2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
             T*Pair[e[1], k[5]]*Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + 2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 
             2*S34*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*
              Pair[e[2], k[3]] + 2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*
              Pair[e[2], k[4]] - U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*
              Pair[e[2], k[5]] + U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*
            Pair[ec[4], ec[5]] + 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
           32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
              Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(2*(MH^2 - U)*Pair[ec[5], 
                 k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + (T - U)*
                Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 2*
                (MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
              Pair[ec[5], k[4]])))/(MW*SW) - 
        (4*Alfas^2*EL*MT^2*S*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((Sqrt[S*(-4*MT^2 + S)]*(MH^2 + S - S45))/(S*Kallen\[Lambda][
                MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*Log[
                (S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^2 - S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*
               Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) - ((4*I)*Alfas^2*EL*MT^2*Pi*Sqrt[S45*(-4*MT^2 + S45)]*
          HeavisideTheta[-4*MT^2 + S45]*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*S45*SW) + (8*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[
             -4*MT^2 + S]*((Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/
              (4*Kallen\[Lambda][MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*
                 S + MT^2*S^2 - 2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + 
                MT^2*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 - S + S45))/(4*Kallen\[Lambda][
                MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*S + MT^2*S^2 - 
                2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + MT^2*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))))*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) + (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            (-(Sqrt[S*(-4*MT^2 + S)]*(-5*MH^4 + 4*MH^2*S + S^2 + 4*MH^2*S45 - 
                 2*S*S45 + S45^2))/(2*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^6 - MH^4*S - MH^2*S^2 + S^3 - 
                2*MH^4*S45 + 8*MH^2*S*S45 - 2*S^2*S45 + MH^2*S45^2 + 
                S*S45^2))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + 
                      S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][
                MH^2, S, S45]^(5/2)))*
          (4*((2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
               Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
             4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
                 Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
                Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
        (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((3*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/Kallen\[Lambda][
                MH^2, S, S45]^2 + (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 
                2*MH^2*S^2 + 2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 
                4*MH^2*S*S45 - 4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + 
                S*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            (-(Sqrt[S45*(-4*MT^2 + S45)]*(-MH^6 + 5*MH^4*S - 7*MH^2*S^2 + 
                 3*S^3 + 3*MH^4*S45 + S^2*S45 - 3*MH^2*S45^2 - 5*S*S45^2 + 
                 S45^3))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 2*MH^2*S^2 + 
                2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 4*MH^2*S*S45 - 
                4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + S*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)))*
          (4*(2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
           16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
             Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW)))/(S*S45) + 
     (col2*((-2*Alfas^2*EL*MT^2*(((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
             Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/
            Sqrt[Kallen\[Lambda][MH^2, S, S45]] + 
           ((2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
             Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                 Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*S45 - 
                Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                   S45]])])/Sqrt[Kallen\[Lambda][MH^2, S, S45]])*
          ((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + S*(-T + U))*
            Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
           4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
             (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
             Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
               (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
            Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
            Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
           4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
            Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], 
                 k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]]) + 
             ((-MH^2 + U)*Pair[ec[4], k[1]] + (MH^2 - T)*Pair[ec[4], k[2]])*
              Pair[ec[5], k[4]])))/(MW*SW) + 
        (2*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((-2*Sqrt[S*(-4*MT^2 + S)])/Kallen\[Lambda][MH^2, S, S45] + 
             (S*(MH^2 - S + S45)*Log[(S*(MH^2 - S + S45) + 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 (S*(MH^2 - S + S45) - Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][MH^2, 
                S, S45]^(3/2)) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 + S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) + (S*(MH^2 - S + S45)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 2*MH^2*(T + 2*T14 - 2*
                T24 - U) + 2*S34*U - 4*T14*U + U^2)*Pair[e[1], e[2]]*
            Pair[ec[4], ec[5]] - 4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*
              Pair[e[2], k[1]] + (-2*MH^2 + T + U)*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + 2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
             T*Pair[e[1], k[5]]*Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + 2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 
             2*S34*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*
              Pair[e[2], k[3]] + 2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*
              Pair[e[2], k[4]] - U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
             2*MH^2*Pair[e[1], k[2]]*Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*
              Pair[e[2], k[5]] + U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*
            Pair[ec[4], ec[5]] + 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
           32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + Pair[
                ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
              Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
            Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
           4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*(2*(MH^2 - U)*Pair[ec[5], 
                 k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + (T - U)*
                Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 2*
                (MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
              Pair[ec[5], k[4]])))/(MW*SW) - 
        (4*Alfas^2*EL*MT^2*S*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((Sqrt[S*(-4*MT^2 + S)]*(MH^2 + S - S45))/(S*Kallen\[Lambda][
                MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*Log[
                (S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*Sqrt[
                    Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^2 - S + S45))/
              (S45*Kallen\[Lambda][MH^2, S, S45]) - (MH^2*(MH^2 - S - S45)*
               Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(3/2)))*
          (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) - ((4*I)*Alfas^2*EL*MT^2*Pi*Sqrt[S45*(-4*MT^2 + S45)]*
          HeavisideTheta[-4*MT^2 + S45]*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*S45*SW) + (8*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[
             -4*MT^2 + S]*((Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/
              (4*Kallen\[Lambda][MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*
                 S + MT^2*S^2 - 2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + 
                MT^2*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(-MH^2 - S + S45))/(4*Kallen\[Lambda][
                MH^2, S, S45]) + ((MH^4*MT^2 - 2*MH^2*MT^2*S + MT^2*S^2 - 
                2*MH^2*MT^2*S45 + MH^2*S*S45 - 2*MT^2*S*S45 + MT^2*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/(2*Kallen\[Lambda][MH^2, S, S45]^(3/2))))*
          (-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
              Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 2*
                Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
            ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
             4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
               (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))))/
         (MW*SW) + (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            (-(Sqrt[S*(-4*MT^2 + S)]*(-5*MH^4 + 4*MH^2*S + S^2 + 4*MH^2*S45 - 
                 2*S*S45 + S45^2))/(2*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            ((Sqrt[S45*(-4*MT^2 + S45)]*(MH^6 - MH^4*S - MH^2*S^2 + S^3 - 
                2*MH^4*S45 + 8*MH^2*S*S45 - 2*S^2*S45 + MH^2*S45^2 + 
                S*S45^2))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             ((-(MH^6*MT^2) - MH^6*S + MH^4*MT^2*S + 2*MH^4*S^2 + 
                MH^2*MT^2*S^2 - MH^2*S^3 - MT^2*S^3 + 3*MH^4*MT^2*S45 - 
                MH^4*S*S45 + 2*MH^2*MT^2*S*S45 - MH^2*S^2*S45 + 3*MT^2*S^2*
                 S45 - 3*MH^2*MT^2*S45^2 + 2*MH^2*S*S45^2 - 3*MT^2*S*S45^2 + 
                MT^2*S45^3)*Log[((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + 
                      S45)]*Sqrt[Kallen\[Lambda][MH^2, S, S45]])/
                 ((MH^2 + S - S45)*S45 - Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])])/Kallen\[Lambda][
                MH^2, S, S45]^(5/2)))*
          (4*((2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
               Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
             4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
                Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
                 Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
                Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW) + 
        (4*Alfas^2*EL*MT^2*((2*I)*Pi*HeavisideTheta[-4*MT^2 + S]*
            ((3*S*Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + S - S45))/Kallen\[Lambda][
                MH^2, S, S45]^2 + (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 
                2*MH^2*S^2 + 2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 
                4*MH^2*S*S45 - 4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + 
                S*S45^2)*Log[(S*(MH^2 - S + S45) + Sqrt[S*(-4*MT^2 + S)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/(S*(MH^2 - S + S45) - 
                  Sqrt[S*(-4*MT^2 + S)]*Sqrt[Kallen\[Lambda][MH^2, S, 
                     S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)) + 
           (2*I)*Pi*HeavisideTheta[-4*MT^2 + S45]*
            (-(Sqrt[S45*(-4*MT^2 + S45)]*(-MH^6 + 5*MH^4*S - 7*MH^2*S^2 + 
                 3*S^3 + 3*MH^4*S45 + S^2*S45 - 3*MH^2*S45^2 - 5*S*S45^2 + 
                 S45^3))/(2*S45*Kallen\[Lambda][MH^2, S, S45]^2) + 
             (S*(2*MH^4*MT^2 + MH^4*S - 4*MH^2*MT^2*S - 2*MH^2*S^2 + 
                2*MT^2*S^2 + S^3 - 4*MH^2*MT^2*S45 + 4*MH^2*S*S45 - 
                4*MT^2*S*S45 - 2*S^2*S45 + 2*MT^2*S45^2 + S*S45^2)*Log[
                ((MH^2 + S - S45)*S45 + Sqrt[S45*(-4*MT^2 + S45)]*
                   Sqrt[Kallen\[Lambda][MH^2, S, S45]])/((MH^2 + S - S45)*
                   S45 - Sqrt[S45*(-4*MT^2 + S45)]*Sqrt[Kallen\[Lambda][MH^2, 
                     S, S45]])])/Kallen\[Lambda][MH^2, S, S45]^(5/2)))*
          (4*(2*MH^2 - 2*S34 - T - U)*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
           16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
              Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
             Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
            ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
             4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], 
                 k[3]]*Pair[ec[5], k[4]]))))/(MW*SW)))/(S*S45)}}}}
