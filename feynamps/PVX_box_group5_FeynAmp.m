(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(col1*(-((Alfas^2*EL*MT^2*(2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + 
          Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 
          2*Pair[e[1], k[4]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
         ((MH^2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
          4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
            (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
       PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]])/(MW*SW)) + 
    (Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
           Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[3]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVC[0, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (Alfas^2*EL*MT^2*
      (-(Pair[e[1], e[2]]*((-(MH^2*(S + T14 - T24)) - T*T24 + T14*U + 
            S*(S34 + U))*Pair[ec[4], ec[5]] + 
          2*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
              (-MH^2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
            ((MH^2 - U)*Pair[ec[4], k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + 
              S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
       2*((Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
             (-MH^2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*
        ((-(S*(S34 + T)) - T*T24 + MH^2*(S - T14 + T24) + T14*U)*
          Pair[ec[4], ec[5]] + 2*(Pair[ec[4], k[5]]*
            ((MH^2 - U)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]] - 
             S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]])*
            Pair[ec[5], k[4]])) + 
       2*((Pair[e[1], k[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[
                ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + T + 2*T14)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*((-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         2*(MH^2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
        Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + S + T)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-MH^2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
         2*(MH^2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (Alfas^2*EL*MT^2*((MH^4 + MH^2*(S - T - 2*T24 - U) - 
         S*(2*S34 + U) + T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]]*(2*(-MH^2 + S34 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
           2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH^2 + S + T)*Pair[e[1], k[4]]*
            Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
             (T14 + T24)*Pair[e[2], k[3]] + (-MH^2 + S + U)*Pair[e[2], k[
                4]])))*Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
        (Pair[ec[4], k[5]]*((MH^2 - T)*Pair[ec[5], k[2]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + T)*Pair[ec[4], k[2]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*((MH^4 - S*(2*S34 + T) + MH^2*(S - T - 2*T14 - U) + 
         (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + (MH^2 - S - T)*
            Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
          (2*(-MH^2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
           2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
          ((MH^2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
           2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH^2 - S - U)*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 4*(-MH^2 + S + U)*
        Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
       4*Pair[e[1], ec[5]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-2*MH^2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
          S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
        ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
       2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVD[0, 0, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[e[2], k[1]]*((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*
      (-((MH^2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
         (Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) - 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[4]] - Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH^2 + S + T + 2*T14)*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
       4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 0, 2, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (2*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-MH^2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
         2*(MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
        ((-MH^2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
         2*(MH^2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      ((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*(PVD[0, 1, 0, 2, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (-((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
          S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
         (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))*
      PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*
      (-(((MH^2 - S - 2*T24 - U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
            (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]]) + 
       4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
            Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
         (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (((-MH^2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
       4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH^2 - S - 2*T24 - U)*
          Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 1, 2, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-((MH^2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
          Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
         Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*(PVD[0, 2, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
          Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
        Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 2*Pair[e[1], k[5]]*
          Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[5], k[4]] - Pair[e[1], e[2]]*((-MH^2 + S + 2*T24 + U)*
          Pair[ec[4], ec[5]] + 4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*(PVD[1, 1, 0, 0, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW)))/
  (MH^2 - S34 - T14 - T24) + 
 (col2*(-((Alfas^2*EL*MT^2*(2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + 
          Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 
          2*Pair[e[1], k[4]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
         ((MH^2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
          4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
            (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
       PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]])/(MW*SW)) + 
    (Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
           Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[3]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVC[0, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (Alfas^2*EL*MT^2*
      (-(Pair[e[1], e[2]]*((-(MH^2*(S + T14 - T24)) - T*T24 + T14*U + 
            S*(S34 + U))*Pair[ec[4], ec[5]] + 
          2*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
              (-MH^2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
            ((MH^2 - U)*Pair[ec[4], k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + 
              S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
       2*((Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
             (-MH^2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*
        ((-(S*(S34 + T)) - T*T24 + MH^2*(S - T14 + T24) + T14*U)*
          Pair[ec[4], ec[5]] + 2*(Pair[ec[4], k[5]]*
            ((MH^2 - U)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]] - 
             S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]])*
            Pair[ec[5], k[4]])) + 
       2*((Pair[e[1], k[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[
                ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + T + 2*T14)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*((-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         2*(MH^2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
        Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + S + T)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-MH^2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
         2*(MH^2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (Alfas^2*EL*MT^2*((MH^4 + MH^2*(S - T - 2*T24 - U) - 
         S*(2*S34 + U) + T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]]*(2*(-MH^2 + S34 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
           2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH^2 + S + T)*Pair[e[1], k[4]]*
            Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
             (T14 + T24)*Pair[e[2], k[3]] + (-MH^2 + S + U)*Pair[e[2], k[
                4]])))*Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
        (Pair[ec[4], k[5]]*((MH^2 - T)*Pair[ec[5], k[2]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + T)*Pair[ec[4], k[2]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*((MH^4 - S*(2*S34 + T) + MH^2*(S - T - 2*T14 - U) + 
         (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + (MH^2 - S - T)*
            Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
          (2*(-MH^2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
           2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
          ((MH^2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
           2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH^2 - S - U)*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 4*(-MH^2 + S + U)*
        Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
       4*Pair[e[1], ec[5]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-2*MH^2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
          S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
        ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
       2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVD[0, 0, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[e[2], k[1]]*((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*
      (-((MH^2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
         (Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) - 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[4]] - Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH^2 + S + T + 2*T14)*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
       4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 0, 2, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (2*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-MH^2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
         2*(MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
        ((-MH^2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
         2*(MH^2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      ((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*(PVD[0, 1, 0, 2, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (-((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
          S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
         (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))*
      PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*
      (-(((MH^2 - S - 2*T24 - U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
            (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]]) + 
       4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
            Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
         (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (((-MH^2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
       4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH^2 - S - 2*T24 - U)*
          Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 1, 2, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-((MH^2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
          Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
         Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*(PVD[0, 2, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
          Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
        Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 2*Pair[e[1], k[5]]*
          Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[5], k[4]] - Pair[e[1], e[2]]*((-MH^2 + S + 2*T24 + U)*
          Pair[ec[4], ec[5]] + 4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*(PVD[1, 1, 0, 0, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW)))/
  (MH^2 - S34 - T14 - T24) + 
 (col5*((Alfas^2*EL*MT^2*(2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 
         2*Pair[e[1], k[4]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
        ((MH^2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
           Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[3]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVC[0, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (Alfas^2*EL*MT^2*
      (-(Pair[e[1], e[2]]*((-(MH^2*(S + T14 - T24)) - T*T24 + T14*U + 
            S*(S34 + U))*Pair[ec[4], ec[5]] + 
          2*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
              (-MH^2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
            ((MH^2 - U)*Pair[ec[4], k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + 
              S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
       2*((Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
             (-MH^2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*
        ((-(S*(S34 + T)) - T*T24 + MH^2*(S - T14 + T24) + T14*U)*
          Pair[ec[4], ec[5]] + 2*(Pair[ec[4], k[5]]*
            ((MH^2 - U)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]] - 
             S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]])*
            Pair[ec[5], k[4]])) + 
       2*((Pair[e[1], k[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[
                ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + T + 2*T14)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*((-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         2*(MH^2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
        Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + S + T)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-MH^2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
         2*(MH^2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (Alfas^2*EL*MT^2*((MH^4 + MH^2*(S - T - 2*T24 - U) - 
         S*(2*S34 + U) + T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]]*(2*(-MH^2 + S34 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
           2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH^2 + S + T)*Pair[e[1], k[4]]*
            Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
             (T14 + T24)*Pair[e[2], k[3]] + (-MH^2 + S + U)*Pair[e[2], k[
                4]])))*Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
        (Pair[ec[4], k[5]]*((MH^2 - T)*Pair[ec[5], k[2]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + T)*Pair[ec[4], k[2]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((MH^4 - S*(2*S34 + T) + MH^2*(S - T - 2*T14 - U) + 
         (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + (MH^2 - S - T)*
            Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
          (2*(-MH^2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
           2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
          ((MH^2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
           2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH^2 - S - U)*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 4*(-MH^2 + S + U)*
        Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
       4*Pair[e[1], ec[5]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-2*MH^2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
          S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
        ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
       2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVD[0, 0, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[e[2], k[1]]*((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*
      (-((MH^2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
         (Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) - 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[4]] - Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH^2 + S + T + 2*T14)*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
       4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 0, 2, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (2*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-MH^2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
         2*(MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
        ((-MH^2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
         2*(MH^2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      ((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*(PVD[0, 1, 0, 2, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (-((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
          S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
         (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))*
      PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*
      (-(((MH^2 - S - 2*T24 - U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
            (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]]) + 
       4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
            Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
         (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (((-MH^2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
       4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH^2 - S - 2*T24 - U)*
          Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 1, 2, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-((MH^2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
          Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
         Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*(PVD[0, 2, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
          Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
        Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 2*Pair[e[1], k[5]]*
          Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[5], k[4]] - Pair[e[1], e[2]]*((-MH^2 + S + 2*T24 + U)*
          Pair[ec[4], ec[5]] + 4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*(PVD[1, 1, 0, 0, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW)))/
  (MH^2 - S34 - T14 - T24) + 
 (col6*((Alfas^2*EL*MT^2*(2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 
         2*Pair[e[1], k[4]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
        ((MH^2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
           (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
           Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
       4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 0, 1, MH^2 - S34 - T14 - T24, MH^2, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(2*Pair[e[2], k[3]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-2*MH^2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVC[0, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (Alfas^2*EL*MT^2*
      (-(Pair[e[1], e[2]]*((-(MH^2*(S + T14 - T24)) - T*T24 + T14*U + 
            S*(S34 + U))*Pair[ec[4], ec[5]] + 
          2*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
              (-MH^2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
            ((MH^2 - U)*Pair[ec[4], k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + 
              S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
       2*((Pair[e[1], k[4]]*((MH^2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
             (-MH^2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*
        ((-(S*(S34 + T)) - T*T24 + MH^2*(S - T14 + T24) + T14*U)*
          Pair[ec[4], ec[5]] + 2*(Pair[ec[4], k[5]]*
            ((MH^2 - U)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[2]] - 
             S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
             (MH^2 - T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]])*
            Pair[ec[5], k[4]])) + 
       2*((Pair[e[1], k[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
             S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
            ((-MH^2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
             (MH^2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
         ((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*
          Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*
          ((-MH^2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] + ((-MH^2 + T)*Pair[e[1], k[2]] + 
           S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
         2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[
                ec[5], k[2]]) + Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
             Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))*
      PVD[0, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + T + 2*T14)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*((-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         2*(MH^2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
        Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH^2 + S + T)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-MH^2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
         2*(MH^2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (Alfas^2*EL*MT^2*((MH^4 + MH^2*(S - T - 2*T24 - U) - 
         S*(2*S34 + U) + T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(Pair[e[1], k[2]]*(2*(-MH^2 + S34 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
           2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH^2 + S + T)*Pair[e[1], k[4]]*
            Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
             (T14 + T24)*Pair[e[2], k[3]] + (-MH^2 + S + U)*Pair[e[2], k[
                4]])))*Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 4*((-T + U)*Pair[e[1], k[2]] + 
         (MH^2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(-MH^2 + S + T)*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
        (Pair[ec[4], k[5]]*((MH^2 - T)*Pair[ec[5], k[2]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + T)*Pair[ec[4], k[2]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVD[0, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((MH^4 - S*(2*S34 + T) + MH^2*(S - T - 2*T14 - U) + 
         (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + (MH^2 - S - T)*
            Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
          (2*(-MH^2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
           2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
          ((MH^2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
           2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH^2 - S - U)*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 4*(-MH^2 + S + U)*
        Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
       4*Pair[e[1], ec[5]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*((-T + U)*Pair[e[2], k[1]] + 
         (-MH^2 + S + T)*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] - 
           S*Pair[ec[5], k[3]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
       8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-2*MH^2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
          S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
        ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
         Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
       2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]])*PVD[0, 0, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      Pair[e[2], k[1]]*((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-3*MH^2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + 
         (-MH^2 + S + T + 4*T14)*Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + T)*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + T)*Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*
      (-((MH^2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
         (Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) - 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[4]] - Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 2, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH^2 + S + T + 2*T14)*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
       4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 0, 2, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*((MH^2 - S - T - 2*T14)*
        (Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[3]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 0, 3, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (2*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-MH^2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
         2*(MH^2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
       2*(MH^2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(-(S*(-MH^2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
        ((-MH^2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
         2*(MH^2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
       4*(-MH^2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-MH^2 + S + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*S*Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*Pair[e[2], k[1]]*
      ((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((2*(-MH^2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
        (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 0, 1, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
          Pair[ec[5], k[4]]))*(PVD[0, 1, 0, 2, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (-((2*(-MH^2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
          S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
       2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
        Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
        (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
         (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))*
      PVD[0, 1, 1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      (((-3*MH^2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + 
         (-MH^2 + S + 4*T24 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       2*(-MH^2 + S + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       2*(-MH^2 + S + U)*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (8*Alfas^2*EL*MT^2*
      (-(((MH^2 - S - 2*T24 - U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
          Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
            (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]]) + 
       4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
            Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
         (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[0, 1, 1, 1, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (((-MH^2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (-MH^2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
       4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
            (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
              Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
            (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 1, 1, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[2], k[3]]*
      (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH^2 - S - 2*T24 - U)*
          Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
           (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))*
      PVD[0, 1, 2, 0, 0, MH^2, MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-((MH^2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
          Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
         Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 1, 2, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*(PVD[0, 2, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
      ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
       4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*Pair[e[1], k[2]]*
      ((MH^2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
        (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
          Pair[ec[5], k[4]]))*PVD[0, 2, 1, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
          Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
         2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 0, 0, 0, MH^2 - S34 - T14 - T24, 
       MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (8*Alfas^2*EL*MT^2*(-2*Pair[e[2], k[1]]*
        ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
          Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*(PVD[1, 0, 0, 1, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 0, 0, 1, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
           2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
        Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
        Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-MH^2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, MH^2, 
       MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 2*Pair[e[1], k[5]]*
          Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[3]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[5], k[4]] - Pair[e[1], e[2]]*((-MH^2 + S + 2*T24 + U)*
          Pair[ec[4], ec[5]] + 4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))*PVD[1, 0, 1, 0, 0, 
       MH^2 - S34 - T14 - T24, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(2*Pair[e[1], k[2]]*
        ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
          Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
        ((-MH^2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
         4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
            Pair[ec[5], k[4]])))*(PVD[1, 1, 0, 0, 0, MH^2, 
        MH^2 - S34 - T14 - T24, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, MH^2 - S34 - T14 - T24, MH^2, 0, T, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/(MW*SW)))/
  (MH^2 - S34 - T14 - T24)
