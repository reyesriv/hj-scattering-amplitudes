(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*EL*GS*MT2*Den[S, 0]*
  (-((2*B0i[bb0, S, MT2, MT2] + (MH2 - S)*C0i[cc0, MH2, S, 0, MT2, MT2, 
        MT2] - 8*C0i[cc00, MH2, S, 0, MT2, MT2, MT2] + 
      2*(MH2 - S)*C0i[cc1, MH2, S, 0, MT2, MT2, MT2])*
     Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], Spinor[k[2], MU, 1]]*
       SUNT[Glu4, Col2, Col1]]) + 2*(C0i[cc0, MH2, S, 0, MT2, MT2, MT2] + 
     4*(C0i[cc1, MH2, S, 0, MT2, MT2, MT2] + C0i[cc11, MH2, S, 0, MT2, MT2, 
        MT2] + C0i[cc12, MH2, S, 0, MT2, MT2, MT2]))*
    Mat[DiracChain[Spinor[k[1], MU, -1], 1, k[3], Spinor[k[2], MU, 1]]*
      SUNT[Glu4, Col2, Col1]]*Pair[ec[4], k[3]]))/(4*MW*Pi*SW)
