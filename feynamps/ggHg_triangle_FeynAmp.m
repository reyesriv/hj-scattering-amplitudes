(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*EL*GS*MT2*Mat[SUNT[Glu1, Glu2, Glu4, 0, 0]]*
   (Den[S, 0]*((B0i[bb0, S, MT2, MT2] - 4*C0i[cc00, MH2, S, 0, MT2, MT2, 
          MT2])*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 
        2*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])) - 
      2*(C0i[cc11, MH2, S, 0, MT2, MT2, MT2] + C0i[cc12, MH2, S, 0, MT2, MT2, 
         MT2])*((-T + U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]))*Pair[ec[4], k[3]] - 
      C0i[cc1, MH2, S, 0, MT2, MT2, MT2]*(2*(-MH2 + S)*Pair[e[1], k[2]]*
         Pair[e[2], ec[4]] + 2*(MH2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
        8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
         (2*(T + U)*Pair[ec[4], k[1]] + (T - 3*U)*Pair[ec[4], k[3]])) - 
      C0i[cc0, MH2, S, 0, MT2, MT2, MT2]*((-MH2 + S)*Pair[e[1], k[2]]*
         Pair[e[2], ec[4]] + (MH2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
        2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
         ((T + U)*Pair[ec[4], k[1]] - U*Pair[ec[4], k[3]]))) + 
    Den[U, 0]*((B0i[bb0, U, MT2, MT2] - 4*C0i[cc00, 0, U, MH2, MT2, MT2, 
          MT2])*(2*Pair[e[1], k[4]]*Pair[e[2], ec[4]] - 
        Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 
        2*Pair[e[1], e[2]]*Pair[ec[4], k[1]]) - 
      2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
       ((-S + T)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
      C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*((-MH2 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]]) + (-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*
       Pair[e[2], k[3]]*((-S + T)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]])) - C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
       (2*(-MH2 + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*(2*(S + T)*Pair[e[2], k[1]] + 
          (-3*S + T)*Pair[e[2], k[3]]) + 2*(-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]]))) - Den[T, 0]*
     ((B0i[bb0, T, MT2, MT2] - 4*C0i[cc00, MH2, T, 0, MT2, MT2, MT2])*
       (-((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
        2*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]) - C0i[cc0, MH2, T, 0, MT2, MT2, MT2]*
       (((S + U)*Pair[e[1], k[2]] - S*Pair[e[1], k[3]])*Pair[e[2], ec[4]] - 
        (MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[2]]) + 2*Pair[e[1], k[3]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - C0i[cc1, MH2, T, 0, MT2, MT2, MT2]*
       ((2*(S + U)*Pair[e[1], k[2]] + (-3*S + U)*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]] - 2*(MH2 - T)*(Pair[e[1], ec[4]]*
           Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
        8*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 
      2*(C0i[cc12, MH2, T, 0, MT2, MT2, MT2]*Pair[e[1], k[3]]*
         ((-S + U)*Pair[e[2], ec[4]] + 
          4*(-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
             Pair[ec[4], k[3]])) + C0i[cc11, MH2, T, 0, MT2, MT2, MT2]*
         Pair[e[1], k[3]]*((-S + U)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]))))))/(MW*Pi*SW) - 
 (Alfas*EL*GS*MT2*Mat[SUNT[Glu1, Glu4, Glu2, 0, 0]]*
   (Den[S, 0]*((B0i[bb0, S, MT2, MT2] - 4*C0i[cc00, MH2, S, 0, MT2, MT2, 
          MT2])*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 
        2*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
         (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])) - 
      2*(C0i[cc11, MH2, S, 0, MT2, MT2, MT2] + C0i[cc12, MH2, S, 0, MT2, MT2, 
         MT2])*((-T + U)*Pair[e[1], e[2]] + 
        4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]]))*Pair[ec[4], k[3]] - 
      C0i[cc1, MH2, S, 0, MT2, MT2, MT2]*(2*(-MH2 + S)*Pair[e[1], k[2]]*
         Pair[e[2], ec[4]] + 2*(MH2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
        8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
         (2*(T + U)*Pair[ec[4], k[1]] + (T - 3*U)*Pair[ec[4], k[3]])) - 
      C0i[cc0, MH2, S, 0, MT2, MT2, MT2]*((-MH2 + S)*Pair[e[1], k[2]]*
         Pair[e[2], ec[4]] + (MH2 - S)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
        2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], k[3]] - Pair[e[1], e[2]]*
         ((T + U)*Pair[ec[4], k[1]] - U*Pair[ec[4], k[3]]))) + 
    Den[U, 0]*((B0i[bb0, U, MT2, MT2] - 4*C0i[cc00, 0, U, MH2, MT2, MT2, 
          MT2])*(2*Pair[e[1], k[4]]*Pair[e[2], ec[4]] - 
        Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 
        2*Pair[e[1], e[2]]*Pair[ec[4], k[1]]) - 
      2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
       ((-S + T)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
      C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*((-MH2 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]]) + (-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*
       Pair[e[2], k[3]]*((-S + T)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]])) - C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
       (2*(-MH2 + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*(2*(S + T)*Pair[e[2], k[1]] + 
          (-3*S + T)*Pair[e[2], k[3]]) + 2*(-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]]))) - Den[T, 0]*
     ((B0i[bb0, T, MT2, MT2] - 4*C0i[cc00, MH2, T, 0, MT2, MT2, MT2])*
       (-((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
        2*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]) - C0i[cc0, MH2, T, 0, MT2, MT2, MT2]*
       (((S + U)*Pair[e[1], k[2]] - S*Pair[e[1], k[3]])*Pair[e[2], ec[4]] - 
        (MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[2]]) + 2*Pair[e[1], k[3]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - C0i[cc1, MH2, T, 0, MT2, MT2, MT2]*
       ((2*(S + U)*Pair[e[1], k[2]] + (-3*S + U)*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]] - 2*(MH2 - T)*(Pair[e[1], ec[4]]*
           Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 
        8*Pair[e[1], k[3]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 
      2*(C0i[cc12, MH2, T, 0, MT2, MT2, MT2]*Pair[e[1], k[3]]*
         ((-S + U)*Pair[e[2], ec[4]] + 
          4*(-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
             Pair[ec[4], k[3]])) + C0i[cc11, MH2, T, 0, MT2, MT2, MT2]*
         Pair[e[1], k[3]]*((-S + U)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]]))))))/(MW*Pi*SW)
