(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
Den[MH2 - S - T - T14, 0]*Den[T24, 0]*Mat[SUNT[Glu1, Glu4, Glu2, Glu5, 0, 0]]*
  ((-4*Alfas2*EL*MT2*C0i[cc12, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]]) - 
      4*(S34 - U)*Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[5], k[3]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc11, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 4*(S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 4*S34*T + 2*S34*T14 + 4*MH2*(S34 - U) + 4*T*U + 2*T14*U + 
        U^2 - 2*S*(S34 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      4*Pair[e[1], ec[5]]*((2*(S34 + U)*Pair[e[2], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[ec[4], k[3]])) - 
      8*(S34 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
      8*(S34 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      32*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (2*(S34 + U)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-3*S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*(2*(S34 + U)*Pair[ec[5], k[2]] + 
          (-3*S34 + U)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 2*S34*T - S34*T24 + 2*T14*T24 - 2*S*(2*S34 + T24) + 
        MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 2*T*U + 4*T14*U + T24*U + U^2)*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - 4*(MH2 - S34 - T24 - U)*Pair[e[1], ec[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])) - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 8*(-MH2 + S34 + T24 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 4*Pair[e[2], ec[4]]*
       ((-MH2 + 2*S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + T24*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + 2*U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 2*S34*Pair[e[1], k[5]]*
         Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[2]] - 
        S34*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + U*Pair[e[1], k[5]]*
         Pair[ec[5], k[3]] - MH2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 
        T24*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 2*U*Pair[e[1], k[5]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*B0i[bb0, T24, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*C0i[cc00, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc0, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-(S34*T) + S34*T14 + MH2*(S34 - U) + T*U + T14*U - S*(S34 + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((S34 + U)*Pair[e[2], k[1]] + (-MH2 + T)*Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + 
          (-MH2 + T)*Pair[ec[4], k[3]])) - 4*(S34 + U)*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S34 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (U*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + S34*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(U*Pair[ec[5], k[2]] + 
          S34*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[MH2 - S - T - T14, 0]*Den[T24, 0]*
  Mat[SUNT[Glu1, Glu5, Glu2, Glu4, 0, 0]]*
  ((-4*Alfas2*EL*MT2*C0i[cc12, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]]) - 
      4*(S34 - U)*Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[5], k[3]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc11, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 4*(S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 4*S34*T + 2*S34*T14 + 4*MH2*(S34 - U) + 4*T*U + 2*T14*U + 
        U^2 - 2*S*(S34 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      4*Pair[e[1], ec[5]]*((2*(S34 + U)*Pair[e[2], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[ec[4], k[3]])) - 
      8*(S34 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
      8*(S34 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      32*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (2*(S34 + U)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-3*S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*(2*(S34 + U)*Pair[ec[5], k[2]] + 
          (-3*S34 + U)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 2*S34*T - S34*T24 + 2*T14*T24 - 2*S*(2*S34 + T24) + 
        MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 2*T*U + 4*T14*U + T24*U + U^2)*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - 4*(MH2 - S34 - T24 - U)*Pair[e[1], ec[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])) - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 8*(-MH2 + S34 + T24 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 4*Pair[e[2], ec[4]]*
       ((-MH2 + 2*S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + T24*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + 2*U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 2*S34*Pair[e[1], k[5]]*
         Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[2]] - 
        S34*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + U*Pair[e[1], k[5]]*
         Pair[ec[5], k[3]] - MH2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 
        T24*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 2*U*Pair[e[1], k[5]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*B0i[bb0, T24, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*C0i[cc00, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc0, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-(S34*T) + S34*T14 + MH2*(S34 - U) + T*U + T14*U - S*(S34 + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((S34 + U)*Pair[e[2], k[1]] + (-MH2 + T)*Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + 
          (-MH2 + T)*Pair[ec[4], k[3]])) - 4*(S34 + U)*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S34 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (U*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + S34*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(U*Pair[ec[5], k[2]] + 
          S34*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[MH2 - S - T - T14, 0]*Den[T24, 0]*
  Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
  ((4*Alfas2*EL*MT2*C0i[cc12, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]]) - 
      4*(S34 - U)*Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[5], k[3]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc11, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 4*(S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 4*S34*T + 2*S34*T14 + 4*MH2*(S34 - U) + 4*T*U + 2*T14*U + 
        U^2 - 2*S*(S34 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      4*Pair[e[1], ec[5]]*((2*(S34 + U)*Pair[e[2], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[ec[4], k[3]])) - 
      8*(S34 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
      8*(S34 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      32*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (2*(S34 + U)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-3*S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*(2*(S34 + U)*Pair[ec[5], k[2]] + 
          (-3*S34 + U)*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 2*S34*T - S34*T24 + 2*T14*T24 - 2*S*(2*S34 + T24) + 
        MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 2*T*U + 4*T14*U + T24*U + U^2)*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - 4*(MH2 - S34 - T24 - U)*Pair[e[1], ec[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])) - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 8*(-MH2 + S34 + T24 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 4*Pair[e[2], ec[4]]*
       ((-MH2 + 2*S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + T24*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + 2*U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 2*S34*Pair[e[1], k[5]]*
         Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[2]] - 
        S34*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + U*Pair[e[1], k[5]]*
         Pair[ec[5], k[3]] - MH2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 
        T24*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 2*U*Pair[e[1], k[5]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*B0i[bb0, T24, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*C0i[cc00, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc0, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-(S34*T) + S34*T14 + MH2*(S34 - U) + T*U + T14*U - S*(S34 + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((S34 + U)*Pair[e[2], k[1]] + (-MH2 + T)*Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + 
          (-MH2 + T)*Pair[ec[4], k[3]])) - 4*(S34 + U)*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S34 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (U*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + S34*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(U*Pair[ec[5], k[2]] + 
          S34*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[MH2 - S - T - T14, 0]*Den[T24, 0]*
  Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
  ((4*Alfas2*EL*MT2*C0i[cc12, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]]) - 
      4*(S34 - U)*Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[5], k[3]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc11, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (-((S34 - U)*(-2*MH2 + S34 + 2*T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) - 4*(2*MH2 - S34 - 2*T - U)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 4*(S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]]) + 16*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         Pair[ec[5], k[3]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 4*S34*T + 2*S34*T14 + 4*MH2*(S34 - U) + 4*T*U + 2*T14*U + 
        U^2 - 2*S*(S34 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(S34 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      4*Pair[e[1], ec[5]]*((2*(S34 + U)*Pair[e[2], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*(S34 + U)*Pair[ec[4], k[1]] + 
          (-4*MH2 + S34 + 4*T + U)*Pair[ec[4], k[3]])) - 
      8*(S34 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
      8*(S34 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      32*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (2*(S34 + U)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-3*S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[5]]*(2*(S34 + U)*Pair[ec[5], k[2]] + 
          (-3*S34 + U)*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-S34^2 - 2*S34*T - S34*T24 + 2*T14*T24 - 2*S*(2*S34 + T24) + 
        MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 2*T*U + 4*T14*U + T24*U + U^2)*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - 4*(MH2 - S34 - T24 - U)*Pair[e[1], ec[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])) - 
      8*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 8*(-MH2 + S34 + T24 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 4*Pair[e[2], ec[4]]*
       ((-MH2 + 2*S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (-S34 + U)*Pair[e[1], k[3]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + T24*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + 2*U*Pair[e[1], k[4]]*Pair[ec[5], k[1]] - 
        MH2*Pair[e[1], k[5]]*Pair[ec[5], k[2]] + 2*S34*Pair[e[1], k[5]]*
         Pair[ec[5], k[2]] + T24*Pair[e[1], k[5]]*Pair[ec[5], k[2]] - 
        S34*Pair[e[1], k[5]]*Pair[ec[5], k[3]] + U*Pair[e[1], k[5]]*
         Pair[ec[5], k[3]] - MH2*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 
        T24*Pair[e[1], k[5]]*Pair[ec[5], k[4]] + 2*U*Pair[e[1], k[5]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*B0i[bb0, T24, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*C0i[cc00, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     (8*Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      8*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 4*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc0, MH2, T24, MH2 - S - T - T14, MT2, MT2, MT2]*
     ((-(S34*T) + S34*T14 + MH2*(S34 - U) + T*U + T14*U - S*(S34 + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(S34 + U)*Pair[e[1], k[5]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((S34 + U)*Pair[e[2], k[1]] + (-MH2 + T)*Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + 
          (-MH2 + T)*Pair[ec[4], k[3]])) - 4*(S34 + U)*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 4*(S34 + U)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
      8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[3]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]]) + 4*Pair[e[2], ec[4]]*
       (U*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + S34*Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(U*Pair[ec[5], k[2]] + 
          S34*Pair[ec[5], k[4]]))))/(MW*SW))
