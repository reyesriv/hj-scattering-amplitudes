(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
Den[MH2 - S34 - T14 - T24, 0]*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
  ((-4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-2*MH2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
       ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
        Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     ((Pair[e[1], k[4]] + Pair[e[1], k[5]])*(Pair[e[2], k[4]] - 
        Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd133, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-MH2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
        2*(MH2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd3, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + T + 2*T14)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*((-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        2*(MH2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
       Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*
       Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
        Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd113, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((2*(-MH2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
        S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-MH2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
        2*(MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
       ((-MH2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
        2*(MH2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*
       Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (-((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, MH2 - S34 - T14 - T24, MH2, S, MT2, 
      MT2, MT2]*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
          Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
       Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd003, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(-2*Pair[e[2], k[1]]*
       ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
         Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-(((MH2 - S - 2*T24 - U)*Pair[e[1], k[3]]*
          Pair[e[2], k[1]] - Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*
            Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*
        Pair[ec[4], ec[5]]) + 
      4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
           Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
        (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 
        2*Pair[e[1], k[5]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd001, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(2*Pair[e[1], k[2]]*
       ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
      4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
          Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2, MH2 - S34 - T14 - T24, 0, 
      U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH2 - S - 2*T24 - U)*
         Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
          Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd2, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, 
      MT2, MT2, MT2, MT2]*((MH2^2 + MH2*(S - T - 2*T24 - U) - S*(2*S34 + U) + 
        T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]]*(2*(-MH2 + S34 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
          2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH2 + S + T)*Pair[e[1], k[4]]*
           Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
            (T14 + T24)*Pair[e[2], k[3]] + (-MH2 + S + U)*Pair[e[2], k[4]])))*
       Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
        (MH2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 
      4*((-T + U)*Pair[e[1], k[2]] + (MH2 - S - U)*Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH2 - T)*Pair[ec[5], k[2]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[3]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     (2*Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((MH2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S34 - T14 - T24, MH2, 0, 
      T, S, MT2, MT2, MT2, MT2]*
     (((-MH2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
        Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
      4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*((MH2^2 - S*(2*S34 + T) + MH2*(S - T - 2*T14 - U) + 
        (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + 
          (MH2 - S - T)*Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
         (2*(-MH2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
          2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
         ((MH2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
          2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH2 - S - U)*Pair[e[2], k[4]]))*
       Pair[ec[4], ec[5]] + 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + U)*Pair[ec[4], k[1]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-(S*(S34 + T)) - T*T24 + 
          MH2*(S - T14 + T24) + T14*U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] + 
            S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]])) + 
      2*((Pair[e[1], k[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
            (MH2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
           (Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(Pair[e[1], e[2]]*((-(MH2*(S + T14 - T24)) - T*T24 + 
           T14*U + S*(S34 + U))*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
             (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
           ((MH2 - U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
      2*((Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
            S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
            (-MH2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[MH2 - S34 - T14 - T24, 0]*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
  ((-4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-2*MH2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
       ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
        Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     ((Pair[e[1], k[4]] + Pair[e[1], k[5]])*(Pair[e[2], k[4]] - 
        Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd133, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-MH2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
        2*(MH2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd3, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + T + 2*T14)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*((-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        2*(MH2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
       Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*
       Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
        Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd113, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((2*(-MH2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
        S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-MH2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
        2*(MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
       ((-MH2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
        2*(MH2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*
       Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (-((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, MH2 - S34 - T14 - T24, MH2, S, MT2, 
      MT2, MT2]*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
          Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
       Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd003, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(-2*Pair[e[2], k[1]]*
       ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
         Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-(((MH2 - S - 2*T24 - U)*Pair[e[1], k[3]]*
          Pair[e[2], k[1]] - Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*
            Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*
        Pair[ec[4], ec[5]]) + 
      4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
           Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
        (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 
        2*Pair[e[1], k[5]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd001, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(2*Pair[e[1], k[2]]*
       ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
      4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
          Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2, MH2 - S34 - T14 - T24, 0, 
      U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH2 - S - 2*T24 - U)*
         Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
          Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd2, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, 
      MT2, MT2, MT2, MT2]*((MH2^2 + MH2*(S - T - 2*T24 - U) - S*(2*S34 + U) + 
        T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]]*(2*(-MH2 + S34 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
          2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH2 + S + T)*Pair[e[1], k[4]]*
           Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
            (T14 + T24)*Pair[e[2], k[3]] + (-MH2 + S + U)*Pair[e[2], k[4]])))*
       Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
        (MH2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 
      4*((-T + U)*Pair[e[1], k[2]] + (MH2 - S - U)*Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH2 - T)*Pair[ec[5], k[2]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[3]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     (2*Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((MH2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S34 - T14 - T24, MH2, 0, 
      T, S, MT2, MT2, MT2, MT2]*
     (((-MH2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
        Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
      4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*((MH2^2 - S*(2*S34 + T) + MH2*(S - T - 2*T14 - U) + 
        (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + 
          (MH2 - S - T)*Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
         (2*(-MH2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
          2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
         ((MH2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
          2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH2 - S - U)*Pair[e[2], k[4]]))*
       Pair[ec[4], ec[5]] + 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + U)*Pair[ec[4], k[1]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-(S*(S34 + T)) - T*T24 + 
          MH2*(S - T14 + T24) + T14*U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] + 
            S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]])) + 
      2*((Pair[e[1], k[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
            (MH2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
           (Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(Pair[e[1], e[2]]*((-(MH2*(S + T14 - T24)) - T*T24 + 
           T14*U + S*(S34 + U))*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
             (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
           ((MH2 - U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
      2*((Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
            S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
            (-MH2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[MH2 - S34 - T14 - T24, 0]*Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
  ((4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-2*MH2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
       ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
        Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     ((Pair[e[1], k[4]] + Pair[e[1], k[5]])*(Pair[e[2], k[4]] - 
        Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd133, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-MH2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
        2*(MH2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + T + 2*T14)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*((-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        2*(MH2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
       Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*
       Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
        Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd113, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((2*(-MH2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
        S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd1, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-MH2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
        2*(MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
       ((-MH2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
        2*(MH2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*
       Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (-((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, MH2 - S34 - T14 - T24, MH2, S, MT2, 
      MT2, MT2]*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
          Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
       Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd003, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(-2*Pair[e[2], k[1]]*
       ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
         Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-(((MH2 - S - 2*T24 - U)*Pair[e[1], k[3]]*
          Pair[e[2], k[1]] - Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*
            Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*
        Pair[ec[4], ec[5]]) + 
      4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
           Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
        (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 
        2*Pair[e[1], k[5]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd001, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(2*Pair[e[1], k[2]]*
       ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
      4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
          Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2, MH2 - S34 - T14 - T24, 0, 
      U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH2 - S - 2*T24 - U)*
         Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
          Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd2, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, 
      MT2, MT2, MT2, MT2]*((MH2^2 + MH2*(S - T - 2*T24 - U) - S*(2*S34 + U) + 
        T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]]*(2*(-MH2 + S34 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
          2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH2 + S + T)*Pair[e[1], k[4]]*
           Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
            (T14 + T24)*Pair[e[2], k[3]] + (-MH2 + S + U)*Pair[e[2], k[4]])))*
       Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
        (MH2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 
      4*((-T + U)*Pair[e[1], k[2]] + (MH2 - S - U)*Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH2 - T)*Pair[ec[5], k[2]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[3]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     (2*Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*C0i[cc0, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((MH2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S34 - T14 - T24, MH2, 0, 
      T, S, MT2, MT2, MT2, MT2]*
     (((-MH2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
        Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
      4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*((MH2^2 - S*(2*S34 + T) + MH2*(S - T - 2*T14 - U) + 
        (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + 
          (MH2 - S - T)*Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
         (2*(-MH2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
          2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
         ((MH2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
          2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH2 - S - U)*Pair[e[2], k[4]]))*
       Pair[ec[4], ec[5]] + 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + U)*Pair[ec[4], k[1]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-(S*(S34 + T)) - T*T24 + 
          MH2*(S - T14 + T24) + T14*U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] + 
            S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]])) + 
      2*((Pair[e[1], k[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
            (MH2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
           (Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(Pair[e[1], e[2]]*((-(MH2*(S + T14 - T24)) - T*T24 + 
           T14*U + S*(S34 + U))*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
             (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
           ((MH2 - U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
      2*((Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
            S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
            (-MH2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW)) + 
 Den[MH2 - S34 - T14 - T24, 0]*Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
  ((4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-2*MH2 + S + 4*T24 + 2*U)*Pair[e[2], k[1]] + 
         S*(Pair[e[2], k[3]] + 2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 8*Pair[e[2], k[1]]*
       ((Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + 
        Pair[ec[4], k[2]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      2*S*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     ((Pair[e[1], k[4]] + Pair[e[1], k[5]])*(Pair[e[2], k[4]] - 
        Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 2*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-3*MH2 + 3*S + 3*T + 4*T14)*Pair[e[1], k[4]] + (-MH2 + S + T + 4*T14)*
         Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     ((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - T - 2*T14)*(Pair[e[1], k[2]] - 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(Pair[e[1], k[4]] + Pair[e[1], k[5]])*
       (2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd133, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*Pair[e[2], k[1]]*
     ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     ((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[1]]*
     (((-MH2 + T)*Pair[e[1], k[2]] + (S + 2*T14)*Pair[e[1], k[3]] + 
        2*(MH2 - T)*Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - T)*(Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[1], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + T + 2*T14)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*((-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
        2*(MH2 - S - T - T14)*Pair[e[1], k[4]] - 2*T14*Pair[e[1], k[5]])*
       Pair[e[2], k[1]]*Pair[ec[4], ec[5]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[3]]*
       Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
        Pair[ec[4], k[1]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd22, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (((-3*MH2 + 3*S + 4*T24 + 3*U)*Pair[e[2], k[4]] + (-MH2 + S + 4*T24 + U)*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 2*(-MH2 + S + U)*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*(-MH2 + S + U)*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      8*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     ((MH2 - S - 2*T24 - U)*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], ec[5]] + 4*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(-((MH2 - S - 2*T24 - U)*(Pair[e[1], k[2]] + 
         Pair[e[1], k[3]])*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]]) - 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[4]] - 
        Pair[e[1], k[5]])*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
       (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd113, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*Pair[e[1], k[2]]*Pair[e[2], k[1]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
     ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
      4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     ((2*(-MH2 + S + 2*T24 + U)*Pair[e[2], k[1]] + 
        S*(Pair[e[2], k[4]] - Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      2*S*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*S*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[2], k[1]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd1, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[2]]*
     (((-MH2 + U)*Pair[e[2], k[1]] + (S + 2*T24)*Pair[e[2], k[3]] + 
        2*(MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      2*(MH2 - U)*(Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + 4*Pair[e[2], k[3]]*
       (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
         Pair[ec[5], k[4]])))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(-(S*(-MH2 + S + 2*T24 + U)*Pair[e[1], e[2]]*
        Pair[ec[4], ec[5]]) + 2*Pair[e[1], k[2]]*
       ((-MH2 + S + U)*Pair[e[2], k[1]] + 2*T24*Pair[e[2], k[3]] + 
        2*(MH2 - S - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + U)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
      4*S*Pair[e[1], e[2]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 8*Pair[e[1], k[2]]*
       Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
        Pair[ec[4], k[2]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (-((2*(-MH2 + S + T + 2*T14)*Pair[e[1], k[2]] + 
         S*(Pair[e[1], k[4]] - Pair[e[1], k[5]]))*Pair[ec[4], ec[5]]) - 
      2*S*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] + 2*S*Pair[e[1], ec[4]]*
       Pair[ec[5], k[4]] + 8*Pair[e[1], k[2]]*
       (Pair[ec[4], k[5]]*(-Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        (Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]])))/
    (MW*SW) + (Alfas2*EL*MT2*C0i[cc0, MH2 - S34 - T14 - T24, MH2, S, MT2, 
      MT2, MT2]*(-2*(Pair[e[1], k[5]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[4]]) + 2*Pair[e[1], k[2]]*(Pair[e[2], k[4]] - 
          Pair[e[2], k[5]]) - Pair[e[1], k[4]]*(Pair[e[2], k[1]] + 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] - 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
      4*(3*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-2*(Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[4]]) - Pair[e[1], k[5]]*(Pair[e[2], k[1]] - 
          2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*
       Pair[ec[4], k[5]] - 4*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[3]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd003, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(-2*Pair[e[2], k[1]]*
       ((Pair[e[1], k[4]] - Pair[e[1], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[1], ec[4]]*
         Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + T + 2*T14)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
      MT2, MT2, MT2]*(-(((MH2 - S - 2*T24 - U)*Pair[e[1], k[3]]*
          Pair[e[2], k[1]] - Pair[e[1], k[2]]*((T + 2*T14 - 2*T24 - U)*
            Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*
        Pair[ec[4], ec[5]]) + 
      4*(-(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]] - 
           Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
           Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
        (Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(2*(-2*Pair[e[1], k[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], k[2]]*(Pair[e[2], k[4]] - Pair[e[2], k[5]]) + 
        2*Pair[e[1], k[5]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
      4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, 
       MT2, MT2, MT2] + D0i[dd001, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, 
       MT2, MT2, MT2, MT2])*(2*Pair[e[1], k[2]]*
       ((Pair[e[2], k[4]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 2*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]]) - Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-MH2 + S + 2*T24 + U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (-(((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T + 2*T14)*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 
      4*(Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
          Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2, MH2 - S34 - T14 - T24, 0, 
      U, S, MT2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (((-T - 2*T14 + 2*T24 + U)*Pair[e[1], k[2]] + (MH2 - S - 2*T24 - U)*
         Pair[e[1], k[3]])*Pair[ec[4], ec[5]] + 
      4*(Pair[e[1], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
          Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[ec[4], k[5]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd2, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, 
      MT2, MT2, MT2, MT2]*((MH2^2 + MH2*(S - T - 2*T24 - U) - S*(2*S34 + U) + 
        T*(2*T24 + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
      2*(Pair[e[1], k[2]]*(2*(-MH2 + S34 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14 + 2*T24)*Pair[e[2], k[3]] + 
          2*(T - U)*Pair[e[2], k[4]]) + 2*((-MH2 + S + T)*Pair[e[1], k[4]]*
           Pair[e[2], k[3]] + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] - 
            (T14 + T24)*Pair[e[2], k[3]] + (-MH2 + S + U)*Pair[e[2], k[4]])))*
       Pair[ec[4], ec[5]] - 4*((-T + U)*Pair[e[1], k[2]] + 
        (MH2 - S - U)*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 
      4*(-MH2 + S + T)*Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]] + 
      4*((-T + U)*Pair[e[1], k[2]] + (MH2 - S - U)*Pair[e[1], k[3]])*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*(-MH2 + S + T)*
       Pair[e[1], ec[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[4]] + 
      4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH2 - T)*Pair[ec[5], k[2]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + T)*Pair[ec[4], k[2]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
         (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[1]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*Pair[e[2], k[3]]*((Pair[e[1], k[4]] - Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[1], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc2, MH2 - S34 - T14 - T24, MH2, S, MT2, MT2, MT2]*
     (2*Pair[e[1], k[2]]*((Pair[e[2], k[4]] - Pair[e[2], k[5]])*
         Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
        2*Pair[e[2], ec[4]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
       ((-2*MH2 + 2*S34 + T + U)*Pair[ec[4], ec[5]] - 
        4*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*C0i[cc0, MH2, MH2 - S34 - T14 - T24, S, MT2, MT2, MT2]*
     (2*(2*Pair[e[1], k[5]]*Pair[e[2], k[4]] + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]] - Pair[e[2], k[5]]) - 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
      4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
       Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[3]])*Pair[ec[5], k[4]] - Pair[e[1], e[2]]*
       ((MH2 + S - 2*S34 + 2*T14 - U)*Pair[ec[4], ec[5]] + 
        4*(-(Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S34 - T14 - T24, MH2, 0, 
      T, S, MT2, MT2, MT2, MT2]*
     (((-MH2 + S + 2*T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
        Pair[e[1], k[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[e[2], k[1]] + 
          (-MH2 + S + T + 2*T14)*Pair[e[2], k[3]]))*Pair[ec[4], ec[5]] + 
      4*((Pair[e[1], k[4]] + Pair[e[1], k[5]])*Pair[e[2], k[1]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*((MH2^2 - S*(2*S34 + T) + MH2*(S - T - 2*T14 - U) + 
        (T + 2*T14)*U)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
      2*(2*Pair[e[1], k[4]]*((T - U)*Pair[e[2], k[1]] + 
          (MH2 - S - T)*Pair[e[2], k[3]]) + Pair[e[1], k[2]]*
         (2*(-MH2 + S + T14 + T24 + U)*Pair[e[2], k[1]] - 
          2*T14*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*
         ((MH2 - S - 2*T14 - 2*T24 - U)*Pair[e[2], k[1]] + 
          2*(T14 + T24)*Pair[e[2], k[3]] + 2*(MH2 - S - U)*Pair[e[2], k[4]]))*
       Pair[ec[4], ec[5]] + 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[4], k[5]] - 4*(-MH2 + S + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*
       ((-T + U)*Pair[e[2], k[1]] + (-MH2 + S + T)*Pair[e[2], k[3]])*
       Pair[ec[5], k[4]] + 4*Pair[e[1], e[2]]*
       (Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] - 
          S*Pair[ec[5], k[3]]) + ((-MH2 + U)*Pair[ec[4], k[1]] + 
          S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]) - 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]] + Pair[e[2], k[5]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*
           Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
         (Pair[e[2], k[1]] - Pair[e[2], k[3]])*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*
           Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
         (Pair[e[2], k[4]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[5]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[4]]) + Pair[e[2], k[3]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S34 - T14 - T24, MH2, 0, T, S, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], e[2]]*((-(S*(S34 + T)) - T*T24 + 
          MH2*(S - T14 + T24) + T14*U)*Pair[ec[4], ec[5]] + 
        2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          ((-MH2 + U)*Pair[ec[4], k[1]] + (MH2 - T)*Pair[ec[4], k[2]] + 
            S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]])) + 
      2*((Pair[e[1], k[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[3]]) - Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + T)*Pair[e[2], k[1]] + T14*Pair[e[2], k[3]] + 
            (MH2 - T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] + Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
           (-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + Pair[ec[4], k[2]]*
             Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
           (Pair[e[2], k[3]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
              Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*Pair[
                ec[5], k[4]]))))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, MH2, MH2 - S34 - T14 - T24, 0, U, S, MT2, MT2, 
      MT2, MT2]*(-(Pair[e[1], e[2]]*((-(MH2*(S + T14 - T24)) - T*T24 + 
           T14*U + S*(S34 + U))*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[5]]*((MH2 - U)*Pair[ec[5], k[1]] + 
             (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) - 
           ((MH2 - U)*Pair[ec[4], k[1]] + (-MH2 + T)*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]])*Pair[ec[5], k[4]]))) + 
      2*((Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
            S*Pair[e[2], k[3]]) + Pair[e[1], k[3]]*(T24*Pair[e[2], k[1]] + 
            S*Pair[e[2], k[4]]) + Pair[e[1], k[2]]*
           ((-MH2 + S34 + U)*Pair[e[2], k[1]] - T14*Pair[e[2], k[3]] + 
            (-MH2 + T)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[5]] - Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
        ((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[4]] + Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] - 
        2*(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] - Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[1]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[3]]) + 
              Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))/(MW*SW))
