(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {(Alfa^2*MT^2*
    ((-(((-3 + 8*SW^2)*(36*MT^2 + MH^2*(9 - 24*SW^2 + 32*SW^4) + 
           (MZ^2 - S)*(9 - 24*SW^2 + 32*SW^4))*
          (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
           DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
           DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
           DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
           DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
           DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
           DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
           DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
           DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
             (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                 MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^
                2, MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
              Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
            -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
           DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
             (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                 MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
           DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
         Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 
       4*(-27 + 108*SW^2 - 144*SW^4 + 128*SW^6)*(2 + Eps^(-1) + 
         Log[Mu^2/MT^2] + (Sqrt[S*(-4*MT^2 + S)]*
           Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
       ((-3 + 8*SW^2)*(9*MH^2 - 9*S + MZ^2*(45 - 96*SW^2 + 128*SW^4))*
         (-((MH^2*(MH^2 - MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
            Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
          (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S))/
        Kallen\[Lambda][MH^2, MZ^2, S] + 
       (2*(8*MH^2*SW^2*(9 - 36*SW^2 + 32*SW^4) + (2*MH^2 + MZ^2 - S)*
           (3 - 8*SW^2)*(9 - 24*SW^2 + 32*SW^4))*
         (-((MZ^2*(-MH^2 + MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
          (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*
            Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
          2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
              (2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S))/
        Kallen\[Lambda][MH^2, MZ^2, S] + 2*(-3 + 8*SW^2)*
        (9 - 24*SW^2 + 32*SW^4)*(3 + Eps^(-1) + 
         (2*(MH^4*MT^2 + MT^2*(MZ^2 - S)^2 + MH^2*(MZ^2*S - 2*MT^2*(MZ^2 + 
                S)))*(DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
          Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 
         (MH*Sqrt[MH^2 - 4*MT^2]*(MH^2 - MZ^2 - S)*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/
          Kallen\[Lambda][MH^2, MZ^2, S] + Log[Mu^2/MT^2] + 
         (MZ*Sqrt[-4*MT^2 + MZ^2]*(-MH^2 + MZ^2 - S)*
           Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/
          Kallen\[Lambda][MH^2, MZ^2, S] + (Sqrt[S*(-4*MT^2 + S)]*
           (-MH^2 - MZ^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
             (2*MT^2)])/Kallen\[Lambda][MH^2, MZ^2, S]))*
      Mat[DiracChain[Spinor[k[1], MU, -1], 1, ec[4], Spinor[k[2], MU, 1]]*
        SUNT[Col1, Col2]] + 
     ((-3*(36*MT^2 + MH^2*(9 - 24*SW^2 + 32*SW^4) + (MZ^2 - S)*
           (9 - 24*SW^2 + 32*SW^4))*
         (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
        Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] - 12*(9 - 12*SW^2 + 16*SW^4)*
        (2 + Eps^(-1) + Log[Mu^2/MT^2] + (Sqrt[S*(-4*MT^2 + S)]*
           Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
       (3*(9*MH^2 - 9*S + MZ^2*(45 - 96*SW^2 + 128*SW^4))*
         (-((MH^2*(MH^2 - MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
            Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
          (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S))/
        Kallen\[Lambda][MH^2, MZ^2, S] - 
       (2*(6*MH^2*(9 - 12*SW^2 + 16*SW^4) + 3*(MZ^2 - S)*(9 - 24*SW^2 + 
            32*SW^4))*(-((MZ^2*(-MH^2 + MZ^2 - S)*
             (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
          (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*
            Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
          2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
              (2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S))/
        Kallen\[Lambda][MH^2, MZ^2, S] + 6*(9 - 24*SW^2 + 32*SW^4)*
        (3 + Eps^(-1) + (2*(MH^4*MT^2 + MT^2*(MZ^2 - S)^2 + 
            MH^2*(MZ^2*S - 2*MT^2*(MZ^2 + S)))*
           (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
          Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 
         (MH*Sqrt[MH^2 - 4*MT^2]*(MH^2 - MZ^2 - S)*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/
          Kallen\[Lambda][MH^2, MZ^2, S] + Log[Mu^2/MT^2] + 
         (MZ*Sqrt[-4*MT^2 + MZ^2]*(-MH^2 + MZ^2 - S)*
           Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/
          Kallen\[Lambda][MH^2, MZ^2, S] + (Sqrt[S*(-4*MT^2 + S)]*
           (-MH^2 - MZ^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
             (2*MT^2)])/Kallen\[Lambda][MH^2, MZ^2, S]))*
      Mat[DiracChain[Spinor[k[1], MU, -1], 5, ec[4], Spinor[k[2], MU, 1]]*
        SUNT[Col1, Col2]] - 
     (12*MU*((9 - 24*SW^2 + 32*SW^4)*
         (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]])*
         Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 
        9*Kallen\[Lambda][MH^2, MZ^2, S]*
         (-((MH^2*(MH^2 - MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
            Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
          (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
        2*(9 - 24*SW^2 + 32*SW^4)*
         ((2*(MH^6*(MT^2 + MZ^2) + MT^2*(MZ^2 - S)^3 + MH^2*(MZ^2 - S)*
              (MZ^4 + 2*MZ^2*S - MT^2*(MZ^2 + 3*S)) - 
             MH^4*(2*MZ^4 - MZ^2*S + MT^2*(MZ^2 + 3*S)))*
            (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]]))/
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + (MH^2 + MZ^2 - S)*
           Kallen\[Lambda][MH^2, MZ^2, S] + MH*Sqrt[MH^2 - 4*MT^2]*
           (MH^4 - 5*MZ^4 + MH^2*(4*MZ^2 - 2*S) + 4*MZ^2*S + S^2)*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          MZ*Sqrt[-4*MT^2 + MZ^2]*(-5*MH^4 + (MZ^2 - S)^2 + 
            4*MH^2*(MZ^2 + S))*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
              (2*MT^2)] - (Sqrt[S*(-4*MT^2 + S)]*(MH^6 - MH^4*(MZ^2 + 2*S) + 
             (MZ^3 - MZ*S)^2 + MH^2*(-MZ^4 + 8*MZ^2*S + S^2))*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S - 
          Kallen\[Lambda][MH^2, MZ^2, S]*
           (-((MZ^2*(-MH^2 + MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MZ*(-MH^2 + MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MZ*(-MH^2 + MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[-((MZ*(-MH^2 + MZ^2 - S - 
                     Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[
                      Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]]))/Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
            (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*Log[(-MH^2 + 2*MT^2 + 
                 MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
            2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + 
                    MZ^2]))/(2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*
              (-MH^2 + MZ^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
                (2*MT^2)])/S)))*Mat[DiracChain[Spinor[k[1], MU, -1], 5, 
          Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*Pair[ec[4], k[3]])/
      Kallen\[Lambda][MH^2, MZ^2, S]^2 - 
     (2*((3 - 8*SW^2)*(9 - 24*SW^2 + 32*SW^4)*
         (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]])*
         Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) - 9*(-3 + 8*SW^2)*
         Kallen\[Lambda][MH^2, MZ^2, S]*
         (-((MH^2*(MH^2 - MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
            Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
          (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
        2*(3 - 8*SW^2)*(9 - 24*SW^2 + 32*SW^4)*
         ((-2*MZ^2*(MH^4*(2*MT^2 + MZ^2) + (2*MT^2 + MZ^2)*(MZ^2 - S)^2 - 
             2*MH^2*(MZ^4 - 2*MZ^2*S + 2*MT^2*(MZ^2 + S)))*
            (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]]))/
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + 
          (2*(MH^6*(MT^2 + MZ^2) + MT^2*(MZ^2 - S)^3 + MH^2*(MZ^2 - S)*
              (MZ^4 + 2*MZ^2*S - MT^2*(MZ^2 + 3*S)) - 
             MH^4*(2*MZ^4 - MZ^2*S + MT^2*(MZ^2 + 3*S)))*
            (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]]))/
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] - 
          2*MZ^2*Kallen\[Lambda][MH^2, MZ^2, S] + (MH^2 + MZ^2 - S)*
           Kallen\[Lambda][MH^2, MZ^2, S] + MH*Sqrt[MH^2 - 4*MT^2]*
           (MH^4 - 5*MZ^4 + MH^2*(4*MZ^2 - 2*S) + 4*MZ^2*S + S^2)*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[MH^2 - 4*MT^2]*(MH^6 + (MZ^2 - S)^2*(3*MZ^2 - S) - 
             MH^4*(5*MZ^2 + 3*S) + MH^2*(MZ^4 + 3*S^2))*
            Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH + 
          6*MZ^3*Sqrt[-4*MT^2 + MZ^2]*(MH^2 - MZ^2 + S)*
           Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)] + 
          MZ*Sqrt[-4*MT^2 + MZ^2]*(-5*MH^4 + (MZ^2 - S)^2 + 
            4*MH^2*(MZ^2 + S))*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
              (2*MT^2)] - (Sqrt[S*(-4*MT^2 + S)]*(MH^6 - MH^4*(MZ^2 + 2*S) + 
             (MZ^3 - MZ*S)^2 + MH^2*(-MZ^4 + 8*MZ^2*S + S^2))*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S + 
          (Sqrt[S*(-4*MT^2 + S)]*(-MH^6 + 3*MZ^6 + MZ^4*S - 5*MZ^2*S^2 + 
             S^3 + MH^4*(5*MZ^2 + 3*S) - MH^2*(7*MZ^4 + 3*S^2))*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S - 
          2*Kallen\[Lambda][MH^2, MZ^2, S]*
           (-((MZ^2*(-MH^2 + MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MZ*(-MH^2 + MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MZ*(-MH^2 + MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[-((MZ*(-MH^2 + MZ^2 - S - 
                     Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[
                      Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]]))/Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
            (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*Log[(-MH^2 + 2*MT^2 + 
                 MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
            2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + 
                    MZ^2]))/(2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*
              (-MH^2 + MZ^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
                (2*MT^2)])/S)))*Mat[DiracChain[Spinor[k[1], MU, -1], 1, k[3], 
          Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*Pair[ec[4], k[3]])/
      Kallen\[Lambda][MH^2, MZ^2, S]^2 + 
     (6*((9 - 24*SW^2 + 32*SW^4)*
         (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]])*
         Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 
        9*Kallen\[Lambda][MH^2, MZ^2, S]*
         (-((MH^2*(MH^2 - MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                  Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                 MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                    MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                  MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[
                   Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                 Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
               MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                     MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[
                   (-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]])), 
               -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
              DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
              DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                   Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
            Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
          (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
        2*(9 - 24*SW^2 + 32*SW^4)*((-2*MZ^2*(MH^4*(2*MT^2 + MZ^2) + 
             (2*MT^2 + MZ^2)*(MZ^2 - S)^2 - 2*MH^2*(MZ^4 - 2*MZ^2*S + 2*MT^2*
                (MZ^2 + S)))*(DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[
                  Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
                Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
              MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]]))/
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + 
          (2*(MH^6*(MT^2 + MZ^2) + MT^2*(MZ^2 - S)^3 + MH^2*(MZ^2 - S)*
              (MZ^4 + 2*MZ^2*S - MT^2*(MZ^2 + 3*S)) - 
             MH^4*(2*MZ^4 - MZ^2*S + MT^2*(MZ^2 + 3*S)))*
            (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                    S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                   Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] + 
             DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]] - 
             DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                  Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - Sqrt[
                Kallen\[Lambda][MH^2, MZ^2, S]]]))/
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] - 
          2*MZ^2*Kallen\[Lambda][MH^2, MZ^2, S] + (MH^2 + MZ^2 - S)*
           Kallen\[Lambda][MH^2, MZ^2, S] + MH*Sqrt[MH^2 - 4*MT^2]*
           (MH^4 - 5*MZ^4 + MH^2*(4*MZ^2 - 2*S) + 4*MZ^2*S + S^2)*
           Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
          (Sqrt[MH^2 - 4*MT^2]*(MH^6 + (MZ^2 - S)^2*(3*MZ^2 - S) - 
             MH^4*(5*MZ^2 + 3*S) + MH^2*(MZ^4 + 3*S^2))*
            Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH + 
          6*MZ^3*Sqrt[-4*MT^2 + MZ^2]*(MH^2 - MZ^2 + S)*
           Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)] + 
          MZ*Sqrt[-4*MT^2 + MZ^2]*(-5*MH^4 + (MZ^2 - S)^2 + 
            4*MH^2*(MZ^2 + S))*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
              (2*MT^2)] - (Sqrt[S*(-4*MT^2 + S)]*(MH^6 - MH^4*(MZ^2 + 2*S) + 
             (MZ^3 - MZ*S)^2 + MH^2*(-MZ^4 + 8*MZ^2*S + S^2))*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S + 
          (Sqrt[S*(-4*MT^2 + S)]*(-MH^6 + 3*MZ^6 + MZ^4*S - 5*MZ^2*S^2 + 
             S^3 + MH^4*(5*MZ^2 + 3*S) - MH^2*(7*MZ^4 + 3*S^2))*
            Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S - 
          2*Kallen\[Lambda][MH^2, MZ^2, S]*
           (-((MZ^2*(-MH^2 + MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MH*(MH^2 - MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - 
                   MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), -MH^2 + MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[(MZ*(-MH^2 + MZ^2 - S - 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] - DiLog[(MZ*(-MH^2 + MZ^2 - S + 
                    Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*MZ) + 
                   MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
                      MZ^2, S]]), MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][
                    MH^2, MZ^2, S]]] + DiLog[-((MZ*(-MH^2 + MZ^2 - S - 
                     Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[
                      Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*MZ - MZ^3 + 
                    MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, 
                       S]])), -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[
                     Kallen\[Lambda][MH^2, MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + 
                   S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, 
                      S]]), MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
                    MZ^2, S]]]))/Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
            (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*Log[(-MH^2 + 2*MT^2 + 
                 MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
            2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + 
                    MZ^2]))/(2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*
              (-MH^2 + MZ^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/
                (2*MT^2)])/S)))*Mat[DiracChain[Spinor[k[1], MU, -1], 5, k[3], 
          Spinor[k[2], MU, 1]]*SUNT[Col1, Col2]]*Pair[ec[4], k[3]])/
      Kallen\[Lambda][MH^2, MZ^2, S]^2))/(144*CW^3*MW*(-MZ^2 + S)*SW^4)}}
