(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas^2*col3*EL*MT^2*
    ((-2*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
           2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
          ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
            Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*
          ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] - 
       4*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
           2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
           2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
         2*Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
           2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
        PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       Pair[ec[4], k[3]]*(16*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] + 
           Pair[e[1], ec[5]]*Pair[e[2], k[4]] - 2*Pair[e[1], e[2]]*
            Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] + 
           Pair[e[1], ec[5]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
            Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
         Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*
          Pair[ec[4], ec[5]])*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
      S34 + (2*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 
           2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
          ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
            Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*
          ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       4*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
           2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
         Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
           2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
         2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
           2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
        PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2]] + Pair[ec[5], k[3]]*
        (16*(Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
            Pair[e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
          PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
           Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
           Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
            Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
           Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
       (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
          Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
        (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
         16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
           Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW)) - 
 (Alfas^2*col4*EL*MT^2*
   ((-2*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
         ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*
         ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      Pair[ec[4], k[3]]*(16*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[4]] - 2*Pair[e[1], e[2]]*
           Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
           Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*
         Pair[ec[4], ec[5]])*(4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     S34 + (2*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
         ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*
         ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      4*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + Pair[ec[5], k[3]]*
       (16*(Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
           Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW) + 
 (Alfas^2*col1*EL*MT^2*
   ((-2*(2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
         ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*
         ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      16*Pair[ec[4], k[3]]*((-2*Pair[e[1], k[4]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 
          Pair[e[1], ec[5]]*Pair[e[2], k[3]] - Pair[e[1], e[2]]*
           Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     S34 + (-2*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
         ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*
         ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + Pair[ec[5], k[3]]*
       (-16*(Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + 16*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 
          2*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW) + 
 (Alfas^2*col2*EL*MT^2*
   ((-2*(2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
         ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*
         ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
        Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      16*Pair[ec[4], k[3]]*((-2*Pair[e[1], k[4]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] + (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 
          Pair[e[1], ec[5]]*Pair[e[2], k[3]] - Pair[e[1], e[2]]*
           Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     S34 + (-2*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 
          2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
         ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
           Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*
         ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + Pair[ec[5], k[3]]*
       (-16*(Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] + 16*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 
          2*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW) - 
 (Alfas^2*col5*EL*MT^2*
   ((-2*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
         ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*
         ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      Pair[ec[4], k[3]]*(16*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 
          2*Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 
          2*Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     S34 + (-2*(2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*
           Pair[e[1], ec[5]] + 2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 
          2*Pair[e[2], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
         ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + Pair[ec[5], k[3]]*
       (16*(-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] - 16*(-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW) - 
 (Alfas^2*col6*EL*MT^2*
   ((-2*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
         ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*
         ((-MH^2 + S34)*Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*
           Pair[ec[5], k[4]]))*PVC[0, 0, 0, MH^2, S34, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
        2*Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
        Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
          2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])))*
       PVC[0, 1, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
      Pair[ec[4], k[3]]*(16*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 
          2*Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[4]])*PVC[0, 1, 1, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]] - 16*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 
          2*Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[3]])*PVC[0, 2, 0, MH^2, S34, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, S34, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, S34, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]))/
     S34 + (-2*(2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*
           Pair[e[1], ec[5]] + 2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 
          2*Pair[e[2], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
         ((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
           Pair[ec[5], k[3]]))*PVC[0, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
      4*(2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
          2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
          2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]]))*
       PVC[0, 1, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2]] + Pair[ec[5], k[3]]*
       (16*(-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
           Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]])*
         PVC[0, 1, 1, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], Sqrt[MT^2], 
          Sqrt[MT^2]] - 16*(-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[3]])*PVC[0, 2, 0, MH^2, 2*MH^2 - S34 - T - U, 0, 
          Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
      (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]])*
       (4*PVB[0, 0, 2*MH^2 - S34 - T - U, Sqrt[MT^2], Sqrt[MT^2]] - 
        16*PVC[1, 0, 0, MH^2, 2*MH^2 - S34 - T - U, 0, Sqrt[MT^2], 
          Sqrt[MT^2], Sqrt[MT^2]]))/(2*MH^2 - S34 - T - U)))/(MW*SW)
