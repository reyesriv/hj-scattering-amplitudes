(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-(Alfas*EL*GS*MT^2*Mat[SUNT[Glu4, Col2, Col1]]*
   (2*PVB[0, 0, S, Sqrt[MT^2], Sqrt[MT^2]]*
     (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], Spinor[k[1], MD, 1, 1, 
        0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], 
       Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, 
       ec[4], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 
        0], 7, ec[4], Spinor[k[1], MU, 1, 1, 0]]) + 
    PVC[0, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]*
     ((MH^2 - S)*WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], 
        Spinor[k[1], MD, 1, 1, 0]] + (MH^2 - S)*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], Spinor[k[1], MD, 1, 
          1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, ec[4], 
         Spinor[k[1], MU, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         7, ec[4], Spinor[k[1], MU, 1, 1, 0]]) - 2*Pair[ec[4], k[3]]*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, k[3], Spinor[k[1], MD, 1, 1, 
          0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, k[3], 
         Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         6, k[3], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[
         Spinor[k[2], MU, -1, 2, 0], 7, k[3], Spinor[k[1], MU, 1, 1, 0]])) - 
    2*(4*PVC[1, 0, 0, MH^2, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]*
       (WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, ec[4], Spinor[k[1], MD, 1, 
          1, 0]] + WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], 
         Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
         6, ec[4], Spinor[k[1], MU, 1, 1, 0]] + 
        WeylChain[Spinor[k[2], MU, -1, 2, 0], 7, ec[4], Spinor[k[1], MU, 1, 
          1, 0]]) + 4*Pair[ec[4], k[3]]*(PVC[0, 1, 1, MH^2, S, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]] + PVC[0, 2, 0, MH^2, S, 0, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]])*(WeylChain[Spinor[k[2], MD, -1, 2, 0], 6, 
         k[3], Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MD, -1, 2, 
          0], 7, k[3], Spinor[k[1], MD, 1, 1, 0]] + 
        WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, k[3], Spinor[k[1], MU, 1, 1, 
          0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 7, k[3], 
         Spinor[k[1], MU, 1, 1, 0]]) + PVC[0, 1, 0, MH^2, S, 0, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]]*((-MH^2 + S)*WeylChain[Spinor[k[2], MD, -1, 
           2, 0], 6, ec[4], Spinor[k[1], MD, 1, 1, 0]] - 
        (MH^2 - S)*(WeylChain[Spinor[k[2], MD, -1, 2, 0], 7, ec[4], 
           Spinor[k[1], MD, 1, 1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 
           6, ec[4], Spinor[k[1], MU, 1, 1, 0]] + WeylChain[
           Spinor[k[2], MU, -1, 2, 0], 7, ec[4], Spinor[k[1], MU, 1, 1, 
            0]]) + 4*Pair[ec[4], k[3]]*(WeylChain[Spinor[k[2], MD, -1, 2, 0], 
           6, k[3], Spinor[k[1], MD, 1, 1, 0]] + WeylChain[
           Spinor[k[2], MD, -1, 2, 0], 7, k[3], Spinor[k[1], MD, 1, 1, 0]] + 
          WeylChain[Spinor[k[2], MU, -1, 2, 0], 6, k[3], Spinor[k[1], MU, 1, 
            1, 0]] + WeylChain[Spinor[k[2], MU, -1, 2, 0], 7, k[3], 
           Spinor[k[1], MU, 1, 1, 0]])))))/(4*MW*Pi*S*SW)
