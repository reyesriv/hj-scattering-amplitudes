(* Created with the Wolfram Language : www.wolfram.com *)
-16*Alfas*GH*Pi*((16*Alfas*GH*Pi*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 - 
   (16*Alfas*GH*Pi*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
      2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 - 
   (92*Alfas*GH*Pi*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3)*(Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
   Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*
    Pair[ec[4], ec[5]]) + 16*Alfas*GH*Pi*
  ((16*Alfas*GH*Pi*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 + 
   (92*Alfas*GH*Pi*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
      2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 + 
   (16*Alfas*GH*Pi*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3)*(2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
   Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - Pair[e[1], e[2]]*
    Pair[ec[4], ec[5]]) - 16*Alfas*GH*Pi*
  ((-92*Alfas*GH*Pi*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 - 
   (16*Alfas*GH*Pi*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
      2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3 + 
   (16*Alfas*GH*Pi*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
      Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
       Pair[ec[2], e[5]]))/3)*(Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
   2*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] + Pair[e[1], e[2]]*
    Pair[ec[4], ec[5]])
