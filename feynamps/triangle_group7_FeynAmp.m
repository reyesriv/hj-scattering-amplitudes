(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
Den[T14, 0]*Den[U, 0]*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
  ((-2*Alfas2*EL*MT2*C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
             Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
      4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + (MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], k[4]]*
       Pair[ec[4], ec[5]] + 2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], ec[5]]*
       Pair[ec[4], k[1]] - 8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*((-MH2 + 2*S + 2*T24 + U)*Pair[ec[5], k[1]] + 
        2*(S34 - T)*Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]] + 
        2*T24*Pair[ec[5], k[3]] - MH2*Pair[ec[5], k[4]] + 
        2*S*Pair[ec[5], k[4]] + 2*T24*Pair[ec[5], k[4]] + 
        U*Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
       ((-3*MH2 + 4*S34 + U)*Pair[ec[5], k[1]] + 2*(S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 4*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (4*Alfas2*EL*MT2*B0i[bb0, U, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (16*Alfas2*EL*MT2*C0i[cc00, 0, U, MH2, MT2, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*
           Pair[e[2], k[3]] + 2*(MH2 - U)*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] - 2*(MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*Pair[e[2], k[3]] + 
            2*(MH2 - U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 
          2*(MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*(MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        Pair[e[2], k[3]]*((7*MH2 - 8*S34 - 2*T14 - 3*U)*Pair[ec[5], k[1]] + 
          4*(-2*MH2 + 2*S34 + T14 + 2*T24 + U)*Pair[ec[5], k[3]] + 
          (7*MH2 - 8*T - 2*T14 - 3*U)*Pair[ec[5], k[4]]) + 
        4*(MH2 - U)*(Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[1]]*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[T14, 0]*Den[U, 0]*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
  ((-2*Alfas2*EL*MT2*C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
             Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
      4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + (MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]))))/
    (MW*SW) + (8*Alfas2*EL*MT2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], k[4]]*
       Pair[ec[4], ec[5]] + 2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], ec[5]]*
       Pair[ec[4], k[1]] - 8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*((-MH2 + 2*S + 2*T24 + U)*Pair[ec[5], k[1]] + 
        2*(S34 - T)*Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]] + 
        2*T24*Pair[ec[5], k[3]] - MH2*Pair[ec[5], k[4]] + 
        2*S*Pair[ec[5], k[4]] + 2*T24*Pair[ec[5], k[4]] + 
        U*Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
       ((-3*MH2 + 4*S34 + U)*Pair[ec[5], k[1]] + 2*(S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 4*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (4*Alfas2*EL*MT2*B0i[bb0, U, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (16*Alfas2*EL*MT2*C0i[cc00, 0, U, MH2, MT2, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*
           Pair[e[2], k[3]] + 2*(MH2 - U)*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] - 2*(MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*Pair[e[2], k[3]] + 
            2*(MH2 - U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 
          2*(MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*(MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        Pair[e[2], k[3]]*((7*MH2 - 8*S34 - 2*T14 - 3*U)*Pair[ec[5], k[1]] + 
          4*(-2*MH2 + 2*S34 + T14 + 2*T24 + U)*Pair[ec[5], k[3]] + 
          (7*MH2 - 8*T - 2*T14 - 3*U)*Pair[ec[5], k[4]]) + 
        4*(MH2 - U)*(Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[1]]*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[T14, 0]*Den[U, 0]*Mat[SUNT[Glu1, Glu4, Glu2, Glu5, 0, 0]]*
  ((2*Alfas2*EL*MT2*C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
             Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
      4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + (MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], k[4]]*
       Pair[ec[4], ec[5]] + 2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], ec[5]]*
       Pair[ec[4], k[1]] - 8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*((-MH2 + 2*S + 2*T24 + U)*Pair[ec[5], k[1]] + 
        2*(S34 - T)*Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]] + 
        2*T24*Pair[ec[5], k[3]] - MH2*Pair[ec[5], k[4]] + 
        2*S*Pair[ec[5], k[4]] + 2*T24*Pair[ec[5], k[4]] + 
        U*Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
       ((-3*MH2 + 4*S34 + U)*Pair[ec[5], k[1]] + 2*(S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 4*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (4*Alfas2*EL*MT2*B0i[bb0, U, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (16*Alfas2*EL*MT2*C0i[cc00, 0, U, MH2, MT2, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*
           Pair[e[2], k[3]] + 2*(MH2 - U)*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] - 2*(MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*Pair[e[2], k[3]] + 
            2*(MH2 - U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 
          2*(MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*(MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        Pair[e[2], k[3]]*((7*MH2 - 8*S34 - 2*T14 - 3*U)*Pair[ec[5], k[1]] + 
          4*(-2*MH2 + 2*S34 + T14 + 2*T24 + U)*Pair[ec[5], k[3]] + 
          (7*MH2 - 8*T - 2*T14 - 3*U)*Pair[ec[5], k[4]]) + 
        4*(MH2 - U)*(Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[1]]*Pair[ec[5], k[4]]))))/(MW*SW)) + 
 Den[T14, 0]*Den[U, 0]*Mat[SUNT[Glu1, Glu5, Glu2, Glu4, 0, 0]]*
  ((2*Alfas2*EL*MT2*C0i[cc0, 0, U, MH2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
             Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
      4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + (MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*C0i[cc12, 0, U, MH2, MT2, MT2, MT2]*
     Pair[e[2], k[3]]*(2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], k[4]]*
       Pair[ec[4], ec[5]] + 2*(-MH2 + 2*S + 2*T24 + U)*Pair[e[1], ec[5]]*
       Pair[ec[4], k[1]] - 8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
           (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*((-MH2 + 2*S + 2*T24 + U)*Pair[ec[5], k[1]] + 
        2*(S34 - T)*Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]] + 
        2*T24*Pair[ec[5], k[3]] - MH2*Pair[ec[5], k[4]] + 
        2*S*Pair[ec[5], k[4]] + 2*T24*Pair[ec[5], k[4]] + 
        U*Pair[ec[5], k[4]])))/(MW*SW) + 
   (8*Alfas2*EL*MT2*C0i[cc22, 0, U, MH2, MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-3*MH2 + 2*S34 + 2*T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      8*(Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
          Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
       ((-3*MH2 + 4*S34 + U)*Pair[ec[5], k[1]] + 2*(S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-3*MH2 + 4*T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) - (4*Alfas2*EL*MT2*B0i[bb0, U, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (16*Alfas2*EL*MT2*C0i[cc00, 0, U, MH2, MT2, MT2, MT2]*
     (-2*Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
       Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
       (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
       (-2*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        4*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      4*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] - 4*Pair[e[2], k[4]]*
         Pair[ec[5], k[1]] + 4*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
        Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, U, MH2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*
           Pair[e[2], k[3]] + 2*(MH2 - U)*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] - 2*(MH2 - U)*Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
         (((-5*MH2 + 4*S34 + 4*T + 2*T14 + U)*Pair[e[2], k[3]] + 
            2*(MH2 - U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] - 
          2*(MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[4]]*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          Pair[e[1], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) - 2*(MH2 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
        Pair[e[2], k[3]]*((7*MH2 - 8*S34 - 2*T14 - 3*U)*Pair[ec[5], k[1]] + 
          4*(-2*MH2 + 2*S34 + T14 + 2*T24 + U)*Pair[ec[5], k[3]] + 
          (7*MH2 - 8*T - 2*T14 - 3*U)*Pair[ec[5], k[4]]) + 
        4*(MH2 - U)*(Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[1]]*Pair[ec[5], k[4]]))))/(MW*SW))
