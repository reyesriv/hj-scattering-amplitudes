(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*EL*MT2*Mat[SUNT[Glu1, Glu2, 0, 0]]*
  ((B0i[bb0, MH2, MT2, MT2] - 4*C0i[cc00, 0, MH2, 0, MT2, MT2, MT2])*
    Pair[e[1], e[2]] + 4*C0i[cc12, 0, MH2, 0, MT2, MT2, MT2]*Pair[e[1], k[2]]*
    Pair[e[2], k[1]] - (C0i[cc0, 0, MH2, 0, MT2, MT2, MT2]*
     (-(MH2*Pair[e[1], e[2]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[1]]))/2))/
 (MW*Pi*SW)
