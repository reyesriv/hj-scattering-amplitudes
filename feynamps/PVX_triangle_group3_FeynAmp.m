(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(col5*((2*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVB[0, 0, MH^2 - S34 - T14 - T24, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + 
         S*(-T + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        (Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
           (-MH^2 + T)*Pair[ec[5], k[2]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           (MH^2 - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*S*
      (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*
      ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 
         2*MH^2*(T + 2*T14 - 2*T24 - U) + 2*S34*U - 4*T14*U + U^2)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - T*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*Pair[e[2], k[1]] + 
         2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 2*S34*Pair[e[1], k[2]]*
          Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
         2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
         U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*Pair[e[2], k[5]] + 
         U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          (2*(MH^2 - U)*Pair[ec[5], k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + 
           (T - U)*Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
          Pair[ec[5], k[4]]))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(4*((2*MH^2 - 2*S34 - T - U)*
          (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
         4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(4*(2*MH^2 - 2*S34 - T - U)*
        (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
       16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
         Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(S*(MH^2 - S34 - T14 - T24)) + 
 (col6*((2*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVB[0, 0, MH^2 - S34 - T14 - T24, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + 
         S*(-T + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        (Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
           (-MH^2 + T)*Pair[ec[5], k[2]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           (MH^2 - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*S*
      (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*
      ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 
         2*MH^2*(T + 2*T14 - 2*T24 - U) + 2*S34*U - 4*T14*U + U^2)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - T*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*Pair[e[2], k[1]] + 
         2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 2*S34*Pair[e[1], k[2]]*
          Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
         2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
         U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*Pair[e[2], k[5]] + 
         U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          (2*(MH^2 - U)*Pair[ec[5], k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + 
           (T - U)*Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
          Pair[ec[5], k[4]]))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(4*((2*MH^2 - 2*S34 - T - U)*
          (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
         4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(4*(2*MH^2 - 2*S34 - T - U)*
        (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
       16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
         Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(S*(MH^2 - S34 - T14 - T24)) + 
 (col1*((-2*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVB[0, 0, MH^2 - S34 - T14 - T24, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + 
         S*(-T + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        (Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
           (-MH^2 + T)*Pair[ec[5], k[2]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           (MH^2 - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*S*
      (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*
      ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 
         2*MH^2*(T + 2*T14 - 2*T24 - U) + 2*S34*U - 4*T14*U + U^2)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - T*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*Pair[e[2], k[1]] + 
         2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 2*S34*Pair[e[1], k[2]]*
          Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
         2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
         U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*Pair[e[2], k[5]] + 
         U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          (2*(MH^2 - U)*Pair[ec[5], k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + 
           (T - U)*Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
          Pair[ec[5], k[4]]))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(4*((2*MH^2 - 2*S34 - T - U)*
          (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
         4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(4*(2*MH^2 - 2*S34 - T - U)*
        (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
       16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
         Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(S*(MH^2 - S34 - T14 - T24)) + 
 (col2*((-2*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVB[0, 0, MH^2 - S34 - T14 - T24, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*((-2*MH^2*T14 + 2*MH^2*T24 - 2*T*T24 + 2*T14*U + 
         S*(-T + U))*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       4*(-((S + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((S + T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH^2 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 4*(-2*MH^2 + T + U)*
        Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       4*(-2*MH^2 + T + U)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[4]] + 8*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        (Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
         (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*Pair[ec[5], k[4]]) + 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*((MH^2 - U)*Pair[ec[5], k[1]] + 
           (-MH^2 + T)*Pair[ec[5], k[2]]) + ((-MH^2 + U)*Pair[ec[4], k[1]] + 
           (MH^2 - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))*
      PVC[0, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*S*
      (4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
           2*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 8*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 8*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 1, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*
      ((-2*S34*T - T^2 + 4*T*T24 + 2*S*(T - U) + 
         2*MH^2*(T + 2*T14 - 2*T24 - U) + 2*S34*U - 4*T14*U + U^2)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-2*(S - S34 + T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]] + 
         (-2*MH^2 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         2*MH^2*Pair[e[1], k[5]]*Pair[e[2], k[1]] - T*Pair[e[1], k[5]]*
          Pair[e[2], k[1]] - U*Pair[e[1], k[5]]*Pair[e[2], k[1]] + 
         2*S*Pair[e[1], k[2]]*Pair[e[2], k[3]] - 2*S34*Pair[e[1], k[2]]*
          Pair[e[2], k[3]] + 2*T14*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 
         2*T24*Pair[e[1], k[2]]*Pair[e[2], k[3]] + 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[4]] - T*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 
         U*Pair[e[1], k[2]]*Pair[e[2], k[4]] - 2*MH^2*Pair[e[1], k[2]]*
          Pair[e[2], k[5]] + T*Pair[e[1], k[2]]*Pair[e[2], k[5]] + 
         U*Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] - 8*(-2*MH^2 + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] + 
       32*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) - 8*(-2*MH^2 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 8*(-2*MH^2 + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] - 
       4*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          (2*(MH^2 - U)*Pair[ec[5], k[1]] - 2*(MH^2 - T)*Pair[ec[5], k[2]] + 
           (T - U)*Pair[ec[5], k[3]]) + (-2*(MH^2 - U)*Pair[ec[4], k[1]] + 
           2*(MH^2 - T)*Pair[ec[4], k[2]] + (-T + U)*Pair[ec[4], k[3]])*
          Pair[ec[5], k[4]]))*PVC[0, 1, 0, MH^2, MH^2 - S34 - T14 - T24, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(4*((2*MH^2 - 2*S34 - T - U)*
          (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] + 
         4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 1, 1, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(4*(2*MH^2 - 2*S34 - T - U)*
        (Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - 
       16*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - 
         Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        ((2*MH^2 - 2*S34 - T - U)*(T - U)*Pair[ec[4], ec[5]] - 
         4*(T - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))*PVC[0, 2, 0, MH^2, MH^2 - S34 - T14 - T24, 
       S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
         2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       8*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] + 
       8*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[1, 0, 0, MH^2, MH^2 - S34 - T14 - T24, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(S*(MH^2 - S34 - T14 - T24))
