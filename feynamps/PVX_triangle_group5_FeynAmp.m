(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(col5*((2*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVB[0, 0, MH^2 - S - T24 - U, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*((-2*S*S34 - S34*T14 + T*T14 + 2*MH^2*(S - T24) + 
         2*T*T24)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] - 4*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
        ((-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T14 + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (S + T14 + T24)*Pair[ec[4], k[3]])) - 4*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 8*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + 4*Pair[e[1], ec[4]]*
        (((-MH^2 + S34)*Pair[e[2], k[1]] + (-MH^2 + T)*Pair[e[2], k[4]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          ((-MH^2 + S34)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*T14*
      (4*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*
      ((4*S*S34 - S34^2 + T^2 + 2*S34*T14 - 2*T*T14 - 
         2*MH^2*(2*S - S34 + T - 2*T24) - 4*T*T24 - 2*S34*U + 2*T*U)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] + 8*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*(2*(-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[ec[4], k[3]])) + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 8*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       32*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - 4*Pair[e[1], ec[4]]*
        ((-2*(MH^2 - S34)*Pair[e[2], k[1]] + (S34 - T)*Pair[e[2], k[3]] + 
           2*(-MH^2 + T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[5], k[1]] + 
           (S34 - T)*Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*Pair[e[2], ec[5]]*
        (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]))*PVC[0, 1, 1, MH^2, 
       MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*(-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*
         Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*
        Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVC[0, 2, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[1, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(T14*(MH^2 - S - T24 - U)) + 
 (col6*((2*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVB[0, 0, MH^2 - S - T24 - U, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*((-2*S*S34 - S34*T14 + T*T14 + 2*MH^2*(S - T24) + 
         2*T*T24)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] - 4*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
        ((-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T14 + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (S + T14 + T24)*Pair[ec[4], k[3]])) - 4*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 8*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + 4*Pair[e[1], ec[4]]*
        (((-MH^2 + S34)*Pair[e[2], k[1]] + (-MH^2 + T)*Pair[e[2], k[4]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          ((-MH^2 + S34)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*T14*
      (4*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (2*Alfas^2*EL*MT^2*
      ((4*S*S34 - S34^2 + T^2 + 2*S34*T14 - 2*T*T14 - 
         2*MH^2*(2*S - S34 + T - 2*T24) - 4*T*T24 - 2*S34*U + 2*T*U)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] + 8*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*(2*(-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[ec[4], k[3]])) + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 8*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       32*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - 4*Pair[e[1], ec[4]]*
        ((-2*(MH^2 - S34)*Pair[e[2], k[1]] + (S34 - T)*Pair[e[2], k[3]] + 
           2*(-MH^2 + T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[5], k[1]] + 
           (S34 - T)*Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*Pair[e[2], ec[5]]*
        (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]))*PVC[0, 1, 1, MH^2, 
       MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) + (4*Alfas^2*EL*MT^2*(-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*
         Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*
        Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVC[0, 2, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[1, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(T14*(MH^2 - S - T24 - U)) + 
 (col3*((-2*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVB[0, 0, MH^2 - S - T24 - U, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*((-2*S*S34 - S34*T14 + T*T14 + 2*MH^2*(S - T24) + 
         2*T*T24)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] - 4*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
        ((-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T14 + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (S + T14 + T24)*Pair[ec[4], k[3]])) - 4*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 8*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + 4*Pair[e[1], ec[4]]*
        (((-MH^2 + S34)*Pair[e[2], k[1]] + (-MH^2 + T)*Pair[e[2], k[4]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          ((-MH^2 + S34)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*T14*
      (4*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*
      ((4*S*S34 - S34^2 + T^2 + 2*S34*T14 - 2*T*T14 - 
         2*MH^2*(2*S - S34 + T - 2*T24) - 4*T*T24 - 2*S34*U + 2*T*U)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] + 8*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*(2*(-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[ec[4], k[3]])) + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 8*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       32*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - 4*Pair[e[1], ec[4]]*
        ((-2*(MH^2 - S34)*Pair[e[2], k[1]] + (S34 - T)*Pair[e[2], k[3]] + 
           2*(-MH^2 + T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[5], k[1]] + 
           (S34 - T)*Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*Pair[e[2], ec[5]]*
        (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]))*PVC[0, 1, 1, MH^2, 
       MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*(-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*
         Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*
        Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVC[0, 2, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[1, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(T14*(MH^2 - S - T24 - U)) + 
 (col4*((-2*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVB[0, 0, MH^2 - S - T24 - U, Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*((-2*S*S34 - S34*T14 + T*T14 + 2*MH^2*(S - T24) + 
         2*T*T24)*Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], k[5]]*
        Pair[ec[4], ec[5]] - 4*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 4*Pair[e[2], ec[5]]*
        ((-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T14 + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (S + T14 + T24)*Pair[ec[4], k[3]])) - 4*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       4*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 8*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]) + 4*Pair[e[1], ec[4]]*
        (((-MH^2 + S34)*Pair[e[2], k[1]] + (-MH^2 + T)*Pair[e[2], k[4]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          ((-MH^2 + S34)*Pair[ec[5], k[1]] + (-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*T14*
      (4*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] + Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[0, 0, 1, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (2*Alfas^2*EL*MT^2*
      ((4*S*S34 - S34^2 + T^2 + 2*S34*T14 - 2*T*T14 - 
         2*MH^2*(2*S - S34 + T - 2*T24) - 4*T*T24 - 2*S34*U + 2*T*U)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] + 8*(-2*MH^2 + S34 + T)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
        (2*(-2*MH^2 + S34 + T)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*(2*(-2*MH^2 + S34 + T)*Pair[ec[4], k[2]] + 
           (6*MH^2 - 3*S34 - 3*T - 4*U)*Pair[ec[4], k[3]])) + 
       8*(-2*MH^2 + S34 + T)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[2]] + 8*(-2*MH^2 + S34 + T)*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
       32*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         (Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - 4*Pair[e[1], ec[4]]*
        ((-2*(MH^2 - S34)*Pair[e[2], k[1]] + (S34 - T)*Pair[e[2], k[3]] + 
           2*(-MH^2 + T)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(-2*(MH^2 - S34)*Pair[ec[5], k[1]] + 
           (S34 - T)*Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]])))*
      PVC[0, 1, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*Pair[e[1], ec[4]]*
         Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*Pair[e[2], ec[5]]*
        (Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[ec[4], k[3]])*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*Pair[ec[5], k[3]]))*PVC[0, 1, 1, MH^2, 
       MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
     (MW*SW) - (4*Alfas^2*EL*MT^2*(-((S34 - T)*(-2*MH^2 + S34 + T + 2*U)*
         Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) - 4*(2*MH^2 - S34 - T - 2*U)*
        Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 4*(S34 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + 16*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*Pair[ec[4], k[3]])*
        (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]))*PVC[0, 2, 0, MH^2, MH^2 - S - T24 - U, T14, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(4*Pair[e[2], ec[5]]*
        (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]])) - 
       8*(Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
           Pair[e[1], e[2]]*Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*
          (Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[2]])) + Pair[e[1], ec[4]]*
        ((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))*
      PVC[1, 0, 0, MH^2, MH^2 - S - T24 - U, T14, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW)))/(T14*(MH^2 - S - T24 - U))
