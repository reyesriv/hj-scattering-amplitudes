(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*(-c124 + c142)*EL*GS*MT^2*
  ((Pair[e[1], k[3]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*Pair[e[2], k[4]])*
    PVC[0, 0, 1, 0, 0, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   (Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])*
    PVC[0, 0, 1, 0, MH^2, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   (Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])*
    PVC[0, 0, 1, MH^2, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
   Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*PVC[0, 1, 0, 0, 0, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] - Pair[e[1], k[4]]*PVC[0, 1, 0, 0, MH^2, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
   (Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])*
    PVC[0, 1, 0, MH^2, 0, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   (((U*Pair[e[1], k[2]] + S*Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
      Pair[e[1], ec[4]]*(T*Pair[e[2], k[1]] + S*Pair[e[2], k[4]]) - 
      2*Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]] + 
      Pair[e[1], e[2]]*(T*Pair[ec[4], k[1]] - (2*S + U)*Pair[ec[4], k[2]]) + 
      2*Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
        2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]))*PVD[0, 0, 0, 0, 0, 0, MH^2, 0, 
      T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/4 + 
   ((((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]] - 
       Pair[e[1], ec[4]]*((-MH^2 + U)*Pair[e[2], k[1]] + 
         S*Pair[e[2], k[3]]) + 2*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - 
           Pair[e[2], k[4]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        ((-MH^2 + U)*Pair[ec[4], k[1]] + (MH^2 - T)*Pair[ec[4], k[2]] - 
         S*Pair[ec[4], k[3]]))*PVD[0, 0, 0, 0, 0, MH^2, 0, 0, U, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (((-MH^2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]] + 
       Pair[e[1], ec[4]]*((MH^2 + U)*Pair[e[2], k[1]] - 
         (S + 2*U)*Pair[e[2], k[3]]) - Pair[e[1], e[2]]*
        ((MH^2 + U)*Pair[ec[4], k[1]] + (-MH^2 + T)*Pair[ec[4], k[2]] + 
         S*Pair[ec[4], k[3]]) + 
       2*(Pair[e[1], k[3]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[1]] - 
           Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
          (-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + Pair[e[2], k[1]]*
            Pair[ec[4], k[3]])))*PVD[0, 0, 0, 0, MH^2, 0, 0, 0, U, T, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/4 + 
   (Pair[e[2], k[1]]*((-MH^2 + T)*Pair[e[1], ec[4]] + 
       2*Pair[e[1], k[3]]*Pair[ec[4], k[1]])*PVD[0, 0, 0, 1, 0, 0, MH^2, 0, 
       T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (Pair[e[2], k[1]]*(-(U*Pair[e[1], ec[4]]) + 2*Pair[e[1], k[3]]*
          Pair[ec[4], k[1]]) + S*Pair[e[1], e[2]]*(Pair[ec[4], k[2]] - 
         Pair[ec[4], k[3]]))*PVD[0, 0, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     ((-MH^2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*Pair[e[2], k[1]])*
      Pair[ec[4], k[1]]*PVD[0, 0, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/2 + 
   (-((-(T*Pair[e[1], k[3]]*Pair[e[2], ec[4]]) + Pair[e[1], ec[4]]*
         (-(T*Pair[e[2], k[1]]) + U*Pair[e[2], k[4]]) + 
        T*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - S*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[2]] + 2*Pair[e[1], k[4]]*Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*PVD[0, 0, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
     (((-T + U)*Pair[e[1], k[2]] + T*Pair[e[1], k[3]])*Pair[e[2], ec[4]] + 
       U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
        ((-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) - 
       2*(Pair[e[1], k[3]]*(Pair[e[2], k[3]] + Pair[e[2], k[4]])*
          Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[e[2], k[4]]*
          Pair[ec[4], k[3]]))*PVD[0, 0, 1, 0, 0, MH^2, 0, 0, U, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/2 + 
   ((((T + U)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
       Pair[e[2], ec[4]] + U*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
        3*Pair[e[2], k[3]]) + 2*(Pair[e[1], k[2]]*Pair[e[2], k[1]]*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(-5*Pair[e[2], k[1]]*
           Pair[ec[4], k[1]] + Pair[e[2], k[4]]*(5*Pair[ec[4], k[1]] + 
            Pair[ec[4], k[2]]))) - Pair[e[1], e[2]]*(2*U*Pair[ec[4], k[1]] + 
        (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]))*
     PVD[0, 0, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]])/2 + Pair[e[1], k[3]]*(-(S*Pair[e[2], ec[4]]) + 
     4*Pair[e[2], k[1]]*Pair[ec[4], k[2]])*PVD[0, 0, 1, 1, 0, 0, MH^2, 0, T, 
     S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   Pair[e[2], k[1]]*(U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*
      Pair[ec[4], k[1]])*PVD[0, 0, 1, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   (U*Pair[e[1], ec[4]]*Pair[e[2], k[1]] - Pair[e[1], k[4]]*
      (U*Pair[e[2], ec[4]] + 4*(-2*Pair[e[2], k[1]] + Pair[e[2], k[4]])*
        Pair[ec[4], k[1]]))*PVD[0, 0, 1, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 4*Pair[e[2], k[1]]*
    Pair[ec[4], k[1]]*(Pair[e[1], k[3]]*PVD[0, 0, 1, 2, 0, 0, MH^2, 0, T, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     Pair[e[1], k[4]]*(PVD[0, 0, 1, 2, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 1, 2, MH^2, 0, 0, 0, 
        U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])) + 
   (T*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
     4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
      Pair[ec[4], k[2]])*PVD[0, 0, 2, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - Pair[e[2], k[3]]*
    (U*Pair[e[1], ec[4]] + 4*Pair[e[1], k[4]]*Pair[ec[4], k[1]])*
    PVD[0, 0, 2, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] - (U*Pair[e[1], ec[4]]*Pair[e[2], k[3]] + 
     Pair[e[1], k[4]]*(-2*U*Pair[e[2], ec[4]] + 8*Pair[e[2], k[3]]*
        Pair[ec[4], k[1]]))*PVD[0, 0, 2, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
   4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], k[2]]*
    PVD[0, 0, 3, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] - 
   4*(Pair[e[1], k[3]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
       Pair[e[2], k[1]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]]))*
      PVD[0, 0, 2, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[1], k[4]]*(2*Pair[e[2], k[1]] - 
       Pair[e[2], k[4]])*Pair[ec[4], k[1]]*(PVD[0, 0, 2, 1, 0, MH^2, 0, 0, U, 
        S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       PVD[0, 0, 2, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]]) + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
      Pair[ec[4], k[1]]*(PVD[0, 0, 3, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 0, 3, 0, MH^2, 0, 0, 0, 
        U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])) + 
   (((Pair[e[1], k[2]] - 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
       Pair[e[1], ec[4]]*Pair[e[2], k[1]] - Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] - Pair[ec[4], k[2]]))*PVC[0, 0, 0, 0, 0, T, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
     ((2*Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
       Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) + 
       Pair[e[1], e[2]]*Pair[ec[4], k[1]])*PVC[0, 0, 0, 0, MH^2, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
     ((Pair[e[1], k[2]] + Pair[e[1], k[4]])*Pair[e[2], ec[4]] - 
       Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*PVC[0, 0, 0, MH^2, 0, S, 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (S*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + Pair[e[1], k[2]]*
        (T*Pair[e[2], ec[4]] - 2*Pair[e[2], k[3]]*Pair[ec[4], k[2]]))*
      PVD[0, 1, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] - Pair[e[1], k[2]]*
      ((-MH^2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*Pair[ec[4], k[2]])*
      PVD[0, 1, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2]] - 
     (-(((-MH^2 + T)*Pair[e[1], k[2]] - (T + 2*U)*Pair[e[1], k[3]])*
         Pair[e[2], ec[4]]) - Pair[e[1], ec[4]]*
        ((MH^2 + U)*Pair[e[2], k[1]] - (2*S + 3*U)*Pair[e[2], k[3]]) + 
       2*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[2]] + 
         5*Pair[e[2], k[3]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
       Pair[e[1], e[2]]*((MH^2 + U)*Pair[ec[4], k[1]] + 
         (-MH^2 + T)*Pair[ec[4], k[2]] + S*Pair[ec[4], k[3]]) - 
       2*Pair[e[1], k[2]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
         Pair[e[2], k[4]]*Pair[ec[4], k[3]]))*PVD[0, 1, 0, 0, MH^2, 0, 0, 0, 
       U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/2 + 
   Pair[e[1], k[2]]*(-(S*Pair[e[2], ec[4]]) + 4*Pair[e[2], k[1]]*
      Pair[ec[4], k[2]])*PVD[0, 1, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[1]]*
    (S*Pair[e[1], ec[4]] - 4*Pair[e[1], k[2]]*Pair[ec[4], k[1]])*
    PVD[0, 1, 0, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] + ((-MH^2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[1]] + 
     Pair[e[1], k[3]]*(-(U*Pair[e[2], ec[4]]) + 
       4*(Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[1]]))*
    PVD[0, 1, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] + Pair[e[2], k[1]]*Pair[ec[4], k[1]]*
    (-4*Pair[e[1], k[2]]*(PVD[0, 1, 0, 2, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 1, 0, 2, 0, MH^2, 0, 0, 
        U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
     4*Pair[e[1], k[3]]*PVD[0, 1, 0, 2, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
   Pair[e[1], k[2]]*(T*Pair[e[2], ec[4]] + 4*Pair[e[2], k[4]]*
      Pair[ec[4], k[2]])*PVD[0, 1, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
    (S*Pair[e[1], ec[4]] - 4*Pair[e[1], k[2]]*Pair[ec[4], k[1]])*
    PVD[0, 1, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] - ((2*U*Pair[e[1], k[2]] - (MH^2 + 3*U)*Pair[e[1], k[4]])*
      Pair[e[2], ec[4]] + Pair[e[2], k[3]]*((S + 2*U)*Pair[e[1], ec[4]] - 
       8*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 4*Pair[e[1], k[4]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))*PVD[0, 1, 1, 0, MH^2, 0, 0, 
     0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] - 
   4*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
      Pair[ec[4], k[1]] - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
      Pair[ec[4], k[2]])*PVD[0, 1, 1, 1, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
     Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
   4*(Pair[e[1], k[2]]*(2*Pair[e[2], k[1]] - Pair[e[2], k[4]])*
      Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
      (-2*Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
        (4*Pair[ec[4], k[1]] + Pair[ec[4], k[2]])))*
    PVD[0, 1, 1, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] + 
   4*((-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]]) + 
       Pair[e[1], k[2]]*(-(Pair[e[2], k[4]]*Pair[ec[4], k[1]]) + 
         2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]))*PVD[0, 1, 1, 1, 0, 0, MH^2, 
       0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (2*Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
      Pair[ec[4], k[2]]*PVD[0, 1, 2, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
      ((-(Pair[e[1], k[3]]*Pair[ec[4], k[2]]) + Pair[e[1], k[2]]*
          (2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))*PVD[0, 1, 2, 0, 0, MH^2, 
         0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
       (Pair[e[1], k[3]]*Pair[ec[4], k[1]] - Pair[e[1], k[4]]*
          (Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*PVD[0, 1, 2, 0, MH^2, 0, 
         0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])) + 
   ((-MH^2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 
     Pair[e[1], k[3]]*((MH^2 + U)*Pair[e[2], ec[4]] - 
       4*Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]])))*
    PVD[0, 2, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] + 4*(Pair[e[1], k[2]]*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
      (PVD[0, 2, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], 
        Sqrt[MT^2], Sqrt[MT^2]] + PVD[0, 2, 0, 1, 0, MH^2, 0, 0, U, S, 
        Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
     Pair[e[1], k[3]]*(Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
       Pair[e[2], k[1]]*Pair[ec[4], k[3]])*PVD[0, 2, 0, 1, MH^2, 0, 0, 0, U, 
       T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
   4*Pair[e[2], k[3]]*(Pair[e[1], k[3]]*(Pair[ec[4], k[2]] - 
       3*Pair[ec[4], k[3]]) + Pair[e[1], k[2]]*Pair[ec[4], k[3]])*
    PVD[0, 2, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[2]]*
      (Pair[e[2], k[4]]*PVD[0, 2, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + Pair[e[2], k[3]]*
        PVD[0, 2, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], 
         Sqrt[MT^2], Sqrt[MT^2]]) + Pair[e[1], k[3]]*Pair[e[2], k[3]]*
      Pair[ec[4], k[3]]*PVD[0, 3, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
   2*((Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], e[2]]*
        Pair[ec[4], k[2]])*PVD[1, 0, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (-(Pair[e[1], ec[4]]*Pair[e[2], k[1]]) + 2*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]])*PVD[1, 0, 0, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     (Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
        (Pair[e[2], k[1]] - 2*Pair[e[2], k[4]]) + 2*Pair[e[1], e[2]]*
        Pair[ec[4], k[1]])*PVD[1, 0, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
   4*(Pair[e[1], ec[4]]*Pair[e[2], k[1]] + Pair[e[1], e[2]]*
      Pair[ec[4], k[1]])*(PVD[1, 0, 0, 1, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + PVD[1, 0, 0, 1, 0, MH^2, 0, 0, U, 
      S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + 
     PVD[1, 0, 0, 1, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]]) - 4*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 
     Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[4], k[2]])*
    PVD[1, 0, 1, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]] - 4*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] - 
     Pair[e[1], ec[4]]*Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[4], k[1]])*
    (PVD[1, 0, 1, 0, 0, MH^2, 0, 0, U, S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
      Sqrt[MT^2]] + PVD[1, 0, 1, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) - 
   4*(Pair[e[1], k[2]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
      Pair[ec[4], k[2]])*(PVD[1, 1, 0, 0, 0, 0, MH^2, 0, T, S, Sqrt[MT^2], 
      Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]] + PVD[1, 1, 0, 0, 0, MH^2, 0, 0, U, 
      S, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]]) + 
   4*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
      Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]])*
    PVD[1, 1, 0, 0, MH^2, 0, 0, 0, U, T, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
     Sqrt[MT^2]]))/(MW*Pi*SW)
