(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
Den[T14, 0]*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
  ((-8*Alfas2*EL*MT2*D0i[dd112, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*((-S + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + T24)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd113, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(-2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
       ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
         Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) - 
      2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
      8*Pair[e[2], k[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd001, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (Pair[e[2], ec[5]]*((-S + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 
      2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((-S34 + T)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]])) - 
      2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((S - T24)*Pair[e[1], ec[4]] - 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 
      (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[2], ec[5]]*((S - T24)*(MH2 - U)*Pair[e[1], ec[4]] - 
        4*(MH2 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 2*Pair[e[2], k[3]]*
       ((-S + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*
       (-((S - T24)*(-MH2 + S + T24 + U)*Pair[e[1], ec[4]]) - 
        4*(MH2 - S - T24 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 
      2*(-2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*(2*T24*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[4]] + 
          (S - T24)*Pair[e[2], k[5]]) - 2*(S + T24)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 4*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd33, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd333, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd333, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*((-S34 + T)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]))*Pair[ec[5], k[3]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
         Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) + 
      2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*Pair[e[2], k[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd223, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
       ((-S - 2*S34 + 2*T + T24)*Pair[e[2], k[3]] + 
        (-S34 + T)*Pair[e[2], k[5]]) + 
      4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((-S + S34 - T + T24)*Pair[e[2], k[3]] + 
        (-2*S + S34 - T + 2*T24)*Pair[e[2], k[5]]) + 
      4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[5]] + Pair[e[2], k[5]]*
           (Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
       ((-2*S - S34 + T + 2*T24)*Pair[e[2], k[3]] + (-S - S34 + T + T24)*
         Pair[e[2], k[5]]) + 4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[3]]*
         (3*Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - U)*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
      2*Pair[e[2], k[3]]*((S + T24)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
        (S + T24)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] - 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
      Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
          Pair[ec[5], k[2]]) - Pair[e[2], k[3]]*
         (2*(S + T24)*Pair[ec[5], k[1]] - (S - 3*T24)*Pair[ec[5], k[2]] + 
          (-3*S + T24)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 
          2*Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
        Pair[e[2], k[5]]*((-2*S + S34 - T + 2*T24)*Pair[ec[5], k[2]] + 
          (-S + T24)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
        2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[4]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
       (Pair[e[2], k[5]]*((-S34 + T)*Pair[ec[5], k[2]] + 
          (-S + T24)*Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
         ((-S - S34 + T + T24)*Pair[ec[5], k[2]] + 2*(-S + T24)*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[4]]*((-7*MH2 + 3*(S34 + T + U))*
         Pair[e[2], k[3]] + (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*
       Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[2], k[5]])*Pair[ec[4], k[1]] - 2*(MH2 + S + T24 - U)*
       Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(MH2 + S + T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
       Pair[ec[5], k[3]] + 8*(2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
         (3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(4*Pair[ec[4], k[3]] + 
            3*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
      Pair[e[1], ec[4]]*(-2*(MH2 + S + T24 - U)*Pair[e[2], k[1]]*
         Pair[ec[5], k[3]] - Pair[e[2], k[3]]*(2*(MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[1]] + (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
          (2*MH2 - 12*S - 4*S34 - 5*T14 - U)*Pair[ec[5], k[3]]) - 
        Pair[e[2], k[5]]*(2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
          (2*MH2 - 6*S - 4*S34 - 3*T14 - 2*T24 - U)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd2, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(-2*Pair[e[1], ec[5]]*
       ((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
        (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
      2*Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*Pair[e[1], k[2]]*
         Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*Pair[ec[4], k[2]] + 
          (S + T24)*Pair[ec[4], k[3]])) - 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
        (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) - 
      2*Pair[e[1], k[4]]*(((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
          (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]])) - 
      Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*
         Pair[e[2], ec[5]] + 2*Pair[e[2], k[1]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[2]] - (2*MH2 + S + T24 - 2*U)*
           Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*
         (2*(MH2 - U)*Pair[ec[5], k[1]] - 2*(MH2 - T)*Pair[ec[5], k[2]] + 
          (MH2 + 2*S - U)*Pair[ec[5], k[3]]) - Pair[e[2], k[3]]*
         (2*(2*S + T14 + 2*T24 + U)*Pair[ec[5], k[1]] + 
          (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[2]] + 
          2*(-2*MH2 - 3*S + T + T24 + U)*Pair[ec[5], k[3]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
          Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 3*Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + 3*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (-(Pair[e[2], k[5]]*Pair[ec[5], k[2]]) + 2*Pair[e[2], k[3]]*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (Pair[e[1], ec[4]]*((-S34 + T)*Pair[ec[5], k[2]] + 
        (-S + T24)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
          Pair[ec[4], k[2]]*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - 
          T24*(T + U))*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[3]]*((S + T24)*Pair[ec[5], k[1]] + 
            T24*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[4]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[1]]*((-MH2 + U)*Pair[ec[5], k[1]] + (-2*MH2 + S34 + U)*
             Pair[ec[5], k[2]] + (MH2 + T24 - U)*Pair[ec[5], k[3]]))) + 
      2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*
           Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*
             Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[2]] + (MH2 + S + T24 - U)*Pair[ec[5], k[3]]) - 
        2*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] - Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (MH2 + S + T24 - U)*Pair[ec[5], k[3]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[4]]*((5*MH2*(S - T24) - S*(3*S34 + T + 3*U) + 
          T24*(S34 + 3*(T + U)))*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[1]]*(2*(-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            2*(MH2 - S34 - U)*Pair[ec[5], k[2]] + (2*MH2 + S - 2*T - 7*T24)*
             Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (-2*(S + T24)*Pair[ec[5], k[1]] - 2*T24*Pair[ec[5], k[2]] + 
            (S + T24)*Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
           ((4*MH2 - 2*(S34 + T))*Pair[ec[5], k[1]] + 
            (6*MH2 - 2*(S34 + 2*T + U))*Pair[ec[5], k[2]] + 
            (-2*MH2 - 7*S + 2*T + T24)*Pair[ec[5], k[3]]))) + 
      4*(Pair[e[1], ec[5]]*((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((-5*MH2 + 2*S34 + 2*T + 3*U)*
           Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-5*MH2 + 2*S34 + 2*T + 3*U)*
             Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(S + T14 + T24)*
           Pair[ec[5], k[2]] - 3*(S + T24)*Pair[ec[5], k[3]]) + 
        Pair[e[1], k[4]]*(((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
            (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
           (2*(S + T14 + T24)*Pair[ec[5], k[2]] - 3*(S + T24)*
             Pair[ec[5], k[3]])) + 
        2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
            Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[2]]*Pair[ec[4], k[1]]*(Pair[e[2], k[3]]*
             Pair[ec[5], k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
            (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(Pair[ec[4], k[3]]*(
                Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[2]]*
               Pair[ec[5], k[3]]))))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd003, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
          Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
       ((-S + T24)*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          Pair[e[2], k[4]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]])))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd00, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*
     (2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
          Pair[e[1], e[2]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*((Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] - 
            2*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
       ((S - T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[5], k[3]] - 
          2*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (-(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
        (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[1]])*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd133, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
        (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*C0i[cc2, U, 0, T14, MT2, MT2, MT2]*
     (Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*
       ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*(Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
        Pair[e[2], k[4]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]])) - 2*Pair[e[2], k[5]]*
       (-2*(Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[ec[4], k[1]]) + Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, U, 0, T14, MT2, MT2, MT2]*
     (2*Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[3]] + 
        Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      2*Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[e[2], k[4]]*Pair[ec[5], k[4]] - Pair[e[2], k[5]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*(S + T24)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
      2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       (Pair[e[2], k[1]]*(-2*(S - T24)*Pair[ec[5], k[1]] + 
          (3*S - 5*T24)*Pair[ec[5], k[3]] - 4*T24*Pair[ec[5], k[4]]) + 
        Pair[e[2], k[4]]*(4*S*Pair[ec[5], k[1]] + (-5*S + 3*T24)*
           Pair[ec[5], k[3]] + 2*(-S + T24)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd33, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[4]]*
       (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(S + T24)*Pair[e[1], e[2]]*
       Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
       (-(Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] + 
           (-5*S + 3*T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]])) + Pair[e[2], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] + (-3*S + 5*T24)*
           Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd22, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[4]]*
       Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
      2*(-MH2 + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(-MH2 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
      8*Pair[e[2], k[3]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[2]] + Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - 
      Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
          Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 2*(S + S34 - T - T24)*
           Pair[ec[5], k[3]] + (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (-((MH2*(S - T24) - S*(S34 - T + U) + T24*(-S34 + T + U))*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) + 4*Pair[e[1], k[4]]*
       ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
        (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
       ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
        (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
         Pair[e[2], k[4]])*Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
       ((-MH2 + U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[4], k[2]] + 
          (S + T24)*Pair[ec[4], k[3]])) - 
      8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) - Pair[e[1], k[3]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]]*
         (Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
      4*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
      2*Pair[e[1], ec[4]]*(2*(S + T24)*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[5]]*(2*(-MH2 + T + U)*Pair[ec[5], k[1]] + 
          (S + T24)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + U)*
           Pair[ec[5], k[4]]) + 2*Pair[e[2], k[3]]*
         ((-MH2 + S + T + U)*Pair[ec[5], k[1]] + T24*Pair[ec[5], k[3]] + 
          (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*
     (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
       ((-S34 + T)*Pair[e[2], ec[5]] - 
        2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, U, 0, T14, MT2, MT2, MT2]*
     (4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
           Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
            (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
        Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]))) + 
      Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
        2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*(3*Pair[ec[5], k[2]] - 
            2*Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
           Pair[e[1], k[4]]*Pair[ec[4], k[5]])) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]) + 
          Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 
            Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] + 
        2*(-2*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
          2*Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((S34 + T)*Pair[e[1], k[2]]*
           Pair[ec[4], k[1]] - (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((S34 + T)*Pair[ec[4], k[2]] - 
            (S + T24)*Pair[ec[4], k[3]])) - Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
          Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
        Pair[e[1], k[4]]*(((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] - Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]))) + 
      Pair[e[1], ec[4]]*((-(S*T) + S34*T24)*Pair[e[2], ec[5]] + 
        2*(Pair[e[2], k[4]]*((-MH2 + S + U)*Pair[ec[5], k[1]] + 
            (-MH2 + S + S34 + U)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
          Pair[e[2], k[3]]*(T24*Pair[ec[5], k[1]] + (-S + T24)*
             Pair[ec[5], k[2]] + S*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
           (T24*Pair[ec[5], k[1]] + (-MH2 + T + T24 + U)*Pair[ec[5], k[2]] + 
            (MH2 - T24 - U)*Pair[ec[5], k[4]])))))/(MW*SW)) + 
 Den[T14, 0]*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
  ((-8*Alfas2*EL*MT2*D0i[dd112, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*((-S + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + T24)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd113, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd113, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(-2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
       ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
         Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) - 
      2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
      8*Pair[e[2], k[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]))*Pair[ec[5], k[2]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd001, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd001, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (Pair[e[2], ec[5]]*((-S + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 
      2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (2*Alfas2*EL*MT2*C0i[cc1, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((-S34 + T)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[3]])) - 
      2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd00, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((S - T24)*Pair[e[1], ec[4]] - 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])) - 
      (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
        2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[2], ec[5]]*((S - T24)*(MH2 - U)*Pair[e[1], ec[4]] - 
        4*(MH2 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 2*Pair[e[2], k[3]]*
       ((-S + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd1, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*
       (-((S - T24)*(-MH2 + S + T24 + U)*Pair[e[1], ec[4]]) - 
        4*(MH2 - S - T24 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 
      2*(-2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], ec[4]]*(2*T24*Pair[e[2], k[1]] + 2*S*Pair[e[2], k[4]] + 
          (S - T24)*Pair[e[2], k[5]]) - 2*(S + T24)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]] + 4*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd33, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd333, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd333, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*((-S34 + T)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]]))*Pair[ec[5], k[3]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*(2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
       ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
         Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) + 
      2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
      8*Pair[e[2], k[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd223, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
       ((-S - 2*S34 + 2*T + T24)*Pair[e[2], k[3]] + 
        (-S34 + T)*Pair[e[2], k[5]]) + 
      4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
           (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[1], ec[4]]*((-S + S34 - T + T24)*Pair[e[2], k[3]] + 
        (-2*S + S34 - T + 2*T24)*Pair[e[2], k[5]]) + 
      4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
         Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[5]] + Pair[e[2], k[5]]*
           (Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd233, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
       ((-2*S - S34 + T + 2*T24)*Pair[e[2], k[3]] + (-S - S34 + T + T24)*
         Pair[e[2], k[5]]) + 4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[3]]*
         (3*Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
            2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd12, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*(MH2 - U)*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
      2*Pair[e[2], k[3]]*((S + T24)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
        (S + T24)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] - 
        4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
      Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
          Pair[ec[5], k[2]]) - Pair[e[2], k[3]]*
         (2*(S + T24)*Pair[ec[5], k[1]] - (S - 3*T24)*Pair[ec[5], k[2]] + 
          (-3*S + T24)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 
          2*Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 2*Pair[e[1], k[4]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
        Pair[e[2], k[5]]*((-2*S + S34 - T + 2*T24)*Pair[ec[5], k[2]] + 
          (-S + T24)*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
        2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[4]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
       (Pair[e[2], k[5]]*((-S34 + T)*Pair[ec[5], k[2]] + 
          (-S + T24)*Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
         ((-S - S34 + T + T24)*Pair[ec[5], k[2]] + 2*(-S + T24)*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd23, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*Pair[e[1], k[4]]*((-7*MH2 + 3*(S34 + T + U))*
         Pair[e[2], k[3]] + (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*
       Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*
       ((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[2], k[5]])*Pair[ec[4], k[1]] - 2*(MH2 + S + T24 - U)*
       Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 
      2*(MH2 + S + T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
       Pair[ec[5], k[3]] + 8*(2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
          Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[5]]*
         (3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(4*Pair[ec[4], k[3]] + 
            3*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
      Pair[e[1], ec[4]]*(-2*(MH2 + S + T24 - U)*Pair[e[2], k[1]]*
         Pair[ec[5], k[3]] - Pair[e[2], k[3]]*(2*(MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[1]] + (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
          (2*MH2 - 12*S - 4*S34 - 5*T14 - U)*Pair[ec[5], k[3]]) - 
        Pair[e[2], k[5]]*(2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
          (MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
          (2*MH2 - 6*S - 4*S34 - 3*T14 - 2*T24 - U)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd2, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(-2*Pair[e[1], ec[5]]*
       ((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
        (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
      2*Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*Pair[e[1], k[2]]*
         Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*Pair[ec[4], k[2]] + 
          (S + T24)*Pair[ec[4], k[3]])) - 2*Pair[e[1], e[2]]*
       Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
        (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) - 
      2*Pair[e[1], k[4]]*(((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
          (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]])) - 
      Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*
         Pair[e[2], ec[5]] + 2*Pair[e[2], k[1]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[2]] - (2*MH2 + S + T24 - 2*U)*
           Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*
         (2*(MH2 - U)*Pair[ec[5], k[1]] - 2*(MH2 - T)*Pair[ec[5], k[2]] + 
          (MH2 + 2*S - U)*Pair[ec[5], k[3]]) - Pair[e[2], k[3]]*
         (2*(2*S + T14 + 2*T24 + U)*Pair[ec[5], k[1]] + 
          (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[2]] + 
          2*(-2*MH2 - 3*S + T + T24 + U)*Pair[ec[5], k[3]])) + 
      4*(-(Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
          Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 3*Pair[e[1], k[2]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[3]] + 3*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
         Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
         (-(Pair[e[2], k[5]]*Pair[ec[5], k[2]]) + 2*Pair[e[2], k[3]]*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*Pair[e[2], k[3]]*
     (Pair[e[1], ec[4]]*((-S34 + T)*Pair[ec[5], k[2]] + 
        (-S + T24)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
        Pair[e[1], k[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        Pair[e[1], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
          Pair[ec[4], k[2]]*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd0, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - 
          T24*(T + U))*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[3]]*((S + T24)*Pair[ec[5], k[1]] + 
            T24*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[4]]*((MH2 - U)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[1]]*((-MH2 + U)*Pair[ec[5], k[1]] + (-2*MH2 + S34 + U)*
             Pair[ec[5], k[2]] + (MH2 + T24 - U)*Pair[ec[5], k[3]]))) + 
      2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*
           Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*
             Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[2]] + (MH2 + S + T24 - U)*Pair[ec[5], k[3]]) - 
        2*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] - Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
          Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
            (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (MH2 + S + T24 - U)*Pair[ec[5], k[3]])))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[4]]*((5*MH2*(S - T24) - S*(3*S34 + T + 3*U) + 
          T24*(S34 + 3*(T + U)))*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[1]]*(2*(-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            2*(MH2 - S34 - U)*Pair[ec[5], k[2]] + (2*MH2 + S - 2*T - 7*T24)*
             Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
           (-2*(S + T24)*Pair[ec[5], k[1]] - 2*T24*Pair[ec[5], k[2]] + 
            (S + T24)*Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
           ((4*MH2 - 2*(S34 + T))*Pair[ec[5], k[1]] + 
            (6*MH2 - 2*(S34 + 2*T + U))*Pair[ec[5], k[2]] + 
            (-2*MH2 - 7*S + 2*T + T24)*Pair[ec[5], k[3]]))) + 
      4*(Pair[e[1], ec[5]]*((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((-5*MH2 + 2*S34 + 2*T + 3*U)*
           Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-5*MH2 + 2*S34 + 2*T + 3*U)*
             Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(S + T14 + T24)*
           Pair[ec[5], k[2]] - 3*(S + T24)*Pair[ec[5], k[3]]) + 
        Pair[e[1], k[4]]*(((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
            (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
           (2*(S + T14 + T24)*Pair[ec[5], k[2]] - 3*(S + T24)*
             Pair[ec[5], k[3]])) + 
        2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
            Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[2]]*Pair[ec[4], k[1]]*(Pair[e[2], k[3]]*
             Pair[ec[5], k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
            (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(Pair[ec[4], k[3]]*(
                Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[2]]*
               Pair[ec[5], k[3]]))))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*(D0i[dd003, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd003, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
          Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
       ((-S + T24)*Pair[e[2], ec[5]] - 
        2*(Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
          Pair[e[2], k[4]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]])))))/
    (MW*SW) + (8*Alfas2*EL*MT2*D0i[dd00, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*
     (2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
          Pair[e[1], e[2]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*((Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] - 
            2*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
       ((S - T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*(Pair[ec[5], k[3]] - 
          2*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc2, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (-(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
        (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[1]])*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])))/
    (MW*SW) - (8*Alfas2*EL*MT2*(D0i[dd133, 0, MH2 - S - T24 - U, MH2, 
       2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2] + 
      D0i[dd133, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2])*
     (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
        (Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd2, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
      0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
     (2*(-MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
      2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
      4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
       ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (S - S34 + T - T24)*
         Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])))/
    (MW*SW) + (8*Alfas2*EL*MT2*C0i[cc2, U, 0, T14, MT2, MT2, MT2]*
     (Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
          Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
          Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*
       ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*(Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
        Pair[e[2], k[4]]*Pair[ec[5], k[4]])))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (Pair[e[2], ec[5]]*((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
        4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]])) - 2*Pair[e[2], k[5]]*
       (-2*(Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[ec[4], k[1]]) + Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, U, 0, T14, MT2, MT2, MT2]*
     (2*Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[3]] + 
        Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
      2*Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
      Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
        2*Pair[e[2], k[4]]*Pair[ec[5], k[4]] - Pair[e[2], k[5]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd13, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 2*(S + T24)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
      2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
      2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - 
      8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
       (Pair[e[2], k[1]]*(-2*(S - T24)*Pair[ec[5], k[1]] + 
          (3*S - 5*T24)*Pair[ec[5], k[3]] - 4*T24*Pair[ec[5], k[4]]) + 
        Pair[e[2], k[4]]*(4*S*Pair[ec[5], k[1]] + (-5*S + 3*T24)*
           Pair[ec[5], k[3]] + 2*(-S + T24)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd33, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*(4*(S + T14 + T24)*Pair[e[1], k[4]]*
       (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
      4*(S + T14 + T24)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
        Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(S + T24)*Pair[e[1], k[4]]*
       Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(S + T24)*Pair[e[1], e[2]]*
       Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
      16*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
       (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
       (-(Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] + 
           (-5*S + 3*T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]])) + Pair[e[2], k[1]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[1]] + (-3*S + 5*T24)*
           Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd22, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
      T14, MT2, MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[4]]*
       Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], ec[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
      2*(-MH2 + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      2*(-MH2 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
      8*Pair[e[2], k[3]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
         (Pair[ec[4], k[2]] + Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - 
      Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
          Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 2*(S + S34 - T - T24)*
           Pair[ec[5], k[3]] + (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd3, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (-((MH2*(S - T24) - S*(S34 - T + U) + T24*(-S34 + T + U))*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) + 4*Pair[e[1], k[4]]*
       ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
        (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
         Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
       ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
        (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
         Pair[e[2], k[4]])*Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
       ((-MH2 + U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
        (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
        Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[4], k[2]] + 
          (S + T24)*Pair[ec[4], k[3]])) - 
      8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
         Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - 
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]*
           (Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) - Pair[e[1], k[3]]*
         (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]]*
         (Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
      4*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
      4*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 
      2*Pair[e[1], ec[4]]*(2*(S + T24)*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[5]]*(2*(-MH2 + T + U)*Pair[ec[5], k[1]] + 
          (S + T24)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + U)*
           Pair[ec[5], k[4]]) + 2*Pair[e[2], k[3]]*
         ((-MH2 + S + T + U)*Pair[ec[5], k[1]] + T24*Pair[ec[5], k[3]] + 
          (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, 
      MT2, MT2, MT2]*
     (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
           Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
       ((-S34 + T)*Pair[e[2], ec[5]] - 
        2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[3]] + 
          Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))))/
    (MW*SW) - (Alfas2*EL*MT2*C0i[cc0, U, 0, T14, MT2, MT2, MT2]*
     (4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
           Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
            (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
        Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]))) + 
      Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
        2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*(3*Pair[ec[5], k[2]] - 
            2*Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[4]])))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
     (-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
           Pair[e[1], k[4]]*Pair[ec[4], k[5]])) + Pair[ec[4], k[1]]*
         (Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]) + 
          Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[4]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 
            Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
       ((-S + S34 - T + T24)*Pair[e[2], ec[5]] + 
        2*(-2*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
          Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
          2*Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, 0, MH2 - S - T24 - U, MH2, 
      2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*
     (2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
         Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((S34 + T)*Pair[e[1], k[2]]*
           Pair[ec[4], k[1]] - (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((S34 + T)*Pair[ec[4], k[2]] - 
            (S + T24)*Pair[ec[4], k[3]])) - Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
        2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
          Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
           Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
           Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
        Pair[e[1], k[4]]*(((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] - Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]))) + 
      Pair[e[1], ec[4]]*((-(S*T) + S34*T24)*Pair[e[2], ec[5]] + 
        2*(Pair[e[2], k[4]]*((-MH2 + S + U)*Pair[ec[5], k[1]] + 
            (-MH2 + S + S34 + U)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
          Pair[e[2], k[3]]*(T24*Pair[ec[5], k[1]] + (-S + T24)*
             Pair[ec[5], k[2]] + S*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
           (T24*Pair[ec[5], k[1]] + (-MH2 + T + T24 + U)*Pair[ec[5], k[2]] + 
            (MH2 - T24 - U)*Pair[ec[5], k[4]])))))/(MW*SW)) + 
 Den[T24, 0]*Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
  ((-8*Alfas2*EL*MT2*D0i[dd133, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd133, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd3, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(-(((-MH2 + T)*Pair[e[1], k[2]] + 
         (S - T14)*Pair[e[1], k[3]] + (-MH2 + T)*Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) - 2*(MH2 - T)*
       (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
     Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*
     ((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
      4*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
     Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd003, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd003, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((MH2 - T)*(S - T14)*Pair[e[2], ec[4]] - 
        4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 2*Pair[e[1], k[3]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(((-S - 2*S34 + T14 + 2*U)*Pair[e[1], k[3]] + 
        (-S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
      4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
         (3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*
           Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(3*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(((-2*S - S34 + 2*T14 + U)*Pair[e[1], k[3]] + 
        (-S - S34 + T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
      4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
        Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          2*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd122, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] - Pair[e[1], k[5]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(2*Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
      Pair[e[1], k[3]]*(-2*(S + T14)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
        2*(S + T14)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
      Pair[e[2], ec[4]]*(2*(-MH2 + T)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (MH2 - T)*Pair[e[1], k[5]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[3]]*((MH2 + S - T - 3*T14)*Pair[ec[5], k[1]] - 
          2*(S + T14)*Pair[ec[5], k[2]] + (3*S - T14)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd12, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
      T, MT2, MT2, MT2, MT2]*
     (2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      2*(MH2 + S - T + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[3]] - 2*(MH2 + S - T + T14)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
      8*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
        Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          3*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
      Pair[e[2], ec[4]]*(2*(MH2 + S - T + T14)*Pair[e[1], k[2]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*((MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[1]] + 2*(MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[2]] + (2*MH2 - 12*S - 4*S34 - T - 5*T24)*
           Pair[ec[5], k[3]]) + Pair[e[1], k[5]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 2*(MH2 + S + T14 + T24)*
           Pair[ec[5], k[2]] + (2*MH2 - 6*S - 4*S34 - T - 2*T14 - 3*T24)*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
        (-S + T14)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
        (-S + T14)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, 
      MT2, MT2]*((5*MH2*(S - T14) - S*(3*S34 + 3*T + U) + 
        T14*(S34 + 3*(T + U)))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
      4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - (S + T14)*Pair[e[1], k[3]] + 
        (2*MH2 - S34 - U)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - 
        (S + T14)*Pair[e[1], k[3]] + (2*MH2 - S34 - U)*Pair[e[1], k[4]])*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[e[2], k[1]] + 
          (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[ec[4], k[1]] + 
          (S + T14)*Pair[ec[4], k[3]])) + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
        3*(S + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 3*(S + T14)*
         Pair[ec[5], k[3]]) + 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*(2*T14*Pair[ec[5], k[1]] + 
          (S + T14)*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*(2*(-MH2 + S34 + T)*Pair[ec[5], k[1]] + 
          (4*MH2 - 2*(S34 + U))*Pair[ec[5], k[2]] - (2*MH2 + S - 7*T14 - 2*U)*
           Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (2*(-3*MH2 + S34 + T + 2*U)*Pair[ec[5], k[1]] + 2*(-2*MH2 + S34 + U)*
           Pair[ec[5], k[2]] + (2*MH2 + 7*S - T14 - 2*U)*
           Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]]) - 4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], ec[4]]*
       (Pair[e[1], k[5]]*((-S34 + U)*Pair[ec[5], k[1]] + 
          (-S + T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         ((-S - S34 + T14 + U)*Pair[ec[5], k[1]] + 2*(-S + T14)*
           Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[3]])))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*((-(S*(S34 + T)) + 2*MH2*(S - T14) + T14*(T + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
        (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
        (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      2*Pair[e[1], ec[5]]*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
          (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
          (S + T14)*Pair[ec[4], k[3]])) - 2*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
        (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
      2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
         Pair[ec[5], k[1]] + (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
      Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*(-2*(MH2 - U)*Pair[ec[5], k[1]] + 
          2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - T)*
           Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] - (2*MH2 + S - 2*T + T14)*
           Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*
         ((3*MH2 + S + 3*T14 + T24 - 2*U)*Pair[ec[5], k[1]] + 
          2*(2*S + T + 2*T14 + T24)*Pair[ec[5], k[2]] + 
          2*(-2*MH2 - 3*S + T + T14 + U)*Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
         ((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
           ((3*Pair[ec[4], k[1]] + 2*Pair[ec[4], k[3]])*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((S*(S34 + T) - 2*MH2*(S - T14) - T14*(T + U))*Pair[e[2], ec[4]] - 
        2*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]]))) + 
      2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*(T14*Pair[ec[5], k[1]] + 
            (S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[5], k[1]] + 
            (MH2 - T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*((-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] + (MH2 - T + T14)*
             Pair[ec[5], k[3]])) - 
        2*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + 2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[1]] + 2*Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd002, MH2, 0, T24, 0, 
      2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
       ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
        Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
       ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
        Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd00, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
      T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
         ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
          2*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
       (Pair[e[1], k[4]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        Pair[e[1], k[2]]*(Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd112, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
       (-(Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] - 
          3*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, 0, T24, T, MT2, MT2, MT2]*Pair[e[1], k[5]]*
     (2*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
        Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, T24, 0, T, MT2, MT2, MT2]*
     (-2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd13, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*((MH2 - T)*(S - T14)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 4*(MH2 - T)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]) + 16*Pair[e[1], k[3]]*
       ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
       Pair[ec[5], k[1]] + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
        2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
        (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(-((S - T14)*(-MH2 + S + T + T14)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) + 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]) - 16*Pair[e[1], k[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
       Pair[ec[5], k[1]] - 2*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
       ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
        2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
        (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd22, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*((S34 - U)*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[5]]*
       Pair[e[2], ec[4]] + 8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 32*Pair[e[1], k[5]]*
       ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
       Pair[ec[5], k[1]] - 4*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
       (-2*(S34 - U)*Pair[ec[5], k[1]] - (MH2 - S34 - T24 - U)*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd001, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
      2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd001, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
      2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*((S34 - U)*Pair[e[2], ec[4]] - 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[1]] + 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, T24, T, MT2, MT2, MT2]*
     (2*Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      2*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      2*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - Pair[e[2], ec[4]]*
       (2*Pair[e[1], k[2]]*Pair[ec[5], k[2]] - 2*Pair[e[1], k[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      Pair[e[1], ec[5]]*((S - 2*S34 - T14 + 2*U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] - 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))) + 
      8*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
      8*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
      2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*C0i[cc0, 0, T24, T, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      4*Pair[ec[4], k[2]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[5], k[1]]) + 
      4*Pair[e[2], k[4]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[4]]*Pair[ec[5], k[1]]) - 
      2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
        (2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, T24, 0, T, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
        4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[5]])) - 2*Pair[e[1], k[3]]*
       (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, T24, 0, T, MT2, MT2, MT2]*
     (4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
      2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] - 
          2*Pair[ec[5], k[2]]) + 2*Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
          2*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(2*(S + T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*(S + T14)*
       (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[1]] - 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]]) + Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*(-2*(S - T14)*Pair[ec[5], k[2]] + 
          (3*S - 5*T14)*Pair[ec[5], k[3]] - 4*T14*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[4]]*(4*S*Pair[ec[5], k[2]] + (-5*S + 3*T14)*
           Pair[ec[5], k[3]] + 2*(-S + T14)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((-3*S34*T - S34*T14 + 2*T14*T24 + 
          MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 3*T*U + 5*T14*U + 
          S*(-5*S34 - 2*T24 + U))*Pair[e[2], ec[4]] + 
        4*((2*(-MH2 + S34 + T24 + U)*Pair[e[2], k[1]] + 
            3*(-MH2 + S + T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(2*(-MH2 + S34 + T24 + U)*Pair[ec[4], k[1]] + 
            3*(-MH2 + S + T + T14)*Pair[ec[4], k[3]]))) + 
      4*(((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
           Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
           Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
           Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
           Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
        2*(Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
          Pair[e[1], k[4]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
          Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*(-5*Pair[e[2], k[1]]*Pair[
                ec[5], k[1]] + Pair[e[2], k[5]]*(5*Pair[ec[5], k[1]] + 
                Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*
             (-5*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] + Pair[ec[4], k[5]]*(
                5*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], 
                 k[4]]))))) - 2*Pair[e[2], ec[4]]*
       (-2*Pair[e[1], k[4]]*((MH2 - T - T14)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*
         (-5*(S34 - U)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          2*(-S + S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], k[2]]*
         ((-MH2 + S + T)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(4*(S + T14 + T24)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
       (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[3]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[3]] - 16*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
       (-(Pair[e[1], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] + 
           (-5*S + 3*T14)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]])) + Pair[e[1], k[2]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-3*S + 5*T14)*
           Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd1, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
      MT2, MT2, MT2, MT2]*(-4*((-MH2 + T)*Pair[e[1], k[2]] + 
        (S - S34 + T14 - U)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      4*((-MH2 + T)*Pair[e[1], k[2]] + (S - S34 + T14 - U)*Pair[e[1], k[3]] + 
        (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      Pair[e[1], ec[5]]*((-3*MH2*S34 + 2*S*S34 + 3*S34*T + 3*MH2*U - 3*T*U - 
          2*T14*U)*Pair[e[2], ec[4]] - 
        4*(((S34 + U)*Pair[e[2], k[1]] + (-3*MH2 + S + 3*T + T14)*
             Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           ((S34 + U)*Pair[ec[4], k[1]] + (-3*MH2 + S + 3*T + T14)*
             Pair[ec[4], k[3]]))) + 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       ((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
         Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       ((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
         Pair[ec[5], k[3]]) - 
      8*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
         (Pair[ec[4], k[2]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[1]] - 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])))) + 
      2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*(S34*Pair[ec[5], k[2]] - 
          (S34 + T14)*Pair[ec[5], k[3]] - (MH2 + S34 - T)*
           Pair[ec[5], k[4]]) + Pair[e[1], k[3]]*
         ((-5*S34 + 2*T14 + 3*U)*Pair[ec[5], k[2]] + 5*(S34 - U)*
           Pair[ec[5], k[3]] + (2*S + 3*S34 - 5*U)*Pair[ec[5], k[4]]) + 
        2*Pair[e[1], k[4]]*((MH2 - T + U)*Pair[ec[5], k[2]] - 
          (S + U)*Pair[ec[5], k[3]] - U*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((MH2 - T)*(S34 - U)*Pair[e[2], ec[4]] - 
        4*(MH2 - T)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 2*Pair[e[1], k[3]]*
       (2*(S34 + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*(S34 + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) - Pair[e[2], ec[4]]*
         ((-3*S34 + U)*Pair[ec[5], k[1]] - (S34 - 3*U)*Pair[ec[5], k[3]] + 
          2*(S34 + U)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd11, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      2*(-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
      2*(-MH2 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
      8*Pair[e[1], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
       (-((MH2 - T)*(Pair[e[1], k[2]] + Pair[e[1], k[4]])*
          Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 2*(S + S34 - T14 - U)*
           Pair[ec[5], k[3]] + (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((MH2*S34 - S*S34 - S34*T - MH2*U + T*U + T14*U)*Pair[e[2], ec[4]] + 
        2*(((S34 + U)*Pair[e[2], k[1]] + (-2*MH2 + S + 2*T + T14)*
             Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           ((S34 + U)*Pair[ec[4], k[1]] + (-2*MH2 + S + 2*T + T14)*
             Pair[ec[4], k[3]]))) + 
      2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((MH2 + S34 - T + U)*
           Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*
           Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 
        2*(Pair[e[1], k[3]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
        Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*((S + U)*Pair[ec[5], k[1]] + 
            (-MH2 + S + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
          Pair[e[1], k[3]]*((S34 - U)*Pair[ec[5], k[1]] + 
            T14*Pair[ec[5], k[2]] + S*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((S34 + T14)*Pair[ec[5], k[1]] + 
            T14*Pair[ec[5], k[2]] + (MH2 - T - T14)*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd12, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       (-((S34 - U)*(S34 - T + T24 + U)*Pair[e[2], ec[4]]) + 
        4*(2*MH2 - S - 2*T - T14)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
      2*(2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
          (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
          (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(3*Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[5]]*((3*S34 - U)*Pair[ec[5], k[2]] - 
            4*(S34 - U)*Pair[ec[5], k[3]] - (S34 - 3*U)*Pair[ec[5], k[4]]) + 
          2*Pair[e[1], k[3]]*((MH2 - 3*S34 - T24 + U)*Pair[ec[5], k[2]] + 
            2*(S34 - U)*Pair[ec[5], k[3]] + (MH2 + S34 - T24 - 3*U)*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], ec[4]]*((-S - S34 + T14 + U)*
         Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         ((S34 - U)*Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[2]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         ((-2*Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         (Pair[ec[4], k[2]]*(2*Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*(-4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[5]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
             (-4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + Pair[ec[5], k[
                4]]))))))/(MW*SW)) + 
 Den[T24, 0]*Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
  ((-8*Alfas2*EL*MT2*D0i[dd133, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd133, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd3, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(-(((-MH2 + T)*Pair[e[1], k[2]] + 
         (S - T14)*Pair[e[1], k[3]] + (-MH2 + T)*Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) - 2*(MH2 - T)*
       (Pair[e[1], ec[4]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd233, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
     Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[5]]*
     ((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
      4*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
     Pair[ec[5], k[1]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd003, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd003, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd3, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((MH2 - T)*(S - T14)*Pair[e[2], ec[4]] - 
        4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 2*Pair[e[1], k[3]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd222, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd111, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd112, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(((-S - 2*S34 + T14 + 2*U)*Pair[e[1], k[3]] + 
        (-S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
      4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
         (3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*
           Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(3*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd122, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(((-2*S - S34 + 2*T14 + U)*Pair[e[1], k[3]] + 
        (-S - S34 + T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
      4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
        Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          2*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]])/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd122, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*Pair[ec[5], k[1]] - Pair[e[1], k[5]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(2*Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd13, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
      Pair[e[1], k[3]]*(-2*(S + T14)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
        2*(S + T14)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
      Pair[e[2], ec[4]]*(2*(-MH2 + T)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        (MH2 - T)*Pair[e[1], k[5]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[3]]*((MH2 + S - T - 3*T14)*Pair[ec[5], k[1]] - 
          2*(S + T14)*Pair[ec[5], k[2]] + (3*S - T14)*Pair[ec[5], k[3]]))))/
    (MW*SW) - (4*Alfas2*EL*MT2*D0i[dd12, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
      T, MT2, MT2, MT2, MT2]*
     (2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*
         Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      2*(MH2 + S - T + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[3]] - 2*(MH2 + S - T + T14)*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
      8*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
        Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          3*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           (4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
      Pair[e[2], ec[4]]*(2*(MH2 + S - T + T14)*Pair[e[1], k[2]]*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*((MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[1]] + 2*(MH2 + 3*(S + T14 + T24))*
           Pair[ec[5], k[2]] + (2*MH2 - 12*S - 4*S34 - T - 5*T24)*
           Pair[ec[5], k[3]]) + Pair[e[1], k[5]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 2*(MH2 + S + T14 + T24)*
           Pair[ec[5], k[2]] + (2*MH2 - 6*S - 4*S34 - T - 2*T14 - 3*T24)*
           Pair[ec[5], k[3]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
        (-S + T14)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd113, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*Pair[e[1], k[3]]*
     (Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
        (-S + T14)*Pair[ec[5], k[3]]) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*D0i[dd2, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, 
      MT2, MT2]*((5*MH2*(S - T14) - S*(3*S34 + 3*T + U) + 
        T14*(S34 + 3*(T + U)))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
      4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - (S + T14)*Pair[e[1], k[3]] + 
        (2*MH2 - S34 - U)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - 
        (S + T14)*Pair[e[1], k[3]] + (2*MH2 - S34 - U)*Pair[e[1], k[4]])*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
       (((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[e[2], k[1]] + 
          (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[ec[4], k[1]] + 
          (S + T14)*Pair[ec[4], k[3]])) + 4*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
        3*(S + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 3*(S + T14)*
         Pair[ec[5], k[3]]) + 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[3]]*(2*T14*Pair[ec[5], k[1]] + 
          (S + T14)*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
        Pair[e[1], k[2]]*(2*(-MH2 + S34 + T)*Pair[ec[5], k[1]] + 
          (4*MH2 - 2*(S34 + U))*Pair[ec[5], k[2]] - (2*MH2 + S - 7*T14 - 2*U)*
           Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
         (2*(-3*MH2 + S34 + T + 2*U)*Pair[ec[5], k[1]] + 2*(-2*MH2 + S34 + U)*
           Pair[ec[5], k[2]] + (2*MH2 + 7*S - T14 - 2*U)*
           Pair[ec[5], k[3]])) + 
      8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
        (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]]) - 4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], ec[4]]*
       (Pair[e[1], k[5]]*((-S34 + U)*Pair[ec[5], k[1]] + 
          (-S + T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         ((-S - S34 + T14 + U)*Pair[ec[5], k[1]] + 2*(-S + T14)*
           Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 
          Pair[ec[5], k[3]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[3]])))))/(MW*SW) - 
   (2*Alfas2*EL*MT2*D0i[dd1, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*((-(S*(S34 + T)) + 2*MH2*(S - T14) + T14*(T + U))*
       Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
        (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
        (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      2*Pair[e[1], ec[5]]*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
          (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
          (S + T14)*Pair[ec[4], k[3]])) - 2*Pair[e[1], ec[4]]*
       Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
        (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
      2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
         Pair[ec[5], k[1]] + (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
      Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*(-2*(MH2 - U)*Pair[ec[5], k[1]] + 
          2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - T)*
           Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*
         ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] - (2*MH2 + S - 2*T + T14)*
           Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*
         ((3*MH2 + S + 3*T14 + T24 - 2*U)*Pair[ec[5], k[1]] + 
          2*(2*S + T + 2*T14 + T24)*Pair[ec[5], k[2]] + 
          2*(-2*MH2 - 3*S + T + T14 + U)*Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
         ((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
           (Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
           ((3*Pair[ec[4], k[1]] + 2*Pair[ec[4], k[3]])*Pair[ec[5], k[3]] + 
            Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]))))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((S*(S34 + T) - 2*MH2*(S - T14) - T14*(T + U))*Pair[e[2], ec[4]] - 
        2*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]]))) + 
      2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*(T14*Pair[ec[5], k[1]] + 
            (S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[5], k[1]] + 
            (MH2 - T)*Pair[ec[5], k[2]] + S*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*((-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            (-MH2 + T)*Pair[ec[5], k[2]] + (MH2 - T + T14)*
             Pair[ec[5], k[3]])) - 
        2*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[1]] + 2*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*Pair[
                ec[5], k[1]] + 2*Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd002, MH2, 0, T24, 0, 
      2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
       ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
        Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
       ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
        Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
        Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd00, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
      T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
         ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
          Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[3]])) + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
          2*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
       (Pair[e[1], k[4]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
        Pair[e[1], k[2]]*(Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]]))))/
    (MW*SW) - (8*Alfas2*EL*MT2*D0i[dd112, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
       (-(Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]])*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] - 
          3*Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd223, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
        Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]])))/(MW*SW) + 
   (4*Alfas2*EL*MT2*C0i[cc1, 0, T24, T, MT2, MT2, MT2]*Pair[e[1], k[5]]*
     (2*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] - Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
        Pair[ec[5], k[4]])))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc1, T24, 0, T, MT2, MT2, MT2]*
     (-2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd13, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*((MH2 - T)*(S - T14)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
      4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 4*(MH2 - T)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]) + 16*Pair[e[1], k[3]]*
       ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
       Pair[ec[5], k[1]] + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
       ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
        2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
        (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd23, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(-((S - T14)*(-MH2 + S + T + T14)*Pair[e[1], ec[5]]*
        Pair[e[2], ec[4]]) + 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]]) - 16*Pair[e[1], k[5]]*
       ((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
       Pair[ec[5], k[1]] - 2*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
       ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
        2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
        (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd22, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*((S34 - U)*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[5]]*
       Pair[e[2], ec[4]] + 8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T24 + U)*
       Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
       (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
         Pair[ec[4], k[3]]) - 32*Pair[e[1], k[5]]*
       ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
        Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
       Pair[ec[5], k[1]] - 4*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
       (-2*(S34 - U)*Pair[ec[5], k[1]] - (MH2 - S34 - T24 - U)*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd001, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
      2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd001, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[3]]*
       (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]]) + Pair[e[1], ec[5]]*
       ((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
      2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
         Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (8*Alfas2*EL*MT2*D0i[dd002, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*((S34 - U)*Pair[e[2], ec[4]] - 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] - 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[1]] + 2*Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         Pair[ec[5], k[1]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*C0i[cc2, 0, T24, T, MT2, MT2, MT2]*
     (2*Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      2*Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
      2*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
        Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       (Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - Pair[e[2], ec[4]]*
       (2*Pair[e[1], k[2]]*Pair[ec[5], k[2]] - 2*Pair[e[1], k[4]]*
         Pair[ec[5], k[4]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd00, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
      4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      Pair[e[1], ec[5]]*((S - 2*S34 - T14 + 2*U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] - 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))) + 
      8*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
      8*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
      2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))/(MW*SW) - 
   (Alfas2*EL*MT2*C0i[cc0, 0, T24, T, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
        4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
      4*Pair[ec[4], k[2]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
         Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[5], k[1]]) + 
      4*Pair[e[2], k[4]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
         Pair[ec[4], ec[5]] + Pair[e[1], ec[4]]*Pair[ec[5], k[1]]) - 
      2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
        Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
        (2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*C0i[cc2, T24, 0, T, MT2, MT2, MT2]*
     (Pair[e[1], ec[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
        4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[5]])) - 2*Pair[e[1], k[3]]*
       (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*C0i[cc0, T24, 0, T, MT2, MT2, MT2]*
     (4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[e[2], k[4]]*
       Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*
       Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[e[1], e[2]]*
       Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
      2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] - 
          2*Pair[ec[5], k[2]]) + 2*Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
          Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
          2*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd23, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(2*(S + T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*(S + T14)*
       (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[1]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[1]] - 8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
        Pair[ec[5], k[4]]) + Pair[e[2], ec[4]]*
       (Pair[e[1], k[2]]*(-2*(S - T14)*Pair[ec[5], k[2]] + 
          (3*S - 5*T14)*Pair[ec[5], k[3]] - 4*T14*Pair[ec[5], k[4]]) + 
        Pair[e[1], k[4]]*(4*S*Pair[ec[5], k[2]] + (-5*S + 3*T14)*
           Pair[ec[5], k[3]] + 2*(-S + T14)*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (Alfas2*EL*MT2*D0i[dd2, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((-3*S34*T - S34*T14 + 2*T14*T24 + 
          MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 3*T*U + 5*T14*U + 
          S*(-5*S34 - 2*T24 + U))*Pair[e[2], ec[4]] + 
        4*((2*(-MH2 + S34 + T24 + U)*Pair[e[2], k[1]] + 
            3*(-MH2 + S + T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(2*(-MH2 + S34 + T24 + U)*Pair[ec[4], k[1]] + 
            3*(-MH2 + S + T + T14)*Pair[ec[4], k[3]]))) + 
      4*(((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
           Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
           Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
           Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
           Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
        2*(Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
          Pair[e[1], k[4]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
          Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*(-5*Pair[e[2], k[1]]*Pair[
                ec[5], k[1]] + Pair[e[2], k[5]]*(5*Pair[ec[5], k[1]] + 
                Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*
             (-5*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] + Pair[ec[4], k[5]]*(
                5*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], 
                 k[4]]))))) - 2*Pair[e[2], ec[4]]*
       (-2*Pair[e[1], k[4]]*((MH2 - T - T14)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*
         (-5*(S34 - U)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
          2*(-S + S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], k[2]]*
         ((-MH2 + S + T)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
           Pair[ec[5], k[4]]))))/(MW*SW) + 
   (4*Alfas2*EL*MT2*D0i[dd22, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(4*(S + T14 + T24)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
       (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
       Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       Pair[ec[5], k[3]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       Pair[ec[5], k[3]] - 16*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
       (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
         Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
       (-(Pair[e[1], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] + 
           (-5*S + 3*T14)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]])) + Pair[e[1], k[2]]*
         (2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-3*S + 5*T14)*
           Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))))/
    (MW*SW) - (Alfas2*EL*MT2*D0i[dd1, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
      MT2, MT2, MT2, MT2]*(-4*((-MH2 + T)*Pair[e[1], k[2]] + 
        (S - S34 + T14 - U)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
      4*((-MH2 + T)*Pair[e[1], k[2]] + (S - S34 + T14 - U)*Pair[e[1], k[3]] + 
        (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      Pair[e[1], ec[5]]*((-3*MH2*S34 + 2*S*S34 + 3*S34*T + 3*MH2*U - 3*T*U - 
          2*T14*U)*Pair[e[2], ec[4]] - 
        4*(((S34 + U)*Pair[e[2], k[1]] + (-3*MH2 + S + 3*T + T14)*
             Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           ((S34 + U)*Pair[ec[4], k[1]] + (-3*MH2 + S + 3*T + T14)*
             Pair[ec[4], k[3]]))) + 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
       ((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
         Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
       ((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
         Pair[ec[5], k[3]]) - 
      8*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[3]] + Pair[e[2], k[4]]*
           (-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
         (Pair[ec[4], k[2]]*(2*Pair[e[2], k[3]]*Pair[ec[5], k[1]] - 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - 
            Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])))) + 
      2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*(S34*Pair[ec[5], k[2]] - 
          (S34 + T14)*Pair[ec[5], k[3]] - (MH2 + S34 - T)*
           Pair[ec[5], k[4]]) + Pair[e[1], k[3]]*
         ((-5*S34 + 2*T14 + 3*U)*Pair[ec[5], k[2]] + 5*(S34 - U)*
           Pair[ec[5], k[3]] + (2*S + 3*S34 - 5*U)*Pair[ec[5], k[4]]) + 
        2*Pair[e[1], k[4]]*((MH2 - T + U)*Pair[ec[5], k[2]] - 
          (S + U)*Pair[ec[5], k[3]] - U*Pair[ec[5], k[4]]))))/(MW*SW) + 
   (2*Alfas2*EL*MT2*D0i[dd11, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
      MT2, MT2]*(Pair[e[1], ec[5]]*((MH2 - T)*(S34 - U)*Pair[e[2], ec[4]] - 
        4*(MH2 - T)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 2*Pair[e[1], k[3]]*
       (2*(S34 + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*(S34 + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]] - 
          Pair[ec[5], k[4]]) - Pair[e[2], ec[4]]*
         ((-3*S34 + U)*Pair[ec[5], k[1]] - (S34 - 3*U)*Pair[ec[5], k[3]] + 
          2*(S34 + U)*Pair[ec[5], k[4]]))))/(MW*SW) - 
   (4*Alfas2*EL*MT2*D0i[dd11, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
      MT2, MT2, MT2]*(2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*
       Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*
       Pair[e[1], k[3]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
      2*(-MH2 + T)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
      2*(-MH2 + T)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
      8*Pair[e[1], k[3]]*((Pair[e[2], k[1]] + Pair[e[2], k[3]])*
         Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
       (-((MH2 - T)*(Pair[e[1], k[2]] + Pair[e[1], k[4]])*
          Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
         ((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 2*(S + S34 - T14 - U)*
           Pair[ec[5], k[3]] + (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))/
    (MW*SW) + (Alfas2*EL*MT2*D0i[dd0, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
      MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       ((MH2*S34 - S*S34 - S34*T - MH2*U + T*U + T14*U)*Pair[e[2], ec[4]] + 
        2*(((S34 + U)*Pair[e[2], k[1]] + (-2*MH2 + S + 2*T + T14)*
             Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           ((S34 + U)*Pair[ec[4], k[1]] + (-2*MH2 + S + 2*T + T14)*
             Pair[ec[4], k[3]]))) + 
      2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
          (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((MH2 + S34 - T + U)*
           Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*
           Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 
        2*(Pair[e[1], k[3]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
        Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*((S + U)*Pair[ec[5], k[1]] + 
            (-MH2 + S + T)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
          Pair[e[1], k[3]]*((S34 - U)*Pair[ec[5], k[1]] + 
            T14*Pair[ec[5], k[2]] + S*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[2]]*((S34 + T14)*Pair[ec[5], k[1]] + 
            T14*Pair[ec[5], k[2]] + (MH2 - T - T14)*Pair[ec[5], k[4]])))))/
    (MW*SW) - (2*Alfas2*EL*MT2*D0i[dd12, MH2, T24, 0, 0, MH2 - S - T - T14, 
      T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
       (-((S34 - U)*(S34 - T + T24 + U)*Pair[e[2], ec[4]]) + 
        4*(2*MH2 - S - 2*T - T14)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
      2*(2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
          (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
          (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(3*Pair[ec[5], k[1]] + 
            Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[5]]*((3*S34 - U)*Pair[ec[5], k[2]] - 
            4*(S34 - U)*Pair[ec[5], k[3]] - (S34 - 3*U)*Pair[ec[5], k[4]]) + 
          2*Pair[e[1], k[3]]*((MH2 - 3*S34 - T24 + U)*Pair[ec[5], k[2]] + 
            2*(S34 - U)*Pair[ec[5], k[3]] + (MH2 + S34 - T24 - 3*U)*
             Pair[ec[5], k[4]])))))/(MW*SW) + 
   (8*Alfas2*EL*MT2*D0i[dd123, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, 
      MT2, MT2, MT2]*(Pair[e[2], ec[4]]*((-S - S34 + T14 + U)*
         Pair[e[1], k[3]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         ((S34 - U)*Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]])) + 
      4*(Pair[e[1], k[2]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
         ((-2*Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
         (Pair[ec[4], k[2]]*(2*Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*(-4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*
           (2*Pair[ec[4], k[5]]*Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*
             (-4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + Pair[ec[5], k[
                4]]))))))/(MW*SW)) - 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu4, Glu2, Glu5, 0, 0]]*
   (Den[T14, 0]*(-4*D0i[dd12, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
        0, T14, MT2, MT2, MT2, MT2]*(-2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
           Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) - 
        2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
        8*Pair[e[2], k[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] - 
      2*C0i[cc1, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-S34 + T)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[3]])) - 2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + 
            Pair[e[2], k[4]]) - 2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]) - 
      D0i[dd1, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((S - T24)*(MH2 - U)*Pair[e[1], ec[4]] - 
          4*(MH2 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 2*Pair[e[2], k[3]]*
         ((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      D0i[dd1, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(Pair[e[2], ec[5]]*(-((S - T24)*(-MH2 + S + T24 + U)*
            Pair[e[1], ec[4]]) - 4*(MH2 - S - T24 - U)*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])) + 2*(-2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*(2*T24*Pair[e[2], k[1]] + 
            2*S*Pair[e[2], k[4]] + (S - T24)*Pair[e[2], k[5]]) - 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          4*Pair[e[2], k[3]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      8*(D0i[dd112, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        D0i[dd112, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        (D0i[dd113, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd113, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*
         (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      8*(D0i[dd333, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
         MT2, MT2, MT2, MT2] + D0i[dd333, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
         T14, MT2, MT2, MT2, MT2])*(-((S - T24)*Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
        4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
      4*D0i[dd23, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
        MT2, MT2, MT2, MT2]*(2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
           Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) + 
        2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
        8*Pair[e[2], k[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           (Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
      8*(D0i[dd222, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*((-S34 + T)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((-S - 2*S34 + 2*T + T24)*
             Pair[e[2], k[3]] + (-S34 + T)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
                Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]) - 
      8*(D0i[dd233, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*((-S + S34 - T + T24)*
             Pair[e[2], k[3]] + (-2*S + S34 - T + 2*T24)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[5]] + Pair[e[2], k[5]]*(Pair[ec[4], k[3]] + 
                2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]] + 
        D0i[dd233, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((-2*S - S34 + T + 2*T24)*
             Pair[e[2], k[3]] + (-S - S34 + T + T24)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 
              2*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
              Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
         Pair[ec[5], k[3]]) + 4*D0i[dd12, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
        T14, MT2, MT2, MT2, MT2]*
       (2*(MH2 - U)*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        2*Pair[e[2], k[3]]*((S + T24)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
          (S + T24)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] - 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
        Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            Pair[ec[5], k[2]]) - Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] - (S - 3*T24)*Pair[ec[5], k[2]] + 
            (-3*S + T24)*Pair[ec[5], k[3]]))) - 
      4*D0i[dd23, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (2*Pair[e[1], k[4]]*((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] - 
        2*(MH2 + S + T24 - U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
         Pair[ec[5], k[3]] - 2*(MH2 + S + T24 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        8*(2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
           Pair[ec[4], k[1]] + Pair[e[1], k[5]]*(3*Pair[e[2], k[3]] + 
            Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
            Pair[e[2], k[3]]*(4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*(-2*(MH2 + S + T24 - U)*
           Pair[e[2], k[1]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
           (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
            (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
            (2*MH2 - 12*S - 4*S34 - 5*T14 - U)*Pair[ec[5], k[3]]) - 
          Pair[e[2], k[5]]*(2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
            (MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
            (2*MH2 - 6*S - 4*S34 - 3*T14 - 2*T24 - U)*Pair[ec[5], k[3]]))) - 
      2*D0i[dd2, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (-2*Pair[e[1], ec[5]]*((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
          (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        2*Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*Pair[e[1], k[2]]*
           Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*Pair[ec[4], k[2]] + 
            (S + T24)*Pair[ec[4], k[3]])) - 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], k[4]]*(((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
            (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]])) - 
        Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*
           Pair[e[2], ec[5]] + 2*Pair[e[2], k[1]]*((MH2 + S + T14 + T24)*
             Pair[ec[5], k[2]] - (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[5]]*(2*(MH2 - U)*Pair[ec[5], k[1]] - 
            2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - U)*
             Pair[ec[5], k[3]]) - Pair[e[2], k[3]]*
           (2*(2*S + T14 + 2*T24 + U)*Pair[ec[5], k[1]] + 
            (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[2]] + 
            2*(-2*MH2 - 3*S + T + T24 + U)*Pair[ec[5], k[3]])) + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 3*Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[3]] + 3*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[5]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
           (-(Pair[e[2], k[5]]*Pair[ec[5], k[2]]) + 2*Pair[e[2], k[3]]*
             Pair[ec[5], k[3]]))) + D0i[dd0, 0, U, 0, 2*MH2 - S34 - T - U, 
        MH2, T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
         ((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]]*((S + T24)*Pair[ec[5], k[1]] + T24*Pair[ec[5], 
                k[2]] - S*Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
             ((MH2 - U)*Pair[ec[5], k[1]] + (-MH2 + T)*Pair[ec[5], k[2]] + 
              S*Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             ((-MH2 + U)*Pair[ec[5], k[1]] + (-2*MH2 + S34 + U)*Pair[ec[5], 
                k[2]] + (MH2 + T24 - U)*Pair[ec[5], k[3]]))) + 
        2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*
             Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*
               Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (MH2 + S + T24 - U)*Pair[ec[5], k[3]]) - 
          2*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] - Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
            Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
              (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
              (MH2 + S + T24 - U)*Pair[ec[5], k[3]])))) - 
      8*(D0i[dd122, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[2]] + 
        D0i[dd123, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
          Pair[e[1], ec[4]]*((-S + S34 - T + T24)*Pair[e[2], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[2], k[5]]*((-2*S + S34 - T + 2*T24)*
               Pair[ec[5], k[2]] + (-S + T24)*Pair[ec[5], k[3]]))) + 
        D0i[dd123, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
              Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
           (Pair[e[2], k[5]]*((-S34 + T)*Pair[ec[5], k[2]] + (-S + T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[3]]*((-S - S34 + T + T24)*Pair[
                ec[5], k[2]] + 2*(-S + T24)*Pair[ec[5], k[3]]))) + 
        D0i[dd122, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*(Pair[e[1], ec[4]]*
           ((-S34 + T)*Pair[ec[5], k[2]] + (-S + T24)*Pair[ec[5], k[3]]) + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
            Pair[e[1], k[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))) - 
      D0i[dd3, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[4]]*((5*MH2*(S - T24) - S*(3*S34 + T + 3*U) + 
            T24*(S34 + 3*(T + U)))*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[1]]*(2*(-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
              2*(MH2 - S34 - U)*Pair[ec[5], k[2]] + (2*MH2 + S - 2*T - 7*T24)*
               Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*(-2*(S + T24)*Pair[
                ec[5], k[1]] - 2*T24*Pair[ec[5], k[2]] + (S + T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[4]]*((4*MH2 - 2*(S34 + T))*Pair[
                ec[5], k[1]] + (6*MH2 - 2*(S34 + 2*T + U))*Pair[ec[5], 
                k[2]] + (-2*MH2 - 7*S + 2*T + T24)*Pair[ec[5], k[3]]))) + 
        4*(Pair[e[1], ec[5]]*((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
            (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*
           ((-5*MH2 + 2*S34 + 2*T + 3*U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             ((-5*MH2 + 2*S34 + 2*T + 3*U)*Pair[ec[4], k[2]] + 
              (S + T24)*Pair[ec[4], k[3]])) + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] - 
            3*(S + T24)*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - (S + T24)*Pair[e[2], 
                k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*(S + T14 + T24)*Pair[
                ec[5], k[2]] - 3*(S + T24)*Pair[ec[5], k[3]])) + 
          2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
              Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], k[2]]*Pair[ec[4], k[1]]*(Pair[e[2], k[3]]*Pair[ec[5], 
                k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[5], 
                k[3]]) + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
               Pair[ec[5], k[2]] - (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(
                Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) - 
                4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]))))) + 
      8*(D0i[dd00, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*
           ((S - T24)*Pair[e[1], ec[4]] - 4*(Pair[e[1], k[2]]*Pair[ec[4], 
                k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
          (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
            2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[1]]))*Pair[ec[5], k[2]]) + 
        D0i[dd00, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
            Pair[e[1], k[4]]*((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] - 
                2*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           ((S - T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
             (2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])))) - 
      8*(D0i[dd133, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
         MT2, MT2, MT2, MT2] + D0i[dd133, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
         T14, MT2, MT2, MT2, MT2])*(-((S - T24)*Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(Pair[ec[5], k[2]] + 
           Pair[ec[5], k[3]])) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
          Pair[ec[5], k[4]])) + 2*D0i[dd2, 0, MH2 - S - T24 - U, MH2, 
        2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
       (2*(-MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
        2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
        4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (S - S34 + T - T24)*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
      8*C0i[cc2, U, 0, T14, MT2, MT2, MT2]*
       (Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
        Pair[e[1], ec[4]]*(Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[4]]*Pair[ec[5], k[4]])) - 
      4*D0i[dd22, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*Pair[e[1], ec[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 2*(-MH2 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
         (-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + 2*(S + S34 - T - T24)*Pair[ec[5], k[3]] + 
            (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))) + 
      D0i[dd3, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(-((MH2*(S - T24) - S*(S34 - T + U) + 
           T24*(-S34 + T + U))*Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) + 
        4*Pair[e[1], k[4]]*((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
         ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
           Pair[e[2], k[4]])*Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
         ((-MH2 + U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[4], k[2]] + 
            (S + T24)*Pair[ec[4], k[3]])) - 
        8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - (Pair[e[2], k[1]] - 
              Pair[e[2], k[4]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]])) - Pair[e[1], k[3]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - 
            Pair[ec[5], k[3]])) + 4*(S + T24)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 4*(S + T24)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 2*Pair[e[1], ec[4]]*
         (2*(S + T24)*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[5]]*(2*(-MH2 + T + U)*Pair[ec[5], k[1]] + 
            (S + T24)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + U)*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[3]]*
           ((-MH2 + S + T + U)*Pair[ec[5], k[1]] + T24*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      4*(C0i[cc2, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
         (-(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
             Pair[ec[4], k[1]])*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        C0i[cc1, U, 0, T14, MT2, MT2, MT2]*(2*Pair[ec[4], k[1]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 2*Pair[e[1], k[4]]*
           (Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
           (2*Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 2*Pair[e[2], k[4]]*
             Pair[ec[5], k[4]] - Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]])))) - C0i[cc0, U, 0, T14, MT2, MT2, MT2]*
       (4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
              (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
          Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*(3*Pair[ec[5], k[2]] - 
              2*Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]])))) + C0i[cc0, MH2 - S - T24 - U, MH2, T14, 
        MT2, MT2, MT2]*
       (-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[4]]*Pair[ec[4], k[5]])) + Pair[ec[4], k[1]]*
           (Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]) + 
            Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + S34 - T + T24)*Pair[e[2], ec[5]] + 
          2*(-2*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
            2*Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))) + 
      4*(D0i[dd13, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        D0i[dd13, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (2*(S + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + 2*(S + T24)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
          2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
            Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]]*(-2*(S - T24)*Pair[ec[5], k[1]] + 
              (3*S - 5*T24)*Pair[ec[5], k[3]] - 4*T24*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(4*S*Pair[ec[5], k[1]] + (-5*S + 3*T24)*Pair[
                ec[5], k[3]] + 2*(-S + T24)*Pair[ec[5], k[4]])))) + 
      4*(D0i[dd33, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
        D0i[dd33, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (4*(S + T14 + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
           Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + 2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]] + 2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[3]] - 16*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
           (-(Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] + 
               (-5*S + 3*T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
                Pair[ec[5], k[4]])) + Pair[e[2], k[1]]*
             (2*(S + T14 + T24)*Pair[ec[5], k[1]] + (-3*S + 5*T24)*Pair[
                ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]])))) + 
      D0i[dd0, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((S34 + T)*Pair[e[1], k[2]]*
             Pair[ec[4], k[1]] - (S + T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((S34 + T)*Pair[ec[4], 
                k[2]] - (S + T24)*Pair[ec[4], k[3]])) - Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
              Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
            Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[
                e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], 
              ec[5]] - Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], 
                k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*((-(S*T) + S34*T24)*Pair[e[2], ec[5]] + 
          2*(Pair[e[2], k[4]]*((-MH2 + S + U)*Pair[ec[5], k[1]] + 
              (-MH2 + S + S34 + U)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
            Pair[e[2], k[3]]*(T24*Pair[ec[5], k[1]] + (-S + T24)*Pair[ec[5], 
                k[2]] + S*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (T24*Pair[ec[5], k[1]] + (-MH2 + T + T24 + U)*Pair[ec[5], 
                k[2]] + (MH2 - T24 - U)*Pair[ec[5], k[4]])))) - 
      8*((D0i[dd001, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd001, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*(Pair[e[2], ec[5]]*
           ((-S + T24)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], 
                k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
          2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
            2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[1]]))*Pair[ec[5], k[2]]) + 
        (D0i[dd003, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd003, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*
         (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
             ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
           ((-S + T24)*Pair[e[2], ec[5]] - 2*(Pair[e[2], k[1]]*(
                2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + Pair[e[2], k[4]]*(
                Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]])))) + 
        D0i[dd002, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*((-S + S34 - T + T24)*
             Pair[e[1], ec[4]] + 4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[2], k[5]]*
           (-2*(Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[
                ec[4], k[1]]) + Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]]))) + D0i[dd002, 0, U, 0, 2*MH2 - S34 - T - U, 
          MH2, T14, MT2, MT2, MT2, MT2]*
         (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[3]]) + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*((-S34 + T)*Pair[e[2], ec[5]] - 
            2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[3]] + 
              Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))))) - 
    Den[T24, 0]*((8*D0i[dd133, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
          MT2, MT2, MT2] - 8*D0i[dd133, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2])*Pair[e[1], k[3]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] - 
      2*D0i[dd3, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-(((-MH2 + T)*Pair[e[1], k[2]] + (S - T14)*Pair[e[1], k[3]] + 
           (-MH2 + T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]) - 
        2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 4*Pair[e[1], k[3]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] - 
      8*D0i[dd003, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
           Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      D0i[dd3, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2 - T)*(S - T14)*Pair[e[2], ec[4]] - 
          4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 2*Pair[e[1], k[3]]*
         ((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      8*(D0i[dd233, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         Pair[e[1], k[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] + 
        D0i[dd233, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      (8*D0i[dd111, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2] - 8*D0i[dd111, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
          MT2, MT2])*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + 
      4*D0i[dd13, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (-2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
        Pair[e[1], k[3]]*(-2*(S + T14)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
          2*(S + T14)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
        Pair[e[2], ec[4]]*(2*(-MH2 + T)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          (MH2 - T)*Pair[e[1], k[5]]*Pair[ec[5], k[1]] + 
          Pair[e[1], k[3]]*((MH2 + S - T - 3*T14)*Pair[ec[5], k[1]] - 
            2*(S + T14)*Pair[ec[5], k[2]] + (3*S - T14)*Pair[ec[5], 
              k[3]]))) + 4*D0i[dd12, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, 
        MT2, MT2, MT2, MT2]*(2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*((-7*MH2 + 3*(S34 + T + U))*
           Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*Pair[e[1], k[5]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 2*(MH2 + S - T + T14)*
         Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
        2*(MH2 + S - T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]] + 8*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]] + Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
          Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            3*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             (4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] + Pair[e[2], ec[4]]*(2*(MH2 + S - T + T14)*
           Pair[e[1], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[3]]*
           ((MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
            2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
            (2*MH2 - 12*S - 4*S34 - T - 5*T24)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[5]]*((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
            2*(MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
            (2*MH2 - 6*S - 4*S34 - T - 2*T14 - 3*T24)*Pair[ec[5], k[3]]))) - 
      8*D0i[dd113, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       Pair[e[1], k[3]]*(Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
          (-S + T14)*Pair[ec[5], k[3]]) + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
      D0i[dd2, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       ((5*MH2*(S - T14) - S*(3*S34 + 3*T + U) + T14*(S34 + 3*(T + U)))*
         Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - (S + T14)*Pair[e[1], k[3]] + 
          (2*MH2 - S34 - U)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - 
          (S + T14)*Pair[e[1], k[3]] + (2*MH2 - S34 - U)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
         (((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]])) + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          3*(S + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          3*(S + T14)*Pair[ec[5], k[3]]) + 2*Pair[e[2], ec[4]]*
         (Pair[e[1], k[3]]*(2*T14*Pair[ec[5], k[1]] + (S + T14)*
             (2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[2]]*(2*(-MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            (4*MH2 - 2*(S34 + U))*Pair[ec[5], k[2]] - 
            (2*MH2 + S - 7*T14 - 2*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(2*(-3*MH2 + S34 + T + 2*U)*Pair[ec[5], k[1]] + 
            2*(-2*MH2 + S34 + U)*Pair[ec[5], k[2]] + 
            (2*MH2 + 7*S - T14 - 2*U)*Pair[ec[5], k[3]])) + 
        8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
          (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 
            4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - 
                Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[1]]*Pair[ec[5], 
                k[3]])))) + 2*D0i[dd1, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
        T, MT2, MT2, MT2, MT2]*((-(S*(S34 + T)) + 2*MH2*(S - T14) + 
          T14*(T + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
        2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
          (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
        2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
          (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        2*Pair[e[1], ec[5]]*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]])) - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*(-2*(MH2 - U)*Pair[ec[5], k[1]] + 
            2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - T)*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*
           ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] - (2*MH2 + S - 2*T + T14)*
             Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*
           ((3*MH2 + S + 3*T14 + T24 - 2*U)*Pair[ec[5], k[1]] + 
            2*(2*S + T + 2*T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-2*MH2 - 3*S + T + T14 + U)*Pair[ec[5], k[3]])) + 
        4*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*
             Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[5]]*
             Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[e[2], k[4]]*((3*Pair[ec[4], k[1]] + 2*Pair[ec[4], k[3]])*
               Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]]))))) + 
      8*(D0i[dd122, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(((-2*S - S34 + 2*T14 + U)*Pair[e[1], k[3]] + 
            (-S - S34 + T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
          4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(
                2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              2*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(
                3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
         Pair[ec[5], k[3]] + D0i[dd122, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
           (Pair[e[1], k[3]]*Pair[ec[5], k[1]] - Pair[e[1], k[5]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(2*Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]))) + D0i[dd123, MH2, 0, T24, 0, 
          2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
         (Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*((-S34 + U)*Pair[ec[5], k[1]] + 
              (-S + T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
             ((-S - S34 + T14 + U)*Pair[ec[5], k[1]] + 2*(-S + T14)*Pair[
                ec[5], k[3]])) + 4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[
                ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                 Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]]))))) - D0i[dd0, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
        T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
         ((S*(S34 + T) - 2*MH2*(S - T14) - T14*(T + U))*Pair[e[2], ec[4]] - 
          2*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + (S + T14)*Pair[
                e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             ((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + (S + T14)*Pair[
                ec[4], k[3]]))) + 2*(((-MH2 + T)*Pair[e[1], k[2]] + 
            (S + T14)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) - 
          Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*(T14*Pair[ec[5], k[1]] + 
              (S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
            Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[5], k[1]] + (MH2 - T)*Pair[
                ec[5], k[2]] + S*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
             ((-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + (-MH2 + T)*Pair[ec[5], 
                k[2]] + (MH2 - T + T14)*Pair[ec[5], k[3]])) - 
          2*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[
                ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                  Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[1]] + 2*Pair[e[2], k[1]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]] + 2*Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]]))))) + 
      8*(D0i[dd112, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(((-S - 2*S34 + T14 + 2*U)*Pair[e[1], k[3]] + 
            (-S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
          4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
             (3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*(3*Pair[ec[4], k[3]] + 
                Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]] + 
        D0i[dd113, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*Pair[e[1], k[3]]*(Pair[e[2], ec[4]]*
           ((-S34 + U)*Pair[ec[5], k[1]] + (-S + T14)*Pair[ec[5], k[3]]) + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
        D0i[dd112, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         ((S34 - U)*Pair[e[2], ec[4]]*(-(Pair[e[1], k[5]]*Pair[ec[5], k[
                3]]) + Pair[e[1], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[
                3]])) + 4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]] + 
            Pair[e[1], k[3]]*(Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])))) + 8*D0i[dd00, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
            2*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
         (Pair[e[1], k[4]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]]))) + 
      8*(D0i[dd222, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         Pair[e[1], k[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
          4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
         Pair[ec[5], k[1]] + D0i[dd223, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2]*Pair[e[1], k[5]]*
         ((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
          4*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
         Pair[ec[5], k[1]] + D0i[dd222, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
          T, MT2, MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
            Pair[ec[5], k[4]]))) - 4*C0i[cc1, 0, T24, T, MT2, MT2, MT2]*
       Pair[e[1], k[5]]*(2*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[2], ec[4]]*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]])) - 
      2*D0i[dd13, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       ((MH2 - T)*(S - T14)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
        4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 4*(MH2 - T)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]) + 16*Pair[e[1], k[3]]*
         ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
         Pair[ec[5], k[1]] + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
         ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
          2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
          (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])) - 
      2*D0i[dd23, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-((S - T14)*(-MH2 + S + T + T14)*Pair[e[1], ec[5]]*
          Pair[e[2], ec[4]]) + 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T24 + U)*
         Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]) - 16*Pair[e[1], k[5]]*
         ((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] - 2*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
         ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
          2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
          (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])) - 
      2*D0i[dd22, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       ((S34 - U)*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[5]]*
         Pair[e[2], ec[4]] + 8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T24 + U)*
         Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - 32*Pair[e[1], k[5]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] - 4*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
         (-2*(S34 - U)*Pair[ec[5], k[1]] - (MH2 - S34 - T24 - U)*
           (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      (8*D0i[dd001, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2] - 8*D0i[dd001, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
          MT2, MT2])*(4*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + 
        Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
        2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]))) - 4*D0i[dd00, MH2, T24, 0, 0, 
        MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[5]]*((S - 2*S34 - T14 + 2*U)*Pair[e[2], ec[4]] - 
          4*((Pair[e[2], k[1]] - 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))) + 
        8*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
        8*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
        2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      C0i[cc0, 0, T24, T, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
        4*Pair[ec[4], k[2]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[5], k[1]]) + 
        4*Pair[e[2], k[4]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[4]]*Pair[ec[5], k[1]]) - 
        2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[5], k[1]] + (2*Pair[e[1], k[3]] + 
            Pair[e[1], k[5]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) - 
      2*C0i[cc2, T24, 0, T, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])) - 2*Pair[e[1], k[3]]*
         (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]))) - C0i[cc0, T24, 0, T, MT2, MT2, MT2]*
       (4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         ((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
         (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] - 
            2*Pair[ec[5], k[2]]) + 2*Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
            2*Pair[ec[5], k[4]]))) - 4*D0i[dd23, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (2*(S + T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*(S + T14)*(Pair[e[1], k[2]] - 
          Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
        2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
        8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          Pair[ec[5], k[4]]) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*(-2*(S - T14)*Pair[ec[5], k[2]] + 
            (3*S - 5*T14)*Pair[ec[5], k[3]] - 4*T14*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*(4*S*Pair[ec[5], k[2]] + (-5*S + 3*T14)*
             Pair[ec[5], k[3]] + 2*(-S + T14)*Pair[ec[5], k[4]]))) - 
      D0i[dd2, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-3*S34*T - S34*T14 + 2*T14*T24 + 
            MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 3*T*U + 5*T14*U + 
            S*(-5*S34 - 2*T24 + U))*Pair[e[2], ec[4]] + 
          4*((2*(-MH2 + S34 + T24 + U)*Pair[e[2], k[1]] + 3*(-MH2 + S + T + 
                T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             (2*(-MH2 + S34 + T24 + U)*Pair[ec[4], k[1]] + 3*(-MH2 + S + T + 
                T14)*Pair[ec[4], k[3]]))) + 
        4*(((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
             Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
             Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
             Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
             Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
          2*(Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
            Pair[e[1], k[4]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*(-5*Pair[e[2], k[1]]*
                 Pair[ec[5], k[1]] + Pair[e[2], k[5]]*(5*Pair[ec[5], k[1]] + 
                  Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*(
                -5*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] + Pair[ec[4], k[5]]*
                 (5*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], 
                   k[4]]))))) - 2*Pair[e[2], ec[4]]*
         (-2*Pair[e[1], k[4]]*((MH2 - T - T14)*Pair[ec[5], k[1]] + 
            (-MH2 + S + T + T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*
           (-5*(S34 - U)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
            2*(-S + S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], k[2]]*
           ((-MH2 + S + T)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
             Pair[ec[5], k[4]]))) - 4*D0i[dd22, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (4*(S + T14 + T24)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
         (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
         Pair[ec[5], k[3]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]] - 16*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
         (-(Pair[e[1], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] + 
             (-5*S + 3*T14)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + Pair[e[1], k[2]]*
           (2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-3*S + 5*T14)*
             Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))) + 
      D0i[dd1, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-4*((-MH2 + T)*Pair[e[1], k[2]] + (S - S34 + T14 - U)*
           Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] - 4*((-MH2 + T)*Pair[e[1], k[2]] + 
          (S - S34 + T14 - U)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         ((-3*MH2*S34 + 2*S*S34 + 3*S34*T + 3*MH2*U - 3*T*U - 2*T14*U)*
           Pair[e[2], ec[4]] - 4*(((S34 + U)*Pair[e[2], k[1]] + 
              (-3*MH2 + S + 3*T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + (-3*MH2 + S + 
                3*T + T14)*Pair[ec[4], k[3]]))) + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
        8*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*(Pair[ec[4], k[2]]*(2*Pair[e[2], k[3]]*Pair[ec[5], 
                k[1]] - Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], 
                 k[3]]) + Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]])) + Pair[e[2], k[4]]*
             (2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - Pair[ec[4], k[5]]*(
                Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*(
                2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])))) + 
        2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*(S34*Pair[ec[5], k[2]] - 
            (S34 + T14)*Pair[ec[5], k[3]] - (MH2 + S34 - T)*
             Pair[ec[5], k[4]]) + Pair[e[1], k[3]]*
           ((-5*S34 + 2*T14 + 3*U)*Pair[ec[5], k[2]] + 5*(S34 - U)*
             Pair[ec[5], k[3]] + (2*S + 3*S34 - 5*U)*Pair[ec[5], k[4]]) + 
          2*Pair[e[1], k[4]]*((MH2 - T + U)*Pair[ec[5], k[2]] - 
            (S + U)*Pair[ec[5], k[3]] - U*Pair[ec[5], k[4]]))) - 
      2*D0i[dd11, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2 - T)*(S34 - U)*Pair[e[2], ec[4]] - 
          4*(MH2 - T)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 2*Pair[e[1], k[3]]*
         (2*(S34 + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          2*(S34 + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]]) - Pair[e[2], ec[4]]*
           ((-3*S34 + U)*Pair[ec[5], k[1]] - (S34 - 3*U)*Pair[ec[5], k[3]] + 
            2*(S34 + U)*Pair[ec[5], k[4]]))) + 
      4*D0i[dd11, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 2*(-MH2 + T)*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 8*Pair[e[1], k[3]]*
         ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
         Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
         (-((MH2 - T)*(Pair[e[1], k[2]] + Pair[e[1], k[4]])*
            Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + 2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
            (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))) + 
      8*(D0i[dd003, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
             Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
              Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
        D0i[dd002, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
            Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
            Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]]))) + 
        D0i[dd002, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         (4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[5]]*((S34 - U)*Pair[e[2], ec[4]] - 
            4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[3]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
           Pair[ec[5], k[1]] - 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[1]] + 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])))) + 
      4*(C0i[cc1, T24, 0, T, MT2, MT2, MT2]*
         (-2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             Pair[ec[4], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])) + 
        C0i[cc2, 0, T24, T, MT2, MT2, MT2]*(2*Pair[e[1], k[3]]*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[3]]*
           Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 
          2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]]) - Pair[e[2], ec[4]]*
           (2*Pair[e[1], k[2]]*Pair[ec[5], k[2]] - 2*Pair[e[1], k[4]]*
             Pair[ec[5], k[4]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])))) - D0i[dd0, MH2, T24, 0, 0, 
        MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2*S34 - S*S34 - S34*T - MH2*U + T*U + T14*U)*
           Pair[e[2], ec[4]] + 2*(((S34 + U)*Pair[e[2], k[1]] + 
              (-2*MH2 + S + 2*T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + (-2*MH2 + S + 
                2*T + T14)*Pair[ec[4], k[3]]))) + 
        2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((MH2 + S34 - T + U)*
             Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*
             Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 
          2*(Pair[e[1], k[3]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
                Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
              Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*((S + U)*Pair[ec[5], k[1]] + (-MH2 + S + T)*Pair[
                ec[5], k[2]] - S*Pair[ec[5], k[4]]) - Pair[e[1], k[3]]*
             ((S34 - U)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + 
              S*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
             ((S34 + T14)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + 
              (MH2 - T - T14)*Pair[ec[5], k[4]])))) + 
      2*D0i[dd12, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*(-((S34 - U)*(S34 - T + T24 + U)*
            Pair[e[2], ec[4]]) + 4*(2*MH2 - S - 2*T - T14)*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])) + 
        2*(2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
            (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
            (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(3*Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[5]]*((3*S34 - U)*Pair[ec[5], k[2]] - 
              4*(S34 - U)*Pair[ec[5], k[3]] - (S34 - 3*U)*Pair[ec[5], 
                k[4]]) + 2*Pair[e[1], k[3]]*((MH2 - 3*S34 - T24 + U)*Pair[
                ec[5], k[2]] + 2*(S34 - U)*Pair[ec[5], k[3]] + 
              (MH2 + S34 - T24 - 3*U)*Pair[ec[5], k[4]])))) - 
      8*D0i[dd123, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[2], ec[4]]*((-S - S34 + T14 + U)*Pair[e[1], k[3]]*
           Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           ((S34 - U)*Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]])) + 
        4*(Pair[e[1], k[2]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
           ((-2*Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))*
           Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*
             (2*Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*(
                -4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + Pair[ec[5], 
                 k[4]])) + Pair[e[2], k[4]]*(2*Pair[ec[4], k[5]]*Pair[ec[5], 
                k[1]] + Pair[ec[4], k[1]]*(-4*Pair[ec[5], k[1]] - 
                Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))))))/(MW*SW) - 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu5, Glu2, Glu4, 0, 0]]*
   (Den[T14, 0]*(-4*D0i[dd12, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 
        0, T14, MT2, MT2, MT2, MT2]*(-2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
         ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
           Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) - 
        2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
        8*Pair[e[2], k[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] - 
      2*C0i[cc1, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((-S34 + T)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[3]])) - 2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + 
            Pair[e[2], k[4]]) - 2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[1]]))*Pair[ec[5], k[2]]) - 
      D0i[dd1, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (Pair[e[2], ec[5]]*((S - T24)*(MH2 - U)*Pair[e[1], ec[4]] - 
          4*(MH2 - U)*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]])) + 2*Pair[e[2], k[3]]*
         ((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      D0i[dd1, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(Pair[e[2], ec[5]]*(-((S - T24)*(-MH2 + S + T24 + U)*
            Pair[e[1], ec[4]]) - 4*(MH2 - S - T24 - U)*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])) + 2*(-2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*(2*T24*Pair[e[2], k[1]] + 
            2*S*Pair[e[2], k[4]] + (S - T24)*Pair[e[2], k[5]]) - 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          4*Pair[e[2], k[3]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      8*(D0i[dd112, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        D0i[dd112, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        (D0i[dd113, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd113, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*
         (-((S - T24)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]]) - 
      8*(D0i[dd333, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
         MT2, MT2, MT2, MT2] + D0i[dd333, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
         T14, MT2, MT2, MT2, MT2])*(-((S - T24)*Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])) + 
        4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
      4*D0i[dd23, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
        MT2, MT2, MT2, MT2]*(2*(-MH2 + S + T24 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
         ((-MH2 + S + T24 + U)*Pair[e[2], k[1]] + (-MH2 + S + T24 + U)*
           Pair[e[2], k[4]] + 2*(-S + T24)*Pair[e[2], k[5]]) + 
        2*(-MH2 + S + T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
        8*Pair[e[2], k[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           (Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
      8*(D0i[dd222, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*((-S34 + T)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((-S - 2*S34 + 2*T + T24)*
             Pair[e[2], k[3]] + (-S34 + T)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
                Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]) - 
      8*(D0i[dd233, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*((-S + S34 - T + T24)*
             Pair[e[2], k[3]] + (-2*S + S34 - T + 2*T24)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[5]] + Pair[e[2], k[5]]*(Pair[ec[4], k[3]] + 
                2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]] + 
        D0i[dd233, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((-2*S - S34 + T + 2*T24)*
             Pair[e[2], k[3]] + (-S - S34 + T + T24)*Pair[e[2], k[5]]) + 
          4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
             Pair[ec[4], k[1]] + Pair[e[1], k[3]]*(3*Pair[e[2], k[3]] + 
              2*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
              Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
         Pair[ec[5], k[3]]) + 4*D0i[dd12, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
        T14, MT2, MT2, MT2, MT2]*
       (2*(MH2 - U)*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]])*Pair[ec[5], k[2]] + 
        2*Pair[e[2], k[3]]*((S + T24)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
          (S + T24)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] - 
          4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
        Pair[e[1], ec[4]]*(-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            Pair[ec[5], k[2]]) - Pair[e[2], k[3]]*
           (2*(S + T24)*Pair[ec[5], k[1]] - (S - 3*T24)*Pair[ec[5], k[2]] + 
            (-3*S + T24)*Pair[ec[5], k[3]]))) - 
      4*D0i[dd23, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (2*Pair[e[1], k[4]]*((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
        2*Pair[e[1], ec[5]]*((-7*MH2 + 3*(S34 + T + U))*Pair[e[2], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] - 
        2*(MH2 + S + T24 - U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
         Pair[ec[5], k[3]] - 2*(MH2 + S + T24 - U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
        8*(2*Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + Pair[e[2], k[5]])*
           Pair[ec[4], k[1]] + Pair[e[1], k[5]]*(3*Pair[e[2], k[3]] + 
            Pair[e[2], k[5]])*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           (Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
            Pair[e[2], k[3]]*(4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*(-2*(MH2 + S + T24 - U)*
           Pair[e[2], k[1]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
           (2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
            (MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
            (2*MH2 - 12*S - 4*S34 - 5*T14 - U)*Pair[ec[5], k[3]]) - 
          Pair[e[2], k[5]]*(2*(MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
            (MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
            (2*MH2 - 6*S - 4*S34 - 3*T14 - 2*T24 - U)*Pair[ec[5], k[3]]))) - 
      2*D0i[dd2, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (-2*Pair[e[1], ec[5]]*((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
          (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], k[1]] + 
        2*Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*Pair[e[1], k[2]]*
           Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*Pair[ec[4], k[2]] + 
            (S + T24)*Pair[ec[4], k[3]])) - 2*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
          (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], k[4]]*(((2*S + T14 + 2*T24 + U)*Pair[e[2], k[3]] + 
            (-MH2 + U)*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]])) - 
        Pair[e[1], ec[4]]*((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*
           Pair[e[2], ec[5]] + 2*Pair[e[2], k[1]]*((MH2 + S + T14 + T24)*
             Pair[ec[5], k[2]] - (2*MH2 + S + T24 - 2*U)*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[5]]*(2*(MH2 - U)*Pair[ec[5], k[1]] - 
            2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - U)*
             Pair[ec[5], k[3]]) - Pair[e[2], k[3]]*
           (2*(2*S + T14 + 2*T24 + U)*Pair[ec[5], k[1]] + 
            (3*MH2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[2]] + 
            2*(-2*MH2 - 3*S + T + T24 + U)*Pair[ec[5], k[3]])) + 
        4*(-(Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[2]]) + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 3*Pair[e[1], k[2]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
          Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[3]] + 3*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
           Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
           Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[3]]*
           Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[5]]*
           Pair[e[2], k[3]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*Pair[ec[4], k[1]]*
           (-(Pair[e[2], k[5]]*Pair[ec[5], k[2]]) + 2*Pair[e[2], k[3]]*
             Pair[ec[5], k[3]]))) + D0i[dd0, 0, U, 0, 2*MH2 - S34 - T - U, 
        MH2, T14, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[4]]*
         ((-2*MH2*(S - T24) + S*(S34 + U) - T24*(T + U))*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[3]]*((S + T24)*Pair[ec[5], k[1]] + T24*Pair[ec[5], 
                k[2]] - S*Pair[ec[5], k[3]]) + Pair[e[2], k[4]]*
             ((MH2 - U)*Pair[ec[5], k[1]] + (-MH2 + T)*Pair[ec[5], k[2]] + 
              S*Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             ((-MH2 + U)*Pair[ec[5], k[1]] + (-2*MH2 + S34 + U)*Pair[ec[5], 
                k[2]] + (MH2 + T24 - U)*Pair[ec[5], k[3]]))) + 
        2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] - Pair[e[2], ec[5]]*((-4*MH2 + S34 + T + 2*U)*
             Pair[e[1], k[2]]*Pair[ec[4], k[1]] + (S + T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((-4*MH2 + S34 + T + 2*U)*
               Pair[ec[4], k[2]] + (S + T24)*Pair[ec[4], k[3]])) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (MH2 + S + T24 - U)*Pair[ec[5], k[3]]) - 
          2*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] - Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
            Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
              (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
              (MH2 + S + T24 - U)*Pair[ec[5], k[3]])))) - 
      8*(D0i[dd122, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
         ((-S + S34 - T + T24)*Pair[e[1], ec[4]] + 
          4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[5]]))*Pair[ec[5], k[2]] + 
        D0i[dd123, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(4*(Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[5]]*
             (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
          Pair[e[1], ec[4]]*((-S + S34 - T + T24)*Pair[e[2], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[2], k[5]]*((-2*S + S34 - T + 2*T24)*
               Pair[ec[5], k[2]] + (-S + T24)*Pair[ec[5], k[3]]))) + 
        D0i[dd123, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*(4*(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[3]]*(2*Pair[e[2], k[3]] + 
              Pair[e[2], k[5]])*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[4]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[5]]*Pair[ec[5], k[2]] + 
            2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 2*Pair[e[1], k[4]]*
             Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
           (Pair[e[2], k[5]]*((-S34 + T)*Pair[ec[5], k[2]] + (-S + T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[3]]*((-S - S34 + T + T24)*Pair[
                ec[5], k[2]] + 2*(-S + T24)*Pair[ec[5], k[3]]))) + 
        D0i[dd122, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, 
          MT2]*Pair[e[2], k[3]]*(Pair[e[1], ec[4]]*
           ((-S34 + T)*Pair[ec[5], k[2]] + (-S + T24)*Pair[ec[5], k[3]]) + 
          4*(Pair[e[1], k[3]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + 
            Pair[e[1], k[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 
            Pair[e[1], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
              Pair[ec[4], k[2]]*Pair[ec[5], k[3]])))) - 
      D0i[dd3, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[4]]*((5*MH2*(S - T24) - S*(3*S34 + T + 3*U) + 
            T24*(S34 + 3*(T + U)))*Pair[e[2], ec[5]] - 
          2*(Pair[e[2], k[1]]*(2*(-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + 
              2*(MH2 - S34 - U)*Pair[ec[5], k[2]] + (2*MH2 + S - 2*T - 7*T24)*
               Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*(-2*(S + T24)*Pair[
                ec[5], k[1]] - 2*T24*Pair[ec[5], k[2]] + (S + T24)*Pair[
                ec[5], k[3]]) + Pair[e[2], k[4]]*((4*MH2 - 2*(S34 + T))*Pair[
                ec[5], k[1]] + (6*MH2 - 2*(S34 + 2*T + U))*Pair[ec[5], 
                k[2]] + (-2*MH2 - 7*S + 2*T + T24)*Pair[ec[5], k[3]]))) + 
        4*(Pair[e[1], ec[5]]*((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - 
            (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*
           ((-5*MH2 + 2*S34 + 2*T + 3*U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             ((-5*MH2 + 2*S34 + 2*T + 3*U)*Pair[ec[4], k[2]] + 
              (S + T24)*Pair[ec[4], k[3]])) + Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] - 
            3*(S + T24)*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (((-2*MH2 + S34 + T)*Pair[e[2], k[1]] - (S + T24)*Pair[e[2], 
                k[3]] + (2*MH2 - S34 - T)*Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*(S + T14 + T24)*Pair[
                ec[5], k[2]] - 3*(S + T24)*Pair[ec[5], k[3]])) + 
          2*(-(Pair[e[1], k[3]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
              Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], k[2]]*Pair[ec[4], k[1]]*(Pair[e[2], k[3]]*Pair[ec[5], 
                k[2]] + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[5], 
                k[3]]) + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
               Pair[ec[5], k[2]] - (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(
                Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) - 
                4*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]))))) + 
      8*(D0i[dd00, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*
           ((S - T24)*Pair[e[1], ec[4]] - 4*(Pair[e[1], k[2]]*Pair[ec[4], 
                k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
          (Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
            2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[1]]))*Pair[ec[5], k[2]]) + 
        D0i[dd00, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
            Pair[e[1], k[4]]*((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] - 
                2*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
           ((S - T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
             (2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
             (Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]])))) - 
      8*(D0i[dd133, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
         MT2, MT2, MT2, MT2] + D0i[dd133, 0, U, 0, 2*MH2 - S34 - T - U, MH2, 
         T14, MT2, MT2, MT2, MT2])*(-((S - T24)*Pair[e[1], ec[4]]*
          (Pair[e[2], k[1]] - Pair[e[2], k[4]])*(Pair[ec[5], k[2]] + 
           Pair[ec[5], k[3]])) + 4*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
          Pair[ec[5], k[4]])) + 2*D0i[dd2, 0, MH2 - S - T24 - U, MH2, 
        2*MH2 - S34 - T - U, 0, T14, MT2, MT2, MT2, MT2]*Pair[e[2], k[5]]*
       (2*(-MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + 
        2*(-MH2 + S34 + T + U)*Pair[e[1], ec[5]]*Pair[ec[4], k[1]] + 
        4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
           Pair[ec[4], k[5]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
         ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (S - S34 + T - T24)*
           Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], k[4]])) + 
      8*C0i[cc2, U, 0, T14, MT2, MT2, MT2]*
       (Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
          Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - 
        Pair[e[1], ec[4]]*(Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 
          Pair[e[2], k[4]]*Pair[ec[5], k[4]])) - 
      4*D0i[dd22, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
       (2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[3]]*
         Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*Pair[e[1], ec[5]]*
         Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 2*(-MH2 + U)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 2*(-MH2 + U)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 8*Pair[e[2], k[3]]*
         (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[3]]*
           Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
            Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
         (-((MH2 - U)*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + 2*(S + S34 - T - T24)*Pair[ec[5], k[3]] + 
            (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))) + 
      D0i[dd3, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(-((MH2*(S - T24) - S*(S34 - T + U) + 
           T24*(-S34 + T + U))*Pair[e[1], ec[4]]*Pair[e[2], ec[5]]) + 
        4*Pair[e[1], k[4]]*((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
           Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
         ((-2*MH2 + S34 + T + 2*U)*Pair[e[2], k[1]] + 
          (S + T24)*Pair[e[2], k[3]] + (2*MH2 - S34 - T - 2*U)*
           Pair[e[2], k[4]])*Pair[ec[4], k[1]] - 4*Pair[e[2], ec[5]]*
         ((-MH2 + U)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
          (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
          Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[4], k[2]] + 
            (S + T24)*Pair[ec[4], k[3]])) - 
        8*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[2]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*Pair[ec[5], k[2]] - (Pair[e[2], k[1]] - 
              Pair[e[2], k[4]])*Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]])) - Pair[e[1], k[3]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], k[1]]*(Pair[ec[5], k[2]] - 
            Pair[ec[5], k[3]])) + 4*(S + T24)*Pair[e[1], k[4]]*
         Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 4*(S + T24)*Pair[e[1], e[2]]*
         Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 2*Pair[e[1], ec[4]]*
         (2*(S + T24)*Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[5]]*(2*(-MH2 + T + U)*Pair[ec[5], k[1]] + 
            (S + T24)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + U)*
             Pair[ec[5], k[4]]) + 2*Pair[e[2], k[3]]*
           ((-MH2 + S + T + U)*Pair[ec[5], k[1]] + T24*Pair[ec[5], k[3]] + 
            (-MH2 + S34 + T24 + U)*Pair[ec[5], k[4]]))) + 
      4*(C0i[cc2, MH2 - S - T24 - U, MH2, T14, MT2, MT2, MT2]*
         (-(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]])*
            (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
          2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*
             Pair[ec[4], k[1]])*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
        C0i[cc1, U, 0, T14, MT2, MT2, MT2]*(2*Pair[ec[4], k[1]]*
           (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 2*Pair[e[1], k[4]]*
           (Pair[e[2], k[3]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*
             (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) - Pair[e[1], ec[4]]*
           (2*Pair[e[2], k[1]]*Pair[ec[5], k[1]] - 2*Pair[e[2], k[4]]*
             Pair[ec[5], k[4]] - Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]])))) - C0i[cc0, U, 0, T14, MT2, MT2, MT2]*
       (4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
              (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
          Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 
            Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*((2*S - S34 + T - 2*T24)*Pair[e[2], ec[5]] - 
          2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*(3*Pair[ec[5], k[2]] - 
              2*Pair[ec[5], k[3]]) + Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]])))) + C0i[cc0, MH2 - S - T24 - U, MH2, T14, 
        MT2, MT2, MT2]*
       (-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[4]]*Pair[ec[4], k[5]])) + Pair[ec[4], k[1]]*
           (Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]) + 
            Pair[e[1], e[2]]*(3*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(3*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + S34 - T + T24)*Pair[e[2], ec[5]] + 
          2*(-2*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
            Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
            2*Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))) + 
      4*(D0i[dd13, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[2]] + 
        D0i[dd13, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (2*(S + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], ec[5]] + 2*(S + T24)*Pair[e[1], ec[5]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 
          2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*Pair[ec[5], k[2]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]] - 
            Pair[ec[5], k[4]]) + Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]]*(-2*(S - T24)*Pair[ec[5], k[1]] + 
              (3*S - 5*T24)*Pair[ec[5], k[3]] - 4*T24*Pair[ec[5], k[4]]) + 
            Pair[e[2], k[4]]*(4*S*Pair[ec[5], k[1]] + (-5*S + 3*T24)*Pair[
                ec[5], k[3]] + 2*(-S + T24)*Pair[ec[5], k[4]])))) + 
      4*(D0i[dd33, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(2*(S + T24)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           (-((S - 3*T24)*Pair[e[2], k[1]]) + (3*S - T24)*Pair[e[2], k[4]]) + 
          2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]] - 
          8*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
        D0i[dd33, 0, U, 0, 2*MH2 - S34 - T - U, MH2, T14, MT2, MT2, MT2, MT2]*
         (4*(S + T14 + T24)*Pair[e[1], k[4]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
           Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + 2*(S + T24)*Pair[e[1], k[4]]*Pair[e[2], ec[4]]*
           Pair[ec[5], k[3]] + 2*(S + T24)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
           Pair[ec[5], k[3]] - 16*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
           (Pair[e[1], k[2]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
           (-(Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] + 
               (-5*S + 3*T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
                Pair[ec[5], k[4]])) + Pair[e[2], k[1]]*
             (2*(S + T14 + T24)*Pair[ec[5], k[1]] + (-3*S + 5*T24)*Pair[
                ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]])))) + 
      D0i[dd0, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, MT2, 
        MT2, MT2, MT2]*(2*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
            (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
           Pair[ec[4], k[1]] + Pair[e[2], ec[5]]*((S34 + T)*Pair[e[1], k[2]]*
             Pair[ec[4], k[1]] - (S + T24)*Pair[e[1], k[3]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*((S34 + T)*Pair[ec[4], 
                k[2]] - (S + T24)*Pair[ec[4], k[3]])) - Pair[e[1], e[2]]*
           Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
            (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]*
              Pair[ec[5], k[2]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[2]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] - 
            Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
             Pair[ec[5], k[2]] + Pair[e[1], k[2]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             Pair[e[2], k[5]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[
                e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], 
              ec[5]] - Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], 
                k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]))) + 
        Pair[e[1], ec[4]]*((-(S*T) + S34*T24)*Pair[e[2], ec[5]] + 
          2*(Pair[e[2], k[4]]*((-MH2 + S + U)*Pair[ec[5], k[1]] + 
              (-MH2 + S + S34 + U)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[4]]) - 
            Pair[e[2], k[3]]*(T24*Pair[ec[5], k[1]] + (-S + T24)*Pair[ec[5], 
                k[2]] + S*Pair[ec[5], k[4]]) + Pair[e[2], k[1]]*
             (T24*Pair[ec[5], k[1]] + (-MH2 + T + T24 + U)*Pair[ec[5], 
                k[2]] + (MH2 - T24 - U)*Pair[ec[5], k[4]])))) - 
      8*((D0i[dd001, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd001, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*(Pair[e[2], ec[5]]*
           ((-S + T24)*Pair[e[1], ec[4]] + 4*(Pair[e[1], k[2]]*Pair[ec[4], 
                k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]])) - 
          2*(Pair[e[1], ec[4]]*(Pair[e[2], k[1]] + Pair[e[2], k[4]]) - 
            2*(Pair[e[1], k[4]]*Pair[e[2], ec[4]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[1]]))*Pair[ec[5], k[2]]) + 
        (D0i[dd003, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
           MT2, MT2, MT2, MT2] + D0i[dd003, 0, U, 0, 2*MH2 - S34 - T - U, 
           MH2, T14, MT2, MT2, MT2, MT2])*
         (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
              Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
             ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*
           ((-S + T24)*Pair[e[2], ec[5]] - 2*(Pair[e[2], k[1]]*(
                2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + Pair[e[2], k[4]]*(
                Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]])))) + 
        D0i[dd002, 0, MH2 - S - T24 - U, MH2, 2*MH2 - S34 - T - U, 0, T14, 
          MT2, MT2, MT2, MT2]*(Pair[e[2], ec[5]]*((-S + S34 - T + T24)*
             Pair[e[1], ec[4]] + 4*(Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[5]])) - 2*Pair[e[2], k[5]]*
           (-2*(Pair[e[1], k[4]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*Pair[
                ec[4], k[1]]) + Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[4]]))) + D0i[dd002, 0, U, 0, 2*MH2 - S34 - T - U, 
          MH2, T14, MT2, MT2, MT2, MT2]*
         (4*(Pair[e[2], ec[5]]*(Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
              Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[ec[4], k[1]]*
             (Pair[e[1], ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[3]]) + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[
                ec[4], ec[5]] + Pair[e[2], ec[4]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*((-S34 + T)*Pair[e[2], ec[5]] - 
            2*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[3]] + 
              Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]])))))) - 
    Den[T24, 0]*((8*D0i[dd133, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, 
          MT2, MT2, MT2] - 8*D0i[dd133, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2])*Pair[e[1], k[3]]*
       ((-S + T14)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] - 
      2*D0i[dd3, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-(((-MH2 + T)*Pair[e[1], k[2]] + (S - T14)*Pair[e[1], k[3]] + 
           (-MH2 + T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]) - 
        2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]) + 4*Pair[e[1], k[3]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] - 
      8*D0i[dd003, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
           Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      D0i[dd3, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2 - T)*(S - T14)*Pair[e[2], ec[4]] - 
          4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 2*Pair[e[1], k[3]]*
         ((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      8*(D0i[dd233, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         Pair[e[1], k[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]] + 
        D0i[dd233, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[4]]) + 4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
      (8*D0i[dd111, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2] - 8*D0i[dd111, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
          MT2, MT2])*Pair[e[1], k[3]]*((-S34 + U)*Pair[e[2], ec[4]] + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + 
      4*D0i[dd13, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (-2*(MH2 - T)*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
        Pair[e[1], k[3]]*(-2*(S + T14)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
          2*(S + T14)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) - 
        Pair[e[2], ec[4]]*(2*(-MH2 + T)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          (MH2 - T)*Pair[e[1], k[5]]*Pair[ec[5], k[1]] + 
          Pair[e[1], k[3]]*((MH2 + S - T - 3*T14)*Pair[ec[5], k[1]] - 
            2*(S + T14)*Pair[ec[5], k[2]] + (3*S - T14)*Pair[ec[5], 
              k[3]]))) + 4*D0i[dd12, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, 
        MT2, MT2, MT2, MT2]*(2*((-7*MH2 + 3*(S34 + T + U))*Pair[e[1], k[3]] + 
          (-3*MH2 + S34 + T + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*((-7*MH2 + 3*(S34 + T + U))*
           Pair[e[1], k[3]] + (-3*MH2 + S34 + T + U)*Pair[e[1], k[5]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 2*(MH2 + S - T + T14)*
         Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
        2*(MH2 + S - T + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]] + 8*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*
             Pair[ec[4], k[2]] + Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
          Pair[e[1], k[3]]*(4*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            3*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             (4*Pair[ec[4], k[3]] + 3*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] + Pair[e[2], ec[4]]*(2*(MH2 + S - T + T14)*
           Pair[e[1], k[2]]*Pair[ec[5], k[3]] + Pair[e[1], k[3]]*
           ((MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
            2*(MH2 + 3*(S + T14 + T24))*Pair[ec[5], k[2]] + 
            (2*MH2 - 12*S - 4*S34 - T - 5*T24)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[5]]*((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
            2*(MH2 + S + T14 + T24)*Pair[ec[5], k[2]] + 
            (2*MH2 - 6*S - 4*S34 - T - 2*T14 - 3*T24)*Pair[ec[5], k[3]]))) - 
      8*D0i[dd113, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       Pair[e[1], k[3]]*(Pair[e[2], ec[4]]*((-S34 + U)*Pair[ec[5], k[1]] + 
          (-S + T14)*Pair[ec[5], k[3]]) + 
        4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
            Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
      D0i[dd2, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       ((5*MH2*(S - T14) - S*(3*S34 + 3*T + U) + T14*(S34 + 3*(T + U)))*
         Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + 
        4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - (S + T14)*Pair[e[1], k[3]] + 
          (2*MH2 - S34 - U)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 4*((-2*MH2 + S34 + U)*Pair[e[1], k[2]] - 
          (S + T14)*Pair[e[1], k[3]] + (2*MH2 - S34 - U)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 4*Pair[e[1], ec[5]]*
         (((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-5*MH2 + 2*S34 + 3*T + 2*U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]])) + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          3*(S + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
          3*(S + T14)*Pair[ec[5], k[3]]) + 2*Pair[e[2], ec[4]]*
         (Pair[e[1], k[3]]*(2*T14*Pair[ec[5], k[1]] + (S + T14)*
             (2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[2]]*(2*(-MH2 + S34 + T)*Pair[ec[5], k[1]] + 
            (4*MH2 - 2*(S34 + U))*Pair[ec[5], k[2]] - 
            (2*MH2 + S - 7*T14 - 2*U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(2*(-3*MH2 + S34 + T + 2*U)*Pair[ec[5], k[1]] + 
            2*(-2*MH2 + S34 + U)*Pair[ec[5], k[2]] + 
            (2*MH2 + 7*S - T14 - 2*U)*Pair[ec[5], k[3]])) + 
        8*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
          (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
             Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 
            4*Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - 
                Pair[ec[5], k[3]]) - 4*Pair[ec[4], k[1]]*Pair[ec[5], 
                k[3]])))) + 2*D0i[dd1, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
        T, MT2, MT2, MT2, MT2]*((-(S*(S34 + T)) + 2*MH2*(S - T14) + 
          T14*(T + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
        2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
          (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
        2*((2*S + T + 2*T14 + T24)*Pair[e[1], k[3]] + 
          (-MH2 + T)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        2*Pair[e[1], ec[5]]*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + 
            (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + 
            (S + T14)*Pair[ec[4], k[3]])) - 2*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
          (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
        2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
           Pair[ec[5], k[1]] + (2*MH2 + S - 2*T + T14)*Pair[ec[5], k[3]]) - 
        Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*(-2*(MH2 - U)*Pair[ec[5], k[1]] + 
            2*(MH2 - T)*Pair[ec[5], k[2]] + (MH2 + 2*S - T)*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*
           ((MH2 + S + T14 + T24)*Pair[ec[5], k[1]] - (2*MH2 + S - 2*T + T14)*
             Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*
           ((3*MH2 + S + 3*T14 + T24 - 2*U)*Pair[ec[5], k[1]] + 
            2*(2*S + T + 2*T14 + T24)*Pair[ec[5], k[2]] + 
            2*(-2*MH2 - 3*S + T + T14 + U)*Pair[ec[5], k[3]])) + 
        4*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*((3*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]])*
             Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[5]]*
             Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            Pair[e[2], k[4]]*((3*Pair[ec[4], k[1]] + 2*Pair[ec[4], k[3]])*
               Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*(Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]]))))) + 
      8*(D0i[dd122, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(((-2*S - S34 + 2*T14 + U)*Pair[e[1], k[3]] + 
            (-S - S34 + T14 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
          4*(Pair[e[1], k[5]]*(2*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(
                2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
            Pair[e[1], k[3]]*(3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              2*Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(
                3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]))))*
         Pair[ec[5], k[3]] + D0i[dd122, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2]*((S34 - U)*Pair[e[2], ec[4]]*
           (Pair[e[1], k[3]]*Pair[ec[5], k[1]] - Pair[e[1], k[5]]*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(2*Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]))) + D0i[dd123, MH2, 0, T24, 0, 
          2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
         (Pair[e[2], ec[4]]*(Pair[e[1], k[5]]*((-S34 + U)*Pair[ec[5], k[1]] + 
              (-S + T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
             ((-S - S34 + T14 + U)*Pair[ec[5], k[1]] + 2*(-S + T14)*Pair[
                ec[5], k[3]])) + 4*(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[
                ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])*
             (Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + 
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*
                 Pair[ec[5], k[1]] + Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]]))))) - D0i[dd0, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
        T, MT2, MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
         ((S*(S34 + T) - 2*MH2*(S - T14) - T14*(T + U))*Pair[e[2], ec[4]] - 
          2*(((-4*MH2 + S34 + 2*T + U)*Pair[e[2], k[1]] + (S + T14)*Pair[
                e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             ((-4*MH2 + S34 + 2*T + U)*Pair[ec[4], k[1]] + (S + T14)*Pair[
                ec[4], k[3]]))) + 2*(((-MH2 + T)*Pair[e[1], k[2]] + 
            (S + T14)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[1]] + (MH2 + S - T + T14)*Pair[ec[5], k[3]]) - 
          Pair[e[2], ec[4]]*(Pair[e[1], k[3]]*(T14*Pair[ec[5], k[1]] + 
              (S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]) + 
            Pair[e[1], k[4]]*((-MH2 + U)*Pair[ec[5], k[1]] + (MH2 - T)*Pair[
                ec[5], k[2]] + S*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
             ((-2*MH2 + S34 + T)*Pair[ec[5], k[1]] + (-MH2 + T)*Pair[ec[5], 
                k[2]] + (MH2 - T + T14)*Pair[ec[5], k[3]])) - 
          2*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
                Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[
                ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                  Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[1]] + 2*Pair[e[2], k[1]]*Pair[ec[4], 
                k[2]]*Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*
                 Pair[ec[5], k[1]] + 2*Pair[ec[4], k[1]]*Pair[ec[5], 
                  k[3]]))))) + 
      8*(D0i[dd112, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(((-S - 2*S34 + T14 + 2*U)*Pair[e[1], k[3]] + 
            (-S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[4]] + 
          4*(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
             (3*Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*(3*Pair[ec[4], k[3]] + 
                Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]] + 
        D0i[dd113, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*Pair[e[1], k[3]]*(Pair[e[2], ec[4]]*
           ((-S34 + U)*Pair[ec[5], k[1]] + (-S + T14)*Pair[ec[5], k[3]]) + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + 
        D0i[dd112, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         ((S34 - U)*Pair[e[2], ec[4]]*(-(Pair[e[1], k[5]]*Pair[ec[5], k[
                3]]) + Pair[e[1], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[
                3]])) + 4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[4], k[3]])*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[5], k[3]] + 
            Pair[e[1], k[3]]*(Pair[ec[5], k[2]] - 3*Pair[ec[5], k[3]] - 
              Pair[ec[5], k[4]])))) + 8*D0i[dd00, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 2*((Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[3]])) + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
            2*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
         (Pair[e[1], k[4]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[5], k[3]] - 2*Pair[ec[5], k[4]]))) + 
      8*(D0i[dd222, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         Pair[e[1], k[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
          4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
         Pair[ec[5], k[1]] + D0i[dd223, MH2, T24, 0, 0, MH2 - S - T - T14, T, 
          MT2, MT2, MT2, MT2]*Pair[e[1], k[5]]*
         ((-S - S34 + T14 + U)*Pair[e[2], ec[4]] + 
          4*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]])))*
         Pair[ec[5], k[1]] + D0i[dd222, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, 
          T, MT2, MT2, MT2, MT2]*(-((S - T14)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]]))*Pair[ec[5], k[3]] + 
        D0i[dd223, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(-((S - T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
            Pair[ec[5], k[4]]))) - 4*C0i[cc1, 0, T24, T, MT2, MT2, MT2]*
       Pair[e[1], k[5]]*(2*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        2*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - Pair[e[2], ec[4]]*
         (Pair[ec[5], k[2]] + Pair[ec[5], k[4]])) - 
      2*D0i[dd13, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       ((MH2 - T)*(S - T14)*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
        4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] - 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 4*(MH2 - T)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]) + 16*Pair[e[1], k[3]]*
         ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
         Pair[ec[5], k[1]] + 2*Pair[e[1], k[3]]*Pair[e[2], ec[4]]*
         ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
          2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
          (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])) - 
      2*D0i[dd23, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-((S - T14)*(-MH2 + S + T + T14)*Pair[e[1], ec[5]]*
          Pair[e[2], ec[4]]) + 4*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(-MH2 + S34 + T24 + U)*
         Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]]) - 16*Pair[e[1], k[5]]*
         ((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] - 2*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
         ((-3*MH2 + 3*S + 4*S34 + T - T14 + 2*T24)*Pair[ec[5], k[2]] - 
          2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
          (-3*MH2 - S + T + 3*T14 + 2*T24 + 4*U)*Pair[ec[5], k[4]])) - 
      2*D0i[dd22, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       ((S34 - U)*(-MH2 + S34 + T24 + U)*Pair[e[1], ec[5]]*
         Pair[e[2], ec[4]] + 8*(-MH2 + S34 + T24 + U)*Pair[e[1], k[5]]*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 8*(-MH2 + S34 + T24 + U)*
         Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
        4*(MH2 - S - T - T14)*Pair[e[1], ec[5]]*
         (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
           Pair[ec[4], k[3]]) - 32*Pair[e[1], k[5]]*
         ((Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]]))*
         Pair[ec[5], k[1]] - 4*Pair[e[1], k[5]]*Pair[e[2], ec[4]]*
         (-2*(S34 - U)*Pair[ec[5], k[1]] - (MH2 - S34 - T24 - U)*
           (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      (8*D0i[dd001, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2] - 8*D0i[dd001, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, 
          MT2, MT2])*(4*Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
           Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*Pair[ec[4], k[2]]) + 
        Pair[e[1], ec[5]]*((-S34 + U)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])) + 4*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - 
        2*Pair[e[2], ec[4]]*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
           Pair[ec[5], k[3]] + Pair[e[1], k[3]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]))) - 4*D0i[dd00, MH2, T24, 0, 0, 
        MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
        4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        Pair[e[1], ec[5]]*((S - 2*S34 - T14 + 2*U)*Pair[e[2], ec[4]] - 
          4*((Pair[e[2], k[1]] - 2*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 2*Pair[ec[4], k[3]]))) + 
        8*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
        8*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
        2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          2*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           (Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) + 
      C0i[cc0, 0, T24, T, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
          4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
        4*Pair[ec[4], k[2]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
           Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[5], k[1]]) + 
        4*Pair[e[2], k[4]]*((2*Pair[e[1], k[3]] + Pair[e[1], k[5]])*
           Pair[ec[4], ec[5]] + Pair[e[1], ec[4]]*Pair[ec[5], k[1]]) - 
        2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
          Pair[e[1], k[4]]*Pair[ec[5], k[1]] + (2*Pair[e[1], k[3]] + 
            Pair[e[1], k[5]])*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) - 
      2*C0i[cc2, T24, 0, T, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((S - S34 - T14 + U)*Pair[e[2], ec[4]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[5]])) - 2*Pair[e[1], k[3]]*
         (-2*(Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]))) - C0i[cc0, T24, 0, T, MT2, MT2, MT2]*
       (4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 4*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         ((-S + T14)*Pair[e[2], ec[4]] + 
          4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[4], k[2]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
         (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
        2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] - 
            2*Pair[ec[5], k[2]]) + 2*Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
            Pair[ec[5], k[4]]) + Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
            2*Pair[ec[5], k[4]]))) - 4*D0i[dd23, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (2*(S + T14)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*(S + T14)*(Pair[e[1], k[2]] - 
          Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
        2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
        2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
        8*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 
          Pair[ec[5], k[4]]) + Pair[e[2], ec[4]]*
         (Pair[e[1], k[2]]*(-2*(S - T14)*Pair[ec[5], k[2]] + 
            (3*S - 5*T14)*Pair[ec[5], k[3]] - 4*T14*Pair[ec[5], k[4]]) + 
          Pair[e[1], k[4]]*(4*S*Pair[ec[5], k[2]] + (-5*S + 3*T14)*
             Pair[ec[5], k[3]] + 2*(-S + T14)*Pair[ec[5], k[4]]))) - 
      D0i[dd2, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((-3*S34*T - S34*T14 + 2*T14*T24 + 
            MH2*(2*S + 3*S34 - 2*T14 - 3*U) + 3*T*U + 5*T14*U + 
            S*(-5*S34 - 2*T24 + U))*Pair[e[2], ec[4]] + 
          4*((2*(-MH2 + S34 + T24 + U)*Pair[e[2], k[1]] + 3*(-MH2 + S + T + 
                T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             (2*(-MH2 + S34 + T24 + U)*Pair[ec[4], k[1]] + 3*(-MH2 + S + T + 
                T14)*Pair[ec[4], k[3]]))) + 
        4*(((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
             Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + S + T + T14)*Pair[e[1], k[3]] + (-MH2 + S34 + T + U)*
             Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
             Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((-3*MH2 + 2*S + 3*T + 2*T14)*
             Pair[ec[5], k[1]] + (MH2 - S - T - T14)*Pair[ec[5], k[3]]) + 
          2*(Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] - 
            Pair[e[1], k[4]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*(-5*Pair[e[2], k[1]]*
                 Pair[ec[5], k[1]] + Pair[e[2], k[5]]*(5*Pair[ec[5], k[1]] + 
                  Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + Pair[e[2], k[4]]*(
                -5*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] + Pair[ec[4], k[5]]*
                 (5*Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - Pair[ec[5], 
                   k[4]]))))) - 2*Pair[e[2], ec[4]]*
         (-2*Pair[e[1], k[4]]*((MH2 - T - T14)*Pair[ec[5], k[1]] + 
            (-MH2 + S + T + T14)*Pair[ec[5], k[2]]) + Pair[e[1], k[5]]*
           (-5*(S34 - U)*Pair[ec[5], k[1]] - 2*(T14 - U)*Pair[ec[5], k[2]] + 
            2*(-S + S34)*Pair[ec[5], k[4]]) + 2*Pair[e[1], k[2]]*
           ((-MH2 + S + T)*Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*
             Pair[ec[5], k[4]]))) - 4*D0i[dd22, MH2, 0, T24, 0, 
        2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (4*(S + T14 + T24)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 4*(S + T14 + T24)*
         (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
         Pair[ec[4], k[2]] + 2*(S + T14)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
         Pair[ec[5], k[3]] + 2*(S + T14)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
         Pair[ec[5], k[3]] - 16*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
         (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
           Pair[ec[4], k[2]])*Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
         (-(Pair[e[1], k[4]]*(2*(S + T14 + T24)*Pair[ec[5], k[2]] + 
             (-5*S + 3*T14)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + Pair[e[1], k[2]]*
           (2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-3*S + 5*T14)*
             Pair[ec[5], k[3]] + 2*(S + T14 + T24)*Pair[ec[5], k[4]]))) + 
      D0i[dd1, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (-4*((-MH2 + T)*Pair[e[1], k[2]] + (S - S34 + T14 - U)*
           Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] - 4*((-MH2 + T)*Pair[e[1], k[2]] + 
          (S - S34 + T14 - U)*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
         ((-3*MH2*S34 + 2*S*S34 + 3*S34*T + 3*MH2*U - 3*T*U - 2*T14*U)*
           Pair[e[2], ec[4]] - 4*(((S34 + U)*Pair[e[2], k[1]] + 
              (-3*MH2 + S + 3*T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + (-3*MH2 + S + 
                3*T + T14)*Pair[ec[4], k[3]]))) + 4*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*Pair[ec[5], k[1]] + 
          (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
        8*(Pair[e[1], k[5]]*(-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
              Pair[ec[5], k[1]]) + Pair[e[2], k[1]]*Pair[ec[4], k[2]]*
             Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*
                Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*Pair[ec[5], k[3]])) + 
          Pair[e[1], k[3]]*(Pair[ec[4], k[2]]*(2*Pair[e[2], k[3]]*Pair[ec[5], 
                k[1]] - Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], 
                 k[3]]) + Pair[e[2], k[1]]*(2*Pair[ec[5], k[1]] + 
                Pair[ec[5], k[3]])) + Pair[e[2], k[4]]*
             (2*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] - Pair[ec[4], k[5]]*(
                Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*(
                2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])))) + 
        2*Pair[e[2], ec[4]]*(2*Pair[e[1], k[2]]*(S34*Pair[ec[5], k[2]] - 
            (S34 + T14)*Pair[ec[5], k[3]] - (MH2 + S34 - T)*
             Pair[ec[5], k[4]]) + Pair[e[1], k[3]]*
           ((-5*S34 + 2*T14 + 3*U)*Pair[ec[5], k[2]] + 5*(S34 - U)*
             Pair[ec[5], k[3]] + (2*S + 3*S34 - 5*U)*Pair[ec[5], k[4]]) + 
          2*Pair[e[1], k[4]]*((MH2 - T + U)*Pair[ec[5], k[2]] - 
            (S + U)*Pair[ec[5], k[3]] - U*Pair[ec[5], k[4]]))) - 
      2*D0i[dd11, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2 - T)*(S34 - U)*Pair[e[2], ec[4]] - 
          4*(MH2 - T)*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*Pair[ec[4], k[3]])) - 2*Pair[e[1], k[3]]*
         (2*(S34 + U)*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          2*(S34 + U)*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[ec[5], k[2]] - 2*Pair[ec[5], k[3]] - 
            Pair[ec[5], k[4]]) - Pair[e[2], ec[4]]*
           ((-3*S34 + U)*Pair[ec[5], k[1]] - (S34 - 3*U)*Pair[ec[5], k[3]] + 
            2*(S34 + U)*Pair[ec[5], k[4]]))) + 
      4*D0i[dd11, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, MT2]*
       (2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*Pair[e[2], k[4]]*
         Pair[ec[4], ec[5]] + 2*(-3*MH2 + S34 + T + U)*Pair[e[1], k[3]]*
         Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 2*(-MH2 + T)*Pair[e[1], ec[4]]*
         Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 2*(-MH2 + T)*Pair[e[1], e[2]]*
         Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 8*Pair[e[1], k[3]]*
         ((Pair[e[2], k[1]] + Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
          Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[3]]))*
         Pair[ec[5], k[3]] - Pair[e[2], ec[4]]*
         (-((MH2 - T)*(Pair[e[1], k[2]] + Pair[e[1], k[4]])*
            Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*((-3*MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + 2*(S + S34 - T14 - U)*Pair[ec[5], k[3]] + 
            (-3*MH2 + S34 + T + U)*Pair[ec[5], k[4]]))) + 
      8*(D0i[dd003, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) - 2*((Pair[e[1], k[2]] + Pair[e[1], k[4]])*
             Pair[e[2], ec[4]] - 2*(Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
              Pair[e[1], e[2]]*Pair[ec[4], k[2]]))*Pair[ec[5], k[1]]) + 
        D0i[dd002, MH2, 0, T24, 0, 2*MH2 - S34 - T - U, T, MT2, MT2, MT2, 
          MT2]*(Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) + 4*Pair[ec[4], k[2]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            Pair[e[1], e[2]]*Pair[ec[5], k[3]]) + 4*Pair[e[2], k[4]]*
           ((Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[ec[4], ec[5]] + 
            Pair[e[1], ec[4]]*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[2]]) + 
            Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[4]]))) + 
        D0i[dd002, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
         (4*Pair[e[1], k[5]]*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          4*Pair[e[1], k[5]]*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          Pair[e[1], ec[5]]*((S34 - U)*Pair[e[2], ec[4]] - 
            4*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[3]])) - 4*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
           Pair[ec[5], k[1]] - 4*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
           Pair[ec[5], k[1]] + 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
             Pair[ec[5], k[1]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])))) + 
      4*(C0i[cc1, T24, 0, T, MT2, MT2, MT2]*
         (-2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           (Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             Pair[ec[4], k[2]]) + (Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], ec[4]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]])) + 
        C0i[cc2, 0, T24, T, MT2, MT2, MT2]*(2*Pair[e[1], k[3]]*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 2*Pair[e[1], k[3]]*
           Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 2*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) - 
          2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - 
            Pair[ec[5], k[3]]) - Pair[e[2], ec[4]]*
           (2*Pair[e[1], k[2]]*Pair[ec[5], k[2]] - 2*Pair[e[1], k[4]]*
             Pair[ec[5], k[4]] - Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
              Pair[ec[5], k[4]])))) - D0i[dd0, MH2, T24, 0, 0, 
        MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2*S34 - S*S34 - S34*T - MH2*U + T*U + T14*U)*
           Pair[e[2], ec[4]] + 2*(((S34 + U)*Pair[e[2], k[1]] + 
              (-2*MH2 + S + 2*T + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*((S34 + U)*Pair[ec[4], k[1]] + (-2*MH2 + S + 
                2*T + T14)*Pair[ec[4], k[3]]))) + 
        2*(((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          ((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
            (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] - 
          Pair[e[1], ec[4]]*Pair[e[2], k[4]]*((MH2 + S34 - T + U)*
             Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) - 
          Pair[e[1], e[2]]*Pair[ec[4], k[2]]*((MH2 + S34 - T + U)*
             Pair[ec[5], k[1]] + (-MH2 + S + T + T14)*Pair[ec[5], k[3]]) + 
          2*(Pair[e[1], k[3]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
                Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             (-(Pair[e[2], k[3]]*Pair[ec[4], k[2]]*Pair[ec[5], k[1]]) + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + 
              Pair[e[2], k[4]]*(-(Pair[ec[4], k[3]]*Pair[ec[5], k[1]]) + 
                Pair[ec[4], k[1]]*Pair[ec[5], k[3]]))) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*((S + U)*Pair[ec[5], k[1]] + (-MH2 + S + T)*Pair[
                ec[5], k[2]] - S*Pair[ec[5], k[4]]) - Pair[e[1], k[3]]*
             ((S34 - U)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + 
              S*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
             ((S34 + T14)*Pair[ec[5], k[1]] + T14*Pair[ec[5], k[2]] + 
              (MH2 - T - T14)*Pair[ec[5], k[4]])))) + 
      2*D0i[dd12, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*(-((S34 - U)*(S34 - T + T24 + U)*
            Pair[e[2], ec[4]]) + 4*(2*MH2 - S - 2*T - T14)*
           (Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])) + 
        2*(2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
            (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
          2*(2*(-MH2 + S34 + T24 + U)*Pair[e[1], k[3]] - 
            (S34 + U)*Pair[e[1], k[5]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
          8*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
             Pair[ec[4], k[3]])*(Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[4]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]]) - Pair[e[1], k[3]]*(3*Pair[ec[5], k[1]] + 
              Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[5]]*((3*S34 - U)*Pair[ec[5], k[2]] - 
              4*(S34 - U)*Pair[ec[5], k[3]] - (S34 - 3*U)*Pair[ec[5], 
                k[4]]) + 2*Pair[e[1], k[3]]*((MH2 - 3*S34 - T24 + U)*Pair[
                ec[5], k[2]] + 2*(S34 - U)*Pair[ec[5], k[3]] + 
              (MH2 + S34 - T24 - 3*U)*Pair[ec[5], k[4]])))) - 
      8*D0i[dd123, MH2, T24, 0, 0, MH2 - S - T - T14, T, MT2, MT2, MT2, MT2]*
       (Pair[e[2], ec[4]]*((-S - S34 + T14 + U)*Pair[e[1], k[3]]*
           Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
           ((S34 - U)*Pair[ec[5], k[1]] + (S - T14)*Pair[ec[5], k[3]])) + 
        4*(Pair[e[1], k[2]]*((2*Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(2*Pair[ec[4], k[1]] - 
              Pair[ec[4], k[5]]))*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
           ((-2*Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
            Pair[e[2], k[4]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))*
           Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[4], k[2]]*
             (2*Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*(
                -4*Pair[ec[5], k[1]] - Pair[ec[5], k[2]] + Pair[ec[5], 
                 k[4]])) + Pair[e[2], k[4]]*(2*Pair[ec[4], k[5]]*Pair[ec[5], 
                k[1]] + Pair[ec[4], k[1]]*(-4*Pair[ec[5], k[1]] - 
                Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))))))))/(MW*SW)
