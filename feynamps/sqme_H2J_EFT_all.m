(* Created with the Wolfram Language : www.wolfram.com *)
2*Alfas*GH*Pi*
  ((23*Alfas*GH*Pi*(8*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
        2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*(4*Den[MH2 - S34 - T14 - T24, 0]*
         (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
              Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
           Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
              Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
           Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
          4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) - 
          2*Pair[e[4], e[5]]*(2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], 
                k[1]] - S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*
             (T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
            Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) - 
        4*(2*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))))) + 
      Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
              2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
                Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*
                 (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                   Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
            4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                  (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                 Pair[ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*Pair[
                ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
                Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
              (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*
                 Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                  Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*
                 Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*
                 ((Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                  Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
          Den[T14, 0]*(Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]]) + 
          2*((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]])))) - 
      4*Den[S, 0]*Den[S34, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*
           Pair[e[4], e[5]] + 2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*
             Pair[e[5], k[1]] - 2*(MH2 - S34)*Pair[e[4], k[1]]*
             (Pair[e[5], k[1]] - Pair[e[5], k[3]] - Pair[e[5], k[4]]) + 
            Pair[e[4], k[3]]*(2*(T14 + T24)*Pair[e[5], k[1]] - 
              2*T14*Pair[e[5], k[3]] + (T - T14 - T24 - U)*Pair[e[5], 
                k[4]])))*Pair[ec[1], ec[2]] - 
        4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], 
                k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*Pair[e[4], k[1]] + 
               (-MH2 + S34)*Pair[e[4], k[2]] + (T14 + T24)*Pair[e[4], k[3]])*
              Pair[ec[2], e[5]])) + (MH2 - S34)*(Pair[e[5], k[1]] + 
            Pair[e[5], k[2]])*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
            (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*
             Pair[ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
           (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
              Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[
                ec[2], k[5]])))) + 
      4*(Den[T, 0]*(-2*((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (-(((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
              Pair[e[5], k[4]])*Pair[ec[1], ec[2]]) - 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 2*Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S34 - T14 - T24, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) + Den[MH2 - S - T24 - U, 0]*
       (-8*Den[T14, 0]*(-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*
           Pair[e[5], k[2]]*Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]])))))))/6 + 
   (2*Alfas*GH*Pi*(8*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*
       (-4*(Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) + Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]])) + 4*(Den[MH2 - S34 - T14 - T24, 0]*
           (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
              4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
                Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
            4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
             Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
             Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
                Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*
                 Pair[e[5], k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
             Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
             ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
            4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                k[1]] + S*Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
             (2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], k[1]] - 
                S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*(
                T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
              Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]])))))) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
            2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
              Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*(
                2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                 Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
          4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[
                ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
              (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
              Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
            (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*Pair[
                ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
            2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*Pair[
                ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(
                (Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
        4*(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Den[T24, 0]*
           (Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(Pair[e[4], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], k[4]])) - 
            2*(-2*Pair[e[4], k[2]]*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]]))) + 
        4*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]])))) + 
      Den[T24, 0]*(8*Den[MH2 - S - T - T14, 0]*(-2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], k[2]]*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
          (MH2*(S - T14) - S*(S34 + T24) + T14*(T24 + U))*Pair[ec[1], e[5]]*
           Pair[ec[2], e[4]] + 2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*
             Pair[ec[1], k[2]] + (-MH2 + T24 + U)*Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + 
              (-MH2 + T24 + U)*Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*
           Pair[ec[1], k[5]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[5], k[1]]*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], e[5]]*Pair[ec[1], k[5]]*
           Pair[ec[2], k[4]] + 2*Pair[ec[1], e[5]]*
           (Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + U)*Pair[ec[2], k[1]] + 
              (S + T14)*Pair[ec[2], k[3]]) + ((-2*MH2 + S34 + 2*T24 + U)*Pair[
                e[4], k[1]] + (S + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
              k[4]]) - 4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - 
            Pair[e[5], k[1]]*Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - 
              Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           ((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]]))) - 
        4*Den[T, 0]*(Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*
              Pair[ec[2], e[4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], 
                k[4]] + Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) + 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      4*(Den[T, 0]*(-(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]]) + 
          2*((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
             Pair[e[5], k[4]])*Pair[ec[1], ec[2]] - 
          2*((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) - 4*Den[S34, 0]*
       (Den[S, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*Pair[e[4], e[5]] + 
            2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*Pair[e[5], k[1]] - 
              2*(MH2 - S34)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                Pair[e[5], k[3]] - Pair[e[5], k[4]]) + Pair[e[4], k[3]]*(
                2*(T14 + T24)*Pair[e[5], k[1]] - 2*T14*Pair[e[5], k[3]] + 
                (T - T14 - T24 - U)*Pair[e[5], k[4]])))*Pair[ec[1], ec[2]] - 
          4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + 
                 Pair[e[5], k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*
                  Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
                 (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[2], e[5]])) + 
            (MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
            ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
              (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
             (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[
                ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
             (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
                Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*Pair[
                ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                  Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*
                 Pair[ec[2], k[5]])))) + Den[MH2 - S - T - T14, 0]*
         (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[ec[2], e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*Pair[
                ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
              T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
            (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
              Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
            Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*Pair[
                e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
            (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*
             (Pair[ec[2], k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*
             (Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
              Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]))))))))/3 + 
   (2*Alfas*GH*Pi*(8*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[T24, 0]*(Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + 
                 T24)*Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(
                Pair[e[4], k[2]]*Pair[ec[2], k[1]] + Pair[e[4], k[1]]*
                 Pair[ec[2], k[4]])) - 2*(-2*Pair[e[4], k[2]]*(
                (-MH2 + S34 + T + U)*Pair[e[5], k[1]] + (MH2 - S34 - T24 - U)*
                 Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) + Den[T14, 0]*
           (Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-2*((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) - 
      4*(Den[T, 0]*(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - 2*Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[U, 0]*
         (Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) + Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]]))))) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], 
                k[5]])))) + Den[T24, 0]*(-8*Den[MH2 - S - T - T14, 0]*
         (-2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[e[5], k[1]]*
           Pair[ec[1], ec[2]] + (MH2*(S - T14) - S*(S34 + T24) + 
            T14*(T24 + U))*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + 
          2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
            (-MH2 + T24 + U)*Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
            ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + (-MH2 + T24 + U)*Pair[
                e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[ec[1], k[5]]*
           Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[5], k[1]]*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], e[5]]*Pair[ec[1], k[5]]*Pair[ec[2], k[4]] + 
          2*Pair[ec[1], e[5]]*(Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + 
                U)*Pair[ec[2], k[1]] + (S + T14)*Pair[ec[2], k[3]]) + 
            ((-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[1]] + (S + T14)*Pair[
                e[4], k[3]])*Pair[ec[2], k[4]]) - 
          4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*
             Pair[ec[2], k[4]] + Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[5]]))) + 4*Den[T, 0]*
         (Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*Pair[ec[2], e[
                4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) - 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      Den[MH2 - S - T24 - U, 0]*(-8*Den[T14, 0]*
         (-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[e[5], k[2]]*
           Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[5], k[2]]*
           Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]]))))) + 
      4*(Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S34, 0]*(-2*((MH2 - S34)*Pair[e[4], e[5]] + 
            2*Pair[e[4], k[3]]*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]]) + Den[MH2 - S - T - T14, 0]*
           (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*Pair[ec[2], 
                e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
                (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
                 Pair[ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                  (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
              Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*
                 Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
              (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(Pair[ec[2], 
                 k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*(
                Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
                Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                   Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                    Pair[ec[2], k[5]])))))))))/3)*
  (8*(2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
      Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) - 
   4*Den[S, 0]*Den[S34, 0]*
    (-4*((MH2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
       Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
         (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
       (MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
         Pair[ec[5], k[2]]) - Pair[e[1], k[2]]*
        (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
           (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         (MH2 - S34)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]])) + 2*Pair[ec[4], k[3]]*
        (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
            Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          Pair[ec[5], k[3]] + Pair[e[1], k[2]]*
          (Pair[e[2], k[3]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + 
     Pair[e[1], e[2]]*((MH2 - S34)*(T + T14 - T24 - U)*Pair[ec[4], ec[5]] + 
       2*(2*(-MH2 + S34)*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
         2*(MH2 - S34)*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(T14 + T24)*Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], k[3]] + 
           (T - T14 - T24 - U)*Pair[ec[5], k[4]])))) + 
   Den[U, 0]*(-4*(-(((-MH2 + U)*Pair[e[1], e[2]] + 2*Pair[e[1], k[2]]*
           Pair[e[2], k[3]])*Pair[ec[4], ec[5]]) + 2*Pair[e[1], ec[5]]*
        ((-MH2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
          Pair[ec[4], k[2]]) - Pair[e[1], ec[4]]*
        ((-MH2 + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[3]]*
          Pair[ec[5], k[2]]) + Den[T14, 0]*
        (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
           2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
             2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], 
                k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
              (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
                Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
         4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
             (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
            Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
              Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
            (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
               (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
             (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], 
                k[4]])) - 2*Pair[e[2], k[3]]*(Pair[e[1], k[5]]*
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
              Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
           (MH2 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[4]])))) + 4*Den[MH2 - S34 - T14 - T24, 0]*
      (-2*(2*Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
           S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*(T24*Pair[e[2], k[3]] + 
           (MH2 - U)*Pair[e[2], k[4]]) - Pair[e[1], k[2]]*
          ((MH2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
           2*(MH2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       4*(MH2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
        ((-MH2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
        Pair[ec[4], k[5]] - 4*(MH2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 4*Pair[e[1], ec[4]]*
        ((-MH2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
        Pair[ec[5], k[4]] + Pair[e[1], e[2]]*((MH2 - S - T - 2*T14)*(MH2 - U)*
          Pair[ec[4], ec[5]] + 4*(MH2 - U)*(Pair[ec[4], k[5]]*
            Pair[ec[5], k[1]] - Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 
       8*Pair[e[2], k[3]]*(Pair[e[1], k[3]]*
          (-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + Pair[ec[4], k[1]]*
            Pair[ec[5], k[4]]) - (Pair[e[1], k[4]] + Pair[e[1], k[5]])*
          (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*
            Pair[ec[5], k[4]])))) + Den[MH2 - S34 - T14 - T24, 0]*
    (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH2 + 2*S + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH2 - 2*S - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       2*(-2*MH2 + 2*S + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 2*(-2*MH2 + 2*S + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + 2*(-2*MH2 + 2*S + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 2*(-2*MH2 + 2*S + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       2*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((MH2 - S - U)*Pair[ec[5], k[1]] + (-MH2 + S + T)*
            Pair[ec[5], k[2]]) + ((-MH2 + S + U)*Pair[ec[4], k[1]] + 
           (MH2 - S - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]])) - 
     4*Den[T, 0]*(2*(Pair[e[1], k[3]]*((MH2 - S34 - T + T14 + T24)*
            Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
           2*S*Pair[e[2], k[4]]) + 2*(MH2 - T)*
          (Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]]) - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       4*((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(MH2 - T)*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(MH2 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[ec[4], ec[5]] + 
         4*(MH2 - T)*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 8*Pair[e[1], k[3]]*
        (Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))) + 
   Den[MH2 - S - T24 - U, 0]*
    (-8*Den[T14, 0]*((-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] + 2*Pair[e[2], ec[5]]*
        ((-2*MH2 + S34 + T + 2*T14)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH2 + S34 + T + 2*T14)*Pair[ec[4], k[2]] + 
           (S + T24)*Pair[ec[4], k[3]])) - 2*(-2*MH2 + S34 + T + 2*T14)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] - 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] - Pair[ec[4], k[5]]))*
        ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
       2*Pair[e[1], ec[4]]*(((-MH2 + S34 + T14)*Pair[e[2], k[1]] + 
           (-MH2 + T + T14)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*((-MH2 + S34 + T14)*Pair[ec[5], k[1]] + 
           (-MH2 + T + T14)*Pair[ec[5], k[4]]))) - 
     4*Den[T, 0]*(4*(T14*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
        Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 4*(MH2 - T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
       2*Pair[e[2], ec[5]]*(2*(-MH2 + T)*Pair[e[1], k[4]]*Pair[ec[4], k[2]] - 
         2*(MH2 - T)*Pair[e[1], k[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*((MH2 + S - T + T24 - U)*
            Pair[ec[4], k[1]] - 2*(T14*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]]))) + 4*(T14*Pair[e[1], k[3]] + 
         (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
       4*(MH2 - T)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
        Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
        ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[e[2], ec[5]] - 
         4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[4]])) + 8*Pair[e[1], k[3]]*
        (Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[3]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[4], k[2]]*
            Pair[ec[5], k[3]] - Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))) + 
     4*(2*Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 
           2*Pair[ec[4], k[2]]) + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 
           2*Pair[ec[4], k[5]])) - 4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Den[S34, 0]*
        (-4*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]]) - 2*Pair[e[2], ec[5]]*
          (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
             2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
            (-2*(MH2 - S34)*Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
             (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) - 
         4*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 4*Pair[e[1], e[2]]*
          ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
          Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
          ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
           4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
             Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 8*Pair[ec[4], k[3]]*
          (-(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*(Pair[e[2], k[3]]*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + 
       Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))) + 
   4*(Den[T, 0]*(((-MH2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*
          Pair[e[2], k[1]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
        ((-MH2 + T)*Pair[e[1], ec[4]] + 2*Pair[e[1], k[3]]*
          Pair[ec[4], k[1]]) - 2*Pair[e[2], ec[4]]*
        ((-MH2 + T)*Pair[e[1], ec[5]] + 2*Pair[e[1], k[3]]*
          Pair[ec[5], k[1]])) + Den[S34, 0]*
      (-(Pair[e[2], ec[5]]*((MH2 - S34)*Pair[e[1], ec[4]] + 
          2*Pair[e[1], k[4]]*Pair[ec[4], k[3]])) + 2*Pair[e[1], ec[5]]*
        ((MH2 - S34)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[4]]*
          Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*
        ((MH2 - S34)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]])) + Den[MH2 - S34 - T14 - T24, 0]*
      (-2*(Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]]) - 
         Pair[e[1], k[3]]*(Pair[e[2], k[1]] + 2*Pair[e[2], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]))*
        Pair[ec[4], ec[5]] + 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))) + 
     Den[T14, 0]*(-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
         Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - 
             Pair[ec[5], k[3]])) + Pair[e[1], k[4]]*
          ((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
       Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         2*(Pair[e[2], k[4]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
           Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + 2*Pair[ec[5], k[4]])))) + 
     Den[S, 0]*(Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*
          Pair[ec[4], ec[5]] - 2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]]) - Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + 
             Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]]))) - 
       4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
            Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
           2*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (-(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]])) + 
           Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))) + 
   Den[2*MH2 - S34 - T - U, 0]*
    (4*(2*Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
         2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], ec[4]]*
        ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*
        ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]])) - 
     4*(Den[S, 0]*(Pair[e[1], e[2]]*((MH2 - S - T14 - T24)*(T14 - T24)*
            Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
              Pair[ec[5], k[3]] + Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (S - S34 - T + U)*Pair[ec[5], k[3]]) + 
             Pair[ec[4], k[2]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
               (-S + S34 - T + U)*Pair[ec[5], k[3]]))) + 
         4*(-((MH2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
              Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) - 
           (MH2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
            (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
           2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
                Pair[ec[4], k[2]])) - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
              Pair[ec[4], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
                (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[1]]*((-MH2 + S + T + U)*Pair[ec[5], k[3]] + 
             (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
            ((MH2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]]) + Pair[e[2], ec[4]]*((-MH2 + S + T + U)*
                Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]])))) + Den[T14, 0]*
        (4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH2 - S34 - T - U)*
            Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + 
           2*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[3]]*(-Pair[e[2], k[1]] + Pair[e[2], k[4]])*
              Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + (-Pair[e[2], k[1]] + Pair[e[2], k[4]])*
                Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
            Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
             (MH2 - S34 - T - T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
            (-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]]) + Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (MH2 - S34 - T - T14)*Pair[ec[5], 
                 k[3]]))) + Pair[e[1], ec[4]]*
          (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
           2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[2], k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
               (S34 - T - T14 + U)*Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
              ((-S34 + T - T14 + U)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + 
                 U)*Pair[ec[5], k[4]]))))))) - 
 2*Alfas*GH*Pi*
  ((-2*Alfas*GH*Pi*(8*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
        2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*(4*Den[MH2 - S34 - T14 - T24, 0]*
         (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
              Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
           Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
              Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
           Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
          4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) - 
          2*Pair[e[4], e[5]]*(2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], 
                k[1]] - S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*
             (T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
            Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) - 
        4*(2*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))))) + 
      Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
              2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
                Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*
                 (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                   Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
            4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                  (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                 Pair[ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*Pair[
                ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
                Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
              (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*
                 Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                  Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*
                 Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*
                 ((Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                  Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
          Den[T14, 0]*(Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]]) + 
          2*((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]])))) - 
      4*Den[S, 0]*Den[S34, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*
           Pair[e[4], e[5]] + 2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*
             Pair[e[5], k[1]] - 2*(MH2 - S34)*Pair[e[4], k[1]]*
             (Pair[e[5], k[1]] - Pair[e[5], k[3]] - Pair[e[5], k[4]]) + 
            Pair[e[4], k[3]]*(2*(T14 + T24)*Pair[e[5], k[1]] - 
              2*T14*Pair[e[5], k[3]] + (T - T14 - T24 - U)*Pair[e[5], 
                k[4]])))*Pair[ec[1], ec[2]] - 
        4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], 
                k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*Pair[e[4], k[1]] + 
               (-MH2 + S34)*Pair[e[4], k[2]] + (T14 + T24)*Pair[e[4], k[3]])*
              Pair[ec[2], e[5]])) + (MH2 - S34)*(Pair[e[5], k[1]] + 
            Pair[e[5], k[2]])*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
            (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*
             Pair[ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
           (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
              Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[
                ec[2], k[5]])))) + 
      4*(Den[T, 0]*(-2*((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (-(((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
              Pair[e[5], k[4]])*Pair[ec[1], ec[2]]) - 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 2*Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S34 - T14 - T24, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) + Den[MH2 - S - T24 - U, 0]*
       (-8*Den[T14, 0]*(-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*
           Pair[e[5], k[2]]*Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]])))))))/3 + 
   (2*Alfas*GH*Pi*(8*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*
       (-4*(Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) + Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]])) + 4*(Den[MH2 - S34 - T14 - T24, 0]*
           (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
              4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
                Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
            4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
             Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
             Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
                Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*
                 Pair[e[5], k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
             Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
             ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
            4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                k[1]] + S*Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
             (2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], k[1]] - 
                S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*(
                T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
              Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]])))))) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
            2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
              Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*(
                2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                 Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
          4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[
                ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
              (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
              Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
            (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*Pair[
                ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
            2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*Pair[
                ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(
                (Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
        4*(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Den[T24, 0]*
           (Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(Pair[e[4], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], k[4]])) - 
            2*(-2*Pair[e[4], k[2]]*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]]))) + 
        4*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]])))) + 
      Den[T24, 0]*(8*Den[MH2 - S - T - T14, 0]*(-2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], k[2]]*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
          (MH2*(S - T14) - S*(S34 + T24) + T14*(T24 + U))*Pair[ec[1], e[5]]*
           Pair[ec[2], e[4]] + 2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*
             Pair[ec[1], k[2]] + (-MH2 + T24 + U)*Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + 
              (-MH2 + T24 + U)*Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*
           Pair[ec[1], k[5]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[5], k[1]]*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], e[5]]*Pair[ec[1], k[5]]*
           Pair[ec[2], k[4]] + 2*Pair[ec[1], e[5]]*
           (Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + U)*Pair[ec[2], k[1]] + 
              (S + T14)*Pair[ec[2], k[3]]) + ((-2*MH2 + S34 + 2*T24 + U)*Pair[
                e[4], k[1]] + (S + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
              k[4]]) - 4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - 
            Pair[e[5], k[1]]*Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - 
              Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           ((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]]))) - 
        4*Den[T, 0]*(Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*
              Pair[ec[2], e[4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], 
                k[4]] + Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) + 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      4*(Den[T, 0]*(-(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]]) + 
          2*((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
             Pair[e[5], k[4]])*Pair[ec[1], ec[2]] - 
          2*((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) - 4*Den[S34, 0]*
       (Den[S, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*Pair[e[4], e[5]] + 
            2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*Pair[e[5], k[1]] - 
              2*(MH2 - S34)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                Pair[e[5], k[3]] - Pair[e[5], k[4]]) + Pair[e[4], k[3]]*(
                2*(T14 + T24)*Pair[e[5], k[1]] - 2*T14*Pair[e[5], k[3]] + 
                (T - T14 - T24 - U)*Pair[e[5], k[4]])))*Pair[ec[1], ec[2]] - 
          4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + 
                 Pair[e[5], k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*
                  Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
                 (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[2], e[5]])) + 
            (MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
            ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
              (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
             (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[
                ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
             (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
                Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*Pair[
                ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                  Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*
                 Pair[ec[2], k[5]])))) + Den[MH2 - S - T - T14, 0]*
         (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[ec[2], e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*Pair[
                ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
              T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
            (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
              Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
            Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*Pair[
                e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
            (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*
             (Pair[ec[2], k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*
             (Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
              Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]))))))))/3 - 
   (23*Alfas*GH*Pi*(8*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[T24, 0]*(Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + 
                 T24)*Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(
                Pair[e[4], k[2]]*Pair[ec[2], k[1]] + Pair[e[4], k[1]]*
                 Pair[ec[2], k[4]])) - 2*(-2*Pair[e[4], k[2]]*(
                (-MH2 + S34 + T + U)*Pair[e[5], k[1]] + (MH2 - S34 - T24 - U)*
                 Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) + Den[T14, 0]*
           (Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-2*((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) - 
      4*(Den[T, 0]*(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - 2*Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[U, 0]*
         (Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) + Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]]))))) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], 
                k[5]])))) + Den[T24, 0]*(-8*Den[MH2 - S - T - T14, 0]*
         (-2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[e[5], k[1]]*
           Pair[ec[1], ec[2]] + (MH2*(S - T14) - S*(S34 + T24) + 
            T14*(T24 + U))*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + 
          2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
            (-MH2 + T24 + U)*Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
            ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + (-MH2 + T24 + U)*Pair[
                e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[ec[1], k[5]]*
           Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[5], k[1]]*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], e[5]]*Pair[ec[1], k[5]]*Pair[ec[2], k[4]] + 
          2*Pair[ec[1], e[5]]*(Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + 
                U)*Pair[ec[2], k[1]] + (S + T14)*Pair[ec[2], k[3]]) + 
            ((-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[1]] + (S + T14)*Pair[
                e[4], k[3]])*Pair[ec[2], k[4]]) - 
          4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*
             Pair[ec[2], k[4]] + Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[5]]))) + 4*Den[T, 0]*
         (Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*Pair[ec[2], e[
                4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) - 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      Den[MH2 - S - T24 - U, 0]*(-8*Den[T14, 0]*
         (-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[e[5], k[2]]*
           Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[5], k[2]]*
           Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]]))))) + 
      4*(Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S34, 0]*(-2*((MH2 - S34)*Pair[e[4], e[5]] + 
            2*Pair[e[4], k[3]]*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]]) + Den[MH2 - S - T - T14, 0]*
           (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*Pair[ec[2], 
                e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
                (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
                 Pair[ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                  (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
              Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*
                 Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
              (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(Pair[ec[2], 
                 k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*(
                Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
                Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                   Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                    Pair[ec[2], k[5]])))))))))/6)*
  (8*(Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
      Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
   Den[T24, 0]*(4*Den[T, 0]*(Pair[e[1], ec[5]]*
        (-((MH2 - T)*(S - S34 - T14 + U)*Pair[e[2], ec[4]]) + 
         4*(MH2 - T)*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 
       2*(-2*((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
           (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
         2*((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
           (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
         2*(MH2 - T)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]]) + 2*(MH2 - T)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
         Pair[e[2], ec[4]]*(2*(MH2 - T)*Pair[e[1], k[4]]*Pair[ec[5], k[2]] - 
           2*(MH2 - T)*Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
            ((S + S34 + T14 - U)*Pair[ec[5], k[1]] + 
             2*((S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]))) + 
         4*Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - Pair[
                ec[5], k[4]])) + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            (Pair[ec[5], k[2]] - Pair[ec[5], k[4]])))) - 
     4*(Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
          ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]])) - 2*Pair[e[2], ec[4]]*
        (Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[2]]) + 
         Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]) + 
         Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[4]]))) - 
     8*Den[MH2 - S - T - T14, 0]*((MH2*(S - T14) - S*(S34 + T24) + 
         T14*(T24 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
       2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
        Pair[ec[4], ec[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], k[5]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 2*Pair[e[1], ec[5]]*
        (((-2*MH2 + S34 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
         Pair[e[2], k[4]]*((-2*MH2 + S34 + 2*T24 + U)*Pair[ec[4], k[1]] + 
           (S + T14)*Pair[ec[4], k[3]])) - 2*(-2*MH2 + S34 + 2*T24 + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
       2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
        Pair[ec[5], k[1]] - 4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
          Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[5]]))*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
         Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
          (Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + 2*Pair[e[2], ec[4]]*
        ((-MH2 + S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
         (-MH2 + T24 + U)*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
         Pair[e[1], k[5]]*((-MH2 + S34 + T24)*Pair[ec[5], k[2]] + 
           (-MH2 + T24 + U)*Pair[ec[5], k[4]])))) + 
   Den[MH2 - S - T24 - U, 0]*
    (-8*Den[T14, 0]*((-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
        Pair[e[1], ec[4]]*Pair[e[2], ec[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*
        Pair[e[1], k[4]]*Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 
       2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
        Pair[ec[4], k[1]] + 2*Pair[e[2], ec[5]]*
        ((-2*MH2 + S34 + T + 2*T14)*Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
         (S + T24)*Pair[e[1], k[3]]*Pair[ec[4], k[1]] + 
         Pair[e[1], k[4]]*((-2*MH2 + S34 + T + 2*T14)*Pair[ec[4], k[2]] + 
           (S + T24)*Pair[ec[4], k[3]])) - 2*(-2*MH2 + S34 + T + 2*T14)*
        Pair[e[1], k[4]]*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 
       2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*
        Pair[ec[5], k[2]] - 4*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] - 
         Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
          (Pair[ec[4], k[2]] - Pair[ec[4], k[5]]))*
        ((Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
       2*Pair[e[1], ec[4]]*(((-MH2 + S34 + T14)*Pair[e[2], k[1]] + 
           (-MH2 + T + T14)*Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
         Pair[e[2], k[5]]*((-MH2 + S34 + T14)*Pair[ec[5], k[1]] + 
           (-MH2 + T + T14)*Pair[ec[5], k[4]]))) - 
     4*Den[T, 0]*(4*(T14*Pair[e[1], k[3]] + (MH2 - T)*Pair[e[1], k[4]])*
        Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 4*(MH2 - T)*Pair[e[1], ec[5]]*
        Pair[e[2], k[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
       2*Pair[e[2], ec[5]]*(2*(-MH2 + T)*Pair[e[1], k[4]]*Pair[ec[4], k[2]] - 
         2*(MH2 - T)*Pair[e[1], k[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*((MH2 + S - T + T24 - U)*
            Pair[ec[4], k[1]] - 2*(T14*Pair[ec[4], k[2]] + 
             S*Pair[ec[4], k[3]]))) + 4*(T14*Pair[e[1], k[3]] + 
         (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 
       4*(MH2 - T)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[3]])*
        Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
        ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[e[2], ec[5]] - 
         4*(MH2 - T)*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*Pair[ec[5], k[4]])) + 8*Pair[e[1], k[3]]*
        (Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[2]] + 
         Pair[e[2], k[3]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[5]])*
          Pair[ec[5], k[2]] + Pair[e[2], k[5]]*(Pair[ec[4], k[2]]*
            Pair[ec[5], k[3]] - Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
           Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))) + 
     4*(2*Pair[e[2], ec[5]]*(Pair[e[1], k[5]]*(Pair[ec[4], k[1]] - 
           2*Pair[ec[4], k[2]]) + Pair[e[1], k[4]]*(Pair[ec[4], k[2]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*(Pair[ec[4], k[1]] + 
           2*Pair[ec[4], k[5]])) - 4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) + 
         ((Pair[e[1], k[3]] + 2*Pair[e[1], k[4]])*Pair[e[2], ec[4]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]))*
          Pair[ec[5], k[2]]) + Den[S34, 0]*
        (-4*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + 4*Pair[e[1], ec[5]]*
          Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
           T14*Pair[ec[4], k[3]]) - 2*Pair[e[2], ec[5]]*
          (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
             2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
            (-2*(MH2 - S34)*Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
           Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
             (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) - 
         4*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
          Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + 4*Pair[e[1], e[2]]*
          ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
          Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
          ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
           4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
             Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 8*Pair[ec[4], k[3]]*
          (-(Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
           (Pair[e[1], k[2]] - Pair[e[1], k[5]])*(Pair[e[2], k[3]]*
              Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]]))) + 
       Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[4]])*Pair[ec[5], k[2]] + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]))))) + 
   4*(Den[S34, 0]*(Pair[e[2], ec[5]]*((MH2 - S34)*Pair[e[1], ec[4]] + 
         2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
        ((MH2 - S34)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[4]]*
          Pair[ec[4], k[3]]) + Den[MH2 - S - T - T14, 0]*
        (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
            Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH2 - S34)*
                Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
             Pair[e[2], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
               (MH2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
             2*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[2]] + T24*
                Pair[ec[4], k[3]]))) + 
         4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                 k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
              ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
           (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
            ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
            Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]) - 2*Pair[ec[4], k[3]]*
            (Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[3]]*(-Pair[e[2], k[1]] + Pair[e[2], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[5], k[2]] + (-Pair[e[2], k[1]] + Pair[e[2], k[5]])*
                Pair[ec[5], k[3]])))) - 2*Pair[e[1], e[2]]*
        ((MH2 - S34)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]])) + Den[T14, 0]*
      (-4*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
             (Pair[ec[4], k[2]] + Pair[ec[4], k[5]]))) + 
         Pair[ec[4], k[1]]*(Pair[e[1], ec[5]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - 
             Pair[ec[5], k[3]])) + Pair[e[1], k[4]]*
          ((Pair[e[2], k[3]] + 2*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
           Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]]))) + 
       Pair[e[1], ec[4]]*((-2*S + S34 - T + 2*T24)*Pair[e[2], ec[5]] + 
         2*(Pair[e[2], k[4]]*(-2*Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           Pair[e[2], k[5]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
           Pair[e[2], k[1]]*(Pair[ec[5], k[2]] + 2*Pair[ec[5], k[4]]))))) + 
   Den[2*MH2 - S34 - T - U, 0]*
    (4*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
         2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
        ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*
        ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]])) - 
     4*(Den[T14, 0]*(4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*
             (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + 
           (MH2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*
              Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + 
           2*(Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]] + 
             Pair[e[1], k[3]]*(-Pair[e[2], k[1]] + Pair[e[2], k[4]])*
              Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + (-Pair[e[2], k[1]] + Pair[e[2], k[4]])*
                Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
            Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
             (MH2 - S34 - T - T14)*Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
            (-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[
                ec[4], ec[5]]) + Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (MH2 - S34 - T - T14)*Pair[ec[5], 
                 k[3]]))) + Pair[e[1], ec[4]]*
          (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
           2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[2], k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
               (S34 - T - T14 + U)*Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
              ((-S34 + T - T14 + U)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + 
                 U)*Pair[ec[5], k[4]])))) + Den[T24, 0]*
        (Pair[e[1], ec[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
             Pair[e[2], ec[4]]) + 4*(MH2 - S34 - T - U)*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])) - 2*(2*(MH2 - S34 - T - U)*
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + 2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
           4*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
                Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
             (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]]))*
            Pair[ec[5], k[3]] - 2*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
            ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T24 - U)*
              Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
            ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T24 - U)*
              Pair[ec[5], k[3]]) + Pair[e[2], ec[4]]*
            ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*((-S34 + T - T24 + U)*
                Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]])))))) - 
   4*(Den[T, 0]*(-2*((-MH2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*
          Pair[e[2], k[1]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
        ((-MH2 + T)*Pair[e[1], ec[4]] + 2*Pair[e[1], k[3]]*
          Pair[ec[4], k[1]]) + Pair[e[2], ec[4]]*
        ((-MH2 + T)*Pair[e[1], ec[5]] + 2*Pair[e[1], k[3]]*
          Pair[ec[5], k[1]])) + Den[MH2 - S - T - T14, 0]*
      (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
         2*(Pair[e[2], k[5]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
           Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + 2*Pair[ec[4], k[5]]))) + 
       4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
            Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
            Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]])))) + Den[U, 0]*
      (-2*((-MH2 + U)*Pair[e[1], e[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
        ((-MH2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
          Pair[ec[4], k[2]]) + Pair[e[1], ec[4]]*
        ((-MH2 + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[3]]*
          Pair[ec[5], k[2]]) + Den[T14, 0]*
        (Pair[e[1], ec[4]]*((S - S34 + T - T24)*(MH2 - U)*Pair[e[2], ec[5]] + 
           2*(2*(MH2 - U)*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
             2*(MH2 - U)*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + Pair[ec[5], 
                k[2]] - Pair[ec[5], k[3]]) + Pair[e[2], k[3]]*
              (2*(S + T24)*Pair[ec[5], k[1]] + (S + S34 - T + T24)*
                Pair[ec[5], k[2]] - 2*S*Pair[ec[5], k[3]]))) - 
         4*(Pair[e[1], ec[5]]*((-MH2 + U)*Pair[e[2], k[1]] + 
             (S + T24)*Pair[e[2], k[3]] + (MH2 - U)*Pair[e[2], k[4]])*
            Pair[ec[4], k[1]] + (MH2 - U)*Pair[e[2], ec[5]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[1]] + Pair[e[1], k[4]]*
              Pair[ec[4], k[5]]) + Pair[e[1], k[4]]*
            (((-MH2 + U)*Pair[e[2], k[1]] + (S + T24)*Pair[e[2], k[3]] + 
               (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
             (MH2 - U)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] - Pair[ec[5], 
                k[4]])) - 2*Pair[e[2], k[3]]*(Pair[e[1], k[5]]*
              Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                (Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + Pair[e[1], k[3]]*
              Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[4]])) + 
           (MH2 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[4]]))) + Den[MH2 - S - T - T14, 0]*
        (Pair[e[1], ec[5]]*((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*
            Pair[e[2], ec[4]] + 2*(-2*(MH2 - U)*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*(Pair[ec[4], k[2]] - 
                 Pair[ec[4], k[3]])) + Pair[e[2], k[3]]*(-2*T24*Pair[ec[4], 
                 k[1]] + (MH2 + S - T + T14 - U)*Pair[ec[4], k[2]] - 2*S*
                Pair[ec[4], k[3]]))) - 
         4*(Pair[e[1], k[5]]*(-((T24*Pair[e[2], k[3]] + (MH2 - U)*
                 Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + 
             (MH2 - U)*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], 
                k[5]])) - Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + 
             (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
           (MH2 - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
            Pair[ec[5], k[1]] + (MH2 - U)*Pair[e[2], ec[4]]*
            (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]) - 2*Pair[e[2], k[3]]*
            (Pair[e[1], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[4], k[1]]*
                Pair[ec[5], k[3]] - Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))))))) - 
 2*Alfas*GH*Pi*
  ((-2*Alfas*GH*Pi*(8*(-(Pair[e[4], e[5]]*Pair[ec[1], ec[2]]) + 
        2*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*(4*Den[MH2 - S34 - T14 - T24, 0]*
         (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
              Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
           Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
              Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
           Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
          4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) - 
          2*Pair[e[4], e[5]]*(2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], 
                k[1]] - S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*
             (T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
            Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) - 
        4*(2*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))))) + 
      Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
              2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
                Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*
                 (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                   Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
            4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                  (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                 Pair[ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*Pair[
                ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
                Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
              (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*
                 Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                  Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*
                 Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*
                 ((Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                  Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
          Den[T14, 0]*(Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]]) + 
          2*((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]])))) - 
      4*Den[S, 0]*Den[S34, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*
           Pair[e[4], e[5]] + 2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*
             Pair[e[5], k[1]] - 2*(MH2 - S34)*Pair[e[4], k[1]]*
             (Pair[e[5], k[1]] - Pair[e[5], k[3]] - Pair[e[5], k[4]]) + 
            Pair[e[4], k[3]]*(2*(T14 + T24)*Pair[e[5], k[1]] - 
              2*T14*Pair[e[5], k[3]] + (T - T14 - T24 - U)*Pair[e[5], 
                k[4]])))*Pair[ec[1], ec[2]] - 
        4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], 
                k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*Pair[e[4], k[1]] + 
               (-MH2 + S34)*Pair[e[4], k[2]] + (T14 + T24)*Pair[e[4], k[3]])*
              Pair[ec[2], e[5]])) + (MH2 - S34)*(Pair[e[5], k[1]] + 
            Pair[e[5], k[2]])*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
            (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*
             Pair[ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
           (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
              Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*
             Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[
                ec[2], k[5]])))) + 
      4*(Den[T, 0]*(-2*((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (-(((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
              Pair[e[5], k[4]])*Pair[ec[1], ec[2]]) - 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 2*Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S34 - T14 - T24, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]]))) + 
        Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S, 0]*
         (((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) + Den[MH2 - S - T24 - U, 0]*
       (-8*Den[T14, 0]*(-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*
           Pair[e[5], k[2]]*Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]])))))))/3 - 
   (23*Alfas*GH*Pi*(8*(Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] - 2*Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[U, 0]*
       (-4*(Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) + Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]])) + 4*(Den[MH2 - S34 - T14 - T24, 0]*
           (((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[e[4], e[5]] + 
              4*(MH2 - U)*(Pair[e[4], k[5]]*Pair[e[5], k[1]] - 
                Pair[e[4], k[1]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
            4*(MH2 - U)*Pair[e[5], k[4]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*(MH2 - U)*
             Pair[e[4], k[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*
             Pair[ec[2], e[5]] + 8*((-(Pair[e[4], k[5]]*Pair[e[5], k[1]]) + 
                Pair[e[4], k[1]]*Pair[e[5], k[4]])*Pair[ec[1], k[3]] - 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*
                 Pair[e[5], k[4]])*(Pair[ec[1], k[4]] + Pair[ec[1], k[5]]))*
             Pair[ec[2], k[3]] - 4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
             ((-MH2 + U)*Pair[ec[2], k[1]] + S*Pair[ec[2], k[3]]) + 
            4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                k[1]] + S*Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
             (2*Pair[ec[1], k[4]]*((MH2 - U)*Pair[ec[2], k[1]] - 
                S*Pair[ec[2], k[3]]) + 2*Pair[ec[1], k[3]]*(
                T24*Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]]) - 
              Pair[ec[1], k[2]]*((MH2 - S34 + T14 + T24 - U)*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[ec[2], k[4]]))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]])))))) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*Den[S, 0]*(((MH2 - S - T14 - T24)*(T14 - T24)*Pair[e[4], e[5]] + 
            2*((-T14 + T24)*Pair[e[4], k[3]]*Pair[e[5], k[3]] + 
              Pair[e[4], k[1]]*(2*(-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (S - S34 - T + U)*Pair[e[5], k[3]]) + Pair[e[4], k[2]]*(
                2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + (-S + S34 - T + U)*
                 Pair[e[5], k[3]])))*Pair[ec[1], ec[2]] + 
          4*(Pair[ec[1], k[2]]*(((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
                (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[
                ec[2], e[5]]) - ((-MH2 + S + T + U)*Pair[e[5], k[3]] + 
              (-MH2 + S34 + T + U)*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] - (MH2 - S34 - T - U)*(Pair[e[4], k[1]] + 
              Pair[e[4], k[2]])*Pair[ec[1], e[5]]*Pair[ec[2], k[1]] - 
            (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[1], k[4]]*Pair[
                ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[ec[2], k[4]]) + 
            2*Pair[e[5], k[3]]*(-((Pair[e[4], k[1]] + Pair[e[4], k[2]])*
                Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - Pair[e[4], k[3]]*Pair[
                ec[1], k[4]]*Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(
                (Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[3]] + 
                Pair[e[4], k[3]]*Pair[ec[2], k[4]])))) + 
        4*(((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Den[T24, 0]*
           (Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(Pair[e[4], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], k[4]])) - 
            2*(-2*Pair[e[4], k[2]]*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) - 2*Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) + Den[MH2 - S34 - T14 - T24, 0]*
       (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
           Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
          2*(Pair[e[4], k[5]]*((MH2 - S - U)*Pair[e[5], k[1]] + 
              (-MH2 + S + T)*Pair[e[5], k[2]]) + 
            ((-MH2 + S + U)*Pair[e[4], k[1]] + (MH2 - S - T)*Pair[e[4], 
                k[2]])*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[5], k[4]]*Pair[ec[1], k[2]]*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*
           Pair[ec[1], k[2]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + 2*S + T + U)*
           Pair[e[5], k[4]]*Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
          2*(-2*MH2 + 2*S + T + U)*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[1]] - 4*(Pair[e[4], k[3]]*(Pair[e[5], k[1]] + 
              Pair[e[5], k[2]]) - (Pair[e[4], k[1]] + Pair[e[4], k[2]])*
             Pair[e[5], k[3]])*(Pair[ec[1], k[3]]*Pair[ec[2], k[1]] - 
            Pair[ec[1], k[2]]*Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (-((T14 + T24)*Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) + 
            (-2*MH2 + 2*S + T + U)*Pair[ec[1], k[4]]*Pair[ec[2], k[1]] + 
            Pair[ec[1], k[2]]*((T14 + T24)*Pair[ec[2], k[3]] + 
              (2*MH2 - 2*S - T - U)*Pair[ec[2], k[4]]))) - 
        4*Den[T, 0]*(((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[e[4], e[5]] + 
            4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[e[5], k[2]] - 
              Pair[e[4], k[2]]*Pair[e[5], k[4]]))*Pair[ec[1], ec[2]] - 
          4*Pair[e[5], k[4]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
            S*Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           ((-MH2 + T)*Pair[ec[1], k[2]] + S*Pair[ec[1], k[3]])*
           Pair[ec[2], e[5]] - 4*(MH2 - T)*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*
           (Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) + 4*(MH2 - T)*
           Pair[e[4], k[5]]*Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) + 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[3]]*((MH2 - S34 - T + T14 + T24)*Pair[ec[2], 
                k[1]] - 2*T14*Pair[ec[2], k[3]] + 2*S*Pair[ec[2], k[4]]) + 
            2*(MH2 - T)*(Pair[ec[1], k[4]]*(Pair[ec[2], k[1]] - Pair[ec[2], 
                 k[3]]) - Pair[ec[1], k[2]]*Pair[ec[2], k[4]])) + 
          8*Pair[ec[1], k[3]]*((-(Pair[e[4], k[5]]*Pair[e[5], k[2]]) + 
              Pair[e[4], k[2]]*Pair[e[5], k[4]])*Pair[ec[2], k[3]] - 
            (Pair[e[4], k[5]]*Pair[e[5], k[3]] - Pair[e[4], k[3]]*Pair[e[5], 
                k[4]])*(Pair[ec[2], k[4]] + Pair[ec[2], k[5]]))) + 
        4*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            4*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) + 
              (Pair[e[4], k[1]] - Pair[e[4], k[2]])*Pair[e[5], k[4]]))*
           Pair[ec[1], ec[2]] - 4*Pair[e[5], k[4]]*(2*Pair[ec[1], k[2]] - 
            Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 4*Pair[e[4], k[5]]*
           (2*Pair[ec[1], k[2]] - Pair[ec[1], k[3]])*Pair[ec[2], e[5]] + 
          4*Pair[e[5], k[4]]*Pair[ec[1], e[4]]*(2*Pair[ec[2], k[1]] - 
            Pair[ec[2], k[3]]) - 4*Pair[e[4], k[5]]*Pair[ec[1], e[5]]*
           (2*Pair[ec[2], k[1]] - Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           (Pair[ec[1], k[5]]*(-4*Pair[ec[2], k[1]] + 2*Pair[ec[2], k[3]]) - 
            Pair[ec[1], k[3]]*(Pair[ec[2], k[1]] + 2*Pair[ec[2], k[5]]) + 
            Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 4*Pair[ec[2], k[5]])))) + 
      Den[T24, 0]*(8*Den[MH2 - S - T - T14, 0]*(-2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], k[2]]*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
          (MH2*(S - T14) - S*(S34 + T24) + T14*(T24 + U))*Pair[ec[1], e[5]]*
           Pair[ec[2], e[4]] + 2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*
             Pair[ec[1], k[2]] + (-MH2 + T24 + U)*Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + 
              (-MH2 + T24 + U)*Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*
           Pair[ec[1], k[5]]*Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[5], k[1]]*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], e[5]]*Pair[ec[1], k[5]]*
           Pair[ec[2], k[4]] + 2*Pair[ec[1], e[5]]*
           (Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + U)*Pair[ec[2], k[1]] + 
              (S + T14)*Pair[ec[2], k[3]]) + ((-2*MH2 + S34 + 2*T24 + U)*Pair[
                e[4], k[1]] + (S + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
              k[4]]) - 4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - 
            Pair[e[5], k[1]]*Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - 
              Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           ((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
            Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]]))) - 
        4*Den[T, 0]*(Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*
              Pair[ec[2], e[4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], 
                k[4]] + Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) + 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      4*(Den[T, 0]*(-(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
              Pair[ec[1], k[3]])*Pair[ec[2], e[4]]) + 
          2*((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[S34, 0]*
         (((MH2 - S34)*Pair[e[4], e[5]] + 2*Pair[e[4], k[3]]*
             Pair[e[5], k[4]])*Pair[ec[1], ec[2]] - 
          2*((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]])) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], k[5]]))) + 
        Den[S, 0]*(((-T - 2*T14 + 2*T24 + U)*Pair[e[4], e[5]] - 
            2*(Pair[e[4], k[5]]*(-Pair[e[5], k[1]] + Pair[e[5], k[2]]) - 
              Pair[e[4], k[2]]*(2*Pair[e[5], k[1]] + Pair[e[5], k[4]]) + 
              Pair[e[4], k[1]]*(2*Pair[e[5], k[2]] + Pair[e[5], k[4]])))*
           Pair[ec[1], ec[2]] - 4*(Pair[ec[1], k[2]]*((Pair[e[5], k[3]] + 
                2*Pair[e[5], k[4]])*Pair[ec[2], e[4]] - (Pair[e[4], k[3]] + 
                2*Pair[e[4], k[5]])*Pair[ec[2], e[5]]) - 
            (Pair[e[5], k[3]] + 2*Pair[e[5], k[4]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[1]] + (Pair[e[4], k[3]] + 2*Pair[e[4], k[5]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[1]] + Pair[e[4], e[5]]*
             (-(Pair[ec[1], k[3]]*Pair[ec[2], k[1]]) - 2*Pair[ec[1], k[5]]*
               Pair[ec[2], k[1]] + Pair[ec[1], k[2]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]]))))) - 4*Den[S34, 0]*
       (Den[S, 0]*(((MH2 - S34)*(T + T14 - T24 - U)*Pair[e[4], e[5]] + 
            2*(2*(-MH2 + S34)*Pair[e[4], k[2]]*Pair[e[5], k[1]] - 
              2*(MH2 - S34)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                Pair[e[5], k[3]] - Pair[e[5], k[4]]) + Pair[e[4], k[3]]*(
                2*(T14 + T24)*Pair[e[5], k[1]] - 2*T14*Pair[e[5], k[3]] + 
                (T - T14 - T24 - U)*Pair[e[5], k[4]])))*Pair[ec[1], ec[2]] - 
          4*(-(Pair[ec[1], k[2]]*((MH2 - S34)*(Pair[e[5], k[1]] + 
                 Pair[e[5], k[2]])*Pair[ec[2], e[4]] + ((-MH2 + S34)*
                  Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
                 (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[2], e[5]])) + 
            (MH2 - S34)*(Pair[e[5], k[1]] + Pair[e[5], k[2]])*
             Pair[ec[1], e[4]]*Pair[ec[2], k[1]] + 
            ((-MH2 + S34)*Pair[e[4], k[1]] + (-MH2 + S34)*Pair[e[4], k[2]] + 
              (T14 + T24)*Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[1]] + (MH2 - S34)*Pair[e[4], e[5]]*
             (Pair[ec[1], k[5]]*Pair[ec[2], k[1]] - Pair[ec[1], k[2]]*Pair[
                ec[2], k[5]]) + 2*Pair[e[4], k[3]]*
             (-((Pair[e[5], k[1]] + Pair[e[5], k[2]])*Pair[ec[1], k[3]]*
                Pair[ec[2], k[1]]) - Pair[e[5], k[3]]*Pair[ec[1], k[5]]*Pair[
                ec[2], k[1]] + Pair[ec[1], k[2]]*((Pair[e[5], k[1]] + 
                  Pair[e[5], k[2]])*Pair[ec[2], k[3]] + Pair[e[5], k[3]]*
                 Pair[ec[2], k[5]])))) + Den[MH2 - S - T - T14, 0]*
         (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[ec[2], e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*Pair[
                ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
              T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
            (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
              Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
            Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*Pair[
                e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
            (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*
             (Pair[ec[2], k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*
             (Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
              Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]))))))))/6 + 
   (2*Alfas*GH*Pi*(8*(-2*Pair[e[4], e[5]]*Pair[ec[1], ec[2]] + 
        Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
         Pair[ec[2], e[5]]) + Den[2*MH2 - S34 - T - U, 0]*
       (-4*(Den[T24, 0]*(Pair[ec[1], e[5]]*(-((S - T14)*(-MH2 + S + T14 + 
                 T24)*Pair[ec[2], e[4]]) + 4*(MH2 - S34 - T - U)*(
                Pair[e[4], k[2]]*Pair[ec[2], k[1]] + Pair[e[4], k[1]]*
                 Pair[ec[2], k[4]])) - 2*(-2*Pair[e[4], k[2]]*(
                (-MH2 + S34 + T + U)*Pair[e[5], k[1]] + (MH2 - S34 - T24 - U)*
                 Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (((-S34 + T - T24 + U)*Pair[e[5], k[3]] + 2*(-MH2 + S34 + T + 
                    U)*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + (S - T14)*
                 Pair[e[5], k[3]]*Pair[ec[1], k[3]] + (2*(MH2 - S34 - T - U)*
                   Pair[e[5], k[2]] + (S34 + T - T24 - U)*Pair[e[5], k[3]])*
                 Pair[ec[1], k[4]])*Pair[ec[2], e[4]] + 2*(MH2 - S34 - T - 
                U)*Pair[e[4], k[2]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
               Pair[ec[2], e[5]] - 2*((-MH2 + S34 + T + U)*Pair[e[5], k[1]] + 
                (MH2 - S34 - T24 - U)*Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
               Pair[ec[2], k[4]] + 2*(MH2 - S34 - T - U)*Pair[e[4], e[5]]*(
                Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*Pair[ec[2], k[4]] + 
              4*Pair[e[5], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*
                    Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                     k[4]])) + (Pair[ec[1], k[2]] - Pair[ec[1], k[4]])*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   Pair[ec[2], k[4]])))) + Den[T14, 0]*
           (Pair[ec[1], e[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*
                Pair[ec[2], e[5]]) - 2*(((-S34 + T - T14 + U)*Pair[e[5], 
                    k[3]] + 2*(-MH2 + S34 + T + U)*Pair[e[5], k[4]])*
                 Pair[ec[2], k[1]] + (S - T24)*Pair[e[5], k[3]]*Pair[ec[2], 
                  k[3]] + (2*(MH2 - S34 - T - U)*Pair[e[5], k[1]] + 
                  (S34 - T - T14 + U)*Pair[e[5], k[3]])*Pair[ec[2], k[4]])) + 
            4*(Pair[e[4], k[1]]*((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              (MH2 - S34 - T - U)*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[4], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
              Pair[ec[1], k[4]]*(((-MH2 + S34 + T + U)*Pair[e[5], k[2]] + 
                  (MH2 - S34 - T - T14)*Pair[e[5], k[3]])*Pair[ec[2], e[4]] - 
                (MH2 - S34 - T - U)*Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - 
                  Pair[ec[2], k[4]])) - (MH2 - S34 - T - U)*Pair[e[4], k[1]]*
               Pair[ec[1], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[4]]) + 
              2*Pair[e[5], k[3]]*(Pair[e[4], k[1]]*Pair[ec[1], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[4], k[1]]*Pair[ec[1], k[3]]*
                 (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*
                 (Pair[e[4], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
                   (-Pair[ec[2], k[1]] + Pair[ec[2], k[4]])))))) + 
        4*(-2*((-MH2 + S34 + T + U)*Pair[e[4], e[5]] + 2*Pair[e[4], k[5]]*
             Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
          ((-MH2 + S34 + T + U)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + Pair[ec[1], e[4]]*
           ((-MH2 + S34 + T + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[3]]*
             Pair[ec[2], k[5]]))) - 
      4*(Den[T, 0]*(((-MH2 + T)*Pair[ec[1], e[5]] + 2*Pair[e[5], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[4]] + 
          ((-MH2 + T)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[1]]*
             Pair[ec[1], k[3]])*Pair[ec[2], e[5]] - 2*Pair[e[4], e[5]]*
           ((-MH2 + T)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[3]]*
             Pair[ec[2], k[1]])) + Den[U, 0]*
         (Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], e[4]] + 
            2*Pair[e[4], k[2]]*Pair[ec[2], k[3]]) + Pair[ec[1], e[4]]*
           ((-MH2 + U)*Pair[ec[2], e[5]] + 2*Pair[e[5], k[2]]*
             Pair[ec[2], k[3]]) - 2*Pair[e[4], e[5]]*
           ((-MH2 + U)*Pair[ec[1], ec[2]] + 2*Pair[ec[1], k[2]]*
             Pair[ec[2], k[3]]) + Den[T14, 0]*(Pair[ec[1], e[4]]*
             ((S - S34 + T - T24)*(MH2 - U)*Pair[ec[2], e[5]] + 
              2*(-2*(MH2 - U)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                  Pair[e[5], k[3]])*Pair[ec[2], k[1]] + 
                (2*(S + T24)*Pair[e[5], k[1]] + (S + S34 - T + T24)*
                   Pair[e[5], k[2]] - 2*S*Pair[e[5], k[3]])*Pair[ec[2], 
                  k[3]] + 2*(MH2 - U)*Pair[e[5], k[1]]*Pair[ec[2], k[4]])) - 
            4*((MH2 - U)*Pair[e[4], k[1]]*(Pair[e[5], k[1]] - Pair[e[5], 
                 k[4]])*Pair[ec[1], ec[2]] + (MH2 - U)*(Pair[e[4], k[5]]*
                 Pair[ec[1], k[4]] + Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[
                ec[2], e[5]] - 2*(Pair[e[4], k[1]]*(Pair[e[5], k[1]] - 
                  Pair[e[5], k[4]])*Pair[ec[1], k[3]] + (Pair[e[4], k[5]]*
                   Pair[e[5], k[3]] + Pair[e[4], k[3]]*(Pair[e[5], k[1]] - 
                    Pair[e[5], k[4]]))*Pair[ec[1], k[4]] + Pair[e[4], k[1]]*
                 Pair[e[5], k[3]]*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] + 
              Pair[e[4], k[1]]*Pair[ec[1], e[5]]*((-MH2 + U)*Pair[ec[2], 
                  k[1]] + (S + T24)*Pair[ec[2], k[3]] + (MH2 - U)*
                 Pair[ec[2], k[4]]) + Pair[ec[1], k[4]]*((MH2 - U)*
                 (Pair[e[5], k[1]] - Pair[e[5], k[4]])*Pair[ec[2], e[4]] + 
                Pair[e[4], e[5]]*((-MH2 + U)*Pair[ec[2], k[1]] + (S + T24)*
                   Pair[ec[2], k[3]] + (MH2 - U)*Pair[ec[2], k[4]])))) + 
          Den[MH2 - S - T - T14, 0]*(-4*((MH2 - U)*(Pair[e[4], k[1]] - 
                Pair[e[4], k[5]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - U)*(Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
                Pair[e[5], k[4]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
              2*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[e[5], k[1]]*
                 Pair[ec[1], k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[1]]*
                 Pair[ec[1], k[4]] + (Pair[e[4], k[1]]*Pair[e[5], k[3]] - 
                  Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                   Pair[e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], k[3]] - 
              Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(T24*Pair[ec[2], k[3]] + 
                (MH2 - U)*Pair[ec[2], k[4]]) + Pair[ec[1], k[5]]*(
                (MH2 - U)*(Pair[e[4], k[1]] - Pair[e[4], k[5]])*Pair[ec[2], 
                  e[5]] - Pair[e[4], e[5]]*(T24*Pair[ec[2], k[3]] + 
                  (MH2 - U)*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
             ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[ec[2], e[4]] + 
              2*((-2*T24*Pair[e[4], k[1]] + (MH2 + S - T + T14 - U)*
                   Pair[e[4], k[2]] - 2*S*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[3]] - 2*(MH2 - U)*((Pair[e[4], k[2]] - Pair[e[4], k[3]])*
                   Pair[ec[2], k[1]] + Pair[e[4], k[1]]*Pair[ec[2], 
                    k[4]]))))) + Den[MH2 - S - T - T14, 0]*
         (4*(-((Pair[e[5], k[1]]*Pair[ec[1], k[2]] + Pair[e[5], k[1]]*
                Pair[ec[1], k[4]] + (Pair[e[5], k[2]] + Pair[e[5], k[4]])*
                Pair[ec[1], k[5]])*Pair[ec[2], e[4]]) + Pair[ec[1], k[5]]*
             ((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[ec[2], e[5]] + 
              Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], k[4]])) + 
            Pair[e[5], k[1]]*((2*Pair[e[4], k[2]] - Pair[e[4], k[3]])*Pair[
                ec[1], ec[2]] + Pair[ec[1], e[4]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[4]]))) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            2*((Pair[e[4], k[2]] + 2*Pair[e[4], k[5]])*Pair[ec[2], k[1]] + 
              (Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              (-2*Pair[e[4], k[1]] + Pair[e[4], k[2]])*Pair[ec[2], 
                k[5]])))) + Den[T24, 0]*(-8*Den[MH2 - S - T - T14, 0]*
         (-2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[e[5], k[1]]*
           Pair[ec[1], ec[2]] + (MH2*(S - T14) - S*(S34 + T24) + 
            T14*(T24 + U))*Pair[ec[1], e[5]]*Pair[ec[2], e[4]] + 
          2*((-MH2 + S34 + T24)*Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
            (-MH2 + T24 + U)*Pair[e[5], k[1]]*Pair[ec[1], k[4]] + 
            ((-MH2 + S34 + T24)*Pair[e[5], k[2]] + (-MH2 + T24 + U)*Pair[
                e[5], k[4]])*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
          2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[2]]*Pair[ec[1], k[5]]*
           Pair[ec[2], e[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[5], k[1]]*
           Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 2*(-2*MH2 + S34 + 2*T24 + U)*
           Pair[e[4], e[5]]*Pair[ec[1], k[5]]*Pair[ec[2], k[4]] + 
          2*Pair[ec[1], e[5]]*(Pair[e[4], k[2]]*((-2*MH2 + S34 + 2*T24 + 
                U)*Pair[ec[2], k[1]] + (S + T14)*Pair[ec[2], k[3]]) + 
            ((-2*MH2 + S34 + 2*T24 + U)*Pair[e[4], k[1]] + (S + T14)*Pair[
                e[4], k[3]])*Pair[ec[2], k[4]]) - 
          4*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] - Pair[e[5], k[1]]*
             Pair[ec[1], k[4]] + (Pair[e[5], k[2]] - Pair[e[5], k[4]])*
             Pair[ec[1], k[5]])*((Pair[e[4], k[1]] - Pair[e[4], k[5]])*
             Pair[ec[2], k[4]] + Pair[e[4], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[5]]))) + 4*Den[T, 0]*
         (Pair[ec[1], e[5]]*(-((MH2 - T)*(S - S34 - T14 + U)*Pair[ec[2], e[
                4]]) + 4*(MH2 - T)*(Pair[e[4], k[5]]*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[ec[2], k[5]])) - 
          2*(2*(MH2 - T)*Pair[e[4], k[2]]*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
            (-2*(MH2 - T)*(Pair[e[5], k[1]] + Pair[e[5], k[2]] - 
                Pair[e[5], k[3]])*Pair[ec[1], k[2]] + ((S + S34 + T14 - U)*
                 Pair[e[5], k[1]] + 2*((S + T14)*Pair[e[5], k[2]] - 
                  S*Pair[e[5], k[3]]))*Pair[ec[1], k[3]] + 2*(MH2 - T)*Pair[
                e[5], k[2]]*Pair[ec[1], k[4]])*Pair[ec[2], e[4]] - 
            2*Pair[e[4], k[2]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], e[5]] + 2*(MH2 - T)*(Pair[e[5], k[1]] - 
              Pair[e[5], k[3]])*Pair[ec[1], e[4]]*Pair[ec[2], k[4]] - 
            2*Pair[e[4], e[5]]*((-MH2 + T)*Pair[ec[1], k[2]] + 
              (S + T14)*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
             Pair[ec[2], k[4]] + 4*Pair[ec[1], k[3]]*(Pair[e[4], k[2]]*(
                Pair[e[5], k[2]] - Pair[e[5], k[4]])*Pair[ec[2], k[3]] + 
              (Pair[e[4], k[5]]*Pair[e[5], k[3]] + Pair[e[4], k[3]]*
                 (Pair[e[5], k[2]] - Pair[e[5], k[4]]))*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*Pair[e[5], k[3]]*Pair[ec[2], k[5]]))) - 
        4*(-2*((Pair[e[5], k[1]] + 2*Pair[e[5], k[4]])*Pair[ec[1], k[2]] + 
            (Pair[e[5], k[1]] - 2*Pair[e[5], k[2]])*Pair[ec[1], k[4]] + 
            (Pair[e[5], k[2]] + Pair[e[5], k[4]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[4]] + 4*(Pair[e[4], k[2]]*((2*Pair[e[5], k[1]] - 
                Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            (2*Pair[e[5], k[1]] - Pair[e[5], k[3]])*Pair[ec[1], e[4]]*
             Pair[ec[2], k[4]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[5]])*Pair[ec[2], k[4]]) + Pair[ec[1], e[5]]*
           ((2*S - S34 - 2*T14 + U)*Pair[ec[2], e[4]] - 
            4*((Pair[e[4], k[1]] + Pair[e[4], k[5]])*Pair[ec[2], k[4]] + 
              Pair[e[4], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[5]]))))) + 
      Den[MH2 - S - T24 - U, 0]*(-8*Den[T14, 0]*
         (-2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[e[5], k[2]]*
           Pair[ec[1], ec[2]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[5], k[2]]*
           Pair[ec[1], k[4]]*Pair[ec[2], e[4]] + 
          (-(S*(S34 + T14)) + MH2*(S - T24) + (T + T14)*T24)*
           Pair[ec[1], e[4]]*Pair[ec[2], e[5]] + 
          2*((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
            (S + T24)*Pair[e[4], k[1]]*Pair[ec[1], k[3]] + 
            ((-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[2]] + (S + T24)*Pair[
                e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] - 
          2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], k[1]]*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] - 2*(-2*MH2 + S34 + T + 2*T14)*Pair[e[4], e[5]]*
           Pair[ec[1], k[4]]*Pair[ec[2], k[5]] - 
          4*(Pair[e[4], k[1]]*Pair[ec[1], k[2]] + (Pair[e[4], k[2]] - 
              Pair[e[4], k[5]])*Pair[ec[1], k[4]] - Pair[e[4], k[1]]*
             Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] - 
              Pair[ec[2], k[4]]) + (Pair[e[5], k[1]] - Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + 2*Pair[ec[1], e[4]]*
           (Pair[e[5], k[2]]*((-MH2 + S34 + T14)*Pair[ec[2], k[1]] + 
              (-MH2 + T + T14)*Pair[ec[2], k[4]]) + 
            ((-MH2 + S34 + T14)*Pair[e[5], k[1]] + (-MH2 + T + T14)*Pair[
                e[5], k[4]])*Pair[ec[2], k[5]])) - 
        4*Den[T, 0]*(4*(MH2 - T)*(Pair[e[4], k[1]] - Pair[e[4], k[3]])*
           Pair[e[5], k[2]]*Pair[ec[1], ec[2]] + 4*Pair[e[5], k[2]]*
           (T14*Pair[ec[1], k[3]] + (MH2 - T)*Pair[ec[1], k[4]])*
           Pair[ec[2], e[4]] + 2*(-2*(MH2 - T)*(Pair[e[4], k[1]] - 
              Pair[e[4], k[3]])*Pair[ec[1], k[2]] + 
            ((MH2 + S - T + T24 - U)*Pair[e[4], k[1]] - 
              2*(T14*Pair[e[4], k[2]] + S*Pair[e[4], k[3]]))*
             Pair[ec[1], k[3]] + 2*(-MH2 + T)*Pair[e[4], k[2]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 4*(MH2 - T)*
           (Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
           Pair[ec[2], k[5]] + 4*Pair[e[4], e[5]]*(T14*Pair[ec[1], k[3]] + 
            (MH2 - T)*Pair[ec[1], k[4]])*Pair[ec[2], k[5]] + 
          8*Pair[ec[1], k[3]]*((Pair[e[4], k[2]] - Pair[e[4], k[5]])*
             Pair[e[5], k[2]]*Pair[ec[2], k[3]] + Pair[e[4], k[3]]*
             Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
            (Pair[e[4], k[2]]*Pair[e[5], k[3]] - Pair[e[4], k[5]]*Pair[e[5], 
                k[3]] + Pair[e[4], k[3]]*Pair[e[5], k[4]])*
             Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((MH2 - T)*(MH2 - S34 - T14 - 2*T24)*Pair[ec[2], e[5]] - 
            4*(MH2 - T)*(Pair[e[5], k[2]]*Pair[ec[2], k[4]] + 
              Pair[e[5], k[4]]*Pair[ec[2], k[5]]))) + 
        4*(2*((Pair[e[4], k[1]] + 2*Pair[e[4], k[5]])*Pair[ec[1], k[2]] + 
            (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
            (Pair[e[4], k[1]] - 2*Pair[e[4], k[2]])*Pair[ec[1], k[5]])*
           Pair[ec[2], e[5]] - 4*(Pair[e[5], k[2]]*((2*Pair[e[4], k[1]] - 
                Pair[e[4], k[3]])*Pair[ec[1], ec[2]] + (Pair[ec[1], k[3]] + 
                2*Pair[ec[1], k[4]])*Pair[ec[2], e[4]]) + 
            (2*Pair[e[4], k[1]] - Pair[e[4], k[3]])*Pair[ec[1], e[5]]*
             Pair[ec[2], k[5]] + Pair[e[4], e[5]]*(Pair[ec[1], k[3]] + 
              2*Pair[ec[1], k[4]])*Pair[ec[2], k[5]]) + Pair[ec[1], e[4]]*
           ((-2*S + S34 - T + 2*T24)*Pair[ec[2], e[5]] + 
            4*(Pair[e[5], k[2]]*(Pair[ec[2], k[1]] + Pair[ec[2], k[4]]) + 
              (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[ec[2], k[5]])) + 
          Den[S34, 0]*(4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], 
                k[3]])*Pair[e[5], k[2]]*Pair[ec[1], ec[2]] - 
            4*(MH2 - S34)*Pair[e[5], k[2]]*(Pair[ec[1], k[2]] - 
              Pair[ec[1], k[5]])*Pair[ec[2], e[4]] - 
            2*((-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
               Pair[ec[1], k[2]] + (-2*(MH2 - S34)*Pair[e[4], k[2]] + 
                2*T24*Pair[e[4], k[3]])*Pair[ec[1], k[3]] + 
              (-2*(MH2 - S34)*Pair[e[4], k[2]] + (MH2 + S - S34 + T24 - U)*
                 Pair[e[4], k[3]])*Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + 
            4*((-MH2 + S34)*Pair[e[4], k[1]] + T14*Pair[e[4], k[3]])*
             Pair[ec[1], e[5]]*Pair[ec[2], k[5]] - 4*(MH2 - S34)*
             Pair[e[4], e[5]]*(Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*
             Pair[ec[2], k[5]] + Pair[ec[1], e[4]]*((MH2 - S34)*(MH2 - 2*S - 
                T - T14)*Pair[ec[2], e[5]] + 4*(MH2 - S34)*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
            8*Pair[e[4], k[3]]*(-(Pair[ec[1], k[3]]*(Pair[e[5], k[2]]*
                  Pair[ec[2], k[1]] + Pair[e[5], k[1]]*Pair[ec[2], k[5]])) + 
              (Pair[ec[1], k[2]] - Pair[ec[1], k[5]])*(Pair[e[5], k[2]]*
                 Pair[ec[2], k[3]] + Pair[e[5], k[3]]*Pair[ec[2], 
                  k[5]]))))) + 
      4*(Den[T14, 0]*(Pair[ec[1], e[4]]*((-2*S + S34 - T + 2*T24)*
             Pair[ec[2], e[5]] + 2*((Pair[e[5], k[2]] + 2*Pair[e[5], k[4]])*
               Pair[ec[2], k[1]] + (-2*Pair[e[5], k[1]] + Pair[e[5], k[2]])*
               Pair[ec[2], k[4]] + (Pair[e[5], k[1]] + Pair[e[5], k[4]])*Pair[
                ec[2], k[5]])) - 4*(-((Pair[e[4], k[1]]*Pair[ec[1], k[2]] + 
               (Pair[e[4], k[2]] + Pair[e[4], k[5]])*Pair[ec[1], k[4]] + 
               Pair[e[4], k[1]]*Pair[ec[1], k[5]])*Pair[ec[2], e[5]]) + 
            Pair[ec[1], k[4]]*((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[
                ec[2], e[4]] + Pair[e[4], e[5]]*(Pair[ec[2], k[3]] + 
                2*Pair[ec[2], k[5]])) + Pair[e[4], k[1]]*
             ((2*Pair[e[5], k[2]] - Pair[e[5], k[3]])*Pair[ec[1], ec[2]] + 
              Pair[ec[1], e[5]]*(Pair[ec[2], k[3]] + 2*Pair[ec[2], 
                  k[5]])))) + Den[S34, 0]*(-2*((MH2 - S34)*Pair[e[4], e[5]] + 
            2*Pair[e[4], k[3]]*Pair[e[5], k[4]])*Pair[ec[1], ec[2]] + 
          ((MH2 - S34)*Pair[ec[1], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[1], k[4]])*Pair[ec[2], e[5]] + Pair[ec[1], e[5]]*
           ((MH2 - S34)*Pair[ec[2], e[4]] + 2*Pair[e[4], k[3]]*
             Pair[ec[2], k[4]]) + Den[MH2 - S - T - T14, 0]*
           (Pair[ec[1], e[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*Pair[ec[2], 
                e[4]] - 2*(2*((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], k[1]] + 
                (-2*(MH2 - S34)*Pair[e[4], k[1]] + 2*T14*Pair[e[4], k[3]])*
                 Pair[ec[2], k[3]] + (-2*(MH2 - S34)*Pair[e[4], k[1]] + 
                  (MH2 + S - S34 - T + T14)*Pair[e[4], k[3]])*Pair[ec[2], 
                  k[4]])) + 4*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                T24*Pair[e[4], k[3]])*Pair[e[5], k[1]]*Pair[ec[1], ec[2]] + 
              (MH2 - S34)*(Pair[e[5], k[1]]*Pair[ec[1], k[2]] + 
                Pair[e[5], k[2]]*Pair[ec[1], k[5]])*Pair[ec[2], e[4]] + 
              Pair[ec[1], k[5]]*(((-MH2 + S34)*Pair[e[4], k[2]] + 
                  T24*Pair[e[4], k[3]])*Pair[ec[2], e[5]] - (MH2 - S34)*
                 Pair[e[4], e[5]]*(Pair[ec[2], k[1]] - Pair[ec[2], k[5]])) - 
              (MH2 - S34)*Pair[e[5], k[1]]*Pair[ec[1], e[4]]*(Pair[ec[2], 
                 k[1]] - Pair[ec[2], k[5]]) - 2*Pair[e[4], k[3]]*(
                Pair[e[5], k[1]]*Pair[ec[1], k[2]]*Pair[ec[2], k[3]] + 
                Pair[e[5], k[1]]*Pair[ec[1], k[3]]*(-Pair[ec[2], k[1]] + 
                  Pair[ec[2], k[5]]) + Pair[ec[1], k[5]]*(Pair[e[5], k[2]]*
                   Pair[ec[2], k[3]] + Pair[e[5], k[3]]*(-Pair[ec[2], k[1]] + 
                    Pair[ec[2], k[5]])))))))))/3)*
  (8*(Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
      Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
   Den[T24, 0]*(-4*Den[T, 0]*(Pair[e[1], ec[5]]*
        (-((MH2 - T)*(S - S34 - T14 + U)*Pair[e[2], ec[4]]) + 
         4*(MH2 - T)*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*Pair[ec[4], k[5]])) - 
       2*(-2*((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
           (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
         2*((-MH2 + T)*Pair[e[1], k[2]] + (S + T14)*Pair[e[1], k[3]] + 
           (MH2 - T)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
         2*(MH2 - T)*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]]) + 2*(MH2 - T)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[3]]) + 
         Pair[e[2], ec[4]]*(2*(MH2 - T)*Pair[e[1], k[4]]*Pair[ec[5], k[2]] - 
           2*(MH2 - T)*Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]] - Pair[ec[5], k[3]]) + Pair[e[1], k[3]]*
            ((S + S34 + T14 - U)*Pair[ec[5], k[1]] + 
             2*((S + T14)*Pair[ec[5], k[2]] - S*Pair[ec[5], k[3]]))) + 
         4*Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]]*
            Pair[ec[5], k[3]] + Pair[e[2], k[4]]*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*(Pair[ec[5], k[2]] - Pair[
                ec[5], k[4]])) + Pair[e[2], k[3]]*Pair[ec[4], k[2]]*
            (Pair[ec[5], k[2]] - Pair[ec[5], k[4]])))) + 
     4*(Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
         4*((Pair[e[2], k[1]] + Pair[e[2], k[5]])*Pair[ec[4], k[2]] + 
           Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]))) + 
       4*((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], k[4]]*
          Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
          ((Pair[e[1], k[3]] + 2*Pair[e[1], k[5]])*Pair[e[2], ec[5]] + 
           Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
           Pair[ec[5], k[3]])) - 2*Pair[e[2], ec[4]]*
        (Pair[e[1], k[4]]*(Pair[ec[5], k[1]] - 2*Pair[ec[5], k[2]]) + 
         Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + Pair[ec[5], k[4]]) + 
         Pair[e[1], k[2]]*(Pair[ec[5], k[1]] + 2*Pair[ec[5], k[4]]))) + 
     8*Den[MH2 - S - T - T14, 0]*((MH2*(S - T14) - S*(S34 + T24) + 
         T14*(T24 + U))*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] - 
       2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], k[5]]*Pair[e[2], k[4]]*
        Pair[ec[4], ec[5]] - 2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], k[5]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 2*Pair[e[1], ec[5]]*
        (((-2*MH2 + S34 + 2*T24 + U)*Pair[e[2], k[1]] + 
           (S + T14)*Pair[e[2], k[3]])*Pair[ec[4], k[2]] + 
         Pair[e[2], k[4]]*((-2*MH2 + S34 + 2*T24 + U)*Pair[ec[4], k[1]] + 
           (S + T14)*Pair[ec[4], k[3]])) - 2*(-2*MH2 + S34 + 2*T24 + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] - 
       2*(-2*MH2 + S34 + 2*T24 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
        Pair[ec[5], k[1]] - 4*((Pair[e[2], k[1]] - Pair[e[2], k[5]])*
          Pair[ec[4], k[2]] + Pair[e[2], k[4]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[5]]))*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] - 
         Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
          (Pair[ec[5], k[2]] - Pair[ec[5], k[4]])) + 2*Pair[e[2], ec[4]]*
        ((-MH2 + S34 + T24)*Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
         (-MH2 + T24 + U)*Pair[e[1], k[4]]*Pair[ec[5], k[1]] + 
         Pair[e[1], k[5]]*((-MH2 + S34 + T24)*Pair[ec[5], k[2]] + 
           (-MH2 + T24 + U)*Pair[ec[5], k[4]])))) + 
   Den[MH2 - S34 - T14 - T24, 0]*
    (8*Den[S, 0]*((S*(T14 - T24) - T*T24 + MH2*(-T14 + T24) + T14*U)*
        Pair[e[1], e[2]]*Pair[ec[4], ec[5]] + 
       2*(-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
         (-2*MH2 + 2*S + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
         Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
           (2*MH2 - 2*S - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] - 
       2*(-2*MH2 + 2*S + T + U)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 2*(-2*MH2 + 2*S + T + U)*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]]*Pair[ec[4], k[5]] - 
       4*(Pair[e[1], k[3]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[3]])*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
          Pair[ec[5], k[3]]) + 2*(-2*MH2 + 2*S + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 2*(-2*MH2 + 2*S + T + U)*
        Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
       2*Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((MH2 - S - U)*Pair[ec[5], k[1]] + (-MH2 + S + T)*
            Pair[ec[5], k[2]]) + ((-MH2 + S + U)*Pair[ec[4], k[1]] + 
           (MH2 - S - T)*Pair[ec[4], k[2]])*Pair[ec[5], k[4]])) + 
     4*(-2*(Pair[e[1], k[5]]*(-4*Pair[e[2], k[1]] + 2*Pair[e[2], k[3]]) - 
         Pair[e[1], k[3]]*(Pair[e[2], k[1]] + 2*Pair[e[2], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 4*Pair[e[2], k[5]]))*
        Pair[ec[4], ec[5]] + 4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*
        Pair[e[2], ec[5]]*Pair[ec[4], k[5]] - 4*Pair[e[1], ec[5]]*
        (2*Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*(2*Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] + 4*Pair[e[1], ec[4]]*(2*Pair[e[2], k[1]] - 
         Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         4*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]]))) - 
     4*Den[T, 0]*(2*(Pair[e[1], k[3]]*((MH2 - S34 - T + T14 + T24)*
            Pair[e[2], k[1]] - 2*T14*Pair[e[2], k[3]] + 
           2*S*Pair[e[2], k[4]]) + 2*(MH2 - T)*
          (Pair[e[1], k[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[3]]) - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       4*((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
        Pair[ec[4], k[5]] + 4*(MH2 - T)*Pair[e[1], ec[5]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[4], k[5]] - 
       4*((-MH2 + T)*Pair[e[1], k[2]] + S*Pair[e[1], k[3]])*Pair[e[2], ec[4]]*
        Pair[ec[5], k[4]] - 4*(MH2 - T)*Pair[e[1], ec[4]]*
        (Pair[e[2], k[1]] - Pair[e[2], k[3]])*Pair[ec[5], k[4]] + 
       Pair[e[1], e[2]]*((MH2 - T)*(MH2 - S - 2*T24 - U)*Pair[ec[4], ec[5]] + 
         4*(MH2 - T)*(Pair[ec[4], k[5]]*Pair[ec[5], k[2]] - 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]])) + 8*Pair[e[1], k[3]]*
        (Pair[e[2], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[2]]*Pair[ec[5], k[4]]) - 
         (Pair[e[2], k[4]] + Pair[e[2], k[5]])*(Pair[ec[4], k[5]]*
            Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))) + 
   4*(Den[T, 0]*(-(((-MH2 + T)*Pair[e[1], e[2]] + 2*Pair[e[1], k[3]]*
           Pair[e[2], k[1]])*Pair[ec[4], ec[5]]) + 2*Pair[e[2], ec[5]]*
        ((-MH2 + T)*Pair[e[1], ec[4]] + 2*Pair[e[1], k[3]]*
          Pair[ec[4], k[1]]) - Pair[e[2], ec[4]]*
        ((-MH2 + T)*Pair[e[1], ec[5]] + 2*Pair[e[1], k[3]]*
          Pair[ec[5], k[1]])) + Den[S34, 0]*
      (-2*Pair[e[2], ec[5]]*((MH2 - S34)*Pair[e[1], ec[4]] + 
         2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
        ((MH2 - S34)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[4]]*
          Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*
        ((MH2 - S34)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[3]]*
          Pair[ec[5], k[4]])) + Den[MH2 - S - T - T14, 0]*
      (Pair[e[1], ec[5]]*((2*S - S34 - 2*T14 + U)*Pair[e[2], ec[4]] - 
         2*(Pair[e[2], k[5]]*(-2*Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
           Pair[e[2], k[4]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[1]]*(Pair[ec[4], k[2]] + 2*Pair[ec[4], k[5]]))) + 
       4*(Pair[e[1], k[5]]*((Pair[e[2], k[3]] + 2*Pair[e[2], k[4]])*
            Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]])) + (Pair[e[1], ec[4]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[4]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
             Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[4]]*
            Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[5], k[2]] + 
             Pair[ec[5], k[4]])))) + Den[S, 0]*
      (Pair[e[1], e[2]]*((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         2*(Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) - 
           Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[4]]) + 
           Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]]))) - 
       4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 2*Pair[e[1], k[5]]*
            Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] + 
             2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
          Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) - 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[3]] + 
           2*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (-(Pair[e[2], ec[5]]*(Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]])) + 
           Pair[e[2], ec[4]]*(Pair[ec[5], k[3]] + 2*Pair[ec[5], k[4]]))))) - 
   4*Den[S34, 0]*(Den[MH2 - S - T - T14, 0]*
      (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
          Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*
            (-2*(MH2 - S34)*Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
           Pair[e[2], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
             (MH2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
           2*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[2]] + 
             T24*Pair[ec[4], k[3]]))) + 
       4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
            ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
         (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
          Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
          ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
          Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
          (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
            Pair[ec[5], k[2]]) - 2*Pair[ec[4], k[3]]*
          (Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]] + 
           Pair[e[1], k[3]]*(-Pair[e[2], k[1]] + Pair[e[2], k[5]])*
            Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
              Pair[ec[5], k[2]] + (-Pair[e[2], k[1]] + Pair[e[2], k[5]])*
              Pair[ec[5], k[3]])))) + Den[S, 0]*
      (-4*((MH2 - S34)*(Pair[e[1], k[5]]*Pair[e[2], k[1]] - 
           Pair[e[1], k[2]]*Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
           (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         (MH2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
           Pair[ec[5], k[2]]) - Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
             (-MH2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[
                3]]) + (MH2 - S34)*Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + 
             Pair[ec[5], k[2]])) + 2*Pair[ec[4], k[3]]*
          (-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[5], k[1]] + 
              Pair[ec[5], k[2]])) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
            Pair[ec[5], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
              (Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + Pair[e[2], k[5]]*
              Pair[ec[5], k[3]]))) + Pair[e[1], e[2]]*
        ((MH2 - S34)*(T + T14 - T24 - U)*Pair[ec[4], ec[5]] + 
         2*(2*(-MH2 + S34)*Pair[ec[4], k[2]]*Pair[ec[5], k[1]] - 
           2*(MH2 - S34)*Pair[ec[4], k[1]]*(Pair[ec[5], k[1]] - 
             Pair[ec[5], k[3]] - Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
            (2*(T14 + T24)*Pair[ec[5], k[1]] - 2*T14*Pair[ec[5], k[3]] + 
             (T - T14 - T24 - U)*Pair[ec[5], k[4]]))))) + 
   Den[2*MH2 - S34 - T - U, 0]*
    (-4*Den[S, 0]*(Pair[e[1], e[2]]*((MH2 - S - T14 - T24)*(T14 - T24)*
          Pair[ec[4], ec[5]] + 2*((-T14 + T24)*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]] + Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
              Pair[ec[5], k[2]] + (S - S34 - T + U)*Pair[ec[5], k[3]]) + 
           Pair[ec[4], k[2]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
             (-S + S34 - T + U)*Pair[ec[5], k[3]]))) + 
       4*(-((MH2 - S34 - T - U)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
            Pair[e[1], k[2]]*Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) - 
         (MH2 - S34 - T - U)*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
         2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
              Pair[ec[4], k[2]])) - Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[3]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]]*
              (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + Pair[e[2], k[4]]*
              Pair[ec[4], k[3]]))*Pair[ec[5], k[3]] - Pair[e[1], ec[4]]*
          Pair[e[2], k[1]]*((-MH2 + S + T + U)*Pair[ec[5], k[3]] + 
           (-MH2 + S34 + T + U)*Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          ((MH2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + 
             Pair[ec[4], k[2]]) + Pair[e[2], ec[4]]*
            ((-MH2 + S + T + U)*Pair[ec[5], k[3]] + (-MH2 + S34 + T + U)*
              Pair[ec[5], k[4]])))) + 
     4*(Pair[e[2], ec[4]]*((-MH2 + S34 + T + U)*Pair[e[1], ec[5]] + 
         2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
        ((-MH2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
          Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*
        ((-MH2 + S34 + T + U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*
          Pair[ec[5], k[3]]) + Den[T24, 0]*
        (Pair[e[1], ec[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
             Pair[e[2], ec[4]]) + 4*(MH2 - S34 - T - U)*
            (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])) - 2*(2*(MH2 - S34 - T - U)*
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + 2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
           4*(-(Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
                Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
             (Pair[e[1], k[2]] - Pair[e[1], k[4]])*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]]))*
            Pair[ec[5], k[3]] - 2*Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
            ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T24 - U)*
              Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
            ((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (MH2 - S34 - T24 - U)*
              Pair[ec[5], k[3]]) + Pair[e[2], ec[4]]*
            ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*((-S34 + T - T24 + U)*
                Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]])))))) + Den[U, 0]*
    (-4*(((-MH2 + U)*Pair[e[1], e[2]] + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]])*
        Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
        ((-MH2 + U)*Pair[e[2], ec[4]] + 2*Pair[e[2], k[3]]*
          Pair[ec[4], k[2]]) - 2*Pair[e[1], ec[4]]*
        ((-MH2 + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[3]]*
          Pair[ec[5], k[2]])) + 4*(Den[MH2 - S34 - T14 - T24, 0]*
        (-2*(2*Pair[e[1], k[4]]*((MH2 - U)*Pair[e[2], k[1]] - 
             S*Pair[e[2], k[3]]) + 2*Pair[e[1], k[3]]*(T24*Pair[e[2], k[3]] + 
             (MH2 - U)*Pair[e[2], k[4]]) - Pair[e[1], k[2]]*
            ((MH2 - S34 + T14 + T24 - U)*Pair[e[2], k[3]] + 
             2*(MH2 - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         4*(MH2 - U)*(Pair[e[1], k[2]] - Pair[e[1], k[3]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[5]] + 4*Pair[e[1], ec[5]]*
          ((-MH2 + U)*Pair[e[2], k[1]] + S*Pair[e[2], k[3]])*
          Pair[ec[4], k[5]] - 4*(MH2 - U)*(Pair[e[1], k[2]] - 
           Pair[e[1], k[3]])*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
         4*Pair[e[1], ec[4]]*((-MH2 + U)*Pair[e[2], k[1]] + 
           S*Pair[e[2], k[3]])*Pair[ec[5], k[4]] + Pair[e[1], e[2]]*
          ((MH2 - S - T - 2*T14)*(MH2 - U)*Pair[ec[4], ec[5]] + 
           4*(MH2 - U)*(Pair[ec[4], k[5]]*Pair[ec[5], k[1]] - 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]])) + 8*Pair[e[2], k[3]]*
          (Pair[e[1], k[3]]*(-(Pair[ec[4], k[5]]*Pair[ec[5], k[1]]) + 
             Pair[ec[4], k[1]]*Pair[ec[5], k[4]]) - 
           (Pair[e[1], k[4]] + Pair[e[1], k[5]])*(Pair[ec[4], k[5]]*
              Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]))) + 
       Den[MH2 - S - T - T14, 0]*(Pair[e[1], ec[5]]*
          ((MH2 - S34 - 2*T14 - T24)*(MH2 - U)*Pair[e[2], ec[4]] + 
           2*(-2*(MH2 - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], 
                 k[1]]*(Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
             Pair[e[2], k[3]]*(-2*T24*Pair[ec[4], k[1]] + (MH2 + S - T + 
                 T14 - U)*Pair[ec[4], k[2]] - 2*S*Pair[ec[4], k[3]]))) - 
         4*(Pair[e[1], k[5]]*(-((T24*Pair[e[2], k[3]] + (MH2 - U)*
                 Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + 
             (MH2 - U)*Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] - Pair[ec[4], 
                k[5]])) - Pair[e[1], ec[4]]*(T24*Pair[e[2], k[3]] + 
             (MH2 - U)*Pair[e[2], k[4]])*Pair[ec[5], k[1]] + 
           (MH2 - U)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
            Pair[ec[5], k[1]] + (MH2 - U)*Pair[e[2], ec[4]]*
            (Pair[e[1], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[4]]) - 2*Pair[e[2], k[3]]*
            (Pair[e[1], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[3]]*(Pair[ec[4], k[1]] - Pair[ec[4], k[5]])*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[ec[4], k[1]]*
                Pair[ec[5], k[3]] - Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + 
               Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))))))
