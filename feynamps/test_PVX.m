(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(col1*
   (-((Alfas^2*EL*MT^2*(-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
            2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
             (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           (-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
         ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
          2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
            Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
            (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
       PVC[0, 0, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
      (MW*SW)) + (4*Alfas^2*EL*MT^2*Pair[ec[4], k[5]]*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
         Pair[ec[5], k[2]]))*PVC[0, 0, 1, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
         2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
          (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
       2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
       2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))*
      PVC[0, 1, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*MH^2*T14 + S34*T14 + 2*MH^2*T24 - 
           S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
         2*(-(Pair[ec[4], k[2]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])) + 
           Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
             T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
           Pair[ec[4], k[1]]*((-MH^2 + S34)*Pair[ec[5], k[1]] + 
             (MH^2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH^2 - S34 - U)*
              Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-MH^2 + S34)*Pair[ec[4], k[1]] + (-MH^2 + S34)*
            Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH^2 - S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], k[
                1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
              Pair[ec[4], k[3]]) + Pair[e[2], ec[4]]*
            ((MH^2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
             (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
              Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((5*MH^2*T14 - 3*S34*T14 - T*T14 - 
           5*MH^2*T24 + 3*S34*T24 + 3*T*T24 - 3*T14*U + T24*U)*
          Pair[ec[4], ec[5]] - 2*(Pair[ec[4], k[3]]*
            (-2*(T14 + T24)*Pair[ec[5], k[1]] + (T14 + T24)*Pair[ec[5], k[
                3]] + 2*T24*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
            (2*(-2*MH^2 + T + U)*Pair[ec[5], k[1]] + (2*MH^2 - 2*T + T14 - 7*
                T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + U)*Pair[ec[5], k[
                4]]) - Pair[ec[4], k[2]]*((4*MH^2 - 2*(T + U))*
              Pair[ec[5], k[1]] + (-2*MH^2 + 2*T - 7*T14 + T24)*
              Pair[ec[5], k[3]] + 2*(-3*MH^2 + S34 + 2*T + U)*
              Pair[ec[5], k[4]]))) - 
       4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-5*MH^2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (5*MH^2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
          Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + (-2*MH^2 + T + U)*
            Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
              Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
            (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
              Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
              Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
             Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 4*
                Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 1, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (4*(S + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 4*(S + T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 2*(T14 + T24)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 2*(T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
       16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
        (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
           2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
            Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
          (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
            Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]])))*
      PVD[0, 0, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 0, 3, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*((-((MH^2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
         4*(MH^2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 2*(T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
           4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
         Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
            Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
       8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
       2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) - 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
      Pair[ec[5], k[4]]*PVD[0, 0, 2, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 
       0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(-(Pair[e[1], e[2]]*
         ((2*MH^2*T14 - S34*T14 - 2*MH^2*T24 + S34*T24 + T*T24 - T14*U)*
           Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
           (-2*(MH^2 - S34)*Pair[ec[5], k[1]] + (-MH^2 + S34 - 2*T14)*
             Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
            2*(-2*MH^2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
            (3*MH^2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
            Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
              Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
           Pair[e[2], ec[4]]*((2*MH^2 - 2*S34 + T14 + T24)*Pair[ec[5], k[
                3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
            Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
            Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
            Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
            Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 0, 0, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*(MH^2 - S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] + 2*(MH^2 - S34 + T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
        Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH^2 - S34 + T14 + T24)*
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
          (2*(MH^2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
           (2*MH^2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
           (2*MH^2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-T + 2*T14 - 2*T24 + U)*
          Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])) - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
             2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 1, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*
      (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
             Pair[ec[5], k[4]])) + (MH^2 - S34)*
          (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        (-((MH^2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
            Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
           (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
          (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
            (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
           (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
              (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((-T14 + T24)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]*
      PVD[0, 1, 2, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(2*(-3*MH^2 + S34 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH^2 + S34 + T + U)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
       2*(-MH^2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
       2*(MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
       Pair[e[1], e[2]]*(-((MH^2 - S34)*(Pair[ec[4], k[1]] - 
            Pair[ec[4], k[2]])*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
          ((-3*MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH^2 - S34 - T - U)*
            Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]])))*
      PVD[0, 2, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
          Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
            (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 2, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
         (-T + U)*Pair[ec[5], k[4]]) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
           Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVD[0, 2, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*((-T + U)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]]*
      PVD[0, 3, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
       2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
         Pair[ec[5], k[4]]) - 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))*PVD[1, 0, 0, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
       4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))*
      PVD[1, 0, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
          Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[2]]))*Pair[ec[5], k[4]])*PVD[1, 0, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
          (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((-T + U)*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]])))*
      PVD[1, 1, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/S + 
 (col2*
   (-((Alfas^2*EL*MT^2*(-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
            2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
             (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
          Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
           Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
           (-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
            Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
         ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
          2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
            Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
            (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
       PVC[0, 0, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/
      (MW*SW)) + (4*Alfas^2*EL*MT^2*Pair[ec[4], k[5]]*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
         Pair[ec[5], k[2]]))*PVC[0, 0, 1, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
         2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
          (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
       2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
       2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))*
      PVC[0, 1, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*MH^2*T14 + S34*T14 + 2*MH^2*T24 - 
           S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
         2*(-(Pair[ec[4], k[2]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])) + 
           Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
             T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
           Pair[ec[4], k[1]]*((-MH^2 + S34)*Pair[ec[5], k[1]] + 
             (MH^2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH^2 - S34 - U)*
              Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-MH^2 + S34)*Pair[ec[4], k[1]] + (-MH^2 + S34)*
            Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH^2 - S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], k[
                1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
              Pair[ec[4], k[3]]) + Pair[e[2], ec[4]]*
            ((MH^2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
             (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
              Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((5*MH^2*T14 - 3*S34*T14 - T*T14 - 
           5*MH^2*T24 + 3*S34*T24 + 3*T*T24 - 3*T14*U + T24*U)*
          Pair[ec[4], ec[5]] - 2*(Pair[ec[4], k[3]]*
            (-2*(T14 + T24)*Pair[ec[5], k[1]] + (T14 + T24)*Pair[ec[5], k[
                3]] + 2*T24*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
            (2*(-2*MH^2 + T + U)*Pair[ec[5], k[1]] + (2*MH^2 - 2*T + T14 - 7*
                T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + U)*Pair[ec[5], k[
                4]]) - Pair[ec[4], k[2]]*((4*MH^2 - 2*(T + U))*
              Pair[ec[5], k[1]] + (-2*MH^2 + 2*T - 7*T14 + T24)*
              Pair[ec[5], k[3]] + 2*(-3*MH^2 + S34 + 2*T + U)*
              Pair[ec[5], k[4]]))) - 
       4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-5*MH^2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (5*MH^2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
          Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + (-2*MH^2 + T + U)*
            Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
              Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
            (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
              Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
              Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
             Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 4*
                Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 1, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (4*(S + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 4*(S + T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 2*(T14 + T24)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 2*(T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
       16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
        (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
           2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
            Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
          (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
            Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]])))*
      PVD[0, 0, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 0, 3, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*((-((MH^2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
         4*(MH^2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(-2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 2*(T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
           4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
         Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
            Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
       8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
       2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(-((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) - 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
      Pair[ec[5], k[4]]*PVD[0, 0, 2, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 
       0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (2*Alfas^2*EL*MT^2*(-(Pair[e[1], e[2]]*
         ((2*MH^2*T14 - S34*T14 - 2*MH^2*T24 + S34*T24 + T*T24 - T14*U)*
           Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
           (-2*(MH^2 - S34)*Pair[ec[5], k[1]] + (-MH^2 + S34 - 2*T14)*
             Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
            2*(-2*MH^2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
            (3*MH^2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
            Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
              Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
           Pair[e[2], ec[4]]*((2*MH^2 - 2*S34 + T14 + T24)*Pair[ec[5], k[
                3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
            Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
            Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
            Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
            Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 0, 0, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*(MH^2 - S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] + 2*(MH^2 - S34 + T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
        Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH^2 - S34 + T14 + T24)*
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
          (2*(MH^2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
           (2*MH^2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
           (2*MH^2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-T + 2*T14 - 2*T24 + U)*
          Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])) - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
             2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 1, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*
      (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
             Pair[ec[5], k[4]])) + (MH^2 - S34)*
          (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        (-((MH^2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
            Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
           (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
          (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
            (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
           (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
              (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((-T14 + T24)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]*
      PVD[0, 1, 2, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*(2*(-3*MH^2 + S34 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH^2 + S34 + T + U)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
       2*(-MH^2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
       2*(MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
       Pair[e[1], e[2]]*(-((MH^2 - S34)*(Pair[ec[4], k[1]] - 
            Pair[ec[4], k[2]])*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
          ((-3*MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH^2 - S34 - T - U)*
            Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]])))*
      PVD[0, 2, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
          Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
            (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 2, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
         (-T + U)*Pair[ec[5], k[4]]) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
           Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVD[0, 2, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*((-T + U)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]]*
      PVD[0, 3, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
       2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
         Pair[ec[5], k[4]]) - 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))*PVD[1, 0, 0, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
       4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))*
      PVD[1, 0, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
          Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[2]]))*Pair[ec[5], k[4]])*PVD[1, 0, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
          (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((-T + U)*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]])))*
      PVD[1, 1, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/S + 
 (col5*((Alfas^2*EL*MT^2*(-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
           2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
          (-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
           Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[5]]*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
         Pair[ec[5], k[2]]))*PVC[0, 0, 1, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
         2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
          (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
       2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
       2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))*
      PVC[0, 1, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*MH^2*T14 + S34*T14 + 2*MH^2*T24 - 
           S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
         2*(-(Pair[ec[4], k[2]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])) + 
           Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
             T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
           Pair[ec[4], k[1]]*((-MH^2 + S34)*Pair[ec[5], k[1]] + 
             (MH^2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH^2 - S34 - U)*
              Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-MH^2 + S34)*Pair[ec[4], k[1]] + (-MH^2 + S34)*
            Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH^2 - S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], k[
                1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
              Pair[ec[4], k[3]]) + Pair[e[2], ec[4]]*
            ((MH^2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
             (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
              Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((5*MH^2*T14 - 3*S34*T14 - T*T14 - 
           5*MH^2*T24 + 3*S34*T24 + 3*T*T24 - 3*T14*U + T24*U)*
          Pair[ec[4], ec[5]] - 2*(Pair[ec[4], k[3]]*
            (-2*(T14 + T24)*Pair[ec[5], k[1]] + (T14 + T24)*Pair[ec[5], k[
                3]] + 2*T24*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
            (2*(-2*MH^2 + T + U)*Pair[ec[5], k[1]] + (2*MH^2 - 2*T + T14 - 7*
                T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + U)*Pair[ec[5], k[
                4]]) - Pair[ec[4], k[2]]*((4*MH^2 - 2*(T + U))*
              Pair[ec[5], k[1]] + (-2*MH^2 + 2*T - 7*T14 + T24)*
              Pair[ec[5], k[3]] + 2*(-3*MH^2 + S34 + 2*T + U)*
              Pair[ec[5], k[4]]))) - 
       4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-5*MH^2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (5*MH^2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
          Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + (-2*MH^2 + T + U)*
            Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
              Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
            (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
              Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
              Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
             Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 4*
                Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 1, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (4*(S + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 4*(S + T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 2*(T14 + T24)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 2*(T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
       16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
        (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
           2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
            Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
          (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
            Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]])))*
      PVD[0, 0, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 0, 3, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((-((MH^2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
         4*(MH^2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(-2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 2*(T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
           4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
         Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
            Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
       8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
       2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) - 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
      Pair[ec[5], k[4]]*PVD[0, 0, 2, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 
       0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-(Pair[e[1], e[2]]*
         ((2*MH^2*T14 - S34*T14 - 2*MH^2*T24 + S34*T24 + T*T24 - T14*U)*
           Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
           (-2*(MH^2 - S34)*Pair[ec[5], k[1]] + (-MH^2 + S34 - 2*T14)*
             Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
            2*(-2*MH^2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
            (3*MH^2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
            Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
              Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
           Pair[e[2], ec[4]]*((2*MH^2 - 2*S34 + T14 + T24)*Pair[ec[5], k[
                3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
            Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
            Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
            Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
            Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 0, 0, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*(MH^2 - S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] + 2*(MH^2 - S34 + T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
        Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH^2 - S34 + T14 + T24)*
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
          (2*(MH^2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
           (2*MH^2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
           (2*MH^2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-T + 2*T14 - 2*T24 + U)*
          Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])) - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
             2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 1, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*
      (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
             Pair[ec[5], k[4]])) + (MH^2 - S34)*
          (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        (-((MH^2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
            Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
           (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
          (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
            (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
           (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
              (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((-T14 + T24)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]*
      PVD[0, 1, 2, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(2*(-3*MH^2 + S34 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH^2 + S34 + T + U)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
       2*(-MH^2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
       2*(MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
       Pair[e[1], e[2]]*(-((MH^2 - S34)*(Pair[ec[4], k[1]] - 
            Pair[ec[4], k[2]])*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
          ((-3*MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH^2 - S34 - T - U)*
            Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]])))*
      PVD[0, 2, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
          Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
            (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 2, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
         (-T + U)*Pair[ec[5], k[4]]) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
           Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVD[0, 2, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*((-T + U)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]]*
      PVD[0, 3, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
       2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
         Pair[ec[5], k[4]]) - 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))*PVD[1, 0, 0, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
       4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))*
      PVD[1, 0, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
          Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[2]]))*Pair[ec[5], k[4]])*PVD[1, 0, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
          (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((-T + U)*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]])))*
      PVD[1, 1, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/S + 
 (col6*((Alfas^2*EL*MT^2*(-4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) - 
           2*Pair[e[1], k[5]]*Pair[e[2], k[1]] + Pair[e[1], k[2]]*
            (Pair[e[2], k[3]] + 2*Pair[e[2], k[5]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          Pair[ec[5], k[4]] + Pair[e[1], k[2]]*
          (-(Pair[e[2], ec[5]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])) + 
           Pair[e[2], ec[4]]*Pair[ec[5], k[4]])) + Pair[e[1], e[2]]*
        ((-T - 2*T14 + 2*T24 + U)*Pair[ec[4], ec[5]] - 
         2*(-2*Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           Pair[ec[4], k[5]]*(-Pair[ec[5], k[1]] + Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[4]])))*
      PVC[0, 0, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*Pair[ec[4], k[5]]*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]] - 2*Pair[e[1], ec[5]]*
        Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[5], k[1]] - 
         Pair[ec[5], k[2]]))*PVC[0, 0, 1, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]]*Pair[ec[5], k[1]] - 
         2*Pair[ec[4], k[2]]*Pair[ec[5], k[2]] + Pair[ec[4], k[5]]*
          (-Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) - 
       2*Pair[e[2], k[1]]*(Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[1], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])) + 
       2*Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + 
         Pair[e[2], ec[4]]*(Pair[ec[5], k[1]] + Pair[ec[5], k[2]])))*
      PVC[0, 1, 0, S34, S, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*MH^2*T14 + S34*T14 + 2*MH^2*T24 - 
           S34*T24 - T*T24 + T14*U)*Pair[ec[4], ec[5]] - 
         2*(-(Pair[ec[4], k[2]]*((MH^2 - S34)*Pair[ec[5], k[1]] + 
              T14*Pair[ec[5], k[3]] + (MH^2 - T)*Pair[ec[5], k[4]])) + 
           Pair[ec[4], k[3]]*((T14 + T24)*Pair[ec[5], k[1]] - 
             T14*Pair[ec[5], k[3]] - T24*Pair[ec[5], k[4]]) + 
           Pair[ec[4], k[1]]*((-MH^2 + S34)*Pair[ec[5], k[1]] + 
             (MH^2 - S34 + T24)*Pair[ec[5], k[3]] + (2*MH^2 - S34 - U)*
              Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-MH^2 + S34)*Pair[ec[4], k[1]] + (-MH^2 + S34)*
            Pair[ec[4], k[2]] + (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((MH^2 - S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[ec[4], k[
                1]] + (-MH^2 + S34)*Pair[ec[4], k[2]] + (T14 + T24)*
              Pair[ec[4], k[3]]) + Pair[e[2], ec[4]]*
            ((MH^2 - S34 + T14 + T24)*Pair[ec[5], k[3]] + 
             (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + Pair[e[2], k[1]]*
            (Pair[e[1], k[5]]*Pair[ec[4], k[3]] - Pair[e[1], k[3]]*
              Pair[ec[4], k[5]])*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            (Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
              Pair[ec[5], k[3]] + (Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[4], k[5]])*Pair[ec[5], k[4]]))))*
      PVD[0, 0, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((5*MH^2*T14 - 3*S34*T14 - T*T14 - 
           5*MH^2*T24 + 3*S34*T24 + 3*T*T24 - 3*T14*U + T24*U)*
          Pair[ec[4], ec[5]] - 2*(Pair[ec[4], k[3]]*
            (-2*(T14 + T24)*Pair[ec[5], k[1]] + (T14 + T24)*Pair[ec[5], k[
                3]] + 2*T24*Pair[ec[5], k[4]]) + Pair[ec[4], k[1]]*
            (2*(-2*MH^2 + T + U)*Pair[ec[5], k[1]] + (2*MH^2 - 2*T + T14 - 7*
                T24)*Pair[ec[5], k[3]] + 2*(-MH^2 + S34 + U)*Pair[ec[5], k[
                4]]) - Pair[ec[4], k[2]]*((4*MH^2 - 2*(T + U))*
              Pair[ec[5], k[1]] + (-2*MH^2 + 2*T - 7*T14 + T24)*
              Pair[ec[5], k[3]] + 2*(-3*MH^2 + S34 + 2*T + U)*
              Pair[ec[5], k[4]]))) - 
       4*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-5*MH^2 + 3*S34 + 2*(T + U))*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (5*MH^2 - 3*S34 - 2*(T + U))*Pair[e[2], k[4]]))*
          Pair[ec[4], ec[5]] - Pair[e[1], ec[5]]*Pair[e[2], k[1]]*
          ((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + (-2*MH^2 + T + U)*
            Pair[ec[4], k[2]] - (T14 + T24)*Pair[ec[4], k[3]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
          (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
            Pair[ec[5], k[4]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*((-2*MH^2 + T + U)*Pair[ec[4], k[1]] + 
             (-2*MH^2 + T + U)*Pair[ec[4], k[2]] - (T14 + T24)*
              Pair[ec[4], k[3]]) - Pair[e[2], ec[4]]*
            (3*(T14 + T24)*Pair[ec[5], k[3]] + 2*(S + T14 + T24)*
              Pair[ec[5], k[4]])) + 2*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]*
             (Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] + 
              Pair[ec[5], k[4]])) + Pair[e[1], k[4]]*Pair[e[2], k[1]]*
            (4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + 4*Pair[ec[4], k[2]]*
              Pair[ec[5], k[3]] - Pair[ec[4], k[3]]*Pair[ec[5], k[4]]) + 
           Pair[e[1], k[2]]*(Pair[e[2], k[3]]*(Pair[ec[4], k[1]] + Pair[
                ec[4], k[2]])*(Pair[ec[5], k[3]] + Pair[ec[5], k[4]]) + 
             Pair[e[2], k[4]]*(-4*Pair[ec[4], k[1]]*Pair[ec[5], k[3]] - 4*
                Pair[ec[4], k[2]]*Pair[ec[5], k[3]] + Pair[ec[4], k[3]]*
                Pair[ec[5], k[4]])))))*PVD[0, 0, 0, 1, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (4*Alfas^2*EL*MT^2*
      (4*(S + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 4*(S + T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 2*(T14 + T24)*Pair[e[1], k[2]]*
        Pair[e[2], ec[4]]*Pair[ec[5], k[3]] - 2*(T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] - 
       16*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        Pair[ec[5], k[3]] + Pair[e[1], e[2]]*
        (Pair[ec[4], k[2]]*(2*(S + T14 + T24)*Pair[ec[5], k[1]] - 
           2*(S + T14 + T24)*Pair[ec[5], k[2]] + (-5*T14 + 3*T24)*
            Pair[ec[5], k[3]]) + Pair[ec[4], k[1]]*
          (2*(S + T14 + T24)*Pair[ec[5], k[1]] - 2*(S + T14 + T24)*
            Pair[ec[5], k[2]] + (-3*T14 + 5*T24)*Pair[ec[5], k[3]])))*
      PVD[0, 0, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
         Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]))*Pair[ec[5], k[3]]*PVD[0, 0, 0, 3, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + 
    (Alfas^2*EL*MT^2*((-((MH^2 - S34)*(T14 - T24)*Pair[e[1], e[2]]) - 
         4*(MH^2 - S34)*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
           Pair[e[1], k[2]]*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(-2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*
        (Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 2*(T14 + T24)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(-2*(T14 - T24)*Pair[ec[5], k[1]] + 
           4*T24*Pair[ec[5], k[2]] + (3*T14 - 5*T24)*Pair[ec[5], k[3]]) + 
         Pair[ec[4], k[2]]*(-4*T14*Pair[ec[5], k[1]] - 2*(T14 - T24)*
            Pair[ec[5], k[2]] + (5*T14 - 3*T24)*Pair[ec[5], k[3]])) + 
       8*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[1]] + Pair[ec[5], k[2]] - 2*Pair[ec[5], k[4]]) + 
       2*(T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[4]] - 
       2*(T14 + T24)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]])*
      PVD[0, 0, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(-((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + 
          Pair[ec[4], k[2]])*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]])) - 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]])*
        (Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))*PVD[0, 0, 1, 2, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) - (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]))*
      Pair[ec[5], k[4]]*PVD[0, 0, 2, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 
       0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (2*Alfas^2*EL*MT^2*(-(Pair[e[1], e[2]]*
         ((2*MH^2*T14 - S34*T14 - 2*MH^2*T24 + S34*T24 + T*T24 - T14*U)*
           Pair[ec[4], ec[5]] + Pair[ec[4], k[5]]*
           (-2*(MH^2 - S34)*Pair[ec[5], k[1]] + (-MH^2 + S34 - 2*T14)*
             Pair[ec[5], k[3]] + 2*(-MH^2 + T)*Pair[ec[5], k[4]]) + 
          2*Pair[ec[4], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
             Pair[ec[5], k[3]] + (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + 
          Pair[ec[4], k[3]]*(2*(S + S34 + 2*(T14 + T24))*Pair[ec[5], k[1]] + 
            2*(-2*MH^2 + S34 + T - 3*T14 + T24)*Pair[ec[5], k[3]] - 
            (3*MH^2 + S - 2*T + T14 + 3*T24)*Pair[ec[5], k[4]]))) + 
       2*((-((T14 + T24)*Pair[e[1], k[3]]*Pair[e[2], k[1]]) + 
           (-4*MH^2 + 2*S34 + T + U)*Pair[e[1], k[4]]*Pair[e[2], k[1]] + 
           Pair[e[1], k[2]]*((T14 + T24)*Pair[e[2], k[3]] + 
             (4*MH^2 - 2*S34 - T - U)*Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
         Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((S + S34 + 2*(T14 + T24))*
            Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
         Pair[e[1], ec[4]]*Pair[e[2], k[1]]*((2*MH^2 - 2*S34 + T14 + T24)*
            Pair[ec[5], k[3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]]) - 
         Pair[e[1], k[2]]*(Pair[e[2], ec[5]]*((S + S34 + 2*(T14 + T24))*
              Pair[ec[4], k[3]] + (-MH^2 + S34)*Pair[ec[4], k[5]]) + 
           Pair[e[2], ec[4]]*((2*MH^2 - 2*S34 + T14 + T24)*Pair[ec[5], k[
                3]] + (3*MH^2 - S34 - T - U)*Pair[ec[5], k[4]])) + 
         2*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]*
             Pair[ec[5], k[3]]) + 2*Pair[e[1], k[2]]*Pair[e[2], k[3]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[3]] - 3*Pair[e[1], k[2]]*
            Pair[e[2], k[4]]*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[5]]*Pair[ec[4], k[3]]*
            Pair[ec[5], k[3]] - Pair[e[1], k[2]]*Pair[e[2], k[4]]*
            Pair[ec[4], k[5]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
            Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])*
            Pair[ec[5], k[3]] + Pair[e[1], k[5]]*Pair[e[2], k[1]]*
            Pair[ec[4], k[3]]*Pair[ec[5], k[4]] - Pair[e[1], k[2]]*
            Pair[e[2], k[5]]*Pair[ec[4], k[3]]*Pair[ec[5], k[4]] + 
           Pair[e[1], k[2]]*Pair[e[2], k[3]]*Pair[ec[4], k[5]]*
            Pair[ec[5], k[4]] - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
            (2*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
              Pair[ec[5], k[4]]))))*PVD[0, 1, 0, 0, MH^2, S34, S, 
       2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (4*Alfas^2*EL*MT^2*
      (2*Pair[e[1], k[2]]*Pair[e[2], ec[5]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*((-7*MH^2 + 3*(S34 + T + U))*
          Pair[ec[4], k[3]] + (-3*MH^2 + S34 + T + U)*Pair[ec[4], k[5]]) - 
       2*(MH^2 - S34 + T14 + T24)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] + 2*(MH^2 - S34 + T14 + T24)*Pair[e[1], ec[4]]*
        Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-2*Pair[e[1], k[3]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) - Pair[e[1], k[5]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (2*Pair[e[2], k[3]]*(2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]) + 
           Pair[e[2], k[5]]*(3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]])))*
        Pair[ec[5], k[3]] - Pair[e[1], e[2]]*(2*(MH^2 - S34 + T14 + T24)*
          Pair[ec[4], k[1]]*Pair[ec[5], k[3]] + Pair[ec[4], k[5]]*
          (2*(MH^2 + S + T14 + T24)*Pair[ec[5], k[1]] + 
           (2*MH^2 - 3*S - S34 - 6*T14 - 2*T24 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + S + T14 + T24)*Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[1]] + 
           (2*MH^2 - 5*S - S34 - 12*T14 - 4*U)*Pair[ec[5], k[3]] - 
           (MH^2 + 3*(S + T14 + T24))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-T + 2*T14 - 2*T24 + U)*
          Pair[ec[4], k[3]] + (-T + T14 - T24 + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])) - Pair[e[1], k[3]]*Pair[e[2], k[1]]*
          (3*Pair[ec[4], k[3]] + 2*Pair[ec[4], k[5]]) + 
         Pair[e[1], k[2]]*(Pair[e[2], k[5]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]]) + Pair[e[2], k[3]]*(3*Pair[ec[4], k[3]] + 
             2*Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 1, 0, 2, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (4*Alfas^2*EL*MT^2*
      (-2*(Pair[ec[4], k[3]]*(-((T14 + T24)*Pair[e[1], k[2]]*
             Pair[e[2], ec[5]]) + (T14 + T24)*Pair[e[1], ec[5]]*
            Pair[e[2], k[1]] + 4*(Pair[e[1], k[4]]*Pair[e[2], k[1]] - 
             Pair[e[1], k[2]]*Pair[e[2], k[4]])*(Pair[ec[5], k[3]] - 
             Pair[ec[5], k[4]])) + (MH^2 - S34)*
          (Pair[e[1], k[2]]*Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
            Pair[e[2], k[1]])*Pair[ec[5], k[4]]) + Pair[e[1], e[2]]*
        (-((MH^2 - S34)*(Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*
           Pair[ec[5], k[4]]) + Pair[ec[4], k[3]]*
          (2*(T14 + T24)*Pair[ec[5], k[1]] + (-3*T14 + T24)*
            Pair[ec[5], k[3]] + (T14 - 3*T24)*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*(Pair[ec[4], k[5]]*
          ((-T14 + T24)*Pair[ec[5], k[3]] + (-T + U)*Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[3]]*(-2*(T14 - T24)*Pair[ec[5], k[3]] + 
           (-T + T14 - T24 + U)*Pair[ec[5], k[4]])) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*(2*Pair[ec[4], k[3]] + 
            Pair[ec[4], k[5]])*Pair[ec[5], k[3]]) - Pair[e[2], k[1]]*
          (Pair[e[1], k[5]]*Pair[ec[4], k[3]] + Pair[e[1], k[3]]*
            (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*(2*Pair[ec[4], k[3]] + 
             Pair[ec[4], k[5]])*Pair[ec[5], k[3]] + 
           (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
              (2*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))*Pair[ec[5], k[4]])))*
      PVD[0, 1, 1, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((-T14 + T24)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[4]]*
      PVD[0, 1, 2, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (4*Alfas^2*EL*MT^2*(2*(-3*MH^2 + S34 + T + U)*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*Pair[ec[4], k[3]] - 2*(-3*MH^2 + S34 + T + U)*
        Pair[e[1], ec[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]] + 
       2*(-MH^2 + S34)*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*Pair[ec[5], k[3]] + 
       2*(MH^2 - S34)*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]] + 
       8*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[4]]*
          Pair[e[2], k[1]] + Pair[e[1], k[2]]*(Pair[e[2], k[3]] - 
           Pair[e[2], k[4]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]] + 
       Pair[e[1], e[2]]*(-((MH^2 - S34)*(Pair[ec[4], k[1]] - 
            Pair[ec[4], k[2]])*Pair[ec[5], k[3]]) + Pair[ec[4], k[3]]*
          ((-3*MH^2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH^2 - S34 - T - U)*
            Pair[ec[5], k[2]] + 2*(-T + T14 - T24 + U)*Pair[ec[5], k[3]])))*
      PVD[0, 2, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*(Pair[e[1], e[2]]*((-2*T + T14 - T24 + 2*U)*
          Pair[ec[4], k[3]] + (-T + U)*Pair[ec[4], k[5]]) + 
       4*(-(Pair[e[1], k[5]]*Pair[e[2], k[1]]*Pair[ec[4], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*(3*Pair[ec[4], k[3]] + 
           Pair[ec[4], k[5]]) + Pair[e[1], k[2]]*
          (Pair[e[2], k[5]]*Pair[ec[4], k[3]] + Pair[e[2], k[3]]*
            (3*Pair[ec[4], k[3]] + Pair[ec[4], k[5]]))))*Pair[ec[5], k[3]]*
      PVD[0, 2, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*Pair[ec[4], k[3]]*
      (Pair[e[1], e[2]]*((-T14 + T24)*Pair[ec[5], k[3]] + 
         (-T + U)*Pair[ec[5], k[4]]) + 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]*Pair[ec[5], k[3]]) - 
         Pair[e[1], k[3]]*Pair[e[2], k[1]]*Pair[ec[5], k[4]] + 
         Pair[e[1], k[2]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] + 
           Pair[e[2], k[3]]*Pair[ec[5], k[4]])))*PVD[0, 2, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*((-T + U)*Pair[e[1], e[2]] + 
       4*(-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[3]]))*Pair[ec[4], k[3]]*Pair[ec[5], k[3]]*
      PVD[0, 3, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) + 
    (8*Alfas^2*EL*MT^2*((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] - 2*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) + 
       2*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + Pair[e[1], e[2]]*
        (-(Pair[ec[4], k[2]]*(2*Pair[ec[5], k[1]] + Pair[ec[5], k[3]])) + 
         Pair[ec[4], k[1]]*(2*Pair[ec[5], k[2]] + Pair[ec[5], k[3]])) + 
       2*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*(2*Pair[ec[5], k[3]] + 
         Pair[ec[5], k[4]]) - 2*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        (2*Pair[ec[5], k[3]] + Pair[ec[5], k[4]]))*PVD[1, 0, 0, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      ((T14 - T24)*Pair[e[1], e[2]]*Pair[ec[4], ec[5]] - 
       4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
          Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 4*Pair[e[1], k[2]]*
        Pair[e[2], ec[5]]*(Pair[ec[4], k[1]] + Pair[ec[4], k[2]]) - 
       4*Pair[e[1], ec[5]]*Pair[e[2], k[1]]*(Pair[ec[4], k[1]] + 
         Pair[ec[4], k[2]]) + 4*Pair[e[1], k[2]]*Pair[e[2], ec[4]]*
        Pair[ec[5], k[3]] - 4*Pair[e[1], ec[4]]*Pair[e[2], k[1]]*
        Pair[ec[5], k[3]] + 2*Pair[e[1], e[2]]*
        (Pair[ec[4], k[1]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[4]]) + 
         Pair[ec[4], k[2]]*(-2*Pair[ec[5], k[2]] + Pair[ec[5], k[4]])))*
      PVD[1, 0, 0, 1, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW) - 
    (8*Alfas^2*EL*MT^2*(((-T14 + T24)*Pair[e[1], e[2]] + 
         4*(-(Pair[e[1], k[4]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[4]]))*Pair[ec[4], ec[5]] + 
       2*(2*Pair[e[1], k[2]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
          Pair[e[2], k[1]] + Pair[e[1], e[2]]*(Pair[ec[4], k[1]] - 
           Pair[ec[4], k[2]]))*Pair[ec[5], k[4]])*PVD[1, 0, 1, 0, MH^2, S34, 
       S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2], 
       Sqrt[MT^2]])/(MW*SW) + (8*Alfas^2*EL*MT^2*
      (4*((-(Pair[e[1], k[3]]*Pair[e[2], k[1]]) + Pair[e[1], k[2]]*
            Pair[e[2], k[3]])*Pair[ec[4], ec[5]] - Pair[e[2], k[1]]*
          (Pair[e[1], ec[5]]*Pair[ec[4], k[3]] + Pair[e[1], ec[4]]*
            Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
          (Pair[e[2], ec[5]]*Pair[ec[4], k[3]] + Pair[e[2], ec[4]]*
            Pair[ec[5], k[3]])) + Pair[e[1], e[2]]*
        ((-T + U)*Pair[ec[4], ec[5]] + 
         2*(Pair[ec[4], k[3]]*(Pair[ec[5], k[1]] - Pair[ec[5], k[2]]) + 
           (Pair[ec[4], k[1]] - Pair[ec[4], k[2]])*Pair[ec[5], k[3]])))*
      PVD[1, 1, 0, 0, MH^2, S34, S, 2*MH^2 - S34 - T - U, 0, 0, Sqrt[MT^2], 
       Sqrt[MT^2], Sqrt[MT^2], Sqrt[MT^2]])/(MW*SW)))/S
