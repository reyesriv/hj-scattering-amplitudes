(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-((Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu2, Glu4, Glu5, 0, 0]]*
    (-(Den[T24, 0]*Den[2*MH2 - S34 - T - U, 0]*
       (8*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (((MH2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
             Pair[e[1], k[3]] + (MH2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
            Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
        8*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-(((MH2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
             2*(S - T14)*Pair[e[1], k[3]] + (MH2 + S34 + T - 2*T24 - 3*U)*
              Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
          2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
          2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]])))*Pair[ec[5], k[3]] + 
        (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
          16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
         (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
            4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
             Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
             (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
              Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
            Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
              Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
           (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
        2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
           Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
           (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
           Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
           (-((S - T14)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
            4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
          8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
           Pair[e[2], k[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
           Pair[ec[4], k[2]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
            (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
           ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
             (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - 
                U)*Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
             ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + 
                U)*Pair[ec[5], k[4]]))) - 
        4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (Pair[e[1], ec[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*
              Pair[e[2], ec[4]]) + 4*(MH2 - S34 - T - U)*
             (Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[4], k[2]])) + 2*(-2*(MH2 - S34 - T - U)*
             (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
             Pair[ec[4], ec[5]] - 2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - 
              Pair[e[1], k[4]])*Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + 
            8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                 Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                 Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + Pair[e[1], ec[4]]*
             Pair[e[2], k[4]]*(2*(-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
              (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]]) + 
            Pair[e[1], e[2]]*Pair[ec[4], k[2]]*(2*(-MH2 + S34 + T + U)*Pair[
                ec[5], k[1]] + (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], 
                k[3]])) - Pair[e[2], ec[4]]*(4*(S - T14)*Pair[e[1], k[3]]*
             Pair[ec[5], k[3]] + Pair[e[1], k[4]]*(4*(MH2 - S34 - T - U)*Pair[
                ec[5], k[2]] + (MH2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], 
                k[3]]) + Pair[e[1], k[2]]*((MH2 - 5*S34 + 3*T - 2*T24 + 3*U)*
               Pair[ec[5], k[3]] + 4*(-MH2 + S34 + T + U)*Pair[ec[5], 
                k[4]]))))) + Den[S34, 0]*Den[MH2 - S - T - T14, 0]*
      ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
        (Pair[e[1], ec[5]]*((-MH2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
           Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
           Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
         2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
              Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
               Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
               Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]))) - 8*(C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
          Pair[ec[4], k[3]]*(2*(-MH2 + S34 + 2*T24)*Pair[e[1], k[5]]*
            Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*T24)*
              Pair[e[2], k[1]] + (-MH2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
             (MH2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
           2*(-MH2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
           8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
             Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[5], k[4]]))) + 
         C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
          (2*(MH2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
           Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
             (MH2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
             (-3*MH2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH2 - S34 - 2*U)*
            Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
           8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
             Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
       2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
        (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
            Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH2 - S34)*
                Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
             Pair[e[2], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
               (MH2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
             2*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[2]] + T24*
                Pair[ec[4], k[3]]))) + 
         4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                 k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
              ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
           (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
            ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
            Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
            (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
              Pair[ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*
            (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
             Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
             Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
               Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
       4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
        (-(Pair[e[1], ec[5]]*(-((MH2 - S34)*(MH2 - 2*S - T24 - U)*
              Pair[e[2], ec[4]]) + Pair[e[2], k[4]]*(-4*(MH2 - S34)*Pair[
                ec[4], k[1]] + (5*MH2 + 2*S - 3*S34 - 6*T + 2*T14)*Pair[
                ec[4], k[3]]) + Pair[e[2], k[3]]*(-4*(MH2 - S34)*Pair[ec[4], 
                k[1]] + (MH2 - 2*S + S34 - 2*T + 6*T14)*Pair[ec[4], k[3]]) + 
            2*Pair[e[2], k[1]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
              (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]]))) + 
         2*(Pair[e[1], k[5]]*(-2*(MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                k[5]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
              (-2*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*
                Pair[ec[4], k[3]])) - 2*(MH2 - S34)*Pair[e[1], ec[4]]*
            (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
           Pair[e[1], e[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
             (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
           2*(MH2 - S34)*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[
                1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 
           8*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[
                ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[4]]*
              Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*
                Pair[ec[5], k[3]] - Pair[e[2], k[3]]*Pair[ec[5], k[4]])))))))/
   (MW*SW)) - (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu5, Glu4, Glu2, 0, 0]]*
   (-(Den[T24, 0]*Den[2*MH2 - S34 - T - U, 0]*
      (8*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
        (((MH2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
            Pair[e[1], k[3]] + (MH2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
          Pair[e[2], ec[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], ec[4]]*
          Pair[e[2], k[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[
                ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
           Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
             Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
       8*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
        (-(((MH2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
            2*(S - T14)*Pair[e[1], k[3]] + (MH2 + S34 + T - 2*T24 - 3*U)*
             Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
         2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
         2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
         8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
            (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
       (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
         16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
        (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
           4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
              Pair[ec[4], k[2]])) - 
         2*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], k[4]]*
            Pair[ec[4], ec[5]] + Pair[ec[4], k[2]]*
            (2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
             Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*Pair[ec[5], k[1]] - 
             Pair[ec[5], k[3]])) + Pair[e[2], ec[4]]*
          (Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[ec[5], k[3]]) + 
           Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
       2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
        (-4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
          Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 4*(MH2 - S34 - T - U)*
          (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
          Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
          (-((S - T14)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
           4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
             Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
         8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
              Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
            (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
              Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
          Pair[e[2], k[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
           (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
          Pair[ec[4], k[2]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
           (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
          ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
            (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - U)*
              Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*
            ((-S34 + T - T24 + U)*Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + U)*
              Pair[ec[5], k[4]]))) - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, 
         MT2, MT2, MT2]*(Pair[e[1], ec[5]]*
          (-((S - T14)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
           4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
             Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
         2*(-2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 2*(MH2 - S34 - T - U)*
            (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
            Pair[ec[4], k[2]] + 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                 Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
             Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
           Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*(-MH2 + S34 + T + U)*
              Pair[ec[5], k[1]] + (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*
              Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
            (2*(-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*Pair[ec[5], k[3]])) - 
         Pair[e[2], ec[4]]*(4*(S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
           Pair[e[1], k[4]]*(4*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + 
             (MH2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], k[3]]) + 
           Pair[e[1], k[2]]*((MH2 - 5*S34 + 3*T - 2*T24 + 3*U)*
              Pair[ec[5], k[3]] + 4*(-MH2 + S34 + T + U)*Pair[ec[5], k[
                4]]))))) + Den[S34, 0]*Den[MH2 - S - T - T14, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (Pair[e[1], ec[5]]*((-MH2 + 2*S + T24 + U)*Pair[e[2], ec[4]] - 
          Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + Pair[ec[4], k[3]]) - 
          Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
        2*(Pair[e[1], k[5]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]])) + (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]]) + Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - 
              Pair[ec[4], k[3]]))*Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]))) - 8*(C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         Pair[ec[4], k[3]]*(2*(-MH2 + S34 + 2*T24)*Pair[e[1], k[5]]*
           Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*T24)*
             Pair[e[2], k[1]] + (-MH2 + S34 + 4*T14)*Pair[e[2], k[3]] + 
            (MH2 + 2*S - S34 - 2*T + 2*T14)*Pair[e[2], k[4]]) + 
          2*(-MH2 + S34 + 2*T24)*Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]]))) + 
        C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
         (2*(MH2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
          Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
            (MH2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
            (-3*MH2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH2 - S34 - 2*U)*
           Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
          8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
      2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
           Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH2 - S34)*Pair[
                ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + Pair[e[2], k[4]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 + S - S34 - T + T14)*
               Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
             ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]]))) + 
        4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*
             ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
          (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*
           Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
           ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
           Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
           (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*
             Pair[ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
            Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (-(Pair[e[1], ec[5]]*(-((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]]) + Pair[e[2], k[4]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[1]] + (5*MH2 + 2*S - 3*S34 - 6*T + 
               2*T14)*Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 - 2*S + S34 - 2*T + 6*
                T14)*Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*
            (-2*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*
              Pair[ec[4], k[3]]))) + 
        2*(Pair[e[1], k[5]]*(-2*(MH2 - S34)*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + Pair[e[2], ec[5]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])) - 2*(MH2 - S34)*Pair[e[1], ec[4]]*
           (Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[5], k[1]] + 
          Pair[e[1], e[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
            (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 
          2*(MH2 - S34)*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 
          8*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*
              Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[4]]*
             Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[
                ec[5], k[3]] - Pair[e[2], k[3]]*Pair[ec[5], k[4]])))))))/
  (MW*SW) + (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu2, Glu5, Glu4, 0, 0]]*
   (Den[T14, 0]*Den[2*MH2 - S34 - T - U, 0]*
     (8*(C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*(MH2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
          Pair[e[1], ec[4]]*((MH2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 
            2*(S - T24)*Pair[e[2], k[3]] + (MH2 + S34 - 3*T + U)*
             Pair[e[2], k[4]]) - 2*(MH2 - S34 - T + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] + C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
          MT2]*(-2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           ((-3*MH2 + S + 4*S34 + 3*T14 + T24)*Pair[e[2], k[1]] - 
            2*(S - T24)*Pair[e[2], k[3]] + (-3*MH2 + S + 4*T + 3*T14 + T24)*
             Pair[e[2], k[4]]) - 2*(MH2 - S34 - T - 2*T14 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]) + 
      (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
           (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
           (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
      4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (2*(-2*(MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(MH2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*
             Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (-2*(MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T + 
                U)*Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*Pair[
                ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) + 
          4*(-S + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(4*(MH2 - S - T14 - T24)*Pair[ec[5], k[1]] + 
            (-7*MH2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[1]]*((-7*MH2 + 3*S + 8*S34 + 5*T14 + 3*T24)*
             Pair[ec[5], k[3]] + 4*(-MH2 + S + T14 + T24)*Pair[ec[5], 
              k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
             ((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH2 + S + T24 + 
                U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
          2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
              (-2*MH2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*((-2*MH2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))) - 
    Den[S34, 0]*Den[MH2 - S - T24 - U, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
         (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
        4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
          Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
         ((-MH2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))) + 8*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         Pair[ec[4], k[3]]*(-(((-2*MH2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - 
             (MH2 - 2*S + S34 + 2*T24 - 2*U)*Pair[e[1], k[3]] + 
             (-3*MH2 + S34 + 4*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 
          2*(-MH2 + S34 + 2*T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          2*(-MH2 + S34 + 2*T)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
          8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]]))) + C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         Pair[ec[4], k[3]]*(((-2*MH2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
            (-MH2 + S34 + 4*T24)*Pair[e[1], k[3]] + 
            (MH2 + 2*S - S34 + 2*T24 - 2*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[5]] + 2*(MH2 - S34 - 2*T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(MH2 - S34 - 2*T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + 
            Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[4]])))) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH2 - S34)*
              Pair[ec[4], k[1]] + 2*(MH2 - S34 - 2*T + 2*T14)*
              Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[2]] + (5*MH2 + 2*S - 3*S34 + 2*
                T24 - 6*U)*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - 2*S + S34 + 6*T24 - 2*
                U)*Pair[ec[4], k[3]]))) + Pair[e[1], ec[4]]*
         ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
          4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
        2*(-2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
            (MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
          2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
           (-2*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 - S34 - 2*T + 2*T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))) - 2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[1], ec[4]]*((MH2 - S34)*(MH2 - 2*S - T - T14)*
           Pair[e[2], ec[5]] + 4*(MH2 - S34)*(Pair[e[2], k[5]]*
             Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
        2*(2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
              2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
              (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
          2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
           ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
           Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))))))/(MW*SW) + 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu4, Glu5, Glu2, 0, 0]]*
   (Den[T14, 0]*Den[2*MH2 - S34 - T - U, 0]*
     (8*(C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
         (-2*(MH2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
          Pair[e[1], ec[4]]*((MH2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 
            2*(S - T24)*Pair[e[2], k[3]] + (MH2 + S34 - 3*T + U)*
             Pair[e[2], k[4]]) - 2*(MH2 - S34 - T + U)*Pair[e[1], e[2]]*
           Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
              Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
             Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[
                ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
         Pair[ec[5], k[3]] + C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
          MT2]*(-2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
           Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*
           ((-3*MH2 + S + 4*S34 + 3*T14 + T24)*Pair[e[2], k[1]] - 
            2*(S - T24)*Pair[e[2], k[3]] + (-3*MH2 + S + 4*T + 3*T14 + T24)*
             Pair[e[2], k[4]]) - 2*(MH2 - S34 - T - 2*T14 + U)*
           Pair[e[1], e[2]]*Pair[ec[4], k[1]] + 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]]) + 
      (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
        16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
       (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
           (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
            Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
          Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
              Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
           (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
           (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
      4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
       (2*(-2*(MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
            Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(MH2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*
             Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*
           (-2*(MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
             Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T + 
                U)*Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*Pair[
                ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) + 
          4*(-S + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
          Pair[e[2], k[4]]*(4*(MH2 - S - T14 - T24)*Pair[ec[5], k[1]] + 
            (-7*MH2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[ec[5], k[3]]) + 
          Pair[e[2], k[1]]*((-7*MH2 + 3*S + 8*S34 + 5*T14 + 3*T24)*
             Pair[ec[5], k[3]] + 4*(-MH2 + S + T14 + T24)*Pair[ec[5], 
              k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
        MT2]*(4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
             Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + (MH2 - S34 - T - U)*
           Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
          2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
            Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
            Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
              Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*
             Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
          Pair[e[1], k[4]]*(-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - Pair[
                e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
             ((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH2 + S + T24 + 
                U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
         (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
          2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
            Pair[e[2], k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
              (-2*MH2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
            Pair[e[2], k[1]]*((-2*MH2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))) - 
    Den[S34, 0]*Den[MH2 - S - T24 - U, 0]*
     ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
       (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
         Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
         (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
         (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
          Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
        4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
         Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
          Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
         ((-MH2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
          4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*
             Pair[ec[5], k[2]]))) + 8*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
         Pair[ec[4], k[3]]*(-(((-2*MH2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - 
             (MH2 - 2*S + S34 + 2*T24 - 2*U)*Pair[e[1], k[3]] + 
             (-3*MH2 + S34 + 4*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 
          2*(-MH2 + S34 + 2*T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
          2*(-MH2 + S34 + 2*T)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
          8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]]))) + C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
         Pair[ec[4], k[3]]*(((-2*MH2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
            (-MH2 + S34 + 4*T24)*Pair[e[1], k[3]] + 
            (MH2 + 2*S - S34 + 2*T24 - 2*U)*Pair[e[1], k[4]])*
           Pair[e[2], ec[5]] + 2*(MH2 - S34 - 2*T14)*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]] + 2*(MH2 - S34 - 2*T14)*Pair[e[1], e[2]]*
           Pair[ec[5], k[2]] + 8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*
                Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + 
            Pair[e[1], k[3]]*(Pair[e[2], k[4]]*Pair[ec[5], k[2]] + 
              Pair[e[2], k[5]]*Pair[ec[5], k[4]])))) - 
      4*C0i[cc1, MH2, S34, 0, MT2, MT2, MT2]*
       (-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH2 - S34)*
              Pair[ec[4], k[1]] + 2*(MH2 - S34 - 2*T + 2*T14)*
              Pair[ec[4], k[3]]) + Pair[e[1], k[4]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[2]] + (5*MH2 + 2*S - 3*S34 + 2*
                T24 - 6*U)*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
            (-4*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - 2*S + S34 + 6*T24 - 2*
                U)*Pair[ec[4], k[3]]))) + Pair[e[1], ec[4]]*
         ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
          4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
            Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
        2*(-2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
            (MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
          2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
           (-2*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 - S34 - 2*T + 2*T14)*
             Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))) - 2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
       (Pair[e[1], ec[4]]*((MH2 - S34)*(MH2 - 2*S - T - T14)*
           Pair[e[2], ec[5]] + 4*(MH2 - S34)*(Pair[e[2], k[5]]*
             Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
        2*(2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
           Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
            T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
           (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
              2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
            Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
              (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
          2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
           Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
           ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
           Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*
           (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
               Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*
             (Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[
                ec[5], k[4]])))))))/(MW*SW) + 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu4, Glu2, Glu5, 0, 0]]*
   (-(Den[2*MH2 - S34 - T - U, 0]*
      (Den[T24, 0]*(8*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (((MH2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
              Pair[e[1], k[3]] + (MH2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                 Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
             Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
         8*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (-(((MH2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
              2*(S - T14)*Pair[e[1], k[3]] + (MH2 + S34 + T - 2*T24 - 3*U)*
               Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
           2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
           2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
           8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
         (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
           16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
          (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], 
                k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
             Pair[ec[4], k[2]]*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
                Pair[e[2], ec[5]] + Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - 
                 Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
              (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[
                ec[5], k[3]]) + Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*
                Pair[ec[5], k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 
           0, MT2, MT2, MT2]*(-4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
           4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
            (-((S - T14)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
             4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
               Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
           8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
            ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*((-S34 + T - T24 + U)*
                Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]]))) - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
           MT2]*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*Pair[
                e[2], ec[4]]) + 4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
           2*(-2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
              Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 2*(MH2 - S34 - T - U)*
              (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
              Pair[ec[4], k[2]] + 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                   Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
               Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
                 Pair[e[2], k[4]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*(-MH2 + S34 + T + U)*
                Pair[ec[5], k[1]] + (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*
                Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
              (2*(-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - 3*S34 + 
                 T - 2*T24 - 3*U)*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
            (4*(S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[1], k[4]]*(4*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + 
               (MH2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], k[3]]) + 
             Pair[e[1], k[2]]*((MH2 - 5*S34 + 3*T - 2*T24 + 3*U)*
                Pair[ec[5], k[3]] + 4*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]])))) + Den[T14, 0]*
        (8*(C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
            (-2*(MH2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
             Pair[e[1], ec[4]]*((MH2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 2*
                (S - T24)*Pair[e[2], k[3]] + (MH2 + S34 - 3*T + U)*
                Pair[e[2], k[4]]) - 2*(MH2 - S34 - T + U)*Pair[e[1], e[2]]*
              Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                 Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
                Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                  Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
            Pair[ec[5], k[3]] + C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, 
             MT2, MT2]*(-2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
              Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-3*MH2 + S + 4*S34 + 
                 3*T14 + T24)*Pair[e[2], k[1]] - 2*(S - T24)*Pair[e[2], 
                 k[3]] + (-3*MH2 + S + 4*T + 3*T14 + T24)*Pair[e[2], k[4]]) - 
             2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[
                1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], 
                  k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], 
                 k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], 
                   k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
            Pair[ec[5], k[3]]) + (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
           16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
          (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
              (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
               Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
             Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
                Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
                 Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
            ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
              (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
              (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
         4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (2*(-2*(MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(MH2 - S34 - T - U)*
              Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
             8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                 Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*(-2*(MH2 - S34 - T - U)*
                (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
               Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
                 (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
           Pair[e[1], ec[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], 
                ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[2], k[4]]*(4*(MH2 - S - T14 - T24)*Pair[ec[5], k[1]] + 
               (-7*MH2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[ec[5], k[3]]) + 
             Pair[e[2], k[1]]*((-7*MH2 + 3*S + 8*S34 + 5*T14 + 3*T24)*
                Pair[ec[5], k[3]] + 4*(-MH2 + S + T14 + T24)*Pair[ec[5], 
                 k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
           MT2]*(4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], 
                 k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + 
             (MH2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*
                Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
             2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                 Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
             Pair[e[1], k[4]]*(-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - 
                  Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
                ((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH2 + S + T24 + 
                   U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
            (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
             2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + Pair[e[2], 
                 k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                 (-2*MH2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
               Pair[e[2], k[1]]*((-2*MH2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                   k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))))) + 
    Den[S34, 0]*(Den[MH2 - S - T - T14, 0]*
       ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, 
            MT2])*(Pair[e[1], ec[5]]*((-MH2 + 2*S + T24 + U)*
             Pair[e[2], ec[4]] - Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[3]]) - Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 
              4*Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
             (2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
            (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))*
             Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]))) - 
        8*(C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
           (2*(-MH2 + S34 + 2*T24)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] - 
            Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*T24)*Pair[e[2], k[1]] + 
              (-MH2 + S34 + 4*T14)*Pair[e[2], k[3]] + (MH2 + 2*S - S34 - 
                2*T + 2*T14)*Pair[e[2], k[4]]) + 2*(-MH2 + S34 + 2*T24)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))) + 
          C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
           (2*(MH2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
            Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
              (MH2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
              (-3*MH2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH2 - S34 - 2*U)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
        2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH2 - S34)*
                 Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
              Pair[e[2], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
                (MH2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
              2*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[2]] + 
                T24*Pair[ec[4], k[3]]))) + 
          4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                  k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*(
                (-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
            (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
             Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
             (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[
                e[2], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
                 Pair[ec[5], k[4]])))) - 4*C0i[cc1, MH2, S34, 0, MT2, MT2, 
          MT2]*(-(Pair[e[1], ec[5]]*(-((MH2 - S34)*(MH2 - 2*S - T24 - U)*Pair[
                e[2], ec[4]]) + Pair[e[2], k[4]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + (5*MH2 + 2*S - 3*S34 - 6*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + (MH2 - 2*S + S34 - 2*T + 6*T14)*
                Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*(-2*(MH2 - S34)*
                Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + 2*(Pair[e[1], k[5]]*(-2*(MH2 - S34)*(
                Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
                (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])) - 
            2*(MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 2*(MH2 - S34)*
             Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))) + 
      Den[MH2 - S - T24 - U, 0]*((4*B0i[bb0, S34, MT2, MT2] - 
          16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
         (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
           ((-MH2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]]))) + 8*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
           Pair[ec[4], k[3]]*(-(((-2*MH2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - 
               (MH2 - 2*S + S34 + 2*T24 - 2*U)*Pair[e[1], k[3]] + 
               (-3*MH2 + S34 + 4*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 
            2*(-MH2 + S34 + 2*T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(-MH2 + S34 + 2*T)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]]))) + C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
           Pair[ec[4], k[3]]*(((-2*MH2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
              (-MH2 + S34 + 4*T24)*Pair[e[1], k[3]] + (MH2 + 2*S - S34 + 
                2*T24 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            2*(MH2 - S34 - 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(MH2 - S34 - 2*T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))) - 4*C0i[cc1, MH2, S34, 0, MT2, MT2, 
          MT2]*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + 2*(MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], 
                 k[3]]) + Pair[e[1], k[4]]*(-4*(MH2 - S34)*Pair[ec[4], 
                 k[2]] + (5*MH2 + 2*S - 3*S34 + 2*T24 - 6*U)*Pair[ec[4], 
                 k[3]]) + Pair[e[1], k[3]]*(-4*(MH2 - S34)*Pair[ec[4], 
                 k[2]] + (MH2 - 2*S + S34 + 6*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + Pair[e[1], ec[4]]*
           ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
          2*(-2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
              (MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
            2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 - S34 - 2*T + 2*T14)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))) - 2*C0i[cc0, MH2, S34, 0, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((MH2 - S34)*(MH2 - 2*S - T - T14)*
             Pair[e[2], ec[5]] + 4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
          2*(2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
             (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
                2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH2 - S34)*
                 Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
              Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
                (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
            2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
             ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
             Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                  Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], 
                  k[4]]))))))))/(MW*SW) + 
 (Alfas2*EL*MT2*Mat[SUNT[Glu1, Glu5, Glu2, Glu4, 0, 0]]*
   (-(Den[2*MH2 - S34 - T - U, 0]*
      (Den[T24, 0]*(8*C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (((MH2 - 3*S34 + T + U)*Pair[e[1], k[2]] + 2*(S - T14)*
              Pair[e[1], k[3]] + (MH2 + S34 + T - 3*U)*Pair[e[1], k[4]])*
            Pair[e[2], ec[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]] + 2*(-MH2 + S34 - T + U)*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]] - 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                 Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
             Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
               Pair[e[2], k[4]]*Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] - 
         8*C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (-(((MH2 - 3*S34 + T - 2*T24 + U)*Pair[e[1], k[2]] + 
              2*(S - T14)*Pair[e[1], k[3]] + (MH2 + S34 + T - 2*T24 - 3*U)*
               Pair[e[1], k[4]])*Pair[e[2], ec[4]]) + 
           2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], ec[4]]*Pair[e[2], k[4]] + 
           2*(MH2 - S34 + T - 2*T24 - U)*Pair[e[1], e[2]]*Pair[ec[4], k[2]] + 
           8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[5]])))*Pair[ec[5], k[3]] + 
         (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
           16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
          (Pair[e[1], ec[5]]*((-S + T14)*Pair[e[2], ec[4]] + 
             4*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + Pair[e[2], k[1]]*
                Pair[ec[4], k[2]])) - 2*(2*(Pair[e[1], k[2]] - Pair[e[1], 
                k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] + 
             Pair[ec[4], k[2]]*(2*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
                Pair[e[2], ec[5]] + Pair[e[1], e[2]]*(2*Pair[ec[5], k[1]] - 
                 Pair[ec[5], k[3]])) + Pair[e[1], ec[4]]*Pair[e[2], k[4]]*
              (2*Pair[ec[5], k[1]] - Pair[ec[5], k[3]])) + 
           Pair[e[2], ec[4]]*(Pair[e[1], k[4]]*(-4*Pair[ec[5], k[2]] + Pair[
                ec[5], k[3]]) + Pair[e[1], k[2]]*(Pair[ec[5], k[3]] + 4*
                Pair[ec[5], k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 
           0, MT2, MT2, MT2]*(-4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - 
             Pair[e[1], k[4]])*Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 
           4*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
            Pair[e[2], ec[5]]*Pair[ec[4], k[2]] + Pair[e[1], ec[5]]*
            (-((S - T14)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[4]]) + 
             4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*Pair[ec[4], k[1]] + 
               Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
           8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*Pair[ec[4], k[2]] + 
                Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + Pair[e[1], k[3]]*
              (Pair[e[2], k[5]]*Pair[ec[4], k[2]] + Pair[e[2], k[4]]*
                Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 4*Pair[e[1], ec[4]]*
            Pair[e[2], k[4]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) + 4*Pair[e[1], e[2]]*
            Pair[ec[4], k[2]]*((-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + 
             (MH2 - S34 - T24 - U)*Pair[ec[5], k[3]]) - 2*Pair[e[2], ec[4]]*
            ((S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + Pair[e[1], k[4]]*
              (2*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + (S34 + T - T24 - U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[2]]*((-S34 + T - T24 + U)*
                Pair[ec[5], k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]]))) - 4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
           MT2]*(Pair[e[1], ec[5]]*(-((S - T14)*(-MH2 + S + T14 + T24)*Pair[
                e[2], ec[4]]) + 4*(MH2 - S34 - T - U)*(Pair[e[2], k[4]]*
                Pair[ec[4], k[1]] + Pair[e[2], k[1]]*Pair[ec[4], k[2]])) + 
           2*(-2*(MH2 - S34 - T - U)*(Pair[e[1], k[2]] - Pair[e[1], k[4]])*
              Pair[e[2], k[4]]*Pair[ec[4], ec[5]] - 2*(MH2 - S34 - T - U)*
              (Pair[e[1], k[2]] - Pair[e[1], k[4]])*Pair[e[2], ec[5]]*
              Pair[ec[4], k[2]] + 8*(-(Pair[e[1], k[5]]*(Pair[e[2], k[3]]*
                   Pair[ec[4], k[2]] + Pair[e[2], k[4]]*Pair[ec[4], k[3]])) + 
               Pair[e[1], k[3]]*(Pair[e[2], k[5]]*Pair[ec[4], k[2]] + 
                 Pair[e[2], k[4]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], ec[4]]*Pair[e[2], k[4]]*(2*(-MH2 + S34 + T + U)*
                Pair[ec[5], k[1]] + (3*MH2 - 3*S34 + T - 2*T24 - 3*U)*
                Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*Pair[ec[4], k[2]]*
              (2*(-MH2 + S34 + T + U)*Pair[ec[5], k[1]] + (3*MH2 - 3*S34 + 
                 T - 2*T24 - 3*U)*Pair[ec[5], k[3]])) - Pair[e[2], ec[4]]*
            (4*(S - T14)*Pair[e[1], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[1], k[4]]*(4*(MH2 - S34 - T - U)*Pair[ec[5], k[2]] + 
               (MH2 + 3*S34 + 3*T - 2*T24 - 5*U)*Pair[ec[5], k[3]]) + 
             Pair[e[1], k[2]]*((MH2 - 5*S34 + 3*T - 2*T24 + 3*U)*
                Pair[ec[5], k[3]] + 4*(-MH2 + S34 + T + U)*Pair[ec[5], 
                 k[4]])))) + Den[T14, 0]*
        (8*(C0i[cc11, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
            (-2*(MH2 - S34 - T + U)*Pair[e[1], k[4]]*Pair[e[2], ec[4]] + 
             Pair[e[1], ec[4]]*((MH2 - 3*S34 + T + U)*Pair[e[2], k[1]] + 2*
                (S - T24)*Pair[e[2], k[3]] + (MH2 + S34 - 3*T + U)*
                Pair[e[2], k[4]]) - 2*(MH2 - S34 - T + U)*Pair[e[1], e[2]]*
              Pair[ec[4], k[1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*
                 Pair[ec[4], k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*
                Pair[ec[4], k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*
                  Pair[ec[4], k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
            Pair[ec[5], k[3]] + C0i[cc12, MH2, 2*MH2 - S34 - T - U, 0, MT2, 
             MT2, MT2]*(-2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], k[4]]*
              Pair[e[2], ec[4]] - Pair[e[1], ec[4]]*((-3*MH2 + S + 4*S34 + 
                 3*T14 + T24)*Pair[e[2], k[1]] - 2*(S - T24)*Pair[e[2], 
                 k[3]] + (-3*MH2 + S + 4*T + 3*T14 + T24)*Pair[e[2], k[4]]) - 
             2*(MH2 - S34 - T - 2*T14 + U)*Pair[e[1], e[2]]*Pair[ec[4], k[
                1]] + 8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], 
                  k[1]]) + Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], 
                 k[1]] + Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], 
                   k[3]] - Pair[e[2], k[3]]*Pair[ec[4], k[5]])))*
            Pair[ec[5], k[3]]) + (4*B0i[bb0, 2*MH2 - S34 - T - U, MT2, MT2] - 
           16*C0i[cc00, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2])*
          (-2*(-2*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*Pair[ec[4], k[2]]) + Pair[ec[4], k[1]]*
              (2*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - Pair[e[2], k[4]]) + 
               Pair[e[1], e[2]]*(2*Pair[ec[5], k[2]] - Pair[ec[5], k[3]])) + 
             Pair[e[1], k[4]]*(2*(Pair[e[2], k[1]] - Pair[e[2], k[4]])*
                Pair[ec[4], ec[5]] + Pair[e[2], ec[4]]*(2*Pair[ec[5], k[2]] - 
                 Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
            ((-S + T24)*Pair[e[2], ec[5]] + Pair[e[2], k[4]]*
              (-4*Pair[ec[5], k[1]] + Pair[ec[5], k[3]]) + Pair[e[2], k[1]]*
              (Pair[ec[5], k[3]] + 4*Pair[ec[5], k[4]]))) - 
         4*C0i[cc1, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, MT2]*
          (2*(-2*(MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], k[1]] - 
               Pair[e[2], k[4]])*Pair[ec[4], k[1]] + 2*(MH2 - S34 - T - U)*
              Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
             8*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                 Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], e[2]]*Pair[ec[4], k[1]]*(2*(-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*
                Pair[ec[5], k[3]]) + Pair[e[1], k[4]]*(-2*(MH2 - S34 - T - U)*
                (Pair[e[2], k[1]] - Pair[e[2], k[4]])*Pair[ec[4], ec[5]] + 
               Pair[e[2], ec[4]]*(2*(-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + 
                 (3*MH2 - 3*S34 - 3*T - 2*T14 + U)*Pair[ec[5], k[3]]))) + 
           Pair[e[1], ec[4]]*(-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], 
                ec[5]]) + 4*(-S + T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + 
             Pair[e[2], k[4]]*(4*(MH2 - S - T14 - T24)*Pair[ec[5], k[1]] + 
               (-7*MH2 + 3*S + 8*T + 5*T14 + 3*T24)*Pair[ec[5], k[3]]) + 
             Pair[e[2], k[1]]*((-7*MH2 + 3*S + 8*S34 + 5*T14 + 3*T24)*
                Pair[ec[5], k[3]] + 4*(-MH2 + S + T14 + T24)*Pair[ec[5], 
                 k[4]]))) - 2*C0i[cc0, MH2, 2*MH2 - S34 - T - U, 0, MT2, MT2, 
           MT2]*(4*(-((MH2 - S34 - T - U)*Pair[e[1], ec[5]]*(Pair[e[2], 
                 k[1]] - Pair[e[2], k[4]])*Pair[ec[4], k[1]]) + 
             (MH2 - S34 - T - U)*Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*
                Pair[ec[4], k[1]] + Pair[e[1], k[4]]*Pair[ec[4], k[2]]) - 
             2*(-(Pair[e[1], k[5]]*Pair[e[2], k[3]]*Pair[ec[4], k[1]]) + 
               Pair[e[1], k[3]]*Pair[e[2], k[5]]*Pair[ec[4], k[1]] + 
               Pair[e[1], k[4]]*(Pair[e[2], k[5]]*Pair[ec[4], k[3]] - 
                 Pair[e[2], k[3]]*Pair[ec[4], k[5]]))*Pair[ec[5], k[3]] + 
             Pair[e[1], e[2]]*Pair[ec[4], k[1]]*((-MH2 + S34 + T + U)*
                Pair[ec[5], k[2]] + (-MH2 + S + T24 + U)*Pair[ec[5], k[3]]) + 
             Pair[e[1], k[4]]*(-((MH2 - S34 - T - U)*(Pair[e[2], k[1]] - 
                  Pair[e[2], k[4]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[4]]*
                ((-MH2 + S34 + T + U)*Pair[ec[5], k[2]] + (-MH2 + S + T24 + 
                   U)*Pair[ec[5], k[3]]))) + Pair[e[1], ec[4]]*
            (-((S - T24)*(-MH2 + S + T14 + T24)*Pair[e[2], ec[5]]) - 
             2*((S - T24)*Pair[e[2], k[3]]*Pair[ec[5], k[3]] + Pair[e[2], 
                 k[4]]*(2*(MH2 - S34 - T - U)*Pair[ec[5], k[1]] + 
                 (-2*MH2 + S + 2*S34 + T24 + 2*U)*Pair[ec[5], k[3]]) + 
               Pair[e[2], k[1]]*((-2*MH2 + S + 2*T + T24 + 2*U)*Pair[ec[5], 
                   k[3]] + 2*(-MH2 + S34 + T + U)*Pair[ec[5], k[4]]))))))) + 
    Den[S34, 0]*(Den[MH2 - S - T - T14, 0]*
       ((4*B0i[bb0, S34, MT2, MT2] - 16*C0i[cc00, MH2, S34, 0, MT2, MT2, 
            MT2])*(Pair[e[1], ec[5]]*((-MH2 + 2*S + T24 + U)*
             Pair[e[2], ec[4]] - Pair[e[2], k[5]]*(-4*Pair[ec[4], k[1]] + 
              Pair[ec[4], k[3]]) - Pair[e[2], k[1]]*(Pair[ec[4], k[3]] + 
              4*Pair[ec[4], k[5]])) + 2*(Pair[e[1], k[5]]*
             (2*(Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]])) + 
            (2*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - Pair[e[2], k[5]]) + 
              Pair[e[1], e[2]]*(2*Pair[ec[4], k[2]] - Pair[ec[4], k[3]]))*
             Pair[ec[5], k[1]] - 2*Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[
                ec[5], k[1]] + Pair[e[1], k[5]]*Pair[ec[5], k[2]]))) - 
        8*(C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
           (2*(-MH2 + S34 + 2*T24)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] - 
            Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*T24)*Pair[e[2], k[1]] + 
              (-MH2 + S34 + 4*T14)*Pair[e[2], k[3]] + (MH2 + 2*S - S34 - 
                2*T + 2*T14)*Pair[e[2], k[4]]) + 2*(-MH2 + S34 + 2*T24)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))) + 
          C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*Pair[ec[4], k[3]]*
           (2*(MH2 - S34 - 2*U)*Pair[e[1], k[5]]*Pair[e[2], ec[5]] + 
            Pair[e[1], ec[5]]*((-2*MH2 + 2*S34 + 4*U)*Pair[e[2], k[1]] - 
              (MH2 - 2*S + S34 - 2*T + 2*T14)*Pair[e[2], k[3]] + 
              (-3*MH2 + S34 + 4*T)*Pair[e[2], k[4]]) + 2*(MH2 - S34 - 2*U)*
             Pair[e[1], e[2]]*Pair[ec[5], k[1]] + 
            8*(-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]])))) - 
        2*C0i[cc0, MH2, S34, 0, MT2, MT2, MT2]*
         (Pair[e[1], ec[5]]*((MH2 - S34)*(MH2 - 2*S - T24 - U)*
             Pair[e[2], ec[4]] - 2*(Pair[e[2], k[3]]*(-2*(MH2 - S34)*
                 Pair[ec[4], k[1]] + 2*T14*Pair[ec[4], k[3]]) + 
              Pair[e[2], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
                (MH2 + S - S34 - T + T14)*Pair[ec[4], k[3]]) + 
              2*Pair[e[2], k[1]]*((-MH2 + S34)*Pair[ec[4], k[2]] + 
                T24*Pair[ec[4], k[3]]))) + 
          4*(Pair[e[1], k[5]]*(-((MH2 - S34)*(Pair[e[2], k[1]] - Pair[e[2], 
                  k[5]])*Pair[ec[4], ec[5]]) + Pair[e[2], ec[5]]*(
                (-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])) - 
            (MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             ((-MH2 + S34)*Pair[ec[4], k[2]] + T24*Pair[ec[4], k[3]])*
             Pair[ec[5], k[1]] + (MH2 - S34)*Pair[e[2], ec[4]]*
             (Pair[e[1], k[2]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*Pair[
                ec[5], k[2]]) + 2*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + Pair[e[1], k[3]]*Pair[
                e[2], k[4]]*Pair[ec[5], k[1]] + Pair[e[1], k[5]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[3]] - Pair[e[2], k[3]]*
                 Pair[ec[5], k[4]])))) - 4*C0i[cc1, MH2, S34, 0, MT2, MT2, 
          MT2]*(-(Pair[e[1], ec[5]]*(-((MH2 - S34)*(MH2 - 2*S - T24 - U)*Pair[
                e[2], ec[4]]) + Pair[e[2], k[4]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + (5*MH2 + 2*S - 3*S34 - 6*T + 2*T14)*
                Pair[ec[4], k[3]]) + Pair[e[2], k[3]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + (MH2 - 2*S + S34 - 2*T + 6*T14)*
                Pair[ec[4], k[3]]) + 2*Pair[e[2], k[1]]*(-2*(MH2 - S34)*
                Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + 2*(Pair[e[1], k[5]]*(-2*(MH2 - S34)*(
                Pair[e[2], k[1]] - Pair[e[2], k[5]])*Pair[ec[4], ec[5]] + 
              Pair[e[2], ec[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
                (MH2 - S34 + 2*T24 - 2*U)*Pair[ec[4], k[3]])) - 
            2*(MH2 - S34)*Pair[e[1], ec[4]]*(Pair[e[2], k[1]] - 
              Pair[e[2], k[5]])*Pair[ec[5], k[1]] + Pair[e[1], e[2]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[2]] + (MH2 - S34 + 2*T24 - 2*U)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[1]] + 2*(MH2 - S34)*
             Pair[e[2], ec[4]]*(Pair[e[1], k[2]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*Pair[ec[5], k[2]]) + 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*Pair[e[2], k[3]]*Pair[ec[5], k[1]]) + 
              Pair[e[1], k[3]]*Pair[e[2], k[4]]*Pair[ec[5], k[1]] + 
              Pair[e[1], k[5]]*(Pair[e[2], k[4]]*Pair[ec[5], k[3]] - 
                Pair[e[2], k[3]]*Pair[ec[5], k[4]]))))) + 
      Den[MH2 - S - T24 - U, 0]*((4*B0i[bb0, S34, MT2, MT2] - 
          16*C0i[cc00, MH2, S34, 0, MT2, MT2, MT2])*
         (4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], k[5]]*
           Pair[ec[4], ec[5]] + 2*Pair[e[1], ec[5]]*Pair[e[2], k[5]]*
           (2*Pair[ec[4], k[1]] - Pair[ec[4], k[3]]) - Pair[e[2], ec[5]]*
           (Pair[e[1], k[5]]*(-4*Pair[ec[4], k[2]] + Pair[ec[4], k[3]]) + 
            Pair[e[1], k[2]]*(Pair[ec[4], k[3]] + 4*Pair[ec[4], k[5]])) + 
          4*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*Pair[e[2], ec[4]]*
           Pair[ec[5], k[2]] + 2*Pair[e[1], e[2]]*(2*Pair[ec[4], k[1]] - 
            Pair[ec[4], k[3]])*Pair[ec[5], k[2]] + Pair[e[1], ec[4]]*
           ((-MH2 + 2*S + T + T14)*Pair[e[2], ec[5]] - 
            4*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + Pair[e[2], k[1]]*Pair[
                ec[5], k[2]]))) + 8*(C0i[cc11, MH2, S34, 0, MT2, MT2, MT2]*
           Pair[ec[4], k[3]]*(-(((-2*MH2 + 2*S34 + 4*T)*Pair[e[1], k[2]] - 
               (MH2 - 2*S + S34 + 2*T24 - 2*U)*Pair[e[1], k[3]] + 
               (-3*MH2 + S34 + 4*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]]) + 
            2*(-MH2 + S34 + 2*T)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(-MH2 + S34 + 2*T)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]]))) + C0i[cc12, MH2, S34, 0, MT2, MT2, MT2]*
           Pair[ec[4], k[3]]*(((-2*MH2 + 2*S34 + 4*T14)*Pair[e[1], k[2]] + 
              (-MH2 + S34 + 4*T24)*Pair[e[1], k[3]] + (MH2 + 2*S - S34 + 
                2*T24 - 2*U)*Pair[e[1], k[4]])*Pair[e[2], ec[5]] + 
            2*(MH2 - S34 - 2*T14)*Pair[e[1], ec[5]]*Pair[e[2], k[5]] + 
            2*(MH2 - S34 - 2*T14)*Pair[e[1], e[2]]*Pair[ec[5], k[2]] + 
            8*(-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))) - 4*C0i[cc1, MH2, S34, 0, MT2, MT2, 
          MT2]*(-(Pair[e[2], ec[5]]*(Pair[e[1], k[2]]*(-4*(MH2 - S34)*
                Pair[ec[4], k[1]] + 2*(MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], 
                 k[3]]) + Pair[e[1], k[4]]*(-4*(MH2 - S34)*Pair[ec[4], 
                 k[2]] + (5*MH2 + 2*S - 3*S34 + 2*T24 - 6*U)*Pair[ec[4], 
                 k[3]]) + Pair[e[1], k[3]]*(-4*(MH2 - S34)*Pair[ec[4], 
                 k[2]] + (MH2 - 2*S + S34 + 6*T24 - 2*U)*Pair[ec[4], 
                 k[3]]))) + Pair[e[1], ec[4]]*
           ((MH2 - S34)*(MH2 - 2*S - T - T14)*Pair[e[2], ec[5]] + 
            4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], k[1]] + 
              Pair[e[2], k[1]]*Pair[ec[5], k[2]])) + 
          2*(-2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] + Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
              (MH2 - S34 - 2*T + 2*T14)*Pair[ec[4], k[3]]) - 
            2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] + Pair[e[1], e[2]]*
             (-2*(MH2 - S34)*Pair[ec[4], k[1]] + (MH2 - S34 - 2*T + 2*T14)*
               Pair[ec[4], k[3]])*Pair[ec[5], k[2]] - 8*Pair[ec[4], k[3]]*
             (-(Pair[e[1], k[4]]*(Pair[e[2], k[3]]*Pair[ec[5], k[2]] + 
                 Pair[e[2], k[5]]*Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(
                Pair[e[2], k[4]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                 Pair[ec[5], k[4]])))) - 2*C0i[cc0, MH2, S34, 0, MT2, MT2, 
          MT2]*(Pair[e[1], ec[4]]*((MH2 - S34)*(MH2 - 2*S - T - T14)*
             Pair[e[2], ec[5]] + 4*(MH2 - S34)*(Pair[e[2], k[5]]*Pair[ec[5], 
                k[1]] + Pair[e[2], k[1]]*Pair[ec[5], k[2]])) - 
          2*(2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], k[5]]*Pair[ec[4], ec[5]] - 2*Pair[e[1], ec[5]]*
             Pair[e[2], k[5]]*((-MH2 + S34)*Pair[ec[4], k[1]] + 
              T14*Pair[ec[4], k[3]]) + Pair[e[2], ec[5]]*
             (Pair[e[1], k[2]]*(-2*(MH2 - S34)*Pair[ec[4], k[1]] + 
                2*T14*Pair[ec[4], k[3]]) + Pair[e[1], k[3]]*(-2*(MH2 - S34)*
                 Pair[ec[4], k[2]] + 2*T24*Pair[ec[4], k[3]]) + 
              Pair[e[1], k[4]]*(-2*(MH2 - S34)*Pair[ec[4], k[2]] + 
                (MH2 + S - S34 + T24 - U)*Pair[ec[4], k[3]])) + 
            2*(MH2 - S34)*(Pair[e[1], k[2]] - Pair[e[1], k[5]])*
             Pair[e[2], ec[4]]*Pair[ec[5], k[2]] - 2*Pair[e[1], e[2]]*
             ((-MH2 + S34)*Pair[ec[4], k[1]] + T14*Pair[ec[4], k[3]])*
             Pair[ec[5], k[2]] + 4*Pair[ec[4], k[3]]*(-(Pair[e[1], k[4]]*
                (Pair[e[2], k[3]]*Pair[ec[5], k[2]] + Pair[e[2], k[5]]*
                  Pair[ec[5], k[3]])) + Pair[e[1], k[3]]*(Pair[e[2], k[4]]*
                 Pair[ec[5], k[2]] + Pair[e[2], k[5]]*Pair[ec[5], 
                  k[4]]))))))))/(MW*SW)
