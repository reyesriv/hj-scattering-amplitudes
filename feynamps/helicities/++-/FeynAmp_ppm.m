(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*(-c124 + c142)*EL*GS*MT2*
      (-(Sqrt[S]*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
           Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2 + (4*MH2*MT2 - 4*MT2*(S + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
             (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])*
          Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]*
         (MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
           2)) - (Sqrt[S]*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^
           2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
          Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)]^2 + (4*MH2*MT2 - 4*MT2*(S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           ScalarD0[0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])*
         Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]*
         (MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
           2)) - (Sqrt[2]*Sqrt[S]*
         (-(2*MH*Sqrt[MH2 - 4*MT2]*S*(-1 + Cos[\[Theta]3])*(MH2 - S - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                 MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
             2*S*Sqrt[S*(-4*MT2 + S)]*(-1 + Cos[\[Theta]3])*
              (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
             (MH2 - S)*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2)*(2*MH2*MT2 - 2*MT2*(S + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^
               2 - (2*MH2^3*MT2 - MH2^2*(4*MT2*(S + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                 (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + (S*(-1 + Cos[\[Theta]3])*(2*MT2*(S + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2) - (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + MH2*
                ((S*(-1 + Cos[\[Theta]3])*(S + ((-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2 + 2*MT2*(S^2 + 
                   (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 + ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                    4)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^
                2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2) + 
             2*Sqrt[2]*(MH2 - S)*S*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                    (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                  Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)] + (MH2 - S)*(2*MH2^2*MT2 - MH2*
                (MT2*(6*S + 2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]]) + (S*(-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2) + 
               (S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 + 2*
                MT2*(2*S^2 + (3*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]])/2 + ((-1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4))*
              Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2 + (MH2 - S)*(MH2 - ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(2*MH2*MT2 - 2*MT2*
                (S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                  (2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                      (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                    Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2) + (MH2 - S)*S*
              (-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(4*MH2*MT2 - 4*MT2*
                (S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, 
               MH2, S, ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2, MT, MT, MT, MT])/(8*(MH2 - S)*S*
            (-1 + Cos[\[Theta]3])*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
          (4*MH*Sqrt[MH2 - 4*MT2]*S*(MH2 - S + ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
            4*Sqrt[S*(-4*MT2 + S)]*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             (2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 + 
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             (2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2) + 
            2*Sqrt[2]*S*(-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[
                 Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*
                (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]*
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)] - (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2)*(2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
              (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 - (2*MH2^2*MT2 - MH2*(MT2*(6*S - 
                  2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
                (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2) + (S*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
               4 + 2*MT2*(2*S^2 - (3*S*(1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2 + ((1 + Cos[\[Theta]3])^2*
                  Kallen\[Lambda][S, MH2, 0])/4))*
             (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
              Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2) - S*(1 + Cos[\[Theta]3])*
             (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)*(4*MH2*MT2 - 4*MT2*(S - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
              (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
              -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
              MT, MT, MT])/(16*S*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)))*Sin[\[Theta]3]^2)/Abs[Sin[\[Theta]3]] - 
       (-(((((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (Sqrt[MH2 - 4*MT2]*S*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                (2*MT2)] - MH*Sqrt[S*(-4*MT2 + S)]*Log[(2*MT2 - S + 
                 Sqrt[S*(-4*MT2 + S)])/(2*MT2)])*Sin[\[Theta]3]^2)/
           (Sqrt[2]*MH*(MH2 - S)*Sqrt[S]*Abs[Sin[\[Theta]3]])) - 
         (Sqrt[S]*(2 + Eps^(-1) + Log[Mu^2/MT2] + (Sqrt[S*(-4*MT2 + S)]*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/S)*
           Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
         (Sqrt[S]*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)*
           Sin[\[Theta]3]^2)/(4*Sqrt[2]*(MH2 - S)*Abs[Sin[\[Theta]3]]) + 
         (Sqrt[S]*(3*MH2 + MH2/Eps - 3*S - S/Eps + MH*Sqrt[MH2 - 4*MT2]*
             Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
            MT2*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 + 
            (MH2 - S)*Log[Mu^2/MT2] - Sqrt[S*(-4*MT2 + S)]*
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
            MT2*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)*
           Sin[\[Theta]3]^2)/(Sqrt[2]*(MH2 - S)*Abs[Sin[\[Theta]3]]))/S + 
       ((S^(3/2)*(-((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 - 
            MH2*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 + S*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                     (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                  Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)]^2 - 
            ((MH2 - S)*(1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/2)*
           Sin[\[Theta]3]^2)/(2*Sqrt[2]*(MH2 - S)*Abs[Sin[\[Theta]3]]*
           (1 + Cos[\[Theta]3])*(MH2 - S + ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
         (Sqrt[S]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]])/2)*(-((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2)/2 - MH2*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                     (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                  Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)]^2 + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2)/2 - ((1 + Cos[\[Theta]3])*
              (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, 
               MH2, ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2, -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2, MT, MT, MT, MT])/2)*Sin[\[Theta]3]^2)/
          (2*Sqrt[2]*Abs[Sin[\[Theta]3]]*(1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]))/2 + 
       4*(-(Sqrt[S]*((4*MH*Sqrt[MH2 - 4*MT2]*S*(MH2 - S - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 4*
                Sqrt[S*(-4*MT2 + S)]*(MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-MH2 + S + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
               (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*(2*MH2*MT2 - 2*MT2*(S + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                 (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 + 
               (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*(2*MH2*MT2 - 2*MT2*(S + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                 (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2) + 2*
                Sqrt[2]*S*(-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                 (-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                   Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)/(2*MT2)] - (MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(2*MH2*MT2 - 
                 2*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*
                Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                        ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2 - (2*MH2^2*MT2 - MH2*(MT2*(6*S + 
                     2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]]) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                       S, MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4 + 2*MT2*(2*S^2 + 
                   (3*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 + ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                      MH2, 0])/4))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                    (2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                        (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                      Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])/2)/(2*MT2)]^2) + S*(-1 + 
                 Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*(4*MH2*MT2 - 
                 4*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/
              (16*S*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/2)*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
             (-2*MH*Sqrt[MH2 - 4*MT2]*S*(1 + Cos[\[Theta]3])*(MH2 - S + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                   MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] - 2*S*
                Sqrt[S*(-4*MT2 + S)]*(1 + Cos[\[Theta]3])*(-MH2 + S - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 - S + 
                   Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - (MH2 - S)*(MH2 + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2) - 
                 (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
               (2*MH2^3*MT2 - MH2^2*(4*MT2*(S - ((1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
                   (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2) - (S*(1 + Cos[\[Theta]3])*(2*MT2*(S - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2) + (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                 MH2*(-(S*(1 + Cos[\[Theta]3])*(S - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])/2 + 2*MT2*(S^2 - 
                     (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                        MH2, 0])/4)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                     2*MT2)/(2*MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + 
                        S)])/(2*MT2)]^2) + 2*Sqrt[2]*(MH2 - S)*S*
                (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]*
                Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)] + (MH2 - S)*(2*MH2^2*MT2 - 
                 MH2*(MT2*(6*S - 2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                        S, MH2, 0]]) - (S*(1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2) + 
                 (S*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 + 
                 2*MT2*(2*S^2 - (3*S*(1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2 + ((1 + Cos[\[Theta]3])^
                      2*Kallen\[Lambda][S, MH2, 0])/4))*
                Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 + (MH2 - S)*(MH2 + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2) - 
                 (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                      Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)/(2*MT2)]^2) - (MH2 - S)*S*
                (1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(4*MH2*MT2 - 
                 4*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/
              (8*(MH2 - S)*S*(1 + Cos[\[Theta]3])*(-MH2 + S - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                2*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
             (-(MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(1 + 
                  Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2 + ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Kallen\[Lambda][S, 
                  MH2, 0]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                   (2*MT2)]) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                 (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Kallen\[Lambda][S, 
                  MH2, 0]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                        ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)])/Sqrt[2] - (MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2 + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
                 ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                    S, MH2, 0])/4)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                       (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                     Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 - (2*MH2^3*MT2 - 
                 MH2^2*(4*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2) - 
                   ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4) - ((-1 + Cos[\[Theta]3])*
                   (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                   (2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2) + 
                    ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4))/4 + 
                 MH2*(-((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0])/4 + 
                   2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 
                        0])/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                       Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^
                        2*Kallen\[Lambda][S, MH2, 0])/4)))*
                (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)]^2) + Sqrt[2]*(-1 + Cos[\[Theta]3])*
                (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                 -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])]*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]]*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)] + (MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(2*MH2^2*MT2 + 
                 ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                 MH2*(MT2*(3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]] - 2*(1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]]) - ((-1 + Cos[\[Theta]3])*
                     (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4) + 
                 2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                    2 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4))*
                Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 + (MH2 - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*(2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4)*
                (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                      Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)/(2*MT2)]^2) - ((-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2 + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Kallen\[Lambda][S, MH2, 0]*(4*MH2*MT2 - 4*MT2*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                  -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                  MT, MT, MT, MT])/2)/(4*(-1 + Cos[\[Theta]3])*(1 + 
                Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-MH2 + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                2*Kallen\[Lambda][S, MH2, 0]))*Sin[\[Theta]3]^2)/
          (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
         (-(-2*MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*(MH2 - S + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                  MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] - 
              2*Sqrt[S*(-4*MT2 + S)]*(1 + Cos[\[Theta]3])*(-MH2 + S - 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 - S + 
                  Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - (MH2 - S)*(2*MH2*MT2 - 
                2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
                  (2*MT2)]^2 - (2*MH2^2*MT2 - MH2*(MT2*(4*S - 
                    3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]]) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2) - (S^2*(1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 2*MT2*(S^2 - 
                  (3*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                   2))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^
                 2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2) + 
              2*Sqrt[2]*(MH2 - S)*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)] - (MH2 - S)*(2*MH2*MT2 - 2*MT2*(S - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                       (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2 + (MH2 - S)*(2*MH2*MT2 - 
                2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                    2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[
                      -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2) - (MH2 - S)*S*(1 + Cos[\[Theta]3])*(
                4*MH2*MT2 - 4*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2) - 
                (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
                -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                MT, MT, MT, MT])/(8*(MH2 - S)*(1 + Cos[\[Theta]3])*
             (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
           (-(MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0]*Log[
                (-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]) - 
             Sqrt[2]*(1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)] + (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                   S, MH2, 0]])/2)*(2*MH2^2*MT2 - ((-1 + Cos[\[Theta]3])^2*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
               MH2*(MT2*(2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]] - 3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]]) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4) + 2*MT2*
                (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                 (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/2))*
              Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2 + (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]])/2)*(MH2 + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              (2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4)*
              (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
               Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                       ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)/(2*MT2)]^2) - ((-1 + Cos[\[Theta]3])*(1 + 
                Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Kallen\[Lambda][S, MH2, 
                0]*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)])/Sqrt[2] - 
             (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*(2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
               ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                  MH2, 0])/4)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                      (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2 - (2*MH2^3*MT2 - MH2^2*
                (4*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + 
                    Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4) - 
               ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                  MH2, 0]*(2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                        S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])/2) + 
                  ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                     S, MH2, 0])/4))/4 + MH2*(-((-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Kallen\[Lambda][S, MH2, 0])/4 + 2*MT2*
                  (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                   ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4)))*
              (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
               Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2) - ((-1 + Cos[\[Theta]3])*(1 + 
                Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2 + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Kallen\[Lambda][S, MH2, 0]*(4*MH2*MT2 - 4*MT2*
                 (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                  Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                MT, MT, MT, MT])/2)/(4*(-1 + Cos[\[Theta]3])*
             (1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
             (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)*Kallen\[Lambda][S, MH2, 0]))*(-(Sqrt[S]*Sin[\[Theta]3]^2)/
            (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
             Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
             Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
             (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
            (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]))) + 
       (-(ScalarD0[0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT]*
           (-(Sqrt[S]*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*
              Abs[Sin[\[Theta]3]]) + (Sqrt[S]*(-MH2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
             (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) + (Sqrt[S]*Cos[\[Theta]3/2]*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
              Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Abs[Sin[
                \[Theta]3]]) + (Sqrt[S]*(1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
              (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(4*Sqrt[2]*Abs[Sin[\[Theta]3]]))) - 
         ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT]*
          (-(Sqrt[S]*(2*S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*
             Abs[Sin[\[Theta]3]]) - (Sqrt[S]*(-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2)/
            (4*Sqrt[2]*Abs[Sin[\[Theta]3]]) + (Sqrt[S]*Cos[\[Theta]3/2]*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
             Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
           (Sqrt[S]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
             Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*
             Abs[Sin[\[Theta]3]])))/4 + 
       (((-((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 - 
            MH2*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 + S*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                     (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                  Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)]^2 - 
            ((MH2 - S)*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]]*ScalarD0[0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/2)*
           ((Sqrt[S]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*
              Abs[Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*(-1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*
              (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
            ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(8*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])))/((MH2 - S)*(1 + Cos[\[Theta]3])*
           (MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
         ((((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 - 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 - 
            MH2*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 + S*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                    (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                  Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)]^2 + 
            ((MH2 - S)*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]]*ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/2)*
           (-(S^(3/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
            (Sqrt[S]*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3]^2)/(4*Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
            (Cos[\[Theta]3/2]*(-1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 
               0]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*
                Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
            ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(8*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])))/((MH2 - S)*(-1 + Cos[\[Theta]3])*
           (MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]))/2 + 
       (-(S^(3/2)*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 - 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 - 
             MH2*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2 + S*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                     (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                   Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2 + ((MH2 - S)*(-1 + 
                Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/2)*
            Sin[\[Theta]3]^2)/(2*Sqrt[2]*(MH2 - S)*Abs[Sin[\[Theta]3]]*
           (-1 + Cos[\[Theta]3])*(MH2 - S - ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
         (Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2*((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*
              Abs[Sin[\[Theta]3]]) - (Sqrt[2]*Cos[\[Theta]3/2]*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
              Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[S]*Abs[Sin[
                \[Theta]3]])))/((-1 + Cos[\[Theta]3])*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
         ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)*
           ((Sqrt[S]*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
            (Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*
                Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])))/(2*(MH2 - S)) - 
         ((-((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
            MH2*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2)/2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                S, MH2, 0]]*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                      (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2)/2 - ((-1 + Cos[\[Theta]3])*
              (1 + Cos[\[Theta]3])*(-MH2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 
               0]*ScalarD0[0, 0, 0, MH2, ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, -((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/4)*
           ((Sqrt[S]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*
              Abs[Sin[\[Theta]3]]) - (Sqrt[S]*(MH2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
             (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*
              (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
               (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
              Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*
              (2*S - (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
              (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
            (Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*
                Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/((-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]))/2 + 
       (2*((2 + Eps^(-1) + Log[Mu^2/MT2] - 
            (Sqrt[2]*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                 Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)])/((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]]))*(-((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[
                Sin[\[Theta]3]])) + (Sqrt[2]*Cos[\[Theta]3/2]*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
              Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[S]*Abs[Sin[
                \[Theta]3]]) + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])) - ((3*MH2 + MH2/Eps + 
             (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              (2*Eps) + MH*Sqrt[MH2 - 4*MT2]*Log[(-MH2 + 
                 MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
             MT2*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 + 
             (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)*Log[Mu^2/MT2] - (Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Log[
                (2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)])/Sqrt[2] - 
             MT2*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2)*(-((Sqrt[S]*Sin[\[Theta]3]^2)/(
                Sqrt[2]*Abs[Sin[\[Theta]3]])) + (Sqrt[2]*Cos[\[Theta]3/2]*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[
                \[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + 
                I*Sin[\[Phi]3]))/(Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[
                Sin[\[Theta]3]])))/(MH2 + ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
          ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2)*(-(Sqrt[S]*(-MH2 - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
              (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]*Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(-MH2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 (4*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (2*Sqrt[2]*Sqrt[S]) - (Sqrt[S]*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
                I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (4*Sqrt[2]*Abs[Sin[\[Theta]3]])))/
           (2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)) + (2*(-(Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                   MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/2 - 
             (MH*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2])*((Sqrt[S]*Sin[\[Theta]3]^2*(MH2 + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                (Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*S)))/
              (Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[2]*Cos[\[Theta]3/2]*(
                MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[
                \[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + 
                I*Sin[\[Phi]3]))/(Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
             ((1 + Cos[\[Theta]3])*(-3*S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
               (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[
                Sin[\[Theta]3]])))/(MH*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
          (((Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                 MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
              2 + (MH*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Log[
                (2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)])/Sqrt[2])*Sin[\[Theta]3]*
            (((1 + Cos[\[Theta]3])*(-S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(Sqrt[2]*MH*Sqrt[S]*
            (1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) + 
          ((-((1 + Cos[\[Theta]3])*(-MH2 - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2 + (MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                  MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/2 + 
             (MT2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
             (MH2*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2] - (MT2*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 + 
                   Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                  (2*MT2)]^2)/2)*Sin[\[Theta]3]*
            (((1 + Cos[\[Theta]3])*(-S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            (1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2)))/
        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       (2*((2*((Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                 (2*MT2)])/2 - (MH*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                  Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2])*(-2*(MH2 - ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              ((Sqrt[S]*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                 Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
                 (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
                 Abs[Sin[\[Theta]3]])) + (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(-3*S - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (2*Sqrt[2]*Sqrt[S])))/(MH*(-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
          ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2)*(-((MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2)*((Sqrt[S]*Sin[\[Theta]3]^
                   2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + 
                   I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[Sin[
                    \[Theta]3]]))) + (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[
                \[Theta]3]*((2*S*Cos[\[Theta]3/2]*Sin[\[Theta]3/2]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(4*Abs[Sin[\[Theta]3]]))*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S])))/
           (2*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)) - (2 + Eps^(-1) + Log[Mu^2/MT2] + 
            (Sqrt[2]*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                 Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)])/((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]]))*((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*
              Abs[Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][
                S, MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*
              (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])) + ((3*MH2 + MH2/Eps - 
             (3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              (2*Eps) + MH*Sqrt[MH2 - 4*MT2]*Log[(-MH2 + 
                 MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
             MT2*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 + 
             (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)*Log[Mu^2/MT2] - (Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                  Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2] - MT2*Log[(2*MT2 + 
                  Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)]^2)*((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[
                Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, 
                 MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - 
                I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]*Abs[
                Sin[\[Theta]3]])))/(MH2 - ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
          (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]*
            (((-(Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                     (2*MT2)])/2 + (MH*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
                  Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)])/Sqrt[2])*((-2*Cos[\[Theta]3/2]*
                  (-S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]])))/
              (MH*(-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]) + ((((-1 + Cos[\[Theta]3])*(-MH2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 
                        4*MT2] + 2*MT2)/(2*MT2)])/2 - 
                (MT2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
                (MH2*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
                  Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)])/Sqrt[2] + (MT2*(-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 + 
                      Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)/(2*MT2)]^2)/2)*((-2*Cos[\[Theta]3/2]*
                  (-S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]])))/
              ((-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]))*(Cos[\[Phi]3] + 
             I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S])))/((-1 + Cos[\[Theta]3])*
         Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
       (2*(-2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2)*Kallen\[Lambda][S, MH2, 0] - MH*Sqrt[MH2 - 4*MT2]*
           (-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (MH2 - (-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] + 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Kallen\[Lambda][S, MH2, 0]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
              2*MT2)/(2*MT2)] + Sqrt[2]*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            2*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/4 + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 - Sqrt[2]*(-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           (-MH2^2 + MH2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0])/4)*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            Kallen\[Lambda][S, MH2, 0]*(MH2^3 - MH2^2*
              ((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
             ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0]^(3/2))/8 + MH2*(((-1 + Cos[\[Theta]3])^2*
                 Kallen\[Lambda][S, MH2, 0])/2 - ((-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/2 + 
               ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4))*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*(2*MH2*MT2 - 
             2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*((Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]*(((1 + Cos[\[Theta]3])*(S - (1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])*Sin[\[Theta]3]*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) - 
             (3*Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]) - 
          (Cos[\[Theta]3/2]*(MH2 - (3*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - 
             I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/((-1 + Cos[\[Theta]3])^2*
         (1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         Kallen\[Lambda][S, MH2, 0]^(3/2)) + 
       (4*((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] + 
          (Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/Sqrt[2])*
         (-((Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
             Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - 
              I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
            (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])) + 
          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]])))/((-1 + Cos[\[Theta]3])^2*
         Kallen\[Lambda][S, MH2, 0]) + 
       ((2*MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
          2*Sqrt[2]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(MH2 + ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0])/4)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                  (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 + 
          (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0])/4)*
           (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2) + Sqrt[2]*(-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(2*MH2*MT2 - 2*MT2*(((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0])/4)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                   (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 - (2*MH2^2*MT2 + 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                MH2, 0]^(3/2))/8 - MH2*(MT2*(3*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]] - 2*(1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) - ((-1 + Cos[\[Theta]3])*
                (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4) + 
            2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
              (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                 S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/4))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Kallen\[Lambda][S, MH2, 0]*(4*MH2*MT2 - 
             4*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*(-((Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - 
              I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
            (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])) + 
          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]])))/(2*(-1 + Cos[\[Theta]3])*
         (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
         Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       ((2*MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(MH2 - S - 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
          2*Sqrt[S*(-4*MT2 + S)]*(-1 + Cos[\[Theta]3])*(-MH2 + S + 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
          (MH2 - S)*(2*MH2*MT2 - 2*MT2*(S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
            (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
          (2*MH2^2*MT2 - MH2*(MT2*(4*S + 3*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (S*(-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
            (S^2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
            2*MT2*(S^2 + (3*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2 + ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/2))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^
             2) + 2*Sqrt[2]*(MH2 - S)*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - (MH2 - S)*(2*MH2*MT2 - 
            2*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                  (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 + (MH2 - S)*(2*MH2*MT2 - 
            2*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2) + (MH2 - S)*S*(-1 + Cos[\[Theta]3])*
           (4*MH2*MT2 - 4*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                 Kallen\[Lambda][S, MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*((Sqrt[S]*Sin[\[Theta]3]^2)/(2*Sqrt[2]*
            Abs[Sin[\[Theta]3]]) - (Cos[\[Theta]3/2]*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
            Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/
        (2*(MH2 - S)*(-1 + Cos[\[Theta]3])*
         (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
            2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
          Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)*
         (-(Sqrt[S]*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
          (Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/(4*(MH2 - S)) + 
       ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
          Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)]^2 - Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                  (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[
                2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)/(2*MT2)]^2 + (4*MH2*MT2 - 4*MT2*(((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
            -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*(-((Sqrt[S]*Sin[\[Theta]3]^2)/
            (Sqrt[2]*Abs[Sin[\[Theta]3]])) + (Cos[\[Theta]3/2]*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
            Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/
        (2*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
           2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)) + 
       ((-4*MH2*(-1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^2*
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]] + 2*MH*Sqrt[MH2 - 4*MT2]*
           (-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            ((1 + Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
          2*Sqrt[2]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(MH*MH2 + (MH*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^2*Sqrt[(-1 + Cos[\[Theta]3])*
             (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + (MH2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
             2*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH*MH2 + (MH*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 + Sqrt[2]*MH2*(-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (-MH2 + (-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] - 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + (MH2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
             2*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/4 + (MH2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            Kallen\[Lambda][S, MH2, 0]*(MH2^2 - 2*MH2*((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]] - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
             ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
             (-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0] + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
              4)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH*MH2 + (MH*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*(2*MH2*MT2 - 
             2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*(-(Sqrt[Kallen\[Lambda][S, MH2, 0]]*
             Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(MH2 - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/Abs[
                Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*
                Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (2*Sqrt[2]*Sqrt[S]) + ((1 + Cos[\[Theta]3])*
            (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/
        (MH2*(-1 + Cos[\[Theta]3])^2*
         (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         Kallen\[Lambda][S, MH2, 0]) - 
       (4*(-2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2)*(MH2 + (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
           Kallen\[Lambda][S, MH2, 0] + MH*Sqrt[MH2 - 4*MT2]*
           (-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Kallen\[Lambda][S, MH2, 0]*(MH2^2 - MH2*(((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0])/4)*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
             (2*MT2)] - Sqrt[2]*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            2*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
             2*Kallen\[Lambda][S, MH2, 0]*(MH2^2 - 
             2*MH2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
             ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
             (-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0] + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
              2)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)^2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 + Sqrt[2]*(-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           (-MH2^3 + MH2^2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2) + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^
               2*Kallen\[Lambda][S, MH2, 0]^(3/2))/8)*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)^2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/4 + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            Kallen\[Lambda][S, MH2, 0]*(MH2^4 - 2*MH2^3*
              (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
             (MH2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*(
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Kallen\[Lambda][S, MH2, 0])/2 - ((-1 + Cos[\[Theta]3])^2*
               (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^2)/16 + 
             MH2^2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 + 
               ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4))*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)^2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*(2*MH2*MT2 - 
             2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*(-(Sqrt[Kallen\[Lambda][S, MH2, 0]]*
             Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
                (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] + 
              (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]])*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]) - 
          ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (8*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/((-1 + Cos[\[Theta]3])^2*
         (1 + Cos[\[Theta]3])^2*(-MH2 + ((-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         Kallen\[Lambda][S, MH2, 0]^2) - 
       ((4*MH2*(MH2 - S)*(-1 + Cos[\[Theta]3])*
           (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]] + 
          2*MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
           (MH2*(S - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2) - S*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2))*(MH2 - S - ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] - 
          2*MH2*Sqrt[S*(-4*MT2 + S)]*(-1 + Cos[\[Theta]3])*
           (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]])/2)*(-MH2 + S + (-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]])*Sqrt[Kallen\[Lambda][S, MH2, 
             0]]*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] + 
          (MH2*(MH2 - S)^2*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
              MH2, 0]]*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 + 
          (MH2*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (MH2^2 + S^2 - 2*MH2*(S + (-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 2*S*(-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]] + ((-1 + Cos[\[Theta]3])^
                2*Kallen\[Lambda][S, MH2, 0])/2)*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))/2 - 
          2*Sqrt[2]*(MH*MH2 - MH*S)^2*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + (MH2*(MH2 - S)^2*S*(-1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/2 - (S*(MH*MH2 - MH*S)^2*(-1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/2 - S*(MH*MH2 - MH*S)^2*(-1 + Cos[\[Theta]3])*
           (2*MH2*MT2 - 2*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                 Kallen\[Lambda][S, MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*(-(Cos[\[Theta]3/2]*(-1 + Cos[\[Theta]3])*
             Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*
             (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
          (Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - 
             I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/(MH2*(MH2 - S)^2*
         (-1 + Cos[\[Theta]3])^2*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
             Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*Kallen\[Lambda][S, MH2, 
          0]) + (ScalarD0[0, 0, 0, MH2, ((-1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
          -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, 
          MT, MT]*(-(Sqrt[S]*(-MH2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
           (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
          (Sqrt[S]*(MH2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
          (Sqrt[S]*Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
          ((1 + Cos[\[Theta]3])*(S - (1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]]) + (Kallen\[Lambda][S, MH2, 0]*
            Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]])))/4 + 
       ((((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 - 
          MH2*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/2 - ((-1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/2 + ((-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, 
             MH2, ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*(-(Sqrt[S]*(-MH2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
           (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[S]*(1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2)/
           (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) + (Cos[\[Theta]3/2]*
            (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
             (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*
            Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            Abs[Sin[\[Theta]3]]) + (3*(1 + Cos[\[Theta]3])^2*
            Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
             I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (8*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
          (Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - 
             I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (2*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]])))/(2*(-1 + Cos[\[Theta]3])*
         (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
         (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
         Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
       4*((Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]*
           ((Sqrt[Kallen\[Lambda][S, MH2, 0]]*((2*(-MH2 + S - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                  2)/(MH2 - S) + (2*MH*Sqrt[MH2 - 4*MT2]*(MH2^3 + 
                  S*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2)^2 - 2*MH2*S*(2*S - ((1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + MH2^2*
                   (2*S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2))*(MH2 - S + ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
                ((MH*MH2 - MH*S)^2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2)) - 
               (2*Sqrt[S*(-4*MT2 + S)]*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-3*MH2 + 3*S - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/
                (MH2 - S)^2 - (2*(-(MH2*MT2) + MT2*(S - ((1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                  (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
               (2*(MH2^3*MT2 - MH2^2*(MT2*(3*S - (5*(1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
                    (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2) + (S^3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2 - MT2*(S^3 - (5*S^2*(1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                    (3*S*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                     2 - ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                       (3/2))/4) + MH2*(-(S^2*(1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]]) + MT2*(3*S^2 - 
                      5*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]] + (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                         MH2, 0])/2)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                      2*MT2)/(2*MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + 
                         S)])/(2*MT2)]^2))/((MH2 - S)^2*(1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) - (2*Sqrt[2]*(-MH2 - S - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]*
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)])/((1 + Cos[\[Theta]3])*
                 (-MH2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
               (2*(-(MH2*MT2) + MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])/2) + 
                  (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                      Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)/(2*MT2)]^2)/((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) - (2*(MH2*MT2 - 
                  MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2)*
                 (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                  Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                       Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)/(2*MT2)]^2))/((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 2*S*(3*MH2*MT2 - 
                 3*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*ScalarD0[0, 0, 0, MH2, S, 
                 -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                 MT, MT, MT, MT])*Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(32*Abs[Sin[\[Theta]3]]*(MH2 - S + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
               3) - ((-(((-1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)^2*(-8*MH2^2 + MH2*(4*(-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]] - (11*(1 + Cos[
                         \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                   ((1 + Cos[\[Theta]3])*((-1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]] - (3*(1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
               (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(MH2 - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*(3*MH2^3 - 
                  3*MH2^2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]) + ((-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                   4 + MH2*((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                       MH2, 0])/2 - (7*(-1 + Cos[\[Theta]3])*(1 + 
                       Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4 + 
                    (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4))*
                 Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
                (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)^3 + Sqrt[2]*(-MH2 + ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                (-3*MH2 + (3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]])/2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
                Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                       ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)/(2*MT2)] - (2*(-(MH2^3*MT2) - ((-1 + Cos[\[Theta]3])^3*
                    (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^2)/16 + 
                  MH2^2*(MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2 - (5*(1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2) - 
                    ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4) + MT2*
                   (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                       (3/2))/8 - (5*(-1 + Cos[\[Theta]3])^2*(1 + 
                       Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
                    (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                      Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - 
                    ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                     4) - MH2*(-((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                       Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + MT2*
                     ((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                       4 - (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                        Kallen\[Lambda][S, MH2, 0])/2 + 
                      (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                       2)))*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)]^2)/((1 + Cos[\[Theta]3])*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]]) + (2*(MH2 - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)^2*(MH2*MT2 - MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
                  ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                     S, MH2, 0])/4)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                      2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[
                          \[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)/(2*MT2)]^2))/((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (Sqrt[2]*(-1 + 
                  Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])]*(3*MH2^4 - 6*MH2^3*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - (MH2*(-1 + Cos[\[Theta]3])*(1 + 
                     Cos[\[Theta]3])*(2*(-1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]] - (3*(1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Kallen\[Lambda][
                     S, MH2, 0])/4 + ((-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])^2*(-((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + MH2^2*
                   ((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                     4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/2 + (3*(1 + Cos[\[Theta]3])^
                       2*Kallen\[Lambda][S, MH2, 0])/4))*
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)])/((1 + Cos[\[Theta]3])*
                 (-MH2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^3) - (2*(MH2 - ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*(MH2*MT2 - 
                  MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                        S, MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4)*
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                      Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)/(2*MT2)]^2)/((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]) - (2*(MH2^6*MT2 + 
                  MH2^5*(-(MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2 - 2*(1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])) - 
                    ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4) - ((-1 + Cos[\[Theta]3])^
                     2*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                     (5/2)*(-(MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)) - 
                     ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                       Kallen\[Lambda][S, MH2, 0])/4))/32 + MH2^4*
                   (((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      (2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]] - (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0])/4 + 
                    MT2*((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 
                         0])/4 - (11*(-1 + Cos[\[Theta]3])*(1 + Cos[
                          \[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4 + 
                      (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                       2)) - MH2^3*(((-1 + Cos[\[Theta]3])*(1 + Cos[
                        \[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                      ((5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                         Kallen\[Lambda][S, MH2, 0])/2 + 
                       (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4))/4 + MT2*(((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                          S, MH2, 0]^(3/2))/8 - (7*(-1 + Cos[\[Theta]3])^2*
                        (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/4 + (15*(-1 + Cos[\[Theta]3])*
                        (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/8 - ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                          S, MH2, 0]^(3/2))/2)) + (MH2*(-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                    (((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                       ((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
                     MT2*(((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/2 - (9*(-1 + Cos[\[Theta]3])^2*(1 + 
                          Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                        8 + (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - 
                       ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/4)))/4 - (MH2^2*(1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]]*(-((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                        (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/4 - (3*(-1 + Cos[\[Theta]3])^2*(1 + 
                          Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                          8 - ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 
                          0]^(3/2))/8))/2 + MT2*((11*(-1 + Cos[\[Theta]3])^3*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                       (9*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + 
                       (9*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                       ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/8)))/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                      2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[
                        -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)/(2*MT2)]^2))/((1 + Cos[\[Theta]3])*
                 (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^3*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
               (-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*(3*MH2*MT2 - 3*MT2*
                  (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                 -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                 MT, MT, MT, MT])*Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*
                Sin[\[Phi]3]))/(4*Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
              (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2)^3*Sqrt[Kallen\[Lambda][S, MH2, 0]]))*(Cos[\[Phi]3] - 
            I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]) + 
         ((-(((-1 + Cos[\[Theta]3])*(-3*MH2^2 + MH2*(S + 
                  (3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2) + S*(2*S + (3*(-1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2))*(-MH2 + S + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                2*Sqrt[Kallen\[Lambda][S, MH2, 0]])/(MH2 - S)^2) + 
            (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(MH2 - S - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              Sqrt[Kallen\[Lambda][S, MH2, 0]]*(MH2^2*(3*S - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + MH2*(-6*S^2 - (9*S*(-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                 ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4) + S*
                (3*S^2 + 5*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]] + (5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                    MH2, 0])/4))*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                (2*MT2)])/(MH2 - S)^3 + (Sqrt[S*(-4*MT2 + S)]*
              (-1 + Cos[\[Theta]3])*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*(3*MH2^3 - 2*MH2^2*(3*S + 2*(-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (S*(-1 + 
                  Cos[\[Theta]3])*(3*S + ((-1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2 + MH2*(3*S^2 + (5*S*(-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                 (5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4))*
              Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/(MH2 - S)^3 + 
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)*(MH2*MT2 - MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 + 
            ((MH2^5*MT2 + MH2^4*(-2*MT2*(2*S + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                 (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + MH2^3*(-(S*(-1 + Cos[\[Theta]3])*(3*S + 
                     (5*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                 MT2*(6*S^2 + (7*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                       S, MH2, 0]])/2 - ((-1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4)) + MH2^2*
                ((3*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                   (S^2 + (3*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2 + ((-1 + Cos[\[Theta]3])^2*
                      Kallen\[Lambda][S, MH2, 0])/2))/2 - MT2*(4*S^3 + 
                   (9*S^2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 + (3*S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                      S, MH2, 0])/4 - ((-1 + Cos[\[Theta]3])^3*
                     Kallen\[Lambda][S, MH2, 0]^(3/2))/2)) - 
               (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                 ((S^3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2 + MT2*(S^3 + (5*S^2*(-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                    (3*S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                     2 + ((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                       (3/2))/4)))/2 + MH2*(-(S*(-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]]*(S^3 + (3*S^2*(-1 + 
                        Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                     (3*S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                      2 + ((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                        (3/2))/4))/2 + MT2*(S^4 + (5*S^3*(-1 + Cos[
                       \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   (9*S^2*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                    4 + (S*(-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                      (3/2))/4 - ((-1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, 
                       MH2, 0]^2)/8)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                   2*MT2)/(2*MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
                  (2*MT2)]^2))/(MH2 - S)^3 + Sqrt[2]*(-MH2 - S + 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)] + (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2)*(MH2*MT2 - MT2*(S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
              (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2 - (MH2 - ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2*MT2 - 
              MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                 (2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                     (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                   Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)]^2) - S*(-1 + Cos[\[Theta]3])*
             (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)*(3*MH2*MT2 - 3*MT2*(S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
              (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
             Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
              MT, MT, MT])*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
           (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(16*Sqrt[2]*Sqrt[S]*
           Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
           (MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)^3)) - 
       4*(-(Kallen\[Lambda][S, MH2, 0]*((2*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2)/(MH2 - S) + 
             (2*MH*Sqrt[MH2 - 4*MT2]*(MH2^3 + MH2^2*(2*S - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2) + S*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)^2 - 2*MH2*S*(2*S + ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2))*(MH2 - S - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
              ((MH*MH2 - MH*S)^2*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)) - 
             (2*Sqrt[S*(-4*MT2 + S)]*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-3*MH2 + 3*S + 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/(MH2 - S)^2 + 
             (2*(-(MH2*MT2) + MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2) - 
                (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
             (2*(MH2^3*MT2 - MH2^2*(MT2*(3*S + (5*(-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                  (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2) - (S^3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/2 - MT2*(S^3 + (5*S^2*(-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                  (3*S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                   2 + ((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                     (3/2))/4) + MH2*(S^2*(-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]] + MT2*(3*S^2 + 
                    5*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]] + (3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                       MH2, 0])/2)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                    2*MT2)/(2*MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + 
                       S)])/(2*MT2)]^2))/((MH2 - S)^2*(-1 + Cos[\[Theta]3])*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (2*Sqrt[2]*(-MH2 - S + 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(
                -MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                    MH2, 0]])/2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                  Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/((-1 + Cos[\[Theta]3])*(-MH2 + 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (2*(-(MH2*MT2) + 
                MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                      (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                    Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2)/((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]) + (2*(MH2*MT2 - 
                MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                    2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[
                         \[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2))/((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]) + 2*S*(3*MH2*MT2 - 3*MT2*
                (S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                  2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])*
            Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(64*Sqrt[2]*Sqrt[S]*
           Abs[Sin[\[Theta]3]]*(MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^3) - 
         (Kallen\[Lambda][S, MH2, 0]*((-(((1 + Cos[\[Theta]3])*(2*MH2^2 + 
                  S*(7*S - 4*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]]) - MH2*(9*S - (1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]]))*(-MH2 + S - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                  2*Sqrt[Kallen\[Lambda][S, MH2, 0]])/(MH2 - S)^2) + 
              (Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*(MH2 - S + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*(3*S^2*
                  (S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2)^2 - (MH2^3*(1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + MH2^2*(3*S^2 + 
                   (7*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 - ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                      MH2, 0])/4) + MH2*(-6*S^3 + S*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]))*Log[(-MH2 + 
                   MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/(MH*(MH2 - S)^3) + 
              (S*Sqrt[S*(-4*MT2 + S)]*(1 + Cos[\[Theta]3])*(-MH2 + S - 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*(3*MH2^2 + 3*S^2 - 
                 MH2*(6*S - (11*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) - (11*S*(1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                 (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2)*
                Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/(MH2 - S)^
                3 + S*(-(MH2*MT2) + MT2*(S - ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
              (S*(MH2^4*MT2 + MH2^3*(-(MT2*(4*S - ((1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)) - 
                   (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2) + (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]]*(S^3 - 3*S^2*(1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]] + (3*S*(1 + Cos[\[Theta]3])^
                       2*Kallen\[Lambda][S, MH2, 0])/2 - 
                    ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                     4))/2 + MT2*(S^4 - (S^3*(1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                   (3*S^2*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                    2 + (5*S*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 
                       0]^(3/2))/4 - ((1 + Cos[\[Theta]3])^4*Kallen\[Lambda][
                       S, MH2, 0]^2)/4) + 3*MH2^2*((S*(1 + Cos[\[Theta]3])*
                     (S - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   MT2*(2*S^2 - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])^2*
                       Kallen\[Lambda][S, MH2, 0])/2)) + 
                 MH2*((-3*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]]*(S^2 - 2*S*(1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]] + ((1 + Cos[\[Theta]3])^2*
                        Kallen\[Lambda][S, MH2, 0])/2))/2 + MT2*(-4*S^3 + 
                     (3*S^2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2 + 3*S*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                       S, MH2, 0] - (5*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                         S, MH2, 0]^(3/2))/4)))*
                (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))/
               (MH2 - S)^3 + Sqrt[2]*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-MH2 + 3*S - 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)] + S*(-(MH2*MT2) + MT2*(S - ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2 + S*(MH2*MT2 - 
                MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                    2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[
                      -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2) - S^2*(1 + Cos[\[Theta]3])*(3*MH2*MT2 - 
                3*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[
                0, 0, 0, MH2, S, -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]])/2, MT, MT, MT, MT])/((1 + Cos[\[Theta]3])^2*
              (MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2)^3*Kallen\[Lambda][S, MH2, 0]) + 
            (4*(-((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                  (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                        S, MH2, 0]])/2)^2*Kallen\[Lambda][S, MH2, 0]*
                  (-6*MH2^3 + MH2^2*(3*(-1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]] - (31*(1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
                   (MH2*(1 + Cos[\[Theta]3])*((25*(-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                      (39*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   ((1 + Cos[\[Theta]3])^2*((13*(-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                      7*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])*Kallen\[Lambda][S, MH2, 0])/4))/
                (2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^2) - (MH*Sqrt[MH2 - 4*MT2]*(-1 + 
                  Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*(MH2 - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*Kallen\[Lambda][S, MH2, 0]*(3*MH2^4 - 
                  6*MH2^3*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])/2) - (5*MH2*(-1 + 
                     Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2)*Kallen\[Lambda][S, MH2, 0])/4 - 
                  ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]^2)/8 + MH2^2*
                   ((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                     4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4 + (3*(1 + Cos[\[Theta]3])^
                       2*Kallen\[Lambda][S, MH2, 0])/4))*
                 Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
                (2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^3) - ((1 + Cos[\[Theta]3])*(-MH2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]]*(3*MH2^2 - MH2*
                   (3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]] - (11*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2) + (3*(-1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0])/4 - (11*(-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4 + 
                  (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2)*
                 Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                        ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)])/Sqrt[2] - (MH2^4*MT2 + MH2^3*
                  (-(MT2*(2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]] - ((1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2)) - 
                   ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4) + ((-1 + Cos[\[Theta]3])*
                   (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                   (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                       (3/2))/8 - (3*(-1 + Cos[\[Theta]3])^2*(1 + 
                       Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + 
                    (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                      Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - 
                    ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                     4))/4 + MT2*(((-1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, 
                       MH2, 0]^2)/16 - ((-1 + Cos[\[Theta]3])^3*(1 + 
                      Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^2)/16 - 
                   (3*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0]^2)/8 + 
                   (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^3*
                     Kallen\[Lambda][S, MH2, 0]^2)/8 - ((1 + Cos[\[Theta]3])^
                      4*Kallen\[Lambda][S, MH2, 0]^2)/4) + 3*MH2^2*
                  (((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])*Kallen\[Lambda][S, MH2, 0])/4 + 
                   MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                      2 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                       Kallen\[Lambda][S, MH2, 0])/4 - ((1 + Cos[\[Theta]3])^
                        2*Kallen\[Lambda][S, MH2, 0])/2)) + 
                 MH2*((-3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0]*(((-1 + Cos[\[Theta]3])^2*
                        Kallen\[Lambda][S, MH2, 0])/4 - (-1 + Cos[\[Theta]3])*
                       (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0] + 
                      ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2))/
                    4 + MT2*(-((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, 
                          MH2, 0]^(3/2))/2 + (3*(-1 + Cos[\[Theta]3])^2*
                       (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                      8 + (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                       Kallen\[Lambda][S, MH2, 0]^(3/2))/2 - 
                     (5*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                        (3/2))/4)))*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                       (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                     Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 - (MH2 - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                 3*(MH2*MT2 - MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
                 ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                    S, MH2, 0])/4)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                    (2*MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                        (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                      Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])/2)/(2*MT2)]^2) - ((-1 + Cos[\[Theta]3])*
                 (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]]*(-3*MH2^5 + 6*MH2^4*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - 3*MH2^3*(((-1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)^2 + (3*MH2^2*(-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                   4 - (MH2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                    (3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]] - (7*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                         MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                  ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^3*
                    ((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                         0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0]^2)/16)*
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)])/(Sqrt[2]*
                 (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^3) + (MH2 - ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3*(MH2*MT2 - 
                 MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + 
                    Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4)*
                Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 + ((MH2^7*MT2 + 
                  MH2^6*(-4*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2) - 
                    ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4) - ((-1 + Cos[\[Theta]3])^
                     3*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^3*
                    (MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2) + ((-1 + Cos[\[Theta]3])*
                       (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4))/
                   64 + 3*MH2^5*(((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0])/4 + 
                    MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                       2 - (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                        Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^
                         2*Kallen\[Lambda][S, MH2, 0])/2)) + 
                  (MH2*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]^2*((-3*(-1 + Cos[\[Theta]3])*
                       (1 + Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                       Kallen\[Lambda][S, MH2, 0])/4 - MT2*
                      ((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4 - (7*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                         Kallen\[Lambda][S, MH2, 0])/4 + 
                       (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4)))/16 + MH2^4*((-3*(-1 + Cos[\[Theta]3])*
                      (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                      (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                         Kallen\[Lambda][S, MH2, 0])/4 + 
                       ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                        4))/4 - MT2*(((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                          S, MH2, 0]^(3/2))/2 - (21*(-1 + Cos[\[Theta]3])^2*
                        (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/8 + (21*(-1 + Cos[\[Theta]3])*
                        (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/8 - ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                          S, MH2, 0]^(3/2))/2)) + (3*MH2^2*(-1 + 
                     Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                     MH2, 0]*(-((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                        Kallen\[Lambda][S, MH2, 0]*(((-1 + Cos[\[Theta]3])^2*
                          Kallen\[Lambda][S, MH2, 0])/4 - (3*(-1 + Cos[
                          \[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                          MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                          Kallen\[Lambda][S, MH2, 0])/4))/4 + MT2*
                      (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/8 - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[
                          \[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
                       ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                       ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/8)))/4 + MH2^3*(((-1 + Cos[\[Theta]3])*
                      (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                      (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/8 + (3*(-1 + Cos[\[Theta]3])^2*(1 + 
                          Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                        8 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                         Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                       ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                          (3/2))/8))/4 + MT2*(((-1 + Cos[\[Theta]3])^4*
                        Kallen\[Lambda][S, MH2, 0]^2)/16 - 
                      (13*(-1 + Cos[\[Theta]3])^3*(1 + Cos[\[Theta]3])*
                        Kallen\[Lambda][S, MH2, 0]^2)/16 + 
                      (21*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                        Kallen\[Lambda][S, MH2, 0]^2)/16 - (13*(-1 + 
                         Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^3*
                        Kallen\[Lambda][S, MH2, 0]^2)/16 + 
                      ((1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, MH2, 0]^2)/
                       16)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                     (2*MT2)]^2 - Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                          (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)/(2*MT2)]^2))/(MH2 + ((1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3 + 
               ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                 (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)^3*Kallen\[Lambda][S, MH2, 0]*(3*MH2*MT2 - 
                  3*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                        S, MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*
                    (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4)*
                 ScalarD0[0, 0, 0, MH2, ((-1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2, -((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])/
                2))/((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
              (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2)^3*Kallen\[Lambda][S, MH2, 0]^2))*Sin[\[Theta]3]^4*
           (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
          (16*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
         (Kallen\[Lambda][S, MH2, 0]*((((1 + Cos[\[Theta]3])*(-3*MH2^2 + 
                 MH2*(S - (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) + S*(2*S - (3*(1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2))*
                (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               (MH2 - S)^2 - (MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
                (MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                     0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                (MH2^2*(3*S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) + MH2*(-6*S^2 + (9*S*(1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4) + 
                 S*(3*S^2 - 5*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]] + (5*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                      MH2, 0])/4))*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                  (2*MT2)])/(MH2 - S)^3 - (Sqrt[S*(-4*MT2 + S)]*
                (1 + Cos[\[Theta]3])*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]]*(3*MH2^3 - 2*MH2^2*(3*S - 2*(1 + Cos[
                      \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
                 (S*(1 + Cos[\[Theta]3])*(3*S - ((1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2 + MH2*(3*S^2 - 
                   (5*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2 + (5*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                      MH2, 0])/4))*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
                  (2*MT2)])/(MH2 - S)^3 + (MH2 + ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2*MT2 - 
                MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
                  (2*MT2)]^2 + ((MH2^5*MT2 + MH2^4*(-2*MT2*(2*S - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2) + MH2^3*((S*(1 + Cos[\[Theta]3])*
                     (3*S - (5*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   MT2*(6*S^2 - (7*S*(1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                      4)) + (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]]*(-(S^3*(1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 + MT2*(S^3 - 
                      (5*S^2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2 + (3*S*(1 + Cos[\[Theta]3])^2*
                        Kallen\[Lambda][S, MH2, 0])/2 - ((1 + Cos[\[Theta]3])^
                         3*Kallen\[Lambda][S, MH2, 0]^(3/2))/4)))/2 + 
                 MH2^2*((-3*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]]*(S^2 - (3*S*(1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2 + 
                      ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2))/
                    2 - MT2*(4*S^3 - (9*S^2*(1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 + 
                     (3*S*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                      4 + ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                        (3/2))/2)) + MH2*((S*(1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]]*(S^3 - (3*S^2*
                        (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 + (3*S*(1 + Cos[\[Theta]3])^2*
                        Kallen\[Lambda][S, MH2, 0])/2 - ((1 + Cos[\[Theta]3])^
                         3*Kallen\[Lambda][S, MH2, 0]^(3/2))/4))/2 + 
                   MT2*(S^4 - (5*S^3*(1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 + (9*S^2*
                       (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                     (S*(1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                        (3/2))/4 - ((1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, 
                         MH2, 0]^2)/8)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                     2*MT2)/(2*MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + 
                        S)])/(2*MT2)]^2))/(MH2 - S)^3 + Sqrt[2]*(-MH2 - S - 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(
                -MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Log[
                (2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)] + (MH2 + ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2*MT2 - 
                MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                       (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                    Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)/(2*MT2)]^2 - (MH2 + ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2*MT2 - 
                MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2)*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                    2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[
                      -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2) + S*(1 + Cos[\[Theta]3])*(MH2 + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(
                3*MH2*MT2 - 3*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2) - 
                (S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
                -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                MT, MT, MT, MT])/((1 + Cos[\[Theta]3])^2*(MH2 - S + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3*
              Kallen\[Lambda][S, MH2, 0]) + 
            (4*(((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*(-2*MH2 + 
                  (-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] - 
                  (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2)*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2)^2*Kallen\[Lambda][S, 
                  MH2, 0])/2 + (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Kallen\[Lambda][S, MH2, 0]*(MH2^2 - MH2*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0])/2)*Log[(-MH2 + 
                    MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
                (2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)) - ((1 + Cos[\[Theta]3])*(MH2 - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*(MH2^2 - MH2*(((-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                    (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
                  ((1 + Cos[\[Theta]3])*((3*(-1 + Cos[\[Theta]3])*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                        ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)])/Sqrt[2] + (MH2^4*MT2 - MH2^3*MT2*
                  ((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]]) + MH2^2*MT2*((3*(-1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4 - (3*(-1 + Cos[\[Theta]3])*
                     (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4 - 
                   ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4) + 
                 MH2*(((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0]^2)/16 + MT2*
                    (-((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/8 + (3*(-1 + Cos[\[Theta]3])*
                       (1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^
                        (3/2))/4 - ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, 
                         MH2, 0]^(3/2))/2)) + ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]]*(-((-1 + Cos[\[Theta]3])^
                        3*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^2)/
                     16 + MT2*(((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, 
                          MH2, 0]^(3/2))/8 - (5*(-1 + Cos[\[Theta]3])^2*
                        (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/8 + (3*(-1 + Cos[\[Theta]3])*(1 + Cos[
                          \[Theta]3])^2*Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - 
                      ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                         (3/2))/4)))/2)*Log[(2*MT2 + Sqrt[(-1 + Cos[
                         \[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                          Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                        Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2 + (MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(MH2^3*MT2 - 
                 2*MH2^2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2) - 
                 ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                    S, MH2, 0]*(MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                         Kallen\[Lambda][S, MH2, 0]])/2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2) + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4))/4 + MH2*MT2*
                  (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                   ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4))*
                (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                 Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)]^2) + ((-1 + Cos[\[Theta]3])*(-MH2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2)*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]]*(MH2^3 - MH2^2*
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2) + (MH2*(-1 + Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                     (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                  ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    (-((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                         S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 0])/4)*
                 Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)])/(Sqrt[2]*(-MH2 - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2)) - (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                     S, MH2, 0]])/2)*(MH2^3*MT2 - 2*MH2^2*MT2*
                  (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0]*(MT2*(((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2) + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4))/4 + MH2*MT2*
                  (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                   ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                     Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                     Kallen\[Lambda][S, MH2, 0])/4))*
                Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                         ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                     Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)/(2*MT2)]^2 - ((MH2^5*MT2 - 3*MH2^4*MT2*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) + 3*MH2^3*MT2*(((-1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)^2 - (MH2*(-1 + Cos[\[Theta]3])^2*
                    (1 + Cos[\[Theta]3])^2*(-MT2 + ((-1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Kallen\[Lambda][S, MH2, 0]^2)/16 + 
                  ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]^2*(-(MT2*(((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)) - ((-1 + Cos[\[Theta]3])*(1 + Cos[
                         \[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4))/16 - 
                  MH2^2*(MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                          Kallen\[Lambda][S, MH2, 0]])/2)^3 - 
                    ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                      Kallen\[Lambda][S, MH2, 0]^2)/16))*
                 (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
                  Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                          ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                       Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                          S, MH2, 0]])/2)/(2*MT2)]^2))/(MH2 + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
               ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*(MH2 - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Kallen\[Lambda][S, MH2, 0]*(MH2^3*MT2 - 2*MH2^2*MT2*
                   (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                        0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0]*(3*MT2*(((-1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2) + ((-1 + Cos[\[Theta]3])*(1 + Cos[
                         \[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4))/4 + 
                  MH2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 
                       0])/4 + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                      Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                      Kallen\[Lambda][S, MH2, 0])/4))*ScalarD0[0, 0, 0, MH2, 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                  -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
                  MT, MT, MT, MT])/2))/((-1 + Cos[\[Theta]3])^2*
              (1 + Cos[\[Theta]3])^2*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3*Kallen\[Lambda][S, 
                MH2, 0]^2))*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
           (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(16*Sqrt[2]*Sqrt[S]*
           Abs[Sin[\[Theta]3]])) + 
       ((-4*MH2*(MH2 - S)*(1 + Cos[\[Theta]3])*
           (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]] - 
          2*MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
           (-(S*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2)) + MH2*(S + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2))*(MH2 - S + ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + 
          2*MH2*Sqrt[S*(-4*MT2 + S)]*(1 + Cos[\[Theta]3])*
           (-MH2 + S - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])*
           (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
          (MH2*(MH2 - S)^2*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
              MH2, 0]]*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/2 - 
          (MH2*S*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (MH2^2 + S^2 - 2*MH2*(S - (1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 2*S*(1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]] + ((1 + Cos[\[Theta]3])^2*
               Kallen\[Lambda][S, MH2, 0])/2)*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))/2 - 
          2*Sqrt[2]*(MH*MH2 - MH*S)^2*(-MH2 + S - ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                  (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[
                2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)/(2*MT2)] - (MH2*(MH2 - S)^2*S*(1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/2 + (S*(MH*MH2 - MH*S)^2*(1 + Cos[\[Theta]3])*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/2 + S*(MH*MH2 - MH*S)^2*(1 + Cos[\[Theta]3])*
           (2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*ScalarD0[0, 0, 0, MH2, S, 
            -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*Sin[\[Theta]3]*
         (-((1 + Cos[\[Theta]3])^2*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
             Sin[\[Theta]3]*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (4*Abs[Sin[\[Theta]3]]) - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
           (2*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
        (2*Sqrt[2]*MH2*(MH2 - S)^2*Sqrt[S]*(1 + Cos[\[Theta]3])^2*
         (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
            2)^2*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       (Sqrt[2]*(2*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            2*Kallen\[Lambda][S, MH2, 0] - MH*Sqrt[MH2 - 4*MT2]*
           (-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Kallen\[Lambda][S, MH2, 0]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
              2*MT2)/(2*MT2)] - Sqrt[2]*(1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
            Kallen\[Lambda][S, MH2, 0]^2*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/16 + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^
             2*Kallen\[Lambda][S, MH2, 0]^2*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/16 + Sqrt[2]*(-1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
              ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
            Kallen\[Lambda][S, MH2, 0]^2*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/16 + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
            Kallen\[Lambda][S, MH2, 0]^2*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
                 2*MT2)/(2*MT2)]^2 - Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                     (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                          MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                  Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2)/(2*MT2)]^2))/16 + 
          ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 
             0]*(2*MH2^3*MT2 - 4*MH2^2*MT2*(((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
             ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                MH2, 0]*(2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                      S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])/2) + ((-1 + Cos[\[Theta]3])*
                  (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4))/4 + 
             2*MH2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                 Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                 Kallen\[Lambda][S, MH2, 0])/4))*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*Sin[\[Theta]3]*
         ((Cos[\[Theta]3/2]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
           Abs[Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
           (2*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
        (Sqrt[S]*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
         (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         Kallen\[Lambda][S, MH2, 0]^(3/2)) + 
       ((-2*MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] - 
          Sqrt[2]*(1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Kallen\[Lambda][S, MH2, 0]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 + 2*Sqrt[2]*(MH2 - ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])]*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                  (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[
                2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
               2)/(2*MT2)] + ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Kallen\[Lambda][S, MH2, 0]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/4 - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Kallen\[Lambda][S, MH2, 0]*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/4 + (1 + Cos[\[Theta]3])*
           (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*(2*MH2^2*MT2 - 
            2*MH2*MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]) + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
            2*MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
              (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                 S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/4))*ScalarD0[0, 0, 0, MH2, 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
            -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*Sin[\[Theta]3]*
         (-((Cos[\[Theta]3/2]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
            Abs[Sin[\[Theta]3]]) + (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
           (2*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
        (2*Sqrt[2]*Sqrt[S]*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
         (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
         (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
         Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       (MH*Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
         (2*Sqrt[MH2 - 4*MT2]*(MH2 + S)*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
              2*MT2)/(2*MT2)] + MH*S*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*
                MT2)/(2*MT2)]^2 + MH*(4*MH2 - 4*S - 4*Sqrt[S*(-4*MT2 + S)]*
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
            S*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))*
         Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]*
         (MH*MH2 - MH*S)^2*Abs[Sin[\[Theta]3]]) + 
       (Sqrt[S]*Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
         ((Sqrt[MH2 - 4*MT2]*(2*MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           (MH*(MH2 - S)*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]])/2)*(MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) - 
          (Sqrt[S*(-4*MT2 + S)]*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
              (2*MT2)])/(S*(-MH2 + S)*(-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) - 
          Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2/
           (4*(-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2) + (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/
           (4*(-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2) - (Sqrt[2]*Sqrt[(-1 + Cos[\[Theta]3])*
              (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/((-1 + Cos[\[Theta]3])*(-MH2 + 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
          Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2/(4*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
          (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/(4*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
          ((2*MH2*MT2 - 2*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]])/2) + (S*(-1 + Cos[\[Theta]3])*
               Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*ScalarD0[0, 0, 0, MH2, S, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/(2*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2))*Sin[\[Theta]3/2]^3*
         Sin[\[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
       (MH*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
         (2*Sqrt[MH2 - 4*MT2]*(MH2 + S)*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 
              2*MT2)/(2*MT2)] + MH*S*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*
                MT2)/(2*MT2)]^2 + MH*(4*MH2 - 4*S - 4*Sqrt[S*(-4*MT2 + S)]*
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - 
            S*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))*
         Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(8*Sqrt[2]*Sqrt[S]*
         (MH*MH2 - MH*S)^2*Abs[Sin[\[Theta]3]]) + 
       (Sqrt[S]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
         ((Sqrt[MH2 - 4*MT2]*(2*MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           (MH*(MH2 - S)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                 MH2, 0]])/2)*(MH2 - S + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) - 
          (Sqrt[S*(-4*MT2 + S)]*Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/
              (2*MT2)])/(S*(-MH2 + S)*(-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) - 
          Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2/
           (4*(-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2) + (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/
           (4*(-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2)^2) + (Sqrt[2]*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Sqrt[Kallen\[Lambda][S, MH2, 0]])]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/((1 + Cos[\[Theta]3])*(-MH2 - 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (-MH2 + S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
          Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2/(4*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
          (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/(4*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2) + 
          ((2*MH2*MT2 - 2*MT2*(S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                   S, MH2, 0]])/2) - (S*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)*ScalarD0[0, 0, 0, MH2, S, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/(2*(-MH2 + S - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2))*Sin[\[Theta]3]^2*
         (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
        (4*Sqrt[2]*Abs[Sin[\[Theta]3]]) + 
       ((((-1 + Cos[\[Theta]3])*(2*MH2^2 - MH2*(9*S + (-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) + 
             S*(7*S + 4*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]]))*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*
            Sqrt[Kallen\[Lambda][S, MH2, 0]])/(MH2 - S)^2 - 
          (Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*(MH2 - S - 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (3*S^2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                    0]])/2)^2 + (MH2^3*(-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2 + MH2^2*(3*S^2 - 
               (7*S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4) + 
             MH2*(-6*S^3 + S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 
                 0]))*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           (MH*(MH2 - S)^3) - (S*Sqrt[S*(-4*MT2 + S)]*(-1 + Cos[\[Theta]3])*
            (-MH2 + S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*(3*MH2^2 + 3*S^2 - 
             MH2*(6*S + (11*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                   MH2, 0]])/2) + (11*S*(-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2 + (3*(-1 + Cos[\[Theta]3])^
                2*Kallen\[Lambda][S, MH2, 0])/2)*
            Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/(MH2 - S)^3 + 
          S*(-(MH2*MT2) + MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[
                 Kallen\[Lambda][S, MH2, 0]])/2) - (S*(-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2 - 
          (S*(MH2^4*MT2 + MH2^3*(-(MT2*(4*S + ((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)) + 
               (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]*(S^3 + 3*S^2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                   S, MH2, 0]] + (3*S*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                   S, MH2, 0])/2 + ((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][
                    S, MH2, 0]^(3/2))/4))/2 + MT2*(S^4 + (S^3*(-1 + 
                  Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
               (3*S^2*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                2 - (5*S*(-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                  (3/2))/4 - ((-1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, MH2, 
                   0]^2)/4) + 3*MH2^2*(-(S*(-1 + Cos[\[Theta]3])*
                  (S + (-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + MT2*
                (2*S^2 + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                     MH2, 0]])/2 - ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                    S, MH2, 0])/2)) + MH2*((3*S*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]*(S^2 + 2*S*(-1 + 
                    Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]] + 
                  ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2))/
                2 + MT2*(-4*S^3 - (3*S^2*(-1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2 + 3*S*
                  (-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0] + 
                 (5*(-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                  4)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^
              2 - Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2))/
           (MH2 - S)^3 + Sqrt[2]*(-MH2 + S + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*(-MH2 + 3*S + 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + S*(-(MH2*MT2) + MT2*(S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2 + S*(MH2*MT2 - MT2*(S + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
            (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2) + S^2*(-1 + Cos[\[Theta]3])*(3*MH2*MT2 - 
            3*MT2*(S + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2) + (S*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           ScalarD0[0, 0, 0, MH2, S, ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, MT, MT, MT])*
         Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
         Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
         (MH2 - S - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
            2)^3) + 
       ((-(((-1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*(-3*MH2^2 + 
              MH2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2) - ((1 + Cos[\[Theta]3])*((3*(-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
             2) - (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (MH2^2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 + (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*(
                (5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                   S, MH2, 0])/2 + (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                   S, MH2, 0])/4))/2 + MH2*(-((-1 + Cos[\[Theta]3])^2*
                  Kallen\[Lambda][S, MH2, 0])/4 - (9*(-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0])/4 + 
               (3*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2))*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
            3 + Sqrt[2]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                S, MH2, 0]])/2)*(-MH2 + ((-1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] + (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]])/2)*(MH2*MT2 - MT2*(((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
            ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
               MH2, 0])/4)*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                  (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 - 
          (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2*MT2 - MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0])/4)*
           (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2) + ((-1 + Cos[\[Theta]3])*
            (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (3*MH2^3 - 2*MH2^2*(2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]] - (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                   S, MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + 
                Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                    S, MH2, 0]])/2 - (3*(1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2)*Kallen\[Lambda][S, MH2, 
                0])/4 + MH2*((5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                  MH2, 0])/4 - (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                 Kallen\[Lambda][S, MH2, 0])/4 + (3*(1 + Cos[\[Theta]3])^2*
                 Kallen\[Lambda][S, MH2, 0])/4))*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/(Sqrt[2]*(MH2 + ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3) + 
          (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           (MH2*MT2 - MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                  MH2, 0]])/2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0])/4)*
           Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)]^2 + ((MH2^5*MT2 + MH2^4*(-2*MT2*
                (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                 (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
               ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                  MH2, 0])/4) - MH2^3*(-((-1 + Cos[\[Theta]3])*
                  (1 + Cos[\[Theta]3])*((5*(-1 + Cos[\[Theta]3])*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]])/2 - 
                   (3*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Kallen\[Lambda][S, MH2, 0])/4 + MT2*
                (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 + 
                 (7*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4 - (3*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/2)) + ((-1 + Cos[\[Theta]3])*(
                1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*(
                -((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^3*
                   Kallen\[Lambda][S, MH2, 0]^2)/16 + MT2*
                 (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                   4 - (3*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + 
                  (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                  ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                   8)))/4 + MH2^2*((-3*(-1 + Cos[\[Theta]3])*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]*
                 (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
                  (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0])/4))/4 + MT2*
                (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                  2 + (3*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                 (9*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
                 ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                  2)) + MH2*(((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                 Kallen\[Lambda][S, MH2, 0]*(((-1 + Cos[\[Theta]3])^3*
                    Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - 
                  (3*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                    Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + 
                  (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                    Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                  ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                   8))/4 + MT2*(-((-1 + Cos[\[Theta]3])^4*Kallen\[Lambda][S, 
                      MH2, 0]^2)/8 - ((-1 + Cos[\[Theta]3])^3*(1 + 
                    Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^2)/8 + 
                 (9*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0]^2)/16 - 
                 (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^3*
                   Kallen\[Lambda][S, MH2, 0]^2)/16 + ((1 + Cos[\[Theta]3])^4*
                   Kallen\[Lambda][S, MH2, 0]^2)/16)))*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^3 + 
          ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Kallen\[Lambda][S, MH2, 0]*(3*MH2*MT2 - 
             3*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
               Kallen\[Lambda][S, MH2, 0])/4)*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
         Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
         (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3) - 
       (((-1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^2*Sqrt[Kallen\[Lambda][S, 
             MH2, 0]] + (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            (MH2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2) - ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
             ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4)*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           ((MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]])/2)) + ((-1 + Cos[\[Theta]3])*(-MH2 + 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
               ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/(Sqrt[2]*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) + 
          (MH2^2*MT2 - MH2*MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]) + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
            MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
              (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                 S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/4))*Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*
                  (-4*MT2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                       MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/
                Sqrt[2] - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 - (MH2^2*MT2 - 
            MH2*MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]) + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
            MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
              (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                 S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/4))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*
                MT2)]^2 - Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2) + ((-1 + Cos[\[Theta]3])*
            (-MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]])]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
              (2*MT2)])/(Sqrt[2]*(-MH2 - ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) + 
          (MH2^2*MT2 - MH2*MT2*((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]) + ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
              Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
            MT2*(((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
              (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                 S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                 MH2, 0])/4))*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                   (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)]^2 + 
          ((MH2^3*MT2 - 3*MH2^2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[
                  Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
             ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                 S, MH2, 0]^2)/16 - MT2*(((-1 + Cos[\[Theta]3])^3*
                 Kallen\[Lambda][S, MH2, 0]^(3/2))/4 - ((-1 + Cos[\[Theta]3])^
                  2*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                2 + (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                 Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - ((1 + Cos[\[Theta]3])^
                  3*Kallen\[Lambda][S, MH2, 0]^(3/2))/8) + 
             MH2*(-((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                  Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + MT2*
                ((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0] - 
                 (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/2 + (3*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4)))*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/(MH2 + ((1 + Cos[\[Theta]3])*
              Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) + 
          ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 
             0]*(MH2^2*MT2 - MH2*MT2*((5*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) + ((-1 + Cos[\[Theta]3])^2*(
                1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
             MT2*((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0] - 
               (5*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                  S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                  MH2, 0])/4))*ScalarD0[0, 0, 0, MH2, 
             ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
             -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
             MT, MT, MT])/2)*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
         Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
         (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3) + 
       ((2*(-1 + Cos[\[Theta]3])*(-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)^2*Sqrt[Kallen\[Lambda][S, 
             MH2, 0]] + (2*MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
           (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2) + 2*Sqrt[2]*(-1 + Cos[\[Theta]3])*
           (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
             2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
           Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, 
               MH2, 0]]]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
           Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
              ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
             (2*MT2)] - (2*(-2*MH2^3*MT2 + 2*MH2^2*MT2*
              ((3*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                2 - 2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]]) - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*(
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 
             2*MT2*(((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^
                  (3/2))/8 - (5*(-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                 Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + (3*(-1 + 
                  Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, 
                   MH2, 0]^(3/2))/4 - ((1 + Cos[\[Theta]3])^3*
                 Kallen\[Lambda][S, MH2, 0]^(3/2))/4) - 
             MH2*(-((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                  Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 2*MT2*
                ((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/4 - 
                 (9*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4 + (5*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4)))*
            Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                    ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(
                2*MT2)]^2)/((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]) + (2*(2*MH2^3*MT2 - 2*MH2^2*MT2*((3*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) - ((-1 + Cos[\[Theta]3])^2*(
                2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2) - ((1 + Cos[\[Theta]3])*
                  (-((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Kallen\[Lambda][S, MH2, 0])/4 + MH2*(((-1 + Cos[\[Theta]3])^2*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 2*
                MT2*((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                  4 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4)))*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
              MH2, 0]]) + (2*Sqrt[2]*(-1 + Cos[\[Theta]3])*
            (-MH2 + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]])]*(MH2^2 - MH2*(((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
             ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][S, 
                MH2, 0])/2)*Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*
                   (-4*MT2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                        MH2, 0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                   0]])/2)/(2*MT2)])/((1 + Cos[\[Theta]3])*
            (-MH2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)) - (2*(2*MH2^3*MT2 - 2*MH2^2*MT2*((3*(-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - (1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]) - ((-1 + Cos[\[Theta]3])^2*(
                2*MT2*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2) - ((1 + Cos[\[Theta]3])*
                  (-((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
               Kallen\[Lambda][S, MH2, 0])/4 + MH2*(((-1 + Cos[\[Theta]3])^2*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 + 2*
                MT2*((3*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                  4 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0])/4)))*
            Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                     ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/(2*
                MT2)]^2)/((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
              0]]) - (2*(2*MH2^4*MT2 - 6*MH2^3*MT2*(((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2) - 
             ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*(-2*MT2*
                 (((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                   2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                      0]])/2) - ((1 + Cos[\[Theta]3])*(((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Kallen\[Lambda][S, 
                 MH2, 0]^(3/2))/8 + MH2^2*(6*MT2*(((-1 + Cos[\[Theta]3])*
                    Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^
                 2 - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                 Kallen\[Lambda][S, MH2, 0]^(3/2))/8) - 
             MH2*(-((-1 + Cos[\[Theta]3])^3*(1 + Cos[\[Theta]3])*
                  Kallen\[Lambda][S, MH2, 0]^2)/16 + 2*MT2*
                (((-1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                  8 - ((-1 + Cos[\[Theta]3])^2*(1 + Cos[\[Theta]3])*
                   Kallen\[Lambda][S, MH2, 0]^(3/2))/4 + 
                 (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])^2*
                   Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
                 ((1 + Cos[\[Theta]3])^3*Kallen\[Lambda][S, MH2, 0]^(3/2))/
                  8)))*(Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^
              2 - Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2))/((1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]) + (-1 + Cos[\[Theta]3])*
           Sqrt[Kallen\[Lambda][S, MH2, 0]]*(-2*MH2^3*MT2 + 
            4*MH2^2*MT2*((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]] - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                  0]])/2) + ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                MH2, 0]]*(-((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                  (-((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Kallen\[Lambda][S, MH2, 0])/4 + 2*MT2*
                (((-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/2 - 
                 ((-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*Kallen\[Lambda][
                    S, MH2, 0])/4 - ((1 + Cos[\[Theta]3])^2*Kallen\[Lambda][
                    S, MH2, 0])/4)))/2 + MH2*(-((-1 + Cos[\[Theta]3])^2*
                 (1 + Cos[\[Theta]3])*Kallen\[Lambda][S, MH2, 0]^(3/2))/8 - 
              2*MT2*((5*(-1 + Cos[\[Theta]3])^2*Kallen\[Lambda][S, MH2, 0])/
                 4 - (3*(-1 + Cos[\[Theta]3])*(1 + Cos[\[Theta]3])*
                  Kallen\[Lambda][S, MH2, 0])/4 + ((1 + Cos[\[Theta]3])^2*
                  Kallen\[Lambda][S, MH2, 0])/4)))*ScalarD0[0, 0, 0, MH2, 
            ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, 
            -((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2, MT, 
            MT, MT, MT])*Sin[\[Theta]3]^4*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
         (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
         Abs[Sin[\[Theta]3]]*(-1 + Cos[\[Theta]3])^2*
         (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
           ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^3)))/
     (MW*Pi*SW)}}}}
