(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*(c124 - c142)*EL*GS*MT2*
      (-((((((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
             ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (Sqrt[MH2 - 4*MT2]*S*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                (2*MT2)] - MH*Sqrt[S*(-4*MT2 + S)]*Log[(2*MT2 - S + 
                 Sqrt[S*(-4*MT2 + S)])/(2*MT2)])*Sin[\[Theta]3]^2)/
           (Sqrt[2]*MH*(MH2 - S)*Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
          (Sqrt[S]*(((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2 - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            (Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)*
            Sin[\[Theta]3]^2)/(4*Sqrt[2]*(MH2 - S)*Abs[Sin[\[Theta]3]]) + 
          (Sqrt[S]*(2 + Eps^(-1) + Log[Mu^2/MT2] + (Sqrt[S*(-4*MT2 + S)]*Log[
                (2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)])/S - 
             (3*MH2 + MH2/Eps - 3*S - S/Eps + MH*Sqrt[MH2 - 4*MT2]*
                Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)] + MT2*
                Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 + 
               (MH2 - S)*Log[Mu^2/MT2] - Sqrt[S*(-4*MT2 + S)]*
                Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)] - MT2*
                Log[(2*MT2 - S + Sqrt[S*(-4*MT2 + S)])/(2*MT2)]^2)/(MH2 - S))*
            Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[Sin[\[Theta]3]]))/S) + 
       (2*(-((2 + Eps^(-1) + Log[Mu^2/MT2] - (Sqrt[2]*Sqrt[
                -((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]) - (3*MH2 + MH2/Eps + (3*(1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/(2*Eps) + MH*
                Sqrt[MH2 - 4*MT2]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                  (2*MT2)] + MT2*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                   (2*MT2)]^2 + (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                    Kallen\[Lambda][S, MH2, 0]])/2)*Log[Mu^2/MT2] - 
               (Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                    Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)])/Sqrt[2] - MT2*Log[(2*MT2 + 
                    Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                    ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                     2)/(2*MT2)]^2)/(MH2 + ((1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2))*
            (-((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[Sin[\[Theta]3]])) + 
             (Sqrt[2]*Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - 
                I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (Sqrt[S]*Abs[Sin[\[Theta]3]]) + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
                I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (4*Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]))) + 
          ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                      ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                       2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                 ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2)*(-(Sqrt[S]*(-MH2 - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]^2)/
              (2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[Kallen\[Lambda][S, MH2, 
                 0]]*Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(-MH2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 (4*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (2*Sqrt[2]*Sqrt[S]) - (Sqrt[S]*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
                I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (4*Sqrt[2]*Abs[Sin[\[Theta]3]])))/
           (2*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)) - (2*(-(Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                   MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/2 - 
             (MH*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2])*((Sqrt[S]*Sin[\[Theta]3]^2*(MH2 + 
                ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 + 
                (Kallen\[Lambda][S, MH2, 0]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*S)))/
              (Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[2]*Cos[\[Theta]3/2]*(
                MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                 2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3/2]^3*Sin[
                \[Theta]3]*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + 
                I*Sin[\[Phi]3]))/(Sqrt[S]*Abs[Sin[\[Theta]3]]) + 
             ((1 + Cos[\[Theta]3])*(-3*S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]*Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
               (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[
                Sin[\[Theta]3]])))/(MH*(1 + Cos[\[Theta]3])*
            (MH2 + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
            Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
          (((Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                 MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/
              2 + (MH*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])]*Log[
                (2*MT2 + Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - 
                       ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])]/
                   Sqrt[2] + ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, 
                      MH2, 0]])/2)/(2*MT2)])/Sqrt[2])*Sin[\[Theta]3]*
            (((1 + Cos[\[Theta]3])*(-S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(Sqrt[2]*MH*Sqrt[S]*
            (1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]])/2)) - 
          ((-((1 + Cos[\[Theta]3])*(-MH2 - ((1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                  S, MH2, 0]])/2 + (MH*Sqrt[MH2 - 4*MT2]*(1 + Cos[\[Theta]3])*
               Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                  MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/2 + 
             (MT2*(1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
             (MH2*Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])]*Log[(2*MT2 + 
                  Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[\[Theta]3])*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                       Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                  ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2] - (MT2*(1 + Cos[\[Theta]3])*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 + 
                   Sqrt[-((1 + Cos[\[Theta]3])*(-4*MT2 - ((1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])]/Sqrt[2] + 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                  (2*MT2)]^2)/2)*Sin[\[Theta]3]*
            (((1 + Cos[\[Theta]3])*(-S + ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sin[\[Theta]3]*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*
            (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S]*
            (1 + Cos[\[Theta]3])*(MH2 + ((1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2)))/
        ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
       (2*((-2*((Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                 (2*MT2)])/2 - (MH*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                  Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                        Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                      Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                  ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                 (2*MT2)])/Sqrt[2])*(-2*(MH2 - ((-1 + Cos[\[Theta]3])*
                 Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
              ((Sqrt[S]*Sin[\[Theta]3]^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
               ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                 Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
                 (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*
                 Abs[Sin[\[Theta]3]])) + (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
               Sin[\[Theta]3]*((-2*Cos[\[Theta]3/2]*(-3*S - 
                   ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                  Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]] - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Sin[\[Theta]3]^3*(Cos[\[Phi]3] - I*Sin[\[Phi]3]))/
                 Abs[Sin[\[Theta]3]])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              (2*Sqrt[2]*Sqrt[S])))/(MH*(-1 + Cos[\[Theta]3])*
            (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]) - 
          ((Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2 - 
             Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)]^2)*(-((MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2)*((Sqrt[S]*Sin[\[Theta]3]^
                   2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]]) - ((1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^2*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3])*(Cos[\[Phi]3] + 
                   I*Sin[\[Phi]3]))/(4*Sqrt[2]*Sqrt[S]*Abs[Sin[
                    \[Theta]3]]))) + (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[
                \[Theta]3]*((2*S*Cos[\[Theta]3/2]*Sin[\[Theta]3/2]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(4*Abs[Sin[\[Theta]3]]))*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S])))/
           (2*(MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
              2)) + (2 + Eps^(-1) + Log[Mu^2/MT2] + 
            (Sqrt[2]*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                Sqrt[Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                 Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                       Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                     Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                 ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)/
                (2*MT2)])/((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                0]]) - (3*MH2 + MH2/Eps - (3*(-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/(2*Eps) + 
              MH*Sqrt[MH2 - 4*MT2]*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                 (2*MT2)] + MT2*Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/
                  (2*MT2)]^2 + (MH2 - ((-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]])/2)*Log[Mu^2/MT2] - 
              (Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[\[Theta]3])*
                     Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]]]*Log[(2*MT2 + 
                   Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)/(2*MT2)])/Sqrt[2] - MT2*Log[(2*MT2 + 
                   Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                      Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)/(2*MT2)]^2)/(MH2 - ((-1 + Cos[\[Theta]3])*
                Sqrt[Kallen\[Lambda][S, MH2, 0]])/2))*
           ((Sqrt[S]*Sin[\[Theta]3]^2)/(Sqrt[2]*Abs[Sin[\[Theta]3]]) - 
            (Cos[\[Theta]3/2]*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3/2]^3*Sin[\[Theta]3]*(Cos[\[Phi]3] - I*
                Sin[\[Phi]3])*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (Sqrt[2]*Sqrt[S]*Abs[Sin[\[Theta]3]]) - 
            ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] - I*Sin[\[Phi]3])*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Sqrt[2]*Sqrt[S]*
              Abs[Sin[\[Theta]3]])) - (Sqrt[Kallen\[Lambda][S, MH2, 0]]*
            Sin[\[Theta]3]*(((-(Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + 
                      MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)])/2 + 
                (MH*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
                  Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)])/Sqrt[2])*((-2*Cos[\[Theta]3/2]*
                  (-S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]])))/
              (MH*(-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*Sqrt[Kallen\[Lambda][
                 S, MH2, 0]]) + ((((-1 + Cos[\[Theta]3])*(-MH2 + 
                   ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                    2)*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2 - 
                (MH*Sqrt[MH2 - 4*MT2]*(-1 + Cos[\[Theta]3])*Sqrt[
                   Kallen\[Lambda][S, MH2, 0]]*Log[(-MH2 + MH*Sqrt[MH2 - 
                        4*MT2] + 2*MT2)/(2*MT2)])/2 - 
                (MT2*(-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]]*
                  Log[(-MH2 + MH*Sqrt[MH2 - 4*MT2] + 2*MT2)/(2*MT2)]^2)/2 + 
                (MH2*Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]*
                  Log[(2*MT2 + Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + 
                         ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)*Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                     ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/
                      2)/(2*MT2)])/Sqrt[2] + (MT2*(-1 + Cos[\[Theta]3])*
                  Sqrt[Kallen\[Lambda][S, MH2, 0]]*Log[(2*MT2 + 
                      Sqrt[(-1 + Cos[\[Theta]3])*(-4*MT2 + ((-1 + Cos[
                          \[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)*
                         Sqrt[Kallen\[Lambda][S, MH2, 0]]]/Sqrt[2] - 
                      ((-1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                          0]])/2)/(2*MT2)]^2)/2)*((-2*Cos[\[Theta]3/2]*
                  (-S - ((1 + Cos[\[Theta]3])*Sqrt[Kallen\[Lambda][S, MH2, 
                       0]])/2)*Sin[\[Theta]3/2]^3*(Cos[\[Phi]3] - 
                   I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]] - 
                (Sqrt[Kallen\[Lambda][S, MH2, 0]]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] - I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]])))/
              ((-1 + Cos[\[Theta]3])*(MH2 - ((-1 + Cos[\[Theta]3])*
                   Sqrt[Kallen\[Lambda][S, MH2, 0]])/2)^2*Sqrt[
                Kallen\[Lambda][S, MH2, 0]]))*(Cos[\[Phi]3] + 
             I*Sin[\[Phi]3]))/(Sqrt[2]*Sqrt[S])))/((-1 + Cos[\[Theta]3])*
         Sqrt[Kallen\[Lambda][S, MH2, 0]])))/(MW*Pi*SW)}}}}
