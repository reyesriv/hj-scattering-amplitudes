(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
-(Alfa^2*col12*MT^2*MU*Sqrt[Kallen\[Lambda][S, MH^2, MZ^2]]*
   ((9 - 24*SW^2 + 32*SW^4)*
     (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
            MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
          S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
            MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
         Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
       MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
      DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, 
            MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
          S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, 
            MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) + 
         Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
       -MH^2 + MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
      DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
           Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
        Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
      DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
           Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
        Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
      DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
         (MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, 
             MZ^2, S]])), -MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
          S]]] + DiLog[(MZ*(MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
            MZ^2, S]]))/(MH^2*MZ - MZ^3 + MZ*S + 
         Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
       -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
      DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
            MZ^2, S]]), MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
          S]]] - DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
            S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
           Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
        Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
      DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
        (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, 
            MZ^2, S]]), -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
          S]]] - DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
            MZ^2, S]]))/(-(MH^2*S) - MZ^2*S + S^2 + 
         Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
       MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]])*
     Kallen\[Lambda][MH^2, MZ^2, S]^(3/2) + 9*Kallen\[Lambda][MH^2, MZ^2, S]*
     (-((MH^2*(MH^2 - MZ^2 - S)*
         (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 - MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                 S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(MZ*(MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
          DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
          DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
            (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
                MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
            Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
        Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) - 2*MH*Sqrt[MH^2 - 4*MT^2]*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
      (Sqrt[-4*MT^2 + MZ^2]*(MH^2 + MZ^2 - S)*
        Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)])/MZ + 
      (Sqrt[S*(-4*MT^2 + S)]*(MH^2 - MZ^2 + S)*
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S) - 
    2*(9 - 24*SW^2 + 32*SW^4)*
     ((2*(MH^6*(MT^2 + MZ^2) + MT^2*(MZ^2 - S)^3 + MH^2*(MZ^2 - S)*
          (MZ^4 + 2*MZ^2*S - MT^2*(MZ^2 + 3*S)) - 
         MH^4*(2*MZ^4 - MZ^2*S + MT^2*(MZ^2 + 3*S)))*
        (DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^3 - MH*(MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - 
            Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), -MH^2 + MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) + 
            Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          -MH^2 + MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
              Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
               Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
         DiLog[(MZ*(MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), -MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 - 
            Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^
                2, MZ^2, S]]), -MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, 
             MZ^2, S]]] + DiLog[(S*(MH^2 + MZ^2 - S + 
             Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^2*S + MZ^2*S - S^2 + 
            Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
          -MH^2 - MZ^2 + S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
         DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/
           (-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*Kallen\[Lambda][
               MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
           Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
       Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]] + (MH^2 + MZ^2 - S)*
       Kallen\[Lambda][MH^2, MZ^2, S] + MH*Sqrt[MH^2 - 4*MT^2]*
       (MH^4 - 5*MZ^4 + MH^2*(4*MZ^2 - 2*S) + 4*MZ^2*S + S^2)*
       Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)] + 
      MZ*Sqrt[-4*MT^2 + MZ^2]*(-5*MH^4 + (MZ^2 - S)^2 + 4*MH^2*(MZ^2 + S))*
       Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/(2*MT^2)] - 
      (Sqrt[S*(-4*MT^2 + S)]*(MH^6 - MH^4*(MZ^2 + 2*S) + (MZ^3 - MZ*S)^2 + 
         MH^2*(-MZ^4 + 8*MZ^2*S + S^2))*
        Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S - 
      Kallen\[Lambda][MH^2, MZ^2, S]*
       (-((MZ^2*(-MH^2 + MZ^2 - S)*(DiLog[(MH*(MH^2 - MZ^2 - S - 
                Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]))/(MH^3 - MH*
                (MZ^2 + S) - Sqrt[(MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, 
                  S]]), MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                S]]] - DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][
                  MH^2, MZ^2, S]]))/(MH^3 - MH*(MZ^2 + S) - Sqrt[
                (MH^2 - 4*MT^2)*Kallen\[Lambda][MH^2, MZ^2, S]]), 
             MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MH*(MH^2 - MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MH*(MH^2 - MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^3 - MH*(MZ^2 + S) + Sqrt[(MH^2 - 4*MT^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MZ*(-MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*MZ) + MZ^3 - MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[-((MZ*(-MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                   S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                  Kallen\[Lambda][MH^2, MZ^2, S]])), -MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(MZ*(MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*MZ - MZ^3 + MZ*S + Sqrt[(-4*MT^2 + MZ^2)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 - Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(MH^2 + MZ^2 - S - Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S + 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] + 
            DiLog[(S*(MH^2 + MZ^2 - S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(MH^2*S + MZ^2*S - S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), -MH^2 - MZ^2 + S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]] - 
            DiLog[(S*(-MH^2 - MZ^2 + S + Sqrt[Kallen\[Lambda][MH^2, MZ^2, 
                  S]]))/(-(MH^2*S) - MZ^2*S + S^2 + Sqrt[S*(-4*MT^2 + S)*
                 Kallen\[Lambda][MH^2, MZ^2, S]]), MH^2 + MZ^2 - S - 
              Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]]))/
          Sqrt[Kallen\[Lambda][MH^2, MZ^2, S]]) + 
        (Sqrt[MH^2 - 4*MT^2]*(MH^2 + MZ^2 - S)*
          Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)])/MH - 
        2*MZ*Sqrt[-4*MT^2 + MZ^2]*Log[1 + (MZ*(-MZ + Sqrt[-4*MT^2 + MZ^2]))/
            (2*MT^2)] + (Sqrt[S*(-4*MT^2 + S)]*(-MH^2 + MZ^2 + S)*
          Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)])/S)))*
   Sin[\[Theta]3]*Sin[\[Phi]3]*
   ((-I)*Conjugate[Cos[\[Phi]3] + I*Cos[\[Theta]3]*Sin[\[Phi]3]] + 
    I*Cos[\[Phi]3] + Cos[\[Theta]3]*Sin[\[Phi]3]))/
 (24*Sqrt[2]*CW^3*MW*(-MZ^2 + S)*SW^4*Kallen\[Lambda][MH^2, MZ^2, S]^2)
