(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
(Alfas*col421*EL*GS*MT^2*(-1 + Cos[\[Theta]3])*
  (-4*Sqrt[MH^2 - 4*MT^2]*S*Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/
      (2*MT^2)] + MH*(MH^2 - 4*MT^2 - S)*
    Log[(-MH^2 + 2*MT^2 + MH*Sqrt[MH^2 - 4*MT^2])/(2*MT^2)]^2 + 
   MH*(-4*MH^2 + 4*S + 4*Sqrt[S*(-4*MT^2 + S)]*
      Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)] + 
     (-MH^2 + 4*MT^2 + S)*Log[(2*MT^2 - S + Sqrt[S*(-4*MT^2 + S)])/(2*MT^2)]^
       2))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(8*Sqrt[2]*MH*MW*Pi*(MH^2 - S)*
  Sqrt[S]*SW)
