(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {{{(Alfas*(c124 - c142)*EL*GS*MT^2*
      (-((PVC[0, 0, 0, 0, U, MH^2, MT, MT, MT]*
           (-((k3*Sin[\[Theta]3]*(-((-MH^2 + U)*(1 + Cos[\[Theta]3])*
                   Sin[\[Theta]3]*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
                 (2*Abs[Sin[\[Theta]3]]) + (k3*Sqrt[S]*Sin[\[Theta]3]^3*
                  (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]))*(
                Cos[\[Phi]3] + I*Sin[\[Phi]3]))/Sqrt[2]) + 
            (k3*S*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]^2*(Cos[\[Phi]3] + 
                I*Sin[\[Phi]3])^2)/(2*Sqrt[2]*Abs[Sin[\[Theta]3]])) + 
          PVC[0, 0, 1, 0, U, MH^2, MT, MT, MT]*
           (-(k3*(-3*S + T)*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]^2*
               (Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/(2*Sqrt[2]*
              Abs[Sin[\[Theta]3]]) - (k3*(MH^2 - U)*(1 + Cos[\[Theta]3])*
              Sin[\[Theta]3]^2*(Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/
             (Sqrt[2]*Abs[Sin[\[Theta]3]]) - (Sqrt[2]*k3^2*Sqrt[S]*
              Sin[\[Theta]3]^4*(Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/
             Abs[Sin[\[Theta]3]]) - Sqrt[2]*k3*PVC[0, 0, 2, 0, U, MH^2, MT, 
            MT, MT]*Sin[\[Theta]3]*(((-S + T)*(1 + Cos[\[Theta]3])*
              Sin[\[Theta]3]*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (2*Abs[Sin[\[Theta]3]]) + (k3*Sqrt[S]*Sin[\[Theta]3]^3*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]])*
           (Cos[\[Phi]3] + I*Sin[\[Phi]3]) - Sqrt[2]*k3*PVC[0, 1, 1, 0, U, 
            MH^2, MT, MT, MT]*Sin[\[Theta]3]*(((-S + T)*(1 + Cos[\[Theta]3])*
              Sin[\[Theta]3]*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
             (2*Abs[Sin[\[Theta]3]]) + (k3*Sqrt[S]*Sin[\[Theta]3]^3*
              (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]])*
           (Cos[\[Phi]3] + I*Sin[\[Phi]3]) - (k3*(1 + Cos[\[Theta]3])*
            (2*U + U/Eps + U*Log[Mu^2/MT^2] + Sqrt[U*(-4*MT^2 + U)]*
              Log[(2*MT^2 - U + Sqrt[U*(-4*MT^2 + U)])/(2*MT^2)] - 
             4*U*PVC[1, 0, 0, 0, U, MH^2, MT, MT, MT])*Sin[\[Theta]3]^2*
            (Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/(2*Sqrt[2]*U*
            Abs[Sin[\[Theta]3]]))/U) + 
       (T*PVC[0, 1, 0, MH^2, T, 0, MT, MT, MT]*
          ((k3*Sin[\[Theta]3]*(-((-3*S + U)*(1 + Cos[\[Theta]3])*
                 Sin[\[Theta]3]*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*
                Abs[Sin[\[Theta]3]]) + (2*k3*Sqrt[S]*Sin[\[Theta]3]^3*
                (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/Abs[Sin[\[Theta]3]])*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/Sqrt[2] - 
           (k3*(MH^2 - T)*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]^2*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/(Sqrt[2]*
             Abs[Sin[\[Theta]3]])) + T*PVC[0, 0, 0, MH^2, T, 0, MT, MT, MT]*
          ((k3*Sin[\[Theta]3]*((S*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]*
                (Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
              (k3*Sqrt[S]*Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/(
                2*Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
            Sqrt[2] - (k3*(MH^2 - T)*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]^2*
             (Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/(2*Sqrt[2]*
             Abs[Sin[\[Theta]3]])) + Sqrt[2]*k3*T*Sin[\[Theta]3]*
          (PVC[0, 1, 1, MH^2, T, 0, MT, MT, MT]*
            (-((-S + U)*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]*(Cos[\[Phi]3] + 
                 I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (k3*Sqrt[S]*Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              Abs[Sin[\[Theta]3]]) + PVC[0, 2, 0, MH^2, T, 0, MT, MT, MT]*
            (-((-S + U)*(1 + Cos[\[Theta]3])*Sin[\[Theta]3]*(Cos[\[Phi]3] + 
                 I*Sin[\[Phi]3]))/(2*Abs[Sin[\[Theta]3]]) + 
             (k3*Sqrt[S]*Sin[\[Theta]3]^3*(Cos[\[Phi]3] + I*Sin[\[Phi]3]))/
              Abs[Sin[\[Theta]3]]))*(Cos[\[Phi]3] + I*Sin[\[Phi]3]) - 
         (k3*(1 + Cos[\[Theta]3])*(2*T + T/Eps + T*Log[Mu^2/MT^2] + 
            Sqrt[T*(-4*MT^2 + T)]*Log[(2*MT^2 - T + Sqrt[T*(-4*MT^2 + T)])/(2*
                MT^2)] - 4*T*PVC[1, 0, 0, MH^2, T, 0, MT, MT, MT])*
           Sin[\[Theta]3]^2*(Cos[\[Phi]3] + I*Sin[\[Phi]3])^2)/
          (2*Sqrt[2]*Abs[Sin[\[Theta]3]]))/T^2))/(MW*Pi*SW)}}}}
