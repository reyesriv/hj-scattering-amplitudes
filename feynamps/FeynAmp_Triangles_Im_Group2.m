(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{
 {
  {
   {-((Alfas^2*col3*EL*MT^2*
        (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
              S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
              (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - (MT^2*
                 Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                        S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                      S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
            (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
              Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
           (((-16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[
                -4*MT^2 + S35]*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] + 
                Pair[e[1], ec[4]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
                 Pair[ec[4], k[3]]))/((MH^2 - S35)*S35) + 
             (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
              ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
               (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*
                       (-4*MT^2 + S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*
                     Sqrt[S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
              (Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
                Pair[e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
            Pair[ec[5], k[3]] - ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
             HeavisideTheta[-4*MT^2 + S35]*(Pair[e[2], ec[4]]*(
                (-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 
                2*(Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
              Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 
                2*(Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
              2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 
                2*(Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], 
                  k[3]])))/((MH^2 - S35)*S35) - 
           ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*Log[((MH^2 - S35)*S35 + 
                (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/((MH^2 - S35)*S35 + 
                (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
             (Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 
                2*Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*(
                (-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                 Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + 
                  U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], 
                  k[3]])))/(MH^2 - S35))/S35 + 
         ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
              S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
              (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - (MT^2*
                 Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                        S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                      S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
            (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
              Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
           ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
             (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 
                2*(Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
              Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 
                2*(Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
              2*Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 
                2*Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], 
                   k[4]]))))/((MH^2 - S34)*S34) + Pair[ec[4], k[3]]*
            (((-16*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[
                -4*MT^2 + S34]*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] + 
                Pair[e[1], ec[5]]*Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*
                 Pair[ec[5], k[3]]))/((MH^2 - S34)*S34) + 
             (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
              ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
               (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*
                       (-4*MT^2 + S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*
                     Sqrt[S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
              (Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
                Pair[e[2], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[4]])) + 
           ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*Log[((MH^2 - S34)*S34 + 
                (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/((MH^2 - S34)*S34 + 
                (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
             (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 
                2*Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*(
                (-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                 Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*((-MH^2 + S34)*
                 Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], 
                  k[4]])))/(MH^2 - S34))/S34))/(MW*SW)) - 
     (Alfas^2*col4*EL*MT^2*
       (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          (((-16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + 
                S35]*(Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
                Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[3]]))/
             ((MH^2 - S35)*S35) + (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (Pair[e[1], k[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*Pair[
                e[2], k[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
           Pair[ec[5], k[3]] - ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
            HeavisideTheta[-4*MT^2 + S35]*(Pair[e[2], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) + 
             Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
             2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 2*
                (Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])))/
           ((MH^2 - S35)*S35) - ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 2*
                Pair[e[1], k[5]]*Pair[ec[5], k[3]]) + Pair[e[1], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                Pair[ec[5], k[3]]) - 2*Pair[e[1], e[2]]*((-MH^2 + S34 + T + 
                 U)*Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], 
                 k[3]])))/(MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           (Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - 2*Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) + 
             Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
             2*Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 2*
                Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/
           ((MH^2 - S34)*S34) + Pair[ec[4], k[3]]*
           (((-16*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + 
                S34]*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*
                Pair[e[2], k[3]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[3]]))/
             ((MH^2 - S34)*S34) + (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             (Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*Pair[
                e[2], k[4]] - 2*Pair[e[1], e[2]]*Pair[ec[5], k[4]])) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 2*
                Pair[e[1], k[4]]*Pair[ec[4], k[3]]) + Pair[e[1], ec[5]]*
              ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]) - 2*Pair[e[1], e[2]]*((-MH^2 + S34)*
                Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))/
           (MH^2 - S34))/S34))/(MW*SW) + 
     (Alfas^2*col1*EL*MT^2*
       (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          (((16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
              (Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
                Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]))/
             ((MH^2 - S35)*S35) - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*Pair[
                e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
           Pair[ec[5], k[3]] + ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
            HeavisideTheta[-4*MT^2 + S35]*(Pair[e[2], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
             2*Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
             Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 2*
                (Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])))/
           ((MH^2 - S35)*S35) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 2*
                Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*
                Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])))/
           (MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
             Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
             Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 2*
                Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/
           ((MH^2 - S34)*S34) - 16*Pair[ec[4], k[3]]*
           ((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
              (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
                Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[5], k[3]]))/
             ((MH^2 - S34)*S34) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             (-2*Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*Pair[
                e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[5], k[4]])) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 2*
                Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
              ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*((-MH^2 + S34)*
                Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))/
           (MH^2 - S34))/S34))/(MW*SW) + 
     (Alfas^2*col2*EL*MT^2*
       (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          (((16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
              (Pair[e[1], k[3]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*
                Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], k[3]]))/
             ((MH^2 - S35)*S35) - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             ((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*(MH^2 - S35)^2*S35) + 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(MH^2 - S35)^2)*
             (Pair[e[1], k[5]]*Pair[e[2], ec[4]] - 2*Pair[e[1], ec[4]]*Pair[
                e[2], k[5]] + Pair[e[1], e[2]]*Pair[ec[4], k[5]]))*
           Pair[ec[5], k[3]] + ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*
            HeavisideTheta[-4*MT^2 + S35]*(Pair[e[2], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
             2*Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) + 
             Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 2*
                (Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])))/
           ((MH^2 - S35)*S35) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 2*
                Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - 2*Pair[e[1], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                Pair[ec[5], k[3]]) + Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*
                Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])))/
           (MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           (-(Pair[e[1], ec[5]]*Pair[e[2], ec[4]]) + 2*Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] - Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
             Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) - 
             Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 2*
                Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/
           ((MH^2 - S34)*S34) - 16*Pair[ec[4], k[3]]*
           ((I*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
              (2*Pair[e[1], k[3]]*Pair[e[2], ec[5]] - Pair[e[1], ec[5]]*
                Pair[e[2], k[3]] - Pair[e[1], e[2]]*Pair[ec[5], k[3]]))/
             ((MH^2 - S34)*S34) + (2*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             ((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*(MH^2 - S34)^2*S34) + 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(MH^2 - S34)^2)*
             (-2*Pair[e[1], k[4]]*Pair[e[2], ec[5]] + Pair[e[1], ec[5]]*Pair[
                e[2], k[4]] + Pair[e[1], e[2]]*Pair[ec[5], k[4]])) + 
          ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (2*Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 2*
                Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - Pair[e[1], ec[5]]*
              ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]) - Pair[e[1], e[2]]*((-MH^2 + S34)*
                Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))/
           (MH^2 - S34))/S34))/(MW*SW) - 
     (Alfas^2*col5*EL*MT^2*
       (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          (((-16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + 
                S35]*(-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], 
                 ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], 
                 k[3]]))/((MH^2 - S35)*S35) + (32*I)*Pi*HeavisideTheta[
              -4*MT^2 + S35]*((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*
                (MH^2 - S35)^2*S35) + (MT^2*Log[((MH^2 - S35)*S35 + 
                   (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/((MH^2 - S35)*
                    S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/
               (MH^2 - S35)^2)*(-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + 
              Pair[e[1], ec[4]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
            (2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
             Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
             Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 2*
                (Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])))/
           ((MH^2 - S35)*S35) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 2*
                Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*
                Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])))/
           (MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
             2*Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
             Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 2*
                Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/
           ((MH^2 - S34)*S34) + Pair[ec[4], k[3]]*
           (((-16*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + 
                S34]*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 2*Pair[e[1], 
                 ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[5], 
                 k[3]]))/((MH^2 - S34)*S34) + (32*I)*Pi*HeavisideTheta[
              -4*MT^2 + S34]*((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*
                (MH^2 - S34)^2*S34) + (MT^2*Log[((MH^2 - S34)*S34 + 
                   (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/((MH^2 - S34)*
                    S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
               (MH^2 - S34)^2)*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 
              2*Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[4]])) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 2*
                Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
              ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*((-MH^2 + S34)*
                Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))/
           (MH^2 - S34))/S34))/(MW*SW) - 
     (Alfas^2*col6*EL*MT^2*
       (((((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35])/
             S35 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
             (-Sqrt[S35*(-4*MT^2 + S35)]/(4*(MH^2 - S35)) - 
              (MT^2*Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + 
                       S35)])/((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[
                     S35*(-4*MT^2 + S35)])])/(2*(MH^2 - S35))))*
           (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          (((-16*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + 
                S35]*(-2*Pair[e[1], k[3]]*Pair[e[2], ec[4]] + Pair[e[1], 
                 ec[4]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[4], 
                 k[3]]))/((MH^2 - S35)*S35) + (32*I)*Pi*HeavisideTheta[
              -4*MT^2 + S35]*((MH^2*Sqrt[S35*(-4*MT^2 + S35)])/(2*
                (MH^2 - S35)^2*S35) + (MT^2*Log[((MH^2 - S35)*S35 + 
                   (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/((MH^2 - S35)*
                    S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])])/
               (MH^2 - S35)^2)*(-2*Pair[e[1], k[5]]*Pair[e[2], ec[4]] + 
              Pair[e[1], ec[4]]*Pair[e[2], k[5]] + Pair[e[1], e[2]]*Pair[
                ec[4], k[5]]))*Pair[ec[5], k[3]] + 
          ((8*I)*Pi*Sqrt[S35*(-4*MT^2 + S35)]*HeavisideTheta[-4*MT^2 + S35]*
            (2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] - 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[5]])*Pair[ec[5], k[3]]) - 
             Pair[e[1], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] - 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[5]])*Pair[ec[5], k[3]]) - 
             Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*Pair[ec[4], ec[5]] - 2*
                (Pair[ec[4], k[3]] - Pair[ec[4], k[5]])*Pair[ec[5], k[3]])))/
           ((MH^2 - S35)*S35) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S35]*
            Log[((MH^2 - S35)*S35 + (-MH^2 + S35)*Sqrt[S35*(-4*MT^2 + S35)])/
              ((MH^2 - S35)*S35 + (MH^2 - S35)*Sqrt[S35*(-4*MT^2 + S35)])]*
            (2*Pair[e[2], ec[4]]*((-MH^2 + S34 + T + U)*Pair[e[1], ec[5]] + 2*
                Pair[e[1], k[5]]*Pair[ec[5], k[3]]) - Pair[e[1], ec[4]]*
              ((-MH^2 + S34 + T + U)*Pair[e[2], ec[5]] + 2*Pair[e[2], k[5]]*
                Pair[ec[5], k[3]]) - Pair[e[1], e[2]]*((-MH^2 + S34 + T + U)*
                Pair[ec[4], ec[5]] + 2*Pair[ec[4], k[5]]*Pair[ec[5], k[3]])))/
           (MH^2 - S35))/S35 + 
        ((((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34])/
             S34 - (32*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
             (-Sqrt[S34*(-4*MT^2 + S34)]/(4*(MH^2 - S34)) - 
              (MT^2*Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + 
                       S34)])/((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[
                     S34*(-4*MT^2 + S34)])])/(2*(MH^2 - S34))))*
           (-2*Pair[e[1], ec[5]]*Pair[e[2], ec[4]] + Pair[e[1], ec[4]]*
             Pair[e[2], ec[5]] + Pair[e[1], e[2]]*Pair[ec[4], ec[5]]) + 
          ((8*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + S34]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] + 2*
                (Pair[e[1], k[3]] - Pair[e[1], k[4]])*Pair[ec[4], k[3]]) - 
             2*Pair[e[1], ec[5]]*((-MH^2 + S34)*Pair[e[2], ec[4]] + 2*
                (Pair[e[2], k[3]] - Pair[e[2], k[4]])*Pair[ec[4], k[3]]) + 
             Pair[e[1], e[2]]*((-MH^2 + S34)*Pair[ec[4], ec[5]] + 2*
                Pair[ec[4], k[3]]*(Pair[ec[5], k[3]] - Pair[ec[5], k[4]]))))/
           ((MH^2 - S34)*S34) + Pair[ec[4], k[3]]*
           (((-16*I)*Pi*Sqrt[S34*(-4*MT^2 + S34)]*HeavisideTheta[-4*MT^2 + 
                S34]*(Pair[e[1], k[3]]*Pair[e[2], ec[5]] - 2*Pair[e[1], 
                 ec[5]]*Pair[e[2], k[3]] + Pair[e[1], e[2]]*Pair[ec[5], 
                 k[3]]))/((MH^2 - S34)*S34) + (32*I)*Pi*HeavisideTheta[
              -4*MT^2 + S34]*((MH^2*Sqrt[S34*(-4*MT^2 + S34)])/(2*
                (MH^2 - S34)^2*S34) + (MT^2*Log[((MH^2 - S34)*S34 + 
                   (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/((MH^2 - S34)*
                    S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])])/
               (MH^2 - S34)^2)*(Pair[e[1], k[4]]*Pair[e[2], ec[5]] - 
              2*Pair[e[1], ec[5]]*Pair[e[2], k[4]] + Pair[e[1], e[2]]*Pair[
                ec[5], k[4]])) + ((4*I)*Pi*HeavisideTheta[-4*MT^2 + S34]*
            Log[((MH^2 - S34)*S34 + (-MH^2 + S34)*Sqrt[S34*(-4*MT^2 + S34)])/
              ((MH^2 - S34)*S34 + (MH^2 - S34)*Sqrt[S34*(-4*MT^2 + S34)])]*
            (Pair[e[2], ec[5]]*((-MH^2 + S34)*Pair[e[1], ec[4]] - 2*
                Pair[e[1], k[4]]*Pair[ec[4], k[3]]) - 2*Pair[e[1], ec[5]]*
              ((-MH^2 + S34)*Pair[e[2], ec[4]] - 2*Pair[e[2], k[4]]*
                Pair[ec[4], k[3]]) + Pair[e[1], e[2]]*((-MH^2 + S34)*
                Pair[ec[4], ec[5]] - 2*Pair[ec[4], k[3]]*Pair[ec[5], k[4]])))/
           (MH^2 - S34))/S34))/(MW*SW)}}}}
